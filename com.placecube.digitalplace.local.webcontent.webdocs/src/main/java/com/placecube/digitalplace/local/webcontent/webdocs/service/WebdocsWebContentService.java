package com.placecube.digitalplace.local.webcontent.webdocs.service;

import java.util.Collections;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.webdocs.constants.WebdocsConstants;
import com.placecube.digitalplace.local.webcontent.webdocs.constants.WebdocsDDMTemplate;

@Component(immediate = true, service = WebdocsWebContentService.class)
public class WebdocsWebContentService {

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	private Portal portal;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		try {
			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
                    "com/placecube/digitalplace/local/webcontent/webdocs/dependencies/ddm/structure.xml", serviceContext);

			return ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), WebdocsConstants.STRUCTURE_KEY);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public DDMTemplate getOrCreateDDMTemplate(ServiceContext serviceContext, WebdocsDDMTemplate webdocsDDMTemplate) throws PortalException {
		try {
			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), webdocsDDMTemplate.getKey());

			return Validator.isNotNull(ddmTemplate) ? ddmTemplate : addDDMTemplate(serviceContext, webdocsDDMTemplate);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private DDMTemplate addDDMTemplate(ServiceContext serviceContext, WebdocsDDMTemplate webdocsDDMTemplate) throws PortalException {

		try {
			String script = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/webdocs/dependencies/ddm/" + webdocsDDMTemplate.getKey() + ".ftl");

			DDMStructure guideStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), WebdocsConstants.STRUCTURE_KEY);

			return ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), guideStructure.getStructureId(),
					guideStructure.getClassNameId(), webdocsDDMTemplate.getKey(), Collections.singletonMap(serviceContext.getLocale(), webdocsDDMTemplate.getName()), null,
					DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, script, true, false, StringPool.BLANK, null,
					serviceContext);
		} catch (Exception e) { 
			throw new PortalException(e);
		}
	}
}
