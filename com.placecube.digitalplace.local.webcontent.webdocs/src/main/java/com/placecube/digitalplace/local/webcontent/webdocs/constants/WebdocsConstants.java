package com.placecube.digitalplace.local.webcontent.webdocs.constants;

public final class WebdocsConstants {

	public static final String STRUCTURE_KEY = "WEBDOCS";

	private WebdocsConstants() {

	}

}
