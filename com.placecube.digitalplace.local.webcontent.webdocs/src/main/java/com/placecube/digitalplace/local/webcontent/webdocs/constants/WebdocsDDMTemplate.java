package com.placecube.digitalplace.local.webcontent.webdocs.constants;

public enum WebdocsDDMTemplate {

	WEBDOCS_CONTENT("WEBDOCS-CONTENT", "Webdocs Content"),
	WEBDOCS_NAVIGATION("WEBDOCS-NAVIGATION", "Webdocs Navigation"),
	WEBDOCS_TITLE("WEBDOCS-TITLE", "Webdocs Title");

	private final String key;
	private final String name;

	WebdocsDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
