package com.placecube.digitalplace.local.webcontent.webdocs.util;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = WebdocsWebContentUtil.class)
public class WebdocsWebContentUtil {

    public String generateWebdocsSectionHash(String predefinedSectionHash, String sectionTitle) {
        return Validator.isNotNull(predefinedSectionHash) ? escapeHash(predefinedSectionHash) : generateSectionHashFromTitle(sectionTitle);
    }

    private String escapeHash(String hash) {
        final String regex = "[^a-zA-Z0-9]+";

        String escapedHash = hash.replaceAll(regex, StringPool.SPACE);

        return escapedHash.replaceAll(StringPool.SPACE, StringPool.DASH).toLowerCase();
    }

    private String generateSectionHashFromTitle(String sectionTitle) {
        if (Validator.isNotNull(sectionTitle)) {
            return escapeHash(sectionTitle);
        } else {
            return StringUtil.randomString(4).toLowerCase();
        }
    }
}
