<#assign DISPLAYDATE = .vars['reserved-article-modified-date'].data?date('EEE, dd MMM yyyy HH:mm:ss Z') />
<#assign title = .vars['reserved-article-title'].data/>

<#if (HeaderImage.getData())?? && HeaderImage.getData() != "">

	<div alt="${HeaderImage.getAttribute("alt")}" data-fileentryid="${HeaderImage.getAttribute("fileEntryId")}" class="banner py-6 text-dark text-break" style="background-image: url('${HeaderImage.getData()}'); background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-8 col-xl-6" style="background: rgba(204, 204, 204, 0.75);">
					<h1 class="text-dark">
						<div>
							${title}
						</div>
					</h1>
					<div class="mb-4 lead">
						<#if (ShowDate.getData())?? && ShowDate.getData() == "true">
							<p class="text-dark">Last Updated  ${DISPLAYDATE?string["dd MMMM yyyy"]}</p>
						</#if>
					</div>

				</div>
			</div>
		</div>
	</div>
<#else>
	<div class="digitalplace-banner-bg">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1>
						<div>
							${title}
						</div>
					</h1>
					<div class="mb-4 lead">
						<#if (ShowDate.getData())?? && ShowDate.getData() == "true">
							<p >Last Updated  ${DISPLAYDATE?string["dd MMMM yyyy"]}</p>
						</#if>
					</div>
				</div>
			</div>
		</div>
	</div>
</#if>