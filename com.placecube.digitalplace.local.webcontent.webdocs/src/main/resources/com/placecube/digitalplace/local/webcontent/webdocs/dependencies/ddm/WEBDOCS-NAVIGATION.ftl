<#if SectionTitle?? && SectionTitle.getSiblings()?has_content && SectionTitle.getSiblings()?size gt 1 >

    <#assign webdocsWebContentUtil = digitalplace_webdocsWebContentUtil />
	<div  class="multi-webdocs-navigation left-tab-nav">
		<ul  class="nav__list nav">

            <#list SectionTitle.getSiblings() as cur_SectionTitle>
                <#if SectionTitle.SectionHash??>
                    <#assign sectionHash = cur_SectionTitle.SectionHash.getData() />
                <#else>
                    <#assign sectionHash = '' />
                </#if>

                <#assign instanceId = webdocsWebContentUtil.generateWebdocsSectionHash(sectionHash, cur_SectionTitle.getData() ) />

				<li class="list__item ${(cur_SectionTitle?index == 0)?then('list__item--active-child','')}">
					<a class="list__link" data-index="${cur_SectionTitle?index}" data-instance-id="${instanceId}" href="#${instanceId}">
						<span class="list__link-text ">${cur_SectionTitle.getData()}</span>
					</a>
				</li>
            </#list>

		<ul>
    </div>
</#if>

<div class="p-3">
    <#if LinkedPDF.getData()?has_content>
        <i class="icon icon-file-alt" aria-hidden="true"></i>  
		<a href="${LinkedPDF.getData()}">
           ${languageUtil.format(locale, "download-x", "full document", false)}
		</a>
    <#else>
        <@liferay_ui["icon"] image="print"/> <a href="#" onClick="window.print();">Print this page</a>
    </#if>
</div>
	<div class="position-fixed scrollup z-[100000]">
		<i class="icon icon-arrow-up" aria-hidden="true"></i> <a href="#topline"> Contents </a>
	</div>
<script>

$(document).ready(function(){
  
	function mobileTabDropdown(){
			$('.left-tab-nav .list__item').on('click', function(){
				$(this).parent().prepend($(this));
				$('.left-tab-nav .list__item').toggleClass('visible-xs visible-sm');
			});
	}

	if($(window).width() < 992) {
	 mobileTabDropdown();
	}

	$(window).resize(function() {
	 if($(window).width() < 992) {
			mobileTabDropdown();
		}
		else {
		 $('.left-tab-nav .list__item').off();
		}
	});

	<!--Smooth Page Scroll to Top-->
		$(window).scroll(function(){
			if ($(this).scrollTop() > 400) {
				$('.scrollup').fadeIn();
			} else {
				$('.scrollup').fadeOut();
			}
		}); 
 
		$('.scrollup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
			return false;
		});
	<!--//-->
	
});
</script>