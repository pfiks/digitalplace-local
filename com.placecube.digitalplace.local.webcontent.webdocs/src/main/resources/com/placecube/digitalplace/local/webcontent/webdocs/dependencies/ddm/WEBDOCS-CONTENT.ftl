<#if SectionTitle?? && SectionTitle.getSiblings()?has_content>

    <#assign webdocsWebContentUtil = digitalplace_webdocsWebContentUtil />

	<div  class="multi-part-guide-content tab-content">

        <#list SectionTitle.getSiblings() as cur_SectionTitle>
            <#if SectionTitle.SectionHash??>
                <#assign sectionHash = cur_SectionTitle.SectionHash.getData() />
            <#else>
                <#assign sectionHash = '' />
            </#if>

            <#assign instanceId = webdocsWebContentUtil.generateWebdocsSectionHash(sectionHash, cur_SectionTitle.getData() ) />

			<div id="multi_part_section_${instanceId}" data-instance-id="${instanceId}" class="multi_part_section ${(cur_SectionTitle?index gt 0)?then('d-none','')}">

				<article>
					<h2 id="topline_${cur_SectionTitle.getData()?lower_case?replace(" ", "_")}" class="section_title">${cur_SectionTitle.getData()}</h2>
					<div class="section_content">${cur_SectionTitle.SectionContent.getData()}</div>
				</article>

			

				<nav class="pagination">

					<ul class="pagination__list">

                        <#if cur_SectionTitle?index gt 0>
                            <#assign previousSibling = SectionTitle.getSiblings()[cur_SectionTitle?index-1] />

                            <#if previousSibling.SectionHash??>
                                <#assign previousSectionHash = previousSibling.SectionHash.getData() />
                            <#else>
                                <#assign previousSectionHash = '' />
                            </#if>

                            <#assign previousInstanceId = webdocsWebContentUtil.generateWebdocsSectionHash(previousSectionHash, previousSibling.getData() ) />

							<li class="pagination__item pagination__item--previous">
								<a class="pagination__link multi_part_nav_previous" data-instance-id="${previousInstanceId}" href="#${previousInstanceId}">
									<strong class="pagination__label">Previous<span class="d-none"> page</span></strong>
									<span class="pagination__meta-group">
										<span class="glyphicon glyphicon-circle-arrow-left"></span>
										<span class="pagination__meta">
											${previousSibling.getData()}
										</span>
									</span>
								</a>
							</li>
                        </#if>

                        <#if cur_SectionTitle?has_next>
                            <#assign nextSibling = SectionTitle.getSiblings()[cur_SectionTitle?index+1] />

                            <#if nextSibling.SectionHash??>
                                <#assign nextSectionHash = nextSibling.SectionHash.getData() />
                            <#else>
                                <#assign nextSectionHash = '' />
                            </#if>

                            <#assign nextInstanceId = webdocsWebContentUtil.generateWebdocsSectionHash(nextSectionHash, nextSibling.getData() ) />

							<li class="pagination__item pagination__item--next">
								<a class="pagination__link multi_part_nav_next" data-instance-id="${nextInstanceId}" href="#${nextInstanceId}">
									<strong class="pagination__label">Next<span class="d-none"> page</span></strong>
									<span class="pagination__meta-group">
										<span class="pagination__meta">
											${nextSibling.getData()}
										</span>
										<span class="glyphicon glyphicon-circle-arrow-right"></span>
									</span>
								</a>
							</li>
                        </#if>
					</ul>
				</nav>
			</div>
        </#list>

	</div>
	
</#if>