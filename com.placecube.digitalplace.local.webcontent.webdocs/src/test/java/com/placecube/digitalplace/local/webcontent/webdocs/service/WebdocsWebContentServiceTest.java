package com.placecube.digitalplace.local.webcontent.webdocs.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import java.util.Collections;
import java.util.Locale;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.webdocs.constants.WebdocsConstants;
import com.placecube.digitalplace.local.webcontent.webdocs.constants.WebdocsDDMTemplate;

public class WebdocsWebContentServiceTest {

	private static final Locale LOCALE = Locale.CANADA;

	private static MockedStatic<StringUtil> mockedStatic;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private DefaultDDMStructureHelper mockDefaultDDMStructureHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private WebdocsWebContentService webdocsWebContentService;

	@AfterClass
	public static void cleanup() {
		mockedStatic.close();
	}

	@BeforeClass
	public static void init() {
		mockedStatic = mockStatic(StringUtil.class);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, WebdocsConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		webdocsWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenAddsDDMStructures() throws Exception {
		long groupId = 123;
		long classNameId = 456;
		long userId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);

		webdocsWebContentService.getOrCreateDDMStructure(mockServiceContext);

		verify(mockDefaultDDMStructureHelper, times(1)).addDDMStructures(userId, groupId, classNameId, getClass().getClassLoader(),
				"com/placecube/digitalplace/local/webcontent/webdocs/dependencies/ddm/structure.xml", mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheGuideStructure() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, WebdocsConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure result = webdocsWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, WebdocsDDMTemplate.WEBDOCS_CONTENT.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, WebdocsConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		webdocsWebContentService.getOrCreateDDMTemplate(mockServiceContext, WebdocsDDMTemplate.WEBDOCS_CONTENT);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExist_ThenCreatesTemplate() throws PortalException {

		long groupId = 1;
		long classNameId = 2;
		long userId = 3;
		long structureId = 4;
		long journalArticleClassNameId = 5;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalArticleClassNameId);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, WebdocsDDMTemplate.WEBDOCS_CONTENT.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, journalArticleClassNameId, WebdocsConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getClassNameId()).thenReturn(classNameId);

		webdocsWebContentService.getOrCreateDDMTemplate(mockServiceContext, WebdocsDDMTemplate.WEBDOCS_CONTENT);

		verify(mockDDMTemplateLocalService, times(1)).addTemplate(userId, groupId, classNameId, structureId, classNameId, WebdocsDDMTemplate.WEBDOCS_CONTENT.getKey(),
				Collections.singletonMap(LOCALE, WebdocsDDMTemplate.WEBDOCS_CONTENT.getName()), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE,
				TemplateConstants.LANG_TYPE_FTL, null, true, false, StringPool.BLANK, null, mockServiceContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateExists_ThenReturnTemplate() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, WebdocsDDMTemplate.WEBDOCS_CONTENT.getKey())).thenReturn(mockDDMTemplate);

		DDMTemplate result = webdocsWebContentService.getOrCreateDDMTemplate(mockServiceContext, WebdocsDDMTemplate.WEBDOCS_CONTENT);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Before
	public void setUp() {
		openMocks(this);
	}

}
