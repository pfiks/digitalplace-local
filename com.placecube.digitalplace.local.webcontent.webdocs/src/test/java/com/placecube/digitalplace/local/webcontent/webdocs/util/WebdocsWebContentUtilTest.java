package com.placecube.digitalplace.local.webcontent.webdocs.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

public class WebdocsWebContentUtilTest {

	private static final String ESCAPED_HASH = "resident-permit-fees-and-tariffs";
	private static MockedStatic<StringUtil> mockedStatic;

	private static final String UNESCAPED_HASH = "Resident (\"£*%)£)$^*\\permit__fees=and//tariffs";

	@InjectMocks
	private WebdocsWebContentUtil webdocsWebContentUtil;

	@AfterClass
	public static void cleanup() {
		mockedStatic.close();
	}

	@BeforeClass
	public static void init() {
		mockedStatic = mockStatic(StringUtil.class);
	}

	@Test
	public void generateWebdocsSectionHash_WhenPredefinedSectionHashIsNotEmpty_ThenReturnsEscapedPredefinedHash() {
		String result = webdocsWebContentUtil.generateWebdocsSectionHash(UNESCAPED_HASH, StringPool.BLANK);
		assertThat(result, equalTo(ESCAPED_HASH));
	}

	@Test
	public void generateWebdocsSectionHash_WhenPredefinedSectionIsEmptyAndSectionTitleIsEmpty_ThenReturnsRandomLowercaseStringFourCharactersLong() {
		final String randomString = "RaNd";

		when(StringUtil.randomString(4)).thenReturn(randomString);

		String result = webdocsWebContentUtil.generateWebdocsSectionHash(StringPool.BLANK, StringPool.BLANK);

		assertThat(result, equalTo(randomString.toLowerCase()));
	}

	@Test
	public void generateWebdocsSectionHash_WhenPredefinedSectionIsEmptyAndSectionTitleIsNotEmpty_ThenReturnsEscapedSectionTitle() {
		String result = webdocsWebContentUtil.generateWebdocsSectionHash(StringPool.BLANK, UNESCAPED_HASH);
		assertThat(result, equalTo(ESCAPED_HASH));
	}

	@Before
	public void setUp() {
		openMocks(this);
	}
}
