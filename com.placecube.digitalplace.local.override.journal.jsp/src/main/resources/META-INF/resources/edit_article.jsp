<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-util:include 
    page="/edit_article.original.jsp" 
    servletContext="<%= application %>" 
/>

<liferay-util:dynamic-include key="/edit_article.jsp#post" />