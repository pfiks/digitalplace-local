package com.placecube.digitalplace.local.webcontent.formerrorpayment.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.formerrorpayment.constants.FormErrorPaymentConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class FormErrorPaymentWebContentServiceTest extends PowerMockito {

	private FormErrorPaymentWebContentService formErrorPaymentWebContentService;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private DefaultDDMStructureHelper mockDefaultDDMStructureHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, FormErrorPaymentConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		formErrorPaymentWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenAddsDDMStructures() throws Exception {
		long groupId = 123;
		long classNameId = 456;
		long userId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);

		formErrorPaymentWebContentService.getOrCreateDDMStructure(mockServiceContext);

		verify(mockDefaultDDMStructureHelper, times(1)).addDDMStructures(userId, groupId, classNameId, getClass().getClassLoader(),
				"com/placecube/digitalplace/local/webcontent/formerrorpayment/dependencies/ddm/structure.xml", mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheFormConfirmationStructure() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, FormErrorPaymentConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure result = formErrorPaymentWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);

		formErrorPaymentWebContentService = new FormErrorPaymentWebContentService();
		formErrorPaymentWebContentService.ddmStructureLocalService = mockDDMStructureLocalService;
		formErrorPaymentWebContentService.defaultDDMStructureHelper = mockDefaultDDMStructureHelper;
		formErrorPaymentWebContentService.ddmTemplateLocalService = mockDDMTemplateLocalService;
		formErrorPaymentWebContentService.portal = mockPortal;
	}

}
