<#assign httpServletRequest = digitalplace_serviceContext.getRequest()/>

<#assign session_redirect = httpServletRequest.getSession().getAttribute("equinox.http.defaultredirect")!""/>

<#assign bodyText = Body.getData()?replace("$REDIRECT_URL$", session_redirect)/>

${ Heading.getData()!"" }

${bodyText}
