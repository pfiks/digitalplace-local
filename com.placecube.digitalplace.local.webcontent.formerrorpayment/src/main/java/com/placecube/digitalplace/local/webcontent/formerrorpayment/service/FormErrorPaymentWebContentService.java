package com.placecube.digitalplace.local.webcontent.formerrorpayment.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.webcontent.formerrorpayment.constants.FormErrorPaymentConstants;

@Component(immediate = true, service = FormErrorPaymentWebContentService.class)
public class FormErrorPaymentWebContentService {

	@Reference
	DDMStructureLocalService ddmStructureLocalService;

	@Reference
	DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	Portal portal;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		try {
			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
					"com/placecube/digitalplace/local/webcontent/formerrorpayment/dependencies/ddm/structure.xml", serviceContext);

			return ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), FormErrorPaymentConstants.STRUCTURE_KEY);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}
}
