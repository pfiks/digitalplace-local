package com.placecube.digitalplace.local.webcontent.formerrorpayment.constants;

public enum FormErrorPaymentDDMTemplate {

	FORM_CONFIRMATION("FORM-ERROR-PAYMENT", "Form Error Payment");

	private final String key;
	private final String name;

	private FormErrorPaymentDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
