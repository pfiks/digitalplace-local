package com.placecube.digitalplace.local.webcontent.formerrorpayment.constants;

public final class FormErrorPaymentConstants {

	public static final String STRUCTURE_KEY = "FORM ERROR PAYMENT";

	private FormErrorPaymentConstants() {

	}

}
