package com.placecube.digitalplace.local.webcontent.formerrorpayment.upgrade.upgrade_1_0_0;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.Disjunction;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.webcontent.formerrorpayment.constants.FormErrorPaymentDDMTemplate;

public class FormErrorPaymentWebContentTemplateStaticUtilUpgrade extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;
	private final DDMTemplateLocalService ddmTemplateLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(FormErrorPaymentWebContentTemplateStaticUtilUpgrade.class);

	public FormErrorPaymentWebContentTemplateStaticUtilUpgrade(DDMTemplateLocalService ddmTemplateLocalService, CompanyLocalService companyLocalService) {
		this.companyLocalService = companyLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId != 0) {
			upgradeWidgetTemplate(companyId, FormErrorPaymentDDMTemplate.FORM_CONFIRMATION);
		} else {
			for (Company company : companyLocalService.getCompanies()) {
				upgradeWidgetTemplate(company.getCompanyId(), FormErrorPaymentDDMTemplate.FORM_CONFIRMATION);
			}
		}
	}

	private void upgradeWidgetTemplate(long companyId, FormErrorPaymentDDMTemplate template) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();

		Disjunction disjunction = RestrictionsFactoryUtil.disjunction();
		disjunction.add(RestrictionsFactoryUtil.ilike("templateKey", template.getKey()));
		disjunction.add(RestrictionsFactoryUtil.ilike("templateKey", template.getKey().replace(StringPool.DASH, StringPool.SPACE)));

		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(disjunction);

		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		if (ddmTemplates.isEmpty()) {
			LOG.warn("Template " + template.getKey() + " not found in company " + companyId);
		} else {
			ddmTemplates.forEach(this::updateTemplate);
		}

	}

	private void updateTemplate(DDMTemplate ddmTemplate) {
		String newScript = getAmendedScript(ddmTemplate.getScript());
		ddmTemplate.setScript(newScript);
		ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
	}

	private String getAmendedScript(String script) {
		return script.replace("<#assign serviceContext = staticUtil[\"com.liferay.portal.kernel.service.ServiceContextThreadLocal\"].getServiceContext()>", StringPool.BLANK)
				.replaceAll("\\bserviceContext\\b", "digitalplace_serviceContext");
	}

}