package com.placecube.digitalplace.local.webcontent.formerrorpayment.upgrade;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.webcontent.formerrorpayment.upgrade.upgrade_1_0_0.FormErrorPaymentWebContentTemplateStaticUtilUpgrade;

import java.rmi.registry.Registry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class FormErrorPaymentUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new FormErrorPaymentWebContentTemplateStaticUtilUpgrade(ddmTemplateLocalService, companyLocalService));
	}
}