<%@page import="com.placecube.digitalplace.local.webcontent.review.web.constants.PortletRequestKeys"%>
<%@page import="com.placecube.digitalplace.local.webcontent.review.web.constants.MVCCommandKeys"%>
<%@ include file="init.jsp"%>

<c:if test="${showContent }">

	<liferay-ui:error key="review-confirmation-failed" message="review-confirmation-failed" />

	<liferay-portlet:actionURL var="completeReviewUrl" name="<%=MVCCommandKeys.COMPLETE_REVIEW %>">
		<liferay-portlet:param name="<%=PortletRequestKeys.JOURNAL_ARTICLE_ID %>" value="${journalArticleId }" />
	</liferay-portlet:actionURL>

	<div class="alert-notifications alert-notifications-fixed">
		<div class="alert alert-dismissible alert-warning alert-notification w-auto" role="alert">
			<aui:form action="${completeReviewUrl}" name="<portlet:namespace />fm" cssClass="review-form">
				<i class="icon-warning-sign"></i>
				<strong class="lead"><liferay-ui:message key="warning" />:</strong>
				<liferay-ui:message key="content-needs-to-be-reviewed" />
				
				<aui:button cssClass="ml-3" type="submit" value="review-complete" />
				
			</aui:form>
		</div>
	</div>
</c:if>