package com.placecube.digitalplace.local.webcontent.review.web.service;

import java.util.Date;
import java.util.Optional;

import org.joda.time.DateTime;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.review.web.configuration.WebContentReviewConfiguration;

@Component(immediate = true, service = WebContentReviewService.class)
public class WebContentReviewService {

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	private static final Log LOG = LogFactoryUtil.getLog(WebContentReviewService.class);

	public boolean articleNeedsToBeReviewed(long companyId, JournalArticle journalArticle) throws PortalException {
		if (articleHasReviewStructure(companyId, journalArticle) && journalArticle.getReviewDate() != null) {
			return DateUtil.compareTo(DateTime.now().toDate(), journalArticle.getReviewDate()) > 0;
		}
		return false;
	}

	public void updateReviewDate(long companyId, long journalArticleId) throws PortalException {
		JournalArticle journalArticle = journalArticleLocalService.getArticle(journalArticleId);
		setNewReviewDateFromConfiguredDelta(companyId, journalArticle);
		journalArticleLocalService.updateJournalArticle(journalArticle);
	}

	public void setNewReviewDateFromConfiguredDelta(long companyId, JournalArticle journalArticle) throws PortalException {
		WebContentReviewConfiguration configuration = configurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, companyId);

		Date reviewDate = DateTime.now().plusMonths(configuration.deltaReviewMonths()).toDate();

		journalArticle.setReviewDate(reviewDate);
	}

	public Optional<JournalArticle> getJournalArticleFromUrl(long groupId, String currentUrl) {
		try {
			String[] friendlyUrl = StringUtil.split(HttpComponentsUtil.decodeURL(currentUrl), "?");
			String[] urlString = friendlyUrl[0].split("/");
			String urlTitle = urlString[urlString.length - 1];

			JournalArticle journalArticle = journalArticleLocalService.getArticleByUrlTitle(groupId, urlTitle);

			return Optional.of(journalArticle);
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to retrieve article from currentUrl: " + e.getMessage());
			return Optional.empty();
		}
	}

	public boolean userHasUpdateArticlePermission(long companyId, long groupId, JournalArticle journalArticle, PermissionChecker permissionChecker) {
		return permissionChecker.isOmniadmin() || permissionChecker.isCompanyAdmin() || permissionChecker.isContentReviewer(companyId, groupId)
				|| permissionChecker.hasPermission(groupId, JournalArticle.class.getName(), journalArticle.getResourcePrimKey(), ActionKeys.UPDATE);
	}

	public boolean articleHasReviewStructure(long companyId, JournalArticle journalArticle) throws PortalException {
		WebContentReviewConfiguration configuration = configurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, companyId);

		String[] structuresToReview = configuration.ddmStructuresToReview();

		return ArrayUtil.contains(structuresToReview, journalArticle.getDDMStructureKey());
	}
}
