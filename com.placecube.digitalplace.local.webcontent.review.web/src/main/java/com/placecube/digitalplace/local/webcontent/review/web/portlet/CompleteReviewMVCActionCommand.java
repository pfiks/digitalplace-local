package com.placecube.digitalplace.local.webcontent.review.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.local.webcontent.review.web.constants.ErrorKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.PortletKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.webcontent.review.web.service.WebContentReviewService;

@Component(property = { "javax.portlet.name=" + PortletKeys.WEBCONTENT_REVIEW, "mvc.command.name=" + MVCCommandKeys.COMPLETE_REVIEW }, service = MVCActionCommand.class)
public class CompleteReviewMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(CompleteReviewMVCActionCommand.class);

	@Reference
	private WebContentReviewService webContentReviewService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try {
			long journalArticleId = ParamUtil.getLong(actionRequest, PortletRequestKeys.JOURNAL_ARTICLE_ID);

			webContentReviewService.updateReviewDate(PortalUtil.getCompanyId(actionRequest), journalArticleId);
		} catch (PortalException e) {
			LOG.error(e);
			SessionErrors.add(actionRequest, ErrorKeys.REVIEW_CONFIRMATION_FAILED);
		}
	}

}