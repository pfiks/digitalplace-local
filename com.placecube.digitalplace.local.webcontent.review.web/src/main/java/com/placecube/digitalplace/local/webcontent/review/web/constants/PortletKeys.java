package com.placecube.digitalplace.local.webcontent.review.web.constants;

/**
 * @author ruben.lopezseoane
 */
public class PortletKeys {

	public static final String WEBCONTENT_REVIEW = "com_placecube_digitalplace_local_webcontent_review_portlet_WebContentReviewPortlet";

}