package com.placecube.digitalplace.local.webcontent.review.web.constants;

public class PortletRequestKeys {

	public static final String SHOW_CONTENT = "showContent";

	public static final String JOURNAL_ARTICLE_ID = "journalArticleId";

	private PortletRequestKeys() {

	}
}
