package com.placecube.digitalplace.local.webcontent.review.web.constants;

public class ErrorKeys {

	public static final String REVIEW_CONFIRMATION_FAILED = "review-confirmation-failed";

	private ErrorKeys() {

	}
}
