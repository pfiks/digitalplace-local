package com.placecube.digitalplace.local.webcontent.review.web.constants;

public class MVCCommandKeys {

	public static final String COMPLETE_REVIEW = "/webcontent-review/complete-review";

	private MVCCommandKeys() {

	}
}
