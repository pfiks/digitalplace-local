package com.placecube.digitalplace.local.webcontent.review.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "web-content", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.webcontent.review.web.configuration.WebContentReviewConfiguration", localization = "content/Language", name = "webcontent-review")
public interface WebContentReviewConfiguration {

	@Meta.AD(required = false, deflt = "", name = "ddm-structures-to-review") String[] ddmStructuresToReview();

	@Meta.AD(required = false, deflt = "6", name = "delta-review-months")
	int deltaReviewMonths();
}
