package com.placecube.digitalplace.local.webcontent.review.web.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.PortletKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.webcontent.review.web.service.WebContentReviewService;

/**
 * @author ruben.lopezseoane
 */
@Component(immediate = true, property = { //
		"com.liferay.portlet.css-class-wrapper=webcontent-review", //
		"com.liferay.portlet.display-category=category.hidden", //
		"com.liferay.portlet.instanceable=false", //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.init-param.view-template=/webcontent-review/view.jsp", //
		"javax.portlet.name=" + PortletKeys.WEBCONTENT_REVIEW, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user"//
}, service = Portlet.class)
public class WebContentReviewPortlet extends MVCPortlet {

	private static final Log LOG = LogFactoryUtil.getLog(WebContentReviewPortlet.class);

	@Reference
	private WebContentReviewService webContentReviewService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			if (themeDisplay.getLayout().getType().equals(LayoutConstants.TYPE_ASSET_DISPLAY)) {
				Optional<JournalArticle> optionalJournalArticle = webContentReviewService.getJournalArticleFromUrl(themeDisplay.getScopeGroupId(), themeDisplay.getURLCurrent());

				if (optionalJournalArticle.isPresent()) {

					JournalArticle journalArticle = optionalJournalArticle.get();

					if (webContentReviewService.articleNeedsToBeReviewed(themeDisplay.getCompanyId(), journalArticle) && webContentReviewService
							.userHasUpdateArticlePermission(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), journalArticle, themeDisplay.getPermissionChecker())) {

						renderRequest.setAttribute(PortletRequestKeys.SHOW_CONTENT, Boolean.TRUE);
						renderRequest.setAttribute(PortletRequestKeys.JOURNAL_ARTICLE_ID, journalArticle.getPrimaryKey());
					}

				}
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		super.render(renderRequest, renderResponse);
	}

}