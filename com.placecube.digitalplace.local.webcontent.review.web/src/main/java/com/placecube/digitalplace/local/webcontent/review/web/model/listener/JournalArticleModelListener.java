package com.placecube.digitalplace.local.webcontent.review.web.model.listener;

import com.liferay.document.library.kernel.model.DLFileEntryConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.placecube.digitalplace.local.webcontent.review.web.service.WebContentReviewService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = ModelListener.class)
public class JournalArticleModelListener extends BaseModelListener<JournalArticle> {

	@Reference
	private WebContentReviewService webContentReviewService;

	@Override
	public void onBeforeCreate(JournalArticle journalArticle) throws ModelListenerException {
		setArticleReviewDateFromConfiguredDelta(journalArticle);
	}

	private void setArticleReviewDateFromConfiguredDelta(JournalArticle journalArticle) {
		try {
			String versionString = String.valueOf(journalArticle.getVersion());
			if (webContentReviewService.articleHasReviewStructure(journalArticle.getCompanyId(), journalArticle) && versionString.equals(DLFileEntryConstants.VERSION_DEFAULT) && journalArticle.getReviewDate() == null) {
				webContentReviewService.setNewReviewDateFromConfiguredDelta(journalArticle.getCompanyId(), journalArticle);
			}
		} catch (Exception e) {
			throw new ModelListenerException(e);
		}
	}

}