package com.placecube.digitalplace.local.webcontent.review.web.portlet;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.webcontent.review.web.constants.ErrorKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.webcontent.review.web.service.WebContentReviewService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class })
public class CompleteReviewMVCActionCommandTest {

	private static final long JOURNAL_ARTICLE_ID = 23444L;
	private static final long COMPANY_ID = 3434L;
	private static final long GROUP_ID = 1239l;

	@InjectMocks
	private CompleteReviewMVCActionCommand completeReviewMVCActionCommand;

	@Mock
	private WebContentReviewService mockWebContentReviewService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class);
	}

	@Test
	public void doProcessAction_WhenNoErrors_ThenUpdatesTheArticleReviewDate() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.JOURNAL_ARTICLE_ID)).thenReturn(JOURNAL_ARTICLE_ID);
		when(PortalUtil.getCompanyId(mockActionRequest)).thenReturn(COMPANY_ID);
		when(PortalUtil.getScopeGroupId(mockActionRequest)).thenReturn(GROUP_ID);

		completeReviewMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockWebContentReviewService, times(1)).updateReviewDate(COMPANY_ID, JOURNAL_ARTICLE_ID);
	}

	@Test
	public void doProcessAction_WhenReviewDateUpdateFails_ThenAddsReviewConfirmationFailedMessage() throws Exception {
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.JOURNAL_ARTICLE_ID)).thenReturn(JOURNAL_ARTICLE_ID);
		when(PortalUtil.getCompanyId(mockActionRequest)).thenReturn(COMPANY_ID);
		when(PortalUtil.getScopeGroupId(mockActionRequest)).thenReturn(GROUP_ID);
		doThrow(new PortalException()).when(mockWebContentReviewService).updateReviewDate(COMPANY_ID, JOURNAL_ARTICLE_ID);

		completeReviewMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, ErrorKeys.REVIEW_CONFIRMATION_FAILED);
	}
}
