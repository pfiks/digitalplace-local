package com.placecube.digitalplace.local.webcontent.review.web.model.listener;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.local.webcontent.review.web.service.WebContentReviewService;

public class JournalArticleModelListenerTest extends PowerMockito {

	private static final long COMPANY_ID = 3434L;
	private static final double VERSION_ONE = 1.0;

	@InjectMocks
	private JournalArticleModelListener journalArticleModelListener;

	@Mock
	private WebContentReviewService mockWebContentReviewService;

	@Mock
	private JournalArticle mockJournalArticle;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void onBeforeCreate_WhenJournalArticleHasReviewStructure_AndIsVersionOne_AndReviewDateNull_ThenSetsReviewDateFromConfiguration() throws Exception {
		when(mockWebContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockJournalArticle.getVersion()).thenReturn(VERSION_ONE);
		when(mockJournalArticle.getReviewDate()).thenReturn(null);
		when(mockJournalArticle.getCompanyId()).thenReturn(COMPANY_ID);

		journalArticleModelListener.onBeforeCreate(mockJournalArticle);

		verify(mockWebContentReviewService, times(1)).setNewReviewDateFromConfiguredDelta(COMPANY_ID, mockJournalArticle);
	}

	@Test
	public void onBeforeCreate_WhenJournalArticleHasReviewStructure_AndIsVersionOne_AndReviewDateNotNull_ThenDoesNotSetReviewDateFromConfiguration() throws Exception {
		when(mockWebContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockJournalArticle.getVersion()).thenReturn(VERSION_ONE);
		when(mockJournalArticle.getReviewDate()).thenReturn(new Date());
		when(mockJournalArticle.getCompanyId()).thenReturn(COMPANY_ID);

		journalArticleModelListener.onBeforeCreate(mockJournalArticle);

		verify(mockWebContentReviewService, never()).setNewReviewDateFromConfiguredDelta(anyLong(), any(JournalArticle.class));
	}

	@Test
	public void onBeforeCreate_WhenJournalArticleHasReviewStructure_AndNotVersionOne_AndReviewDateNull_ThenDoesNotSetReviewDateFromConfiguration() throws Exception {
		when(mockWebContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockJournalArticle.getVersion()).thenReturn(1.1);
		when(mockJournalArticle.getReviewDate()).thenReturn(null);
		when(mockJournalArticle.getCompanyId()).thenReturn(COMPANY_ID);

		journalArticleModelListener.onBeforeCreate(mockJournalArticle);

		verify(mockWebContentReviewService, never()).setNewReviewDateFromConfiguredDelta(anyLong(), any(JournalArticle.class));
	}

	@Test
	public void onBeforeCreate_WhenJournalArticleHasReviewStructure_AndNotVersionOne_AndReviewDateNotNull_ThenDoesNotSetReviewDateFromConfiguration() throws Exception {
		when(mockWebContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockJournalArticle.getVersion()).thenReturn(1.1);
		when(mockJournalArticle.getReviewDate()).thenReturn(new Date());
		when(mockJournalArticle.getCompanyId()).thenReturn(COMPANY_ID);

		journalArticleModelListener.onBeforeCreate(mockJournalArticle);

		verify(mockWebContentReviewService, never()).setNewReviewDateFromConfiguredDelta(anyLong(), any(JournalArticle.class));
	}

	@Test
	public void onBeforeCreate_WhenJournalArticleDoesNotHaveReviewStructure_AndIsVersionOne_AndReviewDateNull_ThenDoesNotSetReviewDateFromConfiguration() throws Exception {
		when(mockWebContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle)).thenReturn(false);
		when(mockJournalArticle.getVersion()).thenReturn(VERSION_ONE);
		when(mockJournalArticle.getReviewDate()).thenReturn(null);
		when(mockJournalArticle.getCompanyId()).thenReturn(COMPANY_ID);

		journalArticleModelListener.onBeforeCreate(mockJournalArticle);

		verify(mockWebContentReviewService, never()).setNewReviewDateFromConfiguredDelta(anyLong(), any(JournalArticle.class));
	}

	@Test(expected = ModelListenerException.class)
	public void onBeforeCreate_WhenArticleReviewDateUpdateFails_ThenThrowsModelListenerException() throws Exception {
		when(mockWebContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockJournalArticle.getVersion()).thenReturn(VERSION_ONE);
		when(mockJournalArticle.getReviewDate()).thenReturn(null);
		when(mockJournalArticle.getCompanyId()).thenReturn(COMPANY_ID);
		doThrow(new PortalException()).when(mockWebContentReviewService).setNewReviewDateFromConfiguredDelta(COMPANY_ID, mockJournalArticle);

		journalArticleModelListener.onBeforeCreate(mockJournalArticle);
	}
}
