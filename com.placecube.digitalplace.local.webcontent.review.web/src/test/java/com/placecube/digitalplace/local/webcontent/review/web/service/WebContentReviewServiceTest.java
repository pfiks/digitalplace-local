package com.placecube.digitalplace.local.webcontent.review.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Date;
import java.util.Optional;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.placecube.digitalplace.local.webcontent.review.web.configuration.WebContentReviewConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, HttpComponentsUtil.class, ArrayUtil.class })
public class WebContentReviewServiceTest {

	private static final String ARTICLE_STRUCTURE_KEY = "ARTICLE";
	private static final String CONFIGURED_STRUCTURES[] = new String[] { "GUIDE", ARTICLE_STRUCTURE_KEY };

	private static final long JOURNAL_ARTICLE_ID = 23222L;
	private static final String ARTICLE_URL_TITLE = "article-url";
	private static final long COMPANY_ID = 3434L;
	private static final long GROUP_ID = 1239l;
	private static final String CURRENT_URL = "http://portal/page";

	@InjectMocks
	private WebContentReviewService webContentReviewService;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private WebContentReviewConfiguration mockWebContentReviewConfiguration;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private Date mockReviewDate;

	private Date fixedDateTime;

	@Before
	public void setUp() {
		fixedDateTime = new Date();
		DateTimeUtils.setCurrentMillisFixed(fixedDateTime.getTime());

		mockStatic(DateUtil.class, HttpComponentsUtil.class, ArrayUtil.class);
	}

	@Test
	public void articleNeedsToBeReviewed_WhenArticleDoesNotHaveConfiguredReviewStructure_ThenReturnsFalse() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(false);

		assertThat(webContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle), equalTo(false));
	}

	@Test(expected = PortalException.class)
	public void articleNeedsToBeReviewed_WhenConfiguredStructuresRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		webContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle);
	}

	@Test
	public void articleNeedsToBeReviewed_WhenArticleHasConfiguredReviewStructure_AndCurrentDateIsAfterReviewDate_ThenReturnsTrue() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(true);
		when(mockJournalArticle.getReviewDate()).thenReturn(mockReviewDate);
		when(DateUtil.compareTo(fixedDateTime, mockReviewDate)).thenReturn(1);

		assertThat(webContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle), equalTo(true));
	}

	@Test
	public void articleNeedsToBeReviewed_WhenArticleHasConfiguredReviewStructure_AndCurrentDateIsEqualsToReviewDate_ThenReturnsFalse() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(true);
		when(mockJournalArticle.getReviewDate()).thenReturn(mockReviewDate);
		when(DateUtil.compareTo(fixedDateTime, mockReviewDate)).thenReturn(0);

		assertThat(webContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle), equalTo(false));
	}

	@Test
	public void articleNeedsToBeReviewed_WhenArticleHasConfiguredReviewStructure_AndCurrentDateIsBeforeReviewDate_ThenReturnsFalse() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(true);
		when(mockJournalArticle.getReviewDate()).thenReturn(mockReviewDate);
		when(DateUtil.compareTo(fixedDateTime, mockReviewDate)).thenReturn(-1);

		assertThat(webContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle), equalTo(false));
	}

	@Test
	public void articleNeedsToBeReviewed_WhenArticleHasConfiguredReviewStructure_AndReviewDateIsNull_ThenReturnsFalse() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(true);
		when(mockJournalArticle.getReviewDate()).thenReturn(null);

		assertThat(webContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle), equalTo(false));
	}

	@Test
	public void updateReviewDate_WhenNoErrors_ThenSetsArticleReviewDateToCurrentDatePlusConfiguredDelta_AndUpdatesArticleInOrder() throws Exception {
		final int deltaMonths = 6;

		DateTime newReviewDate = new DateTime(fixedDateTime).plusMonths(deltaMonths);

		when(mockJournalArticleLocalService.getArticle(JOURNAL_ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenReturn(mockWebContentReviewConfiguration);
		when(mockWebContentReviewConfiguration.deltaReviewMonths()).thenReturn(deltaMonths);

		webContentReviewService.updateReviewDate(COMPANY_ID, JOURNAL_ARTICLE_ID);

		InOrder inOrder = inOrder(mockJournalArticle, mockJournalArticleLocalService);

		inOrder.verify(mockJournalArticle, times(1)).setReviewDate(newReviewDate.toDate());
		inOrder.verify(mockJournalArticleLocalService, times(1)).updateJournalArticle(mockJournalArticle);
	}

	@Test(expected = PortalException.class)
	public void updateReviewDate_WhenConfiguredDeltaRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockJournalArticleLocalService.getArticle(JOURNAL_ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		webContentReviewService.updateReviewDate(COMPANY_ID, JOURNAL_ARTICLE_ID);
	}

	@Test(expected = PortalException.class)
	public void updateReviewDate_WhenJournalArticleRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockJournalArticleLocalService.getArticle(JOURNAL_ARTICLE_ID)).thenThrow(new PortalException());

		webContentReviewService.updateReviewDate(COMPANY_ID, JOURNAL_ARTICLE_ID);
	}

	@Test
	public void setNewReviewDateFromConfiguredDelta_WhenNoErrors_ThenSetsArticleReviewDateToCurrentDatePlusConfiguredDelta() throws Exception {
		final int deltaMonths = 6;

		DateTime newReviewDate = new DateTime(fixedDateTime).plusMonths(deltaMonths);

		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenReturn(mockWebContentReviewConfiguration);
		when(mockWebContentReviewConfiguration.deltaReviewMonths()).thenReturn(deltaMonths);

		webContentReviewService.setNewReviewDateFromConfiguredDelta(COMPANY_ID, mockJournalArticle);

		verify(mockJournalArticle, times(1)).setReviewDate(newReviewDate.toDate());
	}

	@Test(expected = PortalException.class)
	public void setNewReviewDateFromConfiguredDelta_WhenConfiguredDeltaRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		webContentReviewService.setNewReviewDateFromConfiguredDelta(COMPANY_ID, mockJournalArticle);
	}

	@Test
	public void getJournalArticleFromUrl_WhenArticleForCurrentArticleUrlExists_ThenReturnsOptionalOfJournalArticle() throws Exception {
		final String decodedUrl = "http:://page/" + ARTICLE_URL_TITLE + "?param=paramValue";

		when(HttpComponentsUtil.decodeURL(CURRENT_URL)).thenReturn(decodedUrl);
		when(mockJournalArticleLocalService.getArticleByUrlTitle(GROUP_ID, ARTICLE_URL_TITLE)).thenReturn(mockJournalArticle);

		Optional<JournalArticle> result = webContentReviewService.getJournalArticleFromUrl(GROUP_ID, CURRENT_URL);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockJournalArticle));
	}

	@Test
	public void getJournalArticleFromUrl_WhenArticleForCurrentArticleUrlDoesNotExist_ThenReturnsEmptyOptional() throws Exception {
		final String decodedUrl = "http:://page/" + ARTICLE_URL_TITLE + "?param=paramValue";

		when(HttpComponentsUtil.decodeURL(CURRENT_URL)).thenReturn(decodedUrl);
		when(mockJournalArticleLocalService.getArticleByUrlTitle(GROUP_ID, ARTICLE_URL_TITLE)).thenThrow(new PortalException());

		Optional<JournalArticle> result = webContentReviewService.getJournalArticleFromUrl(GROUP_ID, CURRENT_URL);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void userHasUpdateArticlePermission_WhenUserIsOmniAdmin_ThenReturnsTrue() {
		when(mockPermissionChecker.isOmniadmin()).thenReturn(true);
		assertThat(webContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker), equalTo(true));
	}

	@Test
	public void userHasUpdateArticlePermission_WhenUserIsCompanydmin_ThenReturnsTrue() {
		when(mockPermissionChecker.isCompanyAdmin()).thenReturn(true);
		assertThat(webContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker), equalTo(true));
	}

	@Test
	public void userHasUpdateArticlePermission_WhenUserIsContentReviewer_ThenReturnsTrue() {
		when(mockPermissionChecker.isContentReviewer(COMPANY_ID, GROUP_ID)).thenReturn(true);
		assertThat(webContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker), equalTo(true));
	}

	@Test
	public void userHasUpdateArticlePermission_WhenUserHasArticleUpdatePermission_ThenReturnsTrue() {
		final long articleResourceId = 34666L;
		when(mockJournalArticle.getResourcePrimKey()).thenReturn(articleResourceId);
		when(mockPermissionChecker.hasPermission(GROUP_ID, JournalArticle.class.getName(), articleResourceId, ActionKeys.UPDATE)).thenReturn(true);

		assertThat(webContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker), equalTo(true));
	}

	@Test
	public void userHasUpdateArticlePermission_WhenUserDoesNotHaveArticleUpdatePermission_AndUserIsNotOmniAdminCompanyAdminOrContentReviewer_ThenReturnsFalse() {
		final long articleResourceId = 34666L;
		when(mockJournalArticle.getResourcePrimKey()).thenReturn(articleResourceId);
		when(mockPermissionChecker.hasPermission(GROUP_ID, JournalArticle.class.getName(), articleResourceId, ActionKeys.UPDATE)).thenReturn(false);

		assertThat(webContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker), equalTo(false));
	}

	@Test
	public void articleHasReviewStructure_WhenArticleHasConfiguredReviewStructure_ThenReturnsTrue() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(true);

		assertThat(webContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle), equalTo(true));
	}

	@Test
	public void articleHasReviewStructure_WhenArticleDoesNotHaveConfiguredReviewStructure_ThenReturnsFalse() throws Exception {
		mockArticleAndConfiguredReviewStructures(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY);
		when(ArrayUtil.contains(CONFIGURED_STRUCTURES, ARTICLE_STRUCTURE_KEY)).thenReturn(false);

		assertThat(webContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle), equalTo(false));
	}

	@Test(expected = PortalException.class)
	public void articleHasReviewStructure_WhenConfiguredStructuresRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		webContentReviewService.articleHasReviewStructure(COMPANY_ID, mockJournalArticle);
	}

	private void mockArticleAndConfiguredReviewStructures(String configuredStructures[], String articleStructure) throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentReviewConfiguration.class, COMPANY_ID)).thenReturn(mockWebContentReviewConfiguration);
		when(mockWebContentReviewConfiguration.ddmStructuresToReview()).thenReturn(configuredStructures);
		when(mockJournalArticle.getDDMStructureKey()).thenReturn(articleStructure);
	}

}
