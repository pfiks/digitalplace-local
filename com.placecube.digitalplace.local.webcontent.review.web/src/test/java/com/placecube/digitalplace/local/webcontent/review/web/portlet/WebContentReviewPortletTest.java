package com.placecube.digitalplace.local.webcontent.review.web.portlet;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.webcontent.review.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.webcontent.review.web.service.WebContentReviewService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class WebContentReviewPortletTest {

	private static final long JOURNAL_ARTICLE_ID = 6655l;
	private static final long COMPANY_ID = 3434L;
	private static final long GROUP_ID = 1239l;
	private static final String CURRENT_URL = "http://portal/page";

	@InjectMocks
	private WebContentReviewPortlet webContentReviewPortlet;

	@Mock
	private WebContentReviewService mockWebContentReviewService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private Layout mockLayout;

	@Before
	public void setUp() throws Exception {
		mockCallToSuper();
	}

	@Test
	public void render_WhenUrlArticleExists_AndArticleNeedsToBeReviewed_AndUserHasUpdatePermissions_ThenSetsShowContentAndArticleIdAttributes() throws Exception {
		mockRequest();
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);
		when(mockWebContentReviewService.getJournalArticleFromUrl(GROUP_ID, CURRENT_URL)).thenReturn(Optional.of(mockJournalArticle));
		when(mockJournalArticle.getPrimaryKey()).thenReturn(JOURNAL_ARTICLE_ID);
		when(mockWebContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockWebContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker)).thenReturn(true);

		webContentReviewPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.SHOW_CONTENT, Boolean.TRUE);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_ID);
	}

	@Test
	public void render_WhenUrlArticleExists_AndArticleNeedsToBeReviewed_AndUserDoesNotHaveUpdatePermissions_ThenDoesNotSetShowContentAndArticleIdAttributes() throws Exception {
		mockRequest();
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);
		when(mockWebContentReviewService.getJournalArticleFromUrl(GROUP_ID, CURRENT_URL)).thenReturn(Optional.of(mockJournalArticle));
		when(mockJournalArticle.getPrimaryKey()).thenReturn(JOURNAL_ARTICLE_ID);
		when(mockWebContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle)).thenReturn(true);
		when(mockWebContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker)).thenReturn(false);

		webContentReviewPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.SHOW_CONTENT), anyBoolean());
		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.JOURNAL_ARTICLE_ID), anyString());
	}

	@Test
	public void render_WhenUrlArticleExists_AndArticleDoesNotNeedToBeReviewed_AndUserHasUpdatePermissions_ThenDoesNotSetShowContentAndArticleIdAttributes() throws Exception {
		mockRequest();
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);
		when(mockWebContentReviewService.getJournalArticleFromUrl(GROUP_ID, CURRENT_URL)).thenReturn(Optional.of(mockJournalArticle));
		when(mockJournalArticle.getPrimaryKey()).thenReturn(JOURNAL_ARTICLE_ID);
		when(mockWebContentReviewService.articleNeedsToBeReviewed(COMPANY_ID, mockJournalArticle)).thenReturn(false);
		when(mockWebContentReviewService.userHasUpdateArticlePermission(COMPANY_ID, GROUP_ID, mockJournalArticle, mockPermissionChecker)).thenReturn(true);

		webContentReviewPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.SHOW_CONTENT), anyBoolean());
		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.JOURNAL_ARTICLE_ID), anyString());
	}

	@Test
	public void render_WhenUrlArticleDoesNotExist_ThenDoesNotSetShowContentAndArticleIdAttributes() throws Exception {
		mockRequest();
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);
		when(mockWebContentReviewService.getJournalArticleFromUrl(GROUP_ID, CURRENT_URL)).thenReturn(Optional.empty());

		webContentReviewPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.SHOW_CONTENT), anyBoolean());
		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.JOURNAL_ARTICLE_ID), anyString());
	}

	@Test
	public void render_WhenLayoutTypeNotAssetDisplay_ThenWebContentReviewServiceNotCalled() throws Exception{
		mockRequest();
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getType()).thenReturn("Random Type");

		webContentReviewPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockWebContentReviewService, never()).getJournalArticleFromUrl(anyLong(), anyString());

	}

	private void mockRequest() {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getURLCurrent()).thenReturn(CURRENT_URL);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] arguments = new Class[2];
		arguments[0] = RenderRequest.class;
		arguments[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", arguments);
		MemberModifier.suppress(superRenderMethod);
	}
}
