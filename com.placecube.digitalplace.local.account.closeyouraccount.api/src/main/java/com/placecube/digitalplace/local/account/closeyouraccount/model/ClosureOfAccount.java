/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the ClosureOfAccount service. Represents a row in the &quot;Placecube_Account_Closure_ClosureOfAccount&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccountModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountImpl"
)
@ProviderType
public interface ClosureOfAccount
	extends ClosureOfAccountModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ClosureOfAccount, Long>
		CLOSURE_OF_ACCOUNT_ID_ACCESSOR =
			new Accessor<ClosureOfAccount, Long>() {

				@Override
				public Long get(ClosureOfAccount closureOfAccount) {
					return closureOfAccount.getClosureOfAccountId();
				}

				@Override
				public Class<Long> getAttributeClass() {
					return Long.class;
				}

				@Override
				public Class<ClosureOfAccount> getTypeClass() {
					return ClosureOfAccount.class;
				}

			};

}