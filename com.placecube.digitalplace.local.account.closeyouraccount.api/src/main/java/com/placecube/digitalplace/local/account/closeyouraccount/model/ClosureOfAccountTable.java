/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Account_Closure_ClosureOfAccount&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccount
 * @generated
 */
public class ClosureOfAccountTable extends BaseTable<ClosureOfAccountTable> {

	public static final ClosureOfAccountTable INSTANCE =
		new ClosureOfAccountTable();

	public final Column<ClosureOfAccountTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Long> closureOfAccountId =
		createColumn(
			"closureOfAccountId", Long.class, Types.BIGINT,
			Column.FLAG_PRIMARY);
	public final Column<ClosureOfAccountTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Date> modifiedDate =
		createColumn(
			"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Long> formInstanceRecordId =
		createColumn(
			"formInstanceRecordId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Date> closureDate = createColumn(
		"closureDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<ClosureOfAccountTable, Integer> status = createColumn(
		"status", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);

	private ClosureOfAccountTable() {
		super(
			"Placecube_Account_Closure_ClosureOfAccount",
			ClosureOfAccountTable::new);
	}

}