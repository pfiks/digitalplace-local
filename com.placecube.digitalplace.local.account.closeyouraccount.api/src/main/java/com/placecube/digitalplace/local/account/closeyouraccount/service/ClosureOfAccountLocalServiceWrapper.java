/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link ClosureOfAccountLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccountLocalService
 * @generated
 */
public class ClosureOfAccountLocalServiceWrapper
	implements ClosureOfAccountLocalService,
			   ServiceWrapper<ClosureOfAccountLocalService> {

	public ClosureOfAccountLocalServiceWrapper() {
		this(null);
	}

	public ClosureOfAccountLocalServiceWrapper(
		ClosureOfAccountLocalService closureOfAccountLocalService) {

		_closureOfAccountLocalService = closureOfAccountLocalService;
	}

	/**
	 * Adds the closure of account to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was added
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount addClosureOfAccount(
			com.placecube.digitalplace.local.account.closeyouraccount.model.
				ClosureOfAccount closureOfAccount) {

		return _closureOfAccountLocalService.addClosureOfAccount(
			closureOfAccount);
	}

	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount addClosureOfAccount(
				long groupId, long companyId, long userId,
				long formInstanceRecordId, java.util.Date closureDate)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.addClosureOfAccount(
			groupId, companyId, userId, formInstanceRecordId, closureDate);
	}

	/**
	 * Creates a new closure of account with the primary key. Does not add the closure of account to the database.
	 *
	 * @param closureOfAccountId the primary key for the new closure of account
	 * @return the new closure of account
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount createClosureOfAccount(long closureOfAccountId) {

		return _closureOfAccountLocalService.createClosureOfAccount(
			closureOfAccountId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the closure of account from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was removed
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount deleteClosureOfAccount(
			com.placecube.digitalplace.local.account.closeyouraccount.model.
				ClosureOfAccount closureOfAccount) {

		return _closureOfAccountLocalService.deleteClosureOfAccount(
			closureOfAccount);
	}

	/**
	 * Deletes the closure of account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account that was removed
	 * @throws PortalException if a closure of account with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount deleteClosureOfAccount(long closureOfAccountId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.deleteClosureOfAccount(
			closureOfAccountId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _closureOfAccountLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _closureOfAccountLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _closureOfAccountLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _closureOfAccountLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _closureOfAccountLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _closureOfAccountLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _closureOfAccountLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _closureOfAccountLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount fetchClosureOfAccount(long closureOfAccountId) {

		return _closureOfAccountLocalService.fetchClosureOfAccount(
			closureOfAccountId);
	}

	/**
	 * Returns the closure of account matching the UUID and group.
	 *
	 * @param uuid the closure of account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount fetchClosureOfAccountByUuidAndGroupId(
			String uuid, long groupId) {

		return _closureOfAccountLocalService.
			fetchClosureOfAccountByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.closeyouraccount.model.
			ClosureOfAccount> fetchClosureOfAccountsByCompanyId(
				long companyId) {

		return _closureOfAccountLocalService.fetchClosureOfAccountsByCompanyId(
			companyId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.closeyouraccount.model.
			ClosureOfAccount> findByClosureDateAndCompanyId(long companyId) {

		return _closureOfAccountLocalService.findByClosureDateAndCompanyId(
			companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _closureOfAccountLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the closure of account with the primary key.
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account
	 * @throws PortalException if a closure of account with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount getClosureOfAccount(long closureOfAccountId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.getClosureOfAccount(
			closureOfAccountId);
	}

	/**
	 * Returns the closure of account matching the UUID and group.
	 *
	 * @param uuid the closure of account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching closure of account
	 * @throws PortalException if a matching closure of account could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount getClosureOfAccountByUuidAndGroupId(
				String uuid, long groupId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.
			getClosureOfAccountByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the closure of accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of closure of accounts
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.closeyouraccount.model.
			ClosureOfAccount> getClosureOfAccounts(int start, int end) {

		return _closureOfAccountLocalService.getClosureOfAccounts(start, end);
	}

	/**
	 * Returns all the closure of accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the closure of accounts
	 * @param companyId the primary key of the company
	 * @return the matching closure of accounts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.closeyouraccount.model.
			ClosureOfAccount> getClosureOfAccountsByUuidAndCompanyId(
				String uuid, long companyId) {

		return _closureOfAccountLocalService.
			getClosureOfAccountsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of closure of accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the closure of accounts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching closure of accounts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.closeyouraccount.model.
			ClosureOfAccount> getClosureOfAccountsByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.local.account.closeyouraccount.
						model.ClosureOfAccount> orderByComparator) {

		return _closureOfAccountLocalService.
			getClosureOfAccountsByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of closure of accounts.
	 *
	 * @return the number of closure of accounts
	 */
	@Override
	public int getClosureOfAccountsCount() {
		return _closureOfAccountLocalService.getClosureOfAccountsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _closureOfAccountLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _closureOfAccountLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _closureOfAccountLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _closureOfAccountLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the closure of account in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was updated
	 */
	@Override
	public com.placecube.digitalplace.local.account.closeyouraccount.model.
		ClosureOfAccount updateClosureOfAccount(
			com.placecube.digitalplace.local.account.closeyouraccount.model.
				ClosureOfAccount closureOfAccount) {

		return _closureOfAccountLocalService.updateClosureOfAccount(
			closureOfAccount);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _closureOfAccountLocalService.getBasePersistence();
	}

	@Override
	public ClosureOfAccountLocalService getWrappedService() {
		return _closureOfAccountLocalService;
	}

	@Override
	public void setWrappedService(
		ClosureOfAccountLocalService closureOfAccountLocalService) {

		_closureOfAccountLocalService = closureOfAccountLocalService;
	}

	private ClosureOfAccountLocalService _closureOfAccountLocalService;

}