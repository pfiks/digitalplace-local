/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.local.account.closeyouraccount.exception.NoSuchClosureOfAccountException;
import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the closure of account service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccountUtil
 * @generated
 */
@ProviderType
public interface ClosureOfAccountPersistence
	extends BasePersistence<ClosureOfAccount> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ClosureOfAccountUtil} to access the closure of account persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the closure of accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid(String uuid);

	/**
	 * Returns a range of all the closure of accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the closure of accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the closure of accounts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first closure of account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the first closure of account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns the last closure of account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the last closure of account in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns the closure of accounts before and after the current closure of account in the ordered set where uuid = &#63;.
	 *
	 * @param closureOfAccountId the primary key of the current closure of account
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next closure of account
	 * @throws NoSuchClosureOfAccountException if a closure of account with the primary key could not be found
	 */
	public ClosureOfAccount[] findByUuid_PrevAndNext(
			long closureOfAccountId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Removes all the closure of accounts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of closure of accounts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching closure of accounts
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the closure of account where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchClosureOfAccountException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByUUID_G(String uuid, long groupId)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the closure of account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the closure of account where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the closure of account where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the closure of account that was removed
	 */
	public ClosureOfAccount removeByUUID_G(String uuid, long groupId)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the number of closure of accounts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching closure of accounts
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the closure of accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the closure of accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the closure of accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the closure of accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first closure of account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the first closure of account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns the last closure of account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the last closure of account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns the closure of accounts before and after the current closure of account in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param closureOfAccountId the primary key of the current closure of account
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next closure of account
	 * @throws NoSuchClosureOfAccountException if a closure of account with the primary key could not be found
	 */
	public ClosureOfAccount[] findByUuid_C_PrevAndNext(
			long closureOfAccountId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Removes all the closure of accounts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of closure of accounts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching closure of accounts
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the closure of accounts where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByCompanyId(long companyId);

	/**
	 * Returns a range of all the closure of accounts where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByCompanyId(
		long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the closure of accounts where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the closure of accounts where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first closure of account in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByCompanyId_First(
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the first closure of account in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns the last closure of account in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByCompanyId_Last(
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the last closure of account in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns the closure of accounts before and after the current closure of account in the ordered set where companyId = &#63;.
	 *
	 * @param closureOfAccountId the primary key of the current closure of account
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next closure of account
	 * @throws NoSuchClosureOfAccountException if a closure of account with the primary key could not be found
	 */
	public ClosureOfAccount[] findByCompanyId_PrevAndNext(
			long closureOfAccountId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
				orderByComparator)
		throws NoSuchClosureOfAccountException;

	/**
	 * Removes all the closure of accounts where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	public void removeByCompanyId(long companyId);

	/**
	 * Returns the number of closure of accounts where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching closure of accounts
	 */
	public int countByCompanyId(long companyId);

	/**
	 * Returns the closure of account where formInstanceRecordId = &#63; or throws a <code>NoSuchClosureOfAccountException</code> if it could not be found.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching closure of account
	 * @throws NoSuchClosureOfAccountException if a matching closure of account could not be found
	 */
	public ClosureOfAccount findByFormInstanceRecordId(
			long formInstanceRecordId)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the closure of account where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByFormInstanceRecordId(
		long formInstanceRecordId);

	/**
	 * Returns the closure of account where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public ClosureOfAccount fetchByFormInstanceRecordId(
		long formInstanceRecordId, boolean useFinderCache);

	/**
	 * Removes the closure of account where formInstanceRecordId = &#63; from the database.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the closure of account that was removed
	 */
	public ClosureOfAccount removeByFormInstanceRecordId(
			long formInstanceRecordId)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the number of closure of accounts where formInstanceRecordId = &#63;.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the number of matching closure of accounts
	 */
	public int countByFormInstanceRecordId(long formInstanceRecordId);

	/**
	 * Caches the closure of account in the entity cache if it is enabled.
	 *
	 * @param closureOfAccount the closure of account
	 */
	public void cacheResult(ClosureOfAccount closureOfAccount);

	/**
	 * Caches the closure of accounts in the entity cache if it is enabled.
	 *
	 * @param closureOfAccounts the closure of accounts
	 */
	public void cacheResult(java.util.List<ClosureOfAccount> closureOfAccounts);

	/**
	 * Creates a new closure of account with the primary key. Does not add the closure of account to the database.
	 *
	 * @param closureOfAccountId the primary key for the new closure of account
	 * @return the new closure of account
	 */
	public ClosureOfAccount create(long closureOfAccountId);

	/**
	 * Removes the closure of account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account that was removed
	 * @throws NoSuchClosureOfAccountException if a closure of account with the primary key could not be found
	 */
	public ClosureOfAccount remove(long closureOfAccountId)
		throws NoSuchClosureOfAccountException;

	public ClosureOfAccount updateImpl(ClosureOfAccount closureOfAccount);

	/**
	 * Returns the closure of account with the primary key or throws a <code>NoSuchClosureOfAccountException</code> if it could not be found.
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account
	 * @throws NoSuchClosureOfAccountException if a closure of account with the primary key could not be found
	 */
	public ClosureOfAccount findByPrimaryKey(long closureOfAccountId)
		throws NoSuchClosureOfAccountException;

	/**
	 * Returns the closure of account with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account, or <code>null</code> if a closure of account with the primary key could not be found
	 */
	public ClosureOfAccount fetchByPrimaryKey(long closureOfAccountId);

	/**
	 * Returns all the closure of accounts.
	 *
	 * @return the closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findAll();

	/**
	 * Returns a range of all the closure of accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the closure of accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator);

	/**
	 * Returns an ordered range of all the closure of accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of closure of accounts
	 */
	public java.util.List<ClosureOfAccount> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClosureOfAccount>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the closure of accounts from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of closure of accounts.
	 *
	 * @return the number of closure of accounts
	 */
	public int countAll();

}