/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for ClosureOfAccount. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccountLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface ClosureOfAccountLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.local.account.closeyouraccount.service.impl.ClosureOfAccountLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the closure of account local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link ClosureOfAccountLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the closure of account to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public ClosureOfAccount addClosureOfAccount(
		ClosureOfAccount closureOfAccount);

	@Indexable(type = IndexableType.REINDEX)
	public ClosureOfAccount addClosureOfAccount(
			long groupId, long companyId, long userId,
			long formInstanceRecordId, Date closureDate)
		throws PortalException;

	/**
	 * Creates a new closure of account with the primary key. Does not add the closure of account to the database.
	 *
	 * @param closureOfAccountId the primary key for the new closure of account
	 * @return the new closure of account
	 */
	@Transactional(enabled = false)
	public ClosureOfAccount createClosureOfAccount(long closureOfAccountId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the closure of account from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public ClosureOfAccount deleteClosureOfAccount(
		ClosureOfAccount closureOfAccount);

	/**
	 * Deletes the closure of account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account that was removed
	 * @throws PortalException if a closure of account with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public ClosureOfAccount deleteClosureOfAccount(long closureOfAccountId)
		throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ClosureOfAccount fetchClosureOfAccount(long closureOfAccountId);

	/**
	 * Returns the closure of account matching the UUID and group.
	 *
	 * @param uuid the closure of account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ClosureOfAccount fetchClosureOfAccountByUuidAndGroupId(
		String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ClosureOfAccount> fetchClosureOfAccountsByCompanyId(
		long companyId);

	public List<ClosureOfAccount> findByClosureDateAndCompanyId(long companyId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the closure of account with the primary key.
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account
	 * @throws PortalException if a closure of account with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ClosureOfAccount getClosureOfAccount(long closureOfAccountId)
		throws PortalException;

	/**
	 * Returns the closure of account matching the UUID and group.
	 *
	 * @param uuid the closure of account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching closure of account
	 * @throws PortalException if a matching closure of account could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ClosureOfAccount getClosureOfAccountByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the closure of accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of closure of accounts
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ClosureOfAccount> getClosureOfAccounts(int start, int end);

	/**
	 * Returns all the closure of accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the closure of accounts
	 * @param companyId the primary key of the company
	 * @return the matching closure of accounts, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ClosureOfAccount> getClosureOfAccountsByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of closure of accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the closure of accounts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching closure of accounts, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ClosureOfAccount> getClosureOfAccountsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ClosureOfAccount> orderByComparator);

	/**
	 * Returns the number of closure of accounts.
	 *
	 * @return the number of closure of accounts
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getClosureOfAccountsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the closure of account in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public ClosureOfAccount updateClosureOfAccount(
		ClosureOfAccount closureOfAccount);

}