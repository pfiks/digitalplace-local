package com.placecube.digitalplace.local.account.closeyouraccount.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.SYSTEM)
@Meta.OCD(id = "com.placecube.digitalplace.local.account.closeyouraccount.configuration.CloseYourAccountConfiguration", localization = "content/Language", name = "close-your-account")
public interface CloseYourAccountConfiguration {

	@Meta.AD(required = false, deflt = "0 3 0 * * ?", name = "close-your-account-scheduler-cron-expression", description = "close-your-account-scheduler-cron-expression-description")
	String schedulerCronExpression();

	@Meta.AD(required = false, deflt = "false", name = "close-your-account-scheduler-enabled")
	boolean schedulerEnabled();

}
