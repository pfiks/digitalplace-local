/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for ClosureOfAccount. This utility wraps
 * <code>com.placecube.digitalplace.local.account.closeyouraccount.service.impl.ClosureOfAccountLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccountLocalService
 * @generated
 */
public class ClosureOfAccountLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.local.account.closeyouraccount.service.impl.ClosureOfAccountLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the closure of account to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was added
	 */
	public static ClosureOfAccount addClosureOfAccount(
		ClosureOfAccount closureOfAccount) {

		return getService().addClosureOfAccount(closureOfAccount);
	}

	public static ClosureOfAccount addClosureOfAccount(
			long groupId, long companyId, long userId,
			long formInstanceRecordId, java.util.Date closureDate)
		throws PortalException {

		return getService().addClosureOfAccount(
			groupId, companyId, userId, formInstanceRecordId, closureDate);
	}

	/**
	 * Creates a new closure of account with the primary key. Does not add the closure of account to the database.
	 *
	 * @param closureOfAccountId the primary key for the new closure of account
	 * @return the new closure of account
	 */
	public static ClosureOfAccount createClosureOfAccount(
		long closureOfAccountId) {

		return getService().createClosureOfAccount(closureOfAccountId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the closure of account from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was removed
	 */
	public static ClosureOfAccount deleteClosureOfAccount(
		ClosureOfAccount closureOfAccount) {

		return getService().deleteClosureOfAccount(closureOfAccount);
	}

	/**
	 * Deletes the closure of account with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account that was removed
	 * @throws PortalException if a closure of account with the primary key could not be found
	 */
	public static ClosureOfAccount deleteClosureOfAccount(
			long closureOfAccountId)
		throws PortalException {

		return getService().deleteClosureOfAccount(closureOfAccountId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ClosureOfAccount fetchClosureOfAccount(
		long closureOfAccountId) {

		return getService().fetchClosureOfAccount(closureOfAccountId);
	}

	/**
	 * Returns the closure of account matching the UUID and group.
	 *
	 * @param uuid the closure of account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching closure of account, or <code>null</code> if a matching closure of account could not be found
	 */
	public static ClosureOfAccount fetchClosureOfAccountByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchClosureOfAccountByUuidAndGroupId(
			uuid, groupId);
	}

	public static List<ClosureOfAccount> fetchClosureOfAccountsByCompanyId(
		long companyId) {

		return getService().fetchClosureOfAccountsByCompanyId(companyId);
	}

	public static List<ClosureOfAccount> findByClosureDateAndCompanyId(
		long companyId) {

		return getService().findByClosureDateAndCompanyId(companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the closure of account with the primary key.
	 *
	 * @param closureOfAccountId the primary key of the closure of account
	 * @return the closure of account
	 * @throws PortalException if a closure of account with the primary key could not be found
	 */
	public static ClosureOfAccount getClosureOfAccount(long closureOfAccountId)
		throws PortalException {

		return getService().getClosureOfAccount(closureOfAccountId);
	}

	/**
	 * Returns the closure of account matching the UUID and group.
	 *
	 * @param uuid the closure of account's UUID
	 * @param groupId the primary key of the group
	 * @return the matching closure of account
	 * @throws PortalException if a matching closure of account could not be found
	 */
	public static ClosureOfAccount getClosureOfAccountByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getClosureOfAccountByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the closure of accounts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.closeyouraccount.model.impl.ClosureOfAccountModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @return the range of closure of accounts
	 */
	public static List<ClosureOfAccount> getClosureOfAccounts(
		int start, int end) {

		return getService().getClosureOfAccounts(start, end);
	}

	/**
	 * Returns all the closure of accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the closure of accounts
	 * @param companyId the primary key of the company
	 * @return the matching closure of accounts, or an empty list if no matches were found
	 */
	public static List<ClosureOfAccount> getClosureOfAccountsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getClosureOfAccountsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of closure of accounts matching the UUID and company.
	 *
	 * @param uuid the UUID of the closure of accounts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of closure of accounts
	 * @param end the upper bound of the range of closure of accounts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching closure of accounts, or an empty list if no matches were found
	 */
	public static List<ClosureOfAccount> getClosureOfAccountsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ClosureOfAccount> orderByComparator) {

		return getService().getClosureOfAccountsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of closure of accounts.
	 *
	 * @return the number of closure of accounts
	 */
	public static int getClosureOfAccountsCount() {
		return getService().getClosureOfAccountsCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the closure of account in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ClosureOfAccountLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param closureOfAccount the closure of account
	 * @return the closure of account that was updated
	 */
	public static ClosureOfAccount updateClosureOfAccount(
		ClosureOfAccount closureOfAccount) {

		return getService().updateClosureOfAccount(closureOfAccount);
	}

	public static ClosureOfAccountLocalService getService() {
		return _service;
	}

	public static void setService(ClosureOfAccountLocalService service) {
		_service = service;
	}

	private static volatile ClosureOfAccountLocalService _service;

}