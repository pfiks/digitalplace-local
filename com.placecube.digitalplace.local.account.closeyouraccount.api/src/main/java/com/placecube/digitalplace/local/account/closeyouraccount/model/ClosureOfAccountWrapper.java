/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ClosureOfAccount}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccount
 * @generated
 */
public class ClosureOfAccountWrapper
	extends BaseModelWrapper<ClosureOfAccount>
	implements ClosureOfAccount, ModelWrapper<ClosureOfAccount> {

	public ClosureOfAccountWrapper(ClosureOfAccount closureOfAccount) {
		super(closureOfAccount);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("closureOfAccountId", getClosureOfAccountId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("formInstanceRecordId", getFormInstanceRecordId());
		attributes.put("closureDate", getClosureDate());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long closureOfAccountId = (Long)attributes.get("closureOfAccountId");

		if (closureOfAccountId != null) {
			setClosureOfAccountId(closureOfAccountId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long formInstanceRecordId = (Long)attributes.get(
			"formInstanceRecordId");

		if (formInstanceRecordId != null) {
			setFormInstanceRecordId(formInstanceRecordId);
		}

		Date closureDate = (Date)attributes.get("closureDate");

		if (closureDate != null) {
			setClosureDate(closureDate);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	@Override
	public ClosureOfAccount cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the closure date of this closure of account.
	 *
	 * @return the closure date of this closure of account
	 */
	@Override
	public Date getClosureDate() {
		return model.getClosureDate();
	}

	/**
	 * Returns the closure of account ID of this closure of account.
	 *
	 * @return the closure of account ID of this closure of account
	 */
	@Override
	public long getClosureOfAccountId() {
		return model.getClosureOfAccountId();
	}

	/**
	 * Returns the company ID of this closure of account.
	 *
	 * @return the company ID of this closure of account
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this closure of account.
	 *
	 * @return the create date of this closure of account
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the form instance record ID of this closure of account.
	 *
	 * @return the form instance record ID of this closure of account
	 */
	@Override
	public long getFormInstanceRecordId() {
		return model.getFormInstanceRecordId();
	}

	/**
	 * Returns the group ID of this closure of account.
	 *
	 * @return the group ID of this closure of account
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this closure of account.
	 *
	 * @return the modified date of this closure of account
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this closure of account.
	 *
	 * @return the primary key of this closure of account
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the status of this closure of account.
	 *
	 * @return the status of this closure of account
	 */
	@Override
	public int getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the user ID of this closure of account.
	 *
	 * @return the user ID of this closure of account
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this closure of account.
	 *
	 * @return the user name of this closure of account
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this closure of account.
	 *
	 * @return the user uuid of this closure of account
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this closure of account.
	 *
	 * @return the uuid of this closure of account
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the closure date of this closure of account.
	 *
	 * @param closureDate the closure date of this closure of account
	 */
	@Override
	public void setClosureDate(Date closureDate) {
		model.setClosureDate(closureDate);
	}

	/**
	 * Sets the closure of account ID of this closure of account.
	 *
	 * @param closureOfAccountId the closure of account ID of this closure of account
	 */
	@Override
	public void setClosureOfAccountId(long closureOfAccountId) {
		model.setClosureOfAccountId(closureOfAccountId);
	}

	/**
	 * Sets the company ID of this closure of account.
	 *
	 * @param companyId the company ID of this closure of account
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this closure of account.
	 *
	 * @param createDate the create date of this closure of account
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the form instance record ID of this closure of account.
	 *
	 * @param formInstanceRecordId the form instance record ID of this closure of account
	 */
	@Override
	public void setFormInstanceRecordId(long formInstanceRecordId) {
		model.setFormInstanceRecordId(formInstanceRecordId);
	}

	/**
	 * Sets the group ID of this closure of account.
	 *
	 * @param groupId the group ID of this closure of account
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this closure of account.
	 *
	 * @param modifiedDate the modified date of this closure of account
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this closure of account.
	 *
	 * @param primaryKey the primary key of this closure of account
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the status of this closure of account.
	 *
	 * @param status the status of this closure of account
	 */
	@Override
	public void setStatus(int status) {
		model.setStatus(status);
	}

	/**
	 * Sets the user ID of this closure of account.
	 *
	 * @param userId the user ID of this closure of account
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this closure of account.
	 *
	 * @param userName the user name of this closure of account
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this closure of account.
	 *
	 * @param userUuid the user uuid of this closure of account
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this closure of account.
	 *
	 * @param uuid the uuid of this closure of account
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected ClosureOfAccountWrapper wrap(ClosureOfAccount closureOfAccount) {
		return new ClosureOfAccountWrapper(closureOfAccount);
	}

}