package com.placecube.digitalplace.local.bankaccount.loqate.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.bankaccount.loqate.configuration.LoqateBankAccountCompanyConfiguration;
import com.placecube.digitalplace.local.bankaccount.loqate.constants.LoqateBankValidationConstants;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationResponse;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationServiceException;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LoqateBankAccountConnectorTest {

	private static final long COMPANY_ID = 322l;
	private static final String ACCOUNT_NUMBER = "54643651";
	private static final String LOQATE_API_KEY = "api-key";
	private static final String LOQATE_VALIDATION_ENDPOINT = "https://loqate/validation";
	private static final String SORT_CODE = "129823";

	@InjectMocks
	private LoqateBankAccountConnector loqateBankAccountConnector;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private LoqateHttpService mockLoqateHttpService;

	@Mock
	private CloseableHttpClient mockCloseableHttpClient;

	@Mock
	private CloseableHttpResponse mockCloseableHttpResponse;

	@Mock
	private HttpGet mockHttpGet;

	@Mock
	private LoqateBankAccountCompanyConfiguration mockLoqateBankAccountCompanyConfiguration;

	@Test
	public void validateBankAccount_WhenResponseValidationResultIsCorrect_ThenReturnsValidBankAccountValidation() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoqateBankAccountCompanyConfiguration);

		when(mockLoqateBankAccountCompanyConfiguration.apiKey()).thenReturn(LOQATE_API_KEY);
		when(mockLoqateBankAccountCompanyConfiguration.bankAccountValidationEndpoint()).thenReturn(LOQATE_VALIDATION_ENDPOINT);

		when(mockLoqateHttpService.getHttpGetMethod(LOQATE_VALIDATION_ENDPOINT + "?Key=" + LOQATE_API_KEY + "&AccountNumber=" + ACCOUNT_NUMBER + "&SortCode=" + SORT_CODE)).thenReturn(mockHttpGet);
		when(mockLoqateHttpService.getHttpClient()).thenReturn(mockCloseableHttpClient);
		when(mockLoqateHttpService.executeRequest(mockHttpGet, mockCloseableHttpClient)).thenReturn(mockCloseableHttpResponse);
		when(mockLoqateHttpService.getResponseStatusCode(mockCloseableHttpResponse)).thenReturn(HttpURLConnection.HTTP_OK);
		when(mockLoqateHttpService.getResponseValidationResult(mockCloseableHttpResponse)).thenReturn(LoqateBankValidationConstants.VALIDATION_RESPONSE_CORRECT);

		BankAccountValidationResponse response = loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);

		assertThat(response, equalTo(BankAccountValidationResponse.VALID));
	}

	@Test
	public void validateBankAccount_WhenResponseValidationResultIsNotCorrect_ThenReturnsInvalidBankAccountValidation() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoqateBankAccountCompanyConfiguration);

		when(mockLoqateBankAccountCompanyConfiguration.apiKey()).thenReturn(LOQATE_API_KEY);
		when(mockLoqateBankAccountCompanyConfiguration.bankAccountValidationEndpoint()).thenReturn(LOQATE_VALIDATION_ENDPOINT);

		when(mockLoqateHttpService.getHttpGetMethod(LOQATE_VALIDATION_ENDPOINT + "?Key=" + LOQATE_API_KEY + "&AccountNumber=" + ACCOUNT_NUMBER + "&SortCode=" + SORT_CODE)).thenReturn(mockHttpGet);
		when(mockLoqateHttpService.getHttpClient()).thenReturn(mockCloseableHttpClient);
		when(mockLoqateHttpService.executeRequest(mockHttpGet, mockCloseableHttpClient)).thenReturn(mockCloseableHttpResponse);
		when(mockLoqateHttpService.getResponseStatusCode(mockCloseableHttpResponse)).thenReturn(HttpURLConnection.HTTP_OK);
		when(mockLoqateHttpService.getResponseValidationResult(mockCloseableHttpResponse)).thenReturn(LoqateBankValidationConstants.VALIDATION_RESPONSE_INCORRECT);

		BankAccountValidationResponse response = loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);

		assertThat(response, equalTo(BankAccountValidationResponse.INVALID_ACCOUNT));
	}

	@Test(expected = BankAccountValidationException.class)
	public void validateBankAccount_WhenResponseValidationParseFails_ThenThrowsBankAccountValidationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoqateBankAccountCompanyConfiguration);

		when(mockLoqateBankAccountCompanyConfiguration.apiKey()).thenReturn(LOQATE_API_KEY);
		when(mockLoqateBankAccountCompanyConfiguration.bankAccountValidationEndpoint()).thenReturn(LOQATE_VALIDATION_ENDPOINT);

		when(mockLoqateHttpService.getHttpGetMethod(LOQATE_VALIDATION_ENDPOINT + "?Key=" + LOQATE_API_KEY + "&AccountNumber=" + ACCOUNT_NUMBER + "&SortCode=" + SORT_CODE)).thenReturn(mockHttpGet);
		when(mockLoqateHttpService.getHttpClient()).thenReturn(mockCloseableHttpClient);
		when(mockLoqateHttpService.executeRequest(mockHttpGet, mockCloseableHttpClient)).thenReturn(mockCloseableHttpResponse);
		when(mockLoqateHttpService.getResponseStatusCode(mockCloseableHttpResponse)).thenReturn(HttpURLConnection.HTTP_OK);
		when(mockLoqateHttpService.getResponseValidationResult(mockCloseableHttpResponse)).thenThrow(new BankAccountValidationException("Error"));

		loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);
	}

	@Test(expected = BankAccountValidationServiceException.class)
	public void validateBankAccount_WhenResponseCodeIsNotSuccess_ThenThrowsBankAccountValidationServiceException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoqateBankAccountCompanyConfiguration);

		when(mockLoqateBankAccountCompanyConfiguration.apiKey()).thenReturn(LOQATE_API_KEY);
		when(mockLoqateBankAccountCompanyConfiguration.bankAccountValidationEndpoint()).thenReturn(LOQATE_VALIDATION_ENDPOINT);

		when(mockLoqateHttpService.getHttpGetMethod(LOQATE_VALIDATION_ENDPOINT + "?Key=" + LOQATE_API_KEY + "&AccountNumber=" + ACCOUNT_NUMBER + "&SortCode=" + SORT_CODE)).thenReturn(mockHttpGet);
		when(mockLoqateHttpService.getHttpClient()).thenReturn(mockCloseableHttpClient);
		when(mockLoqateHttpService.executeRequest(mockHttpGet, mockCloseableHttpClient)).thenReturn(mockCloseableHttpResponse);
		when(mockLoqateHttpService.getResponseStatusCode(mockCloseableHttpResponse)).thenReturn(HttpURLConnection.HTTP_BAD_REQUEST);

		loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);
	}

	@Test(expected = BankAccountValidationServiceException.class)
	public void validateBankAccount_WhenCallExecutionFails_ThenThrowsBankAccountValidationServiceException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoqateBankAccountCompanyConfiguration);

		when(mockLoqateBankAccountCompanyConfiguration.apiKey()).thenReturn(LOQATE_API_KEY);
		when(mockLoqateBankAccountCompanyConfiguration.bankAccountValidationEndpoint()).thenReturn(LOQATE_VALIDATION_ENDPOINT);

		when(mockLoqateHttpService.getHttpGetMethod(LOQATE_VALIDATION_ENDPOINT + "?Key=" + LOQATE_API_KEY + "&AccountNumber=" + ACCOUNT_NUMBER + "&SortCode=" + SORT_CODE)).thenReturn(mockHttpGet);
		when(mockLoqateHttpService.getHttpClient()).thenReturn(mockCloseableHttpClient);
		when(mockLoqateHttpService.executeRequest(mockHttpGet, mockCloseableHttpClient)).thenThrow(new IOException());

		loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);
	}

	@Test(expected = BankAccountValidationServiceException.class)
	public void validateBankAccount_WhenConfiguredSettingsRetrievalFails_ThenThrowsBankAccountValidationServiceException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);
	}

	@Test
	public void validateBankAccount_WhenServiceCallIsSuccessful_ThenClosesResources() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoqateBankAccountCompanyConfiguration);

		when(mockLoqateBankAccountCompanyConfiguration.apiKey()).thenReturn(LOQATE_API_KEY);
		when(mockLoqateBankAccountCompanyConfiguration.bankAccountValidationEndpoint()).thenReturn(LOQATE_VALIDATION_ENDPOINT);

		when(mockLoqateHttpService.getHttpGetMethod(LOQATE_VALIDATION_ENDPOINT + "?Key=" + LOQATE_API_KEY + "&AccountNumber=" + ACCOUNT_NUMBER + "&SortCode=" + SORT_CODE)).thenReturn(mockHttpGet);
		when(mockLoqateHttpService.getHttpClient()).thenReturn(mockCloseableHttpClient);
		when(mockLoqateHttpService.executeRequest(mockHttpGet, mockCloseableHttpClient)).thenReturn(mockCloseableHttpResponse);
		when(mockLoqateHttpService.getResponseStatusCode(mockCloseableHttpResponse)).thenReturn(HttpURLConnection.HTTP_OK);
		when(mockLoqateHttpService.getResponseValidationResult(mockCloseableHttpResponse)).thenReturn(LoqateBankValidationConstants.VALIDATION_RESPONSE_CORRECT);

		loqateBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE);

		verify(mockLoqateHttpService, times(1)).quietlyCloseResources(mockCloseableHttpClient, mockCloseableHttpResponse);
	}
}
