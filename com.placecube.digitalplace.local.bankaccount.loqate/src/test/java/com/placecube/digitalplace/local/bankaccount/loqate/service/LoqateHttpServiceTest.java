package com.placecube.digitalplace.local.bankaccount.loqate.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.bankaccount.loqate.constants.LoqateBankValidationConstants;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationRequestException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationServiceException;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({ "javax.net.ssl.*" })
@PrepareForTest({ EntityUtils.class })
public class LoqateHttpServiceTest {

	private static final int SUCCESS_STATUS_CODE = 200;
	private static final String URL = "URL";
	private static final String RESPONSE_CALL = "{response}";

	@InjectMocks
	private LoqateHttpService loqateHttpService;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private Portal mockPortal;

	@Mock
	private HttpUriRequest mockHttpUriRequest;

	@Mock
	private CloseableHttpClient mockCloseableHttpClient;

	@Mock
	private CloseableHttpResponse mockCloseableHttpResponse;

	@Mock
	private HttpGet mockHttpGet;

	@Mock
	private HttpEntity mockHttpEntity;

	@Mock
	private StatusLine mockStatusLine;

	@Mock
	private JSONObject mockResultJSONObject;

	@Mock
	private JSONObject mockBankDetailsJSONObject;

	@Mock
	private JSONArray mockJSONArray;

	@Before
	public void setUp() {
		mockStatic(EntityUtils.class);
	}

	@Test
	public void executeRequest_WhenNoErrors_ThenReturnsTheCloseableHttpResponse() throws ClientProtocolException, IOException {
		when(mockCloseableHttpClient.execute(mockHttpUriRequest)).thenReturn(mockCloseableHttpResponse);
		CloseableHttpResponse closeableHttpResponse = loqateHttpService.executeRequest(mockHttpUriRequest, mockCloseableHttpClient);

		assertThat(mockCloseableHttpResponse, sameInstance(closeableHttpResponse));
	}

	@Test(expected = ClientProtocolException.class)
	public void executeRequest_WhenHttpCallFieldsWithProtocolException_ThenThrowsClientProtocolException() throws ClientProtocolException, IOException {
		when(mockCloseableHttpClient.execute(mockHttpUriRequest)).thenThrow(ClientProtocolException.class);
		loqateHttpService.executeRequest(mockHttpUriRequest, mockCloseableHttpClient);
	}

	@Test(expected = IOException.class)
	public void executeRequest_WhenHttpCallFailsWithIOException_ThenThrowsIOException() throws ClientProtocolException, IOException {
		when(mockCloseableHttpClient.execute(mockHttpUriRequest)).thenThrow(IOException.class);
		loqateHttpService.executeRequest(mockHttpUriRequest, mockCloseableHttpClient);
	}

	@Test
	public void getHttpClient_WhenNoErrors_ThenReturnsCloseableHttpClient() {
		CloseableHttpClient closeableHttpClient = loqateHttpService.getHttpClient();
		assertThat(closeableHttpClient, notNullValue());
	}

	@Test
	public void getHttpGetMethod_WhenNoerrors_ThenReturnsHttpGetObject() throws Exception {
		HttpGet httpGet = loqateHttpService.getHttpGetMethod(URL);
		assertThat(httpGet, notNullValue());
	}

	@Test
	public void getResponseStatusCode_WhenStatusLineIsNull_ThenReturnsInternalServerError() throws Exception {
		when(mockCloseableHttpResponse.getStatusLine()).thenReturn(null);

		int statusCode = loqateHttpService.getResponseStatusCode(mockCloseableHttpResponse);

		assertThat(statusCode, equalTo(500));
	}

	@Test
	public void getResponseStatusCode_WhenNoErrors_ThenReturnsResponseStatusCode() throws Exception {
		when(mockCloseableHttpResponse.getStatusLine()).thenReturn(mockStatusLine);
		when(mockStatusLine.getStatusCode()).thenReturn(SUCCESS_STATUS_CODE);

		int statusCode = loqateHttpService.getResponseStatusCode(mockCloseableHttpResponse);

		assertThat(statusCode, equalTo(SUCCESS_STATUS_CODE));
	}

	@Test
	public void quietlyCloseResources_WhenNoErrors_ThenResponseAndClientGetClosed() throws IOException {
		loqateHttpService.quietlyCloseResources(mockCloseableHttpClient, mockCloseableHttpResponse);

		verify(mockCloseableHttpResponse, times(1)).close();
		verify(mockCloseableHttpClient, times(1)).close();
	}

	@Test
	public void quietlyCloseResources_WhenResponseCloseFails_ThenClientIsClosed() throws IOException {
		doThrow(new IOException()).when(mockCloseableHttpResponse).close();

		loqateHttpService.quietlyCloseResources(mockCloseableHttpClient, mockCloseableHttpResponse);

		verify(mockCloseableHttpClient, times(1)).close();
	}

	@Test
	public void quietlyCloseResources_WhenResponseAndClientCloseFail_ThenNoExceptionIsThrown() throws IOException {
		doThrow(new IOException()).when(mockCloseableHttpResponse).close();
		doThrow(new IOException()).when(mockCloseableHttpClient).close();

		try {
			loqateHttpService.quietlyCloseResources(mockCloseableHttpClient, mockCloseableHttpResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void getResponseValidationResult_WhenNoErrorsAndIsCorrectResponsePropertyIsTrue_ThenReturnsCorrectValidationResponse() throws Exception {
		when(mockCloseableHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(RESPONSE_CALL);

		when(mockJSONFactory.createJSONObject(RESPONSE_CALL)).thenReturn(mockResultJSONObject);

		when(mockResultJSONObject.getJSONArray("Items")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockBankDetailsJSONObject);
		when(mockBankDetailsJSONObject.get("IsCorrect")).thenReturn(Boolean.TRUE);

		String result = loqateHttpService.getResponseValidationResult(mockCloseableHttpResponse);

		assertThat(result, equalTo(LoqateBankValidationConstants.VALIDATION_RESPONSE_CORRECT));
	}

	@Test
	public void getResponseValidationResult_WhenNoErrorsAndIsCorrectResponsePropertyIsFalse_ThenReturnsIncorrectValidationResponse() throws Exception {
		when(mockCloseableHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(RESPONSE_CALL);

		when(mockJSONFactory.createJSONObject(RESPONSE_CALL)).thenReturn(mockResultJSONObject);

		when(mockResultJSONObject.getJSONArray("Items")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockBankDetailsJSONObject);
		when(mockBankDetailsJSONObject.get("IsCorrect")).thenReturn(Boolean.FALSE);

		String result = loqateHttpService.getResponseValidationResult(mockCloseableHttpResponse);

		assertThat(result, equalTo(LoqateBankValidationConstants.VALIDATION_RESPONSE_INCORRECT));
	}

	@Test
	public void getResponseValidationResult_WhenResponseContainsErrorProperty_ThenThrowsBankAccountValidationRequestExceptionWithDescriptionMessage() throws Exception {
		final String errorDescription = "There has been an error";
		when(mockCloseableHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(RESPONSE_CALL);

		when(mockJSONFactory.createJSONObject(RESPONSE_CALL)).thenReturn(mockResultJSONObject);

		when(mockResultJSONObject.getJSONArray("Items")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockBankDetailsJSONObject);
		when(mockBankDetailsJSONObject.get("Error")).thenReturn("1001");
		when(mockBankDetailsJSONObject.get("Description")).thenReturn(errorDescription);

		try {
			loqateHttpService.getResponseValidationResult(mockCloseableHttpResponse);

			fail("An exception should have been thrown");

		} catch (BankAccountValidationRequestException e) {
			assertThat(e.getMessage(), equalTo(errorDescription));
		}
	}

	@Test(expected = BankAccountValidationServiceException.class)
	public void getResponseValidationResult_WhenNoItemsAreReturnedInResponse_ThenThrowsBankAccountValidationServiceException() throws Exception {
		when(mockCloseableHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(RESPONSE_CALL);

		when(mockJSONFactory.createJSONObject(RESPONSE_CALL)).thenReturn(mockResultJSONObject);

		when(mockResultJSONObject.getJSONArray("Items")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(0);

		loqateHttpService.getResponseValidationResult(mockCloseableHttpResponse);
	}

	@Test(expected = BankAccountValidationServiceException.class)
	public void getResponseValidationResult_WhenServerResponseHeadersCannotBeParsed_ThenThrowsBankAccountValidationServiceException() throws Exception {
		when(mockCloseableHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenThrow(new ParseException());

		loqateHttpService.getResponseValidationResult(mockCloseableHttpResponse);
	}

	@Test(expected = BankAccountValidationServiceException.class)
	public void getResponseValidationResult_WhenServerResponseCannotBeParsedToJSONObject_ThenThrowsBankAccountValidationServiceException() throws Exception {
		when(mockCloseableHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(RESPONSE_CALL);

		when(mockJSONFactory.createJSONObject(RESPONSE_CALL)).thenThrow(new JSONException());

		loqateHttpService.getResponseValidationResult(mockCloseableHttpResponse);
	}

}
