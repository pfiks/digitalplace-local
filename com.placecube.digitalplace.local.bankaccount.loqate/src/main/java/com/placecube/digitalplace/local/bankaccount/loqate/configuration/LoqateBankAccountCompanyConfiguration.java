package com.placecube.digitalplace.local.bankaccount.loqate.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.bankaccount.loqate.configuration.LoqateBankAccountCompanyConfiguration", localization = "content/Language", name = "loqate-bank-account")
public interface LoqateBankAccountCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "api-key")
	String apiKey();

	@Meta.AD(required = false, deflt = "https://api.addressy.com/BankAccountValidation/Interactive/Validate/v2.00/json3.ws", name = "loqate-validation-end-point")
	String bankAccountValidationEndpoint();
}
