package com.placecube.digitalplace.local.bankaccount.loqate.service;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.bankaccount.loqate.constants.LoqateBankValidationConstants;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationRequestException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationServiceException;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = LoqateHttpService.class)
public class LoqateHttpService {

	public static final String ACCEPT_HEADER = "application/json";

	public static final String ACCOUNT_NUMBER_REQUEST_PARAMETER = "AccountNumber";

	public static final String SORT_CODE_REQUEST_PARAMETER = "SortCode";

	private static final Log LOG = LogFactoryUtil.getLog(LoqateHttpService.class);

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	public CloseableHttpResponse executeRequest(HttpUriRequest httpUriRequest, CloseableHttpClient closeableHttpClient) throws IOException {
		return closeableHttpClient.execute(httpUriRequest);
	}

	public CloseableHttpClient getHttpClient() {
		return HttpClients.createDefault();
	}

	public HttpGet getHttpGetMethod(String url) {
		return new HttpGet(url);
	}

	public int getResponseStatusCode(CloseableHttpResponse closeableHttpResponse) {
		StatusLine statusLine = closeableHttpResponse.getStatusLine();

		if (statusLine != null) {
			return statusLine.getStatusCode();
		} else {
			LOG.error("Http response status line not yet set");
			return HttpURLConnection.HTTP_INTERNAL_ERROR;
		}
	}

	public String getResponseValidationResult(CloseableHttpResponse closeableHttpResponse) throws BankAccountValidationException {
		try {
			String result = EntityUtils.toString(closeableHttpResponse.getEntity());

			JSONObject resultJson = jsonFactory.createJSONObject(result);

			JSONArray itemsJson = resultJson.getJSONArray("Items");

			validateResponse(itemsJson);

			return getResponseValidationResult(itemsJson);

		} catch (ParseException | IOException | JSONException e) {
			throw new BankAccountValidationServiceException("Invalid server response", e);
		}
	}

	public void quietlyCloseResources(CloseableHttpClient client, CloseableHttpResponse response) {
		try {
			response.close();
		} catch (Exception e) {
			LOG.debug(e);
		}
		try {
			client.close();
		} catch (Exception e) {
			LOG.debug(e);
		}
	}

	private String getResponseValidationResult(JSONArray itemsJson) {

		String validationResponse;
		JSONObject bankDetailsItem = itemsJson.getJSONObject(0);

		if (Boolean.TRUE.equals(bankDetailsItem.get("IsCorrect"))) {
			validationResponse = LoqateBankValidationConstants.VALIDATION_RESPONSE_CORRECT;
		} else {
			validationResponse = LoqateBankValidationConstants.VALIDATION_RESPONSE_INCORRECT;
		}

		return validationResponse;
	}

	private void validateResponse(JSONArray itemsJson) throws BankAccountValidationRequestException, BankAccountValidationServiceException {
		if (itemsJson.length() > 0) {
			JSONObject bankDetailsItem = itemsJson.getJSONObject(0);

			if (Validator.isNotNull(bankDetailsItem.get("Error"))) {
				String errorDescription = (String) bankDetailsItem.get("Description");
				throw new BankAccountValidationRequestException(GetterUtil.getString(errorDescription));
			}

		} else {
			throw new BankAccountValidationServiceException("Unknown server response");
		}

	}
}