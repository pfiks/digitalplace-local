package com.placecube.digitalplace.local.bankaccount.loqate.constants;

public class LoqateBankValidationConstants {

	public static final String VALIDATION_RESPONSE_CORRECT = "verification-correct";

	public static final String VALIDATION_RESPONSE_INCORRECT = "verification-incorrect";

	private LoqateBankValidationConstants() {

	}
}
