package com.placecube.digitalplace.local.bankaccount.loqate.constants;

public class LoqateConnectorConstants {

	public static final String LOQATE_BANK_ACCOUNT_CONNECTOR_NAME = "loqate-bank-account-connector";

	private LoqateConnectorConstants() {

	}
}
