package com.placecube.digitalplace.local.bankaccount.loqate.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.bankaccount.constants.BankAccountConnectorConstants;
import com.placecube.digitalplace.local.bankaccount.loqate.configuration.LoqateBankAccountCompanyConfiguration;
import com.placecube.digitalplace.local.bankaccount.loqate.constants.LoqateBankValidationConstants;
import com.placecube.digitalplace.local.bankaccount.loqate.constants.LoqateConnectorConstants;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationResponse;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationServiceException;
import com.placecube.digitalplace.local.bankaccount.service.BankAccountConnector;
import com.placecube.digitalplace.local.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.local.connector.model.Connector;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY + "=200", ConnectorConstants.CONNECTOR_TYPE + "=" + BankAccountConnectorConstants.BANK_ACCOUNT_CONNECTOR_TYPE,
		ConnectorConstants.CONNECTOR_NAME + "=" + LoqateConnectorConstants.LOQATE_BANK_ACCOUNT_CONNECTOR_NAME }, service = Connector.class)
public class LoqateBankAccountConnector implements BankAccountConnector {

	private static final String KEY_URL_PARAMETER = "Key";
	private static final String ACCOUNT_NUMBER_URL_PARAMETER = "AccountNumber";
	private static final String SORT_CODE_URL_PARAMETER = "SortCode";

	private static final int REQUEST_SUCCESS = HttpURLConnection.HTTP_OK;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LoqateHttpService loqateHttpService;

	@Override
	public BankAccountValidationResponse validateBankAccount(long companyId, String accountNumber, String sortCode) throws BankAccountValidationException {

		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;

		try {
			LoqateBankAccountCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoqateBankAccountCompanyConfiguration.class, companyId);

			String apiKey = configuration.apiKey();
			String endPoint = configuration.bankAccountValidationEndpoint();

			String url = buildLoqateUrl(endPoint, apiKey, accountNumber, sortCode);

			HttpGet httpGet = loqateHttpService.getHttpGetMethod(url);
			client = loqateHttpService.getHttpClient();
			response = loqateHttpService.executeRequest(httpGet, client);

			int statusCode = loqateHttpService.getResponseStatusCode(response);
			if (statusCode == REQUEST_SUCCESS) {

				String validationResult = loqateHttpService.getResponseValidationResult(response);

				return LoqateBankValidationConstants.VALIDATION_RESPONSE_CORRECT.equals(validationResult) ? BankAccountValidationResponse.VALID : BankAccountValidationResponse.INVALID_ACCOUNT;
			} else {
				throw new BankAccountValidationServiceException("Invalid response status code: " + statusCode);
			}

		} catch (IOException | ConfigurationException e) {
			throw new BankAccountValidationServiceException(e);
		} finally {
			loqateHttpService.quietlyCloseResources(client, response);
		}
	}

	private String buildLoqateUrl(String endPoint, String apiKey, String accountNumber, String sortCode) {
		return endPoint + "?" + KEY_URL_PARAMETER + "=" + apiKey + "&" + ACCOUNT_NUMBER_URL_PARAMETER + "=" + accountNumber + "&" + SORT_CODE_URL_PARAMETER + "=" + sortCode;
	}
}