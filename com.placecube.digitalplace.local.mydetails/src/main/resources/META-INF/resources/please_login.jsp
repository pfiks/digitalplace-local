<%@ include file="init.jsp"%>

<h2 class="govuk-heading-m"><liferay-ui:message key="personal-details" /></h2>
<p class="govuk-body">
	<a href="${themeDisplay.getURLSignIn()}" class="govuk-link">
		<liferay-ui:message key="please-sign-in" />
	</a>
</p>
