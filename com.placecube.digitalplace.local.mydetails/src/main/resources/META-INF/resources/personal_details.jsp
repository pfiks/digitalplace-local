<%@ include file="init.jsp"%>

<jsp:useBean id="htmlUtil" class="com.liferay.portal.kernel.util.HtmlUtil" scope="request"/>

<liferay-ui:success key="verificationEmailSent" message="verification-email-sent" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.VIEW_MY_DETAIL %>" />
</portlet:renderURL>
<portlet:renderURL var="editPersonalDetailsURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_PERSONAL_DETAILS_RENDER %>" />
	<portlet:param name="backURL" value="${backURL}" />
</portlet:renderURL>
<portlet:renderURL var="editAdditionalDetailsURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_ADDITIONAL_DETAILS %>" />
	<portlet:param name="backURL" value="${backURL}" />
</portlet:renderURL>

<portlet:renderURL var="updateTwoFactorAuthenticationURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_TWO_FACTOR_AUTHENTICATION_RENDER %>" />
	<portlet:param name="backURL" value="${backURL}" />
</portlet:renderURL>

<portlet:renderURL var="changePasswordURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_PASSWORD_RENDER %>" />
	<portlet:param name="backURL" value="${backURL}" />
</portlet:renderURL>

<h2 class="govuk-heading-m"><liferay-ui:message key="personal-details" /></h2>
<dl class="govuk-summary-list govuk-!-margin-bottom-9">
	<div class="govuk-summary-list__row">
		<dt class="govuk-summary-list__key">
			<liferay-ui:message key="first-name" />
		</dt>
		<dd class="govuk-summary-list__value">
				${htmlUtil.escape(myDetailsContext.firstName)}
		</dd>
		<dd class="govuk-summary-list__actions">
			<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.firstNameEditEnabled()}">
				<a class="govuk-link" href="${editPersonalDetailsURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="first-name" /></span>
				</a>
			</c:if>
		</dd>
	</div>

	<div class="govuk-summary-list__row">
		<dt class="govuk-summary-list__key">
			<liferay-ui:message key="last-name" />
		</dt>
		<dd class="govuk-summary-list__value">
				${htmlUtil.escape(myDetailsContext.lastName)}
		</dd>
		<dd class="govuk-summary-list__actions">
			<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.lastNameEditEnabled()}">
				<a class="govuk-link" href="${editPersonalDetailsURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="last-name" /></span>
				</a>
			</c:if>
		</dd>
	</div>

	<div class="govuk-summary-list__row">
		<dt class="govuk-summary-list__key">
			<liferay-ui:message key="known-as" />
		</dt>
		<dd class="govuk-summary-list__value">
				${htmlUtil.escape(myDetailsContext.knownAs)}
		</dd>
		<dd class="govuk-summary-list__actions">
			<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.knownAsEditEnabled()}">
				<a class="govuk-link" href="${editPersonalDetailsURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="known-as" /></span>
				</a>
			</c:if>
		</dd>
	</div>

	<div class="govuk-summary-list__row">
		<dt class="govuk-summary-list__key">
			<liferay-ui:message key="email-address" />
		</dt>
		<dd class="govuk-summary-list__value">
				${htmlUtil.escape(myDetailsContext.emailAddress)}
		</dd>
		<dd class="govuk-summary-list__actions">
			<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.emailAddressEditEnabled()}">
				<a class="govuk-link" href="${editPersonalDetailsURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="email-address" /></span>
				</a>
			</c:if>
		</dd>
	</div>

	<div class="govuk-summary-list__row">
		<dt class="govuk-summary-list__key">
			<liferay-ui:message key="address" />
		</dt>
		<dd class="govuk-summary-list__value">
			<p>
					${not empty myDetailsContext.address.street1 ? htmlUtil.escape(myDetailsContext.address.street1) += '<br>' : ''}
					${not empty myDetailsContext.address.street2 ? htmlUtil.escape(myDetailsContext.address.street2) += '<br>' : ''}
					${not empty myDetailsContext.address.street3 ? htmlUtil.escape(myDetailsContext.address.street3) += '<br>' : ''}
					${not empty myDetailsContext.address.city ? htmlUtil.escape(myDetailsContext.address.city) += '<br>' : ''}
					${not empty myDetailsContext.address.zip ? htmlUtil.escape(myDetailsContext.address.zip) += '<br>' : ''}
			</p>
		</dd>
		<dd class="govuk-summary-list__actions">
			<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.userAddressEditEnabled()}">
				<a class="govuk-link" href="${editPersonalDetailsURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="address" /></span>
				</a>
			</c:if>
		</dd>
	</div>
</dl>

<c:if test="${isAdditionalDetailsSectionAvailable}">
	<%@ include file="/additional_details.jspf" %>
</c:if>

<h2 class="govuk-heading-m"><liferay-ui:message key="authentication" /></h2>
<dl class="govuk-summary-list govuk-!-margin-bottom-9">
	<c:if test="${myDetailsContext.twoFactorAuthenticationAvailable}">
		<div class="govuk-summary-list__row">
			<dt class="govuk-summary-list__key">
				<liferay-ui:message key="two-factor-authentication" />
			</dt>
			<dd class="govuk-summary-list__actions">
				<a class="govuk-link" href="${updateTwoFactorAuthenticationURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="two-factor-authentication" /></span>
				</a>
			</dd>
		</div>
	</c:if>
	<div class="govuk-summary-list__row">
		<dt class="govuk-summary-list__key">
			<liferay-ui:message key="password" />
		</dt>
		<dd class="govuk-summary-list__actions">
			<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.passwordEditEnabled()}">
				<a class="govuk-link" href="${changePasswordURL}">
					<liferay-ui:message key="change" /><span class="govuk-visually-hidden"> <liferay-ui:message key="password" /></span>
				</a>
			</c:if>
		</dd>
	</div>
</dl>


