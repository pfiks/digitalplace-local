<%@ include file="init.jsp"%>
<%@ include file="creation_errors.jspf"%>

<jsp:useBean id="htmlUtil" class="com.liferay.portal.kernel.util.HtmlUtil" scope="request"/>

<portlet:actionURL name="<%=MVCCommandKeys.UPDATE_PERSONAL_DETAILS_ACTION%>" var="updatePersonalDetailsURL" />
<a href="${backURL}" class="govuk-back-link">
	<liferay-ui:message key="back" />
</a>

<aui:form action="${updatePersonalDetailsURL}" method="post" name="myDetailsForm">
	<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.firstNameEditEnabled()}">
		<div class="govuk-form-group">
			<aui:input cssClass="govuk-input" name="<%=PortletRequestKeys.FIRST_NAME %>" required="true" label="first-name"
				value="${myDetailsContext.firstName}">
				<aui:validator name="maxLength">75</aui:validator>
			</aui:input>
		</div>
	</c:if>
	<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.lastNameEditEnabled()}">
		<div class="govuk-form-group">
			<aui:input cssClass="govuk-input" name="<%=PortletRequestKeys.LAST_NAME %>" required="true" label="last-name"
				value="${myDetailsContext.lastName}">
				<aui:validator name="maxLength">75</aui:validator>
			</aui:input>
		</div>
	</c:if>
	<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.knownAsEditEnabled()}">
		<div class="govuk-form-group">
			<aui:input cssClass="govuk-input" name="<%=KnownAsConstants.KNOWN_AS %>" label="known-as"
				value="${myDetailsContext.knownAs}">
				<aui:validator name="maxLength">75</aui:validator>
			</aui:input>
		</div>
	</c:if>
	<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.emailAddressEditEnabled()}">
		<div class="govuk-form-group ${notUniqueEmailAddress ? "govuk-form-group--error" : ""}">
			<p class="govuk-body">
				<liferay-ui:message key="you-need-to-verify-new-email-address" />
			</p>
			<c:if test="${notUniqueEmailAddress}">
				<p class="govuk-error-message">
					<liferay-ui:message key="the-email-address-you-requested-is-already-taken" />
				</p>
			</c:if>
			<aui:input cssClass="govuk-input ${notUniqueEmailAddress ? 'govuk-input--error' : ''}" name="<%=PortletRequestKeys.EMAIL_ADDRESS %>" required="true"
				label="email-address" value="${myDetailsContext.emailAddress}" helpMessage="you-need-to-verify-new-email-address">
				<aui:validator name="email" />
				<aui:validator name="maxLength">250</aui:validator>
			</aui:input>
		</div>
	</c:if>
	<c:if test="${myDetailsContext.myDetailsCompanyConfiguration.userAddressEditEnabled()}">
		<div class="govuk-form-group">
			<label><liferay-ui:message key="address" /></label>
			<p>
				${not empty myDetailsContext.address.street1 ? htmlUtil.escape(myDetailsContext.address.street1) += '<br>' : ''}
				${not empty myDetailsContext.address.street2 ? htmlUtil.escape(myDetailsContext.address.street2) += '<br>' : ''}
				${not empty myDetailsContext.address.street3 ? htmlUtil.escape(myDetailsContext.address.street3) += '<br>' : ''}
				${not empty myDetailsContext.address.city ? htmlUtil.escape(myDetailsContext.address.city) += '<br>' : ''}
				${not empty myDetailsContext.address.zip ? htmlUtil.escape(myDetailsContext.address.zip) += '<br>' : ''}
			</p>
			<div class="row no-gutters" id="my-details-postcode-lookup">
				<div class="col-md-8">
					<dp-address:postcode-lookup required="false" fallbackToNationalLookup="false"/>
				</div>
			</div>
		</div>
	</c:if>

	<aui:button type="submit" cssClass="govuk-button" value="update" />

</aui:form>