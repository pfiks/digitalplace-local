<%@page import="com.placecube.digitalplace.local.mydetails.constants.WebContentArticles"%>
<%@ include file="init.jsp"%>
<%@ include file="creation_errors.jspf"%>

<div class="row no-gutters">
	<a href="${backURL}" class="govuk-back-link">
		<liferay-ui:message key="back" />
	</a>

		<div class="col-md-12">

			<portlet:actionURL name="<%=MVCCommandKeys.UPDATE_TWO_FACTOR_AUTHENTICATION_ACTION%>" var="updateTwoFactorAuthenticationURL" />

				<c:choose>
					<c:when test="${not twoFactorAuthenticationArticleExist}">
						<clay:container-fluid>
							<clay:alert
								displayType="warning"
								message="two-factor-authentication-guidance-is-temporarily-unavailable"
							/>
						</clay:container-fluid>
					</c:when>
					<c:otherwise>
						<liferay-journal:journal-article
							articleId="<%= WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION.getArticleId() %>"
							groupId="${themeDisplay.companyGroupId}"
							showTitle="false" />
					</c:otherwise>
				</c:choose>

				<aui:form action="${updateTwoFactorAuthenticationURL}" method="post" name="myDetailsForm">
					<div class="row no-gutters">
						<div class="col-md-8">
							<gds-forms-ui:input-radio
								portletNamespace="${portletNamespace }"
								fieldName="<%= PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED %>"
								fieldLabel="turn-on-2fa"
								fieldValue="${ myDetailsContext.twoFactorAuthenticationEnabled }"
								fieldOptions="${ twoFactorAuthenticationEnabledOptions }"
								required="true"
							/>
						</div>
					</div>

					<aui:button-row>
						<aui:button type="submit" cssClass="btn btn-primary" value="update"
									icon="glyphicon glyphicon-chevron-right" iconAlign="right" />
					</aui:button-row>

				</aui:form>

		</div>

</div>