<%@ include file="init.jsp"%>
<%@ include file="creation_errors.jspf"%>

<portlet:actionURL name="<%=MVCCommandKeys.UPDATE_PASSWORD_ACTION%>" var="updatePasswordURL" />

<c:choose>
	<c:when test="${myDetailsContext.myDetailsCompanyConfiguration.passwordEditEnabled()}">

		<h1 class="govuk-heading-m current-password-wrapper">
			<liferay-ui:message key="current-password" />
		</h1>

		<h1 class="govuk-heading-m new-password-wrapper hide">
			<liferay-ui:message key="new-password" />
		</h1>

		<c:if test="${isPasswordPolicyArticlePresent}">
			<p>
				<liferay-journal:journal-article groupId="${ scopeGroupId }" articleId="${myDetailsContext.myDetailsCompanyConfiguration.passwordPolicyArticleId()}" />
			</p>
		</c:if>

		<aui:form action="${updatePasswordURL}" method="post" name="passwordForm">
			<p class="govuk-body">
				<liferay-ui:message key="user-logout" />
			</p>
			<aui:input type="hidden" name="tab" value="${empty tab  || tab == 'currentPassword' ?'currentPassword':'newPassword'}" />
			<div class="current-password-wrapper ${ empty tab  || tab == "currentPassword"  ?'':'hide'}">
				<div class="govuk-form-group ${not empty passwordErrorMessage or not empty currentPasswordErrorMessage ? 'govuk-form-group--error' : ''}">
					<c:if test="${not empty passwordErrorMessage or not empty currentPasswordErrorMessage}">
						<p class="govuk-error-message">
							${passwordErrorMessage}
							${currentPasswordErrorMessage}
						</p>
					</c:if>
					<aui:input cssClass="govuk-input ${not empty passwordErrorMessage or not empty currentPasswordErrorMessage ? 'govuk-input--error' : ''}" name="<%=PortletRequestKeys.CURRENT_PASSWORD%>" type="password" label="current-password" />
				</div>
				<aui:button type="submit" cssClass="govuk-button" value="continue" />
			</div>
			<div class="new-password-wrapper ${empty tab  || tab == "currentPassword" ?'hide':''}">

				<div class="govuk-form-group ${not empty passwordErrorMessage ? 'govuk-form-group--error' : ''}">
					<c:if test="${not empty passwordErrorMessage}">
						<p class="govuk-error-message">
							${passwordErrorMessage}
						</p>
					</c:if>
					<aui:input cssClass="govuk-input ${not empty passwordErrorMessage ? 'govuk-input--error' : ''}" name="<%=PortletRequestKeys.PASSWORD%>" type="password" label="password" />
				</div>
				<div class="govuk-form-group">
					<aui:input cssClass="govuk-input" name="<%=PortletRequestKeys.CONFIRM_PASSWORD %>" type="password" label="repeat-password" />
				</div>

				<aui:button type="submit" cssClass="govuk-button" value="change-password" />
			</div>
		</aui:form>
	</c:when>
	<c:otherwise>
		<p class="govuk-body">
			<liferay-ui:message key="password-modification-disabled" />
		</p>
	</c:otherwise>
</c:choose>

<div>
	<a href="${backURL}" class="govuk-back-link">
		<liferay-ui:message key="i-do-not-want-to-change-my-password" />
	</a>
</div>
