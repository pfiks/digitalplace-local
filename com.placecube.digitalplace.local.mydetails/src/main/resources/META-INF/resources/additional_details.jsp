<%@ include file="init.jsp"%>

<a href="${backURL}" class="govuk-back-link"> <liferay-ui:message
		key="back" />
</a>

<portlet:actionURL name="<%=MVCCommandKeys.UPDATE_ADDITIONAL_DETAILS%>"
	var="additionalDetailsURL" />

<aui:form action="${additionalDetailsURL}" method="post"
	name="myDetailsAdditionalDetailsForm">

	<c:if test="${userProfileConfiguration.jobTitleEnabled()}">

		<gds-forms-ui:input-text portletNamespace="${portletNamespace}"
			fieldName="<%= AccountValidatedField.JOB_TITLE.getFieldName()%>"
			fieldLabel="job-title-or-role"
			fieldValue="${accountContext.jobTitle}"
			errorMessage="${accountContext.getFieldError('jobTitle')}"
			required="${userProfileConfiguration.jobTitleRequired()}" />

	</c:if>

	<c:if test="${userProfileConfiguration.homePhoneNumberEnabled()}">

		<gds-forms-ui:input-text portletNamespace="${portletNamespace}"
			fieldName="<%= AccountValidatedField.HOME_PHONE.getFieldName()%>"
			fieldLabel="home-phone-number"
			fieldValue="${accountContext.homePhoneNumber}"
			errorMessage="${accountContext.getFieldError('homePhoneNumber')}"
			required="${userProfileConfiguration.homePhoneNumberRequired()}" />

	</c:if>

	<c:if test="${userProfileConfiguration.mobilePhoneNumberEnabled()}">

		<gds-forms-ui:input-text portletNamespace="${portletNamespace}"
			fieldName="<%= AccountValidatedField.MOBILE_PHONE.getFieldName()%>"
			fieldLabel="mobile-phone-number"
			fieldValue="${accountContext.mobilePhoneNumber}"
			errorMessage="${accountContext.getFieldError('mobilePhoneNumber')}"
			required="${userProfileConfiguration.mobilePhoneNumberRequired()}" />
	</c:if>

	<c:if test="${userProfileConfiguration.businessPhoneNumberEnabled()}">
		<gds-forms-ui:input-text portletNamespace="${portletNamespace}"
			fieldName="<%= AccountValidatedField.BUSINESS_PHONE.getFieldName()%>"
			fieldLabel="business-phone-number"
			fieldValue="${accountContext.businessPhoneNumber}"
			errorMessage="${accountContext.getFieldError('businessPhoneNumber')}"
			required="${userProfileConfiguration.businessPhoneNumberRequired()}" />
	</c:if>

	<c:if test="${not empty additionalDetailsExpandoFields}">
		<c:forEach items="${additionalDetailsExpandoFields}"
			var="expandoField">
			<gds-forms-ui:input-text portletNamespace="${portletNamespace}"
				fieldName="expando--${expandoField.getExpandoFieldKey()}"
				fieldLabel="${expandoField.getLabel(locale)}"
				fieldValue="${accountContext.getExpandoValue(expandoField.getExpandoFieldKey())}"
				errorMessage="${accountContext.getFieldError('expando--'.concat(expandoField.getExpandoFieldKey()) ) }"
				required="${expandoField.required}" />
		</c:forEach>
	</c:if>

	<aui:button type="submit" cssClass="govuk-button" value="update" />
</aui:form>
