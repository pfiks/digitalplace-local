<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri="http://liferay.com/tld/journal" prefix="liferay-journal" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ taglib uri="http://placecube.com/digitalplace/tld/address" prefix="dp-address" %>

<%@ taglib uri="http://placecube.com/digitalplace/tld/gds-forms-ui" prefix="gds-forms-ui"%>

<%@page import="com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.UserEmailAddressException"%>
<%@page import="com.liferay.portal.kernel.exception.UserPasswordException"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.exception.UserEmailAddressException"%>
<%@page import="com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.petra.string.StringUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys"%>
<%@page import="com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.placecube.digitalplace.user.account.constants.AccountValidatedField"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />
				
<c:set var="portletNamespace">
	<portlet:namespace/>
</c:set>