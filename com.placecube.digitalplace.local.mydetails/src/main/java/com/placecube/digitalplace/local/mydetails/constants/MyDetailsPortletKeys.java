package com.placecube.digitalplace.local.mydetails.constants;

public final class MyDetailsPortletKeys {

	public static final String MY_DETAILS = "com_placecube_digitalplace_local_mydetails_MyDetailsPortlet";

	private MyDetailsPortletKeys() {
	}

}