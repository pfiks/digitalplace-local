package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_PERSONAL_DETAILS_ACTION }, service = MVCActionCommand.class)
public class UpdatePersonalDetailsMVCActionCommand extends BaseMVCActionCommand {

	private final Log log = LogFactoryUtil.getLog(UpdatePersonalDetailsMVCActionCommand.class);

	@Reference
	private MyDetailsContextService myDetailsContextService;

	@Reference
	private MyDetailsService myDetailsService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		MyDetailsContext myDetailsContext = myDetailsContextService.getMyDetailsContext();
		myDetailsContextService.configurePersonalDetails(myDetailsContext, actionRequest);

		try {
			myDetailsService.updateMyDetails(actionRequest, myDetailsContext);
			actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_MY_DETAIL);
		} catch (Exception e) {
			log.debug(e);
			actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PERSONAL_DETAILS_RENDER);
			actionRequest.setAttribute(PortletRequestKeys.ERROR_EXCEPTION, e);
			SessionErrors.add(actionRequest, e.getClass(), e);
			hideDefaultErrorMessage(actionRequest);
		}
	}
}
