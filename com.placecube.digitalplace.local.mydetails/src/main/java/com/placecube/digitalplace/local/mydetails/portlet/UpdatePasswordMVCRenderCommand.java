package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.ViewKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "mvc.command.name=" + MVCCommandKeys.UPDATE_PASSWORD_RENDER }, service = MVCRenderCommand.class)
public class UpdatePasswordMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	private final Log log = LogFactoryUtil.getLog(UpdatePasswordMVCRenderCommand.class);

	@Reference
	private MyDetailsContextService myDetailsContextService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String backURL = ParamUtil.getString(renderRequest, PortletRequestKeys.BACK_URL);

		try {
			MyDetailsContext myDetailsContext = myDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(themeDisplay.getCompanyId());
			renderRequest.setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, myDetailsContext);

			String passwordPolicyArticleId = myDetailsContext.getMyDetailsCompanyConfiguration().passwordPolicyArticleId();
			renderRequest.setAttribute(PortletRequestKeys.TAB, renderRequest.getAttribute(PortletRequestKeys.TAB));

			renderRequest.setAttribute(PortletRequestKeys.IS_PASSWORD_POLICY_ARTICLE_PRESENT,
					Validator.isNotNull(journalArticleLocalService.fetchArticle(themeDisplay.getScopeGroupId(), passwordPolicyArticleId)));

		} catch (PortalException e) {
			log.error("Error getting company configuration: " + e.getMessage());
			log.debug(e);
			throw new PortletException(e);
		}

		renderRequest.setAttribute(PortletRequestKeys.BACK_URL, backURL);

		return ViewKeys.PASSWORD;
	}

}
