package com.placecube.digitalplace.local.mydetails.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.security.auth.session.AuthenticatedSessionManagerUtil;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "mvc.command.name=" + MVCCommandKeys.UPDATE_PASSWORD_ACTION }, service = MVCActionCommand.class)
public class UpdatePasswordMVCActionCommand extends BaseMVCActionCommand {

	private final Log log = LogFactoryUtil.getLog(UpdatePasswordMVCActionCommand.class);

	@Reference
	private MyDetailsService myDetailsService;

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		String password = ParamUtil.getString(actionRequest, PortletRequestKeys.PASSWORD);
		String confirmPassword = ParamUtil.getString(actionRequest, PortletRequestKeys.CONFIRM_PASSWORD);
		String currentPassword = ParamUtil.getString(actionRequest, PortletRequestKeys.CURRENT_PASSWORD);
		String tab = ParamUtil.getString(actionRequest, PortletRequestKeys.TAB);

		try {
			if (tab.equalsIgnoreCase(PortletRequestKeys.CURRENT_PASSWORD)) {
				myDetailsService.validatePassword(portal.getUserId(actionRequest), currentPassword);
				actionRequest.setAttribute(PortletRequestKeys.TAB,PortletRequestKeys.NEW_PASSWORD);
				actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PASSWORD_RENDER);

			}else{
				myDetailsService.updatePassword(portal.getUserId(actionRequest), password, confirmPassword);

				AuthenticatedSessionManagerUtil.logout(portal.getOriginalServletRequest(portal.getHttpServletRequest(actionRequest)), portal.getHttpServletResponse(actionResponse));

				redirectToLoginPage(actionResponse);
			}
		} catch (Exception e) {
			log.debug(e);
			actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PASSWORD_RENDER);
			actionRequest.setAttribute(PortletRequestKeys.TAB,tab);
			actionRequest.setAttribute(PortletRequestKeys.ERROR_EXCEPTION, e);
			SessionErrors.add(actionRequest, e.getClass(), e);
			hideDefaultErrorMessage(actionRequest);
		}
	}

	private void redirectToLoginPage(ActionResponse actionResponse) throws IOException {
		actionResponse.sendRedirect(portal.getPathMain() + "/portal/login");
	}

}
