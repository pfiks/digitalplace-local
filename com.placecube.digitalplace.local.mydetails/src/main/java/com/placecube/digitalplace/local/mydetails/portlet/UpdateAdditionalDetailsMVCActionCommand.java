package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "mvc.command.name=" + MVCCommandKeys.UPDATE_ADDITIONAL_DETAILS }, service = MVCActionCommand.class)
public class UpdateAdditionalDetailsMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AccountValidationService accountValidationService;

	@Reference
	private MyDetailsService myDetailsService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		AccountContext accountContext = accountContextService.getOrCreateContext(actionRequest);
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext);

		accountContextService.configureAdditionalDetails(accountContext, actionRequest);
		accountContext.clearErrors();
		accountValidationService.validateAdditionalDetails(accountContext, actionRequest, accountConfiguration, userFieldsConfiguration);

		if (accountContext.hasErrors()) {
			actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_ADDITIONAL_DETAILS);
			hideDefaultErrorMessage(actionRequest);
		} else {
			myDetailsService.updateAdditionalDetails(accountContext, themeDisplay.getUser(), serviceContext, userFieldsConfiguration);
		}
	}
}