package com.placecube.digitalplace.local.mydetails.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsConfigurationConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = MyDetailsConfigurationConstants.PID, localization = "content/Language", name = "my-details-portlet")
public interface MyDetailsCompanyConfiguration {

	@Meta.AD(required = false, deflt = "true", name = "email-address-edit-enabled", description = "email-address-edit-enabled-description")
	boolean emailAddressEditEnabled();

	@Meta.AD(required = false, deflt = "true", name = "first-name-edit-enabled", description = "first-name-edit-enabled-description")
	boolean firstNameEditEnabled();

	@Meta.AD(required = false, deflt = "true", name = "known-as-edit-enabled", description = "known-as-edit-enabled-description")
	boolean knownAsEditEnabled();

	@Meta.AD(required = false, deflt = "true", name = "last-name-edit-enabled", description = "last-name-edit-enabled-description")
	boolean lastNameEditEnabled();

	@Meta.AD(required = false, deflt = "true", name = "password-edit-enabled", description = "password-edit-enabled-description")
	boolean passwordEditEnabled();

	@Meta.AD(required = false, deflt = "PASSWORD-POLICY", name = "password-policy-article-id", description = "password-policy-article-id-description")
	String passwordPolicyArticleId();

	@Meta.AD(required = false, deflt = "true", name = "user-address-edit-enabled", description = "user-address-edit-enabled-description")
	boolean userAddressEditEnabled();

}
