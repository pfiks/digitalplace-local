package com.placecube.digitalplace.local.mydetails.constants;

public enum WebContentArticles {

	MY_DETAILS_TWO_FACTOR_AUTHENTICATION("MY-DETAILS-TWO-FACTOR-AUTHENTICATION", "My details - Two factor authentication guidance");

	private final String articleId;
	private final String articleTitle;

	private WebContentArticles(String articleId, String articleTitle) {
		this.articleId = articleId;
		this.articleTitle = articleTitle;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

}
