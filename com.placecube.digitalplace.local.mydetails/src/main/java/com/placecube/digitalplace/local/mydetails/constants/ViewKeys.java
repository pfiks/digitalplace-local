package com.placecube.digitalplace.local.mydetails.constants;

public final class ViewKeys {

	public static final String ADDITIONAL_DETAILS = "/additional_details.jsp";

	public static final String PASSWORD = "/password.jsp";

	public static final String PERSONAL_DETAILS = "/personal_details.jsp";

	public static final String PLEASE_LOG_IN = "/please_login.jsp";
	
	public static final String TWO_FACTOR_AUTHENTICATION = "/two_factor_authentication.jsp";

	public static final String UPDATE_PERSONAL_DETAILS = "/update_personal_details.jsp";

	private ViewKeys() {

	}
}
