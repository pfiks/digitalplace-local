package com.placecube.digitalplace.local.mydetails.constants;

public final class MVCCommandKeys {

	public static final String UPDATE_ADDITIONAL_DETAILS = "/my-details/update-additional-details";

	public static final String UPDATE_PASSWORD_ACTION = "/my-details/update-password-action";

	public static final String UPDATE_PASSWORD_RENDER = "/my-details/update-password-render";

	public static final String UPDATE_PERSONAL_DETAILS_ACTION = "/my-details/update-personal-details-action";

	public static final String UPDATE_PERSONAL_DETAILS_RENDER = "/my-details/update-personal-details-render";

	public static final String UPDATE_TWO_FACTOR_AUTHENTICATION_ACTION = "/my-details/update-two-factor-authentication-action";

	public static final String UPDATE_TWO_FACTOR_AUTHENTICATION_RENDER = "/my-details/update-two-factor-authentication-render";
	
	public static final String VIEW_MY_DETAIL = "/my-details/view";

	private MVCCommandKeys() {
	}
}
