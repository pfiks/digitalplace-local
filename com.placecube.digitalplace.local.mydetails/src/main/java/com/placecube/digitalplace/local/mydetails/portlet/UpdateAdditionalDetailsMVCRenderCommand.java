package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.ViewKeys;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "mvc.command.name=" + MVCCommandKeys.UPDATE_ADDITIONAL_DETAILS }, service = MVCRenderCommand.class)
public class UpdateAdditionalDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			String backURL = ParamUtil.getString(renderRequest, PortletRequestKeys.BACK_URL);
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);
			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

			renderRequest.setAttribute(PortletRequestKeys.USER_PROFILE_CONFIG, userFieldsConfiguration);
			renderRequest.setAttribute(PortletRequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(PortletRequestKeys.ADDITONAL_DETAILS_EXPANDO_FIELDS, accountConfigurationService.getConfiguredExpandoFields(accountContext, userFieldsConfiguration));
			renderRequest.setAttribute(PortletRequestKeys.BACK_URL, backURL);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}

		return ViewKeys.ADDITIONAL_DETAILS;
	}

}