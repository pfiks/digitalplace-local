package com.placecube.digitalplace.local.mydetails.service;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;

@Component(immediate = true, service = MyDetailsContextService.class)
public class MyDetailsContextService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public void configurePersonalDetails(MyDetailsContext myDetailsContext, PortletRequest portletRequest) {
		myDetailsContext.setFirstName(ParamUtil.getString(portletRequest, PortletRequestKeys.FIRST_NAME));
		myDetailsContext.setLastName(ParamUtil.getString(portletRequest, PortletRequestKeys.LAST_NAME));
		myDetailsContext.setKnownAs(ParamUtil.getString(portletRequest, KnownAsConstants.KNOWN_AS));
		myDetailsContext.setEmailAddress(ParamUtil.getString(portletRequest, PortletRequestKeys.EMAIL_ADDRESS));

		String uprn = ParamUtil.getString(portletRequest, PortletRequestKeys.UPRN);
		String postcode = ParamUtil.getString(portletRequest, PortletRequestKeys.POSTCODE);
		String fullAddress = ParamUtil.getString(portletRequest, PortletRequestKeys.FULL_ADDRESS);

		if (Validator.isNotNull(uprn) && Validator.isNotNull(postcode) && Validator.isNotNull(fullAddress)) {
			myDetailsContext.setUprn(uprn);
			myDetailsContext.setPostcode(postcode);
			myDetailsContext.setFullAddress(fullAddress);
		}
	}

	public MyDetailsContext getMyDetailsContext() {
		return new MyDetailsContext();
	}

	public MyDetailsContext getMyDetailsContextWithCompanyConfigurationSet(long companyId) throws ConfigurationException {
		MyDetailsContext myDetailsContext = new MyDetailsContext();
		MyDetailsCompanyConfiguration myDetailsCompanyConfiguration = configurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId);
		myDetailsContext.setMyDetailsCompanyConfiguration(myDetailsCompanyConfiguration);
		return myDetailsContext;
	}

}
