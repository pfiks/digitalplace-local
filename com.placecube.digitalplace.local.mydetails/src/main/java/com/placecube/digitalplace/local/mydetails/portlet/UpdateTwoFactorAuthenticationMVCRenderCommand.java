package com.placecube.digitalplace.local.mydetails.portlet;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsConstants;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.ViewKeys;
import com.placecube.digitalplace.local.mydetails.constants.WebContentArticles;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_TWO_FACTOR_AUTHENTICATION_RENDER }, service = MVCRenderCommand.class)
public class UpdateTwoFactorAuthenticationMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private MyDetailsContextService myDetailsContextService;

	@Reference
	private MyDetailsService myDetailsService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String backURL = ParamUtil.getString(renderRequest, PortletRequestKeys.BACK_URL);

		try {
			MyDetailsContext myDetailsContext = myDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(themeDisplay.getCompanyId());
			myDetailsService.populateMyDetailsContext(renderRequest, myDetailsContext, themeDisplay.getUser());
			renderRequest.setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, myDetailsContext);

		} catch (PortalException e) {
			throw new PortletException(e);
		}

		renderRequest.setAttribute(PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED_OPTIONS, getTwoFactorAuthenticationEnabledOptions());
		renderRequest.setAttribute(PortletRequestKeys.BACK_URL, backURL);
		renderRequest.setAttribute(PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ARTICLE_EXISTS, Validator.isNotNull(journalArticleLocalService.fetchArticle(themeDisplay.getCompanyGroupId(), WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION.getArticleId()) ));

		return ViewKeys.TWO_FACTOR_AUTHENTICATION;
	}

	private Map<String, String> getTwoFactorAuthenticationEnabledOptions() {
		Map<String, String> twoFactorAuthenticationEnabledOptions = new LinkedHashMap<>();

		twoFactorAuthenticationEnabledOptions.put(Boolean.TRUE.toString(), MyDetailsConstants.I_WANT_TO_TURN_ON_2FA_LABEL);
		twoFactorAuthenticationEnabledOptions.put(Boolean.FALSE.toString(), MyDetailsConstants.I_DONT_WANT_TO_TURN_ON_2FA_LABEL);

		return twoFactorAuthenticationEnabledOptions;

	}

}
