package com.placecube.digitalplace.local.mydetails.constants;

public final class MyDetailsConfigurationConstants {

	public static final String PID = "com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration";

	private MyDetailsConfigurationConstants() {

	}

}
