package com.placecube.digitalplace.local.mydetails.service;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.UserPasswordException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.security.auth.Authenticator;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;
import com.placecube.digitalplace.user.account.constants.PhoneType;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;
import com.placecube.digitalplace.user.account.service.AccountUserFieldsService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

@Component(immediate = true, service = MyDetailsService.class)
public class MyDetailsService {

	private static final Log LOG = LogFactoryUtil.getLog(MyDetailsService.class);

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountPhoneService accountPhoneService;

	@Reference
	private AccountUserFieldsService accountUserFieldsService;

	@Reference
	private AddressLookupService addressLookupService;

	@Reference
	private AddressTypeConfigurationService addressTypeConfigurationService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private Portal portal;

	@Reference
	private UserGroupRoleLocalService userGroupRoleLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private UserProfileFieldsService userProfileFieldsService;

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	public boolean checkAdditionalDetailsSectionAvailable(AccountContext accountContext, UserProfileFieldsConfiguration configuration) {
		return configuration.homePhoneNumberEnabled() || configuration.mobilePhoneNumberEnabled() || configuration.jobTitleEnabled() || configuration.businessPhoneNumberEnabled()
				|| !accountConfigurationService.getConfiguredExpandoFields(accountContext, configuration).isEmpty();
	}

	public boolean checkUserSignedIn(PortletRequest portletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		return themeDisplay.isSignedIn();
	}

	public void populateAccountContextAdditionalDetails(AccountContext accountContext, User user, UserProfileFieldsConfiguration userFieldsConfiguration) throws PortalException {
		accountPhoneService.populateContextWithExistingPhones(accountContext, user);
		accountContext.setJobTitle(user.getJobTitle());
		accountUserFieldsService.populateContextWithExistingConfiguredExpandoFields(accountContext, user, userFieldsConfiguration);
	}

	public void populateMyDetailsContext(PortletRequest portletRequest, MyDetailsContext myDetailsContext, User user) throws PortletException {
		myDetailsContext.setFirstName(user.getFirstName());
		myDetailsContext.setLastName(user.getLastName());
		myDetailsContext.setEmailAddress(user.getEmailAddress());
		myDetailsContext.setKnownAs(GetterUtil.getString(user.getExpandoBridge().getAttribute(KnownAsConstants.KNOWN_AS, false)));
		myDetailsContext.setOldPassword(user.getPassword());
		myDetailsContext.setTwoFactorAuthenticationEnabled(getUserTwoFactorAuthenticationEnabled(user));
		myDetailsContext.setTwoFactorAuthenticationAvailable(twoFactorAuthenticationAvailable(portletRequest));

		List<Address> addressList = user.getAddresses();

		for (Address address : addressList) {
			if (address.isPrimary()) {
				myDetailsContext.setAddress(address);
			}
		}
	}

	public void updateAdditionalDetails(AccountContext accountContext, User user, ServiceContext serviceContext, UserProfileFieldsConfiguration userFieldsConfiguration) throws PortalException {

		if (userFieldsConfiguration.jobTitleEnabled()) {
			updateJobTitle(user.getUserId(), accountContext.getJobTitle());
		}

		updateExpandoFields(accountContext, user);

		if (userFieldsConfiguration.businessPhoneNumberEnabled()) {
			updatePhoneNumber(accountContext.getBusinessPhoneNumber(), PhoneType.BUSINESS, user, serviceContext);
		}

		if (userFieldsConfiguration.mobilePhoneNumberEnabled()) {
			updatePhoneNumber(accountContext.getMobilePhoneNumber(), PhoneType.MOBILE, user, serviceContext);
		}

		if (userFieldsConfiguration.homePhoneNumberEnabled()) {
			updatePhoneNumber(accountContext.getHomePhoneNumber(), PhoneType.PERSONAL, user, serviceContext);
		}
	}

	public void updateJobTitle(long userId, String jobTitle) throws PortalException {
		try {
			userLocalService.updateJobTitle(userId, jobTitle);
		} catch (PortalException e) {
			LOG.error(e);
			throw new PortalException(e);
		}
	}

	public void updateMyDetails(PortletRequest portletRequest, MyDetailsContext myDetailsContext) throws PortalException {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		User user = portal.getUser(portletRequest);

		MyDetailsCompanyConfiguration myDetailsCompanyConfiguration = configurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, user.getCompanyId());
		String firstName = myDetailsCompanyConfiguration.firstNameEditEnabled() ? myDetailsContext.getFirstName() : user.getFirstName();
		String lastName = myDetailsCompanyConfiguration.lastNameEditEnabled() ? myDetailsContext.getLastName() : user.getLastName();
		String emailAddress = myDetailsCompanyConfiguration.emailAddressEditEnabled() ? myDetailsContext.getEmailAddress() : user.getEmailAddress();

		String oldEmailAddress = user.getEmailAddress();
		String oldPassword = user.getPassword();

		Contact contact = user.getContact();

		boolean useExistingPortrait = true;
		byte[] portraitBytes = null;

		Calendar birthdayCal = CalendarFactoryUtil.getCalendar();
		birthdayCal.setTime(contact.getBirthday());

		List<UserGroupRole> userGroupRoles = userGroupRoleLocalService.getUserGroupRoles(user.getUserId());

		updateKnownAs(user, myDetailsContext, myDetailsCompanyConfiguration);
		updateAddress(user, myDetailsContext, portletRequest);

		user = userLocalService.updateUser(user.getUserId(), oldPassword, myDetailsContext.getPassword1(), myDetailsContext.getPassword2(), user.getPasswordReset(), user.getReminderQueryQuestion(),
				user.getReminderQueryAnswer(), user.getScreenName(), emailAddress, useExistingPortrait, portraitBytes, user.getLanguageId(), user.getTimeZoneId(), user.getGreeting(),
				user.getComments(), firstName, user.getMiddleName(), lastName, contact.getPrefixListTypeId(), contact.getSuffixListTypeId(), user.getMale(), birthdayCal.get(Calendar.MONTH),
				birthdayCal.get(Calendar.DATE), birthdayCal.get(Calendar.YEAR), contact.getSmsSn(), contact.getFacebookSn(), contact.getJabberSn(), contact.getSkypeSn(), contact.getTwitterSn(),
				user.getJobTitle(), user.getGroupIds(), user.getOrganizationIds(), user.getRoleIds(), userGroupRoles, user.getUserGroupIds(), serviceContext);

		portal.getHttpServletRequest(portletRequest).setAttribute(WebKeys.USER, user);
		Company company = portal.getCompany(portletRequest);

		if (company.isStrangersVerify() && !StringUtil.equalsIgnoreCase(oldEmailAddress, emailAddress)) {

			SessionMessages.add(portletRequest, "verificationEmailSent");
		}
	}

	public User updatePassword(long userId, String password, String confirmPassword) throws PortalException {
		return userLocalService.updatePassword(userId, password, confirmPassword, false);
	}

	public void updateTwoFactorAuthenticationForUser(PortletRequest portletRequest, boolean twoFactorAuthenticationEnabled) throws PortalException {
		User user = portal.getUser(portletRequest);

		userTwoFactorAuthenticationLocalService.enableAndGetOrCreateTwoFactorAuthenticationForUser(user, twoFactorAuthenticationEnabled);
	}

	public void validatePassword(long userId, String currentPassword) throws PortalException {

		User user = userLocalService.getUserById(userId);
		if (Validator.isNull(user)) {
			throw new PortalException();
		}

		Company company = companyLocalService.getCompany(user.getCompanyId());

		String authType = company.getAuthType();

		Map<String, String[]> headerMap = new HashMap<>();
		Map<String, String[]> parameterMap = new HashMap<>();
		Map<String, Object> resultsMap = new HashMap<>();

		int authResult = Authenticator.FAILURE;

		if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
			authResult = userLocalService.authenticateByEmailAddress(company.getCompanyId(), user.getEmailAddress(), currentPassword, headerMap, parameterMap, resultsMap);
		}

		if (authResult == Authenticator.FAILURE) {
			throw new UserPasswordException.MustMatchCurrentPassword(user.getUserId());
		}
	}

	private boolean getUserTwoFactorAuthenticationEnabled(User user) {
		Optional<UserTwoFactorAuthentication> userTwoFactorAuthentication = userTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(user);

		return userTwoFactorAuthentication.isPresent() && userTwoFactorAuthentication.get().getEnabled();
	}

	private boolean twoFactorAuthenticationAvailable(PortletRequest portletRequest) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			UserProfileFieldsConfiguration userProfileFieldsCompanyConfiguration = userProfileFieldsService.getCompanyUserProfileFieldsConfiguration(themeDisplay.getCompanyId());

			return userProfileFieldsCompanyConfiguration.twoFactorAuthenticationPromptEnabled();

		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

	private void updateAddress(User user, MyDetailsContext myDetailsContext, PortletRequest portletRequest) throws PortalException {
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(portletRequest);

			Optional<AddressContext> addressContextOpt = addressLookupService.getByUprn(serviceContext.getCompanyId(), myDetailsContext.getUprn(), new String[0], false);

			if (addressContextOpt.isPresent()) {
				AddressContext addressContext = addressContextOpt.get();
				addressContext.setAddressType(addressTypeConfigurationService.getDefaultAddressType(serviceContext.getCompanyId()));
				addressLookupService.saveAddress(user, addressContext, serviceContext);
			}
		} catch (Exception e) {
			LOG.error("Exception configuring user address - " + e.getMessage());
			throw new PortalException(e);
		}
	}

	private void updateExpandoFields(AccountContext accountContext, User user) throws PortalException {
		try {
			ExpandoBridge expandoBridge = user.getExpandoBridge();
			for (Entry<String, Serializable> expandoFieldToSave : accountContext.getExpandoFields().entrySet()) {
				expandoBridge.setAttribute(expandoFieldToSave.getKey(), expandoFieldToSave.getValue(), false);
			}
		} catch (Exception e) {
			LOG.warn("Exception configuring user expando fields - " + e.getMessage());
			throw new PortalException(e);
		}
	}

	private void updateKnownAs(User user, MyDetailsContext myDetailsContext, MyDetailsCompanyConfiguration myDetailsCompanyConfiguration) {
		if (myDetailsCompanyConfiguration.knownAsEditEnabled()) {
			user.getExpandoBridge().setAttribute(KnownAsConstants.KNOWN_AS, myDetailsContext.getKnownAs());
		}
	}

	private void updatePhoneNumber(String phoneNumber, PhoneType type, User user, ServiceContext serviceContext) throws PortalException {
		try {
			accountPhoneService.addPhone(user, phoneNumber, type, serviceContext);
		} catch (Exception e) {
			LOG.warn("Exception configuring user phone number - " + e.getMessage());
			throw new PortalException(e);
		}
	}
}
