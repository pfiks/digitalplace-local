package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.ViewKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS,
		"mvc.command.name=" + MVCCommandKeys.UPDATE_PERSONAL_DETAILS_RENDER }, service = MVCRenderCommand.class)
public class UpdatePersonalDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private MyDetailsContextService myDetailsContextService;

	@Reference
	private MyDetailsService myDetailsService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String backURL = ParamUtil.getString(renderRequest, PortletRequestKeys.BACK_URL);

		try {
			MyDetailsContext myDetailsContext = myDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(themeDisplay.getCompanyId());
			myDetailsService.populateMyDetailsContext(renderRequest, myDetailsContext, themeDisplay.getUser());
			renderRequest.setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, myDetailsContext);
		} catch (PortalException e) {
			throw new PortletException(e);
		}

		renderRequest.setAttribute(PortletRequestKeys.BACK_URL, backURL);

		return ViewKeys.UPDATE_PERSONAL_DETAILS;
	}

}
