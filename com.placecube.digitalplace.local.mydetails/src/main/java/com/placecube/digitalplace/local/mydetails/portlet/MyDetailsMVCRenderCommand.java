package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.ViewKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "mvc.command.name=/",
		"mvc.command.name=" + MVCCommandKeys.VIEW_MY_DETAIL }, service = MVCRenderCommand.class)
public class MyDetailsMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(MyDetailsMVCRenderCommand.class);

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private MyDetailsContextService myDetailsContextService;

	@Reference
	private MyDetailsService myDetailsService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		boolean isUserSignedIn = myDetailsService.checkUserSignedIn(renderRequest);
		User user = userLocalService.fetchUser(themeDisplay.getUserId());

		if (!isUserSignedIn) {
			return ViewKeys.PLEASE_LOG_IN;
		}

		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);
			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(themeDisplay.getCompanyId());
			boolean isAdditionalDetailsSectionAvailable = myDetailsService.checkAdditionalDetailsSectionAvailable(accountContext, userFieldsConfiguration);
			renderRequest.setAttribute(PortletRequestKeys.IS_ADDITONAL_DETAILS_SECTION_AVAILABLE, isAdditionalDetailsSectionAvailable);
			MyDetailsContext myDetailsContext = myDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(themeDisplay.getCompanyId());
			myDetailsService.populateMyDetailsContext(renderRequest, myDetailsContext, user);
			renderRequest.setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, myDetailsContext);

			if (isAdditionalDetailsSectionAvailable) {
				myDetailsService.populateAccountContextAdditionalDetails(accountContext, user, userFieldsConfiguration);

				renderRequest.setAttribute(PortletRequestKeys.USER_PROFILE_CONFIG, userFieldsConfiguration);
				renderRequest.setAttribute(PortletRequestKeys.ACCOUNT_CONTEXT, accountContext);
				renderRequest.setAttribute(PortletRequestKeys.ADDITONAL_DETAILS_EXPANDO_FIELDS, accountConfigurationService.getConfiguredExpandoFields(accountContext, userFieldsConfiguration));
			}
		} catch (PortalException | UserAccountException e) {
			LOG.error("Error setting context " + e.getMessage());
			LOG.debug(e);
			throw new PortletException(e);
		}
		return ViewKeys.PERSONAL_DETAILS;
	}

}
