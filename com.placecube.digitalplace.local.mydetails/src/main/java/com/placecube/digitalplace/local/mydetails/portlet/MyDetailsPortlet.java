package com.placecube.digitalplace.local.mydetails.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.tools", "com.liferay.portlet.css-class-wrapper=my-details",
		"com.liferay.portlet.instanceable=false", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/personal_details.jsp",
		"javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class MyDetailsPortlet extends MVCPortlet {
}