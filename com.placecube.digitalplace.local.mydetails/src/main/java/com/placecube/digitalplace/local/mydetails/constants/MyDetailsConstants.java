package com.placecube.digitalplace.local.mydetails.constants;

public class MyDetailsConstants {

	public static final String I_WANT_TO_TURN_ON_2FA_LABEL = "yes-I-want-to-turn-on-2fa";

	public static final String I_DONT_WANT_TO_TURN_ON_2FA_LABEL = "no-I-dont-want-to-turn-on-2fa";

	private MyDetailsConstants() {

	}
}
