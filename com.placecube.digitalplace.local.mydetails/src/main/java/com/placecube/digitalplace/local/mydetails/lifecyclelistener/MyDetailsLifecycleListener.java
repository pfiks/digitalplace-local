package com.placecube.digitalplace.local.mydetails.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.workflow.WorkflowThreadLocal;
import com.placecube.digitalplace.local.mydetails.constants.WebContentArticles;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsSetupService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class MyDetailsLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(MyDetailsLifecycleListener.class);

	@Reference
	private MyDetailsSetupService myDetailsSetupService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		long companyId = company.getCompanyId();
		LOG.debug("Initialising My details portlet for companyId: " + companyId);

		boolean workflowEnabled = WorkflowThreadLocal.isEnabled();

		try {
			WorkflowThreadLocal.setEnabled(false);
			Group globalGroup = company.getGroup();
			ServiceContext serviceContext = myDetailsSetupService.getServiceContext(globalGroup);

			JournalFolder journalFolder = myDetailsSetupService.addFolder(serviceContext);
			myDetailsSetupService.addArticle(WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION, journalFolder, serviceContext);
		} finally {
			WorkflowThreadLocal.setEnabled(workflowEnabled);
		}
		LOG.debug("Configuration finished for My details portlet for companyId: " + companyId);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Waits until the portal has finished initializing. No code needed.
	}

}
