package com.placecube.digitalplace.local.mydetails.portlet;


import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsPortletKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyDetailsPortletKeys.MY_DETAILS, "mvc.command.name=" + MVCCommandKeys.UPDATE_TWO_FACTOR_AUTHENTICATION_ACTION }, service = MVCActionCommand.class)
public class UpdateTwoFactorAuthenticationMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private MyDetailsService myDetailsService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		boolean twoFactorAuthenticationEnabled = ParamUtil.getBoolean(actionRequest, PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED);

		myDetailsService.updateTwoFactorAuthenticationForUser(actionRequest, twoFactorAuthenticationEnabled);

	}

}
