package com.placecube.digitalplace.local.mydetails.constants;

public final class PortletRequestKeys {

	public static final String ACCOUNT_CONFIG = "accountConfiguration";

	public static final String ACCOUNT_CONTEXT = "accountContext";

	public static final String ADDITONAL_DETAILS_EXPANDO_FIELDS = "additionalDetailsExpandoFields";

	public static final String BACK_URL = "backURL";

	public static final String CONFIRM_PASSWORD = "confirmPassword";

	public static final String CURRENT_PASSWORD = "currentPassword";

	public static final String EMAIL_ADDRESS = "emailAddress";

	public static final String ERROR_EXCEPTION = "errorExceptionFromRequest";

	public static final String FIRST_NAME = "firstName";

	public static final String FULL_ADDRESS = "fullAddress";

	public static final String IS_ADDITONAL_DETAILS_SECTION_AVAILABLE = "isAdditionalDetailsSectionAvailable";

	public static final String IS_PASSWORD_POLICY_ARTICLE_PRESENT = "isPasswordPolicyArticlePresent";

	public static final String IS_USER_SIGNED_IN = "isUserSignedIn";

	public static final String LAST_NAME = "lastName";

	public static final String MY_DETAILS_CONTEXT = "myDetailsContext";

	public static final String NEW_PASSWORD = "newPassword";

	public static final String PASSWORD = "password";

	public static final String PASSWORD_MODIFIED_FILTER = "passwordModifiedFilter";

	public static final String PASSWORD_POLICY_DESCRIPTION = "passwordPolicyDescription";

	public static final String POSTCODE = "postcode";

	public static final String TAB = "tab";

	public static final String TWO_FACTOR_AUTHENTICATION_ARTICLE_EXISTS = "twoFactorAuthenticationArticleExist";

	public static final String TWO_FACTOR_AUTHENTICATION_ENABLED = "twoFactorAuthenticationEnabled";

	public static final String TWO_FACTOR_AUTHENTICATION_ENABLED_OPTIONS = "twoFactorAuthenticationEnabledOptions";

	public static final String UPRN = "uprn";

	public static final String USER_PROFILE_CONFIG = "userProfileConfiguration";

	private PortletRequestKeys() {

	}
}
