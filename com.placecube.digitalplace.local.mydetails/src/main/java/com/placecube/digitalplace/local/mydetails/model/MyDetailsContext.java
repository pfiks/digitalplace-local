package com.placecube.digitalplace.local.mydetails.model;

import java.io.Serializable;

import com.liferay.portal.kernel.model.Address;
import com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration;

@SuppressWarnings("serial")
public class MyDetailsContext implements Serializable {

	private boolean twoFactorAuthenticationAvailable;
	private Address address;
	private String emailAddress;
	private String firstName;
	private String fullAddress;
	private String knownAs;
	private String lastName;
	private transient MyDetailsCompanyConfiguration myDetailsCompanyConfiguration;
	private String oldPassword;

	private String password1;

	private String password2;

	private String postcode;

	private boolean twoFactorAuthenticationEnabled;

	private String uprn;

	public Address getAddress() {
		return address;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public String getKnownAs() {
		return knownAs;
	}

	public String getLastName() {
		return lastName;
	}

	public MyDetailsCompanyConfiguration getMyDetailsCompanyConfiguration() {
		return myDetailsCompanyConfiguration;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public String getPassword1() {
		return password1;
	}

	public String getPassword2() {
		return password2;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getUprn() {
		return uprn;
	}

	public boolean isTwoFactorAuthenticationAvailable() {
		return twoFactorAuthenticationAvailable;
	}

	public boolean isTwoFactorAuthenticationEnabled() {
		return twoFactorAuthenticationEnabled;
	}

	public void setTwoFactorAuthenticationAvailable(boolean twoFactorAuthenticationAvailable) {
		this.twoFactorAuthenticationAvailable = twoFactorAuthenticationAvailable;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public void setKnownAs(String knownAs) {
		this.knownAs = knownAs;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMyDetailsCompanyConfiguration(MyDetailsCompanyConfiguration myDetailsCompanyConfiguration) {
		this.myDetailsCompanyConfiguration = myDetailsCompanyConfiguration;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setTwoFactorAuthenticationEnabled(boolean twoFactorAuthenticationEnabled) {
		this.twoFactorAuthenticationEnabled = twoFactorAuthenticationEnabled;
	}

	public void setUprn(String uprn) {
		this.uprn = uprn;
	}
}
