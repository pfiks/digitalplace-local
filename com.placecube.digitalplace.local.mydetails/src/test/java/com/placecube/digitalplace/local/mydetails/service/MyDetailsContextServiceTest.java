package com.placecube.digitalplace.local.mydetails.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class MyDetailsContextServiceTest extends PowerMockito {

	private static final String EMAIL = "emailValue";

	private static final String FIRSTNAME = "firstNameValue";

	private static final String FULL_ADDRESS = "fullAddress";

	private static final String KNOWN_AS = "knownAsValue";

	private static final String LASTNAME = "lastNameValue";

	private static final String POSTCODE = "postcode";

	private static final String UPRN = "uprn";

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private MyDetailsCompanyConfiguration mockMyDetailsCompanyConfiguration;

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private PortletRequest mockPortletRequest;

	@InjectMocks
	private MyDetailsContextService myDetailsContextService;

	@Before
	public void activeSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void configurePersonalDetails_WhenAddressParametersAreNotSet_ThenAddressDataIsNotConfigured() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.FIRST_NAME)).thenReturn(FIRSTNAME);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.LAST_NAME)).thenReturn(LASTNAME);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.EMAIL_ADDRESS)).thenReturn(EMAIL);

		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.UPRN)).thenReturn(null);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.POSTCODE)).thenReturn(null);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.FULL_ADDRESS)).thenReturn(null);

		myDetailsContextService.configurePersonalDetails(mockMyDetailsContext, mockPortletRequest);

		verify(mockMyDetailsContext, never()).setUprn(UPRN);
		verify(mockMyDetailsContext, never()).setPostcode(POSTCODE);
		verify(mockMyDetailsContext, never()).setFullAddress(FULL_ADDRESS);
	}

	@Test
	public void configurePersonalDetails_WhenAddressParametersAreSet_ThenAddressDataIsConfigured() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.FIRST_NAME)).thenReturn(FIRSTNAME);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.LAST_NAME)).thenReturn(LASTNAME);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.EMAIL_ADDRESS)).thenReturn(EMAIL);

		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.UPRN)).thenReturn(UPRN);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.POSTCODE)).thenReturn(POSTCODE);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.FULL_ADDRESS)).thenReturn(FULL_ADDRESS);

		myDetailsContextService.configurePersonalDetails(mockMyDetailsContext, mockPortletRequest);

		verify(mockMyDetailsContext, times(1)).setUprn(UPRN);
		verify(mockMyDetailsContext, times(1)).setPostcode(POSTCODE);
		verify(mockMyDetailsContext, times(1)).setFullAddress(FULL_ADDRESS);
	}

	@Test
	public void configurePersonalDetails_WhenNoError_ThenConfiguresTheRequestFields() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.FIRST_NAME)).thenReturn(FIRSTNAME);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.LAST_NAME)).thenReturn(LASTNAME);
		when(ParamUtil.getString(mockPortletRequest, KnownAsConstants.KNOWN_AS)).thenReturn(KNOWN_AS);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.EMAIL_ADDRESS)).thenReturn(EMAIL);

		myDetailsContextService.configurePersonalDetails(mockMyDetailsContext, mockPortletRequest);

		verify(mockMyDetailsContext, times(1)).setFirstName(FIRSTNAME);
		verify(mockMyDetailsContext, times(1)).setLastName(LASTNAME);
		verify(mockMyDetailsContext, times(1)).setKnownAs(KNOWN_AS);
		verify(mockMyDetailsContext, times(1)).setEmailAddress(EMAIL);
	}

	@Test
	public void getMyDetailsContext_WhenContextNotFoundInSession_ThenReturnsANewContext() {
		MyDetailsContext result = myDetailsContextService.getMyDetailsContext();
		assertNotNull(result);
	}

	@Test(expected = ConfigurationException.class)
	public void getMyDetailsContextWithCompanyConfigurationSet_WhenExceptionGettingConfiguration_ThenThrowConfigurationException() throws ConfigurationException {
		long companyId = 123;

		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		myDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId);
	}

	@Test
	public void getMyDetailsContextWithCompanyConfigurationSet_WhenNoError_ThenReturnsANewContextWithConfigurationFieldSet() throws ConfigurationException {
		long companyId = 123;

		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);

		MyDetailsContext result = myDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId);

		assertThat(result.getMyDetailsCompanyConfiguration(), equalTo(mockMyDetailsCompanyConfiguration));
	}

}
