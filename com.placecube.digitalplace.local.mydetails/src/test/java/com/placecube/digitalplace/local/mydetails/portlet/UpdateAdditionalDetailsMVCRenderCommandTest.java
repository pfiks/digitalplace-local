package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashSet;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class UpdateAdditionalDetailsMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private UserProfileFieldsConfiguration mockUserFieldsConfiguration;

	@InjectMocks
	private UpdateAdditionalDetailsMVCRenderCommand updateAdditionalDetailsMVCRenderCommand;

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowPortletException() throws UserAccountException, PortletException {

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(UserAccountException.class);

		updateAdditionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenSetContextInAttributes() throws PortletException, UserAccountException {
		String backUrl = "url";
		Set<ExpandoField> expandoFields = new HashSet<>();

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserFieldsConfiguration);
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserFieldsConfiguration)).thenReturn(expandoFields);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.BACK_URL)).thenReturn(backUrl);

		updateAdditionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.USER_PROFILE_CONFIG, mockUserFieldsConfiguration);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.ADDITONAL_DETAILS_EXPANDO_FIELDS, expandoFields);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.BACK_URL, backUrl);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		initMocks(this);
	}

}
