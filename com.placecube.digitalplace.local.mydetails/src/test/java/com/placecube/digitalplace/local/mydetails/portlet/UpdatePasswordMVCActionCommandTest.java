package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.security.auth.session.AuthenticatedSessionManagerUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@PrepareForTest({ AuthenticatedSessionManagerUtil.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class, SessionMessages.class, HttpComponentsUtil.class })
@RunWith(PowerMockRunner.class)
public class UpdatePasswordMVCActionCommandTest extends PowerMockito {

	private static final String CONFIRM_PASSWORD = "confirmpassword";

	private static final String CURRENT_PASSWORD = "current password";

	private static final String CURRENT_URL = "/my-details";

	private static final String PASSWORD = "password";

	private static final String PATH_MAIN = "/c";

	private static final String TAB = "currentPassword";

	private static final String TAB_NEW = "newPassword";

	private static final long USER_ID = 34534L;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletRequest mockHttpServletRequestOriginal;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private Portal mockPortal;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@InjectMocks
	private UpdatePasswordMVCActionCommand updatePasswordMVCActionCommand;

	@Test
	public void doProcessAction_WhenCurrentPasswordCheckFails_ThenAddExceptionToSessionErrorsAndDisplayEditPasswordPage() throws Exception {
		mockRequest();

		PortalException exception = new PortalException();

		doThrow(exception).when(mockMyDetailsService).validatePassword(USER_ID, CURRENT_PASSWORD);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		updatePasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.ERROR_EXCEPTION, exception);
		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.TAB, TAB);
		verify(mockRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PASSWORD_RENDER);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, exception.getClass(), exception);
	}

	@Test
	public void doProcessAction_WhenLogoutFails_ThenAddExceptionToSessionErrorsAndDisplayEditPasswordPage() throws Exception {
		mockRequest();
		Exception exception = new Exception();
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequestOriginal);
		when(mockPortal.getHttpServletResponse(mockActionResponse)).thenReturn(mockHttpServletResponse);
		doThrow(exception).when(AuthenticatedSessionManagerUtil.class, "logout", mockHttpServletRequestOriginal, mockHttpServletResponse);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TAB)).thenReturn(TAB_NEW);

		updatePasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.ERROR_EXCEPTION, exception);
		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.TAB, TAB_NEW);
		verify(mockRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PASSWORD_RENDER);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, exception.getClass(), exception);
	}

	@Test
	public void doProcessAction_WhenNoErrors_ThenUpdatesPasswordLogsUserOutAndRedirectsToLoginPageInOrder() throws Exception {
		final String redirectUrl = PATH_MAIN + "/portal/login";

		mockRequest();
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TAB)).thenReturn(TAB_NEW);
		when(mockPortal.getPathMain()).thenReturn(PATH_MAIN);
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequestOriginal);
		when(mockPortal.getHttpServletResponse(mockActionResponse)).thenReturn(mockHttpServletResponse);

		updatePasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockMyDetailsService, mockActionResponse);
		inOrder.verify(mockMyDetailsService, times(1)).updatePassword(USER_ID, PASSWORD, CONFIRM_PASSWORD);
		PowerMockito.verifyStatic(AuthenticatedSessionManagerUtil.class, times(1));
		AuthenticatedSessionManagerUtil.logout(mockHttpServletRequestOriginal, mockHttpServletResponse);
		inOrder.verify(mockActionResponse, times(1)).sendRedirect(redirectUrl);
	}

	@Test
	public void doProcessAction_WhenPasswordUpdateFails_ThenAddExceptionToSessionErrorsAndDisplayEditPasswordPage() throws Exception {
		mockRequest();
		PortalException exception = new PortalException();
		when(mockMyDetailsService.updatePassword(USER_ID, PASSWORD, CONFIRM_PASSWORD)).thenThrow(exception);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TAB)).thenReturn(TAB_NEW);

		updatePasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.ERROR_EXCEPTION, exception);
		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.TAB, TAB_NEW);
		verify(mockRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PASSWORD_RENDER);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, exception.getClass(), exception);
	}

	@Before
	public void setUp() {
		mockStatic(AuthenticatedSessionManagerUtil.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class, SessionMessages.class, HttpComponentsUtil.class);
	}

	private void mockRequest() {
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.PASSWORD)).thenReturn(PASSWORD);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.CONFIRM_PASSWORD)).thenReturn(CONFIRM_PASSWORD);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.CURRENT_PASSWORD)).thenReturn(CURRENT_PASSWORD);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TAB)).thenReturn(TAB);
		when(mockPortal.getUserId(mockActionRequest)).thenReturn(USER_ID);
		when(mockPortal.getCurrentURL(mockActionRequest)).thenReturn(CURRENT_URL);
	}
}
