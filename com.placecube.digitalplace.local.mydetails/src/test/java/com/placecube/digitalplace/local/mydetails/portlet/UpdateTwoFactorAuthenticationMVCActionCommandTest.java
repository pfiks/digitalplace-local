package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
@RunWith(PowerMockRunner.class)
public class UpdateTwoFactorAuthenticationMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private UpdateTwoFactorAuthenticationMVCActionCommand updateTwoFactorAuthenticationMVCActionCommand;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoErrors_ThenUpdatesTwoFactorAuthenticationForUser(boolean twoFactorAuthenticationEnabled) throws Exception {
		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED)).thenReturn(twoFactorAuthenticationEnabled);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		updateTwoFactorAuthenticationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMyDetailsService, times(1)).updateTwoFactorAuthenticationForUser(mockActionRequest, twoFactorAuthenticationEnabled);
	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenTwoFactorAuthenticationUpdateFailsWithPortalException_ThenThrowsPortalException() throws Exception {
		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED)).thenReturn(true);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		doThrow(new PortalException()).when(mockMyDetailsService).updateTwoFactorAuthenticationForUser(mockActionRequest, true);

		updateTwoFactorAuthenticationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}
}
