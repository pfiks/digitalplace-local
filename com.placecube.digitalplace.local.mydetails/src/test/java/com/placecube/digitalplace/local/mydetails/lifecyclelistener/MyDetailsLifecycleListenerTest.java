package com.placecube.digitalplace.local.mydetails.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.workflow.WorkflowThreadLocal;
import com.placecube.digitalplace.local.mydetails.constants.WebContentArticles;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsSetupService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ WorkflowThreadLocal.class })
public class MyDetailsLifecycleListenerTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private Company mockCompany;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private MyDetailsSetupService mockMyDetailsSetupService;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private MyDetailsLifecycleListener myDetailsLifecycleListener;

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionAddingFolder_ThenThrowsPortalException() throws Exception {
		mockServiceContextDetails();
		when(mockMyDetailsSetupService.addFolder(mockServiceContext)).thenThrow(new PortalException());

		myDetailsLifecycleListener.portalInstanceRegistered(mockCompany);

	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionAddingMyDetails2FAArticle_ThenThrowsException() throws Exception {
		mockServiceContextDetails();

		when(mockMyDetailsSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);
		doThrow(new Exception()).when(mockMyDetailsSetupService).addArticle(WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION, mockJournalFolder, mockServiceContext);

		myDetailsLifecycleListener.portalInstanceRegistered(mockCompany);

	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionRetrievingGroupAndWorkflowIsAlreadyDisabled_ThenCreatesMyDetails2FAArticle() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenThrow(new PortalException());
		when(WorkflowThreadLocal.isEnabled()).thenReturn(false);

		myDetailsLifecycleListener.portalInstanceRegistered(mockCompany);

		verifyStatic(WorkflowThreadLocal.class, times(2));
		WorkflowThreadLocal.setEnabled(false);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionRetrievingGroupAndWorkflowIsEnabled_ThenCreatesMyDetails2FAArticle() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenThrow(new PortalException());
		when(WorkflowThreadLocal.isEnabled()).thenReturn(true);

		myDetailsLifecycleListener.portalInstanceRegistered(mockCompany);

		verifyStatic(WorkflowThreadLocal.class, times(1));
		WorkflowThreadLocal.setEnabled(false);

		verifyStatic(WorkflowThreadLocal.class, times(1));
		WorkflowThreadLocal.setEnabled(true);

	}

	@Test
	public void portalInstanceRegistered_WhenNoErrorAndWorkflowIsAlreadyDisabled_ThenCreatesMyDetails2FAArticle() throws Exception {
		mockServiceContextDetails();
		when(WorkflowThreadLocal.isEnabled()).thenReturn(false);
		when(mockMyDetailsSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		myDetailsLifecycleListener.portalInstanceRegistered(mockCompany);

		verifyStatic(WorkflowThreadLocal.class, times(2));
		WorkflowThreadLocal.setEnabled(false);

		verify(mockMyDetailsSetupService, times(1)).addArticle(WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoErrorAndWorkflowIsEnabled_ThenCreatesMyDetails2FAArticle() throws Exception {
		mockServiceContextDetails();
		when(WorkflowThreadLocal.isEnabled()).thenReturn(true);
		when(mockMyDetailsSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		myDetailsLifecycleListener.portalInstanceRegistered(mockCompany);

		verifyStatic(WorkflowThreadLocal.class, times(1));
		WorkflowThreadLocal.setEnabled(false);

		verifyStatic(WorkflowThreadLocal.class, times(1));
		WorkflowThreadLocal.setEnabled(true);

		verify(mockMyDetailsSetupService, times(1)).addArticle(WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION, mockJournalFolder, mockServiceContext);
	}

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(WorkflowThreadLocal.class);
	}

	private void mockServiceContextDetails() throws PortalException {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockMyDetailsSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

	}

}
