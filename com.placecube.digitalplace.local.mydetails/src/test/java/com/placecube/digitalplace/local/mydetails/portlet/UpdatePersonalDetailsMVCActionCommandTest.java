package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@PrepareForTest({ PortalUtil.class, SessionErrors.class, SessionMessages.class })
@RunWith(PowerMockRunner.class)
public class UpdatePersonalDetailsMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private UpdatePersonalDetailsMVCActionCommand updatePersonalDetailsMVCActionCommand;

	@Mock
	private MyDetailsContextService mockMyDetailsContextService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Before
	public void setUp() {
		mockStatic(PortalUtil.class, SessionErrors.class, SessionMessages.class);
	}


	@Test
	public void doProcessAction_WhenNoError_ThenUpdateMyDetailsAndDisplayViewMyDetailsPage() throws Exception {
		mockBasicDetails();
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		updatePersonalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockMyDetailsContextService, mockMyDetailsService, mockRenderParameters);
		inOrder.verify(mockMyDetailsContextService, times(1)).configurePersonalDetails(mockMyDetailsContext, mockActionRequest);
		inOrder.verify(mockMyDetailsService, times(1)).updateMyDetails(mockActionRequest, mockMyDetailsContext);
		inOrder.verify(mockRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_MY_DETAIL);
	}

	@Test
	public void doProcessAction_WhenDetailsUpdateFails_ThenAddExceptionToSessionErrorsAndDisplayEditMyDetailsPage() throws Exception {
		mockBasicDetails();
		PortalException exception = new PortalException();
		doThrow(exception).when(mockMyDetailsService).updateMyDetails(mockActionRequest, mockMyDetailsContext);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		updatePersonalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(PortletRequestKeys.ERROR_EXCEPTION, exception);
		verify(mockRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_PERSONAL_DETAILS_RENDER);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, exception.getClass(), exception);
	}

	private void mockBasicDetails() {
		when(mockMyDetailsContextService.getMyDetailsContext()).thenReturn(mockMyDetailsContext);
	}

}
