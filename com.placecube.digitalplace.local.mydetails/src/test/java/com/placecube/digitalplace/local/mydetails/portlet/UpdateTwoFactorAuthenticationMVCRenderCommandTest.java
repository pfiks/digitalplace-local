package com.placecube.digitalplace.local.mydetails.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MyDetailsConstants;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.WebContentArticles;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class UpdateTwoFactorAuthenticationMVCRenderCommandTest extends PowerMockito {

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private MyDetailsContextService mockMyDetailsContextService;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Captor
	private ArgumentCaptor<Map<String, String>> stringMapCaptor;

	@InjectMocks
	private UpdateTwoFactorAuthenticationMVCRenderCommand updateTwoFactorAuthenticationMVCRenderCommand;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		initMocks(this);
	}

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowPortletException() throws PortalException, PortletException {
		long companyId = 123;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);

		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenThrow(new ConfigurationException());

		updateTwoFactorAuthenticationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenSetContextInAttributes() throws PortalException, PortletException {
		long companyId = 123;
		long groupId = 123;
		long companyGroupId = 234;
		String backUrl = "url";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(companyGroupId);

		when(mockJournalArticleLocalService.fetchArticle(mockThemeDisplay.getCompanyGroupId(), WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION.getArticleId())).thenReturn(mockJournalArticle);

		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenReturn(mockMyDetailsContext);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.BACK_URL)).thenReturn(backUrl);

		updateTwoFactorAuthenticationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockMyDetailsService, mockRenderRequest);
		inOrder.verify(mockMyDetailsService, times(1)).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, mockMyDetailsContext);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.BACK_URL, backUrl);

		verify(mockRenderRequest, times(1)).setAttribute(eq(PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED_OPTIONS), stringMapCaptor.capture());
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ARTICLE_EXISTS, true);

		Map<String, String> twoFactorAuthenticationEnabledOptions = stringMapCaptor.getValue();

		assertThat(twoFactorAuthenticationEnabledOptions.size(), equalTo(2));

		String[] values = twoFactorAuthenticationEnabledOptions.values().toArray(new String[twoFactorAuthenticationEnabledOptions.size()]);

		assertThat(values[0], equalTo(MyDetailsConstants.I_WANT_TO_TURN_ON_2FA_LABEL));
		assertThat(values[1], equalTo(MyDetailsConstants.I_DONT_WANT_TO_TURN_ON_2FA_LABEL));

		assertThat(twoFactorAuthenticationEnabledOptions.get(Boolean.TRUE.toString()), equalTo(MyDetailsConstants.I_WANT_TO_TURN_ON_2FA_LABEL));
		assertThat(twoFactorAuthenticationEnabledOptions.get(Boolean.FALSE.toString()), equalTo(MyDetailsConstants.I_DONT_WANT_TO_TURN_ON_2FA_LABEL));
	}

	@Test(expected = PortletException.class)
	public void render_WhenPortalExceptionConfiguringMyDetailsContext_ThenThrowPortletException() throws PortalException, PortletException {
		long companyId = 123;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);

		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenReturn(mockMyDetailsContext);
		doThrow(new PortletException()).when(mockMyDetailsService).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);

		updateTwoFactorAuthenticationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenTwoFactorArticleDoesNotExist_ThenSetRequestAttributeToFalse() throws PortalException, PortletException {
		long companyId = 123;
		long groupId = 123;
		long companyGroupId = 234;
		String backUrl = "url";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(companyGroupId);

		when(mockJournalArticleLocalService.fetchArticle(mockThemeDisplay.getCompanyGroupId(), WebContentArticles.MY_DETAILS_TWO_FACTOR_AUTHENTICATION.getArticleId())).thenReturn(null);

		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenReturn(mockMyDetailsContext);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.BACK_URL)).thenReturn(backUrl);

		updateTwoFactorAuthenticationMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.TWO_FACTOR_AUTHENTICATION_ARTICLE_EXISTS, false);

	}

}
