package com.placecube.digitalplace.local.mydetails.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashSet;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.constants.ViewKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

@RunWith(PowerMockRunner.class)
public class MyDetailsMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 456L;

	private static final long GROUP_ID = 789L;

	private static final long USER_ID = 321L;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private MyDetailsContextService mockMyDetailsContextService;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserProfileFieldsConfiguration mockUserFieldsConfiguration;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private MyDetailsMVCRenderCommand myDetailsMVCRenderCommand;

	@Test
	public void render_WhenAdditionalDetailsSectionDisabled_ThenDoNotPopulateAccountContext() throws PortalException, PortletException, UserAccountException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(true);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(COMPANY_ID)).thenReturn(mockMyDetailsContext);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserFieldsConfiguration);
		when(mockMyDetailsService.checkAdditionalDetailsSectionAvailable(mockAccountContext, mockUserFieldsConfiguration)).thenReturn(false);

		myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockMyDetailsService, mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.IS_ADDITONAL_DETAILS_SECTION_AVAILABLE, false);
		inOrder.verify(mockMyDetailsService, times(1)).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, mockMyDetailsContext);
	}

	@Test
	public void render_WhenAdditionalDetailsSectionEnabled_ThenPopulateMyDetailsContextAndAccountContext() throws PortalException, PortletException, UserAccountException {
		Set<ExpandoField> expandoFields = new HashSet<>();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(true);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(COMPANY_ID)).thenReturn(mockMyDetailsContext);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserFieldsConfiguration);
		when(mockMyDetailsService.checkAdditionalDetailsSectionAvailable(mockAccountContext, mockUserFieldsConfiguration)).thenReturn(true);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserFieldsConfiguration)).thenReturn(expandoFields);

		myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockMyDetailsService, mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.IS_ADDITONAL_DETAILS_SECTION_AVAILABLE, true);
		inOrder.verify(mockMyDetailsService, times(1)).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, mockMyDetailsContext);
		inOrder.verify(mockMyDetailsService, times(1)).populateAccountContextAdditionalDetails(mockAccountContext, mockUser, mockUserFieldsConfiguration);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.USER_PROFILE_CONFIG, mockUserFieldsConfiguration);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.ADDITONAL_DETAILS_EXPANDO_FIELDS, expandoFields);
	}

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowPortletException() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(true);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(COMPANY_ID)).thenThrow(new ConfigurationException());

		myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenPopulateMyDetailsContextAndSetItInAttributes() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(true);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(COMPANY_ID)).thenReturn(mockMyDetailsContext);

		myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockMyDetailsService, mockRenderRequest);
		inOrder.verify(mockMyDetailsService, times(1)).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, mockMyDetailsContext);
	}

	@Test
	public void render_WhenNoErrorAndUserNotSignedIn_ThenReturnsPleaseLogInJSP() throws PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(false);

		String result = myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(ViewKeys.PLEASE_LOG_IN));
	}

	@Test
	public void render_WhenNoErrorAndUserSignedIn_ThenReturnsPersonalDetailsJSP() throws PortletException, ConfigurationException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(true);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(COMPANY_ID)).thenReturn(mockMyDetailsContext);

		String result = myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(ViewKeys.PERSONAL_DETAILS));
	}

	@Test(expected = PortletException.class)
	public void render_WhenPortalExceptionConfiguringMyDetailsContext_ThenThrowPortletException() throws PortalException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.fetchUser(USER_ID)).thenReturn(mockUser);
		when(mockMyDetailsService.checkUserSignedIn(mockRenderRequest)).thenReturn(true);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(COMPANY_ID)).thenReturn(mockMyDetailsContext);
		doThrow(new PortletException()).when(mockMyDetailsService).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);

		myDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

}
