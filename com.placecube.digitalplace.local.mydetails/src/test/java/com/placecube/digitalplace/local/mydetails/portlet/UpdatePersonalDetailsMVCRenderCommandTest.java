package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class UpdatePersonalDetailsMVCRenderCommandTest extends PowerMockito {

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private MyDetailsContextService mockMyDetailsContextService;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@InjectMocks
	private UpdatePersonalDetailsMVCRenderCommand updatePersonalDetailsMVCRenderCommand;

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowPortletException() throws PortalException, PortletException {
		long companyId = 123;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenThrow(new ConfigurationException());

		updatePersonalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenSetContextInAttributes() throws PortalException, PortletException {
		long companyId = 123;
		String backUrl = "url";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenReturn(mockMyDetailsContext);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.BACK_URL)).thenReturn(backUrl);

		updatePersonalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockMyDetailsService, mockRenderRequest);
		inOrder.verify(mockMyDetailsService, times(1)).populateMyDetailsContext(mockRenderRequest, mockMyDetailsContext, mockUser);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, mockMyDetailsContext);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.BACK_URL, backUrl);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		initMocks(this);
	}

}
