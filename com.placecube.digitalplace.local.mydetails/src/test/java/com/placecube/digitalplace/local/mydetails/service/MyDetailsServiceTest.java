package com.placecube.digitalplace.local.mydetails.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.UserPasswordException.MustMatchCurrentPassword;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;
import com.placecube.digitalplace.user.account.constants.PhoneType;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;
import com.placecube.digitalplace.user.account.service.AccountUserFieldsService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PropsUtil.class, ServiceContextFactory.class, ServiceContextThreadLocal.class, CalendarFactoryUtil.class, LanguageUtil.class, StringUtil.class, SessionMessages.class,
		SessionErrors.class })
public class MyDetailsServiceTest extends PowerMockito {

	private static final Date BIRTHDAY = new Date();

	private static final Long COMPANY_ID = 11l;

	private static final Long CONTACT_ID = 10l;

	private static final String EMAIL = "emailValue";

	private static final String FIRSTNAME = "firstNameValue";

	private static final Long GROUP_ID = 22L;

	private static final String KNOWN_AS = "knownAsValue";

	private static final String LASTNAME = "lastNameValue";

	private static final String PASSWORD1 = "password1Value";

	private static final String PASSWORD2 = "password2Value";

	private static final String UPDATED_EMAIL = "UpdatedEmailValue";

	private static final String UPDATED_FIRSTNAME = "UpdatedFirstNameValue";

	private static final String UPDATED_LASTNAME = "UpdatedLastNameValue";

	private static final String UPRN = "uprn";

	private static final Long USER_ID = 12l;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountPhoneService mockAccountPhoneService;

	@Mock
	private AccountUserFieldsService mockAccountUserFieldsService;

	@Mock
	private Address mockAddress;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Mock
	private AddressTypeConfigurationService mockAddressTypeConfigurationService;

	@Mock
	private Calendar mockCalendar;

	@Mock
	private Company mockCompany;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private Contact mockContact;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private MyDetailsCompanyConfiguration mockMyDetailsCompanyConfiguration;

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserGroupRole mockUserGroupRole;

	@Mock
	private UserGroupRoleLocalService mockUserGroupRoleLocalService;

	@Mock
	private List<UserGroupRole> mockUserGroupRoles;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Mock
	private UserProfileFieldsService mockUserProfileFieldsService;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private MyDetailsService myDetailsService;

	private List<Address> realAddressList;

	@Test
	public void checkAdditionalDetailsSectionAvailable_WhenHomeNumberDisabledAndMobileNumberDisabledAndJobTitleDisabledAndBusinessPhoneNumberDisabledAndNoExpandoFieldsConfigured_ThenReturnFalse() {
		Set<ExpandoField> expandoFields = new HashSet<>();
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(false);
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);

		boolean result = myDetailsService.checkAdditionalDetailsSectionAvailable(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertFalse(result);
	}

	@Test
	public void checkAdditionalDetailsSectionAvailable_WhenHomeNumberEnabledOrMobileNumberEnabledOrJobTitleEnabledOrBusinessPhoneNumberEnabledOrExpandoFieldsConfigured_ThenReturnTrue() {
		Set<ExpandoField> expandoFields = new HashSet<>();
		expandoFields.add(mockExpandoField);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);

		boolean result = myDetailsService.checkAdditionalDetailsSectionAvailable(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertTrue(result);
	}

	@Test(expected = Exception.class)
	public void checkPassword_WhenUserIsNull_ThenThrowsPortalException() throws PortalException {
		String currentPassword = "password";
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(null);

		myDetailsService.validatePassword(USER_ID, currentPassword);
	}

	@Test
	public void checkPassword_WhenUserPasswordIsCorrect_ThenDoNothing() throws PortalException {
		Map<String, String[]> headerMap = new HashMap<>();
		Map<String, String[]> parameterMap = new HashMap<>();
		Map<String, Object> resultsMap = new HashMap<>();

		String currentPassword = "password";
		String emailAddress = "email@test.com";

		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getAuthType()).thenReturn(CompanyConstants.AUTH_TYPE_EA);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(emailAddress);

		myDetailsService.validatePassword(USER_ID, currentPassword);

		verify(mockUserLocalService, times(1)).authenticateByEmailAddress(COMPANY_ID, emailAddress, currentPassword, headerMap, parameterMap, resultsMap);
	}

	@Test(expected = MustMatchCurrentPassword.class)
	public void checkPassword_WhenUserPasswordIsNotCorrect_ThenThrowsUserPasswordException() throws PortalException {
		int authResultOK = -1;

		Map<String, String[]> headerMap = new HashMap<>();
		Map<String, String[]> parameterMap = new HashMap<>();
		Map<String, Object> resultsMap = new HashMap<>();

		String currentPassword = "password";
		String emailAddress = "email@test.com";

		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getAuthType()).thenReturn(CompanyConstants.AUTH_TYPE_EA);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserLocalService.authenticateByEmailAddress(COMPANY_ID, emailAddress, currentPassword, headerMap, parameterMap, resultsMap)).thenReturn(authResultOK);

		myDetailsService.validatePassword(USER_ID, currentPassword);
	}

	@Test()
	public void checkUpdateEnabled_WhenUserIsNotSignedIn_ThenReturnFalse() {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);

		boolean result = myDetailsService.checkUserSignedIn(mockPortletRequest);

		assertFalse(result);
	}

	@Test
	public void checkUserSignedIn_WhenUserIsSignedIn_ThenReturnTrue() {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		boolean result = myDetailsService.checkUserSignedIn(mockPortletRequest);

		assertTrue(result);
	}

	@Test
	public void populateAccountContextAdditionalDetails_WhenNoError_ThenSetValuesInAccountContext() throws PortalException {
		String jobTitle = "jobTitle";
		when(mockUser.getJobTitle()).thenReturn(jobTitle);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);

		myDetailsService.populateAccountContextAdditionalDetails(mockAccountContext, mockUser, mockUserProfileFieldsConfiguration);

		InOrder inOrder = Mockito.inOrder(mockAccountPhoneService, mockAccountContext, mockAccountUserFieldsService);
		inOrder.verify(mockAccountPhoneService, times(1)).populateContextWithExistingPhones(mockAccountContext, mockUser);
		inOrder.verify(mockAccountContext, times(1)).setJobTitle(jobTitle);
		inOrder.verify(mockAccountUserFieldsService, times(1)).populateContextWithExistingConfiguredExpandoFields(mockAccountContext, mockUser, mockUserProfileFieldsConfiguration);
	}

	@Test
	public void populateMyDetailsContext_WhenNoError_ThenUserDetailsAddedToMyDetailsContext() throws PortalException, PortletException {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockUser.getLastName()).thenReturn(LASTNAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getContactId()).thenReturn(CONTACT_ID);

		when(mockUser.getAddresses()).thenReturn(realAddressList);

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);

		verify(mockMyDetailsContext, times(1)).setFirstName(FIRSTNAME);
		verify(mockMyDetailsContext, times(1)).setLastName(LASTNAME);
		verify(mockMyDetailsContext, times(1)).setEmailAddress(EMAIL);
		verify(mockMyDetailsContext, times(1)).setKnownAs(KNOWN_AS);
	}

	@Test
	@Parameters({ "true", "false" })
	public void populateMyDetailsContext_WhenNoErrors_ThenSetsTwoFactorAuthenticationAvailableToConfiguredTwoFactorAuthenticationPromptEnabledValue(boolean twoFactorAuthenticationPromptEnabled)
			throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.twoFactorAuthenticationPromptEnabled()).thenReturn(twoFactorAuthenticationPromptEnabled);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);

		verify(mockMyDetailsContext, times(1)).setTwoFactorAuthenticationAvailable(twoFactorAuthenticationPromptEnabled);
	}

	@Test
	public void populateMyDetailsContext_WhenUserDoesNotHaveAPrimaryAddress_ThenDoesNotAddAddressToTheMyDetailsContext() throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockUser.getLastName()).thenReturn(LASTNAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getContactId()).thenReturn(CONTACT_ID);

		realAddressList.add(mockAddress);
		when(mockUser.getAddresses()).thenReturn(realAddressList);
		when(mockAddress.isPrimary()).thenReturn(false);

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);

		verify(mockMyDetailsContext, never()).setAddress(mockAddress);
	}

	@Test
	public void populateMyDetailsContext_WhenUserHasAPrimaryAddress_ThenAddsAddressToMyDetailsContext() throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockUser.getLastName()).thenReturn(LASTNAME);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getContactId()).thenReturn(CONTACT_ID);

		realAddressList.add(mockAddress);
		when(mockUser.getAddresses()).thenReturn(realAddressList);
		when(mockAddress.isPrimary()).thenReturn(true);

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);

		verify(mockMyDetailsContext, times(1)).setAddress(mockAddress);
	}

	@Test(expected = PortletException.class)
	public void populateMyDetailsContext_WhenUserProfileFieldsConfigurationRetrievalFails_ThenThrowsPortletException() throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);
	}

	@Test
	public void populateMyDetailsContext_WhenUserTwoFactorAuthenticationDoesNotExist_ThenSetsTwoFactorAuthenticationEnabledFlagInMyDetailsContextToFalse() throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);

		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.empty());

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);

		verify(mockMyDetailsContext, times(1)).setTwoFactorAuthenticationEnabled(false);
	}

	@Test
	@Parameters({ "true", "false" })
	public void populateMyDetailsContext_WhenUserTwoFactorAuthenticationExists_ThenSetsTwoFactorAuthenticationEnabledFlagInMyDetailsContext(boolean enabled) throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(GetterUtil.getString(mockExpandoBridge.getAttribute(KnownAsConstants.KNOWN_AS, false))).thenReturn(KNOWN_AS);

		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(enabled);

		myDetailsService.populateMyDetailsContext(mockPortletRequest, mockMyDetailsContext, mockUser);

		verify(mockMyDetailsContext, times(1)).setTwoFactorAuthenticationEnabled(enabled);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ServiceContextFactory.class, ServiceContextThreadLocal.class, LanguageUtil.class, StringUtil.class, SessionMessages.class, SessionErrors.class);
		realAddressList = new ArrayList<>();
	}

	@Test(expected = PortalException.class)
	public void updateAdditionalDetails_WhenAddPhoneHasError_ThenExceptionIsThrown() throws PortalException {
		String businessPhone = "12345";
		Map<String, Serializable> expandoFields = new HashMap<>();
		expandoFields.put("expandoField1", "expandoValue1");
		expandoFields.put("expandoField2", "expandoValue2");
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockAccountContext.getExpandoFields()).thenReturn(expandoFields);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(businessPhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, businessPhone, PhoneType.BUSINESS, mockServiceContext);

		myDetailsService.updateAdditionalDetails(mockAccountContext, mockUser, mockServiceContext, mockUserProfileFieldsConfiguration);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, businessPhone, PhoneType.BUSINESS, mockServiceContext);
	}

	@Test
	public void updateAdditionalDetails_WhenNoError_ThenConfiguresTheUserExpandoAttributesAndPhoneNumbers() throws PortalException {
		String businessNumber = "12345";
		String mobileNumber = "6789";
		String personalNumber = "54321";

		Map<String, Serializable> expandoFields = new HashMap<>();
		expandoFields.put("expandoField1", "expandoValue1");
		expandoFields.put("expandoField2", "expandoValue2");

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockAccountContext.getJobTitle()).thenReturn("jobTitle");
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockAccountContext.getExpandoFields()).thenReturn(expandoFields);
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(businessNumber);
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(mobileNumber);
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(personalNumber);

		myDetailsService.updateAdditionalDetails(mockAccountContext, mockUser, mockServiceContext, mockUserProfileFieldsConfiguration);

		verify(mockExpandoBridge, times(1)).setAttribute("expandoField1", "expandoValue1", false);
		verify(mockExpandoBridge, times(1)).setAttribute("expandoField2", "expandoValue2", false);
		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, businessNumber, PhoneType.BUSINESS, mockServiceContext);
		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, mobileNumber, PhoneType.MOBILE, mockServiceContext);
		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, personalNumber, PhoneType.PERSONAL, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void updateAdditionalDetails_WhenUpdateJobTitleHasError_ThenExceptionIsThrown() throws PortalException {
		String jobTitle = "jobTitle";
		Map<String, Serializable> expandoFields = new HashMap<>();
		expandoFields.put("expandoField1", "expandoValue1");
		expandoFields.put("expandoField2", "expandoValue2");
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockAccountContext.getExpandoFields()).thenReturn(expandoFields);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockAccountContext.getJobTitle()).thenReturn(jobTitle);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);

		doThrow(new PortalException()).when(mockUserLocalService).updateJobTitle(USER_ID, jobTitle);

		myDetailsService.updateAdditionalDetails(mockAccountContext, mockUser, mockServiceContext, mockUserProfileFieldsConfiguration);

		verify(mockUserLocalService, times(1)).updateJobTitle(USER_ID, jobTitle);
	}

	@Test(expected = PortalException.class)
	public void updateAdditionalDetails_WhenUserExpandoBridgeHasError_ThenExceptionIsThrown() throws PortalException {
		Map<String, Serializable> expandoFields = new HashMap<>();
		expandoFields.put("expandoField1", "expandoValue1");
		expandoFields.put("expandoField2", "expandoValue2");
		when(mockUser.getExpandoBridge()).thenReturn(null);
		when(mockAccountContext.getExpandoFields()).thenReturn(expandoFields);

		myDetailsService.updateAdditionalDetails(mockAccountContext, mockUser, mockServiceContext, mockUserProfileFieldsConfiguration);

		verify(mockUser, times(1)).getExpandoBridge();
	}

	@Test
	public void updateMyDetails_UpdateUserSuccessfullyAndAllFieldsEditsAreEnabled_ThenUpdateTheUserDetails() throws PortalException {
		long companyId = 123;

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(true);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(true);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(true);
		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(true);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.getUserById(mockPortal.getUserId(mockPortletRequest))).thenReturn(mockUser);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);
		Long longVal = 11l;
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(EMAIL);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", EMAIL, true, null, "", "", "", "", UPDATED_FIRSTNAME, "", UPDATED_LASTNAME,
				longVal, longVal, false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

	}

	@Test
	public void updateMyDetails_UpdateUserSuccessfullyAndEmailNotChanged_ThenSessionMessageWasNotAdded() throws PortalException {
		long companyId = 123;

		String newEmailAddress = "emailValue";
		String oldEmailAddress = "emailValue";

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockUser.getEmailAddress()).thenReturn(oldEmailAddress);
		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockUser.getLastName()).thenReturn(LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.getUserById(mockPortal.getUserId(mockPortletRequest))).thenReturn(mockUser);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);
		when(mockCompany.isStrangersVerify()).thenReturn(true);
		when(StringUtil.equalsIgnoreCase(oldEmailAddress, newEmailAddress)).thenReturn(true);
		Long longVal = 11l;
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", EMAIL, true, null, "", "", "", "", FIRSTNAME, "", LASTNAME, longVal, longVal,
				false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

		verifyStatic(SessionMessages.class, never());
		SessionMessages.add(any(PortletRequest.class), anyString());
	}

	@Test
	public void updateMyDetails_UpdateUserSuccessfullyAndEmailWasChanged_ThenAddSessionMessage() throws PortalException {
		int authResultOK = 0;

		String newEmailAddress = "emailValue";
		String oldEmailAddress = "emailValue2";

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(true);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(true);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(true);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.getUserById(mockPortal.getUserId(mockPortletRequest))).thenReturn(mockUser);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);
		when(mockCompany.isStrangersVerify()).thenReturn(true);
		when(StringUtil.equalsIgnoreCase(oldEmailAddress, newEmailAddress)).thenReturn(false);
		Map<String, String[]> headerMap = new HashMap<>();
		Map<String, String[]> parameterMap = new HashMap<>();
		Map<String, Object> resultsMap = new HashMap<>();

		String currentPassword = "password";
		String emailAddress = "email@test.com";

		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getAuthType()).thenReturn(CompanyConstants.AUTH_TYPE_EA);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserLocalService.authenticateByEmailAddress(COMPANY_ID, emailAddress, currentPassword, headerMap, parameterMap, resultsMap)).thenReturn(authResultOK);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockPortletRequest, "verificationEmailSent");
	}

	@Test
	public void updateMyDetails_UpdateUserSuccessfullyAndSomeFieldsEditsAreDisabled_ThenUpdateOnlyEnabledUserDetails() throws PortalException {
		long companyId = 123;
		String userFirstName = "fname";
		String userLastName = "lname";

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getFirstName()).thenReturn(userFirstName);
		when(mockUser.getLastName()).thenReturn(userLastName);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.getUserById(mockPortal.getUserId(mockPortletRequest))).thenReturn(mockUser);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);
		Long longVal = 11l;

		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", EMAIL, true, null, "", "", "", "", userFirstName, "", userLastName, longVal,
				longVal, false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

	}

	@Test
	public void updateMyDetails_WhenAllFieldsEditsAreNotReadOnly_ThenUpdateTheUserDetails() throws PortalException {
		long companyId = 123;

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(true);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(true);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(true);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		Long longVal = 11l;

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));

		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", UPDATED_EMAIL, true, null, "", "", "", "", UPDATED_FIRSTNAME, "",
				UPDATED_LASTNAME, longVal, longVal, false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

	}

	@Test
	public void updateMyDetails_WhenEmailAddressEditIsReadOnly_ThenEmailAddressIsNotUpdated() throws PortalException {
		long companyId = 123;

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		Long longVal = 11l;

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockUser.getLastName()).thenReturn(LASTNAME);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", EMAIL, true, null, "", "", "", "", FIRSTNAME, "", LASTNAME, longVal, longVal,
				false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

	}

	@Test(expected = PortalException.class)
	public void updateMyDetails_WhenErrorObtainingAddress_ThenPortalExceptionIsThrown() throws PortalException {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenThrow(PortalException.class);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

	}

	@Test(expected = PortalException.class)
	public void updateMyDetails_WhenErrorSavingAddress_ThenPortalExceptionIsThrown() throws PortalException {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockAddressLookupService.saveAddress(mockUser, mockAddressContext, mockServiceContext)).thenThrow(PortalException.class);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

	}

	@Test(expected = PortalException.class)
	public void updateMyDetails_WhenExceptionRetrievingTheUser_ThenThrowsPortalException() throws PortalException {
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);

		when(mockPortal.getUser(mockPortletRequest)).thenThrow(PortalException.class);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);
	}

	@Test(expected = PortalException.class)
	public void updateMyDetails_WhenExceptionRetrievingTheUserContact_ThenThrowsPortalException() throws Exception {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMyDetailsCompanyConfiguration);

		when(mockUser.getContact()).thenThrow(PortalException.class);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

	}

	@Test
	public void updateMyDetails_WhenFirstNameEditIsReadOnly_ThenFirstNameIsNotUpdated() throws PortalException {
		long companyId = 123;

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockUser.getLastName()).thenReturn(LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		Long longVal = 11l;

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", EMAIL, true, null, "", "", "", "", FIRSTNAME, "", LASTNAME, longVal, longVal,
				false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

	}

	@Test
	public void updateMyDetails_WhenKnownAsIsNotReadOnly_ThenUpdateKnownAsWithMyDetailsContextValue() throws PortalException {
		long companyId = 123;

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockMyDetailsContext.getKnownAs()).thenReturn(KNOWN_AS);
		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(true);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockExpandoBridge, times(1)).setAttribute(KnownAsConstants.KNOWN_AS, KNOWN_AS);

	}

	@Test
	public void updateMyDetails_WhenKnownAsIsReadOnly_ThenDoNotUpdateKnownAs() throws PortalException {
		long companyId = 123;

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);

		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);
		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockExpandoBridge, never()).setAttribute(eq(KnownAsConstants.KNOWN_AS), anyString());

	}

	@Test
	public void updateMyDetails_WhenLastNameEditIsReadOnly_ThenLastNameIsNotUpdated() throws PortalException {
		long companyId = 123;

		when(mockPortal.getUserId(mockPortletRequest)).thenReturn(USER_ID);
		when(mockUserLocalService.getUserById(USER_ID)).thenReturn(mockUser);

		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, companyId)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockUser.getFirstName()).thenReturn(FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockUser.getLastName()).thenReturn(LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);
		Long longVal = 11l;

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockUserLocalService, times(1)).updateUser(USER_ID, PASSWORD1, PASSWORD1, PASSWORD2, true, "", "", "", EMAIL, true, null, "", "", "", "", FIRSTNAME, "", LASTNAME, longVal, longVal,
				false, 0, 0, 0, "", "", "", "", "", null, new long[0], new long[0], new long[0], mockUserGroupRoles, new long[0], mockServiceContext);

	}

	@Test
	public void updateMyDetails_WhenThereIsAddressForUPRN__ThenUpdateUserAddress() throws PortalException {

		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockAddressLookupService, times(1)).saveAddress(mockUser, mockAddressContext, mockServiceContext);
	}

	@Test
	public void updateMyDetails_WhenThereIsNoAddressForUPRN_ThenUserAddress() throws PortalException {

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(MyDetailsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.emailAddressEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getEmailAddress()).thenReturn(UPDATED_EMAIL);
		when(mockMyDetailsCompanyConfiguration.firstNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getFirstName()).thenReturn(UPDATED_FIRSTNAME);
		when(mockMyDetailsCompanyConfiguration.lastNameEditEnabled()).thenReturn(false);
		when(mockMyDetailsContext.getLastName()).thenReturn(UPDATED_LASTNAME);
		mockUpdateUser();
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		mockUserGroupRoles = Arrays.asList(mockUserGroupRole, mockUserGroupRole, mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(mockUser.getUserId())).thenReturn(mockUserGroupRoles);

		when(mockMyDetailsCompanyConfiguration.knownAsEditEnabled()).thenReturn(false);

		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyDetailsContext.getUprn()).thenReturn(UPRN);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.empty());
		when(mockPortal.getCompany(mockPortletRequest)).thenReturn(mockCompany);

		myDetailsService.updateMyDetails(mockPortletRequest, mockMyDetailsContext);

		verify(mockAddressTypeConfigurationService, never()).getDefaultAddressType(COMPANY_ID);
		verify(mockAddressLookupService, never()).saveAddress(mockUser, mockAddressContext, mockServiceContext);
	}

	@Test
	public void updatePassword_WhenNoErrors_TheUpdatesUserPasswordWithNoPasswordResetFlagAndReturnsUpdatedUser() throws PortalException {
		when(mockUserLocalService.updatePassword(USER_ID, PASSWORD1, PASSWORD2, false)).thenReturn(mockUser);

		assertThat(myDetailsService.updatePassword(USER_ID, PASSWORD1, PASSWORD2), sameInstance(mockUser));
	}

	@Test(expected = PortalException.class)
	public void updatePassword_WhenPasswordUpdateFails_TheThrowsPortalException() throws PortalException {
		when(mockUserLocalService.updatePassword(USER_ID, PASSWORD1, PASSWORD2, false)).thenThrow(new PortalException());

		myDetailsService.updatePassword(USER_ID, PASSWORD1, PASSWORD2);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateTwoFactorAuthenticationForUser_WhenNoErrors_ThenEnablesAndGetsOrCreatesTwoFactorAuthenticationForUser(boolean twoFactorAuthenticationEnabled) throws Exception {
		when(mockPortal.getUser(mockPortletRequest)).thenReturn(mockUser);

		myDetailsService.updateTwoFactorAuthenticationForUser(mockPortletRequest, twoFactorAuthenticationEnabled);

		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).enableAndGetOrCreateTwoFactorAuthenticationForUser(mockUser, twoFactorAuthenticationEnabled);
	}

	@Test(expected = PortalException.class)
	public void updateTwoFactorAuthenticationForUser_WhenUserRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockPortal.getUser(mockPortletRequest)).thenThrow(new PortalException());

		myDetailsService.updateTwoFactorAuthenticationForUser(mockPortletRequest, true);
	}

	private void mockUpdateUser() throws PortalException {

		when(mockUser.getPassword()).thenReturn(PASSWORD1);
		when(mockUser.getContact()).thenReturn(mockContact);
		mockStatic(CalendarFactoryUtil.class);
		when(CalendarFactoryUtil.getCalendar()).thenReturn(mockCalendar);
		when(mockContact.getBirthday()).thenReturn(BIRTHDAY);

		Long longVal = 11l;
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockMyDetailsContext.getPassword1()).thenReturn(PASSWORD1);
		when(mockMyDetailsContext.getPassword2()).thenReturn(PASSWORD2);
		when(mockUser.getPasswordReset()).thenReturn(true);
		when(mockUser.getReminderQueryQuestion()).thenReturn("");
		when(mockUser.getReminderQueryAnswer()).thenReturn("");
		when(mockUser.getScreenName()).thenReturn("");
		when(mockUser.getLanguageId()).thenReturn("");
		when(mockUser.getTimeZoneId()).thenReturn("");
		when(mockUser.getGreeting()).thenReturn("");
		when(mockUser.getComments()).thenReturn("");
		when(mockUser.getMiddleName()).thenReturn("");
		when(mockContact.getPrefixListTypeId()).thenReturn(longVal);
		when(mockContact.getSuffixListTypeId()).thenReturn(longVal);
		when(mockContact.getMale()).thenReturn(false);
		when(mockContact.getSmsSn()).thenReturn("");
		when(mockContact.getFacebookSn()).thenReturn("");
		when(mockContact.getJabberSn()).thenReturn("");
		when(mockContact.getSkypeSn()).thenReturn("");
		when(mockContact.getTwitterSn()).thenReturn("");
		when(mockContact.getJobTitle()).thenReturn("");
		when(mockUser.getGroupIds()).thenReturn(new long[0]);
		when(mockUser.getOrganizationIds()).thenReturn(new long[0]);
		when(mockUser.getRoleIds()).thenReturn(new long[0]);
		when(mockUser.getRoleIds()).thenReturn(new long[0]);
		when(mockUser.getUserGroupIds()).thenReturn(new long[0]);
	}

}
