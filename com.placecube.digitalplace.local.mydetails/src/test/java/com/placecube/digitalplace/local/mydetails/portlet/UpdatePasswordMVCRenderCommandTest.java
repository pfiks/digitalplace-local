package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.configuration.MyDetailsCompanyConfiguration;
import com.placecube.digitalplace.local.mydetails.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.mydetails.model.MyDetailsContext;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsContextService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class UpdatePasswordMVCRenderCommandTest extends PowerMockito {

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private MyDetailsCompanyConfiguration mockMyDetailsCompanyConfiguration;

	@Mock
	private MyDetailsContext mockMyDetailsContext;

	@Mock
	private MyDetailsContextService mockMyDetailsContextService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdatePasswordMVCRenderCommand updatePasswordMVCRenderCommand;

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowPortletException() throws PortalException, PortletException {
		long companyId = 123;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenThrow(new ConfigurationException());

		updatePasswordMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenSetContextInAttributes() throws PortalException, PortletException {
		long companyId = 123;
		long groupId = 123;
		String passwordPolicyArticleId = "id";
		String backUrl = "url";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockMyDetailsContextService.getMyDetailsContextWithCompanyConfigurationSet(companyId)).thenReturn(mockMyDetailsContext);
		when(mockMyDetailsContext.getMyDetailsCompanyConfiguration()).thenReturn(mockMyDetailsCompanyConfiguration);
		when(mockMyDetailsCompanyConfiguration.passwordPolicyArticleId()).thenReturn(passwordPolicyArticleId);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.BACK_URL)).thenReturn(backUrl);
		when(mockJournalArticleLocalService.fetchArticle(groupId, passwordPolicyArticleId)).thenReturn(mockJournalArticle);

		updatePasswordMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.MY_DETAILS_CONTEXT, mockMyDetailsContext);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.IS_PASSWORD_POLICY_ARTICLE_PRESENT, true);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.BACK_URL, backUrl);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		initMocks(this);
	}

}
