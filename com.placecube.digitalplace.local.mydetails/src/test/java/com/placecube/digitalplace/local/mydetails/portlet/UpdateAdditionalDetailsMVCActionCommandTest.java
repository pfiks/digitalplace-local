package com.placecube.digitalplace.local.mydetails.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.mydetails.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.mydetails.service.MyDetailsService;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class, SessionMessages.class, HttpComponentsUtil.class, ServiceContextThreadLocal.class })
@RunWith(PowerMockRunner.class)
public class UpdateAdditionalDetailsMVCActionCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 123L;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountValidationService mockAccountValidationService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MyDetailsService mockMyDetailsService;

	@Mock
	private PortletSession mockPortletSession;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@InjectMocks
	private UpdateAdditionalDetailsMVCActionCommand updateAdditionalDetailsMVCActionCommand;

	@Test
	public void doProcessAction_WhenHasErrors_ThenSetMVCRenderCommandNameToUpdateAdditionalDetailsPage() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContext.hasErrors()).thenReturn(true);

		updateAdditionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContextService, mockAccountValidationService, mockRenderParameters, mockMyDetailsService);
		inOrder.verify(mockAccountContextService, times(1)).configureAdditionalDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountValidationService, times(1)).validateAdditionalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);
		inOrder.verify(mockRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_ADDITIONAL_DETAILS);
	}

	@Test
	public void doProcessAction_WhenNoErrors_ThenUpdateAdditionalDetails() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContext.hasErrors()).thenReturn(false);

		updateAdditionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContextService, mockAccountValidationService, mockRenderParameters, mockMyDetailsService, mockPortletSession);
		inOrder.verify(mockAccountContextService, times(1)).configureAdditionalDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountValidationService, times(1)).validateAdditionalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);
		inOrder.verify(mockMyDetailsService, times(1)).updateAdditionalDetails(mockAccountContext, mockUser, mockServiceContext, mockUserProfileFieldsConfiguration);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionErrors.class, SessionMessages.class, HttpComponentsUtil.class, ServiceContextThreadLocal.class);
	}

}
