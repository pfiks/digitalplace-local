package com.placecube.digitalplace.local.webcontent.toppriority;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;

import com.placecube.digitalplace.local.webcontent.toppriority.configuration.TopPriorityGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.toppriority.constants.TopPriorityConstants;
import com.placecube.digitalplace.local.webcontent.toppriority.service.TopPriorityJournalArticleService;

@Component(immediate = true, service = ModelListener.class)
public class AssetEntryModelListener extends BaseModelListener<AssetEntry> {

	private static final Log LOG = LogFactoryUtil.getLog(AssetEntryModelListener.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private TopPriorityJournalArticleService topPriorityJournalArticleService;

	@Override
	public void onBeforeUpdate(AssetEntry originalAssetEntry, AssetEntry assetEntry) throws ModelListenerException {
		try {

			if (assetEntry.getClassName().equals(JournalArticle.class.getName())) {
				TopPriorityGroupConfiguration topPriorityGroupConfiguration = configurationProvider.getGroupConfiguration(TopPriorityGroupConfiguration.class, assetEntry.getGroupId());
				if (topPriorityGroupConfiguration.enabled()) {
					JournalArticle article = (JournalArticle) assetEntry.getAssetRenderer().getAssetObject();
					if (article.isApproved() && topPriorityJournalArticleService.hasTopPriorityField(article, topPriorityGroupConfiguration)) {
						if (topPriorityJournalArticleService.isTopPriorityTickedEntry(assetEntry, topPriorityGroupConfiguration)) {
							assetEntry.setPriority(TopPriorityConstants.TOP_PRIORITY);
							LOG.debug("Top priority set to assetEntry with entryId: " + assetEntry.getEntryId());
						} else {
							assetEntry.setPriority(0);
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}

}
