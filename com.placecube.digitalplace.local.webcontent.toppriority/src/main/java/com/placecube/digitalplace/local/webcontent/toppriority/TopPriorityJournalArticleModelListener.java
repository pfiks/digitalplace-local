package com.placecube.digitalplace.local.webcontent.toppriority;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.placecube.digitalplace.local.webcontent.toppriority.configuration.TopPriorityGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.toppriority.service.TopPriorityJournalArticleService;

@Component(immediate = true, service = ModelListener.class)
public class TopPriorityJournalArticleModelListener extends BaseModelListener<JournalArticle> {

	private static final Log LOG = LogFactoryUtil.getLog(TopPriorityJournalArticleModelListener.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private TopPriorityJournalArticleService topPriorityJournalArticleService;

	@Override
	public void onAfterUpdate(JournalArticle originalJournalArticle, JournalArticle journalArticle) throws ModelListenerException {
		try {
			if (journalArticle.isApproved()) {
				TopPriorityGroupConfiguration topPriorityGroupConfiguration = configurationProvider.getGroupConfiguration(TopPriorityGroupConfiguration.class, journalArticle.getGroupId());
				if (topPriorityGroupConfiguration.enabled()) {
					AssetEntry assetEntry = assetEntryLocalService.getEntry(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
					if (isTopPriorityArticle(journalArticle, assetEntry, topPriorityGroupConfiguration)) {
						removeTopPriorityFromAssetEntriesWithTheSameStructureAtTheSameGroup(journalArticle, assetEntry, topPriorityGroupConfiguration);
					}
				}
			}
		} catch (PortalException e) {
			LOG.error("Unable to get Entry" + e.getMessage());
			LOG.debug(e);
		}
	}

	private boolean isTopPriorityArticle(JournalArticle journalArticle, AssetEntry assetEntry, TopPriorityGroupConfiguration topPriorityGroupConfiguration) {
		return topPriorityJournalArticleService.hasTopPriorityField(journalArticle, topPriorityGroupConfiguration) && topPriorityJournalArticleService.isTopPriorityTickedEntry(assetEntry, topPriorityGroupConfiguration);
	}

	private void removeTopPriorityFromAssetEntriesWithTheSameStructureAtTheSameGroup(JournalArticle journalArticle, AssetEntry assetEntry, TopPriorityGroupConfiguration topPriorityGroupConfiguration) {
		List<AssetEntry> topPriorityAssetEntries = topPriorityJournalArticleService.getTopPriorityAssetEntriesByStructure(journalArticle.getDDMStructure(), journalArticle.getGroupId());
		LOG.debug("Processing " + topPriorityAssetEntries.size() + " Top Priority journal articles.");
		for (AssetEntry topPriorityAssetEntry : topPriorityAssetEntries) {
			if (topPriorityAssetEntry.getEntryId() != assetEntry.getEntryId()) {
				topPriorityJournalArticleService.removeTopPriority(topPriorityAssetEntry, topPriorityGroupConfiguration);
			}
		}
	}

}
