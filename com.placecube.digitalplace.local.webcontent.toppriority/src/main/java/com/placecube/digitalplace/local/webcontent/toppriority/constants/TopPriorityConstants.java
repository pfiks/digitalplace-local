package com.placecube.digitalplace.local.webcontent.toppriority.constants;

public class TopPriorityConstants {

	public static final double TOP_PRIORITY = 100;

	private TopPriorityConstants() {
	}
}
