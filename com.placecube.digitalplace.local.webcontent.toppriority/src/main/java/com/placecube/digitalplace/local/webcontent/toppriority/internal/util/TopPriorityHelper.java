package com.placecube.digitalplace.local.webcontent.toppriority.internal.util;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMFieldLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.util.FieldsToDDMFormValuesConverter;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.persistence.JournalArticlePersistence;
import com.liferay.journal.util.JournalConverter;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

@Component(immediate = true, service = TopPriorityHelper.class)
public class TopPriorityHelper {

	private static final Log LOG = LogFactoryUtil.getLog(TopPriorityHelper.class);

	@Reference
	private DDMFieldLocalService ddmFieldLocalService;

	@Reference
	private FieldsToDDMFormValuesConverter fieldsToDDMFormValuesConverter;

	@Reference
	private JournalArticlePersistence journalArticlePersistence;

	@Reference
	private JournalConverter journalConverter;

	public boolean isTopPriorityDDMFormFieldName(String name, String topPriorityDDMFormFieldName) {
		return Validator.isNotNull(name) && name.equals(topPriorityDDMFormFieldName);
	}

	public void updateDDMFields(JournalArticle article, String content) throws PortalException {

		DDMStructure ddmStructure = article.getDDMStructure();
		DDMFormValues ddmFormValues = fieldsToDDMFormValuesConverter.convert(ddmStructure, journalConverter.getDDMFields(ddmStructure, content));
		ddmFieldLocalService.updateDDMFormValues(ddmStructure.getStructureId(), article.getId(), ddmFormValues);

		// Document Cache

		try {
			article.setDocument(SAXReaderUtil.read(content));
			journalArticlePersistence.cacheResult(article);
		} catch (DocumentException documentException) {
			if (LOG.isWarnEnabled()) {
				LOG.warn(documentException, documentException);
			}
		}
	}

}
