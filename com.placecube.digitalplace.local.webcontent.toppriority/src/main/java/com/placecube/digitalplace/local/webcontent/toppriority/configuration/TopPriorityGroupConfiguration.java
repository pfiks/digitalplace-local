package com.placecube.digitalplace.local.webcontent.toppriority.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "web-content", scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.local.webcontent.toppriority.configuration.TopPriorityGroupConfiguration", localization = "content/Language", name = "top-priority")
public interface TopPriorityGroupConfiguration {

	@Meta.AD(required = false, deflt = "false")
	boolean enabled();

	@Meta.AD(required = false, deflt = "TopPriority", name = "top-priority-field-field-reference", description = "top-priority-field-field-reference-description")
	String topPriorityFieldFieldReference();

}
