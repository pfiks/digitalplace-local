package com.placecube.digitalplace.local.webcontent.toppriority.service;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.mapping.kernel.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.constants.FieldConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReader;
import com.placecube.digitalplace.local.webcontent.toppriority.configuration.TopPriorityGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.toppriority.constants.TopPriorityConstants;
import com.placecube.digitalplace.local.webcontent.toppriority.internal.util.TopPriorityHelper;

@Component(immediate = true, service = TopPriorityJournalArticleService.class)
public class TopPriorityJournalArticleService {

	private static final Log LOG = LogFactoryUtil.getLog(TopPriorityJournalArticleService.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private SAXReader saxReader;

	@Reference
	private TopPriorityHelper topPriorityHelper;

	public List<AssetEntry> getTopPriorityAssetEntriesByStructure(DDMStructure ddmStructure, long groupId) {
		DynamicQuery journalArticleDynamicQuery = journalArticleLocalService.dynamicQuery();
		journalArticleDynamicQuery.add(PropertyFactoryUtil.forName("DDMStructureId").eq(ddmStructure.getStructureId()));
		journalArticleDynamicQuery.add(PropertyFactoryUtil.forName(Field.GROUP_ID).eq(groupId));
		journalArticleDynamicQuery.add(PropertyFactoryUtil.forName(Field.STATUS).eq(WorkflowConstants.STATUS_APPROVED));
		Projection projection = ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("resourcePrimKey"));
		journalArticleDynamicQuery.setProjection(projection);

		List<Long> topPriorityResourcePrimaryKeys = journalArticleLocalService.dynamicQuery(journalArticleDynamicQuery);

		DynamicQuery assetQuery = assetEntryLocalService.dynamicQuery();
		assetQuery.add(PropertyFactoryUtil.forName(Field.CLASS_NAME_ID).eq(PortalUtil.getClassNameId(JournalArticle.class.getName())));
		assetQuery.add(PropertyFactoryUtil.forName(Field.PRIORITY).eq(TopPriorityConstants.TOP_PRIORITY));
		assetQuery.add(PropertyFactoryUtil.forName(Field.CLASS_PK).in(topPriorityResourcePrimaryKeys));

		return assetEntryLocalService.dynamicQuery(assetQuery);
	}

	public boolean hasTopPriorityField(JournalArticle journalArticle, TopPriorityGroupConfiguration topPriorityGroupConfiguration) {
		try {
			DDMFormField ddmFormField = journalArticle.getDDMStructure().getDDMFormFieldByFieldReference(topPriorityGroupConfiguration.topPriorityFieldFieldReference());
			return Validator.isNotNull(ddmFormField) && FieldConstants.BOOLEAN.equals(ddmFormField.getDataType());
		} catch (PortalException ignored) {
			//No action needed
		}
		return false;
	}

	public boolean isTopPriorityTickedEntry(AssetEntry assetEntry, TopPriorityGroupConfiguration topPriorityGroupConfiguration) {
		AssetRenderer<Object> assetEntryAssetRenderer = (AssetRenderer<Object>) assetEntry.getAssetRenderer();
		if (Validator.isNotNull(assetEntryAssetRenderer)) {
			try {
				List<DDMFormFieldValue> ddmFormFieldValues = assetEntryAssetRenderer.getDDMFormValuesReader().getDDMFormValues().getDDMFormFieldValues();
				List<DDMFormFieldValue> topPriorityDDMFormFieldValues = ddmFormFieldValues.stream().filter(x ->
						topPriorityHelper.isTopPriorityDDMFormFieldName((String) x.getDDMFormField().getProperty("fieldReference"), topPriorityGroupConfiguration.topPriorityFieldFieldReference())).collect(Collectors.toList());

				if (Validator.isNotNull(topPriorityDDMFormFieldValues)) {
					return topPriorityDDMFormFieldValues.stream().anyMatch(s -> s.getValue().getString(s.getValue().getDefaultLocale()).equalsIgnoreCase("true"));
				} else {
					return false;
				}
			} catch (PortalException e) {
				LOG.error(e.getMessage());
				LOG.debug(e);
				return false;
			}
		}
		return false;
	}

	public void removeTopPriority(AssetEntry topPriorityAssetEntry, TopPriorityGroupConfiguration topPriorityGroupConfiguration) {
		try {
			JournalArticle topPriorityJournalArticle = (JournalArticle) topPriorityAssetEntry.getAssetRenderer().getAssetObject();
			uncheckTopPriority(topPriorityJournalArticle, topPriorityGroupConfiguration.topPriorityFieldFieldReference());
			setZeroPriority(topPriorityJournalArticle);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
	}

	private String getUpdatedContent(Document document, JournalArticle journalArticle, String topPriorityFieldFieldReference) {
		Node topPriorityNode = document.selectSingleNode("/root/dynamic-element[@field-reference='" + topPriorityFieldFieldReference + "']/dynamic-content");
		String topPriorityXml = topPriorityNode.asXML();
		topPriorityNode.setText("![CDATA[false]]");
		String newTopPriorityXml = topPriorityNode.asXML();
		return journalArticle.getContent().replace(topPriorityXml, newTopPriorityXml);
	}

	private void setZeroPriority(JournalArticle article) {
		try {
			AssetEntry assetEntry = assetEntryLocalService.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
			assetEntry.setPriority(0);
			assetEntryLocalService.updateAssetEntry(assetEntry);
		} catch (PortalException e) {
			LOG.debug("Failed to update priority: " + e);
		}
	}

	private void uncheckTopPriority(JournalArticle journalArticle, String topPriorityFieldFieldReference) {
		try {
			Document document = saxReader.read(journalArticle.getContent());
			if (Validator.isNotNull(topPriorityFieldFieldReference)) {
				String updatedContent = getUpdatedContent(document, journalArticle, topPriorityFieldFieldReference);

				topPriorityHelper.updateDDMFields(journalArticle, updatedContent);
				journalArticleLocalService.updateJournalArticle(journalArticle);
				LOG.debug("Top priority field unchecked from journal article with articleId: " + journalArticle.getArticleId());
			} else {
				LOG.error("Error retrieving top priority field name from article: " + journalArticle.getResourcePrimKey());
			}
		} catch (DocumentException e) {
			LOG.debug("Error removing top priority checkbox: " + e);
		} catch (PortalException e) {
			LOG.error("Unable to get Entry" + e.getMessage());
			LOG.debug(e);
		}
	}

}
