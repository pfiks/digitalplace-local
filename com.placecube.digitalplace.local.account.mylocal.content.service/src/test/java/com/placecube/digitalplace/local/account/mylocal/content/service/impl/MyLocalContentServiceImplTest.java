package com.placecube.digitalplace.local.account.mylocal.content.service.impl;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentConnector;

@RunWith(PowerMockRunner.class)
public class MyLocalContentServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 1;

	private static final String DATE_VALUE_1 = "Date Value 1";

	private static final String DATE_VALUE_2 = "Date Value 2";

	private static final String UPRN = "1234";

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private MyLocalContentConnector mockMyLocalContentConnector1;

	@Mock
	private MyLocalContentConnector mockMyLocalContentConnector2;

	@InjectMocks
	private MyLocalContentServiceImpl myLocalContentServiceImpl;

	@Test
	public void getDateValue_WhenMultipleEnabledConnectors_ThenReturnAnyConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector1.getDateValue(mockJSONObject1)).thenReturn(DATE_VALUE_1);
		when(mockMyLocalContentConnector2.getDateValue(mockJSONObject1)).thenReturn(DATE_VALUE_2);

		String response = myLocalContentServiceImpl.getDateValue(COMPANY_ID, mockJSONObject1);

		assertThat(response, anyOf(is(DATE_VALUE_1), is(DATE_VALUE_2)));
	}

	@Test(expected = PortalException.class)
	public void getDateValue_WhenNoEnabledConnector_ThenThrowsPortalException() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		myLocalContentServiceImpl.getDateValue(COMPANY_ID, mockJSONObject1);
	}

	@Test
	public void getDateValue_WhenOneEnabledConnector_ThenReturnEnabledConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector2.getDateValue(mockJSONObject1)).thenReturn(DATE_VALUE_1);

		String response = myLocalContentServiceImpl.getDateValue(COMPANY_ID, mockJSONObject1);

		assertEquals(DATE_VALUE_1, response);
	}

	@Test
	public void getFoodWasteDay_WhenMultipleEnabledConnectors_ThenReturnAnyConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector1.getFoodWasteDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject1);
		when(mockMyLocalContentConnector2.getFoodWasteDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getFoodWasteDay(COMPANY_ID, UPRN);

		assertThat(response, anyOf(sameInstance(mockJSONObject1), sameInstance(mockJSONObject2)));
	}

	@Test(expected = PortalException.class)
	public void getFoodWasteDay_WhenNoEnabledConnector_ThenThrowsPortalException() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		myLocalContentServiceImpl.getFoodWasteDay(COMPANY_ID, UPRN);
	}

	@Test
	public void getFoodWasteDay_WhenOneEnabledConnector_ThenReturnEnabledConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector2.getFoodWasteDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getFoodWasteDay(COMPANY_ID, UPRN);

		assertThat(response, sameInstance(mockJSONObject2));
	}

	@Test
	public void getGardenWasteDay_WhenMultipleEnabledConnectors_ThenReturnAnyConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector1.getGardenWasteDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject1);
		when(mockMyLocalContentConnector2.getGardenWasteDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getGardenWasteDay(COMPANY_ID, UPRN);

		assertThat(response, anyOf(sameInstance(mockJSONObject1), sameInstance(mockJSONObject2)));
	}

	@Test(expected = PortalException.class)
	public void getGardenWasteDay_WhenNoEnabledConnector_ThenThrowsPortalException() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		myLocalContentServiceImpl.getGardenWasteDay(COMPANY_ID, UPRN);
	}

	@Test
	public void getGardenWasteDay_WhenOneEnabledConnector_ThenReturnEnabledConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector2.getGardenWasteDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getGardenWasteDay(COMPANY_ID, UPRN);

		assertThat(response, sameInstance(mockJSONObject2));
	}

	@Test
	public void getRecyclingDay_WhenMultipleEnabledConnectors_ThenReturnAnyConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector1.getRecyclingDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject1);
		when(mockMyLocalContentConnector2.getRecyclingDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getRecyclingDay(COMPANY_ID, UPRN);

		assertThat(response, anyOf(sameInstance(mockJSONObject1), sameInstance(mockJSONObject2)));
	}

	@Test(expected = PortalException.class)
	public void getRecyclingDay_WhenNoEnabledConnector_ThenThrowsPortalException() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		myLocalContentServiceImpl.getRecyclingDay(COMPANY_ID, UPRN);
	}

	@Test
	public void getRecyclingDay_WhenOneEnabledConnector_ThenReturnEnabledConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector2.getRecyclingDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getRecyclingDay(COMPANY_ID, UPRN);

		assertThat(response, sameInstance(mockJSONObject2));

	}

	@Test
	public void getRefuseDay_WhenMultipleEnabledConnectors_ThenReturnAnyConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector1.getRefuseDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject1);
		when(mockMyLocalContentConnector2.getRefuseDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getRefuseDay(COMPANY_ID, UPRN);

		assertThat(response, anyOf(sameInstance(mockJSONObject1), sameInstance(mockJSONObject2)));
	}

	@Test(expected = PortalException.class)
	public void getRefuseDay_WhenNoEnabledConnector_ThenThrowsPortalException() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		myLocalContentServiceImpl.getRefuseDay(COMPANY_ID, UPRN);
	}

	@Test
	public void getRefuseDay_WhenOneEnabledConnector_ThenReturnEnabledConnectorResponse() throws PortalException {
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector1);
		myLocalContentServiceImpl.setMyLocalContentConnector(mockMyLocalContentConnector2);

		when(mockMyLocalContentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockMyLocalContentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockMyLocalContentConnector2.getRefuseDay(COMPANY_ID, UPRN)).thenReturn(mockJSONObject2);

		JSONObject response = myLocalContentServiceImpl.getRefuseDay(COMPANY_ID, UPRN);

		assertThat(response, sameInstance(mockJSONObject2));
	}
}
