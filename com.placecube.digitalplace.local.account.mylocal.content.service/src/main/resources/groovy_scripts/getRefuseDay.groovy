import org.osgi.framework.Bundle;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.framework.FrameworkUtil;

import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentService;

Bundle bundle = FrameworkUtil.getBundle(com.liferay.portal.scripting.groovy.internal.GroovyExecutor.class);
ServiceTracker<MyLocalContentService,MyLocalContentService> myLocalContentServiceTracker =
        new ServiceTracker(bundle.getBundleContext(), MyLocalContentService.class, null);

myLocalContentServiceTracker.open();

MyLocalContentService myLocalContentService= myLocalContentServiceTracker.getService();

def uprn = "200000760193";
def companyId = 20101L;

try {
    def jsonObject = myLocalContentService.getRefuseDay(companyId, uprn);
    out.println("${jsonObject}");
} catch (Exception e) {
    out.println("${e}");
}
myLocalContentServiceTracker.close();