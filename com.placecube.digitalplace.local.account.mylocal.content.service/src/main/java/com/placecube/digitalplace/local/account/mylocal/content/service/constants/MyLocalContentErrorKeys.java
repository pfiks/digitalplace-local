package com.placecube.digitalplace.local.account.mylocal.content.service.constants;

public final class MyLocalContentErrorKeys {

	public static final String NO_CONNECTOR_CONFIGURED = "No My Local Content Connector configured";

	private MyLocalContentErrorKeys() {

	}

}
