package com.placecube.digitalplace.local.account.mylocal.content.service.impl;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentConnector;
import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentService;
import com.placecube.digitalplace.local.account.mylocal.content.service.constants.MyLocalContentErrorKeys;

@Component(immediate = true, service = MyLocalContentService.class)
public class MyLocalContentServiceImpl implements MyLocalContentService {

	private Set<MyLocalContentConnector> myLocalContentConnectors = new HashSet<>();

	@Override
	public String getDateValue(long companyId, JSONObject jsonObject) throws PortalException {
		Optional<MyLocalContentConnector> myLocalContentConnector = getMyLocalContentConnector(companyId);

		if (myLocalContentConnector.isPresent()) {
			return myLocalContentConnector.get().getDateValue(jsonObject);
		} else {
			throw new PortalException(MyLocalContentErrorKeys.NO_CONNECTOR_CONFIGURED);
		}
	}

	@Override
	public JSONObject getFoodWasteDay(long companyId, String uprn) throws PortalException {
		Optional<MyLocalContentConnector> myLocalContentConnector = getMyLocalContentConnector(companyId);

		if (myLocalContentConnector.isPresent()) {
			return myLocalContentConnector.get().getFoodWasteDay(companyId, uprn);
		} else {
			throw new PortalException(MyLocalContentErrorKeys.NO_CONNECTOR_CONFIGURED);
		}
	}

	@Override
	public JSONObject getGardenWasteDay(long companyId, String uprn) throws PortalException {
		Optional<MyLocalContentConnector> myLocalContentConnector = getMyLocalContentConnector(companyId);

		if (myLocalContentConnector.isPresent()) {
			return myLocalContentConnector.get().getGardenWasteDay(companyId, uprn);
		} else {
			throw new PortalException(MyLocalContentErrorKeys.NO_CONNECTOR_CONFIGURED);
		}
	}

	@Override
	public JSONObject getRecyclingDay(long companyId, String uprn) throws PortalException {
		Optional<MyLocalContentConnector> myLocalContentConnector = getMyLocalContentConnector(companyId);

		if (myLocalContentConnector.isPresent()) {
			return myLocalContentConnector.get().getRecyclingDay(companyId, uprn);
		} else {
			throw new PortalException(MyLocalContentErrorKeys.NO_CONNECTOR_CONFIGURED);
		}
	}

	@Override
	public JSONObject getRefuseDay(long companyId, String uprn) throws PortalException {
		Optional<MyLocalContentConnector> myLocalContentConnector = getMyLocalContentConnector(companyId);

		if (myLocalContentConnector.isPresent()) {
			return myLocalContentConnector.get().getRefuseDay(companyId, uprn);
		} else {
			throw new PortalException(MyLocalContentErrorKeys.NO_CONNECTOR_CONFIGURED);
		}
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setMyLocalContentConnector(MyLocalContentConnector myLocalContentConnector) {
		myLocalContentConnectors.add(myLocalContentConnector);
	}

	protected void unsetMyLocalContentConnector(MyLocalContentConnector myLocalContentConnector) {
		myLocalContentConnectors.remove(myLocalContentConnector);
	}

	private Optional<MyLocalContentConnector> getMyLocalContentConnector(long companyId) {
		return myLocalContentConnectors.stream().filter(connector -> connector.enabled(companyId)).findAny();
	}
}
