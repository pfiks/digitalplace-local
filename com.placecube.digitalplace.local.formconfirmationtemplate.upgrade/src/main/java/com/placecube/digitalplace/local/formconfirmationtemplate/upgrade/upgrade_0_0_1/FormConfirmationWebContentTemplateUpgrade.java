package com.placecube.digitalplace.local.formconfirmationtemplate.upgrade.upgrade_0_0_1;

import java.io.IOException;
import java.sql.SQLException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.webcontent.formconfirmation.service.FormConfirmationWebContentService;

public class FormConfirmationWebContentTemplateUpgrade extends UpgradeProcess {

	private final FormConfirmationWebContentService formConfirmationWebContentService;

	public FormConfirmationWebContentTemplateUpgrade(FormConfirmationWebContentService formConfirmationWebContentService1) {
		this.formConfirmationWebContentService = formConfirmationWebContentService1;
	}

	@Override
	protected void doUpgrade() throws Exception {
		upgradeTemplate();
	}

	private void upgradeTemplate() throws IOException, SQLException {
		String script = formConfirmationWebContentService.getDDMTemplateScript();
		script = script.replace("'", "''");
		runSQL("update DDMTemplate set script='" + script + "' where templateKey='FORM CONFIRMATION';");
	}

}