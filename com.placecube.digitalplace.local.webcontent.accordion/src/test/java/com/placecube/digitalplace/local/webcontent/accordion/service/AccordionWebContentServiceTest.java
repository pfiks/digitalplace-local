package com.placecube.digitalplace.local.webcontent.accordion.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.accordion.constants.AccordionConstants;
import com.placecube.digitalplace.local.webcontent.accordion.constants.AccordionDDMTemplate;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class AccordionWebContentServiceTest extends PowerMockito {

	private static final Locale LOCALE = Locale.UK;

	private AccordionWebContentService accordionWebContentService;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private DefaultDDMStructureHelper mockDefaultDDMStructureHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, AccordionConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		accordionWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenAddsDDMStructures() throws Exception {
		long groupId = 123;
		long classNameId = 456;
		long userId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);

		accordionWebContentService.getOrCreateDDMStructure(mockServiceContext);

		verify(mockDefaultDDMStructureHelper, times(1)).addDDMStructures(userId, groupId, classNameId, getClass().getClassLoader(),
				"com/placecube/digitalplace/local/webcontent/accordion/dependencies/ddm/structure.xml", mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheAccordionStructure() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, AccordionConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure result = accordionWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, AccordionDDMTemplate.ACCORDION_CONTENT.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, AccordionConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		accordionWebContentService.getOrCreateDDMTemplate(mockServiceContext, AccordionDDMTemplate.ACCORDION_CONTENT);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExist_ThenCreatesTemplate() throws PortalException {

		long groupId = 1;
		long classNameId = 2;
		long userId = 3;
		long structureId = 4;
		long journalArticleClassNameId = 5;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalArticleClassNameId);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, AccordionDDMTemplate.ACCORDION_CONTENT.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, journalArticleClassNameId, AccordionConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getClassNameId()).thenReturn(classNameId);

		accordionWebContentService.getOrCreateDDMTemplate(mockServiceContext, AccordionDDMTemplate.ACCORDION_CONTENT);

		verify(mockDDMTemplateLocalService, times(1)).addTemplate(userId, groupId, classNameId, structureId, classNameId, AccordionDDMTemplate.ACCORDION_CONTENT.getKey(),
				Collections.singletonMap(LOCALE, AccordionDDMTemplate.ACCORDION_CONTENT.getName()), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE,
				TemplateConstants.LANG_TYPE_FTL, null, true, false, StringPool.BLANK, null, mockServiceContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateExists_ThenReturnTemplate() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, AccordionDDMTemplate.ACCORDION_CONTENT.getKey())).thenReturn(mockDDMTemplate);

		DDMTemplate result = accordionWebContentService.getOrCreateDDMTemplate(mockServiceContext, AccordionDDMTemplate.ACCORDION_CONTENT);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);

		accordionWebContentService = new AccordionWebContentService();
		accordionWebContentService.ddmStructureLocalService = mockDDMStructureLocalService;
		accordionWebContentService.defaultDDMStructureHelper = mockDefaultDDMStructureHelper;
		accordionWebContentService.ddmTemplateLocalService = mockDDMTemplateLocalService;
		accordionWebContentService.portal = mockPortal;
	}

}
