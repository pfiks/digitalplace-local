<#assign accordionNamespace = 'accordion-' + randomNamespace />
<div class="govuk-accordion" data-module="govuk-accordion" id="${accordionNamespace}">
	<#if SectionHeading.getSiblings()?has_content>
		<#list SectionHeading.getSiblings() as section>

			<#assign accordionIndex = section?index + 1 />

			<div class="govuk-accordion__section ">
				<div class="govuk-accordion__section-header">
					<h2 class="govuk-accordion__section-heading my-0">
						<span class="govuk-accordion__section-button" id="${accordionNamespace}-heading-${accordionIndex}">
							${section.getData()}
						</span>
					</h2>
					<#if section.SectionSummary?? && section.SectionSummary.getData()?has_content>
						<div class="govuk-accordion__section-summary govuk-body" id="${accordionNamespace}-summary-${accordionIndex}">
							${section.SectionSummary.getData()}
						</div>
					</#if>
				</div>

				<div id="${accordionNamespace}-content-${accordionIndex}" class="govuk-accordion__section-content" aria-labelledby="${accordionNamespace}-heading-${accordionIndex}">
					${section.SectionContent.getData()}
				</div>
			</div>
		</#list>
	</#if>
</div>