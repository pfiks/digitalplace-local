package com.placecube.digitalplace.local.webcontent.accordion.service;

import java.util.Collections;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.accordion.constants.AccordionConstants;
import com.placecube.digitalplace.local.webcontent.accordion.constants.AccordionDDMTemplate;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = AccordionWebContentService.class)
public class AccordionWebContentService {

	@Reference
	private AssetListEntryInitializer assetListEntryInitializer;

	@Reference
	private DDMInitializer ddmInitializer;

	@Reference
	DDMStructureLocalService ddmStructureLocalService;

	@Reference
	DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	Portal portal;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		try {

			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
					"com/placecube/digitalplace/local/webcontent/accordion/dependencies/ddm/structure.xml", serviceContext);

			return ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), AccordionConstants.STRUCTURE_KEY);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public DDMTemplate getOrCreateDDMTemplate(ServiceContext serviceContext, AccordionDDMTemplate accordionDDMTemplate) throws PortalException {
		try {
			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), accordionDDMTemplate.getKey());

			return Validator.isNotNull(ddmTemplate) ? ddmTemplate : addDDMTemplate(serviceContext, accordionDDMTemplate);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private DDMTemplate addDDMTemplate(ServiceContext serviceContext, AccordionDDMTemplate accordionDDMTemplate) throws PortalException {

		try {
			String script = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/accordion/dependencies/ddm/" + accordionDDMTemplate.getKey() + ".ftl");

			DDMStructure guideStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), AccordionConstants.STRUCTURE_KEY);

			return ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), guideStructure.getStructureId(),
					guideStructure.getClassNameId(), accordionDDMTemplate.getKey(), Collections.singletonMap(serviceContext.getLocale(), accordionDDMTemplate.getName()), null,
					DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, script, true, false, StringPool.BLANK, null,
					serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}
}
