<#if entries?has_content>
	<div class="nav-menu-collapsible-box">
		<ul class="list list-rich list-services">
			<#list entries as navigationEntry>
				<li class="list-item">
					<button data-target="#target-${navigationEntry?index}" data-toggle="collapse" class="list-button" id="data-toggle-${navigationEntry?index}" type="button">
						<span class="list-heading">${navigationEntry.getName()}</span>
					</button>
					<div aria-labelledby="data-toggle-${navigationEntry?index}" class="collapse list-content" id="target-${navigationEntry?index}">
						
						<#if navigationEntry.getLayout()?has_content>
							<p class="list-description">${navigationEntry.getLayout().getDescription()}</p>
						</#if>
	
						<ul class="list list-sub-links list-two-column">				
							<#list navigationEntry.getBrowsableChildren() as childNavigationEntry>
								<li class="list-item"><a class="list-link" href="${childNavigationEntry.getRegularURL()}">${childNavigationEntry.getName()}</a>
								</li>
							</#list>
						</ul>
					</div>
				</li>
			</#list>
		</ul>
	</div>
</#if>