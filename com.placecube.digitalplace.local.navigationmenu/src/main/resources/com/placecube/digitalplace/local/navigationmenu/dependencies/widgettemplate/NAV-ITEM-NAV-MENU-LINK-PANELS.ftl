
<#if serviceLocator??>
	<#assign
		assetEntries = infoCollectionProviderService.getInfoCollection() 
		infoCollectionProviderService = digitalplace_infoCollectionProviderService 
		categoryNavigationService = digitalplace_categoryNavigationService
		journalArticleRetrievalService = digitalplace_journalArticleRetrievalService
		journalArticleService = serviceLocator.findService("com.liferay.journal.service.JournalArticleService")
		sortedCategoryLinks = categoryNavigationService.getAssetEntriesAndMenuItemsSortedByName(assetEntries, entries, locale)
	/>
	
	<#if sortedCategoryLinks?has_content>
		<div class="nav-menu-link-panels-navigation-widget">
			<div class="row">
				<#list sortedCategoryLinks as link>
					<#if link.isNavItem()>
						<#assign navItem = link.wrappedObject />
						<#if navItem.layout?has_content>
							<div class="col-md-4 nav-item-entry">
								<h2 class="h3 page-title">
									<a href="${navItem.regularURL}">
										${navItem.name}
									</a>
								</h2>
								<p class="page-description">
									${navItem.layout.description}
								</p>
							</div>
						</#if>
						
					<#else>
						<#assign curEntry = link.wrappedObject />
						<#assign journalArticle = journalArticleService.getLatestArticle(curEntry.getClassPK()) />
						<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />
		
						<#if articleURLOptional.isPresent()>
							<#assign articleURL = articleURLOptional.get() />
						<#else>
							<#assign articleURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
						</#if>
		
						<div class="col-md-4 nav-item-entry">
							<h2 class="h3 page-title">
								<a href="${articleURL}">
									${curEntry.getTitle(locale)}
								</a>
							</h2>
		
							<#assign articleSummary = journalArticle.getDescription(locale) />
							<#if articleSummary?has_content>
								<p class="page-description">
								 	${articleSummary}
								</p>
							</#if>
							
						</div>
					
					</#if>
				</#list>
			</div>	
		</div>
	</#if>
</#if>
