<#if entries?has_content>
	<div class="nav-menu-page-category-link">
		<#assign 
			assetCategoryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")
			categoryRetrievalService = digitalplace_categoryRetrievalService
			navItemTemplateService = digitalplace_navItemTemplateService
			iconVocabularyId = navItemTemplateService.getIconVocabularyId(themeDisplay.getCompanyId()) 
		/>
		
		<div class="section-content card-deck">
			<#list entries as navigationEntry>

				<#assign entryCssClasses = categoryRetrievalService.getCategoriesPropertyValues("com.liferay.portal.kernel.model.Layout", navigationEntry.getLayout().getPlid(), "css-style-classes", iconVocabularyId) />
				
				<#if !entryCssClasses?has_content>
					<#assign entryCssClasses = "glyphicon glyphicon-arrow-right" />
				<#else>
					<#assign entryCssClasses = entryCssClasses?replace(",", " ") />
				</#if>
				
				<div class="card-cell">
					<div class="popular-page card">
						<div class="card-icon">
							<span class="padded-round-icon-large ${entryCssClasses}"></span>
						</div>
						
						<div class="card-body">
							<h2 class="h3 card-title">							
								<a href="${navigationEntry.getRegularURL()}">
									${navigationEntry.getName()}
								</a>
							</h2>
							
							<p class="card-text">
								${navigationEntry.getLayout().getDescription()}
							</p>
						</div>
					</div>
				</div>

			</#list>
		</div>	
	</div>
</#if>