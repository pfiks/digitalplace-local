<#if serviceLocator??>
    <#assign
    	categoryNavigationService = digitalplace_categoryNavigationService
		journalArticleRetrievalService = digitalplace_journalArticleRetrievalService
		sortedCategoryLinks = categoryNavigationService.getMenuItemsSortedByName(entries)
    />

    <#if sortedCategoryLinks?has_content>
		<div class="featured-content-nav-panels-assets-widget">
			<div class="section-content card-deck">
                <#list sortedCategoryLinks as link>
					<div class="card-cell">
						<div class="popular-page card">
							<div class="card-body">
                                <#if link.isPage()>
                                    <#assign navItem = link.wrappedObject />
									<h2 class="h3 card-title">
										<a href="${navItem.regularURL}">
                                            ${navItem.name}
										</a>
									</h2>
									<p class="card-text">
                                        ${navItem.layout.description}
									</p>
                                <#else>
                                    <#assign journalArticle = link.wrappedObject />
                                    <#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />

                                    <#if articleURLOptional.isPresent()>
                                        <#assign articleURL = articleURLOptional.get() />
                                    </#if>

									<h2 class="h3 card-title">
										<a href="${articleURL}">
                                            ${journalArticle.getTitle(locale)}
										</a>
									</h2>

                                    <#assign articleSummary = journalArticle.getDescription(locale) />
                                    <#if articleSummary?has_content>
										<p class="card-text">
                                            ${articleSummary}
										</p>
                                    </#if>
                                </#if>
							</div>

							<div class="card-icon">
								<span class="padded-round-icon glyphicon glyphicon-arrow-right"></span>
							</div>
						</div>
					</div>
                </#list>
			</div>
		</div>
    <#else>
		<div class="featured-content-nav-panels-assets-widget-no-values">
			<script type="text/javascript">
			if($('.featured-content-nav-panels-assets-widget-no-values').parents('div[class*="page-editor"]').length===0){
				var containerDiv = $('.featured-content-nav-panels-assets-widget-no-values').closest('.lfr-layout-structure-item-container');
                    var containerDivHeight = $(containerDiv).height();
                    if (containerDivHeight <= 40) {
                        $(containerDiv).addClass('hide');
                    }
                }
			</script>
		</div>
    </#if>
</#if>