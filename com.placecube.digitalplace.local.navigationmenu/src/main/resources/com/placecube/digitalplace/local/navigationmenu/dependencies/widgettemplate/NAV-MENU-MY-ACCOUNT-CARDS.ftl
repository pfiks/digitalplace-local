<#if entries?has_content>

	<div class="nav-menu-my-account-links">
		<div class="card-deck">
			<#list entries as navigationEntry>
				<div class="card">
				    <div class="card-body">
					    <h3 class="card-title"><a href="${navigationEntry.getRegularURL()}">${navigationEntry.getName()}</a></h3>
					    <p class="card-text">${navigationEntry.getLayout().getDescription()}</p>
					</div>
				</div>
			</#list>
		</div>
	</div>

</#if>