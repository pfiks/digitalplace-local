<#if entries?has_content>

	<div class="nav-menu-my-account-links">
		<div class=" row">
			<#list entries as navigationEntry>
				<div class="col-md-4">
					<h2 class="h3"><a href="${navigationEntry.getRegularURL()}">${navigationEntry.getName()}</a></h2>
					<p>${navigationEntry.getLayout().getDescription()}</p>
				</div>
			</#list>
		</div>
	</div>

</#if>
