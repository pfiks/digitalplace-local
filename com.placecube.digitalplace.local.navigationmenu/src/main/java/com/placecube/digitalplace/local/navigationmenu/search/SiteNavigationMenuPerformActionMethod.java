package com.placecube.digitalplace.local.navigationmenu.search;

import java.util.Objects;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.site.navigation.model.SiteNavigationMenu;

public class SiteNavigationMenuPerformActionMethod implements ActionableDynamicQuery.PerformActionMethod<SiteNavigationMenu> {

	private IndexableActionableDynamicQuery indexableActionableDynamicQuery;

	private SiteNavigationMenuIndexer indexer;

	private static final Log LOG = LogFactoryUtil.getLog(SiteNavigationMenuPerformActionMethod.class);

	public SiteNavigationMenuPerformActionMethod(IndexableActionableDynamicQuery indexableActionableDynamicQuery, SiteNavigationMenuIndexer indexer) {
		this.indexer = indexer;
		this.indexableActionableDynamicQuery = indexableActionableDynamicQuery;
	}

	@Override
	public void performAction(SiteNavigationMenu entry) {

		try {
			if (Objects.nonNull(indexableActionableDynamicQuery) && Objects.nonNull(indexer)) {
				Document document = indexer.getDocument(entry);
				indexableActionableDynamicQuery.addDocuments(document);
			}

		} catch (PortalException pe) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("Unable to index entry " + entry.getSiteNavigationMenuId(), pe);
			}
		}
	}

	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return indexableActionableDynamicQuery;
	}

	public SiteNavigationMenuIndexer getIndexer() {
		return indexer;
	}
}
