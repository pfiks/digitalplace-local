package com.placecube.digitalplace.local.navigationmenu.helper;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.navigation.constants.SiteNavigationConstants;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.type.SiteNavigationMenuItemTypeRegistry;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = SiteNavigationMenItemHelper.class)
public class SiteNavigationMenItemHelper {

	public static final String MENU_ITEM_TYPE_JOURNAL = "com.liferay.journal.model.JournalArticle";
	public static final String MENU_ITEM_TYPE_LAYOUT = "layout";
	public static final String MENU_ITEM_TYPE_URL = "url";	

	private static final Log LOG = LogFactoryUtil.getLog(SiteNavigationMenItemHelper.class);

	@Reference
	private Portal portal;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private SiteNavigationMenuItemTypeRegistry siteNavigationMenuItemTypeRegistry;

	public boolean isSecondaryNavigation(SiteNavigationMenu siteNavigationMenu) {
		return SiteNavigationConstants.TYPE_SECONDARY == siteNavigationMenu.getType();
	}

	public boolean isSocialNavigation(SiteNavigationMenu siteNavigationMenu) {
		return SiteNavigationConstants.TYPE_SOCIAL == siteNavigationMenu.getType();
	}

	public List<SiteNavigationMenuItem> getSortedSiteNavigationMenuItems(List<SiteNavigationMenuItem> siteNavigationMenuItems) {

		return siteNavigationMenuItems.stream().sorted(Comparator.comparingInt(SiteNavigationMenuItem::getOrder)).collect(Collectors.toList());

	}

	public UnicodeProperties getSiteNavigationMenuItemProperties(SiteNavigationMenuItem siteNavigationMenuItem) {

		UnicodeProperties properties = new UnicodeProperties();

		properties.fastLoad(siteNavigationMenuItem.getTypeSettings());

		return properties;
	}

	public String getMenuItemName(UnicodeProperties properties, ThemeDisplay themeDisplay) {

		String defaultLanguageId = properties.getProperty("defaultLanguageId");

		if (Validator.isNotNull(defaultLanguageId)) {
			String localisedLanguageName = properties.getProperty("name_" + themeDisplay.getLanguageId());

			return localisedLanguageName != null ? localisedLanguageName : properties.getProperty("name_" + defaultLanguageId);
		} else {
			return properties.getProperty("title");
		}

	}

	public String getMenuItemUrl(SiteNavigationMenuItem siteNavigationMenuItem, UnicodeProperties properties, ThemeDisplay themeDisplay) throws PortalException {

		if (MENU_ITEM_TYPE_URL.equalsIgnoreCase(siteNavigationMenuItem.getType())) {

			return properties.getProperty("url");

		} else if (MENU_ITEM_TYPE_LAYOUT.equalsIgnoreCase(siteNavigationMenuItem.getType())) {

			Layout navigationLayout;
			try {

				navigationLayout = layoutLocalService.getLayoutByUuidAndGroupId(properties.getProperty("layoutUuid"), GetterUtil.getLong(properties.getProperty("groupId")),
						GetterUtil.getBoolean(properties.getProperty("privateLayout")));
			} catch (PortalException e) {
				return StringPool.BLANK;
			}

			return navigationLayout.getRegularURL(themeDisplay.getRequest());
		} else if (MENU_ITEM_TYPE_JOURNAL.equalsIgnoreCase(siteNavigationMenuItem.getType())) {

			try {
				return siteNavigationMenuItemTypeRegistry.getSiteNavigationMenuItemType(siteNavigationMenuItem).getRegularURL(themeDisplay.getRequest(), siteNavigationMenuItem);
			} catch (Exception e) {
				LOG.error(String.format("Unable to retrieve menu item url. Type:%s Menu Item Id:%d", siteNavigationMenuItem.getType(), siteNavigationMenuItem.getSiteNavigationMenuItemId()), e);
				return StringPool.BLANK;
			}

		}

		return StringPool.BLANK;
	}

	public Optional<Layout> getMenuItemLayout(SiteNavigationMenuItem siteNavigationMenuItem, UnicodeProperties properties) {

		if (MENU_ITEM_TYPE_LAYOUT.equalsIgnoreCase(siteNavigationMenuItem.getType())) {
			try {
				Layout navigationLayout = layoutLocalService.getLayoutByUuidAndGroupId(properties.getProperty("layoutUuid"), GetterUtil.getLong(properties.getProperty("groupId")),
						GetterUtil.getBoolean(properties.getProperty("privateLayout")));

				return Optional.of(navigationLayout);
			} catch (PortalException e) {
				LOG.error(e);
			}
		}

		return Optional.empty();
	}

	public boolean isRedirectToCurrentWebsite(LayoutSet layoutSet, String redirectUrl) throws URISyntaxException {

		Map<String,String> currentUriHosts = portal.getVirtualHostnames(layoutSet);
		URI redirectUri = new URI(redirectUrl);
		String redirectUriHost = redirectUri.getHost();

		return redirectUriHost == null || currentUriHosts.containsKey(redirectUriHost);

	}

}
