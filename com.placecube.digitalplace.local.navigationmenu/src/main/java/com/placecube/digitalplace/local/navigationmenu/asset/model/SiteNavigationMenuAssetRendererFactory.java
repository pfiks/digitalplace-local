package com.placecube.digitalplace.local.navigationmenu.asset.model;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;

@Component(immediate = true, property = "javax.portlet.name=" + JournalPortletKeys.JOURNAL, service = AssetRendererFactory.class)
public class SiteNavigationMenuAssetRendererFactory extends BaseAssetRendererFactory<SiteNavigationMenu> {

	public static final String TYPE = "siteNavigationMenu";

	@Reference
	private SiteNavigationMenuLocalService siteNavigationMenuLocalService;

	@Reference
	private SiteNavigationMenuAssetRendererFactoryHelper siteNavigationMenuAssetRendererFactoryHelper;

	@Reference(target = "(osgi.web.symbolicname=com.liferay.journal.web)")
	private ServletContext servletContext;

	public SiteNavigationMenuAssetRendererFactory() {
		setClassName(SiteNavigationMenu.class.getName());
		setLinkable(true);
		setPortletId(JournalPortletKeys.JOURNAL);
		setSearchable(false);
	}

	@Override
	public AssetRenderer<SiteNavigationMenu> getAssetRenderer(long classPK, int type) throws PortalException {

		SiteNavigationMenuAssetRenderer siteNavigationMenuAssetRenderer = siteNavigationMenuAssetRendererFactoryHelper
				.createSiteNavigationMenuAssetRenderer(siteNavigationMenuLocalService.getSiteNavigationMenu(classPK));

		siteNavigationMenuAssetRenderer.setAssetRendererType(type);
		siteNavigationMenuAssetRenderer.setServletContext(servletContext);

		return siteNavigationMenuAssetRenderer;
	}

	@Override
	public AssetRenderer<SiteNavigationMenu> getAssetRenderer(long groupId, String name) throws PortalException {

		List<SiteNavigationMenu> entries = siteNavigationMenuLocalService.getSiteNavigationMenus(groupId);

		Optional<SiteNavigationMenu> optionalSiteNavMenu = entries.stream().filter(x -> x.getName().equals(name)).findFirst();

		if (optionalSiteNavMenu.isPresent()) {
			SiteNavigationMenuAssetRenderer siteNavigationMenuAssetRenderer = siteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(optionalSiteNavMenu.get());

			siteNavigationMenuAssetRenderer.setServletContext(servletContext);

			return siteNavigationMenuAssetRenderer;

		} else {
			throw new PortalException("Site navigation not found");
		}
	}

	@Override
	public String getTypeName(Locale locale) {
		ResourceBundle resourceBundle = ResourceBundleUtil.getBundle(locale, this.getClass());

		String modelResourceNamePrefix = ResourceActionsUtil.getModelResourceNamePrefix();

		String key = modelResourceNamePrefix.concat(getClassName());

		String value = LanguageUtil.get(resourceBundle, key, null);

		return value == null ? super.getTypeName(locale) : value;
	}

	@Override
	public String getClassName() {
		return SiteNavigationMenu.class.getName();
	}

	@Override
	public String getIconCssClass() {
		return "list";
	}

	@Override
	public String getType() {
		return TYPE;
	}

}