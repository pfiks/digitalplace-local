package com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_0_1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;

public class SiteNavigationMenuItemTypeSettingsUpgradeProcess extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(SiteNavigationMenuItemTypeSettingsUpgradeProcess.class);
	
	private static final String MENU_ITEM_TYPE_JOURNAL = "journal";
	
	private static final String MENU_ITEM_TYPE_JOURNAL_ARTICLE = "com.liferay.journal.model.JournalArticle";
	
	private final AssetEntryLocalService assetEntryLocalService;
	
	private final SiteNavigationMenuItemLocalService siteNavigationMenuItemLocalService;
	
	public SiteNavigationMenuItemTypeSettingsUpgradeProcess(AssetEntryLocalService assetEntryLocalService, SiteNavigationMenuItemLocalService siteNavigationMenuItemLocalService) {
		this.assetEntryLocalService = assetEntryLocalService;
		this.siteNavigationMenuItemLocalService = siteNavigationMenuItemLocalService;
	}
	

	@Override
	protected void doUpgrade() throws Exception {
		LOG.info("Initialised upgrade process for SiteNavigationMenuItemTableUpdate records");
		
		List<SiteNavigationMenuItem> siteNavigationMenuItems = siteNavigationMenuItemLocalService.getSiteNavigationMenuItems(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		
		for (SiteNavigationMenuItem siteNavigationMenuItem : siteNavigationMenuItems) {
			if (MENU_ITEM_TYPE_JOURNAL.equals(siteNavigationMenuItem.getType())) {
				
				siteNavigationMenuItem.setType(MENU_ITEM_TYPE_JOURNAL_ARTICLE);
				
				UnicodeProperties properties = new UnicodeProperties();
				properties.fastLoad(siteNavigationMenuItem.getTypeSettings());
				AssetEntry assetEntry = assetEntryLocalService.fetchEntry(MENU_ITEM_TYPE_JOURNAL_ARTICLE, Long.parseLong(properties.get("assetClassPk")));
				Map<String, String> typeSettings = new HashMap<String, String>();
				
				if(Validator.isNotNull(assetEntry)) {
					typeSettings.put("className", MENU_ITEM_TYPE_JOURNAL_ARTICLE);
					typeSettings.put("classNameId", Long.toString(assetEntry.getClassNameId()));
					typeSettings.put("classPK", Long.toString(assetEntry.getClassPK()));
					typeSettings.put("classTypeId", Long.toString(assetEntry.getClassTypeId()));
					typeSettings.put("title", assetEntry.getTitle(properties.get("defaultLanguageId")));
					typeSettings.put("type", "Web Content Article");
				}
				
				properties = new UnicodeProperties(typeSettings, false);
				siteNavigationMenuItem.setTypeSettings(properties.toString());

				siteNavigationMenuItemLocalService.updateSiteNavigationMenuItem(siteNavigationMenuItem);
			}
		}
		
		LOG.info("Completed upgrade process for SiteNavigationMenuItemTableUpdate records");
	}

}
