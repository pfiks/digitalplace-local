package com.placecube.digitalplace.local.navigationmenu.asset.model;

import org.osgi.service.component.annotations.Component;

import com.liferay.site.navigation.model.SiteNavigationMenu;

@Component(immediate = true, service = SiteNavigationMenuAssetRendererFactoryHelper.class)
public class SiteNavigationMenuAssetRendererFactoryHelper {

	public SiteNavigationMenuAssetRenderer createSiteNavigationMenuAssetRenderer(SiteNavigationMenu siteNavigationMenu) {
		return new SiteNavigationMenuAssetRenderer(siteNavigationMenu);
	}
}