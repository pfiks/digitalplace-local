package com.placecube.digitalplace.local.navigationmenu.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "navigation", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.navigationmenu.configuration.NavigationMenuCompanyConfiguration", localization = "content/Language", name = "navigation-menu")
public interface NavigationMenuCompanyConfiguration {

	@Meta.AD(required = false, deflt = "0", name = "icon-vocabulary-id")
	long iconVocabularyId();

}