package com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_1_0;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;

import java.util.List;

public class NavigationMenuWidgetTemplateUpgrade extends UpgradeProcess {

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(NavigationMenuWidgetTemplateUpgrade.class);

	public NavigationMenuWidgetTemplateUpgrade(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		upgradeWidgetTemplate(companyId, NavigationMenuWidgetTemplate.NAV_MENU_LINK_PANELS);
		upgradeWidgetTemplate(companyId, NavigationMenuWidgetTemplate.NAV_MENU_POPULAR_PAGES_WEB_CONTENT_NAV_PANELS);
		upgradeWidgetTemplate(companyId, NavigationMenuWidgetTemplate.NAV_MENU_PAGE_CATEGORY_LINK);
	}

	private void upgradeWidgetTemplate(long companyId, NavigationMenuWidgetTemplate template) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("templateKey", template.getKey()));

		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		if (ddmTemplates.isEmpty()) {
			LOG.warn("Template " + template.getKey() + " not found in company " + companyId);
		} else {
			ddmTemplates.forEach(this::updateTemplate);
		}

	}

	private void updateTemplate(DDMTemplate ddmTemplate) {
		String newScript = getAmendedScript(ddmTemplate.getScript());
		ddmTemplate.setScript(newScript);
		ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
	}

	private String getAmendedScript(String script) {
		return script
				.replace("serviceLocator.findService(\"com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService\")",
						"digitalplace_infoCollectionProviderService")
				.replace("serviceLocator.findService(\"com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService\")", "digitalplace_categoryNavigationService")
				.replace("serviceLocator.findService(\"com.placecube.journal.service.JournalArticleRetrievalService\")", "digitalplace_journalArticleRetrievalService")
				.replace("serviceLocator.findService(\"com.placecube.digitalplace.category.service.CategoryRetrievalService\")", "digitalplace_categoryRetrievalService")
				.replace("serviceLocator.findService(\"com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService\")", "digitalplace_navItemTemplateService");
	}

}
