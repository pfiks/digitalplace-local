package com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_0_2;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;

public class DDMTemplateTableUpdate extends UpgradeProcess {

	private final DDMTemplateLocalService ddmTemplateLocalService;

	public DDMTemplateTableUpdate(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("templateKey", NavigationMenuWidgetTemplate.NAV_MENU_POPULAR_PAGES_WEB_CONTENT_NAV_PANELS.getKey()));
		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		for (DDMTemplate ddmTemplate : ddmTemplates) {
			ddmTemplate.setScript(ddmTemplate.getScript().replace("fragments-editor", "page-editor").replace("container.py-3", "lfr-layout-structure-item-container"));
			ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
		}
	}

}
