package com.placecube.digitalplace.local.navigationmenu.constants;

public enum NavigationMenuWidgetTemplate {

	MY_ACCOUNT("NAV-MENU-MY-ACCOUNT", "My Account Page"),

	MY_ACCOUNT_CARDS("NAV-MENU-MY-ACCOUNT-CARDS", "My Account Cards"),

	NAV_MENU_COLLAPSIBLE_BOX("NAV-MENU-COLLAPSIBLE-BOX", "Nav Menu Collapsible Box"),

	NAV_MENU_LINK_PANELS("NAV-ITEM-NAV-MENU-LINK-PANELS", "Category Page Link Panels"),

	NAV_MENU_PAGE_CATEGORY_LINK("NAV-MENU-PAGE-CATEGORY-LINK", "Nav Menu Page Category Link"),

	NAV_MENU_POPULAR_PAGES_WEB_CONTENT_NAV_PANELS("NAV-ITEM-NAV-MENU-POPULAR-PAGES-WEB-CONTENT-NAV-PANELS", "Nav Menu Popular Pages and Web Content panels");

	private final String key;
	private final String name;

	private NavigationMenuWidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}
}
