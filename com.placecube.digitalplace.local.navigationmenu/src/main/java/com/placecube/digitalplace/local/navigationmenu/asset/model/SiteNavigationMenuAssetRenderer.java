package com.placecube.digitalplace.local.navigationmenu.asset.model;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseJSPAssetRenderer;
import com.liferay.portal.kernel.trash.TrashRenderer;
import com.liferay.site.navigation.model.SiteNavigationMenu;

public class SiteNavigationMenuAssetRenderer extends BaseJSPAssetRenderer<SiteNavigationMenu> implements TrashRenderer {

	private final SiteNavigationMenu siteNavigationMenu;

	public SiteNavigationMenuAssetRenderer(SiteNavigationMenu siteNavigationMenu) {
		this.siteNavigationMenu = siteNavigationMenu;
	}

	@Override
	public SiteNavigationMenu getAssetObject() {
		return siteNavigationMenu;
	}

	@Override
	public String getClassName() {
		return SiteNavigationMenu.class.getName();
	}

	@Override
	public long getClassPK() {
		return siteNavigationMenu.getPrimaryKey();
	}

	@Override
	public long getGroupId() {
		return siteNavigationMenu.getGroupId();
	}

	@Override
	public String getJspPath(HttpServletRequest httpServletRequest, String template) {
		return null;
	}

	@Override
	public String getPortletId() {
		AssetRendererFactory<SiteNavigationMenu> assetRendererFactory = getAssetRendererFactory();

		return assetRendererFactory.getPortletId();
	}

	@Override
	public String getSummary(PortletRequest portletRequest, PortletResponse portletResponse) {
		return siteNavigationMenu.getName();
	}

	@Override
	public String getTitle(Locale locale) {
		return siteNavigationMenu.getName();
	}

	@Override
	public String getType() {
		return SiteNavigationMenuAssetRendererFactory.TYPE;
	}

	@Override
	public long getUserId() {
		return siteNavigationMenu.getUserId();
	}

	@Override
	public String getUserName() {
		return siteNavigationMenu.getUserName();
	}

	@Override
	public String getUuid() {
		return siteNavigationMenu.getUuid();
	}

}