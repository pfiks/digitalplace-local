package com.placecube.digitalplace.local.navigationmenu.service;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.NavItem;
import com.placecube.digitalplace.local.navigationmenu.configuration.NavigationMenuCompanyConfiguration;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = NavItemTemplateService.class)
public class NavItemTemplateService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDMInitializer ddmInitializer;

	public long getIconVocabularyId(long companyId) throws PortalException {

		NavigationMenuCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(NavigationMenuCompanyConfiguration.class, companyId);

		return configuration.iconVocabularyId();

	}

	public DDMTemplate getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = "com/placecube/digitalplace/local/navigationmenu/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), NavItem.class, templateResourcePath,
				getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}
}
