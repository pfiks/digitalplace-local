package com.placecube.digitalplace.local.navigationmenu.upgrade;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_0_0.DDMTemplateTableUpdate;
import com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_0_1.SiteNavigationMenuItemTypeSettingsUpgradeProcess;
import com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_1_0.NavigationMenuWidgetTemplateUpgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class NavigationMenuUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private Portal portal;

	@Reference
	private SiteNavigationMenuItemLocalService siteNavigationMenuItemLocalService;

	@Reference
	private NavItemTemplateService navItemTemplateService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new DDMTemplateTableUpdate(ddmTemplateLocalService, portal));
		registry.register("1.0.0", "1.0.1", new SiteNavigationMenuItemTypeSettingsUpgradeProcess(assetEntryLocalService, siteNavigationMenuItemLocalService));
		registry.register("1.0.1", "1.0.2", new com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_0_2.DDMTemplateTableUpdate(ddmTemplateLocalService));
		registry.register("1.0.2", "1.1.0", new NavigationMenuWidgetTemplateUpgrade(ddmTemplateLocalService));
	}

}
