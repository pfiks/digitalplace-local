package com.placecube.digitalplace.local.navigationmenu.search;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;

@Component(immediate = true, service = Indexer.class)
public class SiteNavigationMenuIndexer extends BaseIndexer<SiteNavigationMenu> {

	public static final String CLASS_NAME = SiteNavigationMenu.class.getName();

	public static final Log LOG = LogFactoryUtil.getLog(SiteNavigationMenuIndexer.class);

	@Reference
	private SiteNavigationMenuLocalService siteNavigationMenuLocalService;

	@Reference
	private SiteNavigationMenuIndexerHelper siteNavigationMenuIndexerHelper;

	@Reference
	private IndexWriterHelper indexWriterHelper;

	@Reference
	private Portal portal;

	public SiteNavigationMenuIndexer() {
		setDefaultSelectedFieldNames(Field.COMPANY_ID, Field.ENTRY_CLASS_NAME, Field.ENTRY_CLASS_PK, Field.UID, Field.GROUP_ID);
		setDefaultSelectedLocalizedFieldNames(Field.TITLE);
	}

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) throws Exception {
		addSearchLocalizedTerm(searchQuery, searchContext, Field.TITLE, false);
	}

	@Override
	protected void doDelete(SiteNavigationMenu entry) throws Exception {
		deleteDocument(entry.getCompanyId(), entry.getSiteNavigationMenuId());
	}

	@Override
	protected Document doGetDocument(SiteNavigationMenu entry) throws Exception {
		Document document = getBaseModelDocument(CLASS_NAME, entry);
		document.addDate(Field.MODIFIED_DATE, entry.getModifiedDate());

		Locale defaultLocale = portal.getSiteDefaultLocale(entry.getGroupId());
		String localizedTitle = LocalizationUtil.getLocalizedName(Field.TITLE, defaultLocale.toString());

		document.addText(localizedTitle, entry.getName());

		return document;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest, PortletResponse portletResponse) throws Exception {

		Summary summary = createSummary(document);

		summary.setMaxContentLength(200);

		return summary;
	}

	@Override
	protected void doReindex(SiteNavigationMenu entry) throws Exception {

		Document document = getDocument(entry);
		indexWriterHelper.updateDocument(entry.getCompanyId(), document);
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {

		SiteNavigationMenu entry = siteNavigationMenuLocalService.getSiteNavigationMenu(classPK);
		doReindex(entry);
	}

	@Override
	protected void doReindex(String[] ids) throws Exception {

		if (ids != null && ids.length > 0) {
			long companyId = GetterUtil.getLong(ids[0]);
			reindexEntries(companyId);
		}
	}

	@Override
	public String[] getDefaultSelectedFieldNames() {
		return super.getDefaultSelectedFieldNames();
	}

	@Override
	public String[] getDefaultSelectedLocalizedFieldNames() {
		return super.getDefaultSelectedLocalizedFieldNames();
	}

	private void reindexEntries(long companyId) throws PortalException {

		final IndexableActionableDynamicQuery indexableActionableDynamicQuery = siteNavigationMenuLocalService.getIndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setCompanyId(companyId);

		SiteNavigationMenuPerformActionMethod performActionMethod = siteNavigationMenuIndexerHelper.createSiteNavigationMenuPerformActionMethod(indexableActionableDynamicQuery, this);

		indexableActionableDynamicQuery.setPerformActionMethod(performActionMethod);
		indexableActionableDynamicQuery.performActions();
	}

}