package com.placecube.digitalplace.local.navigationmenu.search;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;

@Component(immediate = true, service = SiteNavigationMenuIndexerHelper.class)
public class SiteNavigationMenuIndexerHelper {

	public SiteNavigationMenuPerformActionMethod createSiteNavigationMenuPerformActionMethod(IndexableActionableDynamicQuery indexableActionableDynamicQuery,
			SiteNavigationMenuIndexer siteNavigationMenuIndexer) {
		return new SiteNavigationMenuPerformActionMethod(indexableActionableDynamicQuery, siteNavigationMenuIndexer);
	}
}
