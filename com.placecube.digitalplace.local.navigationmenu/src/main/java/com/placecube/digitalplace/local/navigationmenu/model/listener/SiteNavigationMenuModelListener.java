package com.placecube.digitalplace.local.navigationmenu.model.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.site.navigation.model.SiteNavigationMenu;

@Component(immediate = true, service = { ModelListener.class })
public class SiteNavigationMenuModelListener extends BaseModelListener<SiteNavigationMenu> {

	private static final Log LOG = LogFactoryUtil.getLog(SiteNavigationMenuModelListener.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Override
	public void onAfterCreate(SiteNavigationMenu model) throws ModelListenerException {
		updateEntry(model);
	}

	@Override
	public void onAfterUpdate(SiteNavigationMenu originalModel, SiteNavigationMenu model)
			throws ModelListenerException {
		updateEntry(model);
	}
	
	@Override
	public void onAfterRemove(SiteNavigationMenu model) throws ModelListenerException {
		deleteEntry(model);
	}

	private void updateEntry(SiteNavigationMenu siteNavigationMenu) {

		try {

			assetEntryLocalService.updateEntry(siteNavigationMenu.getUserId(), siteNavigationMenu.getGroupId(), siteNavigationMenu.getCreateDate(), siteNavigationMenu.getModifiedDate(),
					SiteNavigationMenu.class.getName(), siteNavigationMenu.getPrimaryKey(), siteNavigationMenu.getUuid(), 0, null, null, true, true, null, null, siteNavigationMenu.getCreateDate(),
					null, ContentTypes.TEXT_HTML, siteNavigationMenu.getName(), null, null, null, null, 0, 0, null);

			Indexer<SiteNavigationMenu> indexer = IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class);

			indexer.reindex(siteNavigationMenu);
		} catch (PortalException e) {
			LOG.error(e);
		}
	}

	private void deleteEntry(SiteNavigationMenu siteNavigationMenu) {
		try {
			assetEntryLocalService.deleteEntry(SiteNavigationMenu.class.getName(), siteNavigationMenu.getPrimaryKey());

			Indexer<SiteNavigationMenu> indexer = IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class);
			indexer.delete(siteNavigationMenu);
		} catch (PortalException e) {
			LOG.error(e);
		}
	}
}
