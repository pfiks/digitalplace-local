package com.placecube.digitalplace.local.navigationmenu.upgrade.upgrade_1_0_0;

import java.util.List;
import java.util.Locale;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;

public class DDMTemplateTableUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(DDMTemplateTableUpdate.class);

	private static final String TEMPLATE_KEY = "NAV-ITEM-NAV-MENU-LINK-PANELS";

	private static final String TEMPLATE_NAME = "Category Page Link Panels";

	private final DDMTemplateLocalService ddmTemplateLocalService;
	
	private final Portal portal;

	public DDMTemplateTableUpdate(DDMTemplateLocalService ddmTemplateLocalService, Portal portal) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
		this.portal = portal;
	}

	@Override
	protected void doUpgrade() throws Exception {
		LOG.info("Initialised upgrade process for DDMTemplate records");

		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.getDDMTemplates(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		String templateResourcePath = "com/placecube/digitalplace/local/navigationmenu/dependencies/widgettemplate/" + TEMPLATE_KEY + ".ftl";
		String templateContent = StringUtil.read(getClass().getClassLoader(), templateResourcePath);

		for (DDMTemplate ddmTemplate : ddmTemplates) {
			if (TEMPLATE_KEY.equals(ddmTemplate.getTemplateKey())) {
				Locale locale = portal.getSiteDefaultLocale(ddmTemplate.getGroupId());
				ddmTemplate.setName(TEMPLATE_NAME, locale);
				ddmTemplate.setScript(templateContent);

				ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
			}
		}

		LOG.info("Completed upgrade process for DDMTemplate records");
	}

}
