package com.placecube.digitalplace.local.navigationmenu.constants;

import com.liferay.site.navigation.constants.SiteNavigationConstants;

public enum NavigationMenuEntry {

	FOOTER_LINKS("Footer Links", SiteNavigationConstants.TYPE_SECONDARY),

	GLOBAL_NAVIGATION("Global Navigation", SiteNavigationConstants.TYPE_PRIMARY),

	MY_ACCOUNT_LINKS("My Account Links", SiteNavigationConstants.TYPE_DEFAULT),

	SOCIAL_MEDIA_LINKS("Social Media Links", SiteNavigationConstants.TYPE_SOCIAL);

	private final String name;
	private final int type;

	private NavigationMenuEntry(String name, int type) {
		this.name = name;
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
