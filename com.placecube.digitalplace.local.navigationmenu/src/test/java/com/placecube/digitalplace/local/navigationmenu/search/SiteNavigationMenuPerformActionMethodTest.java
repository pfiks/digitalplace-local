package com.placecube.digitalplace.local.navigationmenu.search;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.site.navigation.model.SiteNavigationMenu;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.search.BaseIndexer", "com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery",
		"com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery" })
public class SiteNavigationMenuPerformActionMethodTest {

	private SiteNavigationMenuPerformActionMethod siteNavigationMenuPerformActionMethod;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Mock
	private SiteNavigationMenuIndexer mockSiteNavigationMenuIndexer;

	@Mock
	private Document mockDocument;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Before
	public void setUp() {
		siteNavigationMenuPerformActionMethod = new SiteNavigationMenuPerformActionMethod(mockIndexableActionableDynamicQuery, mockSiteNavigationMenuIndexer);
	}

	@Test
	public void performAction_WhenIndexableActionableDynamicQueryAndIndexerAreNotNull_ThenAddsEntryDocumentToIndexableActionableDynamicQuery() throws Exception {
		when(mockSiteNavigationMenuIndexer.getDocument(mockSiteNavigationMenu)).thenReturn(mockDocument);

		siteNavigationMenuPerformActionMethod.performAction(mockSiteNavigationMenu);

		verify(mockIndexableActionableDynamicQuery, times(1)).addDocuments(mockDocument);
	}

	@Test
	public void performAction_WhenIndexerIsNull_ThenDoesNotAddEntryDocumentToIndexableActionableDynamicQuery() throws Exception {
		siteNavigationMenuPerformActionMethod = new SiteNavigationMenuPerformActionMethod(null, mockSiteNavigationMenuIndexer);

		siteNavigationMenuPerformActionMethod.performAction(mockSiteNavigationMenu);

		verify(mockIndexableActionableDynamicQuery, never()).addDocuments(any(Document.class));
	}

	@Test
	public void performAction_WhenIndexableActionableDynamicQueryIsNull_ThenDoesNotRetrieveDocument() throws Exception {
		siteNavigationMenuPerformActionMethod = new SiteNavigationMenuPerformActionMethod(null, mockSiteNavigationMenuIndexer);

		siteNavigationMenuPerformActionMethod.performAction(mockSiteNavigationMenu);

		verify(mockSiteNavigationMenuIndexer, never()).getDocument(mockSiteNavigationMenu);
	}

	@Test
	public void performAction_WhenDocumentRetrievalFails_ThenDoesNotThrowException() throws Exception {
		when(mockSiteNavigationMenuIndexer.getDocument(mockSiteNavigationMenu)).thenThrow(new SearchException());

		try {
			siteNavigationMenuPerformActionMethod.performAction(mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected");
		}
	}

	@Test
	public void performAction_WhenDocumentAdditionToDynamicQueryFails_ThenDoesNotThrowException() throws Exception {
		when(mockSiteNavigationMenuIndexer.getDocument(mockSiteNavigationMenu)).thenReturn(mockDocument);
		doThrow(new PortalException()).when(mockIndexableActionableDynamicQuery).addDocuments(mockDocument);

		try {
			siteNavigationMenuPerformActionMethod.performAction(mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected");
		}
	}

}
