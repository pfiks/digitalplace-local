package com.placecube.digitalplace.local.navigationmenu.helper;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.site.navigation.constants.SiteNavigationConstants;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.type.SiteNavigationMenuItemType;
import com.liferay.site.navigation.type.SiteNavigationMenuItemTypeRegistry;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SiteNavigationMenItemHelperTest extends PowerMockito {

	private static final String GROUP_ID = "865";
	private static final String LAYOUT_UUID = "2352332";
	private static final String PRIVATE_LAYOUT = "true";
	private static final TreeMap<String,String> VIRTUAL_HOSTNAMES = new TreeMap<String,String>();
	
	static {
		VIRTUAL_HOSTNAMES.put("www.placecube.com", "www.placecube.com");
	}

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItem;

	@Mock
	private SiteNavigationMenuItemType mockSiteNavigationMenuItemType;

	@Mock
	private SiteNavigationMenuItemTypeRegistry mockSiteNavigationMenuItemTypeRegistry;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutSet mockLayoutSet;

	@Mock
	private Portal mockPortal;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@InjectMocks
	private SiteNavigationMenItemHelper siteNavigationMenItemHelper;

	@Test
	public void getMenuItemName_WhenAnyParameterIsNull_ThenThrowsNullPointerException() {
		siteNavigationMenItemHelper.getMenuItemName(mockUnicodeProperties, mockThemeDisplay);
	}

	@Test
	public void getMenuItemName_WhenDefaultLangIdNotSetInProperties_ThenNameIsTakenFromTitleProperty() {

		when(mockUnicodeProperties.getProperty("defaultLanguageId")).thenReturn("");

		siteNavigationMenItemHelper.getMenuItemName(mockUnicodeProperties, mockThemeDisplay);

		verify(mockUnicodeProperties, times(1)).getProperty("title");

	}

	@Test
	public void getMenuItemName_WhenDefaultLangIdSetInPropertiesAndNoThemeLangId_ThenNameIsTakenFromDefaultLang() {

		when(mockUnicodeProperties.getProperty("defaultLanguageId")).thenReturn("en");
		when(mockThemeDisplay.getLanguageId()).thenReturn("");
		when(mockUnicodeProperties.getProperty("name_en")).thenReturn("test_item_name");

		String menuItemName = siteNavigationMenItemHelper.getMenuItemName(mockUnicodeProperties, mockThemeDisplay);

		assertThat(menuItemName, equalTo("test_item_name"));

	}

	@Test
	public void getMenuItemName_WhenDefaultLangIdSetInPropertiesAndThemeLangId_ThenNameIsTakenFromThemeLang() {
		when(mockUnicodeProperties.getProperty("defaultLanguageId")).thenReturn("en");
		when(mockThemeDisplay.getLanguageId()).thenReturn("de");
		when(mockUnicodeProperties.getProperty("name_de")).thenReturn("test_item_name");

		String menuItemName = siteNavigationMenItemHelper.getMenuItemName(mockUnicodeProperties, mockThemeDisplay);

		assertThat(menuItemName, equalTo("test_item_name"));
	}

	@Test
	public void getMenuItemUrl_WhenMenuItemTypeIsUrl_ThenUrlValueFromPropertiesIsReturned() throws PortalException {
		String expectedUrl = "properties-url";
		when(mockSiteNavigationMenuItem.getType()).thenReturn("url");
		when(mockUnicodeProperties.getProperty("url")).thenReturn(expectedUrl);

		String returnedUrl = siteNavigationMenItemHelper.getMenuItemUrl(mockSiteNavigationMenuItem, mockUnicodeProperties, mockThemeDisplay);

		assertThat(returnedUrl, equalTo(expectedUrl));
	}

	@Test
	public void getMenuItemUrl_WhenTypeIsLayoutAndExceptionOccurs_ThenBlankStringIsReturned() throws PortalException {
		String regularUrl = "regularUrl";
		mockUnicodeProperties();
		when(mockLayoutLocalService.getLayoutByUuidAndGroupId(LAYOUT_UUID, GetterUtil.getLong(GROUP_ID), GetterUtil.getBoolean(PRIVATE_LAYOUT))).thenThrow(new PortalException());
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockLayout.getRegularURL(mockHttpServletRequest)).thenReturn(regularUrl);
		when(mockSiteNavigationMenuItem.getType()).thenReturn("layout");

		String returnedUrl = siteNavigationMenItemHelper.getMenuItemUrl(mockSiteNavigationMenuItem, mockUnicodeProperties, mockThemeDisplay);

		assertThat(returnedUrl, equalTo(StringPool.BLANK));

	}

	@Test
	public void getMenuItemUrl_WhenMenuItemTypeIsLayout_ThenUrlValueFromLayoutServiceIsReturned() throws PortalException {
		String regularUrl = "regularUrl";
		mockUnicodeProperties();
		when(mockLayoutLocalService.getLayoutByUuidAndGroupId(LAYOUT_UUID, GetterUtil.getLong(GROUP_ID), GetterUtil.getBoolean(PRIVATE_LAYOUT))).thenReturn(mockLayout);
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockLayout.getRegularURL(mockHttpServletRequest)).thenReturn(regularUrl);
		when(mockSiteNavigationMenuItem.getType()).thenReturn("layout");

		String returnedUrl = siteNavigationMenItemHelper.getMenuItemUrl(mockSiteNavigationMenuItem, mockUnicodeProperties, mockThemeDisplay);

		verify(mockLayoutLocalService, times(1)).getLayoutByUuidAndGroupId(LAYOUT_UUID, GetterUtil.getLong(GROUP_ID), GetterUtil.getBoolean(PRIVATE_LAYOUT));
		assertThat(returnedUrl, equalTo(regularUrl));

	}

	@Test
	public void getMenuItemUrl_WhenMenuItemTypeIsJournalAndExceptionOccurs_ThenBlankStringIsReturned() throws Exception {
		String friendlyUrl = "friendlyUrl";
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockSiteNavigationMenuItem.getType()).thenReturn("com.liferay.journal.model.JournalArticle");
		when(mockSiteNavigationMenuItemTypeRegistry.getSiteNavigationMenuItemType(mockSiteNavigationMenuItem)).thenReturn(mockSiteNavigationMenuItemType);
		when(mockSiteNavigationMenuItemType.getRegularURL(mockHttpServletRequest, mockSiteNavigationMenuItem)).thenReturn(friendlyUrl);

		String returnedUrl = siteNavigationMenItemHelper.getMenuItemUrl(mockSiteNavigationMenuItem, mockUnicodeProperties, mockThemeDisplay);

		assertThat(returnedUrl, equalTo(friendlyUrl));

	}

	@Test
	public void getMenuItemUrl_WhenMenuItemTypeIsJournal_ThenUrlFromJournalIsReturned() throws Exception {

		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockSiteNavigationMenuItem.getType()).thenReturn("journal");
		when(mockSiteNavigationMenuItemTypeRegistry.getSiteNavigationMenuItemType(mockSiteNavigationMenuItem)).thenReturn(mockSiteNavigationMenuItemType);
		when(mockSiteNavigationMenuItemType.getRegularURL(mockHttpServletRequest, mockSiteNavigationMenuItem)).thenThrow(new Exception());

		String returnedUrl = siteNavigationMenItemHelper.getMenuItemUrl(mockSiteNavigationMenuItem, mockUnicodeProperties, mockThemeDisplay);

		assertThat(returnedUrl, equalTo(StringPool.BLANK));

	}

	@Test
	public void getMenuItemUrl_WhenUnknownType_ThenBlankStringIsReturned() throws PortalException {

		when(mockSiteNavigationMenuItem.getType()).thenReturn("unknown");

		String returnedUrl = siteNavigationMenItemHelper.getMenuItemUrl(mockSiteNavigationMenuItem, mockUnicodeProperties, mockThemeDisplay);

		assertThat(returnedUrl, equalTo(StringPool.BLANK));

	}

	@Test
	public void getSiteNavigationMenuItemProperties_WhenMenuItemIsSuppliedWithEmptySettings_ThenEmptyPropertiesReturned() {

		when(mockSiteNavigationMenuItem.getTypeSettings()).thenReturn("");

		UnicodeProperties properties = siteNavigationMenItemHelper.getSiteNavigationMenuItemProperties(mockSiteNavigationMenuItem);

		assertThat(properties.isEmpty(), equalTo(true));

	}

	@Test
	public void getSiteNavigationMenuItemProperties_WhenMenuItemIsSuppliedWithSettings_ThenPropertiesWithSettingsReturned() {

		when(mockSiteNavigationMenuItem.getTypeSettings()).thenReturn("test_val=25");

		UnicodeProperties properties = siteNavigationMenItemHelper.getSiteNavigationMenuItemProperties(mockSiteNavigationMenuItem);

		assertThat(properties.getProperty("test_val"), equalTo("25"));

	}

	@Test
	public void getSortedSiteNavigationMenuItems_WhenSortedMenuItemsSupplied_ThenSortedItemsAreReturned() {

		List<SiteNavigationMenuItem> mockedSortedMenuItems = getMockedMenuItemList(true);

		List<SiteNavigationMenuItem> sortedMenuItems = siteNavigationMenItemHelper.getSortedSiteNavigationMenuItems(mockedSortedMenuItems);

		assertThat(sortedMenuItems, contains(mockedSortedMenuItems.get(0), mockedSortedMenuItems.get(1)));

	}

	@Test
	public void getSortedSiteNavigationMenuItems_WhenUnsortedMenuItemsSupplied_ThenSortedItemsAreReturned() {

		List<SiteNavigationMenuItem> mockedunSortedMenuItems = getMockedMenuItemList(false);

		List<SiteNavigationMenuItem> sortedMenuItems = siteNavigationMenItemHelper.getSortedSiteNavigationMenuItems(mockedunSortedMenuItems);

		assertThat(sortedMenuItems, contains(mockedunSortedMenuItems.get(1), mockedunSortedMenuItems.get(0)));

	}

	@Test(expected = NullPointerException.class)
	public void isSecondaryNavigation_WhenNavigationMenuIsNull_ThenThrowsNullPointerException() {

		boolean isSecondaryNavigationType = siteNavigationMenItemHelper.isSecondaryNavigation(null);

		assertThat(isSecondaryNavigationType, equalTo(false));

	}

	@Test
	public void isSecondaryNavigation_WhenNavigationMenuTypeIsSecondary_ThenReturnsTrue() {

		when(mockSiteNavigationMenu.getType()).thenReturn(SiteNavigationConstants.TYPE_SECONDARY);

		boolean isSecondaryNavigationType = siteNavigationMenItemHelper.isSecondaryNavigation(mockSiteNavigationMenu);

		assertThat(isSecondaryNavigationType, equalTo(true));

	}

	@Test
	public void isSecondaryNavigation_WhenNavigationMenuTypeNotSecondary_ThenReturnsFalse() {

		when(mockSiteNavigationMenu.getType()).thenReturn(SiteNavigationConstants.TYPE_SOCIAL);

		boolean isSecondaryNavigationType = siteNavigationMenItemHelper.isSecondaryNavigation(mockSiteNavigationMenu);

		assertThat(isSecondaryNavigationType, equalTo(false));

	}

	@Test
	public void isSocialNavigation_WhenNavigationMenuTypeIsSocial_ThenReturnsTrue() {

		when(mockSiteNavigationMenu.getType()).thenReturn(SiteNavigationConstants.TYPE_SOCIAL);

		boolean isSocialNavigationType = siteNavigationMenItemHelper.isSocialNavigation(mockSiteNavigationMenu);

		assertThat(isSocialNavigationType, equalTo(true));

	}

	@Test
	public void isSocialNavigation_WhenNavigationMenuTypeNotSocial_ThenReturnsFalse() {

		when(mockSiteNavigationMenu.getType()).thenReturn(SiteNavigationConstants.TYPE_SECONDARY);

		boolean isSocialNavigationType = siteNavigationMenItemHelper.isSocialNavigation(mockSiteNavigationMenu);

		assertThat(isSocialNavigationType, equalTo(false));

	}

	@Test(expected = NullPointerException.class)
	public void isSocialNavigation_WhenNavigationMenuIsNull_ThenThrowsNullPointerException() {

		boolean isSocialNavigationType = siteNavigationMenItemHelper.isSocialNavigation(null);

		assertThat(isSocialNavigationType, equalTo(false));
	}

	@Test
	public void getMenuItemLayout_WhenMenuItemTypeIsNotLayout_ThenReturnsEmptyOptional() {
		when(mockSiteNavigationMenuItem.getType()).thenReturn(SiteNavigationMenItemHelper.MENU_ITEM_TYPE_URL);

		Optional<Layout> result = siteNavigationMenItemHelper.getMenuItemLayout(mockSiteNavigationMenuItem, mockUnicodeProperties);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getMenuItemLayout_WhenMenuItemTypeIsLayout_AndLayoutDoesNotExist_ThenReturnsEmptyOptional() throws Exception {
		mockUnicodeProperties();
		when(mockSiteNavigationMenuItem.getType()).thenReturn(SiteNavigationMenItemHelper.MENU_ITEM_TYPE_LAYOUT);
		when(mockLayoutLocalService.getLayoutByUuidAndGroupId(LAYOUT_UUID, GetterUtil.getLong(GROUP_ID), GetterUtil.getBoolean(PRIVATE_LAYOUT))).thenThrow(new PortalException());

		Optional<Layout> result = siteNavigationMenItemHelper.getMenuItemLayout(mockSiteNavigationMenuItem, mockUnicodeProperties);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getMenuItemLayout_WhenMenuItemTypeIsLayout_AndLayoutExists_ThenReturnsOptionalOfLayout() throws Exception {
		mockUnicodeProperties();
		when(mockSiteNavigationMenuItem.getType()).thenReturn(SiteNavigationMenItemHelper.MENU_ITEM_TYPE_LAYOUT);
		when(mockLayoutLocalService.getLayoutByUuidAndGroupId(LAYOUT_UUID, GetterUtil.getLong(GROUP_ID), GetterUtil.getBoolean(PRIVATE_LAYOUT))).thenReturn(mockLayout);

		Optional<Layout> result = siteNavigationMenItemHelper.getMenuItemLayout(mockSiteNavigationMenuItem, mockUnicodeProperties);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockLayout));
	}

	@Test(expected = URISyntaxException.class)
	public void isRedirectToCurrentWebsite_WhenUrlIsNotValid_ThenThrowsException() throws URISyntaxException {
		when(mockPortal.getVirtualHostnames(mockLayoutSet)).thenReturn(VIRTUAL_HOSTNAMES);
		siteNavigationMenItemHelper.isRedirectToCurrentWebsite(mockLayoutSet, "INVALID URL");
	}

	@Test
	@Parameters({ //
			"https://www.placecube.com/ABC", //
			"https://www.placecube.com?a=b", //
			"https://www.placecube.com/DEF", //
			"/RelativePath" //
	})
	public void isRedirectToCurrentWebsite_WhenWebsitesAreTheSame_ThenReturnsTrue(String redirectUrl) throws URISyntaxException {
		when(mockPortal.getVirtualHostnames(mockLayoutSet)).thenReturn(VIRTUAL_HOSTNAMES);
		assertThat(true, equalTo(siteNavigationMenItemHelper.isRedirectToCurrentWebsite(mockLayoutSet, redirectUrl)));
	}

	@Test
	public void isRedirectToCurrentWebsite_WhenWebsitesAreDifferent_ThenReturnsFalse() throws URISyntaxException {
		when(mockPortal.getVirtualHostnames(mockLayoutSet)).thenReturn(VIRTUAL_HOSTNAMES);
		assertThat(false, equalTo(siteNavigationMenItemHelper.isRedirectToCurrentWebsite(mockLayoutSet, "https://www.pfiks.com")));
	}

	private void mockUnicodeProperties() {
		when(mockUnicodeProperties.getProperty("layoutUuid")).thenReturn(LAYOUT_UUID);
		when(mockUnicodeProperties.getProperty("groupId")).thenReturn(GROUP_ID);
		when(mockUnicodeProperties.getProperty("privateLayout")).thenReturn(PRIVATE_LAYOUT);
	}

	private List<SiteNavigationMenuItem> getMockedMenuItemList(boolean inOrder) {

		SiteNavigationMenuItem item1 = mock(SiteNavigationMenuItem.class);
		when(item1.getOrder()).thenReturn(1);
		SiteNavigationMenuItem item2 = mock(SiteNavigationMenuItem.class);
		when(item2.getOrder()).thenReturn(2);
		List<SiteNavigationMenuItem> menuItems = new ArrayList<>();
		if (inOrder) {
			menuItems.add(item1);
			menuItems.add(item2);
		} else {
			menuItems.add(item2);
			menuItems.add(item1);
		}

		return menuItems;
	}
}
