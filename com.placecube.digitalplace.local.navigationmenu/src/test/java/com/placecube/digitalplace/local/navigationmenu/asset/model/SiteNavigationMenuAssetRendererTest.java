package com.placecube.digitalplace.local.navigationmenu.asset.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.site.navigation.model.SiteNavigationMenu;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.model.BaseAssetRenderer" })
public class SiteNavigationMenuAssetRendererTest {

	private SiteNavigationMenuAssetRenderer siteNavigationMenuAssetRenderer;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private SiteNavigationMenuAssetRendererFactory mockSiteNavigationMenuAssetRendererFactory;

	@Before
	public void setUp() {
		siteNavigationMenuAssetRenderer = new SiteNavigationMenuAssetRenderer(mockSiteNavigationMenu);
	}

	@Test
	public void getAssetObject_WhenNoErrors_ThenReturnsSiteNavigationMenuAsset() {
		assertThat(siteNavigationMenuAssetRenderer.getAssetObject(), sameInstance(mockSiteNavigationMenu));
	}

	@Test
	public void getClassName_WhenNoErrors_ThenReturnsSiteNavigationMenuClassName() {
		assertThat(siteNavigationMenuAssetRenderer.getClassName(), equalTo(SiteNavigationMenu.class.getName()));
	}

	@Test
	public void getClassPK_WhenNoErrors_ThenReturnsSiteNavigationMenuPriomaryKey() {
		final long classPK = 2344L;
		when(mockSiteNavigationMenu.getPrimaryKey()).thenReturn(classPK);
		assertThat(siteNavigationMenuAssetRenderer.getClassPK(), equalTo(classPK));
	}

	@Test
	public void getGroupId_WhenNoErrors_ThenReturnsSiteNavigationMenuGroupId() {
		final long groupId = 2344L;
		when(mockSiteNavigationMenu.getGroupId()).thenReturn(groupId);
		assertThat(siteNavigationMenuAssetRenderer.getGroupId(), equalTo(groupId));
	}

	@Test
	public void getJspPath_WhenNoErrors_ThenReturnsNull() {
		assertThat(siteNavigationMenuAssetRenderer.getJspPath(mockHttpServletRequest, "template"), nullValue());
	}

	@Test
	public void getPortletId_WhenNoErrors_ThenReturnsAssetRendererFactoryPortletId() {
		final SiteNavigationMenuAssetRenderer spiedSiteNavigationMenuAssetRenderer = spy(siteNavigationMenuAssetRenderer);
		final String portletId = "portletId";

		doReturn(mockSiteNavigationMenuAssetRendererFactory).when(spiedSiteNavigationMenuAssetRenderer).getAssetRendererFactory();
		when(mockSiteNavigationMenuAssetRendererFactory.getPortletId()).thenReturn(portletId);

		assertThat(spiedSiteNavigationMenuAssetRenderer.getPortletId(), equalTo(portletId));
	}

	@Test
	public void getSummary_WhenNoErrors_ThenReturnsSiteNavigationMenuName() {
		final String name = "menu";

		when(mockSiteNavigationMenu.getName()).thenReturn(name);

		assertThat(siteNavigationMenuAssetRenderer.getSummary(mockPortletRequest, mockPortletResponse), equalTo(name));
	}

	@Test
	public void getTitle_WhenNoErrors_ThenReturnsSiteNavigationMenuName() {
		final String name = "menu";

		when(mockSiteNavigationMenu.getName()).thenReturn(name);

		assertThat(siteNavigationMenuAssetRenderer.getTitle(Locale.UK), equalTo(name));
	}

	@Test
	public void getType_WhenNoErrors_ThenReturnsSiteNavigationMenuAssetRendererFactoryType() {
		assertThat(siteNavigationMenuAssetRenderer.getType(), equalTo(SiteNavigationMenuAssetRendererFactory.TYPE));
	}

	@Test
	public void getUserId_WhenNoErrors_ThenReturnsSiteNavigationMenuUserId() {
		final long userId = 453L;

		when(mockSiteNavigationMenu.getUserId()).thenReturn(userId);

		assertThat(siteNavigationMenuAssetRenderer.getUserId(), equalTo(userId));
	}

	@Test
	public void getUserName_WhenNoErrors_ThenReturnsSiteNavigationMenuUserName() {
		final String userName = "user";

		when(mockSiteNavigationMenu.getUserName()).thenReturn(userName);

		assertThat(siteNavigationMenuAssetRenderer.getUserName(), equalTo(userName));
	}

	@Test
	public void getUuid_WhenNoErrors_ThenReturnsSiteNavigationMenuUuid() {
		final String uuid = "adhuhugsdfuis";

		when(mockSiteNavigationMenu.getUuid()).thenReturn(uuid);

		assertThat(siteNavigationMenuAssetRenderer.getUuid(), equalTo(uuid));
	}
}
