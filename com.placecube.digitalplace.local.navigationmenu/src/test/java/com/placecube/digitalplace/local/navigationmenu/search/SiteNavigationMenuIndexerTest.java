package com.placecube.digitalplace.local.navigationmenu.search;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.util.SystemBundleUtil;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.search.BaseIndexer", "com.liferay.portal.kernel.search.DocumentImpl", "com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery", "com.liferay.portal.kernel.module.util.SystemBundleUtil",
		"com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery" })
@PrepareForTest({ LocalizationUtil.class, SystemBundleUtil.class })
public class SiteNavigationMenuIndexerTest {

	private static final long SITE_NAVIGATION_MENU_ID = 9775L;
	private static final long COMPANY_ID = 34776565l;
	private static final long GROUP_ID = 2311L;
	private static final String NAME = "name";
	private static final Date MODIFIED_DATE = new Date();

	private SiteNavigationMenuIndexer siteNavigationMenuIndexer;

	@Mock
	private SiteNavigationMenuLocalService mockSiteNavigationMenuLocalService;

	@Mock
	private IndexWriterHelper mockIndexWriterHelper;

	@Mock
	private SiteNavigationMenuIndexerHelper mockSiteNavigationMenuIndexerHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private Document mockDocument;

	@Mock
	private Summary mockSummary;

	@Mock
	private BooleanQuery mockSearchQuery;

	@Mock
	private BooleanFilter mockBooleanFilter;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Mock
	private SiteNavigationMenuPerformActionMethod mockSiteNavigationMenuPerformActionMethod;

	@Before
	public void setUp() {
		mockStatic(LocalizationUtil.class, SystemBundleUtil.class);
		when(SystemBundleUtil.getBundleContext()).thenReturn(mockBundleContext);

		siteNavigationMenuIndexer = new SiteNavigationMenuIndexer();
		Whitebox.setInternalState(siteNavigationMenuIndexer, "siteNavigationMenuLocalService", mockSiteNavigationMenuLocalService);
		Whitebox.setInternalState(siteNavigationMenuIndexer, "siteNavigationMenuIndexerHelper", mockSiteNavigationMenuIndexerHelper);
		Whitebox.setInternalState(siteNavigationMenuIndexer, "indexWriterHelper", mockIndexWriterHelper);
		Whitebox.setInternalState(siteNavigationMenuIndexer, "portal", mockPortal);
	}

	@Test
	public void new_WhenNoErrors_ThenSetsDefaultSelectedFieldNames() {
		String[] defaultSelectedFieldNames = siteNavigationMenuIndexer.getDefaultSelectedFieldNames();

		List<String> defaultSelectedFieldNamesList = Arrays.asList(defaultSelectedFieldNames);

		assertThat(defaultSelectedFieldNamesList.size(), equalTo(5));
		assertThat(defaultSelectedFieldNamesList.contains(Field.COMPANY_ID), equalTo(true));
		assertThat(defaultSelectedFieldNamesList.contains(Field.ENTRY_CLASS_NAME), equalTo(true));
		assertThat(defaultSelectedFieldNamesList.contains(Field.ENTRY_CLASS_PK), equalTo(true));
		assertThat(defaultSelectedFieldNamesList.contains(Field.UID), equalTo(true));
		assertThat(defaultSelectedFieldNamesList.contains(Field.GROUP_ID), equalTo(true));
	}

	@Test
	public void new_WhenNoErrors_ThenSetsDefaultSelectedLocalizedFieldNames() {
		String[] defaultSelectedLocalizedFieldNames = siteNavigationMenuIndexer.getDefaultSelectedLocalizedFieldNames();

		assertThat(defaultSelectedLocalizedFieldNames.length, equalTo(1));
		assertThat(defaultSelectedLocalizedFieldNames[0], equalTo(Field.TITLE));
	}

	@Test
	public void getClassName_WhenNoErrors_ThenReturnsSiteNavigationMenuClassName() {
		assertThat(siteNavigationMenuIndexer.getClassName(), equalTo(SiteNavigationMenu.class.getName()));
	}

	@Test
	public void postProcessSearchQuery_WhenNoErrors_ThenAddsTitleToSearchLocalizedTerms() throws Exception {
		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		spiedSiteNavigationMenuIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);

		verifyPrivate(spiedSiteNavigationMenuIndexer, times(1)).invoke("addSearchLocalizedTerm", mockSearchQuery, mockSearchContext, Field.TITLE, false);
	}

	@Test(expected = Exception.class)
	public void postProcessSearchQuery_WhenAddSearchLocalizedTermFails_ThenThrowsException() throws Exception {
		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doThrow(new Exception()).when(spiedSiteNavigationMenuIndexer, "addSearchLocalizedTerm", mockSearchQuery, mockSearchContext, Field.TITLE, false);

		spiedSiteNavigationMenuIndexer.postProcessSearchQuery(mockSearchQuery, mockBooleanFilter, mockSearchContext);
	}

	@Test
	public void doDelete_WhenNoErrors_ThenDeletesSiteNavigationMenuDocument() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doNothing().when(spiedSiteNavigationMenuIndexer, "deleteDocument", COMPANY_ID, SITE_NAVIGATION_MENU_ID);

		spiedSiteNavigationMenuIndexer.doDelete(mockSiteNavigationMenu);

		verifyPrivate(spiedSiteNavigationMenuIndexer, times(1)).invoke("deleteDocument", COMPANY_ID, SITE_NAVIGATION_MENU_ID);
	}

	@Test(expected = Exception.class)
	public void doDelete_WhenDeletionFails_ThenThrowsException() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doThrow(new Exception()).when(spiedSiteNavigationMenuIndexer, "deleteDocument", COMPANY_ID, SITE_NAVIGATION_MENU_ID);

		spiedSiteNavigationMenuIndexer.doDelete(mockSiteNavigationMenu);
	}

	@Test
	public void doGetDocument_WhenNoErrors_ThenReturnsDocument() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getBaseModelDocument", SiteNavigationMenu.class.getName(), mockSiteNavigationMenu);
		when(mockPortal.getSiteDefaultLocale(GROUP_ID)).thenReturn(Locale.UK);

		Document result = spiedSiteNavigationMenuIndexer.doGetDocument(mockSiteNavigationMenu);

		assertThat(result, sameInstance(mockDocument));
	}

	@Test
	public void doGetDocument_WhenNoErrors_ThenAddsMenuModifiedDateFieldToDocument() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getBaseModelDocument", SiteNavigationMenu.class.getName(), mockSiteNavigationMenu);
		when(mockPortal.getSiteDefaultLocale(GROUP_ID)).thenReturn(Locale.UK);

		spiedSiteNavigationMenuIndexer.doGetDocument(mockSiteNavigationMenu);

		verify(mockDocument, times(1)).addDate(Field.MODIFIED_DATE, MODIFIED_DATE);
	}

	@Test
	public void doGetDocument_WhenNoErrors_ThenAddsMenuNameAsLocalizedTitleToDocument() throws Exception {
		final String localizedTitle = "Title_UK";
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getBaseModelDocument", SiteNavigationMenu.class.getName(), mockSiteNavigationMenu);
		when(mockPortal.getSiteDefaultLocale(GROUP_ID)).thenReturn(Locale.UK);
		when(LocalizationUtil.getLocalizedName(Field.TITLE, Locale.UK.toString())).thenReturn(localizedTitle);

		spiedSiteNavigationMenuIndexer.doGetDocument(mockSiteNavigationMenu);

		verify(mockDocument, times(1)).addText(localizedTitle, NAME);
	}

	@Test(expected = PortalException.class)
	public void doGetDocument_WhenDefaultLocaleRetrievalFails_ThenThrowsPortalException() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getBaseModelDocument", SiteNavigationMenu.class.getName(), mockSiteNavigationMenu);
		when(mockPortal.getSiteDefaultLocale(GROUP_ID)).thenThrow(new PortalException());

		spiedSiteNavigationMenuIndexer.doGetDocument(mockSiteNavigationMenu);
	}

	@Test
	public void doGetSummary_WhenNoErrors_ThenReturnsDefaultSummary() throws Exception {
		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockSummary).when(spiedSiteNavigationMenuIndexer, "createSummary", mockDocument);

		Summary result = spiedSiteNavigationMenuIndexer.doGetSummary(mockDocument, Locale.UK, StringPool.BLANK, mockPortletRequest, mockPortletResponse);

		assertThat(result, sameInstance(mockSummary));
	}

	@Test
	public void doGetSummary_WhenNoErrors_ThenSetsMaxContentLength() throws Exception {
		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockSummary).when(spiedSiteNavigationMenuIndexer, "createSummary", mockDocument);

		spiedSiteNavigationMenuIndexer.doGetSummary(mockDocument, Locale.UK, StringPool.BLANK, mockPortletRequest, mockPortletResponse);

		verify(mockSummary, times(1)).setMaxContentLength(200);
	}

	@Test(expected = Exception.class)
	public void doGetSummary_WhenSummaryBuildingFails_ThenThrowsException() throws Exception {
		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doThrow(new Exception()).when(spiedSiteNavigationMenuIndexer, "createSummary", mockDocument);

		spiedSiteNavigationMenuIndexer.doGetSummary(mockDocument, Locale.UK, StringPool.BLANK, mockPortletRequest, mockPortletResponse);
	}

	@Test
	public void doReindex_WhenCalledWithSiteNavigationMenu_ThenUpdatesDocumentInIndex() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getDocument", mockSiteNavigationMenu);

		spiedSiteNavigationMenuIndexer.doReindex(mockSiteNavigationMenu);

		verify(mockIndexWriterHelper, times(1)).updateDocument(COMPANY_ID, mockDocument);
	}

	@Test(expected = SearchException.class)
	public void doReindex_WhenCalledWithSiteNavigationMenuAndDocumentRetrievalFails_ThenThrowsSearchException() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doThrow(new SearchException()).when(spiedSiteNavigationMenuIndexer, "getDocument", mockSiteNavigationMenu);

		spiedSiteNavigationMenuIndexer.doReindex(mockSiteNavigationMenu);
	}

	@Test(expected = SearchException.class)
	public void doReindex_WhenCalledWithSiteNavigationMenuAndDocumentUpdateFails_ThenThrowsSearchException() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getDocument", mockSiteNavigationMenu);

		doThrow(new SearchException()).when(mockIndexWriterHelper).updateDocument(COMPANY_ID, mockDocument);

		spiedSiteNavigationMenuIndexer.doReindex(mockSiteNavigationMenu);
	}

	@Test
	public void doReindex_WhenCalledWithClassNameAndClassPK_ThenUpdatesDocumentInIndex() throws Exception {
		mockSiteNavigationMenu();

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenu(SITE_NAVIGATION_MENU_ID)).thenReturn(mockSiteNavigationMenu);

		doReturn(mockDocument).when(spiedSiteNavigationMenuIndexer, "getDocument", mockSiteNavigationMenu);

		spiedSiteNavigationMenuIndexer.doReindex(SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID);

		verify(mockIndexWriterHelper, times(1)).updateDocument(COMPANY_ID, mockDocument);
	}

	@Test(expected = PortalException.class)
	public void doReindex_WhenCalledWithClassNameAndClassPKAndSiteNavigationMenuDoesNotExist_ThenThrowsPortalException() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenu(SITE_NAVIGATION_MENU_ID)).thenThrow(new PortalException());

		siteNavigationMenuIndexer.doReindex(SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID);
	}

	@Test
	public void doReindex_WhenCalledWithIdsArray_ThenSetsPerformActionMethodAndSearchEngineAndPerformsIndexableDynamicQueryActionsInOrder() throws Exception {
		final String[] values = { String.valueOf(COMPANY_ID) };

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		when(mockSiteNavigationMenuLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);

		when(mockSiteNavigationMenuIndexerHelper.createSiteNavigationMenuPerformActionMethod(mockIndexableActionableDynamicQuery, spiedSiteNavigationMenuIndexer))
				.thenReturn(mockSiteNavigationMenuPerformActionMethod);

		spiedSiteNavigationMenuIndexer.doReindex(values);

		InOrder inOrder = inOrder(mockIndexableActionableDynamicQuery);

		inOrder.verify(mockIndexableActionableDynamicQuery, times(1)).setPerformActionMethod(mockSiteNavigationMenuPerformActionMethod);
		inOrder.verify(mockIndexableActionableDynamicQuery, times(1)).performActions();
	}

	@Test
	public void doReindex_WhenCompanyIdValuesIsEmpty_ThenDoesNotReindex() throws Exception {
		siteNavigationMenuIndexer.doReindex(new String[] {});

		verify(mockSiteNavigationMenuLocalService, never()).getIndexableActionableDynamicQuery();
	}

	@Test(expected = PortalException.class)
	public void doReindex_WhenPerformIndexableDynamicQueryActionsFails_ThenThrowsPortalException() throws Exception {
		final String[] values = { String.valueOf(COMPANY_ID) };

		SiteNavigationMenuIndexer spiedSiteNavigationMenuIndexer = spy(siteNavigationMenuIndexer);

		when(mockSiteNavigationMenuLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);

		when(mockSiteNavigationMenuIndexerHelper.createSiteNavigationMenuPerformActionMethod(mockIndexableActionableDynamicQuery, spiedSiteNavigationMenuIndexer))
				.thenReturn(mockSiteNavigationMenuPerformActionMethod);

		doThrow(new PortalException()).when(mockIndexableActionableDynamicQuery).performActions();

		spiedSiteNavigationMenuIndexer.doReindex(values);
	}

	private void mockSiteNavigationMenu() {
		when(mockSiteNavigationMenu.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSiteNavigationMenu.getGroupId()).thenReturn(GROUP_ID);
		when(mockSiteNavigationMenu.getModifiedDate()).thenReturn(MODIFIED_DATE);
		when(mockSiteNavigationMenu.getSiteNavigationMenuId()).thenReturn(SITE_NAVIGATION_MENU_ID);
		when(mockSiteNavigationMenu.getName()).thenReturn(NAME);
	}
}
