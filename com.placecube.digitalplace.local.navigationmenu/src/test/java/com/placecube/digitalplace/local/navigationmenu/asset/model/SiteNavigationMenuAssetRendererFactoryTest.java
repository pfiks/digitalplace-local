package com.placecube.digitalplace.local.navigationmenu.asset.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.model.BaseAssetRenderer" })
@PrepareForTest({ ResourceBundleUtil.class, ResourceActionsUtil.class, LanguageUtil.class })
public class SiteNavigationMenuAssetRendererFactoryTest {

	private static final long CLASS_PK = 34534l;
	private static final long GROUP_ID = 8643l;
	private static final Locale LOCALE = Locale.UK;
	private static final String NAME = "menu";
	private static final int TYPE = 1;

	@InjectMocks
	private SiteNavigationMenuAssetRendererFactory siteNavigationMenuAssetRendererFactory;

	@Mock
	private SiteNavigationMenuLocalService mockSiteNavigationMenuLocalService;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private SiteNavigationMenuAssetRenderer mockSiteNavigationMenuAssetRenderer;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private SiteNavigationMenuAssetRendererFactoryHelper mockSiteNavigationMenuAssetRendererFactoryHelper;

	@Mock
	private ResourceBundle mockResourceBundle;

	@Before
	public void setUp() {
		mockStatic(ResourceBundleUtil.class, ResourceActionsUtil.class, LanguageUtil.class);
	}

	@Test
	public void new_WhenNoErrors_ThenSetsSiteNavigationMenuClassName() {
		assertThat(siteNavigationMenuAssetRendererFactory.getClassName(), equalTo(SiteNavigationMenu.class.getName()));
	}

	@Test
	public void new_WhenNoErrors_ThenSetsJournalPortletId() {
		assertThat(siteNavigationMenuAssetRendererFactory.getPortletId(), equalTo(JournalPortletKeys.JOURNAL));
	}

	@Test
	public void new_WhenNoErrors_ThenSetsLinkableToTrue() {
		assertThat(siteNavigationMenuAssetRendererFactory.isLinkable(), equalTo(true));
	}

	@Test
	public void new_WhenNoErrors_ThenSetsSearchableToFalse() {
		assertThat(siteNavigationMenuAssetRendererFactory.isSearchable(), equalTo(false));
	}

	@Test
	public void getAssetRenderer_WhenCalledWithClassPKAndType_ThenReturnsNotNullSiteNavigationMenuAssetRenderer() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenu(CLASS_PK)).thenReturn(mockSiteNavigationMenu);
		when(mockSiteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(mockSiteNavigationMenu)).thenReturn(mockSiteNavigationMenuAssetRenderer);

		AssetRenderer<SiteNavigationMenu> result = siteNavigationMenuAssetRendererFactory.getAssetRenderer(CLASS_PK, TYPE);

		assertThat(result, sameInstance(mockSiteNavigationMenuAssetRenderer));
	}

	@Test
	public void getAssetRenderer_WhenCalledWithClassPKAndType_ThenSetsTypeInAssetRenderer() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenu(CLASS_PK)).thenReturn(mockSiteNavigationMenu);
		when(mockSiteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(mockSiteNavigationMenu)).thenReturn(mockSiteNavigationMenuAssetRenderer);

		siteNavigationMenuAssetRendererFactory.getAssetRenderer(CLASS_PK, TYPE);

		verify(mockSiteNavigationMenuAssetRenderer, times(1)).setAssetRendererType(TYPE);
	}

	@Test
	public void getAssetRenderer_WhenCalledWithClassPKAndType_ThenSetsServletContextInAssetRenderer() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenu(CLASS_PK)).thenReturn(mockSiteNavigationMenu);
		when(mockSiteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(mockSiteNavigationMenu)).thenReturn(mockSiteNavigationMenuAssetRenderer);

		siteNavigationMenuAssetRendererFactory.getAssetRenderer(CLASS_PK, TYPE);

		verify(mockSiteNavigationMenuAssetRenderer, times(1)).setServletContext(mockServletContext);
	}

	@Test(expected = PortalException.class)
	public void getAssetRenderer_WhenCalledWithClassPKAndTypeAndMenuDoesNotExist_ThenThrowsPortalException() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenu(CLASS_PK)).thenThrow(new PortalException());

		siteNavigationMenuAssetRendererFactory.getAssetRenderer(CLASS_PK, TYPE);
	}

	@Test
	public void getAssetRenderer_WhenCalledWithGroupIdAndNameAndThereAreMenusInGroup_ThenReturnsAssetRendererForFirstSiteNavigationMenuInGroupWithName() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(Arrays.asList(new SiteNavigationMenu[] { mockSiteNavigationMenu }));
		when(mockSiteNavigationMenu.getName()).thenReturn(NAME);
		when(mockSiteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(mockSiteNavigationMenu)).thenReturn(mockSiteNavigationMenuAssetRenderer);

		AssetRenderer<SiteNavigationMenu> result = siteNavigationMenuAssetRendererFactory.getAssetRenderer(GROUP_ID, NAME);

		assertThat(result, sameInstance(mockSiteNavigationMenuAssetRenderer));
	}

	@Test
	public void getAssetRenderer_WhenCalledWithGroupIdAndNameAndThereAreMenusInGroup_ThenSetsServletContextInAssetRenderer() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(Arrays.asList(new SiteNavigationMenu[] { mockSiteNavigationMenu }));
		when(mockSiteNavigationMenu.getName()).thenReturn(NAME);
		when(mockSiteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(mockSiteNavigationMenu)).thenReturn(mockSiteNavigationMenuAssetRenderer);

		siteNavigationMenuAssetRendererFactory.getAssetRenderer(GROUP_ID, NAME);

		verify(mockSiteNavigationMenuAssetRenderer, times(1)).setServletContext(mockServletContext);
	}

	@Test(expected = PortalException.class)
	public void getAssetRenderer_WhenCalledWithGroupIdAndNameAndNoMenusWithNameAreFound_ThenThrowsPortalException() throws Exception {
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(new ArrayList<>());

		siteNavigationMenuAssetRendererFactory.getAssetRenderer(GROUP_ID, NAME);
	}

	@Test
	public void getTypeName_WhenResourceNameLanguagePropertyIsPresentInBundle_ThenReturnsResourceNameLanguagePropertyValue() {
		final String modelResourceNamePrefix = "model.resource.";
		final String typeName = "Navigation Menu";

		when(ResourceBundleUtil.getBundle(LOCALE, SiteNavigationMenuAssetRendererFactory.class)).thenReturn(mockResourceBundle);
		when(ResourceActionsUtil.getModelResourceNamePrefix()).thenReturn(modelResourceNamePrefix);
		when(LanguageUtil.get(mockResourceBundle, modelResourceNamePrefix + SiteNavigationMenu.class.getName(), null)).thenReturn(typeName);

		String result = siteNavigationMenuAssetRendererFactory.getTypeName(LOCALE);

		assertThat(result, equalTo(typeName));
	}

	@Test
	public void getTypeName_WhenResourceNameLanguagePropertyIsNotPresentInBundle_ThenReturnsParentClassTypeNameResult() throws Exception {
		final String modelResourceNamePrefix = "model.resource.";
		final String typeName = "Navigation Menu";

		SiteNavigationMenuAssetRendererFactory spiedSiteNavigationMenuAssetRendererFactory = spy(siteNavigationMenuAssetRendererFactory);

		when(ResourceBundleUtil.getBundle(LOCALE, SiteNavigationMenuAssetRendererFactory.class)).thenReturn(mockResourceBundle);
		when(ResourceActionsUtil.getModelResourceNamePrefix()).thenReturn(modelResourceNamePrefix);
		when(LanguageUtil.get(mockResourceBundle, modelResourceNamePrefix + SiteNavigationMenu.class.getName(), null)).thenReturn(null);

		doReturn(typeName).when(spiedSiteNavigationMenuAssetRendererFactory, "getTypeName", LOCALE);

		String result = spiedSiteNavigationMenuAssetRendererFactory.getTypeName(LOCALE);

		assertThat(result, equalTo(typeName));
	}

	@Test
	public void getClassName_WhenNoErrors_ThenReturnsSiteNavigationMenuClassName() {
		assertThat(siteNavigationMenuAssetRendererFactory.getClassName(), equalTo(SiteNavigationMenu.class.getName()));
	}

	@Test
	public void getIconCssClass_WhenNoErrors_ThenReturnsListIconClass() {
		assertThat(siteNavigationMenuAssetRendererFactory.getIconCssClass(), equalTo("list"));
	}

	@Test
	public void getType_WhenNoErrors_ThenReturnsSiteNavigationMenu() {
		assertThat(siteNavigationMenuAssetRendererFactory.getType(), equalTo("siteNavigationMenu"));
	}

}
