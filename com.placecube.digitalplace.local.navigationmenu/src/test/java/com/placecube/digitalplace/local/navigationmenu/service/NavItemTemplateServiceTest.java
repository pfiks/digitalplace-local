package com.placecube.digitalplace.local.navigationmenu.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.NavItem;
import com.placecube.digitalplace.local.navigationmenu.configuration.NavigationMenuCompanyConfiguration;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

@RunWith(MockitoJUnitRunner.class)
public class NavItemTemplateServiceTest extends PowerMockito {

	private final static long COMPANY_ID = 322L;

	@Mock
	public DDMInitializer mockDDMInitializer;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private NavigationMenuCompanyConfiguration mockNavigationMenuCompanyConfiguration;

	@InjectMocks
	private NavItemTemplateService navItemTemplateService;

	private ClassLoader navItemTemplateServiceClassLoader;

	@Before
	public void setUp() {
		navItemTemplateServiceClassLoader = NavItemTemplateService.class.getClassLoader();
	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenException_ThenThrowsPortalException() throws Exception {

		NavigationMenuWidgetTemplate widgetTemplate = NavigationMenuWidgetTemplate.NAV_MENU_LINK_PANELS;
		String templateResourcePath = "com/placecube/digitalplace/local/navigationmenu/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), NavItem.class, templateResourcePath, navItemTemplateServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		navItemTemplateService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		NavigationMenuWidgetTemplate widgetTemplate = NavigationMenuWidgetTemplate.NAV_MENU_LINK_PANELS;
		String templateResourcePath = "com/placecube/digitalplace/local/navigationmenu/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), NavItem.class, templateResourcePath, navItemTemplateServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = navItemTemplateService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

	@Test
	public void getIconVocabularyId_WhenNoErrors_ThenReturnsIconVocabularyIdConfiguredAtCompanyLevel() throws Exception {
		final long vocabularyId = 123L;

		when(mockConfigurationProvider.getCompanyConfiguration(NavigationMenuCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockNavigationMenuCompanyConfiguration);
		when(mockNavigationMenuCompanyConfiguration.iconVocabularyId()).thenReturn(vocabularyId);

		long result = navItemTemplateService.getIconVocabularyId(COMPANY_ID);

		assertThat(result, equalTo(vocabularyId));
	}

}
