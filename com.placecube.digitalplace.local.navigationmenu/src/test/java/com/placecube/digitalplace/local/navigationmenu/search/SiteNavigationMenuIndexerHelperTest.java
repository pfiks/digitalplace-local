package com.placecube.digitalplace.local.navigationmenu.search;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;

public class SiteNavigationMenuIndexerHelperTest {

	private SiteNavigationMenuIndexerHelper siteNavigationMenuIndexerHelper;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Mock
	private SiteNavigationMenuIndexer mockSiteNavigationMenuIndexer;

	@Before
	public void setUp() {
		siteNavigationMenuIndexerHelper = new SiteNavigationMenuIndexerHelper();
	}

	@Test
	public void createSiteNavigationMenuPerformActionMethod_WhenNoErrors_TheReturnsNotNullSiteNavigationPerformActionMethodWithParameters() {
		SiteNavigationMenuPerformActionMethod result = siteNavigationMenuIndexerHelper.createSiteNavigationMenuPerformActionMethod(mockIndexableActionableDynamicQuery, mockSiteNavigationMenuIndexer);

		assertThat(result, notNullValue());
		assertThat(result.getIndexableActionableDynamicQuery(), sameInstance(mockIndexableActionableDynamicQuery));
		assertThat(result.getIndexer(), sameInstance(mockSiteNavigationMenuIndexer));
	}

}
