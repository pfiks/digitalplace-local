package com.placecube.digitalplace.local.navigationmenu.asset.model;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.site.navigation.model.SiteNavigationMenu;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.model.BaseAssetRenderer" })
public class SiteNavigationMenuAssetRendererFactoryHelperTest {

	private SiteNavigationMenuAssetRendererFactoryHelper siteNavigationMenuAssetRendererFactoryHelper;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Before
	public void setUp() {
		siteNavigationMenuAssetRendererFactoryHelper = new SiteNavigationMenuAssetRendererFactoryHelper();
	}

	@Test
	public void createSiteNavigationMenuAssetRenderer_WhenNoErrors_TheReturnsNotNullSiteNavigationMenuAssetRendererWithSiteNavigationMenu() {
		SiteNavigationMenuAssetRenderer result = siteNavigationMenuAssetRendererFactoryHelper.createSiteNavigationMenuAssetRenderer(mockSiteNavigationMenu);

		assertThat(result, notNullValue());
		assertThat(result.getAssetObject(), sameInstance(mockSiteNavigationMenu));
	}

}
