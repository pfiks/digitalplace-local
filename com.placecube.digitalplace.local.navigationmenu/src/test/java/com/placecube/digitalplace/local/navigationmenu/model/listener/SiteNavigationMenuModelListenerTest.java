package com.placecube.digitalplace.local.navigationmenu.model.listener;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.site.navigation.model.SiteNavigationMenu;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.search.IndexerRegistryUtil" })
@PrepareForTest({ IndexerRegistryUtil.class })
public class SiteNavigationMenuModelListenerTest {

	private static final long SITE_NAVIGATION_MENU_ID = 9775L;
	private static final long USER_ID = 34534L;
	private static final long GROUP_ID = 2311L;
	private static final String NAME = "name";
	private static final String UUID = "jhusofhoh";
	private static final Date CREATE_DATE = new Date();
	private static final Date MODIFIED_DATE = new Date();

	@InjectMocks
	private SiteNavigationMenuModelListener siteNavigationMenuModelListener;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private Indexer<SiteNavigationMenu> mockIndexer;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Before
	public void setUp() {
		mockStatic(IndexerRegistryUtil.class);
	}

	@Test
	public void onAfterCreate_WhenNoErrors_ThenUpdatesAssetEntryAndReindexesSiteNavigationMenuInOrder() throws Exception {
		mockSiteNavigationMenu();
		when(IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class)).thenReturn(mockIndexer);

		siteNavigationMenuModelListener.onAfterCreate(mockSiteNavigationMenu);

		InOrder inOrder = inOrder(mockAssetEntryLocalService, mockIndexer);

		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, CREATE_DATE, MODIFIED_DATE, SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID, UUID, 0, null,
				null, true, true, null, null, CREATE_DATE, null, ContentTypes.TEXT_HTML, NAME, null, null, null, null, 0, 0, null);

		inOrder.verify(mockIndexer, times(1)).reindex(mockSiteNavigationMenu);
	}

	@Test
	public void onAfterCreate_WhenEntryUpdateFails_ThenDoesNotThrowExceptions() throws Exception {
		mockSiteNavigationMenu();

		when(mockAssetEntryLocalService.updateEntry(USER_ID, GROUP_ID, CREATE_DATE, MODIFIED_DATE, SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID, UUID, 0, null, null, true, true, null,
				null, CREATE_DATE, null, ContentTypes.TEXT_HTML, NAME, null, null, null, null, 0, 0, null)).thenThrow(new PortalException());

		try {
			siteNavigationMenuModelListener.onAfterCreate(mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}
	}

	@Test
	public void onAfterCreate_WhenEntryIndexationFails_ThenDoesNotThrowExceptions() throws Exception {
		mockSiteNavigationMenu();

		when(IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class)).thenReturn(mockIndexer);
		doThrow(new SearchException()).when(mockIndexer).reindex(mockSiteNavigationMenu);

		try {
			siteNavigationMenuModelListener.onAfterCreate(mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}
	}

	@Test
	public void onAfterUpdate_WhenNoErrors_ThenUpdatesAssetEntryAndReindexesSiteNavigationMenuInOrder() throws Exception {
		mockSiteNavigationMenu();
		when(IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class)).thenReturn(mockIndexer);

		siteNavigationMenuModelListener.onAfterUpdate(mockSiteNavigationMenu, mockSiteNavigationMenu);

		InOrder inOrder = inOrder(mockAssetEntryLocalService, mockIndexer);

		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, CREATE_DATE, MODIFIED_DATE, SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID, UUID, 0, null,
				null, true, true, null, null, CREATE_DATE, null, ContentTypes.TEXT_HTML, NAME, null, null, null, null, 0, 0, null);

		inOrder.verify(mockIndexer, times(1)).reindex(mockSiteNavigationMenu);
	}

	@Test
	public void onAfterUpdate_WhenEntryUpdateFails_ThenDoesNotThrowExceptions() throws Exception {
		mockSiteNavigationMenu();

		when(mockAssetEntryLocalService.updateEntry(USER_ID, GROUP_ID, CREATE_DATE, MODIFIED_DATE, SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID, UUID, 0, null, null, true, true, null,
				null, CREATE_DATE, null, ContentTypes.TEXT_HTML, NAME, null, null, null, null, 0, 0, null)).thenThrow(new PortalException());

		try {
			siteNavigationMenuModelListener.onAfterUpdate(mockSiteNavigationMenu, mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}
	}

	@Test
	public void onAfterUpdate_WhenEntryIndexationFails_ThenDoesNotThrowExceptions() throws Exception {
		mockSiteNavigationMenu();

		when(IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class)).thenReturn(mockIndexer);
		doThrow(new SearchException()).when(mockIndexer).reindex(mockSiteNavigationMenu);

		try {
			siteNavigationMenuModelListener.onAfterUpdate(mockSiteNavigationMenu, mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}
	}

	@Test
	public void onAfterRemove_WhenNoErrors_ThenRemovesAssetEntryAndDeltesSiteNavigationMenuFromIndexInOrder() throws Exception {
		mockSiteNavigationMenu();
		when(IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class)).thenReturn(mockIndexer);

		siteNavigationMenuModelListener.onAfterRemove(mockSiteNavigationMenu);

		InOrder inOrder = inOrder(mockAssetEntryLocalService, mockIndexer);

		inOrder.verify(mockAssetEntryLocalService, times(1)).deleteEntry(SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID);

		inOrder.verify(mockIndexer, times(1)).delete(mockSiteNavigationMenu);
	}

	@Test
	public void onAfterRemove_WhenEntryDeletionFails_ThenDoesNotThrowExceptions() throws Exception {
		mockSiteNavigationMenu();

		doThrow(new PortalException()).when(mockAssetEntryLocalService).deleteEntry(SiteNavigationMenu.class.getName(), SITE_NAVIGATION_MENU_ID);

		try {
			siteNavigationMenuModelListener.onAfterRemove(mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}
	}

	@Test
	public void onAfterRemove_WhenDocumentDeletionFails_ThenDoesNotThrowExceptions() throws Exception {
		mockSiteNavigationMenu();

		when(IndexerRegistryUtil.nullSafeGetIndexer(SiteNavigationMenu.class)).thenReturn(mockIndexer);
		doThrow(new SearchException()).when(mockIndexer).delete(mockSiteNavigationMenu);

		try {
			siteNavigationMenuModelListener.onAfterRemove(mockSiteNavigationMenu);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}
	}

	private void mockSiteNavigationMenu() {
		when(mockSiteNavigationMenu.getUserId()).thenReturn(USER_ID);
		when(mockSiteNavigationMenu.getGroupId()).thenReturn(GROUP_ID);
		when(mockSiteNavigationMenu.getCreateDate()).thenReturn(CREATE_DATE);
		when(mockSiteNavigationMenu.getModifiedDate()).thenReturn(MODIFIED_DATE);
		when(mockSiteNavigationMenu.getPrimaryKey()).thenReturn(SITE_NAVIGATION_MENU_ID);
		when(mockSiteNavigationMenu.getUuid()).thenReturn(UUID);
		when(mockSiteNavigationMenu.getName()).thenReturn(NAME);
	}
}
