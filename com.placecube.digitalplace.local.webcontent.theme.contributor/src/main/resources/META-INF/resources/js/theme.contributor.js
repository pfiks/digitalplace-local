AUI().ready(
	function () {
		
		setInterval(function () {
			updateJournalArticlesOptionsLabel();
		}, 1000);
	}
);


function updateJournalArticlesOptionsLabel() {
	let journalArticleAssetRenderFactoryOptions = $('#_com_liferay_asset_list_web_portlet_AssetListPortlet_JournalArticleAssetRendererFactoryOptions');

	if (journalArticleAssetRenderFactoryOptions.length == 1 && !isGlobalLabelPresentInWebContentArticlesOptions()) {
		
		Liferay.Service(
			'/classname/fetch-class-name',
			{
				value: 'com.liferay.journal.model.JournalArticle'
			},
			function(className) {

				Liferay.Service(
					'/ddm.ddmstructure/get-structures',
					{
						companyId: Liferay.ThemeDisplay.getCompanyId(),
						groupIds: [Liferay.ThemeDisplay.getCompanyGroupId()],
						classNameId: className.classNameId,
						status: Liferay.Workflow.STATUS_APPROVED
					},
					function(ddmStructures) {
						ddmStructures.forEach(function(ddmStructure) {
							let singleItemSubtypeOption = document.querySelector("#_com_liferay_asset_list_web_portlet_AssetListPortlet_JournalArticleAssetRendererFactoryOptions option[value='" + ddmStructure.structureId + "']");
							if(singleItemSubtypeOption){
								singleItemSubtypeOption.innerHTML = singleItemSubtypeOption.innerHTML + ' (Global)';
							}
							
							
							let choiceSelectorOption = document.querySelector("#_com_liferay_asset_list_web_portlet_AssetListPortlet_JournalArticleAssetRendererFactorycurrentClassTypeIds option[value='" + ddmStructure.structureId + "']");
							if(choiceSelectorOption){
								choiceSelectorOption.innerHTML = choiceSelectorOption.innerHTML + ' (Global)';
							}
							
							let availableSelectorOption = document.querySelector("#_com_liferay_asset_list_web_portlet_AssetListPortlet_JournalArticleAssetRendererFactoryavailableClassTypeIds option[value='" + ddmStructure.structureId + "']");
							if(availableSelectorOption){
								availableSelectorOption.innerHTML = availableSelectorOption.innerHTML + ' (Global)';
							}
						});
					}
				);
			}
		);
	}
}

function isGlobalLabelPresentInWebContentArticlesOptions() {
	let isGlobalLabelPresent = false;
	let optionGroup = document.querySelector("#_com_liferay_asset_list_web_portlet_AssetListPortlet_anyClassTypeJournalArticleAssetRendererFactory optgroup[label='Single Item Subtype']");

	for(const option of optionGroup.children) {
		let optionText = option.innerHTML;
		if (optionText.includes("(Global)")) {
			isGlobalLabelPresent = true;
			break;
		}
	}
	return isGlobalLabelPresent;
}
