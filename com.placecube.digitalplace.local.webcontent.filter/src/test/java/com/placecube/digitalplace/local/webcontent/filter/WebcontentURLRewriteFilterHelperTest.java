package com.placecube.digitalplace.local.webcontent.filter;

import static org.junit.Assert.assertEquals;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.webcontent.filter.configuration.WebContentFilterCompanyConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CompanyThreadLocal.class })
public class WebcontentURLRewriteFilterHelperTest extends PowerMockito {

	private static final long COMPANY_ID = 111l;

	private static final long GROUP_ID = 999l;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private Portal mockPortal;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutSet mockLayoutSet;

	@Mock
	private ServletRequest mockServletRequest;

	@Mock
	private WebContentFilterCompanyConfiguration mockWebContentFilterCompanyConfiguration;

	@InjectMocks
	private WebcontentURLRewriteFilterHelper webcontentURLRewriteFilterHelper;

	@Before
	public void activateSetup() {
		mockStatic(CompanyThreadLocal.class);
	}

	@Test
	public void getCanonicalURL_WhenQuestionMarkPresentInCurrentUrl_ThenTextAfterAndIncludingQuestionMarkIsRemovedFromCurrentUrl() {
		String currentUrl = "/search?stuff_to_be_discarded";
		String portalUrl = "http://localhost";
		String canonicalUrl = "http://localhost/search";
		when(mockPortal.getCurrentURL(mockHttpServletRequest)).thenReturn(currentUrl);
		when(mockPortal.getPortalURL(mockHttpServletRequest) + "localhost").thenReturn(portalUrl);

		String result = webcontentURLRewriteFilterHelper.getCanonicalURL(mockHttpServletRequest);

		assertEquals(canonicalUrl, result);
	}

	@Test
	public void getCanonicalURL_WhenQuestionMarkNotPresentInCurrentUrl_ThenCurrentUrlIsUntouched() {
		String currentUrl = "/search/stuff_NOT_to_be_discarded";
		String portalUrl = "http://localhost";
		String canonicalUrl = portalUrl + currentUrl;
		when(mockPortal.getCurrentURL(mockHttpServletRequest)).thenReturn(currentUrl);
		when(mockPortal.getPortalURL(mockHttpServletRequest) + "localhost").thenReturn(portalUrl);

		String result = webcontentURLRewriteFilterHelper.getCanonicalURL(mockHttpServletRequest);

		assertEquals(canonicalUrl, result);
	}

	@Test
	public void getFriendlyUrl_WhenDocumentUriStartsWithWebSlashAndHasGroupName_ThenFriendlyUrlBeginsAfterGroupName() {
		String documentUri = "/web/group_name/something/hello/friendly_url";
		String friendlyUrl = "/something/hello/friendly_url";

		String result = webcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri);

		assertEquals(friendlyUrl, result);
	}

	@Test
	public void getFriendlyUrl_WhenDocumentUriStartsWithWebSlashAndHasNoOtherUrlComponents_ThenFriendlyUrlEqualsDocumentUri() {
		String documentUri = "/web";

		String result = webcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri);

		assertEquals(documentUri, result);
	}

	@Test
	public void getFriendlyUrl_WhenDocumentUriNotStartsWithWebSlash_ThenFriendlyUrlEqualsDocumentUri() {
		String documentUri = "/something/group_name/friendly_url";

		String result = webcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri);

		assertEquals(documentUri, result);
	}

	@Test
	public void getGroupId_WhenDocumentUriStartsWithWebAndGroupNameIsNotFollowedBySlash_ThenExtractLastUrlComponentAsGroupFriendlyUrl() {
		String documentUri = "/web/group_name";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), "/group_name")).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(GROUP_ID, groupdId.longValue());
	}
	
	@Test
	public void getGroupId_WhenDocumentUriStartsWithWebAndGroupNameIsFollowedByOtherUrlComponents_ThenExtractFirstUrlComponentAfterWebSlashAsGroupFriendlyUrl() throws Exception {
		String documentUri = "/web/group_name/anything-here";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), "/group_name")).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(GROUP_ID, groupdId.longValue());
	}
	
	@Test
	public void getGroupId_WhenDocumentUriStartsWithWebAndGroupNameIsPresentAndGroupIdReturnedIsZero_ThenGroupIdIsReturnedFromLayout() {
		String documentUri = "/web/group_name/anything-here";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), "/group_name")).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(new Long(0));
		when(mockServletRequest.getAttribute(WebKeys.VIRTUAL_HOST_LAYOUT_SET)).thenReturn(mockLayoutSet);
		when(mockLayoutSet.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(GROUP_ID, groupdId.longValue());
	}

	@Test
	public void getGroupId_WhenDocumentUriStartsWithWebSlashAndNoGroupNameInUrl_ThenGroupIdIsReturnedFromLayout() {
		String documentUri = "/web";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServletRequest.getAttribute(WebKeys.VIRTUAL_HOST_LAYOUT_SET)).thenReturn(mockLayoutSet);
		when(mockGroupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), "")).thenReturn(null);
		when(mockLayoutSet.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(GROUP_ID, groupdId.longValue());
	}

	@Test
	public void getGroupId_WhenDocumentUriNotStartsWithWebSlashAndNoGroup_ThenGroupIdIsReturnedFromLayout() {
		String documentUri = "web/friendly-url";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServletRequest.getAttribute(WebKeys.VIRTUAL_HOST_LAYOUT_SET)).thenReturn(mockLayoutSet);
		when(mockLayoutSet.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(GROUP_ID, groupdId.longValue());
	}
	
	@Test
	public void getGroupId_WhenGroupNotFoundFromFriendlyUrlAndLayoutSetIsNotNull_ThenGroupIdIsReturnedFromLayout() {
		String documentUri = "/web/friendly-url";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServletRequest.getAttribute(WebKeys.VIRTUAL_HOST_LAYOUT_SET)).thenReturn(mockLayoutSet);
		when(mockGroupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), "/friendly-url")).thenReturn(null);
		when(mockLayoutSet.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(GROUP_ID, groupdId.longValue());
	}

	@Test
	public void getGroupId_WhenGroupNotFoundAndLayoutSetIsNull_ThenGroupIdIsNull() {
		String documentUri = "/web/friendly-url";
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), "/friendly-url")).thenReturn(null);
		when(mockLayoutSet.getGroupId()).thenReturn(GROUP_ID);

		Long groupdId = webcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri);

		assertEquals(null, groupdId);
	}

	@Test
	public void getPortalUrl() throws Exception {
		String articleUrl = "/web/article-url";
		String portalUrl = "/portal-url/";
		when(mockPortal.getPortalURL(mockHttpServletRequest)).thenReturn(portalUrl);

		String result = webcontentURLRewriteFilterHelper.getPortalUrl(mockHttpServletRequest, articleUrl);

		assertEquals(portalUrl + articleUrl, result);
	}

	@Test
	public void isFilterEnabled_WhenNoErrorAndFilterIsEnabled_ThenReturnsTrue() throws Exception {

		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentFilterCompanyConfiguration.class, CompanyThreadLocal.getCompanyId())).thenReturn(mockWebContentFilterCompanyConfiguration);
		when(mockWebContentFilterCompanyConfiguration.enabled()).thenReturn(true);

		boolean result = webcontentURLRewriteFilterHelper.isFilterEnabled();

		assertEquals(true, result);
	}

	@Test
	public void isFilterEnabled_WhenNoErrorAndFilterIsEnabled_ThenReturnsFalse() throws Exception {

		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentFilterCompanyConfiguration.class, CompanyThreadLocal.getCompanyId())).thenReturn(mockWebContentFilterCompanyConfiguration);
		when(mockWebContentFilterCompanyConfiguration.enabled()).thenReturn(true);

		boolean result = webcontentURLRewriteFilterHelper.isFilterEnabled();

		assertEquals(true, result);
	}

	@Test
	public void isFilterEnabled_WhenNoExceptionIsThrownCreateConfig_ThenReturnsFalse() throws Exception {

		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(WebContentFilterCompanyConfiguration.class, CompanyThreadLocal.getCompanyId())).thenThrow(new ConfigurationException());
		boolean result = webcontentURLRewriteFilterHelper.isFilterEnabled();

		assertEquals(false, result);
	}

	@Test
	public void isJournalArticleExist_WhenJournalArticleExists_ThenReturnTrue() {
		String articleUrl = "/web/article-url";
		when(mockJournalArticleLocalService.fetchLatestArticleByUrlTitle(GROUP_ID, articleUrl, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);

		boolean result = webcontentURLRewriteFilterHelper.isJournalArticleExist(articleUrl, GROUP_ID);

		assertEquals(true, result);
	}

	@Test
	public void isJournalArticleExist_WhenJournalArticleNotExists_ThenReturnFalse() {
		String articleUrl = "/web/article-url";
		when(mockJournalArticleLocalService.fetchLatestArticleByUrlTitle(GROUP_ID, articleUrl, WorkflowConstants.STATUS_APPROVED)).thenReturn(null);

		boolean result = webcontentURLRewriteFilterHelper.isJournalArticleExist(articleUrl, GROUP_ID);

		assertEquals(false, result);
	}

	@Test
	public void isLayoutExist_WhenLayoutExists_ThenReturnTrue() {
		String friendlyUrl = "/web/friendly-url";
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, false, friendlyUrl)).thenReturn(mockLayout);

		boolean result = webcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, GROUP_ID);

		assertEquals(true, result);
	}

	@Test
	public void isLayoutExist_WhenLayoutNotExists_ThenReturnFalse() {
		String friendlyUrl = "/web/friendly-url";
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, false, friendlyUrl)).thenReturn(null);

		boolean result = webcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, GROUP_ID);

		assertEquals(false, result);
	}
}
