package com.placecube.digitalplace.local.webcontent.filter;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.WebKeys;

@RunWith(PowerMockRunner.class)
public class WebcontentURLRewriteFilterTest extends PowerMockito {

	private static final long GROUP_ID = 999l;

	@Mock
	private FilterChain mockFilterChain;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Group mockGroup;
	
	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private RequestDispatcher mockRequestDispatcher;

	@Mock
	private ServletRequest mockServletRequest;

	@Mock
	private ServletResponse mockServletResponse;

	@Mock
	private WebcontentURLRewriteFilterHelper mockWebcontentURLRewriteFilterHelper;

	@InjectMocks
	private WebcontentURLRewriteFilter webcontentURLRewriteFilter;

	@Test
	public void doFilter_WhenFilterIsNotEnabled_ThenInvokerFilterUriIsNeverRead() throws Exception {
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(false);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockServletRequest, never()).getAttribute(WebKeys.INVOKER_FILTER_URI);
	}

	@Test
	public void doFilter_WhenFilterIsNotEnabled_ThenFilterChainIsInvoked() throws Exception {
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(false);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockFilterChain, times(1)).doFilter(mockServletRequest, mockServletResponse);
	}

	@Test
	public void doFilter_WhenFilterIsEnabledAndUriPrefixIsExcluded_ThenGroupIdIsNeverExtracted() throws Exception {
		String documentUri = "/-/excluded_prefix";
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(true);
		when(mockServletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI)).thenReturn(documentUri);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockWebcontentURLRewriteFilterHelper, never()).getGroupId(mockServletRequest, documentUri);
	}
	
	@Test
	public void doFilter_WhenFilterIsEnabledAndUriIsSingleSlash_ThenGetGroupIdIsNeverCalled() throws Exception {
		String documentUri = "/";
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(true);
		when(mockServletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI)).thenReturn(documentUri);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockWebcontentURLRewriteFilterHelper, never()).getGroupId(mockServletRequest, documentUri);
	}

	@Test
	public void doFilter_WhenFilterIsEnabledAndUriPrefixNotExcludedAndLayoutExist_ThenJournalArticleExistsIsNeverCalled() throws Exception {
		String documentUri = "/allowed_prefix";
		String friendlyUrl = "/friendlyUrl";
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(true);
		when(mockServletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI)).thenReturn(documentUri);
		when(mockWebcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri)).thenReturn(GROUP_ID);
		when(mockWebcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri)).thenReturn(friendlyUrl);
		when(mockWebcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, GROUP_ID)).thenReturn(true);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockWebcontentURLRewriteFilterHelper, never()).isJournalArticleExist(ArgumentMatchers.anyString(), ArgumentMatchers.anyLong());
	}

	@Test
	public void doFilter_WhenFilterIsEnabledAndUriPrefixNotExcludedAndLayoutNotExistAndJournalArticleNotExist_ThenGroupIsNeverRetrieved() throws Exception {
		String documentUri = "/allowed_prefix/article_url";
		String friendlyUrl = "/friendlyUrl";
		String articleUrl = "article_url";
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(true);
		when(mockServletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI)).thenReturn(documentUri);
		when(mockWebcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri)).thenReturn(GROUP_ID);
		when(mockWebcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri)).thenReturn(friendlyUrl);
		when(mockWebcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, GROUP_ID)).thenReturn(false);
		when(mockWebcontentURLRewriteFilterHelper.isJournalArticleExist(articleUrl, GROUP_ID)).thenReturn(false);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockGroupLocalService, never()).getGroup(GROUP_ID);
	}

	@Test
	public void doFilter_WhenFilterIsEnabledAndUriPrefixNotExcludedAndLayoutNotExistAndJournalArticleExist_ThenAttributesAreSetOnServletRequest() throws Exception {
		String documentUri = "/allowed_prefix/article_url";
		String friendlyUrl = "/friendlyUrl";
		String articleUrl = "article_url";
		String groupFriendlyUrl = "/group";
		String customCanonicalURL = "/customCanonicalURL/";
		String joinedArticleUrl = "/web" + groupFriendlyUrl + "/-/" + articleUrl;
		String portalUrl = "portalUrl";
		
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(true);
		when(mockServletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI)).thenReturn(documentUri);
		when(mockWebcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri)).thenReturn(GROUP_ID);
		when(mockWebcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri)).thenReturn(friendlyUrl);
		when(mockWebcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, GROUP_ID)).thenReturn(false);
		when(mockWebcontentURLRewriteFilterHelper.isJournalArticleExist(articleUrl, GROUP_ID)).thenReturn(true);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.getFriendlyURL()).thenReturn(groupFriendlyUrl);
		when(mockWebcontentURLRewriteFilterHelper.getHttpServletRequest(mockServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockWebcontentURLRewriteFilterHelper.getCanonicalURL(mockHttpServletRequest)).thenReturn(customCanonicalURL);
		when(mockWebcontentURLRewriteFilterHelper.getPortalUrl(mockHttpServletRequest, joinedArticleUrl)).thenReturn(portalUrl);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockServletRequest, times(1)).setAttribute(WebKeys.CURRENT_COMPLETE_URL, portalUrl);
		verify(mockServletRequest, times(1)).setAttribute("customCanonicalURL", customCanonicalURL);

	}
	
	@Test
	public void doFilter_WhenFilterIsEnabledAndUriPrefixNotExcludedAndLayoutNotExistAndJournalArticleExist_ThenForwardRequestDispatcherToJoinedArticleUrl() throws Exception {
		String documentUri = "/allowed_prefix/article_url";
		String friendlyUrl = "/friendlyUrl";
		String articleUrl = "article_url";
		String groupFriendlyUrl = "/group";
		String joinedArticleUrl = "/web" + groupFriendlyUrl + "/-/" + articleUrl;
		
		when(mockWebcontentURLRewriteFilterHelper.isFilterEnabled()).thenReturn(true);
		when(mockServletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI)).thenReturn(documentUri);
		when(mockWebcontentURLRewriteFilterHelper.getGroupId(mockServletRequest, documentUri)).thenReturn(GROUP_ID);
		when(mockWebcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri)).thenReturn(friendlyUrl);
		when(mockWebcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, GROUP_ID)).thenReturn(false);
		when(mockWebcontentURLRewriteFilterHelper.isJournalArticleExist(articleUrl, GROUP_ID)).thenReturn(true);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.getFriendlyURL()).thenReturn(groupFriendlyUrl);
		when(mockServletRequest.getRequestDispatcher(joinedArticleUrl)).thenReturn(mockRequestDispatcher);

		webcontentURLRewriteFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockServletRequest, mockServletResponse);
	}
}
