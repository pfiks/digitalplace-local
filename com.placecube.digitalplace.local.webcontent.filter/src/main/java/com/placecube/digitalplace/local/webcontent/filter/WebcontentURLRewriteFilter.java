package com.placecube.digitalplace.local.webcontent.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.WebKeys;

@Component(immediate = true, property = { "servlet-context-name=", "servlet-filter-name=Webcontent URL rewrite filter", "before-filter=Virtual Host Filter", "url-pattern=/*" }, service = Filter.class)
public class WebcontentURLRewriteFilter implements Filter {

	private static final Log LOG = LogFactoryUtil.getLog(WebcontentURLRewriteFilter.class);

	private static final List<String> EXCLUDED_URL_PREFIXES = Arrays.asList("/o/", "/c/", "/group/", "/combo", "/image", "/-/", "/documents/", "/api/", "/webdav/", "/search");

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private WebcontentURLRewriteFilterHelper webcontentURLRewriteFilterHelper;

	@Override
	public void destroy() {
		// Not needed
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

		if (webcontentURLRewriteFilterHelper.isFilterEnabled()) {
			try {
				String documentUri = (String) servletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI);

				if (EXCLUDED_URL_PREFIXES.stream().noneMatch(documentUri::startsWith) && !documentUri.equals("/")) {

					LOG.debug("Document Uri: " + documentUri);
					Long groupId = webcontentURLRewriteFilterHelper.getGroupId(servletRequest, documentUri);

					String friendlyUrl = webcontentURLRewriteFilterHelper.getFriendlyUrl(documentUri);
					if (!webcontentURLRewriteFilterHelper.isLayoutExist(friendlyUrl, groupId)) {

						String articleURL = documentUri.substring(documentUri.lastIndexOf("/") + 1);
						if (webcontentURLRewriteFilterHelper.isJournalArticleExist(articleURL, groupId)) {
							Group group = groupLocalService.getGroup(groupId);
							articleURL = "/web" + group.getFriendlyURL() + "/-/" + articleURL;

							HttpServletRequest httpServletRequest = webcontentURLRewriteFilterHelper.getHttpServletRequest(servletRequest);
							String customCanonicalURL = webcontentURLRewriteFilterHelper.getCanonicalURL(httpServletRequest);

							servletRequest.setAttribute(WebKeys.CURRENT_COMPLETE_URL, webcontentURLRewriteFilterHelper.getPortalUrl(httpServletRequest, articleURL));
							servletRequest.setAttribute("customCanonicalURL", customCanonicalURL);

							RequestDispatcher rq = servletRequest.getRequestDispatcher(articleURL);
							rq.forward(servletRequest, servletResponse);
							return;
						}

					}
				}
			} catch (Exception e) {
				LOG.error("Error redirecting: " + e.getMessage(), e);
			}
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Not needed
	}

}
