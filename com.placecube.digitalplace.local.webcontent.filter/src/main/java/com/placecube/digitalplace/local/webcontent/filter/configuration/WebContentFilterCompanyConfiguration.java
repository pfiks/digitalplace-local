package com.placecube.digitalplace.local.webcontent.filter.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "filters", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.webcontent.filter.configuration.WebContentFilterCompanyConfiguration", localization = "content/Language", name = "digitalplace-web-content-filter")
public interface WebContentFilterCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false")
	boolean enabled();
}
