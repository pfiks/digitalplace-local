package com.placecube.digitalplace.local.webcontent.filter;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.webcontent.filter.configuration.WebContentFilterCompanyConfiguration;

@Component(immediate = true, service = WebcontentURLRewriteFilterHelper.class)
public class WebcontentURLRewriteFilterHelper {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	public String getCanonicalURL(HttpServletRequest httpServletRequest) {

		String portalCurrentURL = portal.getCurrentURL(httpServletRequest);

		int parametersPos = portalCurrentURL.indexOf("?");

		return portal.getPortalURL(httpServletRequest) + portalCurrentURL.substring(0, parametersPos == -1 ? portalCurrentURL.length() : parametersPos);
	}

	public String getFriendlyUrl(String documentUri) {
		String friendlyUrl = documentUri;
		if (documentUri.startsWith("/web") && documentUri.indexOf("/", 5) != -1) {
			friendlyUrl = documentUri.substring(documentUri.indexOf("/", 5));
		}

		return friendlyUrl;
	}

	public Long getGroupId(ServletRequest servletRequest, String documentUri) {

		LayoutSet virtualHostLayoutSet = (LayoutSet) servletRequest.getAttribute(WebKeys.VIRTUAL_HOST_LAYOUT_SET);

		Long groupId = null;

		if (documentUri.startsWith("/web")) {
			String friendlyUrl = documentUri.substring(4);
			if (friendlyUrl.indexOf("/", 1) != -1) {
				
				friendlyUrl = friendlyUrl.substring(0, friendlyUrl.indexOf("/", 1));
			}

			Group friendlyUrlGroup = groupLocalService.fetchFriendlyURLGroup(CompanyThreadLocal.getCompanyId(), friendlyUrl);
			if (Validator.isNotNull(friendlyUrlGroup)) {
				groupId = friendlyUrlGroup.getGroupId();
			}

		}

		if ((groupId == null || groupId == 0) && virtualHostLayoutSet != null) {

			groupId = virtualHostLayoutSet.getGroupId();
		}

		return groupId;
	}

	public HttpServletRequest getHttpServletRequest(ServletRequest servletRequest) {
		return (HttpServletRequest) servletRequest;
	}

	public String getPortalUrl(HttpServletRequest httpServletRequest, String articleUrl) {
		return portal.getPortalURL(httpServletRequest) + articleUrl;
	}

	public boolean isFilterEnabled() {
		try {
			return configurationProvider.getCompanyConfiguration(WebContentFilterCompanyConfiguration.class, CompanyThreadLocal.getCompanyId()).enabled();
		} catch (Exception e) {
			return false;
		}

	}

	public boolean isJournalArticleExist(String articleURL, long groupId) {
		return Validator.isNotNull(journalArticleLocalService.fetchLatestArticleByUrlTitle(groupId, articleURL, WorkflowConstants.STATUS_APPROVED));
	}

	public boolean isLayoutExist(String friendlyUrl, long groupId) {
		return Validator.isNotNull(layoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl));
	}

}
