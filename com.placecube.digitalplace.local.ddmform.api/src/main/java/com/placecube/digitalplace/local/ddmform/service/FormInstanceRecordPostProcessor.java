package com.placecube.digitalplace.local.ddmform.service;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.service.ServiceContext;

public interface FormInstanceRecordPostProcessor {

	void process(DDMFormInstanceRecord formInstanceRecord, ServiceContext serviceContext);
}
