package com.placecube.digitalplace.local.ddmform.service;

import java.util.List;

public interface FormInstanceRecordPostProcessorRegistry {

	List<FormInstanceRecordPostProcessor> getPostProcessors();
}
