package com.placecube.digitalplace.local.ddmform.service;

import java.util.Optional;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;

public interface FormInstanceRecordIdRetrievalRegistry {

	Optional<FormInstanceRecordIdRetrieval> getRetrieval(DDMFormInstanceRecord formInstanceRecord);
}
