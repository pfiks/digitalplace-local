package com.placecube.digitalplace.local.ddmform.service;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;

public interface FormInstanceRecordIdRetrieval {

	boolean isEnabled(DDMFormInstanceRecord formInstanceRecord);

	String getId(DDMFormInstanceRecord formInstanceRecord);
}
