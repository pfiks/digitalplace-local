package com.placecube.digitalplace.local.webcontent.news.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsContentSet;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

@RunWith(MockitoJUnitRunner.class)
public class NewsWebContentServiceTest {

	public static final String STRUCTURE_KEY = "NEWS";

	public static final String STRUCTURE_RESOURCE_PATH = "com/placecube/digitalplace/local/webcontent/news/dependencies/ddm/structure.xml";

	@InjectMocks
	private NewsWebContentService newsWebContentService;

	private ClassLoader newsWebContentServiceClassLoader;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private AssetListEntryInitializer mockAssetListEntryInitializer;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		newsWebContentServiceClassLoader = NewsWebContentService.class.getClassLoader();
	}

	@Test(expected = PortalException.class)
	public void getOrCreateContentSet_WhenException_ThenThrowsPortalException() throws Exception {
		when(mockAssetListEntryInitializer.getOrCreateContentSet(NewsContentSet.LATEST_NEWS.getName(), true, mockServiceContext)).thenThrow(new PortalException());

		newsWebContentService.getOrCreateContentSet(NewsContentSet.LATEST_NEWS, mockServiceContext);
	}

	@Test
	public void getOrCreateContentSet_WhenNoError_ThenReturnsTheContentSet() throws Exception {
		when(mockAssetListEntryInitializer.getOrCreateContentSet(NewsContentSet.LATEST_NEWS.getName(), true, mockServiceContext)).thenReturn(mockAssetListEntry);

		AssetListEntry result = newsWebContentService.getOrCreateContentSet(NewsContentSet.LATEST_NEWS, mockServiceContext);

		assertThat(result, sameInstance(mockAssetListEntry));
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheNewsStructure() throws PortalException {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, getClass().getClassLoader(), JournalArticle.class, mockServiceContext)).thenReturn(mockDDMStructure);

		DDMStructure ddmStructure = newsWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(ddmStructure, sameInstance(mockDDMStructure));

	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, getClass().getClassLoader(), JournalArticle.class, mockServiceContext))
				.thenThrow(new PortalException());

		newsWebContentService.getOrCreateDDMStructure(mockServiceContext);

	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenException_ThenThrowsPortalException() throws Exception {

		NewsWidgetTemplate widgetTemplate = NewsWidgetTemplate.NEWS_CAROUSEL;
		String templateResourcePath = "com/placecube/digitalplace/local/webcontent/news/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath, newsWebContentServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		newsWebContentService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		NewsWidgetTemplate widgetTemplate = NewsWidgetTemplate.NEWS_CAROUSEL;
		String templateResourcePath = "com/placecube/digitalplace/local/webcontent/news/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath, newsWebContentServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = newsWebContentService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

}
