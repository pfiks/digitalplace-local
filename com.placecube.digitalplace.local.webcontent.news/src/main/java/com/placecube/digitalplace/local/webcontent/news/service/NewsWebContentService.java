package com.placecube.digitalplace.local.webcontent.news.service;

import java.util.Collections;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsContentSet;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsDDMTemplate;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = NewsWebContentService.class)
public class NewsWebContentService {

	@Reference
	private AssetListEntryInitializer assetListEntryInitializer;

	@Reference
	private DDMInitializer ddmInitializer;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private Portal portal;

	public AssetListEntry getOrCreateContentSet(NewsContentSet contentSet, ServiceContext serviceContext) throws PortalException {
		return assetListEntryInitializer.getOrCreateContentSet(contentSet.getName(), true, serviceContext);
	}

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {

		String resourcePath = "com/placecube/digitalplace/local/webcontent/news/dependencies/ddm/structure.xml";

		return ddmInitializer.getOrCreateDDMStructure("NEWS", resourcePath, getClass().getClassLoader(), JournalArticle.class, serviceContext);
	}

	public DDMTemplate getOrCreateDDMTemplate(ServiceContext serviceContext, NewsDDMTemplate newsDDMTemplate, String templatePrefix) throws PortalException {
		try {

			String ddmTemplateName = Validator.isNull(templatePrefix) ? newsDDMTemplate.getName() : templatePrefix + StringPool.SPACE + newsDDMTemplate.getName();
			String ddmTemplateKey = normalizeString(ddmTemplateName);

			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), ddmTemplateKey);

			if (Validator.isNotNull(ddmTemplate)) {
				return ddmTemplate;
			}

			DDMStructure newsStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), newsDDMTemplate.getKey());

			String script = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/news/dependencies/ddm/carousel_slide.ftl");

			return ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), newsStructure.getStructureId(),
					newsStructure.getClassNameId(), ddmTemplateKey, Collections.singletonMap(serviceContext.getLocale(), ddmTemplateName), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY,
					DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, script, true, false, StringPool.BLANK, null, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public DDMTemplate getOrCreateWidgetTemplate(NewsWidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = "com/placecube/digitalplace/local/webcontent/news/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath,
				getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}

	private String normalizeString(String string) {
		String regex = " +";
		String stringNoSpace = string.trim().replaceAll(regex, StringPool.SPACE);
		String stringCaps = stringNoSpace.toUpperCase();
		return stringCaps.replace(StringPool.SPACE, StringPool.DASH);
	}
}
