package com.placecube.digitalplace.local.webcontent.news.constants;

public enum NewsContentSet {

	LATEST_NEWS("Latest News");

	private final String name;

	private NewsContentSet(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
