package com.placecube.digitalplace.local.webcontent.news.constants;

public enum NewsWidgetTemplate {

	NEWS_CAROUSEL("ASSET-PUBLISHER-NEWS-CAROUSEL", "News Carousel");

	private final String key;
	private final String name;

	private NewsWidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
