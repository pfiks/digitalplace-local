<#if entries?has_content>
	<div class="news-widget">
		<#assign randomId = stringUtil.randomId()/>
		<#assign showIndicators = false />
		<div class="carousel slide" id="${randomId}" data-ride="carousel">
			<div class="carousel-inner" data-matchheight=".news-carousel-slide">
				<#assign journalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")/>

				<#list entries as entry>
					<#if entry?counter==1>
						<#assign class_active="active"/>
					<#else>
						<#assign class_active=""/>
						<#assign showIndicators = true />
					</#if>

					<div class="carousel-item ${class_active}">
						<#assign journalArticle = journalService.fetchLatestArticle(entry.getClassPK()) />
						<#assign content = journalService.getArticleContent(journalArticle,null,null,themeDisplay.getLanguageId(),null,themeDisplay)/>
	
						${content}
	
					</div>

				</#list>
			</div>
			
			<#if showIndicators>
				<a class="carousel-control-prev" href="#${randomId}" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only"><@liferay.language key="previous" /></span>
				</a>
				<a class="carousel-control-next" href="#${randomId}" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only"><@liferay.language key="next" /></span>
				</a>	
				
				<ol class="carousel-indicators">
					<#list 0..entries?size-1 as x >
						<#if x==0>
							<#assign class_active="active"/>
						<#else>
							<#assign class_active=""/>
						</#if>
			
						<li data-target="#${randomId}" data-slide-to="${x}" class="${class_active}"></li>
	
					</#list>
				</ol>
			</#if>
		</div>
	</div>
	    
	<script>
		(function() {
			'use strict';
			$('[data-matchHeight]').each(function(){
				var $this = $(this);
				var matchHeightTarget = $this.attr('data-matchHeight');
				$this.find(matchHeightTarget).matchHeight();
			});
		})();
	</script>

</#if>