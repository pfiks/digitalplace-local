<div class="row news-carousel-slide">
	<div class="col-md-8 col-xs-12 slide-image">
		<img class="news-image img-responsive" alt="${Image.getAttribute("alt")}" data-fileentryid="${Image.getAttribute("fileEntryId")}" src="${Image.getData()}" />
	</div>
	
	<div class="col-md-4 col-xs-12 slide-content">
		<#assign PublishDate_Data = getterUtil.getString(PublishDate.getData())>
		<#if validator.isNotNull(PublishDate_Data)>
			<div class="news-header">
				<#assign PublishDate_DateObj = dateUtil.parseDate("yyyy-MM-dd", PublishDate_Data, locale)>
				${dateUtil.getDate(PublishDate_DateObj, "EEEE d MMMM yyyy", locale)}
			</div>
		</#if>
		
		<#assign has_link_to_page = LinkToPage?? && LinkToPage.getFriendlyUrl()?has_content />
		<div class="news-title">
			<h3>
				<#if has_link_to_page>
					<a class="h3" href="${LinkToPage.getFriendlyUrl()}">${Headline.getData()}</a>
				<#else>
					<span class="h3">${Headline.getData()}</span>
				</#if>
			</h3>
		</div>
		
		<div class="news-summary">
			${Summary.getData()}
		</div>

		<#if has_link_to_page>
			<div class="news-link">
				<a href="${LinkToPage.getFriendlyUrl()}" class="btn-primary float-right">Read news</a>
			</div>
		</#if>
	</div>
</div>