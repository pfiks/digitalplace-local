package com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.constants;

public final class DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants {

	public static final String BUNDLE_NAME = "com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric";

	public static final String GLOBAL_GROUP_ID_PLACEHOLDER = "${PLACEHOLDER_GLOBAL_GROUP_ID}";

	public static final String SITE_INITIALIZER_KEY = "digital-place-council-web-site-elesctric-site-initializer";

	public static final String SITE_INITIALIZER_NAME = "Digital Place Council Web Site Electric";

	public static final String THUMBNAIL_PATH = "/images/thumbnail.png";

	private DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants() {
		return;
	}
}
