package com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.initializer;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.site.exception.InitializationException;
import com.liferay.site.initializer.SiteInitializer;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.constants.DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants;
import com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.service.DigitalPlaceCouncilWebsiteElectricSiteInitializerService;

@Component(immediate = true, property = "site.initializer.key=" + DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_KEY, service = SiteInitializer.class)
public class DigitalPlaceCouncilWebsiteElectricSiteInitializer implements SiteInitializer {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceCouncilWebsiteElectricSiteInitializer.class);

	@Reference
	private DigitalPlaceCouncilWebsiteElectricSiteInitializerService digitalPlaceCouncilWebsiteElectricSiteInitializerService;

	@Reference
	private DigitalPlaceInitializerService digitalPlaceInitializerService;

	@Reference(target = "(osgi.web.symbolicname=" + DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.BUNDLE_NAME + ")")
	private ServletContext servletContext;

	@Override
	public String getDescription(Locale locale) {
		return StringPool.BLANK;
	}

	@Override
	public String getKey() {
		return DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_KEY;
	}

	@Override
	public String getName(Locale locale) {
		return DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_NAME;
	}

	@Override
	public String getThumbnailSrc() {
		return servletContext.getContextPath() + DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.THUMBNAIL_PATH;
	}

	@Override
	public void initialize(long groupId) throws InitializationException {
		try {
			LOG.info("Initializing Digital Place Council Web Site Electric For Site GroupId: " + groupId);

			ServiceContext serviceContext = digitalPlaceCouncilWebsiteElectricSiteInitializerService.getServiceContext();

			digitalPlaceCouncilWebsiteElectricSiteInitializerService.configureDigitalPlaceElectricTheme(serviceContext);
			digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeContentSets(serviceContext);
			digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeDynamicDataLists(serviceContext);
			digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(serviceContext);
			digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeSiteNavigationMenus(serviceContext);

			Map<String, String> globalContentPrefsPlaceholders = digitalPlaceCouncilWebsiteElectricSiteInitializerService.getGlobalContentPreferencesPlaceholders(serviceContext.getCompanyId());

			digitalPlaceInitializerService.configurePageAsFirstLayout(configureContentPage("home", serviceContext, globalContentPrefsPlaceholders));
			configureContentPage("page-not-found", serviceContext, globalContentPrefsPlaceholders);

			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceCouncilWebsiteElectricSiteInitializer.class,
					"com/placecube/digitalplace/local/siteinitializer/councilwebsiteelectric/dependencies/layouts/search.json", serviceContext);

			LOG.info("Digital Place Council Web Site Electric initialization For Site Scope Completed");

		} catch (Exception e) {
			throw new InitializationException(e);
		}

	}

	@Override
	public boolean isActive(long companyId) {
		return true;
	}

	private Layout configureContentPage(String pageName, ServiceContext serviceContext, Map<String, String> prefsPlaceholders) throws PortalException {
		return digitalPlaceInitializerService.initializeContentPage(DigitalPlaceCouncilWebsiteElectricSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/councilwebsiteelectric/dependencies/layouts/" + pageName + ".json", prefsPlaceholders, serviceContext);
	}

}