package com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.service;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationContentSet;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.ddl.offer.service.OfferDDMService;
import com.placecube.digitalplace.local.ddl.place.service.PlaceDDMService;
import com.placecube.digitalplace.local.ddl.venue.service.VenueDDMService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuEntry;
import com.placecube.digitalplace.local.pagetemplates.constants.DigitalPlacePageTemplateKey;
import com.placecube.digitalplace.local.pagetemplates.service.PageTemplateService;
import com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.constants.DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants;
import com.placecube.digitalplace.local.webcontent.article.constants.ContentSet;
import com.placecube.digitalplace.local.webcontent.article.service.ArticleWebContentService;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsContentSet;
import com.placecube.digitalplace.local.webcontent.news.service.NewsWebContentService;
import com.placecube.initializer.service.NavigationMenuInitializer;

@Component(immediate = true, service = DigitalPlaceCouncilWebsiteElectricSiteInitializerService.class)
public class DigitalPlaceCouncilWebsiteElectricSiteInitializerService {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceCouncilWebsiteElectricSiteInitializerService.class);

	@Reference
	private ArticleWebContentService articleWebContentService;

	@Reference
	private CategoryNavigationService categoryNavigationService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LayoutSetLocalService layoutSetLocalService;

	@Reference
	private NavigationMenuInitializer navigationMenuInitializer;

	@Reference
	private NewsWebContentService newsWebContentService;

	@Reference
	private OfferDDMService offerDDMService;

	@Reference
	private PageTemplateService pageTemplateService;

	@Reference
	private PlaceDDMService placeDDMService;

	@Reference
	private VenueDDMService venueDDMService;

	public void configureDigitalPlaceElectricTheme(ServiceContext serviceContext) throws PortalException {
		Group scopeGroup = serviceContext.getScopeGroup();
		UnicodeProperties typeSettingsProperties = scopeGroup.getTypeSettingsProperties();
		if (!GetterUtil.getBoolean(typeSettingsProperties.getProperty("theme.initialized"), false)) {
			String themeId = "digitalplaceelectrictheme_WAR_digitalplaceelectrictheme";
			layoutSetLocalService.updateLookAndFeel(scopeGroup.getGroupId(), false, themeId, StringPool.BLANK, StringPool.BLANK);
			layoutSetLocalService.updateLookAndFeel(scopeGroup.getGroupId(), true, themeId, StringPool.BLANK, StringPool.BLANK);
			saveGroupTypeSettings(scopeGroup.getGroupId(), typeSettingsProperties, "theme.initialized", "true");
			LOG.info("Electric theme configured");
		}
	}

	public Map<String, String> getGlobalContentPreferencesPlaceholders(long companyId) throws PortalException {
		Group companyGroup = groupLocalService.getCompanyGroup(companyId);
		Map<String, String> prefsPlaceholders = new HashMap<>();
		prefsPlaceholders.put(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.GLOBAL_GROUP_ID_PLACEHOLDER, String.valueOf(companyGroup.getGroupId()));
		return prefsPlaceholders;
	}

	public Map<String, String> getPreferencesPlaceholders(ServiceContext serviceContext) throws PortalException {
		Map<String, String> results = new HashMap<>();
		SiteNavigationMenu siteNavigationMenuMyAccountLinks = navigationMenuInitializer.getOrCreateSiteNavigationMenu(serviceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(),
				NavigationMenuEntry.MY_ACCOUNT_LINKS.getType());
		results.put("${PLACEHOLDER_SITE_NAVIGATION_MENU_MY_ACCOUNT_LINKS}", String.valueOf(siteNavigationMenuMyAccountLinks.getSiteNavigationMenuId()));
		results.put("${PLACEHOLDER_GROUP_ID}", String.valueOf(serviceContext.getScopeGroupId()));
		return results;
	}

	public ServiceContext getServiceContext() {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		ModelPermissions modelPermissions = serviceContext.getModelPermissions();
		modelPermissions.addRolePermissions(RoleConstants.GUEST, ActionKeys.VIEW);

		return serviceContext;
	}

	public void initializeContentSets(ServiceContext serviceContext) throws PortalException {
		articleWebContentService.getOrCreateContentSet(ContentSet.POPULAR_PAGES, serviceContext);
		categoryNavigationService.getOrCreateContentSet(CategoryNavigationContentSet.SERVICE_MENU, serviceContext);
		newsWebContentService.getOrCreateContentSet(NewsContentSet.LATEST_NEWS, serviceContext);
	}

	public void initializeDynamicDataLists(ServiceContext serviceContext) throws PortalException {
		offerDDMService.configure(serviceContext);
		placeDDMService.configure(serviceContext);
		venueDDMService.configure(serviceContext);
	}

	public void initializePageTemplates(ServiceContext serviceContext) throws PortalException {
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.FORM_CONFIRMATION_PAGE, serviceContext);
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.FORM_PAGE, serviceContext);
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.CATEGORY_PAGE, serviceContext);
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.ARTICLE_PAGE, serviceContext);
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.GUIDE_PAGE, serviceContext);
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.FORM_ERROR_PAYMENT_PAGE, serviceContext);
		pageTemplateService.getOrCreatePageTemplate(DigitalPlacePageTemplateKey.WEBDOCS_PAGE, serviceContext);
	}

	public void initializeSiteNavigationMenus(ServiceContext serviceContext) throws PortalException {
		navigationMenuInitializer.getOrCreateSiteNavigationMenu(serviceContext, NavigationMenuEntry.FOOTER_LINKS.getName(), NavigationMenuEntry.FOOTER_LINKS.getType());
		navigationMenuInitializer.getOrCreateSiteNavigationMenu(serviceContext, NavigationMenuEntry.GLOBAL_NAVIGATION.getName(), NavigationMenuEntry.GLOBAL_NAVIGATION.getType());
		navigationMenuInitializer.getOrCreateSiteNavigationMenu(serviceContext, NavigationMenuEntry.SOCIAL_MEDIA_LINKS.getName(), NavigationMenuEntry.SOCIAL_MEDIA_LINKS.getType());
	}

	private void saveGroupTypeSettings(long groupId, UnicodeProperties typeSettingsProperties, String propertyKey, String propertyValue) throws PortalException {
		typeSettingsProperties.remove(propertyKey);
		typeSettingsProperties.setProperty(propertyKey, propertyValue);
		groupLocalService.updateGroup(groupId, typeSettingsProperties.toString());
	}
}
