package com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.constants;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPrivate;
import static java.lang.reflect.Modifier.isStatic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;

public class DigitalPlaceCouncilWebsiteElectricSiteInitializerConstantsTest {

	@Test
	public void testClassIsAConstantsClass() throws Exception {
		Class<?> classToTest = DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.class;

		assertThat(isFinal(classToTest.getModifiers()), equalTo(true));

		assertThat(classToTest.getDeclaredConstructors().length, equalTo(1));
		Constructor<?> constructor = classToTest.getDeclaredConstructor();
		assertThat(isPrivate(constructor.getModifiers()), equalTo(true));
		assertThat(constructor.isAccessible(), equalTo(false));

		constructor.setAccessible(true);
		constructor.newInstance();
		constructor.setAccessible(false);

		for (Method method : classToTest.getMethods()) {
			if (method.isAccessible()) {
				assertThat(isStatic(method.getModifiers()), equalTo(true));
				assertThat(method.getDeclaringClass(), equalTo(classToTest));
			}
		}
	}

	@Test
	public void testConstantValueForBundleName() {
		assertThat(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.BUNDLE_NAME, equalTo("com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric"));
	}

	@Test
	public void testConstantValueForSiteInitializerKey() {
		assertThat(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_KEY, equalTo("digital-place-council-web-site-elesctric-site-initializer"));
	}

	@Test
	public void testConstantValueForSiteInitializerName() {
		assertThat(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_NAME, equalTo("Digital Place Council Web Site Electric"));
	}

	@Test
	public void testConstantValueForThumbnailPath() {
		assertThat(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.THUMBNAIL_PATH, equalTo("/images/thumbnail.png"));
	}
}
