package com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.initializer;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.constants.DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants;
import com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.service.DigitalPlaceCouncilWebsiteElectricSiteInitializerService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, CompanyThreadLocal.class })
public class DigitalPlaceCouncilWebsiteElectricSiteInitializerTest extends PowerMockito {

	private static final long COMPANY_ID = 3434L;
	private static final long GLOBAL_GROUP_ID = 3535L;
	private static final long GROUP_ID = 123;

	@InjectMocks
	private DigitalPlaceCouncilWebsiteElectricSiteInitializer digitalPlaceCouncilWebsiteElectricSiteInitializer;

	@Mock
	private DigitalPlaceCouncilWebsiteElectricSiteInitializerService mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService;

	@Mock
	private DigitalPlaceInitializerService mockDigitalPlaceInitializerService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Layout mockLayoutHome;

	@Mock
	private Map<String, String> mockPrefsPlaceholder;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private Map<String, String> mockStringMap;

	@Before
	public void activateSetUp() {
		mockStatic(PortalUtil.class, CompanyThreadLocal.class);
	}

	@Test
	public void getDescriptionWithAnyLocale_WhenNoError_ThenReturnsEmptyString() {
		String result = digitalPlaceCouncilWebsiteElectricSiteInitializer.getDescription(Locale.CANADA_FRENCH);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getKey_WhenNoError_ThenReturnsSiteInitializerKey() {
		String result = digitalPlaceCouncilWebsiteElectricSiteInitializer.getKey();

		assertThat(result, equalTo(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_KEY));
	}

	@Test
	public void getNameWithAnyLocale_WhenNoError_ThenReturnsSiteInitializerName() {
		String result = digitalPlaceCouncilWebsiteElectricSiteInitializer.getName(Locale.GERMAN);

		assertThat(result, equalTo(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.SITE_INITIALIZER_NAME));
	}

	@Test
	public void getThumbnailSrc_WhenNoError_ThenReturnsTheThumbnailPath() {
		String contextPath = "contextPathValue";
		when(mockServletContext.getContextPath()).thenReturn(contextPath);
		String expected = contextPath + DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.THUMBNAIL_PATH;

		String result = digitalPlaceCouncilWebsiteElectricSiteInitializer.getThumbnailSrc();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void initialize_WhenNoError_ThenInitializesTheResources() throws Exception {
		when(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService.getServiceContext()).thenReturn(mockServiceContext);
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getGroupId()).thenReturn(GLOBAL_GROUP_ID);
		when(mockGroupLocalService.getCompanyGroup(CompanyThreadLocal.getCompanyId())).thenReturn(mockGroup);
		when(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService.getGlobalContentPreferencesPlaceholders(COMPANY_ID)).thenReturn(mockStringMap);

		when(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService.getPreferencesPlaceholders(mockServiceContext)).thenReturn(mockPrefsPlaceholder);

		when(mockDigitalPlaceInitializerService.initializeContentPage(DigitalPlaceCouncilWebsiteElectricSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/councilwebsiteelectric/dependencies/layouts/home.json", mockPrefsPlaceholder, mockServiceContext)).thenReturn(mockLayoutHome);

		digitalPlaceCouncilWebsiteElectricSiteInitializer.initialize(GROUP_ID);

		InOrder inOrder = inOrder(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService, mockDigitalPlaceInitializerService);

		inOrder.verify(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService, times(1)).configureDigitalPlaceElectricTheme(mockServiceContext);
		inOrder.verify(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService, times(1)).initializeContentSets(mockServiceContext);
		inOrder.verify(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService, times(1)).initializeDynamicDataLists(mockServiceContext);
		inOrder.verify(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService, times(1)).initializePageTemplates(mockServiceContext);
		inOrder.verify(mockDigitalPlaceCouncilWebsiteElectricSiteInitializerService, times(1)).initializeSiteNavigationMenus(mockServiceContext);

		inOrder.verify(mockDigitalPlaceInitializerService, times(1)).initializeWidgetPage(DigitalPlaceCouncilWebsiteElectricSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/councilwebsiteelectric/dependencies/layouts/search.json", mockServiceContext);

	}

	@Test
	public void isActive_WhenNoError_ThenReturnsTrue() {
		long companyId = 123;

		boolean result = digitalPlaceCouncilWebsiteElectricSiteInitializer.isActive(companyId);

		assertThat(result, equalTo(true));
	}
}
