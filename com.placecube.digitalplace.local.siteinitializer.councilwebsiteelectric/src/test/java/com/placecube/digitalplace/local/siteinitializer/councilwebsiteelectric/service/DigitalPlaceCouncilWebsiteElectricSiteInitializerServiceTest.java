package com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationContentSet;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.ddl.offer.service.OfferDDMService;
import com.placecube.digitalplace.local.ddl.place.service.PlaceDDMService;
import com.placecube.digitalplace.local.ddl.venue.service.VenueDDMService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuEntry;
import com.placecube.digitalplace.local.pagetemplates.constants.DigitalPlacePageTemplateKey;
import com.placecube.digitalplace.local.pagetemplates.service.PageTemplateService;
import com.placecube.digitalplace.local.siteinitializer.councilwebsiteelectric.constants.DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants;
import com.placecube.digitalplace.local.webcontent.article.constants.ContentSet;
import com.placecube.digitalplace.local.webcontent.article.service.ArticleWebContentService;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsContentSet;
import com.placecube.digitalplace.local.webcontent.news.service.NewsWebContentService;
import com.placecube.initializer.service.NavigationMenuInitializer;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
public class DigitalPlaceCouncilWebsiteElectricSiteInitializerServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 76454;
	private static final long ID = 1;

	@InjectMocks
	private DigitalPlaceCouncilWebsiteElectricSiteInitializerService digitalPlaceCouncilWebsiteElectricSiteInitializerService;

	@Mock
	private ArticleWebContentService mockArticleWebContentService;

	@Mock
	private CategoryNavigationService mockCategoryNavigationService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private LayoutSetLocalService mockLayoutSetLocalService;

	@Mock
	private ModelPermissions mockModelPermissions;

	@Mock
	private NavigationMenuInitializer mockNavigationMenuInitializer;

	@Mock
	private NewsWebContentService mockNewsWebContentService;

	@Mock
	private OfferDDMService mockOfferDDMService;

	@Mock
	private PageTemplateService mockPageTemplateService;

	@Mock
	private PlaceDDMService mockPlaceDDMService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Mock
	private VenueDDMService mockVenueDDMService;

	@Before
	public void activateSetUp() {
		mockStatic(ServiceContextThreadLocal.class);
	}

	@Test(expected = PortalException.class)
	public void configureDigitalPlaceTheme_WhenErrorRetrievingGroup_ThenThrowsPortalException() throws PortalException {
		when(mockServiceContext.getScopeGroup()).thenThrow(new PortalException());

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.configureDigitalPlaceElectricTheme(mockServiceContext);
	}

	@Test
	public void configureDigitalPlaceTheme_WhenThemeIsAlreadyInitialized_ThenDoesUpdateTheLookAndFeel() throws PortalException {
		mockGroupTypeSettings("theme.initialized", "true");

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.configureDigitalPlaceElectricTheme(mockServiceContext);

		verifyZeroInteractions(mockLayoutSetLocalService);
	}

	@Test
	public void configureDigitalPlaceTheme_WhenThemeIsNotAlreadyInitialized_ThenUpdatesTheLookAndFeel() throws PortalException {
		String typeSettingsValue = "typeSettingsValue";
		when(mockUnicodeProperties.toString()).thenReturn(typeSettingsValue);
		when(mockGroup.getGroupId()).thenReturn(ID);
		mockGroupTypeSettings("theme.initialized", "false");

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.configureDigitalPlaceElectricTheme(mockServiceContext);

		InOrder inOrder = inOrder(mockLayoutSetLocalService, mockGroupLocalService, mockUnicodeProperties);
		inOrder.verify(mockLayoutSetLocalService, times(1)).updateLookAndFeel(ID, false, "digitalplaceelectrictheme_WAR_digitalplaceelectrictheme", StringPool.BLANK, StringPool.BLANK);
		inOrder.verify(mockUnicodeProperties, times(1)).remove("theme.initialized");
		inOrder.verify(mockUnicodeProperties, times(1)).setProperty("theme.initialized", "true");
		inOrder.verify(mockGroupLocalService, times(1)).updateGroup(ID, typeSettingsValue);
	}

	@Test(expected = PortalException.class)
	public void configureDigitalPlaceTheme_WhenThemeIsNotAlreadyInitializedAndExceptionSavingTypeSettings_ThenThrowsPortalException() throws PortalException {
		when(mockGroup.getGroupId()).thenReturn(ID);
		String typeSettingsValue = "typeSettingsValue";
		when(mockUnicodeProperties.toString()).thenReturn(typeSettingsValue);
		mockGroupTypeSettings("theme.initialized", "false");
		when(mockGroupLocalService.updateGroup(ID, typeSettingsValue)).thenThrow(new PortalException());

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.configureDigitalPlaceElectricTheme(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void configureDigitalPlaceTheme_WhenThemeIsNotAlreadyInitializedAndExceptionUpdatingPublicLayoutsLookAndFeel_ThenThrowsPortalException() throws PortalException {
		when(mockGroup.getGroupId()).thenReturn(ID);
		mockGroupTypeSettings("theme.initialized", "false");
		doThrow(new PortalException()).when(mockLayoutSetLocalService).updateLookAndFeel(ID, false, "digitalplaceelectrictheme_WAR_digitalplaceelectrictheme", StringPool.BLANK, StringPool.BLANK);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.configureDigitalPlaceElectricTheme(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getGlobalContentPreferencesPlaceholders_WhenCompanyGroupRetrievalFails_ThenThrowsPortalException() throws Exception {
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenThrow(new PortalException());
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.getGlobalContentPreferencesPlaceholders(COMPANY_ID);
	}

	@Test
	public void getGlobalContentPreferencesPlaceholders_WhenNoErrors_ThenReturnsMapWithGlobalGroupPlaceholder() throws Exception {
		final long globalGroupId = 4333L;

		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(globalGroupId);

		Map<String, String> result = digitalPlaceCouncilWebsiteElectricSiteInitializerService.getGlobalContentPreferencesPlaceholders(COMPANY_ID);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DigitalPlaceCouncilWebsiteElectricSiteInitializerConstants.GLOBAL_GROUP_ID_PLACEHOLDER), equalTo(String.valueOf(globalGroupId)));
	}

	@Test(expected = PortalException.class)
	public void getPreferencesPlaceholders_WhenErrorRetrievingSiteNavigationMenu_ThenThrowsPortalException() throws PortalException {
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(), NavigationMenuEntry.MY_ACCOUNT_LINKS.getType()))
				.thenThrow(new PortalException());

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.getPreferencesPlaceholders(mockServiceContext);
	}

	@Test
	public void getPreferencesPlaceholders_WhenNoError_ThenReturnsMapWithPlaceholderValues() throws PortalException {
		long navigationMenuId = 123;
		long groupId = 2;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(), NavigationMenuEntry.MY_ACCOUNT_LINKS.getType()))
				.thenReturn(mockSiteNavigationMenu);
		when(mockSiteNavigationMenu.getSiteNavigationMenuId()).thenReturn(navigationMenuId);

		Map<String, String> results = digitalPlaceCouncilWebsiteElectricSiteInitializerService.getPreferencesPlaceholders(mockServiceContext);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("${PLACEHOLDER_SITE_NAVIGATION_MENU_MY_ACCOUNT_LINKS}"), equalTo(String.valueOf(navigationMenuId)));
		assertThat(results.get("${PLACEHOLDER_GROUP_ID}"), equalTo(String.valueOf(groupId)));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenAddsGuestViewPermissionToServiceContextModelPermissions() {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.getServiceContext();

		verify(mockModelPermissions, times(1)).addRolePermissions(RoleConstants.GUEST, ActionKeys.VIEW);
	}

	@Test
	public void getServiceContext_WhenNoError_ThenConfiguresServiceContextWithAddGroupPermissionsTrue() {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.getServiceContext();

		verify(mockServiceContext, times(1)).setAddGroupPermissions(true);
	}

	@Test
	public void getServiceContext_WhenNoError_ThenConfiguresServiceContextWithAddGuestPermissionsTrue() {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.getServiceContext();

		verify(mockServiceContext, times(1)).setAddGuestPermissions(true);
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsTheServiceContext() {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		ServiceContext result = digitalPlaceCouncilWebsiteElectricSiteInitializerService.getServiceContext();

		assertThat(result, sameInstance(mockServiceContext));
	}

	@Test(expected = PortalException.class)
	public void initializeContentSets_WhenErrorGettingOrCreatingContentSet_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockArticleWebContentService).getOrCreateContentSet(ContentSet.POPULAR_PAGES, mockServiceContext);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeContentSets(mockServiceContext);
	}

	@Test
	public void initializeContentSets_WhenNoError_ThenCreatesPopularPagesContentSet() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeContentSets(mockServiceContext);

		verify(mockArticleWebContentService, times(1)).getOrCreateContentSet(ContentSet.POPULAR_PAGES, mockServiceContext);
	}

	@Test
	public void initializeContentSets_WhenNoError_ThenCreatesServiceMenuContentSet() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeContentSets(mockServiceContext);

		verify(mockCategoryNavigationService, times(1)).getOrCreateContentSet(CategoryNavigationContentSet.SERVICE_MENU, mockServiceContext);
	}

	@Test
	public void initializeContentSets_WhenNoError_ThenCreatesLatestNewsContentSet() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeContentSets(mockServiceContext);

		verify(mockNewsWebContentService, times(1)).getOrCreateContentSet(NewsContentSet.LATEST_NEWS, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeDynamicDataLists_WhenErrorConfiguringOffer_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockOfferDDMService).configure(mockServiceContext);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeDynamicDataLists(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeDynamicDataLists_WhenErrorConfiguringPlace_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockPlaceDDMService).configure(mockServiceContext);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeDynamicDataLists(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeDynamicDataLists_WhenErrorConfiguringVenue_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockVenueDDMService).configure(mockServiceContext);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeDynamicDataLists(mockServiceContext);
	}

	@Test
	public void initializeDynamicDataLists_WhenNoErrors_ThenConfiguresTheDynamicDataLists() throws Exception {

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeDynamicDataLists(mockServiceContext);

		InOrder inOrder = inOrder(mockOfferDDMService, mockPlaceDDMService, mockVenueDDMService);
		inOrder.verify(mockOfferDDMService, times(1)).configure(mockServiceContext);
		inOrder.verify(mockPlaceDDMService, times(1)).configure(mockServiceContext);
		inOrder.verify(mockVenueDDMService, times(1)).configure(mockServiceContext);
		inOrder.verifyNoMoreInteractions();

	}

	@Test(expected = PortalException.class)
	public void initializePageTemplates_WhenErrorGettingOrCreatingPageTemplate_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockPageTemplateService).getOrCreatePageTemplate(DigitalPlacePageTemplateKey.FORM_PAGE, mockServiceContext);

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(mockServiceContext);
	}

	@Test
	public void initializePageTemplates_WhenNoError_ThenInitializesPageTemplateForArticlePage() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(mockServiceContext);

		verify(mockPageTemplateService, times(1)).getOrCreatePageTemplate(DigitalPlacePageTemplateKey.ARTICLE_PAGE, mockServiceContext);
	}

	@Test
	public void initializePageTemplates_WhenNoError_ThenInitializesPageTemplateForCategoryPage() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(mockServiceContext);

		verify(mockPageTemplateService, times(1)).getOrCreatePageTemplate(DigitalPlacePageTemplateKey.CATEGORY_PAGE, mockServiceContext);
	}

	@Test
	public void initializePageTemplates_WhenNoError_ThenInitializesPageTemplateForFormConfirmationPage() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(mockServiceContext);

		verify(mockPageTemplateService, times(1)).getOrCreatePageTemplate(DigitalPlacePageTemplateKey.FORM_CONFIRMATION_PAGE, mockServiceContext);
	}

	@Test
	public void initializePageTemplates_WhenNoError_ThenInitializesPageTemplateForFormPage() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(mockServiceContext);

		verify(mockPageTemplateService, times(1)).getOrCreatePageTemplate(DigitalPlacePageTemplateKey.FORM_PAGE, mockServiceContext);
	}

	@Test
	public void initializePageTemplates_WhenNoError_ThenInitializesPageTemplateForGuidePage() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializePageTemplates(mockServiceContext);

		verify(mockPageTemplateService, times(1)).getOrCreatePageTemplate(DigitalPlacePageTemplateKey.GUIDE_PAGE, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeSiteNavigationMenus_WhenErrorCreatingFooterLinks_ThenThrowsPortalException() throws Exception {
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.FOOTER_LINKS.getName(), NavigationMenuEntry.FOOTER_LINKS.getType()))
				.thenThrow(new PortalException());

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeSiteNavigationMenus(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeSiteNavigationMenus_WhenErrorCreatingSocialMediaLinks_ThenThrowsPortalException() throws Exception {
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.SOCIAL_MEDIA_LINKS.getName(), NavigationMenuEntry.SOCIAL_MEDIA_LINKS.getType()))
				.thenThrow(new PortalException());

		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeSiteNavigationMenus(mockServiceContext);
	}

	@Test
	public void initializeSiteNavigationMenus_WhenNoError_ThenCreatesSiteNavigationMenus() throws Exception {
		digitalPlaceCouncilWebsiteElectricSiteInitializerService.initializeSiteNavigationMenus(mockServiceContext);

		InOrder inOrder = inOrder(mockNavigationMenuInitializer);
		inOrder.verify(mockNavigationMenuInitializer, times(1)).getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.FOOTER_LINKS.getName(),
				NavigationMenuEntry.FOOTER_LINKS.getType());
		inOrder.verify(mockNavigationMenuInitializer, times(1)).getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.GLOBAL_NAVIGATION.getName(),
				NavigationMenuEntry.GLOBAL_NAVIGATION.getType());
		inOrder.verify(mockNavigationMenuInitializer, times(1)).getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.SOCIAL_MEDIA_LINKS.getName(),
				NavigationMenuEntry.SOCIAL_MEDIA_LINKS.getType());
	}

	private void mockGroupTypeSettings(String key, String value) throws PortalException {
		when(mockServiceContext.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockUnicodeProperties.getProperty(key)).thenReturn(value);
	}
}
