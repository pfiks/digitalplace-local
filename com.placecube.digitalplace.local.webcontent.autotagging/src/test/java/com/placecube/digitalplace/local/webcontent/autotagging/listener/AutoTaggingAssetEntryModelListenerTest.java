package com.placecube.digitalplace.local.webcontent.autotagging.listener;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.placecube.digitalplace.local.webcontent.autotagging.configuration.WebContentAutoTaggingGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.autotagging.model.WebContentAutoTaggingConfigurationEntry;
import com.placecube.digitalplace.local.webcontent.autotagging.service.WebContentAutoTaggingService;

@RunWith(MockitoJUnitRunner.class)
public class AutoTaggingAssetEntryModelListenerTest {

	@InjectMocks
	private AutoTaggingAssetEntryModelListener autoTaggingAssetEntryModelListener;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntry mockAssetEntryOriginal;

	@Mock
	private AssetRenderer mockAssetRenderer;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DDMStructure mockDDMStructure;

	private MockedStatic<ServiceContextThreadLocal> mockedServiceContextThreadLocal;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private WebContentAutoTaggingConfigurationEntry mockWebContentAutoTaggingConfigurationEntry1;

	@Mock
	private WebContentAutoTaggingConfigurationEntry mockWebContentAutoTaggingConfigurationEntry2;

	@Mock
	private WebContentAutoTaggingConfigurationEntry mockWebContentAutoTaggingConfigurationEntry3;

	@Mock
	private WebContentAutoTaggingGroupConfiguration mockWebContentAutoTaggingGroupConfiguration;

	@Mock
	private WebContentAutoTaggingService mockWebContentAutoTaggingService;

	@Before
	public void activateSetUp() {
		openMocks(this);
		mockedServiceContextThreadLocal = mockStatic(ServiceContextThreadLocal.class);
	}

	@Test
	public void onAfterUpdate_WhenClassNameIsJournalArticleAndConfigurationEnabledAndArticleApproved_ThenAppliesAllTheValidConfigurations() throws ConfigurationException {
		long groupId = 1;
		when(mockAssetEntry.getClassName()).thenReturn(JournalArticle.class.getName());
		when(mockAssetEntry.getGroupId()).thenReturn(groupId);
		when(mockConfigurationProvider.getGroupConfiguration(WebContentAutoTaggingGroupConfiguration.class, groupId)).thenReturn(mockWebContentAutoTaggingGroupConfiguration);
		when(mockWebContentAutoTaggingGroupConfiguration.enabled()).thenReturn(true);
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockJournalArticle);
		when(mockJournalArticle.isApproved()).thenReturn(true);
		when(mockJournalArticle.getDDMStructure()).thenReturn(mockDDMStructure);
		mockedServiceContextThreadLocal.when(ServiceContextThreadLocal::getServiceContext).thenReturn(mockServiceContext);
		when(mockWebContentAutoTaggingService.getConfigurationEntries(mockWebContentAutoTaggingGroupConfiguration))
				.thenReturn(Set.of(mockWebContentAutoTaggingConfigurationEntry1, mockWebContentAutoTaggingConfigurationEntry2, mockWebContentAutoTaggingConfigurationEntry3));
		when(mockWebContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(mockDDMStructure, mockWebContentAutoTaggingConfigurationEntry1)).thenReturn(false);
		when(mockWebContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(mockDDMStructure, mockWebContentAutoTaggingConfigurationEntry2)).thenReturn(true);
		when(mockWebContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(mockDDMStructure, mockWebContentAutoTaggingConfigurationEntry3)).thenReturn(true);
		when(mockWebContentAutoTaggingService.doesFieldHaveValue(mockJournalArticle, mockWebContentAutoTaggingConfigurationEntry2)).thenReturn(true);
		when(mockWebContentAutoTaggingService.doesFieldHaveValue(mockJournalArticle, mockWebContentAutoTaggingConfigurationEntry3)).thenReturn(false);

		autoTaggingAssetEntryModelListener.onAfterUpdate(mockAssetEntry, mockAssetEntry);

		verify(mockWebContentAutoTaggingService, never()).addTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry1, mockServiceContext);
		verify(mockWebContentAutoTaggingService, never()).removeTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry1);

		verify(mockWebContentAutoTaggingService, times(1)).addTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry2, mockServiceContext);
		verify(mockWebContentAutoTaggingService, never()).removeTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry2);

		verify(mockWebContentAutoTaggingService, never()).addTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry3, mockServiceContext);
		verify(mockWebContentAutoTaggingService, times(1)).removeTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry3);
	}

	@Test
	public void onAfterUpdate_WhenClassNameIsJournalArticleAndConfigurationEnabledAndArticleNotApproved_ThenNoActionIsPerformed() throws ConfigurationException {
		long groupId = 1;
		when(mockAssetEntry.getClassName()).thenReturn(JournalArticle.class.getName());
		when(mockAssetEntry.getGroupId()).thenReturn(groupId);
		when(mockConfigurationProvider.getGroupConfiguration(WebContentAutoTaggingGroupConfiguration.class, groupId)).thenReturn(mockWebContentAutoTaggingGroupConfiguration);
		when(mockWebContentAutoTaggingGroupConfiguration.enabled()).thenReturn(true);
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockJournalArticle);
		when(mockJournalArticle.isApproved()).thenReturn(false);

		autoTaggingAssetEntryModelListener.onAfterUpdate(mockAssetEntry, mockAssetEntry);

		verifyNoInteractions(mockWebContentAutoTaggingService);
	}

	@Test
	public void onAfterUpdate_WhenClassNameIsJournalArticleAndConfigurationNotEnabled_ThenNoActionIsPerformed() throws ConfigurationException {
		long groupId = 1;
		when(mockAssetEntry.getClassName()).thenReturn(JournalArticle.class.getName());
		when(mockAssetEntry.getGroupId()).thenReturn(groupId);
		when(mockConfigurationProvider.getGroupConfiguration(WebContentAutoTaggingGroupConfiguration.class, groupId)).thenReturn(mockWebContentAutoTaggingGroupConfiguration);
		when(mockWebContentAutoTaggingGroupConfiguration.enabled()).thenReturn(false);

		autoTaggingAssetEntryModelListener.onAfterUpdate(mockAssetEntry, mockAssetEntry);

		verifyNoInteractions(mockWebContentAutoTaggingService);
	}

	@Test
	public void onAfterUpdate_WhenClassNameIsJournalArticleAndExceptionRetrievingConfiguration_ThenNoActionIsPerformedAndNoExceptionThrown() throws ConfigurationException {
		long groupId = 1;
		when(mockAssetEntry.getClassName()).thenReturn(JournalArticle.class.getName());
		when(mockAssetEntry.getGroupId()).thenReturn(groupId);
		when(mockConfigurationProvider.getGroupConfiguration(WebContentAutoTaggingGroupConfiguration.class, groupId)).thenThrow(new ConfigurationException());

		try {
			autoTaggingAssetEntryModelListener.onAfterUpdate(mockAssetEntry, mockAssetEntry);
		} catch (Exception e) {
			fail();
		}

		verifyNoInteractions(mockWebContentAutoTaggingService);
	}

	@Test
	public void onAfterUpdate_WhenClassNameNotJournalArticle_ThenNoActionIsPerformed() {
		when(mockAssetEntry.getClassName()).thenReturn("someOtherClassName");

		autoTaggingAssetEntryModelListener.onAfterUpdate(mockAssetEntry, mockAssetEntry);

		verifyNoInteractions(mockConfigurationProvider, mockWebContentAutoTaggingService);
	}

	@After
	public void tearDown() {
		mockedServiceContextThreadLocal.close();
	}
}
