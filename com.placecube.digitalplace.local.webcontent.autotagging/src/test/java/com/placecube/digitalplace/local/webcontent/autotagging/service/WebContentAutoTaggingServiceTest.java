package com.placecube.digitalplace.local.webcontent.autotagging.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.asset.kernel.service.AssetTagLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.webcontent.autotagging.configuration.WebContentAutoTaggingGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.autotagging.model.WebContentAutoTaggingConfigurationEntry;

@RunWith(MockitoJUnitRunner.class)
public class WebContentAutoTaggingServiceTest {

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetTag mockAssetTag1;

	@Mock
	private AssetTag mockAssetTag2;

	@Mock
	private AssetTag mockAssetTag3;

	@Mock
	private AssetTag mockAssetTag4;

	@Mock
	private AssetTagLocalService mockAssetTagLocalService;

	@Mock
	private DDMFormField mockDDMFormField1;

	@Mock
	private DDMFormField mockDDMFormField2;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue1;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue2;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Value mockValue;

	@Mock
	private WebContentAutoTaggingConfigurationEntry mockWebContentAutoTaggingConfigurationEntry;

	@Mock
	private WebContentAutoTaggingGroupConfiguration mockWebContentAutoTaggingGroupConfiguration;

	@Mock
	private WebContentAutoTaggingService mockWebContentAutoTaggingService;

	@InjectMocks
	private WebContentAutoTaggingService webContentAutoTaggingService;

	@Before
	public void activateSetUp() {
		openMocks(this);
	}

	@Test
	public void addTags_WhenNoError_ThenAddsAllTheTagsNotAlreadyThere() throws PortalException {
		long entryId = 11;
		long groupId = 22;
		long userId = 33;
		when(mockWebContentAutoTaggingConfigurationEntry.getTags()).thenReturn(new String[] { "tag1", "tag2", "tag3", "tag4" });
		when(mockAssetEntry.getEntryId()).thenReturn(entryId);
		when(mockAssetTagLocalService.getAssetEntryAssetTags(entryId)).thenReturn(List.of(mockAssetTag1, mockAssetTag2));
		when(mockAssetTag1.getName()).thenReturn("TAG1");
		when(mockAssetTag2.getName()).thenReturn("tag2");
		when(mockAssetEntry.getGroupId()).thenReturn(groupId);
		when(mockAssetEntry.getUserId()).thenReturn(userId);
		when(mockAssetTagLocalService.getTag(groupId, "tag1")).thenReturn(mockAssetTag3);
		when(mockAssetTagLocalService.getTag(groupId, "tag3")).thenThrow(new PortalException());
		when(mockAssetTagLocalService.addTag(userId, groupId, "tag3", mockServiceContext)).thenReturn(mockAssetTag4);
		when(mockAssetTagLocalService.getTag(groupId, "tag4")).thenThrow(new PortalException());
		when(mockAssetTagLocalService.addTag(userId, groupId, "tag4", mockServiceContext)).thenThrow(new PortalException());

		webContentAutoTaggingService.addTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry, mockServiceContext);

		verify(mockAssetTagLocalService, times(1)).addAssetEntryAssetTag(entryId, mockAssetTag3);
		verify(mockAssetTagLocalService, times(1)).addAssetEntryAssetTag(entryId, mockAssetTag4);
	}

	@Test
	public void doesFieldHaveValue_WhenFieldWithSpecifiedReferenceIsFoundAndHasValue_ThenReturnsTrue() {
		when(mockJournalArticle.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockWebContentAutoTaggingConfigurationEntry.getFieldReferenceName()).thenReturn("myFieldNameRef1");
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(List.of(mockDDMFormFieldValue1, mockDDMFormFieldValue2));
		when(mockDDMFormFieldValue1.getDDMFormField()).thenReturn(mockDDMFormField1);
		when(mockDDMFormFieldValue2.getDDMFormField()).thenReturn(mockDDMFormField2);
		when(mockDDMFormField1.getProperty("fieldReference")).thenReturn(null);
		when(mockDDMFormField2.getProperty("fieldReference")).thenReturn("myFieldNameRef1");
		when(mockDDMFormFieldValue2.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(Locale.ENGLISH);
		when(mockValue.getString(Locale.ENGLISH)).thenReturn("fieldValueFound");

		boolean result = webContentAutoTaggingService.doesFieldHaveValue(mockJournalArticle, mockWebContentAutoTaggingConfigurationEntry);

		assertTrue(result);
	}

	@Test
	public void doesFieldHaveValue_WhenFieldWithSpecifiedReferenceIsFoundButHasNoValue_ThenReturnsFalse() {
		when(mockJournalArticle.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockWebContentAutoTaggingConfigurationEntry.getFieldReferenceName()).thenReturn("myFieldNameRef1");
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(List.of(mockDDMFormFieldValue1, mockDDMFormFieldValue2));
		when(mockDDMFormFieldValue1.getDDMFormField()).thenReturn(mockDDMFormField1);
		when(mockDDMFormFieldValue2.getDDMFormField()).thenReturn(mockDDMFormField2);
		when(mockDDMFormField1.getProperty("fieldReference")).thenReturn(null);
		when(mockDDMFormField2.getProperty("fieldReference")).thenReturn("myFieldNameRef1");
		when(mockDDMFormFieldValue2.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(Locale.ENGLISH);
		when(mockValue.getString(Locale.ENGLISH)).thenReturn("");

		boolean result = webContentAutoTaggingService.doesFieldHaveValue(mockJournalArticle, mockWebContentAutoTaggingConfigurationEntry);

		assertFalse(result);
	}

	@Test
	public void doesFieldHaveValue_WhenNoFieldWithSpecifiedReference_ThenReturnsFalse() {
		when(mockJournalArticle.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(List.of(mockDDMFormFieldValue1, mockDDMFormFieldValue2));
		when(mockDDMFormFieldValue1.getDDMFormField()).thenReturn(mockDDMFormField1);
		when(mockWebContentAutoTaggingConfigurationEntry.getFieldReferenceName()).thenReturn("myFieldNameRef1");
		when(mockDDMFormFieldValue2.getDDMFormField()).thenReturn(mockDDMFormField2);
		when(mockDDMFormField1.getProperty("fieldReference")).thenReturn(null);
		when(mockDDMFormField2.getProperty("fieldReference")).thenReturn("someOtherRef");

		boolean result = webContentAutoTaggingService.doesFieldHaveValue(mockJournalArticle, mockWebContentAutoTaggingConfigurationEntry);

		assertFalse(result);
	}

	@Test
	public void getConfigurationEntries_WhenNoError_ThenReturnsOnlyTheValidConfiguredEntries() {
		when(mockWebContentAutoTaggingGroupConfiguration.autoTaggingConfigurations())
				.thenReturn(new String[] { "3=myFieldNameRef1=tag1A,tag1B", "2=myFieldRef4=", "1==tag3", "2==", "0=myFieldRef2=tag2", "=myFieldRef4=", "==tag5", "==", "someRandomConfig" });

		Set<WebContentAutoTaggingConfigurationEntry> results = webContentAutoTaggingService.getConfigurationEntries(mockWebContentAutoTaggingGroupConfiguration);

		assertThat(results.size(), equalTo(1));
		WebContentAutoTaggingConfigurationEntry entry1 = results.iterator().next();
		assertThat(entry1.getStructureId(), equalTo(3l));
		assertThat(entry1.getFieldReferenceName(), equalTo("myFieldNameRef1"));
		assertThat(entry1.getTags(), equalTo(new String[] { "tag1A", "tag1B" }));
	}

	@Test
	public void getConfigurationEntries_WhenNoErrorAndNoConfigurations_ThenReturnsEmptyList() {
		when(mockWebContentAutoTaggingGroupConfiguration.autoTaggingConfigurations()).thenReturn(null);

		Set<WebContentAutoTaggingConfigurationEntry> results = webContentAutoTaggingService.getConfigurationEntries(mockWebContentAutoTaggingGroupConfiguration);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void removeTags_WhenEntryHasNoTags_ThenNoActionIsPerformed() {
		long entryId = 11;
		when(mockWebContentAutoTaggingConfigurationEntry.getTags()).thenReturn(new String[] { "tag1", "tag2" });
		when(mockAssetEntry.getEntryId()).thenReturn(entryId);
		when(mockAssetTagLocalService.getAssetEntryAssetTags(entryId)).thenReturn(Collections.emptyList());

		webContentAutoTaggingService.removeTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry);

		verify(mockAssetTagLocalService, never()).deleteAssetEntryAssetTag(anyLong(), any());
	}

	@Test
	public void removeTags_WhenEntryHasTags_ThenRemovesTheTagsThatMatchTheConfiguration() {
		long entryId = 11;
		when(mockWebContentAutoTaggingConfigurationEntry.getTags()).thenReturn(new String[] { "tag1", "tag2" });
		when(mockAssetEntry.getEntryId()).thenReturn(entryId);
		when(mockAssetTagLocalService.getAssetEntryAssetTags(entryId)).thenReturn(List.of(mockAssetTag1, mockAssetTag2, mockAssetTag3));
		when(mockAssetTag1.getName()).thenReturn("TAG1");
		when(mockAssetTag2.getName()).thenReturn("tag2");
		when(mockAssetTag3.getName()).thenReturn("tag3");

		webContentAutoTaggingService.removeTags(mockAssetEntry, mockWebContentAutoTaggingConfigurationEntry);

		verify(mockAssetTagLocalService, times(1)).deleteAssetEntryAssetTag(entryId, mockAssetTag2);
		verify(mockAssetTagLocalService, never()).deleteAssetEntryAssetTag(entryId, mockAssetTag1);
		verify(mockAssetTagLocalService, never()).deleteAssetEntryAssetTag(entryId, mockAssetTag3);
	}

	@Test
	public void shouldAutoTaggingConfigurationEntryBeApplied_WhenException_ThenReturnsFalse() {
		boolean result = webContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(null, mockWebContentAutoTaggingConfigurationEntry);

		assertFalse(result);
	}

	@Test
	public void shouldAutoTaggingConfigurationEntryBeApplied_WhenStructureIdIsDifferent_ThenReturnsFalse() {
		when(mockDDMStructure.getStructureId()).thenReturn(11l);
		when(mockWebContentAutoTaggingConfigurationEntry.getStructureId()).thenReturn(22l);

		boolean result = webContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(mockDDMStructure, mockWebContentAutoTaggingConfigurationEntry);

		assertFalse(result);
	}

	@Test
	public void shouldAutoTaggingConfigurationEntryBeApplied_WhenStructureIdIsTheSameAndFieldFound_ThenReturnsTrue() throws PortalException {
		when(mockDDMStructure.getStructureId()).thenReturn(11l);
		when(mockWebContentAutoTaggingConfigurationEntry.getStructureId()).thenReturn(11l);
		when(mockWebContentAutoTaggingConfigurationEntry.getFieldReferenceName()).thenReturn("myFieldRefValue");
		when(mockDDMStructure.getDDMFormFieldByFieldReference("myFieldRefValue")).thenReturn(mockDDMFormField1);

		boolean result = webContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(mockDDMStructure, mockWebContentAutoTaggingConfigurationEntry);

		assertTrue(result);
	}

	@Test
	public void shouldAutoTaggingConfigurationEntryBeApplied_WhenStructureIdIsTheSameAndFieldNotFound_ThenReturnsFalse() throws PortalException {
		when(mockDDMStructure.getStructureId()).thenReturn(11l);
		when(mockWebContentAutoTaggingConfigurationEntry.getStructureId()).thenReturn(11l);
		when(mockWebContentAutoTaggingConfigurationEntry.getFieldReferenceName()).thenReturn("myFieldRefValue");
		when(mockDDMStructure.getDDMFormFieldByFieldReference("myFieldRefValue")).thenReturn(null);

		boolean result = webContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(mockDDMStructure, mockWebContentAutoTaggingConfigurationEntry);

		assertFalse(result);
	}

}
