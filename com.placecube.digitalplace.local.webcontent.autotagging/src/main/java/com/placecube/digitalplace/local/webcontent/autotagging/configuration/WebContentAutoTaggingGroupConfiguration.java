package com.placecube.digitalplace.local.webcontent.autotagging.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "web-content", scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.local.webcontent.autotagging.configuration.WebContentAutoTaggingGroupConfiguration", localization = "content/Language", name = "webcontent-auto-tagging")
public interface WebContentAutoTaggingGroupConfiguration {

	@Meta.AD(required = false, deflt = "", name = "webcontent-auto-tagging-configuration", description = "webcontent-auto-tagging-configuration-description")
	String[] autoTaggingConfigurations();

	@Meta.AD(required = false, deflt = "false")
	boolean enabled();

}
