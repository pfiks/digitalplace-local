package com.placecube.digitalplace.local.webcontent.autotagging.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.asset.kernel.service.AssetTagLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.autotagging.configuration.WebContentAutoTaggingGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.autotagging.model.WebContentAutoTaggingConfigurationEntry;

@Component(immediate = true, service = WebContentAutoTaggingService.class)
public class WebContentAutoTaggingService {

	private static final Log LOG = LogFactoryUtil.getLog(WebContentAutoTaggingService.class);

	@Reference
	private AssetTagLocalService assetTagLocalService;

	public void addTags(AssetEntry assetEntry, WebContentAutoTaggingConfigurationEntry webContentAutoTaggingConfigurationEntry, ServiceContext serviceContext) {
		String[] tagsToAdd = webContentAutoTaggingConfigurationEntry.getTags();
		List<AssetTag> currentEntryTags = assetTagLocalService.getAssetEntryAssetTags(assetEntry.getEntryId());

		for (String tagToAdd : tagsToAdd) {
			if (currentEntryTags.stream().noneMatch(entry -> entry.getName().equals(tagToAdd))) {
				try {
					AssetTag tag = getOrCreateAssetTag(assetEntry.getUserId(), assetEntry.getGroupId(), tagToAdd, serviceContext);
					assetTagLocalService.addAssetEntryAssetTag(assetEntry.getEntryId(), tag);
				} catch (Exception e) {
					LOG.warn("Unable to add auto-tag", e);
				}
			}
		}
	}

	public boolean doesFieldHaveValue(JournalArticle article, WebContentAutoTaggingConfigurationEntry webContentAutoTaggingConfigurationEntry) {
		String fieldReferenceToMatch = webContentAutoTaggingConfigurationEntry.getFieldReferenceName();

		List<DDMFormFieldValue> ddmFormFieldValues = article.getDDMFormValues().getDDMFormFieldValues();

		List<DDMFormFieldValue> fieldValuesFound = ddmFormFieldValues.stream()
				.filter(field -> fieldReferenceToMatch.equals(GetterUtil.getString(field.getDDMFormField().getProperty("fieldReference")))).collect(Collectors.toList());

		return fieldValuesFound.stream().anyMatch(fieldValue -> Validator.isNotNull(fieldValue.getValue().getString(fieldValue.getValue().getDefaultLocale())));
	}

	public Set<WebContentAutoTaggingConfigurationEntry> getConfigurationEntries(WebContentAutoTaggingGroupConfiguration configuration) {
		Set<WebContentAutoTaggingConfigurationEntry> results = new HashSet<>();

		String[] autoTaggingConfigurations = configuration.autoTaggingConfigurations();
		if (ArrayUtil.isNotEmpty(autoTaggingConfigurations)) {
			for (String configString : autoTaggingConfigurations) {
				try {
					String[] values = configString.split(StringPool.EQUAL);

					long structureId = GetterUtil.getLong(values[0], 0l);
					String fieldRefName = values[1];
					String[] tags = values[2].split(StringPool.COMMA);

					if (structureId > 0 && Validator.isNotNull(fieldRefName) && ArrayUtil.isNotEmpty(tags)) {
						results.add(new WebContentAutoTaggingConfigurationEntry(structureId, fieldRefName, tags));
					}
				} catch (Exception e) {
					LOG.warn("Unable to add auto-tag configuration: " + configString + ", error: " + e.getMessage());
					LOG.debug(e);
				}
			}
		}
		return results;
	}

	public void removeTags(AssetEntry assetEntry, WebContentAutoTaggingConfigurationEntry webContentAutoTaggingConfigurationEntry) {
		String[] tagsToRemove = webContentAutoTaggingConfigurationEntry.getTags();
		List<AssetTag> currentEntryTags = assetTagLocalService.getAssetEntryAssetTags(assetEntry.getEntryId());
		for (AssetTag assetTag : currentEntryTags) {
			if (ArrayUtil.contains(tagsToRemove, assetTag.getName())) {
				assetTagLocalService.deleteAssetEntryAssetTag(assetEntry.getEntryId(), assetTag);
			}
		}
	}

	public boolean shouldAutoTaggingConfigurationEntryBeApplied(DDMStructure ddmStructure, WebContentAutoTaggingConfigurationEntry webContentAutoTaggingConfigurationEntry) {
		try {
			return ddmStructure.getStructureId() == webContentAutoTaggingConfigurationEntry.getStructureId()
					&& Validator.isNotNull(ddmStructure.getDDMFormFieldByFieldReference(webContentAutoTaggingConfigurationEntry.getFieldReferenceName()));
		} catch (Exception e) {
			LOG.warn("Unable to check if auto-tagging configuration should be applied", e);
			return false;
		}
	}

	private AssetTag getOrCreateAssetTag(long userId, long groupId, String name, ServiceContext serviceContext) throws PortalException {
		try {
			return assetTagLocalService.getTag(groupId, name);
		} catch (Exception e) {
			return assetTagLocalService.addTag(userId, groupId, name, serviceContext);
		}
	}

}
