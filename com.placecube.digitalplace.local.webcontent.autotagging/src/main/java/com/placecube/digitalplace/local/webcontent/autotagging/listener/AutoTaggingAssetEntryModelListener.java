package com.placecube.digitalplace.local.webcontent.autotagging.listener;

import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.placecube.digitalplace.local.webcontent.autotagging.configuration.WebContentAutoTaggingGroupConfiguration;
import com.placecube.digitalplace.local.webcontent.autotagging.model.WebContentAutoTaggingConfigurationEntry;
import com.placecube.digitalplace.local.webcontent.autotagging.service.WebContentAutoTaggingService;

@Component(immediate = true, service = ModelListener.class)
public class AutoTaggingAssetEntryModelListener extends BaseModelListener<AssetEntry> {

	private static final Log LOG = LogFactoryUtil.getLog(AutoTaggingAssetEntryModelListener.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private WebContentAutoTaggingService webContentAutoTaggingService;

	@Override
	public void onAfterUpdate(AssetEntry originalAssetEntry, AssetEntry assetEntry) throws ModelListenerException {
		try {
			if (assetEntry.getClassName().equals(JournalArticle.class.getName())) {
				WebContentAutoTaggingGroupConfiguration config = configurationProvider.getGroupConfiguration(WebContentAutoTaggingGroupConfiguration.class, assetEntry.getGroupId());
				if (config.enabled()) {
					JournalArticle article = (JournalArticle) assetEntry.getAssetRenderer().getAssetObject();

					if (article.isApproved()) {
						DDMStructure ddmStructure = article.getDDMStructure();
						ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

						Set<WebContentAutoTaggingConfigurationEntry> configuredEntries = webContentAutoTaggingService.getConfigurationEntries(config);

						for (WebContentAutoTaggingConfigurationEntry webContentAutoTaggingConfigurationEntry : configuredEntries) {
							processConfigurationEntry(assetEntry, article, ddmStructure, serviceContext, webContentAutoTaggingConfigurationEntry);
						}
					}

				}
			}
		} catch (Exception e) {
			LOG.warn("Unable to update auto-tagging", e);
		}
	}

	private void processConfigurationEntry(AssetEntry assetEntry, JournalArticle article, DDMStructure ddmStructure, ServiceContext serviceContext,
			WebContentAutoTaggingConfigurationEntry webContentAutoTaggingConfigurationEntry) {
		if (webContentAutoTaggingService.shouldAutoTaggingConfigurationEntryBeApplied(ddmStructure, webContentAutoTaggingConfigurationEntry)) {
			boolean addTags = webContentAutoTaggingService.doesFieldHaveValue(article, webContentAutoTaggingConfigurationEntry);
			if (addTags) {
				webContentAutoTaggingService.addTags(assetEntry, webContentAutoTaggingConfigurationEntry, serviceContext);
			} else {
				webContentAutoTaggingService.removeTags(assetEntry, webContentAutoTaggingConfigurationEntry);
			}
		}
	}

}
