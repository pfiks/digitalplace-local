package com.placecube.digitalplace.local.webcontent.autotagging.model;

public class WebContentAutoTaggingConfigurationEntry {

	private final String fieldReferenceName;
	private final long structureId;
	private final String[] tags;

	public WebContentAutoTaggingConfigurationEntry(long structureId, String fieldReferenceName, String[] tags) {
		this.structureId = structureId;
		this.fieldReferenceName = fieldReferenceName;
		this.tags = tags;
	}

	public String getFieldReferenceName() {
		return fieldReferenceName;
	}

	public long getStructureId() {
		return structureId;
	}

	public String[] getTags() {
		return tags;
	}

}
