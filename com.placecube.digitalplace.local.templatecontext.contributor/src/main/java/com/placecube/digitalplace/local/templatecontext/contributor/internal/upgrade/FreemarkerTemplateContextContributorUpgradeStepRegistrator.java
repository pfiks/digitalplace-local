package com.placecube.digitalplace.local.templatecontext.contributor.internal.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.templatecontext.contributor.internal.upgrade.upgrade_1_0_0.DDMTemplateUpgradeProcess;
import com.placecube.digitalplace.local.templatecontext.contributor.internal.upgrade.upgrade_1_0_0.FragmentsUpgradeProcess;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class FreemarkerTemplateContextContributorUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private FragmentEntryLocalService fragmentEntryLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new DDMTemplateUpgradeProcess(companyLocalService, ddmTemplateLocalService), new FragmentsUpgradeProcess(companyLocalService, fragmentEntryLocalService));
	}
}
