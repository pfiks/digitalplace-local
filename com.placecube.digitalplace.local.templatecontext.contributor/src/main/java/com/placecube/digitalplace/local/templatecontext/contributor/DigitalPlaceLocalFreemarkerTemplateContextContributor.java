package com.placecube.digitalplace.local.templatecontext.contributor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.template.TemplateContextContributor;
import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentService;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService;
import com.placecube.digitalplace.local.assetpublisher.service.AssetPublisherTemplateService;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.navigationmenu.helper.SiteNavigationMenItemHelper;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;
import com.placecube.digitalplace.local.webcontent.webdocs.util.WebdocsWebContentUtil;

@Component(immediate = true, property = "type=" + TemplateContextContributor.TYPE_GLOBAL, service = TemplateContextContributor.class)

public class DigitalPlaceLocalFreemarkerTemplateContextContributor implements TemplateContextContributor {

	public static final String DIGITALPLACE_ASSET_PUBLISHER_TEMPLATE_SERVICE = "digitalplace_assetPublisherTemplateService";
	public static final String DIGITALPLACE_CATEGORY_NAVIGATION_SERVICE = "digitalplace_categoryNavigationService";
	public static final String DIGITALPLACE_GUIDE_WEB_CONTENT_SERVICE = "digitalplace_guideWebContentService";
	public static final String DIGITALPLACE_INFO_COLLECTION_PROVIDER_SERVICE = "digitalplace_infoCollectionProviderService";
	public static final String DIGITALPLACE_MY_LOCAL_CONTENT_SERVICE = "digitalplace_myLocalContentService";
	public static final String DIGITALPLACE_NAV_ITEM_TEMPLATE_SERVICE = "digitalplace_navItemTemplateService";
	public static final String DIGITALPLACE_PERMISSION_SERVICE = "digitalplace_permissionService";
	public static final String DIGITALPLACE_SITE_NAVIGATION_MENU_ITEM_HELPER = "digitalplace_siteNavigationMenuItemHelper";
	public static final String DIGITALPLACE_WEBDOCS_WEB_CONTENT_UTIL = "digitalplace_webdocsWebContentUtil";

	@Reference
	private AssetPublisherTemplateService assetPublisherTemplateService;

	@Reference
	private CategoryNavigationService categoryNavigationService;

	@Reference
	private DigitalPlacePermissionService digitalplacePermissionService;

	@Reference
	private GuideWebContentService guideWebContentService;

	@Reference
	private InfoCollectionProviderService infoCollectionProviderService;

	@Reference
	private MyLocalContentService myLocalContentService;

	@Reference
	private NavItemTemplateService navItemTemplateService;

	@Reference
	private SiteNavigationMenItemHelper siteNavigationMenItemHelper;

	@Reference
	private WebdocsWebContentUtil webdocsWebContentUtil;

	@Override
	public void prepare(Map<String, Object> contextObjects, HttpServletRequest httpServletRequest) {

		contextObjects.put(DIGITALPLACE_PERMISSION_SERVICE, digitalplacePermissionService);
		contextObjects.put(DIGITALPLACE_SITE_NAVIGATION_MENU_ITEM_HELPER, siteNavigationMenItemHelper);
		contextObjects.put(DIGITALPLACE_INFO_COLLECTION_PROVIDER_SERVICE, infoCollectionProviderService);
		contextObjects.put(DIGITALPLACE_CATEGORY_NAVIGATION_SERVICE, categoryNavigationService);
		contextObjects.put(DIGITALPLACE_NAV_ITEM_TEMPLATE_SERVICE, navItemTemplateService);
		contextObjects.put(DIGITALPLACE_ASSET_PUBLISHER_TEMPLATE_SERVICE, assetPublisherTemplateService);
		contextObjects.put(DIGITALPLACE_GUIDE_WEB_CONTENT_SERVICE, guideWebContentService);
		contextObjects.put(DIGITALPLACE_MY_LOCAL_CONTENT_SERVICE, myLocalContentService);
		contextObjects.put(DIGITALPLACE_WEBDOCS_WEB_CONTENT_UTIL, webdocsWebContentUtil);
	}

}
