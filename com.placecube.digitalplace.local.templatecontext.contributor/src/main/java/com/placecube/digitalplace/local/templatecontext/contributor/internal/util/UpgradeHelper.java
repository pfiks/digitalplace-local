package com.placecube.digitalplace.local.templatecontext.contributor.internal.util;

import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_ASSET_PUBLISHER_TEMPLATE_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_CATEGORY_NAVIGATION_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_GUIDE_WEB_CONTENT_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_INFO_COLLECTION_PROVIDER_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_MY_LOCAL_CONTENT_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_NAV_ITEM_TEMPLATE_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_PERMISSION_SERVICE;
import static com.placecube.digitalplace.local.templatecontext.contributor.DigitalPlaceLocalFreemarkerTemplateContextContributor.DIGITALPLACE_SITE_NAVIGATION_MENU_ITEM_HELPER;

import java.io.File;
import java.io.IOException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.FileUtil;
import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentService;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService;
import com.placecube.digitalplace.local.assetpublisher.service.AssetPublisherTemplateService;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.navigationmenu.helper.SiteNavigationMenItemHelper;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;

public class UpgradeHelper {

	private UpgradeHelper() {

	}

	public static boolean hasComponent(String script) {
		return script.contains(AssetPublisherTemplateService.class.getName()) || script.contains(CategoryNavigationService.class.getName())
				|| script.contains(DigitalPlacePermissionService.class.getName()) || script.contains(GuideWebContentService.class.getName())
				|| script.contains(InfoCollectionProviderService.class.getName()) || script.contains(MyLocalContentService.class.getName()) || script.contains(NavItemTemplateService.class.getName())
				|| script.contains(SiteNavigationMenItemHelper.class.getName());

	}

	public static String upgradeScript(String script) {

		String upgradedScript = replaceServiceLocatorCallByReference(script, AssetPublisherTemplateService.class, DIGITALPLACE_ASSET_PUBLISHER_TEMPLATE_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, DigitalPlacePermissionService.class, DIGITALPLACE_PERMISSION_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, InfoCollectionProviderService.class, DIGITALPLACE_INFO_COLLECTION_PROVIDER_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, CategoryNavigationService.class, DIGITALPLACE_CATEGORY_NAVIGATION_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, GuideWebContentService.class, DIGITALPLACE_GUIDE_WEB_CONTENT_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, MyLocalContentService.class, DIGITALPLACE_MY_LOCAL_CONTENT_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, NavItemTemplateService.class, DIGITALPLACE_NAV_ITEM_TEMPLATE_SERVICE);
		return replaceServiceLocatorCallByReference(upgradedScript, SiteNavigationMenItemHelper.class, DIGITALPLACE_SITE_NAVIGATION_MENU_ITEM_HELPER);

	}

	public static File getOutputFile(UpgradeProcess upgradeProcess) {
		return new File(System.getProperty("liferay.home") + "/upgrade/" + upgradeProcess.getClass().getName() + ".txt");
	}

	public static void writeOutput(File file, String output) throws IOException {
		FileUtil.write(file, output, false, true);
	}

	private static String replaceServiceLocatorCallByReference(String script, Class clazz, String reference) {
		return script.replace("serviceLocator.findService(\"" + clazz.getName() + "\")", reference);
	}
}
