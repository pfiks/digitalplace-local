package com.placecube.digitalplace.local.templatecontext.contributor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentService;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService;
import com.placecube.digitalplace.local.assetpublisher.service.AssetPublisherTemplateService;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.navigationmenu.helper.SiteNavigationMenItemHelper;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;
import com.placecube.digitalplace.local.webcontent.webdocs.util.WebdocsWebContentUtil;

@RunWith(MockitoJUnitRunner.class)
public class DigitalPlaceLocalFreemarkerTemplateContextContributorTest {

	@InjectMocks
	private DigitalPlaceLocalFreemarkerTemplateContextContributor digitalPlaceLocalFreemarkerTemplateContextContributor;

	@Mock
	private AssetPublisherTemplateService mockAssetPublisherTemplateService;

	@Mock
	private CategoryNavigationService mockCategoryNavigationService;

	@Mock
	private Map<String, Object> mockContextObjects;

	@Mock
	private DigitalPlacePermissionService mockDigitalplacePermissionService;

	@Mock
	private GuideWebContentService mockGuideWebContentService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private InfoCollectionProviderService mockInfoCollectionProviderService;

	@Mock
	private MyLocalContentService mockMyLocalContentService;

	@Mock
	private NavItemTemplateService mockNavItemTemplateService;

	@Mock
	private SiteNavigationMenItemHelper mockSiteNavigationMenItemHelper;

	@Mock
	private WebdocsWebContentUtil mockWebdocsWebContentUtil;

	@Test
	public void prepare_WhenNoErrors_ThenPutsDigitalPlaceLocalServicesInContext() {
		digitalPlaceLocalFreemarkerTemplateContextContributor.prepare(mockContextObjects, mockHttpServletRequest);

		verify(mockContextObjects, times(1)).put("digitalplace_assetPublisherTemplateService", mockAssetPublisherTemplateService);
		verify(mockContextObjects, times(1)).put("digitalplace_categoryNavigationService", mockCategoryNavigationService);
		verify(mockContextObjects, times(1)).put("digitalplace_permissionService", mockDigitalplacePermissionService);
		verify(mockContextObjects, times(1)).put("digitalplace_guideWebContentService", mockGuideWebContentService);
		verify(mockContextObjects, times(1)).put("digitalplace_infoCollectionProviderService", mockInfoCollectionProviderService);
		verify(mockContextObjects, times(1)).put("digitalplace_myLocalContentService", mockMyLocalContentService);
		verify(mockContextObjects, times(1)).put("digitalplace_navItemTemplateService", mockNavItemTemplateService);
		verify(mockContextObjects, times(1)).put("digitalplace_siteNavigationMenuItemHelper", mockSiteNavigationMenItemHelper);
		verify(mockContextObjects, times(1)).put("digitalplace_webdocsWebContentUtil", mockWebdocsWebContentUtil);
	}

}
