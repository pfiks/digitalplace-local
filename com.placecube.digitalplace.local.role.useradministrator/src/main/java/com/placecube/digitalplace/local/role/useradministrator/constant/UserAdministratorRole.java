package com.placecube.digitalplace.local.role.useradministrator.constant;

public final class UserAdministratorRole {

	public static final String ROLE_NAME = "User Administrator";

	private UserAdministratorRole() {
	}

}
