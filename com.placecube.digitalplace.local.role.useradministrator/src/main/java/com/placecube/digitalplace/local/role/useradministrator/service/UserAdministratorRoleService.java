package com.placecube.digitalplace.local.role.useradministrator.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.placecube.digitalplace.local.role.useradministrator.constant.UserAdministratorRole;

@Component(immediate = true, service = UserAdministratorRoleService.class)
public class UserAdministratorRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(UserAdministratorRoleService.class);

	@Reference
	private RoleLocalService roleLocalService;

	public Role getUserAdministratorRole(long companyId) throws PortalException {
		return roleLocalService.getRole(companyId, UserAdministratorRole.ROLE_NAME);
	}

	public long getUserAdministratorRoleId(long companyId) throws PortalException {
		return getUserAdministratorRole(companyId).getRoleId();
	}

	public boolean hasRole(long userId, long companyId) {
		try {
			return roleLocalService.hasUserRole(userId, companyId, UserAdministratorRole.ROLE_NAME, true);
		} catch (PortalException e) {
			LOG.error("Unable to retrieve User Administrator Role", e);
			return false;
		}
	}
}
