package com.placecube.digitalplace.local.role.useradministrator.lifecyclelistener;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.pfiks.role.service.RoleCreatorInputStreamService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class UserAdministratorRoleLifecycleListener extends BasePortalInstanceLifecycleListener {

	@Reference
	private RoleCreatorInputStreamService roleCreatorInputStreamService;

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED)
	private ModuleServiceLifecycle portalInitialized;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		InputStream roleInputStream = getClass().getClassLoader().getResourceAsStream("/role-definition.xml");
		roleCreatorInputStreamService.configureMissingRoleFromInputStream(company, roleInputStream);
	}
}
