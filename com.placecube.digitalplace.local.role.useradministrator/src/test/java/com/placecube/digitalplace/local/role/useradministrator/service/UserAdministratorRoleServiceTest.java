package com.placecube.digitalplace.local.role.useradministrator.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.placecube.digitalplace.local.role.useradministrator.constant.UserAdministratorRole;

public class UserAdministratorRoleServiceTest extends PowerMockito {

	@InjectMocks
	private UserAdministratorRoleService userAdministratorRoleService;

	@Mock
	private Role mockRole;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void getUserAdministratorRole_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, UserAdministratorRole.ROLE_NAME)).thenThrow(new PortalException());

		userAdministratorRoleService.getUserAdministratorRole(companyId);
	}

	@Test
	public void getUserAdministratorRole_WhenNoError_ThenReturnsTheRole() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, UserAdministratorRole.ROLE_NAME)).thenReturn(mockRole);

		Role result = userAdministratorRoleService.getUserAdministratorRole(companyId);

		assertThat(result, sameInstance(mockRole));
	}

	@Test(expected = PortalException.class)
	public void getUserAdministratorRoleId_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, UserAdministratorRole.ROLE_NAME)).thenThrow(new PortalException());

		userAdministratorRoleService.getUserAdministratorRoleId(companyId);
	}

	@Test
	public void getUserAdministratorRoleId_WhenNoError_ThenReturnsTheRoleId() throws PortalException {
		long companyId = 1;
		long roleId = 2;
		when(mockRoleLocalService.getRole(companyId, UserAdministratorRole.ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);

		long result = userAdministratorRoleService.getUserAdministratorRoleId(companyId);

		assertThat(result, equalTo(roleId));
	}

	@Test
	public void hasRole_WheNoError_ThenCallsHasUserRole() throws PortalException {

		long userId = 1;
		long companyId = 2;

		userAdministratorRoleService.hasRole(userId, companyId);

		verify(mockRoleLocalService, times(1)).hasUserRole(userId, companyId, UserAdministratorRole.ROLE_NAME, true);
	}

	@Test
	public void hasRole_WheErrorCallsHasUserRoles_ThenReturnsFalse() throws PortalException {

		long userId = 1;
		long companyId = 2;

		when(mockRoleLocalService.hasUserRole(userId, companyId, UserAdministratorRole.ROLE_NAME, true)).thenThrow(new PortalException());

		assertFalse(userAdministratorRoleService.hasRole(userId, companyId));

	}

}
