package com.placecube.digitalplace.local.connector.service.impl.util;

import com.liferay.osgi.service.tracker.collections.map.ServiceReferenceMapper;
import com.placecube.digitalplace.local.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.local.connector.model.Connector;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = ConnectorRegistryUtil.class)
public class ConnectorRegistryUtil {

	public ServiceReferenceMapper<String, Connector> getInterfaceServiceReferenceMapper() {
		return (serviceReference, emitter) -> emitter.emit((String) serviceReference.getProperty(ConnectorConstants.CONNECTOR_TYPE));
	}

}
