package com.placecube.digitalplace.local.connector.service.impl.util;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.portal.kernel.util.MapUtil;
import com.placecube.digitalplace.local.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.local.connector.model.Connector;

import java.util.Comparator;

public class ConnectorDisplayPriorityComparator implements Comparator<ServiceWrapper<Connector>> {

	@Override
	public int compare(ServiceWrapper<Connector> serviceWrapper1, ServiceWrapper<Connector> serviceWrapper2) {

		String service1value = MapUtil.getString(serviceWrapper1.getProperties(), ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY);
		String service2Value = MapUtil.getString(serviceWrapper2.getProperties(), ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY);

		return service1value.compareTo(service2Value);
	}
}
