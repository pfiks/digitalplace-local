package com.placecube.digitalplace.local.user.expando.preferredcontact.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.user.expando.preferredcontact.configuration.PreferredContactMethodCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.ContactMethod;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.PreferredMethodConstants;
import com.placecube.digitalplace.local.user.expando.preferredcontact.service.LanguageMessageService;
import com.placecube.digitalplace.local.user.expando.preferredcontact.service.PreferredMethodContactService;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;

public class PreferredContactCreateUserAccountUserFieldTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	private Locale locale = Locale.ENGLISH;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private LanguageMessageService mockLanguageMessageService;

	@Mock
	private PreferredContactMethodCompanyConfiguration mockPreferredContactMethodCompanyConfiguration;

	@Mock
	private PreferredMethodContactService mockPreferredMethodContactService;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private PreferredContactUserAccountField preferredContactCreateUserAccountField;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getAccountCreationStep_WhenNoErrors_ThenAdditonalDetailsStepReturned() {

		AccountCreationStep result = preferredContactCreateUserAccountField.getAccountCreationStep();

		assertEquals(result, AccountCreationStep.ADDITIONAL_DETAILS);
	}

	@Test
	public void getBundleId_WhenNoErrors_ThenBundleIdIsReturned() {

		String result = preferredContactCreateUserAccountField.getBundleId();

		assertEquals(result, PreferredMethodConstants.BUNDLE_ID);
	}

	@Test
	public void getDependentRequiredFieldNamesAndMessage_WhenSelectedValueIsBusinessPhone_ThenReturnMapWithBusinessPhoneField() {
		when(mockLanguageMessageService.getMessage("business-number-required", locale)).thenReturn("business-message");

		Map<String, String> results = preferredContactCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(ContactMethod.BUSINESS_PHONE.getKey(), locale);

		assertThat(results, IsMapContaining.hasEntry("businessPhoneNumber", "business-message"));
	}

	@Test
	public void getDependentRequiredFieldNamesAndMessage_WhenSelectedValueIsEmailAddress_ThenReturnMapWithEmailAddressField() {
		when(mockLanguageMessageService.getMessage("email-address-required", locale)).thenReturn("email-message");

		Map<String, String> results = preferredContactCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(ContactMethod.EMAIL.getKey(), locale);

		assertThat(results, IsMapContaining.hasEntry("emailAddress", "email-message"));
	}

	@Test
	public void getDependentRequiredFieldNamesAndMessage_WhenSelectedValueIsHomePhone_ThenReturnMapWithHomePhoneField() {
		when(mockLanguageMessageService.getMessage("home-number-required", locale)).thenReturn("home-message");

		Map<String, String> results = preferredContactCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(ContactMethod.HOME_PHONE.getKey(), locale);

		assertThat(results, IsMapContaining.hasEntry("homePhoneNumber", "home-message"));
	}

	@Test
	public void getDependentRequiredFieldNamesAndMessage_WhenSelectedValueIsMobilePhone_ThenReturnMapWithMobilePhoneField() {
		when(mockLanguageMessageService.getMessage("mobile-number-required", locale)).thenReturn("mobile-message");

		Map<String, String> results = preferredContactCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(ContactMethod.MOBILE_PHONE.getKey(), locale);

		assertThat(results, IsMapContaining.hasEntry("mobilePhoneNumber", "mobile-message"));
	}

	@Test
	public void getDisplayOrder_WhenNoErrors_ThenDisplayOrderIsReturned() {

		int result = preferredContactCreateUserAccountField.getDisplayOrder();

		assertEquals(result, 300);
	}

	@Test
	public void getRequiredMessage_WhenNoErrors_ThenRequiredMessageIsReturned() {

		when(mockLanguageMessageService.getMessage("preferred-contact-method-is-required", locale)).thenReturn("is-required-message");

		String result = preferredContactCreateUserAccountField.getRequiredMessage(locale);

		assertEquals(result, "is-required-message");
	}

	@Test
	public void isEnabled_WhenNoErrorsAndConfigurationIsFalse_ThenFalseIsReturned() throws ConfigurationException {
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenReturn(mockPreferredContactMethodCompanyConfiguration);
		when(mockPreferredContactMethodCompanyConfiguration.preferredMethodOfContactEnabled()).thenReturn(false);

		boolean result = preferredContactCreateUserAccountField.isEnabled(COMPANY_ID);

		assertEquals(false, result);
	}

	@Test
	public void isEnabled_WhenNoErrorsAndConfigurationIsTrue_ThenTrueIsReturned() throws ConfigurationException {
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenReturn(mockPreferredContactMethodCompanyConfiguration);
		when(mockPreferredContactMethodCompanyConfiguration.preferredMethodOfContactEnabled()).thenReturn(true);

		boolean result = preferredContactCreateUserAccountField.isEnabled(COMPANY_ID);

		assertEquals(true, result);
	}

	@Test
	public void isEnabled_WhenThereAreErrors_ThenReturnsFalse() throws ConfigurationException {
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		preferredContactCreateUserAccountField.isEnabled(COMPANY_ID);
	}

	@Test
	public void isRequired_WhenNoErrorsAndConfigurationIsFalse_ThenFalseIsReturned() throws ConfigurationException {
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenReturn(mockPreferredContactMethodCompanyConfiguration);
		when(mockPreferredContactMethodCompanyConfiguration.preferredMethodOfContactRequired()).thenReturn(false);

		boolean result = preferredContactCreateUserAccountField.isRequired(COMPANY_ID);

		assertEquals(false, result);
	}

	@Test
	public void isRequired_WhenNoErrorsAndConfigurationIsTrue_ThenTrueIsReturned() throws ConfigurationException {
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenReturn(mockPreferredContactMethodCompanyConfiguration);
		when(mockPreferredContactMethodCompanyConfiguration.preferredMethodOfContactRequired()).thenReturn(true);

		boolean result = preferredContactCreateUserAccountField.isRequired(COMPANY_ID);

		assertEquals(true, result);
	}

	@Test
	public void isRequired_WhenThereAreErrors_ThenReturnsFalse() throws ConfigurationException {
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		preferredContactCreateUserAccountField.isRequired(COMPANY_ID);
	}

	@Test
	public void render_WhenNoErrors_ThenJSPIsRendered() throws PortalException, IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		preferredContactCreateUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenNoErrors_ThenRequestAttributesAreSet() throws Exception {
		Map<String, String> availableContactMethods = new HashMap<>();
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockLanguageMessageService.getMessage("field-optional", locale)).thenReturn("optional-message");
		when(mockPreferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale)).thenReturn(availableContactMethods);
		when(mockLanguageMessageService.getMessage("preferred-method-contact", locale)).thenReturn("preferred-message");
		when(mockPreferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID)).thenReturn(mockPreferredContactMethodCompanyConfiguration);
		when(mockPreferredContactMethodCompanyConfiguration.preferredMethodOfContactRequired()).thenReturn(true);
		when(mockLanguageMessageService.getMessage("field-required", locale)).thenReturn("required-message");

		preferredContactCreateUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("optionalMessage", "optional-message");
		verify(mockHttpServletRequest, times(1)).setAttribute(same("preferredContactMethods"), same(availableContactMethods));
		verify(mockHttpServletRequest, times(1)).setAttribute("preferredContactMethodsFieldName", "preferred-method-of-contact");
		verify(mockHttpServletRequest, times(1)).setAttribute("preferredContactMethodsTitle", "preferred-message");
		verify(mockHttpServletRequest, times(1)).setAttribute("required", true);
		verify(mockHttpServletRequest, times(1)).setAttribute("requiredMessage", "required-message");

	}

	@Test(expected = PortalException.class)
	public void render_WhenErrorsRednering_ThenExceptionIsThrown() throws PortalException, IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		doThrow(new IOException()).when(mockJspRenderer).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");

		preferredContactCreateUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

}
