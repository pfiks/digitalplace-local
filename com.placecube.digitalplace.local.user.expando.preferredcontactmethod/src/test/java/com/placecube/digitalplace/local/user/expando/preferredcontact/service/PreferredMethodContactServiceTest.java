package com.placecube.digitalplace.local.user.expando.preferredcontact.service;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Locale;
import java.util.Map;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.user.expando.preferredcontact.configuration.PreferredContactMethodCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.ContactMethod;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsCompanyConfiguration;

public class PreferredMethodContactServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	private static final String LABEL_KEY = "label-key";

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	private Locale locale = Locale.ENGLISH;

	@Mock
	private LanguageMessageService mockLanguageMessageService;

	@Mock
	private PreferredContactMethodCompanyConfiguration mockPreferredContactMethodCompanyConfiguration;

	@Mock
	private UserProfileFieldsCompanyConfiguration mockUserProfileFieldsCompanyConfiguration;

	@InjectMocks
	private PreferredMethodContactService preferredMethodContactService;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getAvailableContactMethods_WhenBusinessPhoneEnabled_ThenIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.BUSINESS_PHONE.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.businessPhoneNumberEnabled()).thenReturn(true);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, IsMapContaining.hasEntry(ContactMethod.BUSINESS_PHONE.getKey(), LABEL_KEY));

	}

	@Test
	public void getAvailableContactMethods_WhenBusinessPhoneDisabled_ThenNotIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.BUSINESS_PHONE.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.businessPhoneNumberEnabled()).thenReturn(false);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, not(IsMapContaining.hasEntry(ContactMethod.BUSINESS_PHONE.getKey(), LABEL_KEY)));

	}

	@Test
	public void getAvailableContactMethods_WhenEmailEnabled_ThenIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.EMAIL.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, IsMapContaining.hasEntry(ContactMethod.EMAIL.getKey(), LABEL_KEY));

	}

	@Test
	public void getAvailableContactMethods_WhenHomePhoneEnabled_ThenIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.HOME_PHONE.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.homePhoneNumberEnabled()).thenReturn(true);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, IsMapContaining.hasEntry(ContactMethod.HOME_PHONE.getKey(), LABEL_KEY));

	}

	@Test
	public void getAvailableContactMethods_WhenHomePhoneDisabled_ThenNotIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.HOME_PHONE.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.homePhoneNumberEnabled()).thenReturn(false);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, not(IsMapContaining.hasEntry(ContactMethod.HOME_PHONE.getKey(), LABEL_KEY)));

	}

	@Test
	public void getAvailableContactMethods_WhenMobilePhoneEnabled_ThenIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.MOBILE_PHONE.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, IsMapContaining.hasEntry(ContactMethod.MOBILE_PHONE.getKey(), LABEL_KEY));

	}

	@Test
	public void getAvailableContactMethods_WhenMobilePhoneDisabled_ThenNotIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(ContactMethod.MOBILE_PHONE.getLabel(), locale)).thenReturn(LABEL_KEY);
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);
		when(mockUserProfileFieldsCompanyConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);

		Map<String, String> contactMethods = preferredMethodContactService.getAvailableContactMethods(COMPANY_ID, locale);

		assertThat(contactMethods, not(IsMapContaining.hasEntry(ContactMethod.MOBILE_PHONE.getKey(), LABEL_KEY)));

	}

	@Test(expected = ConfigurationException.class)
	public void getPreferredContactMethodCompanyConfiguration_WhenErrors_ThenExceptionIsThrown() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(PreferredContactMethodCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		preferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID);
	}

	@Test
	public void getPreferredContactMethodCompanyConfiguration_WhenNoErrors_ThenConfigurationIsReturned() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(PreferredContactMethodCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockPreferredContactMethodCompanyConfiguration);

		PreferredContactMethodCompanyConfiguration result = preferredMethodContactService.getPreferredContactMethodCompanyConfiguration(COMPANY_ID);

		assertThat(result, sameInstance(mockPreferredContactMethodCompanyConfiguration));
	}

	@Test(expected = ConfigurationException.class)
	public void getUserProfileFieldsCompanyConfiguration_WhenErrors_ThenExceptionIsThrown() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		preferredMethodContactService.getUserProfileFieldsCompanyConfiguration(COMPANY_ID);
	}

	@Test
	public void getUserProfileFieldsCompanyConfiguration_WhenNoErrors_ThenConfigurationIsReturned() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserProfileFieldsCompanyConfiguration);

		UserProfileFieldsCompanyConfiguration result = preferredMethodContactService.getUserProfileFieldsCompanyConfiguration(COMPANY_ID);

		assertThat(result, sameInstance(mockUserProfileFieldsCompanyConfiguration));
	}

}
