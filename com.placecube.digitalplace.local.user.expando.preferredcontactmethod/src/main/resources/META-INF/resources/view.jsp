<%@ include file="init.jsp"%>

<gds-forms-ui:input-radio 
	portletNamespace="${ portletNamespace }" 
	fieldName="${ preferredContactMethodsFieldName }"
	fieldLabel="${ preferredContactMethodsTitle }"
	fieldValue="${ accountContext.getExpandoValue(preferredContactMethodsFieldName) }"
	fieldOptions="${ preferredContactMethods }"
	errorMessage="${ accountContext.getFieldError(preferredContactMethodsFieldName) }" 
	required="${ required }"
/>