package com.placecube.digitalplace.local.user.expando.preferredcontact.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.user.expando.preferredcontact.configuration.PreferredContactMethodCompanyConfiguration", localization = "content/Language", name = "user-profile-preferred-contact-method")
public interface PreferredContactMethodCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "preferred-method-of-contact-enabled")
	boolean preferredMethodOfContactEnabled();

	@Meta.AD(required = false, deflt = "false", name = "preferred-method-of-contact-required")
	boolean preferredMethodOfContactRequired();

}
