package com.placecube.digitalplace.local.user.expando.preferredcontact.constants.expando;

public final class UserExpandoPreferredContactMethod {

	public static final String FIELD_NAME = "preferred-method-of-contact";

	private UserExpandoPreferredContactMethod() {

	}

}
