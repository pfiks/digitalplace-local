package com.placecube.digitalplace.local.user.expando.preferredcontact.field;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.user.expando.preferredcontact.configuration.PreferredContactMethodCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.ContactMethod;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.PreferredMethodConstants;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.expando.UserExpandoPreferredContactMethod;
import com.placecube.digitalplace.local.user.expando.preferredcontact.service.LanguageMessageService;
import com.placecube.digitalplace.local.user.expando.preferredcontact.service.PreferredMethodContactService;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;

@Component(immediate = true, service = UserAccountField.class)
public class PreferredContactUserAccountField implements UserAccountField {

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private LanguageMessageService languageMessageService;

	@Reference
	private PreferredMethodContactService preferredMethodContactService;

	@Reference(target = "(osgi.web.symbolicname=" + PreferredMethodConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public void configureContextValue(AccountContext accountContext, PortletRequest portletRequest) {
		accountContext.addExpandoField(getExpandoFieldName(), ParamUtil.getString(portletRequest, getExpandoFieldName()));
	}

	@Override
	public AccountCreationStep getAccountCreationStep() {
		return AccountCreationStep.ADDITIONAL_DETAILS;
	}

	@Override
	public String getBundleId() {
		return PreferredMethodConstants.BUNDLE_ID;
	}

	@Override
	public Map<String, String> getDependentRequiredFieldNamesAndMessage(String selectedValue, Locale locale) {
		Map<String, String> dependentRequiredFieldNames = new HashMap<>();

		if (ContactMethod.BUSINESS_PHONE.getKey().equals(selectedValue)) {
			dependentRequiredFieldNames.put("businessPhoneNumber", languageMessageService.getMessage("business-number-required", locale));
		} else if (ContactMethod.HOME_PHONE.getKey().equals(selectedValue)) {
			dependentRequiredFieldNames.put("homePhoneNumber", languageMessageService.getMessage("home-number-required", locale));
		} else if (ContactMethod.MOBILE_PHONE.getKey().equals(selectedValue)) {
			dependentRequiredFieldNames.put("mobilePhoneNumber", languageMessageService.getMessage("mobile-number-required", locale));
		} else if (ContactMethod.EMAIL.getKey().equals(selectedValue)) {
			dependentRequiredFieldNames.put("emailAddress", languageMessageService.getMessage("email-address-required", locale));
		}

		return dependentRequiredFieldNames;
	}

	@Override
	public int getDisplayOrder() {
		return 300;
	}

	@Override
	public String getExpandoFieldName() {
		return UserExpandoPreferredContactMethod.FIELD_NAME;
	}

	@Override
	public String getRequiredMessage(Locale locale) {
		return languageMessageService.getMessage("preferred-contact-method-is-required", locale);
	}

	@Override
	public boolean isEnabled(long companyId) {
		try {
			PreferredContactMethodCompanyConfiguration configuration = preferredMethodContactService.getPreferredContactMethodCompanyConfiguration(companyId);
			return configuration.preferredMethodOfContactEnabled();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean isRequired(long companyId) {
		try {
			PreferredContactMethodCompanyConfiguration configuration = preferredMethodContactService.getPreferredContactMethodCompanyConfiguration(companyId);
			return configuration.preferredMethodOfContactRequired();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void render(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws PortalException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long companyId = themeDisplay.getCompanyId();
			Locale locale = themeDisplay.getLocale();
			httpServletRequest.setAttribute("optionalMessage", languageMessageService.getMessage("field-optional", locale));

			httpServletRequest.setAttribute("preferredContactMethods", preferredMethodContactService.getAvailableContactMethods(companyId, locale));
			httpServletRequest.setAttribute("preferredContactMethodsFieldName", getExpandoFieldName());
			httpServletRequest.setAttribute("preferredContactMethodsTitle", languageMessageService.getMessage("preferred-method-contact", locale));
			httpServletRequest.setAttribute("required", isRequired(companyId));
			httpServletRequest.setAttribute("requiredMessage", languageMessageService.getMessage("field-required", locale));
			jspRenderer.renderJSP(servletContext, httpServletRequest, httpServletResponse, "/view.jsp");
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

}
