package com.placecube.digitalplace.local.user.expando.preferredcontact.constants;

public enum ContactMethod {

	BUSINESS_PHONE("business-phone", "preferred-method-business-phone"),
	EMAIL("email", "preferred-method-email"),
	HOME_PHONE("home-phone", "preferred-method-home-phone"),
	MOBILE_PHONE("mobile-phone", "preferred-method-mobile"),
	NONE("none", "preferred-method-none");

	private String key;
	private String label;

	private ContactMethod(String key, String label) {
		this.label = label;
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public String getLabel() {
		return label;
	}

}
