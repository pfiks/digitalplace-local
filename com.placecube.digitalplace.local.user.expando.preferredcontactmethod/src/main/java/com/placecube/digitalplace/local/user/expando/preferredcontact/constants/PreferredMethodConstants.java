package com.placecube.digitalplace.local.user.expando.preferredcontact.constants;


public final class PreferredMethodConstants {
	
	public static final String BUNDLE_ID = "com.placecube.digitalplace.local.user.expando.preferredcontactmethod";
	
	private PreferredMethodConstants() {
		
	}
}
