package com.placecube.digitalplace.local.user.expando.preferredcontact.service;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.user.expando.preferredcontact.configuration.PreferredContactMethodCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.preferredcontact.constants.ContactMethod;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsCompanyConfiguration;

@Component(immediate = true, service = PreferredMethodContactService.class)
public class PreferredMethodContactService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LanguageMessageService languageMessageService;

	public Map<String, String> getAvailableContactMethods(long companyId, Locale locale) throws Exception {
		UserProfileFieldsCompanyConfiguration configuration = getUserProfileFieldsCompanyConfiguration(companyId);
		Map<String, String> contactMethods = new LinkedHashMap<>();

		if (configuration.homePhoneNumberEnabled()) {
			contactMethods.put(ContactMethod.HOME_PHONE.getKey(), languageMessageService.getMessage(ContactMethod.HOME_PHONE.getLabel(), locale));
		}

		if (configuration.mobilePhoneNumberEnabled()) {
			contactMethods.put(ContactMethod.MOBILE_PHONE.getKey(), languageMessageService.getMessage(ContactMethod.MOBILE_PHONE.getLabel(), locale));
		}

		if (configuration.businessPhoneNumberEnabled()) {
			contactMethods.put(ContactMethod.BUSINESS_PHONE.getKey(), languageMessageService.getMessage(ContactMethod.BUSINESS_PHONE.getLabel(), locale));
		}

		contactMethods.put(ContactMethod.EMAIL.getKey(), languageMessageService.getMessage(ContactMethod.EMAIL.getLabel(), locale));

		contactMethods.put(ContactMethod.NONE.getKey(), languageMessageService.getMessage(ContactMethod.NONE.getLabel(), locale));

		return contactMethods;

	}

	public UserProfileFieldsCompanyConfiguration getUserProfileFieldsCompanyConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, companyId);
	}

	public PreferredContactMethodCompanyConfiguration getPreferredContactMethodCompanyConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(PreferredContactMethodCompanyConfiguration.class, companyId);
	}
}
