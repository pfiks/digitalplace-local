$(document).ready(function () {
	initGDSThemeContributor();
});

Liferay.on('endNavigate', function() {
	initGDSThemeContributor();
	focusErrors();
});

Liferay.on('allPortletsReady', function() {
	focusErrors();
})

function focusErrors() {
	var errorsSummary = $(".govuk-error-summary").first();
	
	if (errorsSummary.length > 0) {
		errorsSummary.focus();
	}
}

function initGDSThemeContributor() {
	document.body.className = ((document.body.className) ? document.body.className + ' js-enabled' : 'js-enabled');
	window.GOVUKFrontend.initAll();
}