package com.placecube.digitalplace.local.account.closeyouraccount.internal.util;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountWebContentConstants;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = CloseYourAccountContentUtil.class)
public class CloseYourAccountContentUtil {

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	public String getBody(JournalArticle journalArticle, Locale locale) throws PortletException {
		Optional<String> body = journalArticleRetrievalService.getFieldValue(journalArticle, CloseYourAccountWebContentConstants.CLOSURE_OF_ACCOUNT_ARTICLE_BODY_FIELD, locale);
		if (body.isPresent()) {
			return body.get();
		} else {
			throw new PortletException("Email body field is not present in journalArticleId " + journalArticle.getArticleId());
		}
	}

	public JournalArticle getOrCreateJournalArticleEmail(ServiceContext serviceContext) throws PortalException, IOException {
		JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(groupLocalService.getCompanyGroup(serviceContext.getCompanyId()).getGroupId(), CloseYourAccountWebContentConstants.CLOSURE_OF_ACCOUNT_CONFIRMATION_TEMPLATE_ARTICLE_ID, WorkflowConstants.STATUS_APPROVED);

		if (Validator.isNull(journalArticle)) {
			JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder(CloseYourAccountWebContentConstants.TEMPLATES_FOLDER, serviceContext);
			DDMStructure emailStructure = emailWebContentService.getOrCreateDDMStructure(serviceContext);

			String articleContent = StringUtil.read(getClass().getClassLoader(),
					"com/placecube/digitalplace/local/account/closeyouraccount/dependencies/webcontent/" + CloseYourAccountWebContentConstants.CLOSURE_OF_ACCOUNT_CONFIRMATION_TEMPLATE_FILE_NAME);

			JournalArticleContext context = JournalArticleContext.init(CloseYourAccountWebContentConstants.CLOSURE_OF_ACCOUNT_CONFIRMATION_TEMPLATE_ARTICLE_ID,
					CloseYourAccountWebContentConstants.CLOSURE_OF_ACCOUNT_CONFIRMATION_ARTICLE_TITLE, articleContent);
			context.setJournalFolder(journalFolder);
			context.setDDMStructure(emailStructure);
			context.setIndexable(false);

			journalArticle = journalArticleCreationService.getOrCreateArticle(context, serviceContext);
		}

		return journalArticle;
	}

	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(false);

		return serviceContext;
	}

	public String getSubject(JournalArticle journalArticle, Locale locale) throws PortalException {
		Optional<String> subject = journalArticleRetrievalService.getFieldValue(journalArticle, CloseYourAccountWebContentConstants.CLOSURE_OF_ACCOUNT_ARTICLE_SUBJECT_FIELD, locale);
		if (subject.isPresent()) {
			return subject.get();
		} else {
			throw new PortalException("Email subject field is not present in journalArticleId " + journalArticle.getArticleId());
		}
	}

	public String replaceEmailValues(String section, User user) {
		return StringUtil.replace(section,
				new String[] { CloseYourAccountWebContentConstants.EMAIL_PLACEHOLDER_FIRST_NAME, CloseYourAccountWebContentConstants.EMAIL_PLACEHOLDER_LAST_NAME },
				new String[] { user.getFirstName(), user.getLastName() });
	}

}
