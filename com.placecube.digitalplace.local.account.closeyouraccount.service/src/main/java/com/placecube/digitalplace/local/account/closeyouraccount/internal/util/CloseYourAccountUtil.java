package com.placecube.digitalplace.local.account.closeyouraccount.internal.util;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountFormConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountStatusConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;
import com.placecube.digitalplace.local.account.closeyouraccount.service.ClosureOfAccountLocalService;

@Component(immediate = true, service = CloseYourAccountUtil.class)
public class CloseYourAccountUtil {

	private static final Log LOG = LogFactoryUtil.getLog(CloseYourAccountUtil.class);

	@Reference
	private CloseYourAccountContentUtil closeYourAccountContentUtil;

	@Reference
	private ClosureOfAccountLocalService closureOfAccountLocalService;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private MailService mailService;

	@Reference
	private UserLocalService userLocalService;

	public void processClosureDateAccounts(long companyId) throws PortalException {
		List<ClosureOfAccount> closureOfAccountsByClosureDate = closureOfAccountLocalService.findByClosureDateAndCompanyId(companyId);

		ServiceContext serviceContext = new ServiceContext();
		long groupId = 0;
		String emailSubject = StringPool.BLANK;
		String emailBody = StringPool.BLANK;

		serviceContext.setCompanyId(companyId);
		serviceContext.setUserId(userLocalService.getGuestUserId(companyId));

		for (ClosureOfAccount closureOfAccount : closureOfAccountsByClosureDate) {
			try {
				User user = userLocalService.getUser(closureOfAccount.getUserId());

				if (user.getStatus() == WorkflowConstants.STATUS_APPROVED) {
					userLocalService.updateStatus(user.getUserId(), WorkflowConstants.STATUS_INACTIVE, new ServiceContext());

					closureOfAccount.setModifiedDate(new Date());
					closureOfAccount.setStatus(CloseYourAccountStatusConstants.DEACTIVATED);
					closureOfAccountLocalService.updateClosureOfAccount(closureOfAccount);

					if (LOG.isDebugEnabled()) {
						LOG.debug(String.format("Deactivated account with userId %d", user.getUserId()));
					}

					DDMFormInstanceRecord formRecord = ddmFormInstanceRecordLocalService.getDDMFormInstanceRecord(closureOfAccount.getFormInstanceRecordId());
					DDMFormValues ddmFormValues = formRecord.getDDMFormValues();
					Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = ddmFormValues.getDDMFormFieldValuesMap();
					Locale defaultLocale = ddmFormValues.getDefaultLocale();

					serviceContext.setUuid(PortalUUIDUtil.generate());

					if (groupId != closureOfAccount.getGroupId()) {
						groupId = closureOfAccount.getGroupId();

						serviceContext.setScopeGroupId(groupId);

						JournalArticle article = closeYourAccountContentUtil.getOrCreateJournalArticleEmail(serviceContext);
						emailSubject = closeYourAccountContentUtil.getSubject(article, defaultLocale);
						emailBody = closeYourAccountContentUtil.getBody(article, defaultLocale);
					}

					String toEmailAddress = CloseYourAccountFormUtil.getFieldValue(ddmFormFieldValuesMap, CloseYourAccountFormConstants.EMAIL_ADDRESS_FIELD_NAME, defaultLocale);
					emailSubject = closeYourAccountContentUtil.replaceEmailValues(emailSubject, user);
					emailBody = closeYourAccountContentUtil.replaceEmailValues(emailBody, user);

					sendConfirmationEmail(emailSubject, emailBody, toEmailAddress);

					if (LOG.isDebugEnabled()) {
						LOG.debug(String.format("Sent email confirmation for account with userId %d", user.getUserId()));
					}
				}

			} catch (NoSuchUserException nsue) {
				LOG.error(nsue.getMessage());
			} catch (Exception e) {
				LOG.error(e);
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug(String.format("Processed %d account closures that reached the closure date", closureOfAccountsByClosureDate.size()));
		}
	}

	public void processFormInstanceRecord(DDMFormInstanceRecord model) throws PortalException {
		DDMFormValues ddmFormValues = model.getDDMFormValues();
		Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = ddmFormValues.getDDMFormFieldValuesMap();
		Locale defaultLocale = ddmFormValues.getDefaultLocale();

		try {
			Date closureDate = CloseYourAccountFormUtil.getClosureDate(ddmFormFieldValuesMap, defaultLocale);

			closureOfAccountLocalService.addClosureOfAccount(model.getGroupId(), model.getCompanyId(), model.getUserId(), model.getFormInstanceRecordId(), setCurrentTime(closureDate));

		} catch (Exception e) {
			LOG.error("Error processing form instance record with formInstanceRecordId: " + model.getFormInstanceRecordId());
			throw new PortalException(e);
		}
	}

	private void sendConfirmationEmail(String subject, String body, String toAddress) {
		try {
			mailService.sendEmail(toAddress, toAddress, subject, body);
		} catch (Exception e) {
			LOG.error("Unable to send closure of account confirmation email", e);
		}
	}

	private Date setCurrentTime(Date date) {
		Calendar dateTime = Calendar.getInstance();
		dateTime.setLenient(false);
		dateTime.setTime(date);

		Calendar now = Calendar.getInstance();
		dateTime.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
		dateTime.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
		dateTime.set(Calendar.SECOND, now.get(Calendar.SECOND));
		dateTime.set(Calendar.MILLISECOND, now.get(Calendar.MILLISECOND));

		return dateTime.getTime();
	}
}