package com.placecube.digitalplace.local.account.closeyouraccount.internal.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.util.CloseYourAccountContentUtil;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class CloseYourAccountEmailWebContentLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(CloseYourAccountEmailWebContentLifecycleListener.class);

	@Reference
	private CloseYourAccountContentUtil closeYourAccountContentUtil;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {

		long companyId = company.getCompanyId();
		LOG.debug("Creating Close your account resources for companyId: " + companyId);

		ServiceContext serviceContext = closeYourAccountContentUtil.getServiceContext(company.getGroup());

		closeYourAccountContentUtil.getOrCreateJournalArticleEmail(serviceContext);

		LOG.debug("Created Close your account resources for companyId: " + companyId);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Waits until the portal has finished initializing. No code needed.
	}

}