/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.local.account.closeyouraccount.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountStatusConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;
import com.placecube.digitalplace.local.account.closeyouraccount.service.base.ClosureOfAccountLocalServiceBaseImpl;

/**
 * The implementation of the closure of account local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.local.account.closeyouraccount.service.ClosureOfAccountLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClosureOfAccountLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount", service = AopService.class)
public class ClosureOfAccountLocalServiceImpl extends ClosureOfAccountLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.local.account.closeyouraccount.service.
	 * ClosureOfAccountLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.local.account.closeyouraccount.service.
	 * ClosureOfAccountLocalServiceUtil</code>.
	 */

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public ClosureOfAccount addClosureOfAccount(long groupId, long companyId, long userId, long formInstanceRecordId, Date closureDate) throws PortalException {

		ClosureOfAccount closureOfAccount = closureOfAccountPersistence.create(counterLocalService.increment(ClosureOfAccount.class.getName()));

		User user = userLocalService.getUser(userId);

		closureOfAccount.setGroupId(groupId);
		closureOfAccount.setCompanyId(companyId);
		closureOfAccount.setUserId(userId);
		closureOfAccount.setUserName(user.getFullName());

		Date now = new Date();
		closureOfAccount.setCreateDate(now);
		closureOfAccount.setModifiedDate(now);

		closureOfAccount.setFormInstanceRecordId(formInstanceRecordId);
		closureOfAccount.setClosureDate(closureDate);
		closureOfAccount.setStatus(CloseYourAccountStatusConstants.CREATED);

		return closureOfAccountPersistence.update(closureOfAccount);
	}

	@Override
	public List<ClosureOfAccount> fetchClosureOfAccountsByCompanyId(long companyId) {
		return closureOfAccountPersistence.findByCompanyId(companyId);
	}

	@Override
	public List<ClosureOfAccount> findByClosureDateAndCompanyId(long companyId) {
		DynamicQuery dynamicQuery = closureOfAccountLocalService.dynamicQuery();

		Criterion criterionStatusAndClosureDate = RestrictionsFactoryUtil.and(
				RestrictionsFactoryUtil.eq("status", CloseYourAccountStatusConstants.CREATED),
				RestrictionsFactoryUtil.le("closureDate", new Date()));

		dynamicQuery.add(RestrictionsFactoryUtil.and(criterionStatusAndClosureDate, RestrictionsFactoryUtil.eq("companyId", companyId)));

		return closureOfAccountLocalService.dynamicQuery(dynamicQuery);
	}

}