package com.placecube.digitalplace.local.account.closeyouraccount.internal.constants;

public final class CloseYourAccountStatusConstants {

	public static final int CREATED = 0;

	public static final int DEACTIVATED = 1;

	private CloseYourAccountStatusConstants() {
	}

}
