package com.placecube.digitalplace.local.account.closeyouraccount.internal.upgrade.upgrade_1_0_1;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;


public class CloseYourAccountIndexUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		
		if (hasIndex("Placecube_Account_Closure", "IX_E8F7088E")) {
			runSQLTemplateString("drop index IX_E8F7088E on Placecube_Account_Closure;", false);
		}
		if (hasIndex("Placecube_Account_Closure", "IX_1A245A90")) {
			runSQLTemplateString("drop index IX_1A245A90 on Placecube_Account_Closure;", false);
		}

		if (!hasIndex("Placecube_Account_Closure", "IX_7AA056CE")) {
			runSQLTemplateString("create index IX_7AA056CE on Placecube_Account_Closure_ClosureOfAccount (companyId);", false);
		}
		if (!hasIndex("Placecube_Account_Closure", "IX_95B9BF5A")) {
			runSQLTemplateString("create index IX_95B9BF5A on Placecube_Account_Closure_ClosureOfAccount (uuid_[$COLUMN_LENGTH:75$]);", false);
		}
	}

}
