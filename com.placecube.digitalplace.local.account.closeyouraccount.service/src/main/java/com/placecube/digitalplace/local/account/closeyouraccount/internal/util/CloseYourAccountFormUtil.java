package com.placecube.digitalplace.local.account.closeyouraccount.internal.util;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountFormConstants;

public class CloseYourAccountFormUtil {

	private CloseYourAccountFormUtil() {

	}

	public static String formatSelectField(String fieldValue) {
		return Validator.isNull(fieldValue) ? fieldValue
				: fieldValue.replace(StringPool.OPEN_BRACKET, StringPool.BLANK).replace(StringPool.CLOSE_BRACKET, StringPool.BLANK).replace(StringPool.QUOTE, StringPool.BLANK);
	}

	public static Date getClosureDate(Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, Locale defaultLocale) throws ParseException {
		String closureDate = getFieldValue(ddmFormFieldValuesMap, CloseYourAccountFormConstants.CLOSURE_DATE_FIELD_NAME, defaultLocale);
		return DateFormatFactoryUtil.getSimpleDateFormat(CloseYourAccountFormConstants.FORM_DATE_FORMAT).parse(closureDate);
	}

	public static String getFieldValue(Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, String fieldName, Locale defaultLocale) {
		return ddmFormFieldValuesMap.get(fieldName).get(0).getValue().getString(defaultLocale);
	}

}
