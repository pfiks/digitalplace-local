package com.placecube.digitalplace.local.account.closeyouraccount.internal.constants;

public final class CloseYourAccountFormConstants {

	public static final String CLOSE_YOUR_ACCOUNT_STRUCTURE_KEY = "CLOSE-YOUR-ACCOUNT";

	public static final String CLOSURE_DATE_FIELD_NAME = "ClosureDate";

	public static final String EMAIL_ADDRESS_FIELD_NAME = "EmailAddress";

	public static final String FORM_DATE_FORMAT = "yyyy-MM-dd";

	private CloseYourAccountFormConstants() {

	}

}
