package com.placecube.digitalplace.local.account.closeyouraccount.internal.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.upgrade.upgrade_1_0_1.CloseYourAccountIndexUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class CloseYourAccountUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new CloseYourAccountIndexUpdate());
	}

}
