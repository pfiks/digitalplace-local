package com.placecube.digitalplace.local.account.closeyouraccount.internal.constants;

public final class CloseYourAccountWebContentConstants {

	public static final String CLOSURE_OF_ACCOUNT_ARTICLE_BODY_FIELD = "Body";
	public static final String CLOSURE_OF_ACCOUNT_ARTICLE_SUBJECT_FIELD = "Subject";
	public static final String CLOSURE_OF_ACCOUNT_CONFIRMATION_ARTICLE_TITLE = "Closure of Account Confirmation Email";
	public static final String CLOSURE_OF_ACCOUNT_CONFIRMATION_TEMPLATE_ARTICLE_ID = "CLOSURE-OF-ACCOUNT-CONFIRMATION-EMAIL";
	public static final String CLOSURE_OF_ACCOUNT_CONFIRMATION_TEMPLATE_FILE_NAME = "Closure of Account Confirmation Email.xml";
	public static final String EMAIL_PLACEHOLDER_FIRST_NAME = "$FirstName$";
	public static final String EMAIL_PLACEHOLDER_LAST_NAME = "$LastName$";
	public static final String TEMPLATES_FOLDER = "Templates";

	private CloseYourAccountWebContentConstants() {

	}
}
