package com.placecube.digitalplace.local.account.closeyouraccount.internal.scheduler;

import java.util.Date;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeRunnable;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.scheduler.SchedulerJobConfiguration;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.placecube.digitalplace.local.account.closeyouraccount.configuration.CloseYourAccountConfiguration;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.util.CloseYourAccountUtil;

@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE, configurationPid = "com.placecube.digitalplace.local.account.closeyouraccount.configuration.CloseYourAccountConfiguration", service = SchedulerJobConfiguration.class)
public class CloseYourAccountSchedulerJobConfiguration implements SchedulerJobConfiguration {

	private static final Log log = LogFactoryUtil.getLog(CloseYourAccountSchedulerJobConfiguration.class);

	@Reference
	private CloseYourAccountUtil closeYourAccountUtil;

	@Reference
	private CompanyLocalService companyLocalService;

	private CloseYourAccountConfiguration closeYourAccountConfiguration;

	@Activate
	protected void activate(Map<String, Object> properties) {
		closeYourAccountConfiguration = ConfigurableUtil.createConfigurable(CloseYourAccountConfiguration.class, properties);
	}

	@Override
	public UnsafeConsumer<Long, Exception> getCompanyJobExecutorUnsafeConsumer() {
		return this::runJobForCompany;
	}

	@Override
	public UnsafeRunnable<Exception> getJobExecutorUnsafeRunnable() {
		return () -> companyLocalService.forEachCompanyId(this::runJobForCompany);
	}

	@Override
	public TriggerConfiguration getTriggerConfiguration() {
		String cronExpression = closeYourAccountConfiguration.schedulerCronExpression();

		TriggerConfiguration triggerConfiguration = TriggerConfiguration.createTriggerConfiguration(cronExpression);
		triggerConfiguration.setStartDate(new Date());

		return triggerConfiguration;
	}

	private void runJobForCompany(long companyId) throws PortalException {
		CompanyThreadLocal.setCompanyId(companyId);
		if (closeYourAccountConfiguration.schedulerEnabled()) {
			if (log.isDebugEnabled()) {
				log.debug("Running scheduled closure of account task for company with companyId: " + companyId);
			}

			closeYourAccountUtil.processClosureDateAccounts(companyId);

			if (log.isDebugEnabled()) {
				log.debug("Finished scheduled closure of account task for company with companyId: " + companyId);
			}
		}

	}

}
