package com.placecube.digitalplace.local.account.closeyouraccount.internal.model.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountFormConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.util.CloseYourAccountUtil;

@Component(immediate = true, service = ModelListener.class)
public class CloseYourAccountDDMFormInstanceModelListener extends BaseModelListener<DDMFormInstanceRecord> {

	private static final Log LOG = LogFactoryUtil.getLog(CloseYourAccountDDMFormInstanceModelListener.class);

	@Reference
	private CloseYourAccountUtil closeYourAccountUtil;

	@Override
	public void onAfterUpdate(DDMFormInstanceRecord originalModel, DDMFormInstanceRecord model) throws ModelListenerException {

		try {
			if (model.getFormInstance().getStructure().getStructureKey().equals(CloseYourAccountFormConstants.CLOSE_YOUR_ACCOUNT_STRUCTURE_KEY)) {
				closeYourAccountUtil.processFormInstanceRecord(model);
			}
		} catch (PortalException e) {
			LOG.error(e);
		}
	}

}
