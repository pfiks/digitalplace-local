/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.closeyouraccount.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ClosureOfAccount in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ClosureOfAccountCacheModel
	implements CacheModel<ClosureOfAccount>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ClosureOfAccountCacheModel)) {
			return false;
		}

		ClosureOfAccountCacheModel closureOfAccountCacheModel =
			(ClosureOfAccountCacheModel)object;

		if (closureOfAccountId ==
				closureOfAccountCacheModel.closureOfAccountId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, closureOfAccountId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", closureOfAccountId=");
		sb.append(closureOfAccountId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", formInstanceRecordId=");
		sb.append(formInstanceRecordId);
		sb.append(", closureDate=");
		sb.append(closureDate);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ClosureOfAccount toEntityModel() {
		ClosureOfAccountImpl closureOfAccountImpl = new ClosureOfAccountImpl();

		if (uuid == null) {
			closureOfAccountImpl.setUuid("");
		}
		else {
			closureOfAccountImpl.setUuid(uuid);
		}

		closureOfAccountImpl.setClosureOfAccountId(closureOfAccountId);
		closureOfAccountImpl.setGroupId(groupId);
		closureOfAccountImpl.setCompanyId(companyId);
		closureOfAccountImpl.setUserId(userId);

		if (userName == null) {
			closureOfAccountImpl.setUserName("");
		}
		else {
			closureOfAccountImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			closureOfAccountImpl.setCreateDate(null);
		}
		else {
			closureOfAccountImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			closureOfAccountImpl.setModifiedDate(null);
		}
		else {
			closureOfAccountImpl.setModifiedDate(new Date(modifiedDate));
		}

		closureOfAccountImpl.setFormInstanceRecordId(formInstanceRecordId);

		if (closureDate == Long.MIN_VALUE) {
			closureOfAccountImpl.setClosureDate(null);
		}
		else {
			closureOfAccountImpl.setClosureDate(new Date(closureDate));
		}

		closureOfAccountImpl.setStatus(status);

		closureOfAccountImpl.resetOriginalValues();

		return closureOfAccountImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		closureOfAccountId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		formInstanceRecordId = objectInput.readLong();
		closureDate = objectInput.readLong();

		status = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(closureOfAccountId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(formInstanceRecordId);
		objectOutput.writeLong(closureDate);

		objectOutput.writeInt(status);
	}

	public String uuid;
	public long closureOfAccountId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long formInstanceRecordId;
	public long closureDate;
	public int status;

}