create index IX_7AA056CE on Placecube_Account_Closure_ClosureOfAccount (companyId);
create index IX_C9ACCE33 on Placecube_Account_Closure_ClosureOfAccount (formInstanceRecordId);
create index IX_95B9BF5A on Placecube_Account_Closure_ClosureOfAccount (uuid_[$COLUMN_LENGTH:75$]);