create table Placecube_Account_Closure_ClosureOfAccount (
	uuid_ VARCHAR(75) null,
	closureOfAccountId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	formInstanceRecordId LONG,
	closureDate DATE null,
	status INTEGER
);