package com.placecube.digitalplace.local.account.closeyouraccount.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountStatusConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;
import com.placecube.digitalplace.local.account.closeyouraccount.service.ClosureOfAccountLocalService;
import com.placecube.digitalplace.local.account.closeyouraccount.service.persistence.ClosureOfAccountPersistence;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class })
public class ClosureOfAccountLocalServiceImplTest extends PowerMockito {

	private static final long CLOSURE_OF_ACCOUNT_ID = 6;

	private static final long COMPANY_ID = 2;

	private static final long FORM_INSTANCE_RECORD_ID = 3;

	private static final long GROUP_ID = 1;

	private static final long USER_ID = 4;

	private static final String USERNAME = "username";

	@InjectMocks
	private ClosureOfAccountLocalServiceImpl closureOfAccountLocalServiceImpl;

	@Mock
	private Criterion mockAndCriterion;

	@Mock
	private Criterion mockAndCriterion2;

	@Mock
	private ClosureOfAccount mockClosureOfAccount;

	@Mock
	private ClosureOfAccountLocalService mockClosureOfAccountLocalService;

	@Mock
	private ClosureOfAccountPersistence mockClosureOfAccountPersistence;

	@Mock
	private Criterion mockCompanyIdCriterion;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Criterion mockDateCriterion;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Criterion mockStatusCriterion;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Test
	public void addClosureOfAccount_WhenNoError_ThenClosureOfAccountIsCreated() throws PortalException {

		when(mockCounterLocalService.increment(ClosureOfAccount.class.getName())).thenReturn(CLOSURE_OF_ACCOUNT_ID);
		when(mockClosureOfAccountPersistence.create(CLOSURE_OF_ACCOUNT_ID)).thenReturn(mockClosureOfAccount);

		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USERNAME);

		closureOfAccountLocalServiceImpl.addClosureOfAccount(GROUP_ID, COMPANY_ID, USER_ID, FORM_INSTANCE_RECORD_ID, new Date());

		InOrder inOrder = inOrder(mockClosureOfAccount, mockClosureOfAccountPersistence);
		inOrder.verify(mockClosureOfAccount, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockClosureOfAccount, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockClosureOfAccount, times(1)).setUserId(USER_ID);
		inOrder.verify(mockClosureOfAccount, times(1)).setUserName(USERNAME);
		inOrder.verify(mockClosureOfAccount, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockClosureOfAccount, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockClosureOfAccount, times(1)).setFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);
		inOrder.verify(mockClosureOfAccount, times(1)).setClosureDate(any(Date.class));
		inOrder.verify(mockClosureOfAccount, times(1)).setStatus(CloseYourAccountStatusConstants.CREATED);
		inOrder.verify(mockClosureOfAccountPersistence, times(1)).update(mockClosureOfAccount);
	}

	@Test
	public void findByClosureDate_WhenNoError_ThenListIsReturned() {
		when(mockClosureOfAccountLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);

		when(RestrictionsFactoryUtil.eq("status", CloseYourAccountStatusConstants.CREATED)).thenReturn(mockStatusCriterion);
		when(RestrictionsFactoryUtil.le(eq("closureDate"), any(Date.class))).thenReturn(mockDateCriterion);
		when(RestrictionsFactoryUtil.and(mockStatusCriterion, mockDateCriterion)).thenReturn(mockAndCriterion);
		when(RestrictionsFactoryUtil.eq("companyId", COMPANY_ID)).thenReturn(mockCompanyIdCriterion);
		when(RestrictionsFactoryUtil.and(mockAndCriterion, mockCompanyIdCriterion)).thenReturn(mockAndCriterion2);

		List<Object> expected = Collections.singletonList(mockClosureOfAccount);
		when(mockClosureOfAccountLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);

		List<ClosureOfAccount> results = closureOfAccountLocalServiceImpl.findByClosureDateAndCompanyId(COMPANY_ID);

		assertThat(results, sameInstance(expected));
		InOrder inOrder = Mockito.inOrder(mockDynamicQuery, mockClosureOfAccountLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockAndCriterion2);
		inOrder.verify(mockClosureOfAccountLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

	@Before
	public void setUp() {
		mockStatic(RestrictionsFactoryUtil.class);
	}

}