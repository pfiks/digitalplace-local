package com.placecube.digitalplace.local.account.closeyouraccount.internal.scheduler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.local.account.closeyouraccount.configuration.CloseYourAccountConfiguration;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.util.CloseYourAccountUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ConfigurableUtil.class, TriggerConfiguration.class, CompanyThreadLocal.class })
public class CloseYourAccountSchedulerJobConfigurationTest extends PowerMockito {

	private static final String CRON_EXPRESSION = "0 3 0 * * ?";

	private static final long COMPANY_ID = 1L;

	@InjectMocks
	private CloseYourAccountSchedulerJobConfiguration closeYourAccountSchedulerJobConfiguration;

	@Mock
	private CloseYourAccountConfiguration mockCloseYourAccountConfiguration;

	@Mock
	private CloseYourAccountUtil mockCloseYourAccountUtil;

	@Mock
	private Map<String, Object> mockProperties;

	@Mock
	private TriggerConfiguration mockTriggerConfiguration;

	@Test
	public void activate_WhenNoError_ActivateScheduler() {
		closeYourAccountSchedulerJobConfiguration.activate(mockProperties);

		verifyStatic(ConfigurableUtil.class, times(1));
		ConfigurableUtil.createConfigurable(CloseYourAccountConfiguration.class, mockProperties);
	}

	@Before
	public void activateSetUp() {
		mockStatic(ConfigurableUtil.class, TriggerConfiguration.class, CompanyThreadLocal.class);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenNoError_ThenRecordsAreProcessed() throws Exception {
		when(mockCloseYourAccountConfiguration.schedulerEnabled()).thenReturn(true);

		UnsafeConsumer<Long, Exception> companyJobExecutorUnsafeConsumer = closeYourAccountSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		companyJobExecutorUnsafeConsumer.accept(COMPANY_ID);

		InOrder inOrder = Mockito.inOrder(mockCloseYourAccountUtil);
		inOrder.verify(mockCloseYourAccountUtil, times(1)).processClosureDateAccounts(COMPANY_ID);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenSchedulerDisabled_ThenDoNothing() throws Exception {
		when(mockCloseYourAccountConfiguration.schedulerEnabled()).thenReturn(false);

		UnsafeConsumer<Long, Exception> companyJobExecutorUnsafeConsumer = closeYourAccountSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		companyJobExecutorUnsafeConsumer.accept(COMPANY_ID);

		verifyNoInteractions(mockCloseYourAccountUtil);
		verifyStatic(CompanyThreadLocal.class, times(1));
		CompanyThreadLocal.setCompanyId(COMPANY_ID);
	}

	@Test
	public void getTriggerConfiguration_WhenNoError_ThenReturnTriggerConfiguration() {
		when(mockCloseYourAccountConfiguration.schedulerCronExpression()).thenReturn(CRON_EXPRESSION);
		when(TriggerConfiguration.createTriggerConfiguration(CRON_EXPRESSION)).thenReturn(mockTriggerConfiguration);

		TriggerConfiguration result = closeYourAccountSchedulerJobConfiguration.getTriggerConfiguration();

		assertThat(result, equalTo(mockTriggerConfiguration));
	}

}
