package com.placecube.digitalplace.local.account.closeyouraccount.internal.model.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountFormConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.util.CloseYourAccountUtil;

@RunWith(PowerMockRunner.class)
public class CloseYourAccountDDMFormInstanceModelListenerTest extends PowerMockito {

	@InjectMocks
	private CloseYourAccountDDMFormInstanceModelListener closeYourAccountDDMFormInstanceModelListener;

	@Mock
	private CloseYourAccountUtil mockCloseYourAccountUtil;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMFormInstanceRecord mockOriginalDDMFormInstanceRecord;

	@Test
	public void onAfterUpdate_WhenNoError_ThenRecordIsProcessed() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureKey()).thenReturn(CloseYourAccountFormConstants.CLOSE_YOUR_ACCOUNT_STRUCTURE_KEY);

		closeYourAccountDDMFormInstanceModelListener.onAfterUpdate(mockOriginalDDMFormInstanceRecord, mockDDMFormInstanceRecord);

		verify(mockCloseYourAccountUtil, times(1)).processFormInstanceRecord(mockDDMFormInstanceRecord);
	}

}
