package com.placecube.digitalplace.local.account.closeyouraccount.internal.util;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountFormConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.internal.constants.CloseYourAccountStatusConstants;
import com.placecube.digitalplace.local.account.closeyouraccount.model.ClosureOfAccount;
import com.placecube.digitalplace.local.account.closeyouraccount.service.ClosureOfAccountLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUUIDUtil.class, CloseYourAccountFormUtil.class })
public class CloseYourAccountUtilTest extends PowerMockito {

	private static final long COMPANY_ID = 3;

	private static final long DEFAULT_USER_ID = 5;

	private static final String EMAIL_ADDRESS = "a@a.com";

	private static final String EMAIL_BODY = "body";

	private static final String EMAIL_REPLACED_BODY = "replacedBody";

	private static final String EMAIL_REPLACED_SUBJECT = "replacedSubject";

	private static final String EMAIL_SUBJECT = "subject";

	private static final long FORM_INSTANCE_RECORD_ID = 2;

	private static final long GROUP_ID = 4;

	private static final long USER_ID = 1;

	private static final String UUID = "uuid";

	@InjectMocks
	private CloseYourAccountUtil closeYourAccountUtil;

	@Mock
	private CloseYourAccountContentUtil mockCloseYourAccountContentUtil;

	@Mock
	private ClosureOfAccount mockClosureOfAccount;

	@Mock
	private ClosureOfAccountLocalService mockClosureOfAccountLocalService;

	@Mock
	private Map<String, List<DDMFormFieldValue>> mockDDMFormFieldValuesMap;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private Locale mockLocale;

	@Mock
	private MailService mockMailService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(PortalUUIDUtil.class, CloseYourAccountFormUtil.class);
	}

	@Test
	public void processClosureDateAccounts_WhenThereAreActiveUsersOnClosureDate_ThenDeactivateUsersAndSendEmail() throws Exception {
		List<ClosureOfAccount> results = Collections.singletonList(mockClosureOfAccount);
		when(mockClosureOfAccountLocalService.findByClosureDateAndCompanyId(COMPANY_ID)).thenReturn(results);
		when(mockClosureOfAccount.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);

		when(mockClosureOfAccount.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);
		when(mockDDMFormInstanceRecordLocalService.getDDMFormInstanceRecord(FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(mockDDMFormFieldValuesMap);
		when(mockDDMFormValues.getDefaultLocale()).thenReturn(mockLocale);

		when(PortalUUIDUtil.generate()).thenReturn(UUID);

		when(mockClosureOfAccount.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockClosureOfAccount.getGroupId()).thenReturn(GROUP_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(DEFAULT_USER_ID);

		when(mockCloseYourAccountContentUtil.getOrCreateJournalArticleEmail(any(ServiceContext.class))).thenReturn(mockJournalArticle);
		when(mockCloseYourAccountContentUtil.getSubject(mockJournalArticle, mockLocale)).thenReturn(EMAIL_SUBJECT);
		when(mockCloseYourAccountContentUtil.getBody(mockJournalArticle, mockLocale)).thenReturn(EMAIL_BODY);

		when(CloseYourAccountFormUtil.getFieldValue(mockDDMFormFieldValuesMap, CloseYourAccountFormConstants.EMAIL_ADDRESS_FIELD_NAME, mockLocale)).thenReturn(EMAIL_ADDRESS);
		when(mockCloseYourAccountContentUtil.replaceEmailValues(EMAIL_SUBJECT, mockUser)).thenReturn(EMAIL_REPLACED_SUBJECT);
		when(mockCloseYourAccountContentUtil.replaceEmailValues(EMAIL_BODY, mockUser)).thenReturn(EMAIL_REPLACED_BODY);

		closeYourAccountUtil.processClosureDateAccounts(COMPANY_ID);

		InOrder inOrder = Mockito.inOrder(mockUserLocalService, mockClosureOfAccount, mockClosureOfAccountLocalService, mockMailService);
		inOrder.verify(mockUserLocalService, times(1)).updateStatus(eq(USER_ID), eq(WorkflowConstants.STATUS_INACTIVE), any(ServiceContext.class));
		inOrder.verify(mockClosureOfAccount, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockClosureOfAccount, times(1)).setStatus(CloseYourAccountStatusConstants.DEACTIVATED);
		inOrder.verify(mockClosureOfAccountLocalService, times(1)).updateClosureOfAccount(mockClosureOfAccount);
		inOrder.verify(mockMailService, times(1)).sendEmail(EMAIL_ADDRESS, EMAIL_ADDRESS, EMAIL_REPLACED_SUBJECT, EMAIL_REPLACED_BODY);
	}

	@Test
	public void processClosureDateAccounts_WhenThereAreDeactivatedUsersOnClosureDate_ThenDoNotUpdateUsers() throws PortalException {
		List<ClosureOfAccount> results = Collections.singletonList(mockClosureOfAccount);
		when(mockClosureOfAccountLocalService.findByClosureDateAndCompanyId(COMPANY_ID)).thenReturn(results);
		when(mockClosureOfAccount.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getStatus()).thenReturn(WorkflowConstants.STATUS_INACTIVE);

		closeYourAccountUtil.processClosureDateAccounts(COMPANY_ID);

		verify(mockUserLocalService, never()).updateStatus(eq(USER_ID), eq(WorkflowConstants.STATUS_INACTIVE), any(ServiceContext.class));
		verify(mockClosureOfAccountLocalService, never()).updateClosureOfAccount(mockClosureOfAccount);
	}

	@Test
	public void processClosureDateAccounts_WhenThereAreNoUsersOnClosureDate_ThenDoNotUpdateUsers() throws PortalException {
		when(mockClosureOfAccountLocalService.findByClosureDateAndCompanyId(COMPANY_ID)).thenReturn(new ArrayList<>());

		closeYourAccountUtil.processClosureDateAccounts(COMPANY_ID);

		verify(mockClosureOfAccountLocalService, times(1)).findByClosureDateAndCompanyId(COMPANY_ID);
		verify(mockUserLocalService, times(1)).getGuestUserId(COMPANY_ID);
		verifyNoMoreInteractions(mockClosureOfAccountLocalService, mockUserLocalService);
	}

}
