package com.placecube.digitalplace.local.account.relationship.internal.search.spi.model.index.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.placecube.digitalplace.local.account.relationship.internal.search.spi.model.index.contributor.RelationshipModelIndexWriterContributor;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery", "com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery" })
public class RelationshipModelIndexWriterContributorTest {

	@Mock
	private BatchIndexingActionable mockBatchIndexingActionable;

	@Mock
	private DynamicQueryBatchIndexingActionableFactory mockDynamicQueryBatchIndexingActionableFactory;

	@Mock
	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Mock
	private Relationship mockRelationship;

	@Mock
	private RelationshipLocalService mockRelationshipLocalService;

	@InjectMocks
	private RelationshipModelIndexWriterContributor relationshipModelIndexWriterContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getBatchIndexingActionable_WhenNoError_ThenReturnsTheBatchIndexingActionable() {
		when(mockRelationshipLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);
		when(mockDynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(mockIndexableActionableDynamicQuery)).thenReturn(mockBatchIndexingActionable);

		BatchIndexingActionable result = relationshipModelIndexWriterContributor.getBatchIndexingActionable();

		assertThat(result, sameInstance(mockBatchIndexingActionable));
	}

	@Test
	public void getCompanyId_WhenNoError_ThenReturnsTheCompanyId() {
		long expected = 1;
		when(mockRelationship.getCompanyId()).thenReturn(expected);

		long result = relationshipModelIndexWriterContributor.getCompanyId(mockRelationship);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getIndexerWriterMode_WhenRelationshipIsFound_ThenReturnsUpdate() {
		long relationshipId = 1;
		when(mockRelationship.getRelationshipId()).thenReturn(relationshipId);
		when(mockRelationshipLocalService.fetchRelationship(relationshipId)).thenReturn(mockRelationship);

		IndexerWriterMode result = relationshipModelIndexWriterContributor.getIndexerWriterMode(mockRelationship);

		assertThat(result, equalTo(IndexerWriterMode.UPDATE));
	}

	@Test
	public void getIndexerWriterMode_WhenRelationshipIsNotFound_ThenReturnsDelete() {

		long relationshipId = 1;
		when(mockRelationship.getRelationshipId()).thenReturn(relationshipId);
		when(mockRelationshipLocalService.fetchRelationship(relationshipId)).thenReturn(null);

		IndexerWriterMode result = relationshipModelIndexWriterContributor.getIndexerWriterMode(mockRelationship);

		assertThat(result, equalTo(IndexerWriterMode.DELETE));
	}

}
