package com.placecube.digitalplace.local.account.relationship.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.persistence.RelationshipPersistence;

@RunWith(PowerMockRunner.class)
public class RelationshipLocalServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 2;
	private static final long REL_USER_ID = 2;
	private static final long RELATIONSHIP_ID = 1;
	private static final String TYPE = "type";
	private static final long USER_ID = 1;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Relationship mockRelationship;

	@Mock
	private RelationshipPersistence mockRelationshipPersistence;

	@InjectMocks
	private RelationshipLocalServiceImpl relationshipLocalServiceImpl;

	@Test
	public void addRelationship_WhenRelationshipDoesNotExist_ThenRelationshipIsCreated() throws PortalException {

		when(mockRelationshipPersistence.fetchByUserIdAndRelUserId(USER_ID, REL_USER_ID)).thenReturn(null);
		when(mockCounterLocalService.increment(Relationship.class.getName(), 1)).thenReturn(RELATIONSHIP_ID);
		when(mockRelationshipPersistence.create(RELATIONSHIP_ID)).thenReturn(mockRelationship);

		relationshipLocalServiceImpl.addRelationship(USER_ID, REL_USER_ID, COMPANY_ID, TYPE);

		InOrder inOrder = inOrder(mockRelationship, mockRelationshipPersistence);
		inOrder.verify(mockRelationship, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockRelationship, times(1)).setUserId(USER_ID);
		inOrder.verify(mockRelationship, times(1)).setRelUserId(REL_USER_ID);
		inOrder.verify(mockRelationship, times(1)).setType(TYPE);
		inOrder.verify(mockRelationship, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockRelationshipPersistence, times(1)).update(mockRelationship);
	}

	@Test
	public void addRelationship_WhenRelationshipExists_ThenExistingRelationshipIsReturned() throws PortalException {

		when(mockRelationshipPersistence.fetchByUserIdAndRelUserId(USER_ID, REL_USER_ID)).thenReturn(mockRelationship);

		Relationship result = relationshipLocalServiceImpl.addRelationship(USER_ID, REL_USER_ID, COMPANY_ID, TYPE);

		verify(mockRelationship, never()).setCompanyId(COMPANY_ID);
		verify(mockRelationship, never()).setUserId(USER_ID);
		verify(mockRelationship, never()).setRelUserId(REL_USER_ID);
		verify(mockRelationship, never()).setType(TYPE);
		verify(mockRelationship, never()).setCreateDate(any(Date.class));
		verify(mockRelationshipPersistence, never()).update(mockRelationship);

		assertThat(result, sameInstance(mockRelationship));
	}
}
