package com.placecube.digitalplace.local.account.relationship.internal.search.spi.model.index.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;

public class RelationshipModelDocumentContributorTest extends PowerMockito {

	@Mock
	private Document mockDocument;

	@Mock
	private Relationship mockRelationship;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private RelationshipModelDocumentContributor relationshipModelDocumentContributor;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenRelationshipFieldsAreAddedAsKeyword() {

		long userId = 1;
		when(mockRelationship.getUserId()).thenReturn(userId);
		long relUserId = 2;
		when(mockRelationship.getRelUserId()).thenReturn(relUserId);
		when(mockUserLocalService.fetchUser(relUserId)).thenReturn(mockUser);
		String relUserFullName = "relUserFullName";
		when(mockUser.getFullName()).thenReturn(relUserFullName);
		String type = "type";
		when(mockRelationship.getType()).thenReturn(type);
		Date now = new Date();
		when(mockRelationship.getCreateDate()).thenReturn(now);
		long companyId = 3;
		when(mockRelationship.getCompanyId()).thenReturn(companyId);

		relationshipModelDocumentContributor.contribute(mockDocument, mockRelationship);

		InOrder inOrder = inOrder(mockDocument, mockRelationship);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.USER_ID, userId);
		inOrder.verify(mockDocument, times(1)).addKeyword("relUserId", relUserId);
		inOrder.verify(mockDocument, times(1)).addKeyword("relUserFullName", relUserFullName);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.TYPE, type);
		inOrder.verify(mockDocument, times(1)).addDate(Field.CREATE_DATE, now);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.COMPANY_ID, companyId);

	}

	@Test
	public void contribute_WhenRelUserIsNull_ThenRelUserFullNameFieldIsNotAddedAsKeyword() {

		long userId = 1;
		when(mockRelationship.getUserId()).thenReturn(userId);
		long relUserId = 2;
		when(mockRelationship.getRelUserId()).thenReturn(relUserId);
		when(mockUserLocalService.fetchUser(relUserId)).thenReturn(null);
		String relUserFullName = "relUserFullName";
		String type = "type";
		when(mockRelationship.getType()).thenReturn(type);
		Date now = new Date();
		when(mockRelationship.getCreateDate()).thenReturn(now);
		long companyId = 3;
		when(mockRelationship.getCompanyId()).thenReturn(companyId);

		relationshipModelDocumentContributor.contribute(mockDocument, mockRelationship);

		InOrder inOrder = inOrder(mockDocument, mockRelationship);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.USER_ID, userId);
		inOrder.verify(mockDocument, times(1)).addKeyword("relUserId", relUserId);
		inOrder.verify(mockDocument, never()).addKeyword("relUserFullName", relUserFullName);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.TYPE, type);
		inOrder.verify(mockDocument, times(1)).addDate(Field.CREATE_DATE, now);
		inOrder.verify(mockDocument, times(1)).addKeyword(Field.COMPANY_ID, companyId);

	}

}
