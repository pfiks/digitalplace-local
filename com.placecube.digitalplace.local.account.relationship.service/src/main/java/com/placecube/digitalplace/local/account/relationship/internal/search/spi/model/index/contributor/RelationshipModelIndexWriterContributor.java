package com.placecube.digitalplace.local.account.relationship.internal.search.spi.model.index.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.liferay.portal.search.spi.model.index.contributor.helper.ModelIndexerWriterDocumentHelper;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.local.account.relationship.model.Relationship", service = ModelIndexerWriterContributor.class)
public class RelationshipModelIndexWriterContributor implements ModelIndexerWriterContributor<Relationship> {

	@Reference
	protected DynamicQueryBatchIndexingActionableFactory dynamicQueryBatchIndexingActionableFactory;

	@Reference
	private RelationshipLocalService relationshipLocalService;

	@Override
	public void customize(BatchIndexingActionable batchIndexingActionable, ModelIndexerWriterDocumentHelper modelIndexerWriterDocumentHelper) {

		batchIndexingActionable.setPerformActionMethod((Relationship relationship) -> batchIndexingActionable.addDocuments(modelIndexerWriterDocumentHelper.getDocument(relationship)));

	}

	@Override
	public BatchIndexingActionable getBatchIndexingActionable() {
		return dynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(relationshipLocalService.getIndexableActionableDynamicQuery());
	}

	@Override
	public long getCompanyId(Relationship relationship) {
		return relationship.getCompanyId();
	}

	@Override
	public IndexerWriterMode getIndexerWriterMode(Relationship relationship) {
		if (Validator.isNull(relationshipLocalService.fetchRelationship(relationship.getRelationshipId()))) {
			return IndexerWriterMode.DELETE;
		}
		return IndexerWriterMode.UPDATE;
	}

}
