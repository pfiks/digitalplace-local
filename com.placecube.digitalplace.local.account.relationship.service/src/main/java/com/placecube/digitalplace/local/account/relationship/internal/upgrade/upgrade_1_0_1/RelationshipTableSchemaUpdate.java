package com.placecube.digitalplace.local.account.relationship.internal.upgrade.upgrade_1_0_1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

public class RelationshipTableSchemaUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(RelationshipTableSchemaUpdate.class);

	private RelationshipLocalService relationshipLocalService;

	public RelationshipTableSchemaUpdate(RelationshipLocalService relationshipLocalService) {
		this.relationshipLocalService = relationshipLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		LOG.info("Initialised upgrade process for Relationship records");

		PreparedStatement ps = connection
				.prepareStatement("select userId as userId, relUserId as relUserId, companyId as companyId, createDate as createDate, type_ as type from Placecube_Account_Relationships_Relationship");
		ResultSet rs = ps.executeQuery();
		JSONArray relationships = JSONFactoryUtil.createJSONArray();
		while (rs.next()) {
			JSONObject relationship = JSONFactoryUtil.createJSONObject();
			relationship.put("userId", rs.getLong(1));
			relationship.put("relUserId", rs.getLong(2));
			relationship.put("companyId", rs.getLong(3));
			relationship.put("createDate", rs.getDate(4));
			relationship.put("type", rs.getString(5));

			relationships.put(relationship);
		}

		runSQLTemplateString("drop table Placecube_Account_Relationships_Relationship;", false);
		runSQLTemplateString(StringUtil.read(RelationshipTableSchemaUpdate.class.getResourceAsStream("/META-INF/sql/tables.sql")), false);
		runSQLTemplateString("create index IX_2FE010D7 on Placecube_Account_Relationships_Relationship (userId, companyId);", false);
		runSQLTemplateString("create index IX_7DD5DCDE on Placecube_Account_Relationships_Relationship (userId, relUserId);", false);
		runSQLTemplateString("create index IX_9F18CBAB on Placecube_Account_Relationships_Relationship (uuid_[$COLUMN_LENGTH:75$], companyId);", false);

		for (Object object : relationships) {
			JSONObject jsonObject = (JSONObject) object;
			Relationship rel = relationshipLocalService.addRelationship(jsonObject.getLong("userId"), jsonObject.getLong("relUserId"), jsonObject.getLong("companyId"), jsonObject.getString("type"));
			Date createDate = new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.getString("createDate"));
			rel.setCreateDate(createDate);
			relationshipLocalService.updateRelationship(rel);
		}

		LOG.info("Completed upgrade process for Relationship records");

	}

}
