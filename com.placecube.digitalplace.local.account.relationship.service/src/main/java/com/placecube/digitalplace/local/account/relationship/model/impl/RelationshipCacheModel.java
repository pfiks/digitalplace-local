/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.local.account.relationship.model.Relationship;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Relationship in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RelationshipCacheModel
	implements CacheModel<Relationship>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof RelationshipCacheModel)) {
			return false;
		}

		RelationshipCacheModel relationshipCacheModel =
			(RelationshipCacheModel)object;

		if (relationshipId == relationshipCacheModel.relationshipId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, relationshipId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", relationshipId=");
		sb.append(relationshipId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", relUserId=");
		sb.append(relUserId);
		sb.append(", type=");
		sb.append(type);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Relationship toEntityModel() {
		RelationshipImpl relationshipImpl = new RelationshipImpl();

		if (uuid == null) {
			relationshipImpl.setUuid("");
		}
		else {
			relationshipImpl.setUuid(uuid);
		}

		relationshipImpl.setRelationshipId(relationshipId);
		relationshipImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			relationshipImpl.setCreateDate(null);
		}
		else {
			relationshipImpl.setCreateDate(new Date(createDate));
		}

		relationshipImpl.setUserId(userId);
		relationshipImpl.setRelUserId(relUserId);

		if (type == null) {
			relationshipImpl.setType("");
		}
		else {
			relationshipImpl.setType(type);
		}

		relationshipImpl.resetOriginalValues();

		return relationshipImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		relationshipId = objectInput.readLong();

		companyId = objectInput.readLong();
		createDate = objectInput.readLong();

		userId = objectInput.readLong();

		relUserId = objectInput.readLong();
		type = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(relationshipId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);

		objectOutput.writeLong(userId);

		objectOutput.writeLong(relUserId);

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}
	}

	public String uuid;
	public long relationshipId;
	public long companyId;
	public long createDate;
	public long userId;
	public long relUserId;
	public String type;

}