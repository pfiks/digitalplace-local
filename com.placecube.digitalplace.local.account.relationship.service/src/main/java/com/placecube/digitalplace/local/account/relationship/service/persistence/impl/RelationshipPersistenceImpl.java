/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.local.account.relationship.exception.NoSuchRelationshipException;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.model.RelationshipTable;
import com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipImpl;
import com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl;
import com.placecube.digitalplace.local.account.relationship.service.persistence.RelationshipPersistence;
import com.placecube.digitalplace.local.account.relationship.service.persistence.RelationshipUtil;
import com.placecube.digitalplace.local.account.relationship.service.persistence.impl.constants.Placecube_Account_RelationshipsPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the relationship service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = RelationshipPersistence.class)
public class RelationshipPersistenceImpl
	extends BasePersistenceImpl<Relationship>
	implements RelationshipPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>RelationshipUtil</code> to access the relationship persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		RelationshipImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the relationships where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching relationships
	 */
	@Override
	public List<Relationship> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	@Override
	public List<Relationship> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	@Override
	public List<Relationship> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	@Override
	public List<Relationship> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Relationship> list = null;

		if (useFinderCache) {
			list = (List<Relationship>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Relationship relationship : list) {
					if (!uuid.equals(relationship.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(RelationshipModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Relationship>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUuid_First(
			String uuid, OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUuid_First(uuid, orderByComparator);

		if (relationship != null) {
			return relationship;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchRelationshipException(sb.toString());
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUuid_First(
		String uuid, OrderByComparator<Relationship> orderByComparator) {

		List<Relationship> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUuid_Last(
			String uuid, OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUuid_Last(uuid, orderByComparator);

		if (relationship != null) {
			return relationship;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchRelationshipException(sb.toString());
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUuid_Last(
		String uuid, OrderByComparator<Relationship> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Relationship> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where uuid = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship[] findByUuid_PrevAndNext(
			long relationshipId, String uuid,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		uuid = Objects.toString(uuid, "");

		Relationship relationship = findByPrimaryKey(relationshipId);

		Session session = null;

		try {
			session = openSession();

			Relationship[] array = new RelationshipImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, relationship, uuid, orderByComparator, true);

			array[1] = relationship;

			array[2] = getByUuid_PrevAndNext(
				session, relationship, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Relationship getByUuid_PrevAndNext(
		Session session, Relationship relationship, String uuid,
		OrderByComparator<Relationship> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(RelationshipModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(relationship)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Relationship> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the relationships where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Relationship relationship :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(relationship);
		}
	}

	/**
	 * Returns the number of relationships where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching relationships
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_RELATIONSHIP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"relationship.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(relationship.uuid IS NULL OR relationship.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching relationships
	 */
	@Override
	public List<Relationship> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	@Override
	public List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	@Override
	public List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	@Override
	public List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Relationship> list = null;

		if (useFinderCache) {
			list = (List<Relationship>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Relationship relationship : list) {
					if (!uuid.equals(relationship.getUuid()) ||
						(companyId != relationship.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(RelationshipModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<Relationship>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (relationship != null) {
			return relationship;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchRelationshipException(sb.toString());
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		List<Relationship> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (relationship != null) {
			return relationship;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchRelationshipException(sb.toString());
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Relationship> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship[] findByUuid_C_PrevAndNext(
			long relationshipId, String uuid, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		uuid = Objects.toString(uuid, "");

		Relationship relationship = findByPrimaryKey(relationshipId);

		Session session = null;

		try {
			session = openSession();

			Relationship[] array = new RelationshipImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, relationship, uuid, companyId, orderByComparator,
				true);

			array[1] = relationship;

			array[2] = getByUuid_C_PrevAndNext(
				session, relationship, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Relationship getByUuid_C_PrevAndNext(
		Session session, Relationship relationship, String uuid, long companyId,
		OrderByComparator<Relationship> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(RelationshipModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(relationship)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Relationship> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the relationships where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Relationship relationship :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(relationship);
		}
	}

	/**
	 * Returns the number of relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching relationships
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_RELATIONSHIP_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"relationship.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(relationship.uuid IS NULL OR relationship.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"relationship.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByUserIdAndCompanyId;
	private FinderPath _finderPathWithoutPaginationFindByUserIdAndCompanyId;
	private FinderPath _finderPathCountByUserIdAndCompanyId;

	/**
	 * Returns all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @return the matching relationships
	 */
	@Override
	public List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId) {

		return findByUserIdAndCompanyId(
			userId, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	@Override
	public List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end) {

		return findByUserIdAndCompanyId(userId, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	@Override
	public List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return findByUserIdAndCompanyId(
			userId, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	@Override
	public List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByUserIdAndCompanyId;
				finderArgs = new Object[] {userId, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUserIdAndCompanyId;
			finderArgs = new Object[] {
				userId, companyId, start, end, orderByComparator
			};
		}

		List<Relationship> list = null;

		if (useFinderCache) {
			list = (List<Relationship>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Relationship relationship : list) {
					if ((userId != relationship.getUserId()) ||
						(companyId != relationship.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

			sb.append(_FINDER_COLUMN_USERIDANDCOMPANYID_USERID_2);

			sb.append(_FINDER_COLUMN_USERIDANDCOMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(RelationshipModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				queryPos.add(companyId);

				list = (List<Relationship>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUserIdAndCompanyId_First(
			long userId, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUserIdAndCompanyId_First(
			userId, companyId, orderByComparator);

		if (relationship != null) {
			return relationship;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchRelationshipException(sb.toString());
	}

	/**
	 * Returns the first relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUserIdAndCompanyId_First(
		long userId, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		List<Relationship> list = findByUserIdAndCompanyId(
			userId, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUserIdAndCompanyId_Last(
			long userId, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUserIdAndCompanyId_Last(
			userId, companyId, orderByComparator);

		if (relationship != null) {
			return relationship;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchRelationshipException(sb.toString());
	}

	/**
	 * Returns the last relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUserIdAndCompanyId_Last(
		long userId, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		int count = countByUserIdAndCompanyId(userId, companyId);

		if (count == 0) {
			return null;
		}

		List<Relationship> list = findByUserIdAndCompanyId(
			userId, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship[] findByUserIdAndCompanyId_PrevAndNext(
			long relationshipId, long userId, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws NoSuchRelationshipException {

		Relationship relationship = findByPrimaryKey(relationshipId);

		Session session = null;

		try {
			session = openSession();

			Relationship[] array = new RelationshipImpl[3];

			array[0] = getByUserIdAndCompanyId_PrevAndNext(
				session, relationship, userId, companyId, orderByComparator,
				true);

			array[1] = relationship;

			array[2] = getByUserIdAndCompanyId_PrevAndNext(
				session, relationship, userId, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Relationship getByUserIdAndCompanyId_PrevAndNext(
		Session session, Relationship relationship, long userId, long companyId,
		OrderByComparator<Relationship> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

		sb.append(_FINDER_COLUMN_USERIDANDCOMPANYID_USERID_2);

		sb.append(_FINDER_COLUMN_USERIDANDCOMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(RelationshipModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(userId);

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(relationship)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Relationship> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the relationships where userId = &#63; and companyId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUserIdAndCompanyId(long userId, long companyId) {
		for (Relationship relationship :
				findByUserIdAndCompanyId(
					userId, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(relationship);
		}
	}

	/**
	 * Returns the number of relationships where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @return the number of matching relationships
	 */
	@Override
	public int countByUserIdAndCompanyId(long userId, long companyId) {
		FinderPath finderPath = _finderPathCountByUserIdAndCompanyId;

		Object[] finderArgs = new Object[] {userId, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_RELATIONSHIP_WHERE);

			sb.append(_FINDER_COLUMN_USERIDANDCOMPANYID_USERID_2);

			sb.append(_FINDER_COLUMN_USERIDANDCOMPANYID_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERIDANDCOMPANYID_USERID_2 =
		"relationship.userId = ? AND ";

	private static final String _FINDER_COLUMN_USERIDANDCOMPANYID_COMPANYID_2 =
		"relationship.companyId = ?";

	private FinderPath _finderPathFetchByUserIdAndRelUserId;
	private FinderPath _finderPathCountByUserIdAndRelUserId;

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or throws a <code>NoSuchRelationshipException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	@Override
	public Relationship findByUserIdAndRelUserId(long userId, long relUserId)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByUserIdAndRelUserId(
			userId, relUserId);

		if (relationship == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("userId=");
			sb.append(userId);

			sb.append(", relUserId=");
			sb.append(relUserId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchRelationshipException(sb.toString());
		}

		return relationship;
	}

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUserIdAndRelUserId(long userId, long relUserId) {
		return fetchByUserIdAndRelUserId(userId, relUserId, true);
	}

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public Relationship fetchByUserIdAndRelUserId(
		long userId, long relUserId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {userId, relUserId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUserIdAndRelUserId, finderArgs, this);
		}

		if (result instanceof Relationship) {
			Relationship relationship = (Relationship)result;

			if ((userId != relationship.getUserId()) ||
				(relUserId != relationship.getRelUserId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_RELATIONSHIP_WHERE);

			sb.append(_FINDER_COLUMN_USERIDANDRELUSERID_USERID_2);

			sb.append(_FINDER_COLUMN_USERIDANDRELUSERID_RELUSERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				queryPos.add(relUserId);

				List<Relationship> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUserIdAndRelUserId, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {userId, relUserId};
							}

							_log.warn(
								"RelationshipPersistenceImpl.fetchByUserIdAndRelUserId(long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Relationship relationship = list.get(0);

					result = relationship;

					cacheResult(relationship);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Relationship)result;
		}
	}

	/**
	 * Removes the relationship where userId = &#63; and relUserId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the relationship that was removed
	 */
	@Override
	public Relationship removeByUserIdAndRelUserId(long userId, long relUserId)
		throws NoSuchRelationshipException {

		Relationship relationship = findByUserIdAndRelUserId(userId, relUserId);

		return remove(relationship);
	}

	/**
	 * Returns the number of relationships where userId = &#63; and relUserId = &#63;.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the number of matching relationships
	 */
	@Override
	public int countByUserIdAndRelUserId(long userId, long relUserId) {
		FinderPath finderPath = _finderPathCountByUserIdAndRelUserId;

		Object[] finderArgs = new Object[] {userId, relUserId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_RELATIONSHIP_WHERE);

			sb.append(_FINDER_COLUMN_USERIDANDRELUSERID_USERID_2);

			sb.append(_FINDER_COLUMN_USERIDANDRELUSERID_RELUSERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				queryPos.add(relUserId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERIDANDRELUSERID_USERID_2 =
		"relationship.userId = ? AND ";

	private static final String _FINDER_COLUMN_USERIDANDRELUSERID_RELUSERID_2 =
		"relationship.relUserId = ?";

	public RelationshipPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("type", "type_");

		setDBColumnNames(dbColumnNames);

		setModelClass(Relationship.class);

		setModelImplClass(RelationshipImpl.class);
		setModelPKClass(long.class);

		setTable(RelationshipTable.INSTANCE);
	}

	/**
	 * Caches the relationship in the entity cache if it is enabled.
	 *
	 * @param relationship the relationship
	 */
	@Override
	public void cacheResult(Relationship relationship) {
		entityCache.putResult(
			RelationshipImpl.class, relationship.getPrimaryKey(), relationship);

		finderCache.putResult(
			_finderPathFetchByUserIdAndRelUserId,
			new Object[] {
				relationship.getUserId(), relationship.getRelUserId()
			},
			relationship);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the relationships in the entity cache if it is enabled.
	 *
	 * @param relationships the relationships
	 */
	@Override
	public void cacheResult(List<Relationship> relationships) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (relationships.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (Relationship relationship : relationships) {
			if (entityCache.getResult(
					RelationshipImpl.class, relationship.getPrimaryKey()) ==
						null) {

				cacheResult(relationship);
			}
		}
	}

	/**
	 * Clears the cache for all relationships.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(RelationshipImpl.class);

		finderCache.clearCache(RelationshipImpl.class);
	}

	/**
	 * Clears the cache for the relationship.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Relationship relationship) {
		entityCache.removeResult(RelationshipImpl.class, relationship);
	}

	@Override
	public void clearCache(List<Relationship> relationships) {
		for (Relationship relationship : relationships) {
			entityCache.removeResult(RelationshipImpl.class, relationship);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(RelationshipImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(RelationshipImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		RelationshipModelImpl relationshipModelImpl) {

		Object[] args = new Object[] {
			relationshipModelImpl.getUserId(),
			relationshipModelImpl.getRelUserId()
		};

		finderCache.putResult(
			_finderPathCountByUserIdAndRelUserId, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUserIdAndRelUserId, args, relationshipModelImpl);
	}

	/**
	 * Creates a new relationship with the primary key. Does not add the relationship to the database.
	 *
	 * @param relationshipId the primary key for the new relationship
	 * @return the new relationship
	 */
	@Override
	public Relationship create(long relationshipId) {
		Relationship relationship = new RelationshipImpl();

		relationship.setNew(true);
		relationship.setPrimaryKey(relationshipId);

		String uuid = PortalUUIDUtil.generate();

		relationship.setUuid(uuid);

		relationship.setCompanyId(CompanyThreadLocal.getCompanyId());

		return relationship;
	}

	/**
	 * Removes the relationship with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship that was removed
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship remove(long relationshipId)
		throws NoSuchRelationshipException {

		return remove((Serializable)relationshipId);
	}

	/**
	 * Removes the relationship with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the relationship
	 * @return the relationship that was removed
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship remove(Serializable primaryKey)
		throws NoSuchRelationshipException {

		Session session = null;

		try {
			session = openSession();

			Relationship relationship = (Relationship)session.get(
				RelationshipImpl.class, primaryKey);

			if (relationship == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchRelationshipException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(relationship);
		}
		catch (NoSuchRelationshipException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Relationship removeImpl(Relationship relationship) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(relationship)) {
				relationship = (Relationship)session.get(
					RelationshipImpl.class, relationship.getPrimaryKeyObj());
			}

			if (relationship != null) {
				session.delete(relationship);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (relationship != null) {
			clearCache(relationship);
		}

		return relationship;
	}

	@Override
	public Relationship updateImpl(Relationship relationship) {
		boolean isNew = relationship.isNew();

		if (!(relationship instanceof RelationshipModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(relationship.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					relationship);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in relationship proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Relationship implementation " +
					relationship.getClass());
		}

		RelationshipModelImpl relationshipModelImpl =
			(RelationshipModelImpl)relationship;

		if (Validator.isNull(relationship.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			relationship.setUuid(uuid);
		}

		if (isNew && (relationship.getCreateDate() == null)) {
			ServiceContext serviceContext =
				ServiceContextThreadLocal.getServiceContext();

			Date date = new Date();

			if (serviceContext == null) {
				relationship.setCreateDate(date);
			}
			else {
				relationship.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(relationship);
			}
			else {
				relationship = (Relationship)session.merge(relationship);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			RelationshipImpl.class, relationshipModelImpl, false, true);

		cacheUniqueFindersCache(relationshipModelImpl);

		if (isNew) {
			relationship.setNew(false);
		}

		relationship.resetOriginalValues();

		return relationship;
	}

	/**
	 * Returns the relationship with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the relationship
	 * @return the relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship findByPrimaryKey(Serializable primaryKey)
		throws NoSuchRelationshipException {

		Relationship relationship = fetchByPrimaryKey(primaryKey);

		if (relationship == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchRelationshipException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return relationship;
	}

	/**
	 * Returns the relationship with the primary key or throws a <code>NoSuchRelationshipException</code> if it could not be found.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship findByPrimaryKey(long relationshipId)
		throws NoSuchRelationshipException {

		return findByPrimaryKey((Serializable)relationshipId);
	}

	/**
	 * Returns the relationship with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship, or <code>null</code> if a relationship with the primary key could not be found
	 */
	@Override
	public Relationship fetchByPrimaryKey(long relationshipId) {
		return fetchByPrimaryKey((Serializable)relationshipId);
	}

	/**
	 * Returns all the relationships.
	 *
	 * @return the relationships
	 */
	@Override
	public List<Relationship> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of relationships
	 */
	@Override
	public List<Relationship> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of relationships
	 */
	@Override
	public List<Relationship> findAll(
		int start, int end, OrderByComparator<Relationship> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of relationships
	 */
	@Override
	public List<Relationship> findAll(
		int start, int end, OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Relationship> list = null;

		if (useFinderCache) {
			list = (List<Relationship>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_RELATIONSHIP);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_RELATIONSHIP;

				sql = sql.concat(RelationshipModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Relationship>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the relationships from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Relationship relationship : findAll()) {
			remove(relationship);
		}
	}

	/**
	 * Returns the number of relationships.
	 *
	 * @return the number of relationships
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_RELATIONSHIP);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "relationshipId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_RELATIONSHIP;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return RelationshipModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the relationship persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByUserIdAndCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserIdAndCompanyId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"userId", "companyId"}, true);

		_finderPathWithoutPaginationFindByUserIdAndCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUserIdAndCompanyId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"userId", "companyId"}, true);

		_finderPathCountByUserIdAndCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserIdAndCompanyId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"userId", "companyId"}, false);

		_finderPathFetchByUserIdAndRelUserId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUserIdAndRelUserId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"userId", "relUserId"}, true);

		_finderPathCountByUserIdAndRelUserId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUserIdAndRelUserId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"userId", "relUserId"}, false);

		RelationshipUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		RelationshipUtil.setPersistence(null);

		entityCache.removeCache(RelationshipImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_Account_RelationshipsPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_Account_RelationshipsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_Account_RelationshipsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_RELATIONSHIP =
		"SELECT relationship FROM Relationship relationship";

	private static final String _SQL_SELECT_RELATIONSHIP_WHERE =
		"SELECT relationship FROM Relationship relationship WHERE ";

	private static final String _SQL_COUNT_RELATIONSHIP =
		"SELECT COUNT(relationship) FROM Relationship relationship";

	private static final String _SQL_COUNT_RELATIONSHIP_WHERE =
		"SELECT COUNT(relationship) FROM Relationship relationship WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "relationship.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Relationship exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Relationship exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		RelationshipPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "type"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}