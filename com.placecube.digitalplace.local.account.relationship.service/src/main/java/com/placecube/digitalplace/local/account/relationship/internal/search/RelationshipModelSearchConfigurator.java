package com.placecube.digitalplace.local.account.relationship.internal.search;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.registrar.ModelSearchConfigurator;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;

@Component(immediate = true, service = { ModelSearchConfigurator.class })
public class RelationshipModelSearchConfigurator implements ModelSearchConfigurator<Relationship> {

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.local.account.relationship.model.Relationship)")
	protected ModelIndexerWriterContributor<Relationship> modelIndexWriterContributor;
	

	@Override
	public String getClassName() {
		return Relationship.class.getName();
	}

	@Override
	public ModelIndexerWriterContributor<Relationship> getModelIndexerWriterContributor() {
		return modelIndexWriterContributor;
	}

}
