package com.placecube.digitalplace.local.account.relationship.internal.upgrade.upgrade_1_0_1;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade_1_0_1 implements UpgradeStepRegistrator {

	@Reference
	private RelationshipLocalService relationshipLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new RelationshipTableSchemaUpdate(relationshipLocalService));
	}

}
