package com.placecube.digitalplace.local.account.relationship.internal.search.spi.model.index.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.local.account.relationship.model.Relationship", service = ModelDocumentContributor.class)
public class RelationshipModelDocumentContributor implements ModelDocumentContributor<Relationship> {

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void contribute(Document document, Relationship relationship) {

		document.addKeyword(Field.USER_ID, relationship.getUserId());
		document.addKeyword("relUserId", relationship.getRelUserId());
		User relUser = userLocalService.fetchUser(relationship.getRelUserId());
		if (Validator.isNotNull(relUser)) {
			document.addKeyword("relUserFullName", relUser.getFullName());
		}
		document.addKeyword(Field.TYPE, relationship.getType());
		document.addDate(Field.CREATE_DATE, relationship.getCreateDate());
		document.addKeyword(Field.COMPANY_ID, relationship.getCompanyId());
	}
}
