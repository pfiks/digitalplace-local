/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.local.account.relationship.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;
import com.placecube.digitalplace.local.account.relationship.service.base.RelationshipLocalServiceBaseImpl;

/**
 * The implementation of the relationship local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.local.account.relationship.service.RelationshipLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RelationshipLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.local.account.relationship.model.Relationship", service = AopService.class)
public class RelationshipLocalServiceImpl extends RelationshipLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.local.account.relationship.service.
	 * RelationshipLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.local.account.relationship.service.
	 * RelationshipLocalServiceUtil</code>.
	 */

	private static final Log LOG = LogFactoryUtil.getLog(RelationshipLocalServiceImpl.class);

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Relationship addRelationship(long userId, long relUserId, long companyId, String type) {

		Relationship relationship = relationshipPersistence.fetchByUserIdAndRelUserId(userId, relUserId);
		if (Validator.isNull(relationship)) {
			relationship = relationshipPersistence.create(counterLocalService.increment(Relationship.class.getName(), 1));

			relationship.setCompanyId(companyId);
			relationship.setUserId(userId);
			relationship.setRelUserId(relUserId);
			relationship.setType(type);

			Date now = new Date();
			relationship.setCreateDate(now);

			relationship = relationshipPersistence.update(relationship);
		} else {
			LOG.warn("Relationship already exists - relationshipId: " + relationship.getRelationshipId());
		}
		return relationship;
	}

	@Override
	public List<Relationship> getRelationshipsByUserIdAndCompanyId(long userId, long companyId) {
		return relationshipPersistence.findByUserIdAndCompanyId(userId, companyId);
	}
}