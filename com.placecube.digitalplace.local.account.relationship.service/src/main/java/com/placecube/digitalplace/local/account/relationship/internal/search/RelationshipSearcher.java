package com.placecube.digitalplace.local.account.relationship.internal.search;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.BaseSearcher;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;

@Component(immediate = true, property = "model.class.name=com.placecube.digitalplace.local.account.relationship.model.Relationship", service = BaseSearcher.class)
public class RelationshipSearcher extends BaseSearcher {

	public RelationshipSearcher() {
		setPermissionAware(false);
	}

	@Override
	public String getClassName() {
		return Relationship.class.getName();
	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) throws Exception {
		addSearchTerm(searchQuery, searchContext, Field.USER_ID, false);
	}

}