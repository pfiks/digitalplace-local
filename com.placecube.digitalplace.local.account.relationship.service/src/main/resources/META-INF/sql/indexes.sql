create index IX_2FE010D7 on Placecube_Account_Relationships_Relationship (userId, companyId);
create index IX_7DD5DCDE on Placecube_Account_Relationships_Relationship (userId, relUserId);
create index IX_7E72CC5D on Placecube_Account_Relationships_Relationship (uuid_[$COLUMN_LENGTH:75$]);