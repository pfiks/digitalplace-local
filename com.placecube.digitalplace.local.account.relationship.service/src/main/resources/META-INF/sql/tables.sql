create table Placecube_Account_Relationships_Relationship (
	uuid_ VARCHAR(75) null,
	relationshipId LONG not null primary key,
	companyId LONG,
	createDate DATE null,
	userId LONG,
	relUserId LONG,
	type_ VARCHAR(75) null
);