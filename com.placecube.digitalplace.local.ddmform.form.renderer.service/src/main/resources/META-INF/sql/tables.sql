create table Placecube_FormRenderer_FormEntry (
	uuid_ VARCHAR(75) null,
	formEntryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	uniqueReference VARCHAR(75) null,
	formInstanceRecordVersionId LONG,
	plid LONG,
	status INTEGER
);