create index IX_CBC83334 on Placecube_FormRenderer_FormEntry (status, formInstanceRecordVersionId);
create index IX_7523F2E5 on Placecube_FormRenderer_FormEntry (status, uniqueReference[$COLUMN_LENGTH:75$]);
create index IX_7A752FD5 on Placecube_FormRenderer_FormEntry (uuid_[$COLUMN_LENGTH:75$]);