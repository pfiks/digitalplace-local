/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.local.ddmform.form.renderer.exception.NoSuchFormEntryException;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntryTable;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryImpl;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence.FormEntryPersistence;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence.FormEntryUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence.impl.constants.Placecube_FormRendererPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the form entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = FormEntryPersistence.class)
public class FormEntryPersistenceImpl
	extends BasePersistenceImpl<FormEntry> implements FormEntryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>FormEntryUtil</code> to access the form entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		FormEntryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the form entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<FormEntry> list = null;

		if (useFinderCache) {
			list = (List<FormEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FormEntry formEntry : list) {
					if (!uuid.equals(formEntry.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_FORMENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FormEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<FormEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUuid_First(
			String uuid, OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUuid_First(uuid, orderByComparator);

		if (formEntry != null) {
			return formEntry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFormEntryException(sb.toString());
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUuid_First(
		String uuid, OrderByComparator<FormEntry> orderByComparator) {

		List<FormEntry> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUuid_Last(
			String uuid, OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUuid_Last(uuid, orderByComparator);

		if (formEntry != null) {
			return formEntry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFormEntryException(sb.toString());
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUuid_Last(
		String uuid, OrderByComparator<FormEntry> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<FormEntry> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uuid = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry[] findByUuid_PrevAndNext(
			long formEntryId, String uuid,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		uuid = Objects.toString(uuid, "");

		FormEntry formEntry = findByPrimaryKey(formEntryId);

		Session session = null;

		try {
			session = openSession();

			FormEntry[] array = new FormEntryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, formEntry, uuid, orderByComparator, true);

			array[1] = formEntry;

			array[2] = getByUuid_PrevAndNext(
				session, formEntry, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FormEntry getByUuid_PrevAndNext(
		Session session, FormEntry formEntry, String uuid,
		OrderByComparator<FormEntry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_FORMENTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FormEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(formEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FormEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the form entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (FormEntry formEntry :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(formEntry);
		}
	}

	/**
	 * Returns the number of form entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching form entries
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FORMENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"formEntry.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(formEntry.uuid IS NULL OR formEntry.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUUID_G(String uuid, long groupId)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUUID_G(uuid, groupId);

		if (formEntry == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFormEntryException(sb.toString());
		}

		return formEntry;
	}

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof FormEntry) {
			FormEntry formEntry = (FormEntry)result;

			if (!Objects.equals(uuid, formEntry.getUuid()) ||
				(groupId != formEntry.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_FORMENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<FormEntry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					FormEntry formEntry = list.get(0);

					result = formEntry;

					cacheResult(formEntry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FormEntry)result;
		}
	}

	/**
	 * Removes the form entry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the form entry that was removed
	 */
	@Override
	public FormEntry removeByUUID_G(String uuid, long groupId)
		throws NoSuchFormEntryException {

		FormEntry formEntry = findByUUID_G(uuid, groupId);

		return remove(formEntry);
	}

	/**
	 * Returns the number of form entries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching form entries
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FORMENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"formEntry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(formEntry.uuid IS NULL OR formEntry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"formEntry.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<FormEntry> list = null;

		if (useFinderCache) {
			list = (List<FormEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FormEntry formEntry : list) {
					if (!uuid.equals(formEntry.getUuid()) ||
						(companyId != formEntry.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_FORMENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FormEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<FormEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (formEntry != null) {
			return formEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchFormEntryException(sb.toString());
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<FormEntry> orderByComparator) {

		List<FormEntry> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (formEntry != null) {
			return formEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchFormEntryException(sb.toString());
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<FormEntry> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<FormEntry> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry[] findByUuid_C_PrevAndNext(
			long formEntryId, String uuid, long companyId,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		uuid = Objects.toString(uuid, "");

		FormEntry formEntry = findByPrimaryKey(formEntryId);

		Session session = null;

		try {
			session = openSession();

			FormEntry[] array = new FormEntryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, formEntry, uuid, companyId, orderByComparator, true);

			array[1] = formEntry;

			array[2] = getByUuid_C_PrevAndNext(
				session, formEntry, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FormEntry getByUuid_C_PrevAndNext(
		Session session, FormEntry formEntry, String uuid, long companyId,
		OrderByComparator<FormEntry> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_FORMENTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FormEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(formEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FormEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the form entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (FormEntry formEntry :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(formEntry);
		}
	}

	/**
	 * Returns the number of form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching form entries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FORMENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"formEntry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(formEntry.uuid IS NULL OR formEntry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"formEntry.companyId = ?";

	private FinderPath _finderPathFetchByFormInstanceRecordVersionId;
	private FinderPath _finderPathCountByFormInstanceRecordVersionId;

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);

		if (formEntry == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("formInstanceRecordVersionId=");
			sb.append(formInstanceRecordVersionId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFormEntryException(sb.toString());
		}

		return formEntry;
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId) {

		return fetchByFormInstanceRecordVersionId(
			formInstanceRecordVersionId, true);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {formInstanceRecordVersionId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByFormInstanceRecordVersionId, finderArgs,
				this);
		}

		if (result instanceof FormEntry) {
			FormEntry formEntry = (FormEntry)result;

			if (formInstanceRecordVersionId !=
					formEntry.getFormInstanceRecordVersionId()) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_FORMENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDVERSIONID_FORMINSTANCERECORDVERSIONID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(formInstanceRecordVersionId);

				List<FormEntry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByFormInstanceRecordVersionId,
							finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									formInstanceRecordVersionId
								};
							}

							_log.warn(
								"FormEntryPersistenceImpl.fetchByFormInstanceRecordVersionId(long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					FormEntry formEntry = list.get(0);

					result = formEntry;

					cacheResult(formEntry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FormEntry)result;
		}
	}

	/**
	 * Removes the form entry where formInstanceRecordVersionId = &#63; from the database.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the form entry that was removed
	 */
	@Override
	public FormEntry removeByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws NoSuchFormEntryException {

		FormEntry formEntry = findByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);

		return remove(formEntry);
	}

	/**
	 * Returns the number of form entries where formInstanceRecordVersionId = &#63;.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the number of matching form entries
	 */
	@Override
	public int countByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId) {

		FinderPath finderPath = _finderPathCountByFormInstanceRecordVersionId;

		Object[] finderArgs = new Object[] {formInstanceRecordVersionId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FORMENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDVERSIONID_FORMINSTANCERECORDVERSIONID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(formInstanceRecordVersionId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_FORMINSTANCERECORDVERSIONID_FORMINSTANCERECORDVERSIONID_2 =
			"formEntry.formInstanceRecordVersionId = ?";

	private FinderPath _finderPathWithPaginationFindByUniqueReferenceAndStatus;
	private FinderPath
		_finderPathWithoutPaginationFindByUniqueReferenceAndStatus;
	private FinderPath _finderPathCountByUniqueReferenceAndStatus;

	/**
	 * Returns all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @return the matching form entries
	 */
	@Override
	public List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		return findByUniqueReferenceAndStatus(
			uniqueReference, status, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end) {

		return findByUniqueReferenceAndStatus(
			uniqueReference, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return findByUniqueReferenceAndStatus(
			uniqueReference, status, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	@Override
	public List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end,
		OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		uniqueReference = Objects.toString(uniqueReference, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByUniqueReferenceAndStatus;
				finderArgs = new Object[] {uniqueReference, status};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByUniqueReferenceAndStatus;
			finderArgs = new Object[] {
				uniqueReference, status, start, end, orderByComparator
			};
		}

		List<FormEntry> list = null;

		if (useFinderCache) {
			list = (List<FormEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (FormEntry formEntry : list) {
					if (!uniqueReference.equals(
							formEntry.getUniqueReference()) ||
						(status != formEntry.getStatus())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_FORMENTRY_WHERE);

			boolean bindUniqueReference = false;

			if (uniqueReference.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_3);
			}
			else {
				bindUniqueReference = true;

				sb.append(
					_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_2);
			}

			sb.append(_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FormEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUniqueReference) {
					queryPos.add(uniqueReference);
				}

				queryPos.add(status);

				list = (List<FormEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUniqueReferenceAndStatus_First(
			String uniqueReference, int status,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUniqueReferenceAndStatus_First(
			uniqueReference, status, orderByComparator);

		if (formEntry != null) {
			return formEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uniqueReference=");
		sb.append(uniqueReference);

		sb.append(", status=");
		sb.append(status);

		sb.append("}");

		throw new NoSuchFormEntryException(sb.toString());
	}

	/**
	 * Returns the first form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUniqueReferenceAndStatus_First(
		String uniqueReference, int status,
		OrderByComparator<FormEntry> orderByComparator) {

		List<FormEntry> list = findByUniqueReferenceAndStatus(
			uniqueReference, status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByUniqueReferenceAndStatus_Last(
			String uniqueReference, int status,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByUniqueReferenceAndStatus_Last(
			uniqueReference, status, orderByComparator);

		if (formEntry != null) {
			return formEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uniqueReference=");
		sb.append(uniqueReference);

		sb.append(", status=");
		sb.append(status);

		sb.append("}");

		throw new NoSuchFormEntryException(sb.toString());
	}

	/**
	 * Returns the last form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByUniqueReferenceAndStatus_Last(
		String uniqueReference, int status,
		OrderByComparator<FormEntry> orderByComparator) {

		int count = countByUniqueReferenceAndStatus(uniqueReference, status);

		if (count == 0) {
			return null;
		}

		List<FormEntry> list = findByUniqueReferenceAndStatus(
			uniqueReference, status, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry[] findByUniqueReferenceAndStatus_PrevAndNext(
			long formEntryId, String uniqueReference, int status,
			OrderByComparator<FormEntry> orderByComparator)
		throws NoSuchFormEntryException {

		uniqueReference = Objects.toString(uniqueReference, "");

		FormEntry formEntry = findByPrimaryKey(formEntryId);

		Session session = null;

		try {
			session = openSession();

			FormEntry[] array = new FormEntryImpl[3];

			array[0] = getByUniqueReferenceAndStatus_PrevAndNext(
				session, formEntry, uniqueReference, status, orderByComparator,
				true);

			array[1] = formEntry;

			array[2] = getByUniqueReferenceAndStatus_PrevAndNext(
				session, formEntry, uniqueReference, status, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected FormEntry getByUniqueReferenceAndStatus_PrevAndNext(
		Session session, FormEntry formEntry, String uniqueReference,
		int status, OrderByComparator<FormEntry> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_FORMENTRY_WHERE);

		boolean bindUniqueReference = false;

		if (uniqueReference.isEmpty()) {
			sb.append(
				_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_3);
		}
		else {
			bindUniqueReference = true;

			sb.append(
				_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_2);
		}

		sb.append(_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FormEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUniqueReference) {
			queryPos.add(uniqueReference);
		}

		queryPos.add(status);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(formEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<FormEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the form entries where uniqueReference = &#63; and status = &#63; from the database.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 */
	@Override
	public void removeByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		for (FormEntry formEntry :
				findByUniqueReferenceAndStatus(
					uniqueReference, status, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(formEntry);
		}
	}

	/**
	 * Returns the number of form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @return the number of matching form entries
	 */
	@Override
	public int countByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		uniqueReference = Objects.toString(uniqueReference, "");

		FinderPath finderPath = _finderPathCountByUniqueReferenceAndStatus;

		Object[] finderArgs = new Object[] {uniqueReference, status};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FORMENTRY_WHERE);

			boolean bindUniqueReference = false;

			if (uniqueReference.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_3);
			}
			else {
				bindUniqueReference = true;

				sb.append(
					_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_2);
			}

			sb.append(_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_STATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUniqueReference) {
					queryPos.add(uniqueReference);
				}

				queryPos.add(status);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_2 =
			"formEntry.uniqueReference = ? AND ";

	private static final String
		_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_UNIQUEREFERENCE_3 =
			"(formEntry.uniqueReference IS NULL OR formEntry.uniqueReference = '') AND ";

	private static final String
		_FINDER_COLUMN_UNIQUEREFERENCEANDSTATUS_STATUS_2 =
			"formEntry.status = ?";

	private FinderPath _finderPathFetchByFormInstanceRecordVersionIdAndStatus;
	private FinderPath _finderPathCountByFormInstanceRecordVersionIdAndStatus;

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	@Override
	public FormEntry findByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);

		if (formEntry == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("formInstanceRecordVersionId=");
			sb.append(formInstanceRecordVersionId);

			sb.append(", status=");
			sb.append(status);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFormEntryException(sb.toString());
		}

		return formEntry;
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status) {

		return fetchByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status, true);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public FormEntry fetchByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {formInstanceRecordVersionId, status};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByFormInstanceRecordVersionIdAndStatus,
				finderArgs, this);
		}

		if (result instanceof FormEntry) {
			FormEntry formEntry = (FormEntry)result;

			if ((formInstanceRecordVersionId !=
					formEntry.getFormInstanceRecordVersionId()) ||
				(status != formEntry.getStatus())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_FORMENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDVERSIONIDANDSTATUS_FORMINSTANCERECORDVERSIONID_2);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDVERSIONIDANDSTATUS_STATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(formInstanceRecordVersionId);

				queryPos.add(status);

				List<FormEntry> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByFormInstanceRecordVersionIdAndStatus,
							finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									formInstanceRecordVersionId, status
								};
							}

							_log.warn(
								"FormEntryPersistenceImpl.fetchByFormInstanceRecordVersionIdAndStatus(long, int, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					FormEntry formEntry = list.get(0);

					result = formEntry;

					cacheResult(formEntry);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FormEntry)result;
		}
	}

	/**
	 * Removes the form entry where formInstanceRecordVersionId = &#63; and status = &#63; from the database.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the form entry that was removed
	 */
	@Override
	public FormEntry removeByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws NoSuchFormEntryException {

		FormEntry formEntry = findByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);

		return remove(formEntry);
	}

	/**
	 * Returns the number of form entries where formInstanceRecordVersionId = &#63; and status = &#63;.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the number of matching form entries
	 */
	@Override
	public int countByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status) {

		FinderPath finderPath =
			_finderPathCountByFormInstanceRecordVersionIdAndStatus;

		Object[] finderArgs = new Object[] {
			formInstanceRecordVersionId, status
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FORMENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDVERSIONIDANDSTATUS_FORMINSTANCERECORDVERSIONID_2);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDVERSIONIDANDSTATUS_STATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(formInstanceRecordVersionId);

				queryPos.add(status);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_FORMINSTANCERECORDVERSIONIDANDSTATUS_FORMINSTANCERECORDVERSIONID_2 =
			"formEntry.formInstanceRecordVersionId = ? AND ";

	private static final String
		_FINDER_COLUMN_FORMINSTANCERECORDVERSIONIDANDSTATUS_STATUS_2 =
			"formEntry.status = ?";

	public FormEntryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(FormEntry.class);

		setModelImplClass(FormEntryImpl.class);
		setModelPKClass(long.class);

		setTable(FormEntryTable.INSTANCE);
	}

	/**
	 * Caches the form entry in the entity cache if it is enabled.
	 *
	 * @param formEntry the form entry
	 */
	@Override
	public void cacheResult(FormEntry formEntry) {
		entityCache.putResult(
			FormEntryImpl.class, formEntry.getPrimaryKey(), formEntry);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {formEntry.getUuid(), formEntry.getGroupId()},
			formEntry);

		finderCache.putResult(
			_finderPathFetchByFormInstanceRecordVersionId,
			new Object[] {formEntry.getFormInstanceRecordVersionId()},
			formEntry);

		finderCache.putResult(
			_finderPathFetchByFormInstanceRecordVersionIdAndStatus,
			new Object[] {
				formEntry.getFormInstanceRecordVersionId(),
				formEntry.getStatus()
			},
			formEntry);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the form entries in the entity cache if it is enabled.
	 *
	 * @param formEntries the form entries
	 */
	@Override
	public void cacheResult(List<FormEntry> formEntries) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (formEntries.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (FormEntry formEntry : formEntries) {
			if (entityCache.getResult(
					FormEntryImpl.class, formEntry.getPrimaryKey()) == null) {

				cacheResult(formEntry);
			}
		}
	}

	/**
	 * Clears the cache for all form entries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(FormEntryImpl.class);

		finderCache.clearCache(FormEntryImpl.class);
	}

	/**
	 * Clears the cache for the form entry.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FormEntry formEntry) {
		entityCache.removeResult(FormEntryImpl.class, formEntry);
	}

	@Override
	public void clearCache(List<FormEntry> formEntries) {
		for (FormEntry formEntry : formEntries) {
			entityCache.removeResult(FormEntryImpl.class, formEntry);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FormEntryImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(FormEntryImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		FormEntryModelImpl formEntryModelImpl) {

		Object[] args = new Object[] {
			formEntryModelImpl.getUuid(), formEntryModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, formEntryModelImpl);

		args = new Object[] {
			formEntryModelImpl.getFormInstanceRecordVersionId()
		};

		finderCache.putResult(
			_finderPathCountByFormInstanceRecordVersionId, args,
			Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByFormInstanceRecordVersionId, args,
			formEntryModelImpl);

		args = new Object[] {
			formEntryModelImpl.getFormInstanceRecordVersionId(),
			formEntryModelImpl.getStatus()
		};

		finderCache.putResult(
			_finderPathCountByFormInstanceRecordVersionIdAndStatus, args,
			Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByFormInstanceRecordVersionIdAndStatus, args,
			formEntryModelImpl);
	}

	/**
	 * Creates a new form entry with the primary key. Does not add the form entry to the database.
	 *
	 * @param formEntryId the primary key for the new form entry
	 * @return the new form entry
	 */
	@Override
	public FormEntry create(long formEntryId) {
		FormEntry formEntry = new FormEntryImpl();

		formEntry.setNew(true);
		formEntry.setPrimaryKey(formEntryId);

		String uuid = PortalUUIDUtil.generate();

		formEntry.setUuid(uuid);

		formEntry.setCompanyId(CompanyThreadLocal.getCompanyId());

		return formEntry;
	}

	/**
	 * Removes the form entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry that was removed
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry remove(long formEntryId) throws NoSuchFormEntryException {
		return remove((Serializable)formEntryId);
	}

	/**
	 * Removes the form entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the form entry
	 * @return the form entry that was removed
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry remove(Serializable primaryKey)
		throws NoSuchFormEntryException {

		Session session = null;

		try {
			session = openSession();

			FormEntry formEntry = (FormEntry)session.get(
				FormEntryImpl.class, primaryKey);

			if (formEntry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFormEntryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(formEntry);
		}
		catch (NoSuchFormEntryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FormEntry removeImpl(FormEntry formEntry) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(formEntry)) {
				formEntry = (FormEntry)session.get(
					FormEntryImpl.class, formEntry.getPrimaryKeyObj());
			}

			if (formEntry != null) {
				session.delete(formEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (formEntry != null) {
			clearCache(formEntry);
		}

		return formEntry;
	}

	@Override
	public FormEntry updateImpl(FormEntry formEntry) {
		boolean isNew = formEntry.isNew();

		if (!(formEntry instanceof FormEntryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(formEntry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(formEntry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in formEntry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom FormEntry implementation " +
					formEntry.getClass());
		}

		FormEntryModelImpl formEntryModelImpl = (FormEntryModelImpl)formEntry;

		if (Validator.isNull(formEntry.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			formEntry.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (formEntry.getCreateDate() == null)) {
			if (serviceContext == null) {
				formEntry.setCreateDate(date);
			}
			else {
				formEntry.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!formEntryModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				formEntry.setModifiedDate(date);
			}
			else {
				formEntry.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(formEntry);
			}
			else {
				formEntry = (FormEntry)session.merge(formEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			FormEntryImpl.class, formEntryModelImpl, false, true);

		cacheUniqueFindersCache(formEntryModelImpl);

		if (isNew) {
			formEntry.setNew(false);
		}

		formEntry.resetOriginalValues();

		return formEntry;
	}

	/**
	 * Returns the form entry with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the form entry
	 * @return the form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFormEntryException {

		FormEntry formEntry = fetchByPrimaryKey(primaryKey);

		if (formEntry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFormEntryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return formEntry;
	}

	/**
	 * Returns the form entry with the primary key or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry findByPrimaryKey(long formEntryId)
		throws NoSuchFormEntryException {

		return findByPrimaryKey((Serializable)formEntryId);
	}

	/**
	 * Returns the form entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry, or <code>null</code> if a form entry with the primary key could not be found
	 */
	@Override
	public FormEntry fetchByPrimaryKey(long formEntryId) {
		return fetchByPrimaryKey((Serializable)formEntryId);
	}

	/**
	 * Returns all the form entries.
	 *
	 * @return the form entries
	 */
	@Override
	public List<FormEntry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of form entries
	 */
	@Override
	public List<FormEntry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of form entries
	 */
	@Override
	public List<FormEntry> findAll(
		int start, int end, OrderByComparator<FormEntry> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of form entries
	 */
	@Override
	public List<FormEntry> findAll(
		int start, int end, OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<FormEntry> list = null;

		if (useFinderCache) {
			list = (List<FormEntry>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_FORMENTRY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_FORMENTRY;

				sql = sql.concat(FormEntryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<FormEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the form entries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (FormEntry formEntry : findAll()) {
			remove(formEntry);
		}
	}

	/**
	 * Returns the number of form entries.
	 *
	 * @return the number of form entries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_FORMENTRY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "formEntryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_FORMENTRY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return FormEntryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the form entry persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathFetchByFormInstanceRecordVersionId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByFormInstanceRecordVersionId",
			new String[] {Long.class.getName()},
			new String[] {"formInstanceRecordVersionId"}, true);

		_finderPathCountByFormInstanceRecordVersionId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByFormInstanceRecordVersionId",
			new String[] {Long.class.getName()},
			new String[] {"formInstanceRecordVersionId"}, false);

		_finderPathWithPaginationFindByUniqueReferenceAndStatus =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByUniqueReferenceAndStatus",
				new String[] {
					String.class.getName(), Integer.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				},
				new String[] {"uniqueReference", "status"}, true);

		_finderPathWithoutPaginationFindByUniqueReferenceAndStatus =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByUniqueReferenceAndStatus",
				new String[] {String.class.getName(), Integer.class.getName()},
				new String[] {"uniqueReference", "status"}, true);

		_finderPathCountByUniqueReferenceAndStatus = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUniqueReferenceAndStatus",
			new String[] {String.class.getName(), Integer.class.getName()},
			new String[] {"uniqueReference", "status"}, false);

		_finderPathFetchByFormInstanceRecordVersionIdAndStatus = new FinderPath(
			FINDER_CLASS_NAME_ENTITY,
			"fetchByFormInstanceRecordVersionIdAndStatus",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"formInstanceRecordVersionId", "status"}, true);

		_finderPathCountByFormInstanceRecordVersionIdAndStatus = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByFormInstanceRecordVersionIdAndStatus",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"formInstanceRecordVersionId", "status"}, false);

		FormEntryUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		FormEntryUtil.setPersistence(null);

		entityCache.removeCache(FormEntryImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_FormRendererPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_FormRendererPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_FormRendererPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_FORMENTRY =
		"SELECT formEntry FROM FormEntry formEntry";

	private static final String _SQL_SELECT_FORMENTRY_WHERE =
		"SELECT formEntry FROM FormEntry formEntry WHERE ";

	private static final String _SQL_COUNT_FORMENTRY =
		"SELECT COUNT(formEntry) FROM FormEntry formEntry";

	private static final String _SQL_COUNT_FORMENTRY_WHERE =
		"SELECT COUNT(formEntry) FROM FormEntry formEntry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "formEntry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No FormEntry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No FormEntry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		FormEntryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}