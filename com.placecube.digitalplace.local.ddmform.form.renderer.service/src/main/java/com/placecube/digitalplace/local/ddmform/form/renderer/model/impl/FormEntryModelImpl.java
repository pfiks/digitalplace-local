/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;

import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntryModel;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.sql.Blob;
import java.sql.Types;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the FormEntry service. Represents a row in the &quot;Placecube_FormRenderer_FormEntry&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>FormEntryModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link FormEntryImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryImpl
 * @generated
 */
public class FormEntryModelImpl
	extends BaseModelImpl<FormEntry> implements FormEntryModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a form entry model instance should use the <code>FormEntry</code> interface instead.
	 */
	public static final String TABLE_NAME = "Placecube_FormRenderer_FormEntry";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"formEntryId", Types.BIGINT},
		{"groupId", Types.BIGINT}, {"companyId", Types.BIGINT},
		{"userId", Types.BIGINT}, {"userName", Types.VARCHAR},
		{"createDate", Types.TIMESTAMP}, {"modifiedDate", Types.TIMESTAMP},
		{"uniqueReference", Types.VARCHAR},
		{"formInstanceRecordVersionId", Types.BIGINT}, {"plid", Types.BIGINT},
		{"status", Types.INTEGER}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("formEntryId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("groupId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("modifiedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("uniqueReference", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("formInstanceRecordVersionId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("plid", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("status", Types.INTEGER);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Placecube_FormRenderer_FormEntry (uuid_ VARCHAR(75) null,formEntryId LONG not null primary key,groupId LONG,companyId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,uniqueReference VARCHAR(75) null,formInstanceRecordVersionId LONG,plid LONG,status INTEGER)";

	public static final String TABLE_SQL_DROP =
		"drop table Placecube_FormRenderer_FormEntry";

	public static final String ORDER_BY_JPQL =
		" ORDER BY formEntry.formEntryId ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY Placecube_FormRenderer_FormEntry.formEntryId ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long COMPANYID_COLUMN_BITMASK = 1L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long FORMINSTANCERECORDVERSIONID_COLUMN_BITMASK = 2L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long GROUPID_COLUMN_BITMASK = 4L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long STATUS_COLUMN_BITMASK = 8L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UNIQUEREFERENCE_COLUMN_BITMASK = 16L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UUID_COLUMN_BITMASK = 32L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *		#getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long FORMENTRYID_COLUMN_BITMASK = 64L;

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
	}

	public FormEntryModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _formEntryId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setFormEntryId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _formEntryId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return FormEntry.class;
	}

	@Override
	public String getModelClassName() {
		return FormEntry.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<FormEntry, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<FormEntry, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<FormEntry, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName, attributeGetterFunction.apply((FormEntry)this));
		}

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<FormEntry, Object>> attributeSetterBiConsumers =
			getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<FormEntry, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(FormEntry)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<FormEntry, Object>>
		getAttributeGetterFunctions() {

		return AttributeGetterFunctionsHolder._attributeGetterFunctions;
	}

	public Map<String, BiConsumer<FormEntry, Object>>
		getAttributeSetterBiConsumers() {

		return AttributeSetterBiConsumersHolder._attributeSetterBiConsumers;
	}

	private static class AttributeGetterFunctionsHolder {

		private static final Map<String, Function<FormEntry, Object>>
			_attributeGetterFunctions;

		static {
			Map<String, Function<FormEntry, Object>> attributeGetterFunctions =
				new LinkedHashMap<String, Function<FormEntry, Object>>();

			attributeGetterFunctions.put("uuid", FormEntry::getUuid);
			attributeGetterFunctions.put(
				"formEntryId", FormEntry::getFormEntryId);
			attributeGetterFunctions.put("groupId", FormEntry::getGroupId);
			attributeGetterFunctions.put("companyId", FormEntry::getCompanyId);
			attributeGetterFunctions.put("userId", FormEntry::getUserId);
			attributeGetterFunctions.put("userName", FormEntry::getUserName);
			attributeGetterFunctions.put(
				"createDate", FormEntry::getCreateDate);
			attributeGetterFunctions.put(
				"modifiedDate", FormEntry::getModifiedDate);
			attributeGetterFunctions.put(
				"uniqueReference", FormEntry::getUniqueReference);
			attributeGetterFunctions.put(
				"formInstanceRecordVersionId",
				FormEntry::getFormInstanceRecordVersionId);
			attributeGetterFunctions.put("plid", FormEntry::getPlid);
			attributeGetterFunctions.put("status", FormEntry::getStatus);

			_attributeGetterFunctions = Collections.unmodifiableMap(
				attributeGetterFunctions);
		}

	}

	private static class AttributeSetterBiConsumersHolder {

		private static final Map<String, BiConsumer<FormEntry, Object>>
			_attributeSetterBiConsumers;

		static {
			Map<String, BiConsumer<FormEntry, ?>> attributeSetterBiConsumers =
				new LinkedHashMap<String, BiConsumer<FormEntry, ?>>();

			attributeSetterBiConsumers.put(
				"uuid", (BiConsumer<FormEntry, String>)FormEntry::setUuid);
			attributeSetterBiConsumers.put(
				"formEntryId",
				(BiConsumer<FormEntry, Long>)FormEntry::setFormEntryId);
			attributeSetterBiConsumers.put(
				"groupId", (BiConsumer<FormEntry, Long>)FormEntry::setGroupId);
			attributeSetterBiConsumers.put(
				"companyId",
				(BiConsumer<FormEntry, Long>)FormEntry::setCompanyId);
			attributeSetterBiConsumers.put(
				"userId", (BiConsumer<FormEntry, Long>)FormEntry::setUserId);
			attributeSetterBiConsumers.put(
				"userName",
				(BiConsumer<FormEntry, String>)FormEntry::setUserName);
			attributeSetterBiConsumers.put(
				"createDate",
				(BiConsumer<FormEntry, Date>)FormEntry::setCreateDate);
			attributeSetterBiConsumers.put(
				"modifiedDate",
				(BiConsumer<FormEntry, Date>)FormEntry::setModifiedDate);
			attributeSetterBiConsumers.put(
				"uniqueReference",
				(BiConsumer<FormEntry, String>)FormEntry::setUniqueReference);
			attributeSetterBiConsumers.put(
				"formInstanceRecordVersionId",
				(BiConsumer<FormEntry, Long>)
					FormEntry::setFormInstanceRecordVersionId);
			attributeSetterBiConsumers.put(
				"plid", (BiConsumer<FormEntry, Long>)FormEntry::setPlid);
			attributeSetterBiConsumers.put(
				"status", (BiConsumer<FormEntry, Integer>)FormEntry::setStatus);

			_attributeSetterBiConsumers = Collections.unmodifiableMap(
				(Map)attributeSetterBiConsumers);
		}

	}

	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uuid = uuid;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUuid() {
		return getColumnOriginalValue("uuid_");
	}

	@Override
	public long getFormEntryId() {
		return _formEntryId;
	}

	@Override
	public void setFormEntryId(long formEntryId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_formEntryId = formEntryId;
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_groupId = groupId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalGroupId() {
		return GetterUtil.getLong(this.<Long>getColumnOriginalValue("groupId"));
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_companyId = companyId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalCompanyId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("companyId"));
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException portalException) {
			return "";
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	@Override
	public String getUserName() {
		if (_userName == null) {
			return "";
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userName = userName;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public boolean hasSetModifiedDate() {
		return _setModifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_setModifiedDate = true;

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_modifiedDate = modifiedDate;
	}

	@Override
	public String getUniqueReference() {
		if (_uniqueReference == null) {
			return "";
		}
		else {
			return _uniqueReference;
		}
	}

	@Override
	public void setUniqueReference(String uniqueReference) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uniqueReference = uniqueReference;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUniqueReference() {
		return getColumnOriginalValue("uniqueReference");
	}

	@Override
	public long getFormInstanceRecordVersionId() {
		return _formInstanceRecordVersionId;
	}

	@Override
	public void setFormInstanceRecordVersionId(
		long formInstanceRecordVersionId) {

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_formInstanceRecordVersionId = formInstanceRecordVersionId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalFormInstanceRecordVersionId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("formInstanceRecordVersionId"));
	}

	@Override
	public long getPlid() {
		return _plid;
	}

	@Override
	public void setPlid(long plid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_plid = plid;
	}

	@Override
	public int getStatus() {
		return _status;
	}

	@Override
	public void setStatus(int status) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_status = status;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public int getOriginalStatus() {
		return GetterUtil.getInteger(
			this.<Integer>getColumnOriginalValue("status"));
	}

	@Override
	public StagedModelType getStagedModelType() {
		return new StagedModelType(
			PortalUtil.getClassNameId(FormEntry.class.getName()));
	}

	public long getColumnBitmask() {
		if (_columnBitmask > 0) {
			return _columnBitmask;
		}

		if ((_columnOriginalValues == null) ||
			(_columnOriginalValues == Collections.EMPTY_MAP)) {

			return 0;
		}

		for (Map.Entry<String, Object> entry :
				_columnOriginalValues.entrySet()) {

			if (!Objects.equals(
					entry.getValue(), getColumnValue(entry.getKey()))) {

				_columnBitmask |= _columnBitmasks.get(entry.getKey());
			}
		}

		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), FormEntry.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public FormEntry toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, FormEntry>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		FormEntryImpl formEntryImpl = new FormEntryImpl();

		formEntryImpl.setUuid(getUuid());
		formEntryImpl.setFormEntryId(getFormEntryId());
		formEntryImpl.setGroupId(getGroupId());
		formEntryImpl.setCompanyId(getCompanyId());
		formEntryImpl.setUserId(getUserId());
		formEntryImpl.setUserName(getUserName());
		formEntryImpl.setCreateDate(getCreateDate());
		formEntryImpl.setModifiedDate(getModifiedDate());
		formEntryImpl.setUniqueReference(getUniqueReference());
		formEntryImpl.setFormInstanceRecordVersionId(
			getFormInstanceRecordVersionId());
		formEntryImpl.setPlid(getPlid());
		formEntryImpl.setStatus(getStatus());

		formEntryImpl.resetOriginalValues();

		return formEntryImpl;
	}

	@Override
	public FormEntry cloneWithOriginalValues() {
		FormEntryImpl formEntryImpl = new FormEntryImpl();

		formEntryImpl.setUuid(this.<String>getColumnOriginalValue("uuid_"));
		formEntryImpl.setFormEntryId(
			this.<Long>getColumnOriginalValue("formEntryId"));
		formEntryImpl.setGroupId(this.<Long>getColumnOriginalValue("groupId"));
		formEntryImpl.setCompanyId(
			this.<Long>getColumnOriginalValue("companyId"));
		formEntryImpl.setUserId(this.<Long>getColumnOriginalValue("userId"));
		formEntryImpl.setUserName(
			this.<String>getColumnOriginalValue("userName"));
		formEntryImpl.setCreateDate(
			this.<Date>getColumnOriginalValue("createDate"));
		formEntryImpl.setModifiedDate(
			this.<Date>getColumnOriginalValue("modifiedDate"));
		formEntryImpl.setUniqueReference(
			this.<String>getColumnOriginalValue("uniqueReference"));
		formEntryImpl.setFormInstanceRecordVersionId(
			this.<Long>getColumnOriginalValue("formInstanceRecordVersionId"));
		formEntryImpl.setPlid(this.<Long>getColumnOriginalValue("plid"));
		formEntryImpl.setStatus(this.<Integer>getColumnOriginalValue("status"));

		return formEntryImpl;
	}

	@Override
	public int compareTo(FormEntry formEntry) {
		int value = 0;

		if (getFormEntryId() < formEntry.getFormEntryId()) {
			value = -1;
		}
		else if (getFormEntryId() > formEntry.getFormEntryId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FormEntry)) {
			return false;
		}

		FormEntry formEntry = (FormEntry)object;

		long primaryKey = formEntry.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isEntityCacheEnabled() {
		return true;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isFinderCacheEnabled() {
		return true;
	}

	@Override
	public void resetOriginalValues() {
		_columnOriginalValues = Collections.emptyMap();

		_setModifiedDate = false;

		_columnBitmask = 0;
	}

	@Override
	public CacheModel<FormEntry> toCacheModel() {
		FormEntryCacheModel formEntryCacheModel = new FormEntryCacheModel();

		formEntryCacheModel.uuid = getUuid();

		String uuid = formEntryCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			formEntryCacheModel.uuid = null;
		}

		formEntryCacheModel.formEntryId = getFormEntryId();

		formEntryCacheModel.groupId = getGroupId();

		formEntryCacheModel.companyId = getCompanyId();

		formEntryCacheModel.userId = getUserId();

		formEntryCacheModel.userName = getUserName();

		String userName = formEntryCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			formEntryCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			formEntryCacheModel.createDate = createDate.getTime();
		}
		else {
			formEntryCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			formEntryCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			formEntryCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		formEntryCacheModel.uniqueReference = getUniqueReference();

		String uniqueReference = formEntryCacheModel.uniqueReference;

		if ((uniqueReference != null) && (uniqueReference.length() == 0)) {
			formEntryCacheModel.uniqueReference = null;
		}

		formEntryCacheModel.formInstanceRecordVersionId =
			getFormInstanceRecordVersionId();

		formEntryCacheModel.plid = getPlid();

		formEntryCacheModel.status = getStatus();

		return formEntryCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<FormEntry, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 2);

		sb.append("{");

		for (Map.Entry<String, Function<FormEntry, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<FormEntry, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("\"");
			sb.append(attributeName);
			sb.append("\": ");

			Object value = attributeGetterFunction.apply((FormEntry)this);

			if (value == null) {
				sb.append("null");
			}
			else if (value instanceof Blob || value instanceof Date ||
					 value instanceof Map || value instanceof String) {

				sb.append(
					"\"" + StringUtil.replace(value.toString(), "\"", "'") +
						"\"");
			}
			else {
				sb.append(value);
			}

			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, FormEntry>
			_escapedModelProxyProviderFunction =
				ProxyUtil.getProxyProviderFunction(
					FormEntry.class, ModelWrapper.class);

	}

	private String _uuid;
	private long _formEntryId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _setModifiedDate;
	private String _uniqueReference;
	private long _formInstanceRecordVersionId;
	private long _plid;
	private int _status;

	public <T> T getColumnValue(String columnName) {
		columnName = _attributeNames.getOrDefault(columnName, columnName);

		Function<FormEntry, Object> function =
			AttributeGetterFunctionsHolder._attributeGetterFunctions.get(
				columnName);

		if (function == null) {
			throw new IllegalArgumentException(
				"No attribute getter function found for " + columnName);
		}

		return (T)function.apply((FormEntry)this);
	}

	public <T> T getColumnOriginalValue(String columnName) {
		if (_columnOriginalValues == null) {
			return null;
		}

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		return (T)_columnOriginalValues.get(columnName);
	}

	private void _setColumnOriginalValues() {
		_columnOriginalValues = new HashMap<String, Object>();

		_columnOriginalValues.put("uuid_", _uuid);
		_columnOriginalValues.put("formEntryId", _formEntryId);
		_columnOriginalValues.put("groupId", _groupId);
		_columnOriginalValues.put("companyId", _companyId);
		_columnOriginalValues.put("userId", _userId);
		_columnOriginalValues.put("userName", _userName);
		_columnOriginalValues.put("createDate", _createDate);
		_columnOriginalValues.put("modifiedDate", _modifiedDate);
		_columnOriginalValues.put("uniqueReference", _uniqueReference);
		_columnOriginalValues.put(
			"formInstanceRecordVersionId", _formInstanceRecordVersionId);
		_columnOriginalValues.put("plid", _plid);
		_columnOriginalValues.put("status", _status);
	}

	private static final Map<String, String> _attributeNames;

	static {
		Map<String, String> attributeNames = new HashMap<>();

		attributeNames.put("uuid_", "uuid");

		_attributeNames = Collections.unmodifiableMap(attributeNames);
	}

	private transient Map<String, Object> _columnOriginalValues;

	public static long getColumnBitmask(String columnName) {
		return _columnBitmasks.get(columnName);
	}

	private static final Map<String, Long> _columnBitmasks;

	static {
		Map<String, Long> columnBitmasks = new HashMap<>();

		columnBitmasks.put("uuid_", 1L);

		columnBitmasks.put("formEntryId", 2L);

		columnBitmasks.put("groupId", 4L);

		columnBitmasks.put("companyId", 8L);

		columnBitmasks.put("userId", 16L);

		columnBitmasks.put("userName", 32L);

		columnBitmasks.put("createDate", 64L);

		columnBitmasks.put("modifiedDate", 128L);

		columnBitmasks.put("uniqueReference", 256L);

		columnBitmasks.put("formInstanceRecordVersionId", 512L);

		columnBitmasks.put("plid", 1024L);

		columnBitmasks.put("status", 2048L);

		_columnBitmasks = Collections.unmodifiableMap(columnBitmasks);
	}

	private long _columnBitmask;
	private FormEntry _escapedModel;

}