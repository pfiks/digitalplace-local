/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing FormEntry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FormEntryCacheModel
	implements CacheModel<FormEntry>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FormEntryCacheModel)) {
			return false;
		}

		FormEntryCacheModel formEntryCacheModel = (FormEntryCacheModel)object;

		if (formEntryId == formEntryCacheModel.formEntryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, formEntryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", formEntryId=");
		sb.append(formEntryId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", uniqueReference=");
		sb.append(uniqueReference);
		sb.append(", formInstanceRecordVersionId=");
		sb.append(formInstanceRecordVersionId);
		sb.append(", plid=");
		sb.append(plid);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FormEntry toEntityModel() {
		FormEntryImpl formEntryImpl = new FormEntryImpl();

		if (uuid == null) {
			formEntryImpl.setUuid("");
		}
		else {
			formEntryImpl.setUuid(uuid);
		}

		formEntryImpl.setFormEntryId(formEntryId);
		formEntryImpl.setGroupId(groupId);
		formEntryImpl.setCompanyId(companyId);
		formEntryImpl.setUserId(userId);

		if (userName == null) {
			formEntryImpl.setUserName("");
		}
		else {
			formEntryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			formEntryImpl.setCreateDate(null);
		}
		else {
			formEntryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			formEntryImpl.setModifiedDate(null);
		}
		else {
			formEntryImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (uniqueReference == null) {
			formEntryImpl.setUniqueReference("");
		}
		else {
			formEntryImpl.setUniqueReference(uniqueReference);
		}

		formEntryImpl.setFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
		formEntryImpl.setPlid(plid);
		formEntryImpl.setStatus(status);

		formEntryImpl.resetOriginalValues();

		return formEntryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		formEntryId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		uniqueReference = objectInput.readUTF();

		formInstanceRecordVersionId = objectInput.readLong();

		plid = objectInput.readLong();

		status = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(formEntryId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (uniqueReference == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uniqueReference);
		}

		objectOutput.writeLong(formInstanceRecordVersionId);

		objectOutput.writeLong(plid);

		objectOutput.writeInt(status);
	}

	public String uuid;
	public long formEntryId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String uniqueReference;
	public long formInstanceRecordVersionId;
	public long plid;
	public int status;

}