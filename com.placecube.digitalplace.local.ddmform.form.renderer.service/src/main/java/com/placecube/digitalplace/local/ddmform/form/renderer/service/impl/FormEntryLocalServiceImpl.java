/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.service.impl;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordVersionLocalService;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.exception.NoSuchFormEntryException;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.base.FormEntryLocalServiceBaseImpl;

/**
 * The implementation of the FormEntry local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>uk.gov.ccew.rsi.service.FormEntryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry", service = AopService.class)
public class FormEntryLocalServiceImpl extends FormEntryLocalServiceBaseImpl {

	private static final Log LOG = LogFactoryUtil.getLog(FormEntryLocalServiceImpl.class);

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private DDMFormInstanceRecordVersionLocalService ddmFormInstanceRecordVersionLocalService;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>uk.gov.ccew.rsi.service.FormEntryLocalService</code> via injection
	 * or a <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>uk.gov.ccew.rsi.service.FormEntryLocalServiceUtil</code>.
	 */
	@Override
	public FormEntry addFormEntry(long userId, long plid, long formInstanceRecordVersionId, int status, ServiceContext serviceContext) throws PortalException {
		return addFormEntry(userId, plid, formInstanceRecordVersionId, generateUniqueReference(), status, serviceContext);
	}

	@Override
	public FormEntry addFormEntry(long userId, long plid, long formInstanceRecordVersionId, String uniqueReference, int status, ServiceContext serviceContext) throws PortalException {
		validate(userId, formInstanceRecordVersionId);

		Date now = new Date();
		User user = userLocalService.getUserById(userId);
		long formEntryId = counterLocalService.increment();
		FormEntry formEntry = formEntryPersistence.create(formEntryId);

		formEntry.setGroupId(serviceContext.getScopeGroupId());
		formEntry.setCompanyId(user.getCompanyId());
		formEntry.setUserId(userId);
		formEntry.setUserName(user.getFullName());
		formEntry.setCreateDate(now);
		formEntry.setModifiedDate(now);
		formEntry.setModifiedDate(now);

		formEntry.setUniqueReference(uniqueReference);
		formEntry.setFormInstanceRecordVersionId(formInstanceRecordVersionId);
		formEntry.setStatus(status);
		formEntry.setPlid(plid);

		return formEntryPersistence.update(formEntry);
	}

	@Override
	public FormEntry deleteFormEntry(FormEntry formEntry) {
		try {
			DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion = ddmFormInstanceRecordVersionLocalService.fetchDDMFormInstanceRecordVersion(formEntry.getFormInstanceRecordVersionId());
			if (Validator.isNotNull(ddmFormInstanceRecordVersion)) {
				ddmFormInstanceRecordLocalService.deleteDDMFormInstanceRecord(ddmFormInstanceRecordVersion.getFormInstanceRecord());
			}

			return formEntryLocalService.deleteFormEntry(formEntry.getFormEntryId());
		} catch (PortalException e) {
			LOG.error("Error during remove formEntryId: " + formEntry.getFormEntryId(), e);
		}
		
		return formEntry;
		
	}

	@Override
	public List<FormEntry> getFormEntriesByUniqueReferenceAndStatus(String uniqueReference, int status) {
		return formEntryPersistence.findByUniqueReferenceAndStatus(uniqueReference, status);
	}

	@Override
	public List<FormEntry> getFormEntriesModifiedBeforeDate(long companyId, long plid, Date dateBefore) {
		DynamicQuery expiryDateQuery = formEntryLocalService.dynamicQuery();
		expiryDateQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		expiryDateQuery.add(RestrictionsFactoryUtil.eq("plid", plid));
		expiryDateQuery.add(RestrictionsFactoryUtil.lt("modifiedDate", dateBefore));

		return formEntryLocalService.dynamicQuery(expiryDateQuery);
	}

	@Override
	public FormEntry getFormEntryByFormInstanceRecordVersionId(long formInstanceRecordVersionId) throws NoSuchFormEntryException {
		return formEntryPersistence.findByFormInstanceRecordVersionId(formInstanceRecordVersionId);
	}

	@Override
	public FormEntry getFormEntryByFormInstanceRecordVersionIdAndStatus(long formInstanceRecordVersionId, int status) throws NoSuchFormEntryException {
		return formEntryPersistence.findByFormInstanceRecordVersionIdAndStatus(formInstanceRecordVersionId, status);
	}

	@Override
	public FormEntry updateFormEntry(FormEntry formEntry, int status) {
		formEntry.setModifiedDate(new Date());
		formEntry.setStatus(status);

		return formEntryPersistence.update(formEntry);
	}

	protected void validate(long userId, long formInstanceRecordVersionId) throws PortalException {
		if (userId <= 0) {
			throw new PortalException("Invalid user ID");
		}
		if (formInstanceRecordVersionId <= 0) {
			throw new PortalException("Invalid form instance record ID");
		}
	}

	private String generateUniqueReference() {
		return PortalUUIDUtil.generate();
	}
}