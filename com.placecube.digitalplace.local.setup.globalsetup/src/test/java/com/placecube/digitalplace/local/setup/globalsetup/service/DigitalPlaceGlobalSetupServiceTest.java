package com.placecube.digitalplace.local.setup.globalsetup.service;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import com.placecube.digitalplace.local.webcontent.webdocs.constants.WebdocsDDMTemplate;
import com.placecube.digitalplace.local.webcontent.webdocs.service.WebdocsWebContentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.placecube.cookieconsent.service.CookieConsentConfigurationService;
import com.placecube.digitalplace.local.assetpublisher.constants.AssetPublisherWidgetTemplate;
import com.placecube.digitalplace.local.assetpublisher.service.AssetPublisherWebContentService;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.search.constants.SearchWidgetTemplate;
import com.placecube.digitalplace.local.search.service.SearchService;
import com.placecube.digitalplace.local.setup.globalsetup.constants.DigitalPlaceGlobalSetupWebContent;
import com.placecube.digitalplace.local.webcontent.accordion.service.AccordionWebContentService;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleDDMTemplate;
import com.placecube.digitalplace.local.webcontent.article.constants.WidgetTemplate;
import com.placecube.digitalplace.local.webcontent.article.service.ArticleWebContentService;
import com.placecube.digitalplace.local.webcontent.formconfirmation.service.FormConfirmationWebContentService;
import com.placecube.digitalplace.local.webcontent.formerrorpayment.service.FormErrorPaymentWebContentService;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsWidgetTemplate;
import com.placecube.digitalplace.local.webcontent.news.service.NewsWebContentService;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
public class DigitalPlaceGlobalSetupServiceTest extends PowerMockito {
	private static final long ID = 1;
	@InjectMocks
	private DigitalPlaceGlobalSetupService digitalPlaceGlobalSetupService;
	@Mock
	private AccordionWebContentService mockAccordionWebContentService;
	@Mock
	private ArticleWebContentService mockArticleWebContentService;
	@Mock
	private AssetPublisherWebContentService mockAssetPublisherWebContentService;
	@Mock
	private CategoryNavigationService mockCategoryNavigationService;
	@Mock
	private CookieConsentConfigurationService mockCookieConsentConfigurationService;
	@Mock
	private DDMStructure mockDDMStructure1;
	@Mock
	private DDMStructure mockDDMStructure2;
	@Mock
	private DigitalPlaceGlobalSetupWebContentService mockDigitalPlaceGlobalSetupWebContentService;
	@Mock
	private EmailWebContentService mockEmailWebContentService;
	@Mock
	private FormConfirmationWebContentService mockFormConfirmationWebContentService;
	@Mock
	private FormErrorPaymentWebContentService mockFormErrorPaymentWebContentService;
	@Mock
	private GuideWebContentService mockGuideWebContentService;
	@Mock
	private NavItemTemplateService mockNavItemTemplateService;
	@Mock
	private NewsWebContentService mockNewsWebContentService;
	@Mock
	private Portal mockPortal;
	@Mock
	private Dictionary mockPropertyDictionary;
	@Mock
	private SearchService mockSearchService;
	@Mock
	private ServiceContext mockServiceContext;
	@Mock
	private WebdocsWebContentService mockWebdocsWebContentService;
	@Mock
	private ServicesAssetVocabularyService mockServicesAssetVocabularyService;
	@Mock
	private UnicodeProperties mockUnicodeProperties;
	@Before
	public void activateSetUp() {
		mockStatic(ServiceContextThreadLocal.class);
	}
	@Test(expected = PortalException.class)
	public void configureCookieConsentWidget_WhenErrorRetrievingPropertyDictionary_ThenThrowsPortalException() throws PortalException {
		when(mockCookieConsentConfigurationService.getPropertyDictionary(anyLong())).thenThrow(new ConfigurationException());
		digitalPlaceGlobalSetupService.configureCookieConsentWidget(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void configureCookieConsentWidget_WhenErrorSettingWebContentArticleIdConfiguration_ThenThrowsPortalException() throws PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(ID);
		when(mockCookieConsentConfigurationService.getPropertyDictionary(ID)).thenReturn(mockPropertyDictionary);
		doThrow(new ConfigurationException()).when(mockCookieConsentConfigurationService).setConsentBannerArticleId(ID, DigitalPlaceGlobalSetupWebContent.COOKIE_CONSENT_BANNER.getArticleId(),
				mockPropertyDictionary);
		digitalPlaceGlobalSetupService.configureCookieConsentWidget(mockServiceContext);
	}
	@Test
	public void configureCookieConsentWidget_WhenNoError_ThenConsentBannerArticleIdIsSet() throws PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(ID);
		when(mockCookieConsentConfigurationService.getPropertyDictionary(ID)).thenReturn(mockPropertyDictionary);
		digitalPlaceGlobalSetupService.configureCookieConsentWidget(mockServiceContext);
		verify(mockCookieConsentConfigurationService, times(1)).setConsentBannerArticleId(ID, DigitalPlaceGlobalSetupWebContent.COOKIE_CONSENT_BANNER.getArticleId(), mockPropertyDictionary);
	}
	@Test(expected = PortalException.class)
	public void initializeAssetVocabularies_WhenErrorGettingOrCreatingVocabulary_ThenThrowsPortalException() throws Exception {
		Long journalClassNameId = 123L;
		Long layoutClassNameId = 456L;
		Long articleStructureId = 789L;
		Long guideStructureId = 1011L;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalClassNameId);
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(layoutClassNameId);
		when(mockArticleWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure1);
		when(mockDDMStructure1.getStructureId()).thenReturn(articleStructureId);
		when(mockGuideWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure2);
		when(mockDDMStructure2.getStructureId()).thenReturn(guideStructureId);
		Map<Long, Set<Long>> requiredClassNameIds = new HashMap<>();
		Set<Long> webContentStructureIds = new LinkedHashSet<>();
		webContentStructureIds.add(articleStructureId);
		webContentStructureIds.add(guideStructureId);
		requiredClassNameIds.put(journalClassNameId, webContentStructureIds);
		Map<Long, Set<Long>> selectedClassNameIds = new HashMap<>(requiredClassNameIds);
		selectedClassNameIds.put(layoutClassNameId, Collections.emptySet());
		when(mockServicesAssetVocabularyService.getOrCreateVocabulary(mockServiceContext, selectedClassNameIds, requiredClassNameIds)).thenThrow(new PortalException());
		digitalPlaceGlobalSetupService.initializeAssetVocabularies(mockServiceContext);
	}
	@Test
	public void initializeAssetVocabularies_WhenNoError_ThenGetsOrCreatesVocabulary() throws Exception {
		Long journalClassNameId = 123L;
		Long layoutClassNameId = 456L;
		Long articleStructureId = 789L;
		Long guideStructureId = 1011L;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalClassNameId);
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(layoutClassNameId);
		when(mockArticleWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure1);
		when(mockDDMStructure1.getStructureId()).thenReturn(articleStructureId);
		when(mockGuideWebContentService.getOrCreateDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure2);
		when(mockDDMStructure2.getStructureId()).thenReturn(guideStructureId);
		Map<Long, Set<Long>> requiredClassNameIds = new HashMap<>();
		Set<Long> webContentStructureIds = new LinkedHashSet<>();
		webContentStructureIds.add(articleStructureId);
		webContentStructureIds.add(guideStructureId);
		requiredClassNameIds.put(journalClassNameId, webContentStructureIds);
		Map<Long, Set<Long>> selectedClassNameIds = new HashMap<>(requiredClassNameIds);
		selectedClassNameIds.put(layoutClassNameId, Collections.emptySet());
		digitalPlaceGlobalSetupService.initializeAssetVocabularies(mockServiceContext);
		verify(mockServicesAssetVocabularyService, times(1)).getOrCreateVocabulary(mockServiceContext, selectedClassNameIds, requiredClassNameIds);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContent_WhenErrorCreatingCookieConsentWebContent_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupWebContentService).getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.COOKIE_CONSENT_BANNER, mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContent(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContent_WhenErrorCreatingEmailAddressVerifiedWebContent_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupWebContentService).getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.EMAIL_ADDRESS_VERIFIED_CONTENT,
				mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContent(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContent_WhenErrorCreatingSignInContentWebContent_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupWebContentService).getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.SIGN_IN_CONTENT, mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContent(mockServiceContext);
	}
	@Test
	public void initializeWebContent_WhenNoError_ThenGetOrCreateBasicWebContentDesired() throws Exception {
		digitalPlaceGlobalSetupService.initializeWebContent(mockServiceContext);
		InOrder inOrder = inOrder(mockDigitalPlaceGlobalSetupWebContentService);
		inOrder.verify(mockDigitalPlaceGlobalSetupWebContentService, times(1)).getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.COOKIE_CONSENT_BANNER, mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupWebContentService, times(1)).getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.EMAIL_ADDRESS_VERIFIED_CONTENT, mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupWebContentService, times(1)).getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.SIGN_IN_CONTENT, mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentStructures_WhenErrorCallingFormConfirmationWebContentService_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockFormConfirmationWebContentService).getOrCreateDDMStructure(mockServiceContext, GroupConstants.GLOBAL);
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentStructures_WhenErrorCallingFormErrorPaymentWebContentService_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockFormErrorPaymentWebContentService).getOrCreateDDMStructure(mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentStructures_WhenErrorCallingGuideWebContentService_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockGuideWebContentService).getOrCreateDDMStructure(mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentStructures_WhenErrorCallingNewsWebContentService_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockNewsWebContentService).getOrCreateDDMStructure(mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentStructures_WhenErrorCallingsArticleWebContentService_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockArticleWebContentService).getOrCreateDDMStructure(mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentStructures_WhenErrorCallingsEmailWebContentService_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockEmailWebContentService).getOrCreateDDMStructure(mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
	}
	@Test
	public void initializeWebContentStructures_WhenNoError_ThenGetOrCreateDDMStructuresDesired() throws Exception {
		digitalPlaceGlobalSetupService.initializeWebContentStructures(mockServiceContext);
		InOrder inOrder = inOrder(mockAccordionWebContentService, mockArticleWebContentService, mockEmailWebContentService, mockFormConfirmationWebContentService,
		mockFormErrorPaymentWebContentService, mockGuideWebContentService, mockNewsWebContentService, mockWebdocsWebContentService);
		inOrder.verify(mockAccordionWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
		inOrder.verify(mockArticleWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
		inOrder.verify(mockFormConfirmationWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext, GroupConstants.GLOBAL);
		inOrder.verify(mockGuideWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
		inOrder.verify(mockNewsWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
		inOrder.verify(mockEmailWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
		inOrder.verify(mockFormErrorPaymentWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
		inOrder.verify(mockWebdocsWebContentService, times(1)).getOrCreateDDMStructure(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentTemplates_WhenErrorCreatingGuideContentTemplate_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockGuideWebContentService).getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_CONTENT, GroupConstants.GLOBAL);
		digitalPlaceGlobalSetupService.initializeWebContentTemplates(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWebContentTemplates_WhenErrorCreatingGuideNavigationTemplate_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockGuideWebContentService).getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_NAVIGATION, GroupConstants.GLOBAL);
		digitalPlaceGlobalSetupService.initializeWebContentTemplates(mockServiceContext);
	}
	@Test
	public void initializeWebContentTemplates_WhenNoError_ThenGetOrCreateDDMTemplateDesired() throws Exception {
		digitalPlaceGlobalSetupService.initializeWebContentTemplates(mockServiceContext);
		InOrder inOrder = inOrder(mockGuideWebContentService, mockWebdocsWebContentService);
		inOrder.verify(mockGuideWebContentService, times(1)).getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_CONTENT, GroupConstants.GLOBAL);
		inOrder.verify(mockGuideWebContentService, times(1)).getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_NAVIGATION, GroupConstants.GLOBAL);
		inOrder.verify(mockWebdocsWebContentService, times(1)).getOrCreateDDMTemplate(mockServiceContext, WebdocsDDMTemplate.WEBDOCS_TITLE);
		inOrder.verify(mockWebdocsWebContentService, times(1)).getOrCreateDDMTemplate(mockServiceContext, WebdocsDDMTemplate.WEBDOCS_NAVIGATION);
		inOrder.verify(mockWebdocsWebContentService, times(1)).getOrCreateDDMTemplate(mockServiceContext, WebdocsDDMTemplate.WEBDOCS_CONTENT);
	}
	@Test(expected = PortalException.class)
	public void initializeWidgetTemplates_WhenErrorCreatingWidgetTemplateFeaturedContentNavPanels_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockArticleWebContentService).getOrCreateWidgetTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS, mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWidgetTemplates(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWidgetTemplates_WhenErrorCreatingWidgetTemplateFeaturedContentNavPanelsLarge_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockArticleWebContentService).getOrCreateWidgetTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS_LARGE, mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWidgetTemplates(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWidgetTemplates_WhenErrorCreatingWidgetTemplateNewsCarousel_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockNewsWebContentService).getOrCreateWidgetTemplate(NewsWidgetTemplate.NEWS_CAROUSEL, mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWidgetTemplates(mockServiceContext);
	}
	@Test(expected = PortalException.class)
	public void initializeWidgetTemplates_WhenErrorCreatingWidgetTemplateSearchList_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockSearchService).getOrCreateWidgetTemplate(SearchWidgetTemplate.SEARCH_LIST, mockServiceContext);
		digitalPlaceGlobalSetupService.initializeWidgetTemplates(mockServiceContext);
	}
	@Test
	public void initializeWidgetTemplates_WhenNoError_ThenGetOrCreateWidgetTemplatesDesiredAndArticleDDMTemplate() throws Exception {
		digitalPlaceGlobalSetupService.initializeWidgetTemplates(mockServiceContext);
		InOrder inOrder = inOrder(mockNavItemTemplateService, mockArticleWebContentService, mockCategoryNavigationService, mockAssetPublisherWebContentService, mockNewsWebContentService,
				mockSearchService);
		inOrder.verify(mockArticleWebContentService, times(1)).getOrCreateWidgetTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS, mockServiceContext);
		inOrder.verify(mockArticleWebContentService, times(1)).getOrCreateWidgetTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS_LARGE, mockServiceContext);
		inOrder.verify(mockArticleWebContentService, times(1)).getOrCreateWidgetTemplate(WidgetTemplate.RELATED_CONTENT_LIST, mockServiceContext);
		inOrder.verify(mockArticleWebContentService, times(1)).getOrCreateDDMTemplate(ArticleDDMTemplate.ARTICLE, mockServiceContext);
		inOrder.verify(mockCategoryNavigationService, times(1)).getOrCreateWidgetTemplate(CategoryNavigationWidgetTemplate.SERVICE_MENU, mockServiceContext);
		inOrder.verify(mockCategoryNavigationService, times(1)).getOrCreateWidgetTemplate(CategoryNavigationWidgetTemplate.CATEGORY_NAV_LINKS, mockServiceContext);
		inOrder.verify(mockNavItemTemplateService, times(1)).getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.MY_ACCOUNT, mockServiceContext);
		inOrder.verify(mockArticleWebContentService, times(1)).getOrCreateWidgetTemplate(WidgetTemplate.CATEGORY_CONTENT_LIST, mockServiceContext);
		inOrder.verify(mockNavItemTemplateService, times(1)).getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_LINK_PANELS, mockServiceContext);
		inOrder.verify(mockNavItemTemplateService, times(1)).getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_PAGE_CATEGORY_LINK, mockServiceContext);
		inOrder.verify(mockNavItemTemplateService, times(1)).getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_COLLAPSIBLE_BOX, mockServiceContext);
		inOrder.verify(mockNavItemTemplateService, times(1)).getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_POPULAR_PAGES_WEB_CONTENT_NAV_PANELS, mockServiceContext);
		inOrder.verify(mockNewsWebContentService, times(1)).getOrCreateWidgetTemplate(NewsWidgetTemplate.NEWS_CAROUSEL, mockServiceContext);
		inOrder.verify(mockAssetPublisherWebContentService, times(1)).getOrCreateWidgetTemplate(AssetPublisherWidgetTemplate.CONTENT_LINK_PANELS, mockServiceContext);
		inOrder.verify(mockSearchService, times(1)).getOrCreateWidgetTemplate(SearchWidgetTemplate.SEARCH_LIST, mockServiceContext);
	}
}