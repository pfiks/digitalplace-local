package com.placecube.digitalplace.local.setup.globalsetup.service;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.setup.globalsetup.constants.DigitalPlaceGlobalSetupWebContent;
import com.placecube.journal.service.JournalArticleCreationService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DigitalPlaceGlobalSetupWebContentServiceTest extends PowerMockito {

	@InjectMocks
	private DigitalPlaceGlobalSetupWebContentService digitalPlaceGlobalSetupWebContentService;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetUp() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "COOKIE_CONSENT_BANNER", "SIGN_IN_CONTENT" })
	public void getOrCreateBasicWebContent_WhenExceptionCreatingArticle_ThenThrowsException(String webContent) throws Exception {
		DigitalPlaceGlobalSetupWebContent digitalPlaceWebContent = DigitalPlaceGlobalSetupWebContent.valueOf(webContent);
		String webContentArticleText = "webContentArticleText";
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/setup/globalsetup/dependencies/webcontent/" + digitalPlaceWebContent.getArticleId() + ".xml"))
				.thenReturn(webContentArticleText);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(digitalPlaceWebContent.getTitle(), mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockJournalArticleCreationService.getOrCreateBasicWebContentArticle(digitalPlaceWebContent.getArticleId(), digitalPlaceWebContent.getTitle(), webContentArticleText, mockJournalFolder,
				mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceGlobalSetupWebContentService.getOrCreateBasicWebContent(digitalPlaceWebContent, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "COOKIE_CONSENT_BANNER", "SIGN_IN_CONTENT" })
	public void getOrCreateBasicWebContent_WhenExceptionCreatingArticleFolder_ThenThrowsException(String webContent) throws Exception {
		DigitalPlaceGlobalSetupWebContent digitalPlaceWebContent = DigitalPlaceGlobalSetupWebContent.valueOf(webContent);
		String webContentArticleText = "webContentArticleText";
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/setup/globalsetup/dependencies/webcontent/" + digitalPlaceWebContent.getArticleId() + ".xml"))
				.thenReturn(webContentArticleText);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(digitalPlaceWebContent.getTitle(), mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceGlobalSetupWebContentService.getOrCreateBasicWebContent(digitalPlaceWebContent, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "COOKIE_CONSENT_BANNER", "SIGN_IN_CONTENT" })
	public void getOrCreateBasicWebContent_WhenExceptionReadingContent_ThenThrowsException(String webContent) throws Exception {
		DigitalPlaceGlobalSetupWebContent digitalPlaceWebContent = DigitalPlaceGlobalSetupWebContent.valueOf(webContent);
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/setup/globalsetup/dependencies/webcontent/" + digitalPlaceWebContent.getArticleId() + ".xml"))
				.thenThrow(new IOException());

		digitalPlaceGlobalSetupWebContentService.getOrCreateBasicWebContent(digitalPlaceWebContent, mockServiceContext);
	}

	@Test
	@Parameters({ "COOKIE_CONSENT_BANNER", "SIGN_IN_CONTENT" })
	public void getOrCreateBasicWebContent_WhenNoError_ThenCreatesTheWebContent(String webContent) throws Exception {
		DigitalPlaceGlobalSetupWebContent digitalPlaceGlobalSetupWebContent = DigitalPlaceGlobalSetupWebContent.valueOf(webContent);
		String webContentArticleText = "webContentArticleText";
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream resourceAsStream = classLoader.getResourceAsStream("com/placecube/digitalplace/local/setup/globalsetup/dependencies/webcontent/" + digitalPlaceGlobalSetupWebContent.getArticleId() + ".xml");

		when(StringUtil.read(classLoader, "com/placecube/digitalplace/local/setup/globalsetup/dependencies/webcontent/" + digitalPlaceGlobalSetupWebContent.getArticleId() + ".xml"))
				.thenReturn(webContentArticleText);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(digitalPlaceGlobalSetupWebContent.getTitle(), mockServiceContext)).thenReturn(mockJournalFolder);

		digitalPlaceGlobalSetupWebContentService.getOrCreateBasicWebContent(digitalPlaceGlobalSetupWebContent, mockServiceContext);

		verify(mockJournalArticleCreationService, times(1)).getOrCreateBasicWebContentArticle(digitalPlaceGlobalSetupWebContent.getArticleId(), digitalPlaceGlobalSetupWebContent.getTitle(),
				webContentArticleText, mockJournalFolder, mockServiceContext);
		assertThat(resourceAsStream, is(notNullValue()));
	}

}
