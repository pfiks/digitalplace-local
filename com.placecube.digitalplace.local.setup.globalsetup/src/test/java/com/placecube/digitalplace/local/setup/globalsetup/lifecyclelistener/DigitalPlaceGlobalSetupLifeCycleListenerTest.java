package com.placecube.digitalplace.local.setup.globalsetup.lifecyclelistener;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.local.setup.globalsetup.service.DigitalPlaceGlobalSetupService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class })
public class DigitalPlaceGlobalSetupLifeCycleListenerTest extends PowerMockito {

	private static final long COMPANY_ID = 3434L;

	@Mock
	private Company mockCompany;

	@InjectMocks
	private DigitalPlaceGlobalSetupLifeCycleListener digitalPlaceGlobalSetupLifeCycleListener;

	@Mock
	private DigitalPlaceGlobalSetupService mockDigitalPlaceGlobalSetupService;

	@Mock
	private Group mockGroup;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetUp() {
		mockStatic(PortalUtil.class);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenErrorConfigureCookieConsentWidget_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupService).configureCookieConsentWidget(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenErrorInitializingAssetVocabularies_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupService).initializeAssetVocabularies(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenErrorInitializingWebContent_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupService).initializeWebContent(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenErrorInitializingWebContentStructures_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupService).initializeWebContentStructures(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenErrorInitializingWebContentTemplates_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupService).initializeWebContentTemplates(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenErrorInitializingWidgetTemplates_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

		doThrow(new PortalException()).when(mockDigitalPlaceGlobalSetupService).initializeWidgetTemplates(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenInitializesTheResources() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockDigitalPlaceGlobalSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);

		digitalPlaceGlobalSetupLifeCycleListener.portalInstanceRegistered(mockCompany);

		InOrder inOrder = inOrder(mockDigitalPlaceGlobalSetupService, mockServiceContext);

		inOrder.verify(mockDigitalPlaceGlobalSetupService, times(1)).initializeWebContentStructures(mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupService, times(1)).initializeWebContentTemplates(mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupService, times(1)).initializeWebContent(mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupService, times(1)).initializeWidgetTemplates(mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupService, times(1)).initializeAssetVocabularies(mockServiceContext);
		inOrder.verify(mockDigitalPlaceGlobalSetupService, times(1)).configureCookieConsentWidget(mockServiceContext);

	}

}
