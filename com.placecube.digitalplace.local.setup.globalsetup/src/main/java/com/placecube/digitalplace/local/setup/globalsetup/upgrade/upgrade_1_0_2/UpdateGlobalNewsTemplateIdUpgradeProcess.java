package com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_2;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

public class UpdateGlobalNewsTemplateIdUpgradeProcess extends UpgradeProcess {

	private static final String GLOBAL_NEWS_KEY = "GLOBAL-NEWS";

	private static final String NEWS_KEY = "NEWS";

	private final CompanyLocalService companyLocalService;

	private final DDMStructureLocalService ddmStructureLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private final GroupLocalService groupLocalService;

	private final JournalArticleLocalService journalArticleLocalService;

	private final Portal portal;

	public UpdateGlobalNewsTemplateIdUpgradeProcess(CompanyLocalService companyLocalService, DDMStructureLocalService ddmStructureLocalService, DDMTemplateLocalService ddmTemplateLocalService,
			GroupLocalService groupLocalService, JournalArticleLocalService journalArticleLocalService, Portal portal) {
		this.ddmStructureLocalService = ddmStructureLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
		this.groupLocalService = groupLocalService;
		this.companyLocalService = companyLocalService;
		this.journalArticleLocalService = journalArticleLocalService;
		this.portal = portal;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void createOrUpdateEmailTemplate(long globalGroupId) throws PortalException {
		DDMTemplate oldNewsTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), NEWS_KEY);
		DDMTemplate newNewsTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), GLOBAL_NEWS_KEY);

		if (Validator.isNotNull(oldNewsTemplate) && Validator.isNotNull(newNewsTemplate)) {
			ddmTemplateLocalService.deleteTemplate(newNewsTemplate);
		}

		if (Validator.isNotNull(oldNewsTemplate)) {
			oldNewsTemplate.setTemplateKey(GLOBAL_NEWS_KEY);
			oldNewsTemplate.setName(oldNewsTemplate.getName().replace("News", "Global News"));
			ddmTemplateLocalService.updateDDMTemplate(oldNewsTemplate);
		}
	}

	private void updateJournalArticlesWithEmailTemplate(long globalGroupId) {
		DDMStructure newsGlobalStructure = ddmStructureLocalService.fetchStructure(globalGroupId, portal.getClassNameId(JournalArticle.class), NEWS_KEY);

		if (Validator.isNotNull(newsGlobalStructure)) {
			DynamicQuery dynamicQuery = journalArticleLocalService.dynamicQuery();
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", globalGroupId));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("DDMStructureId", newsGlobalStructure.getStructureId()));
			List<JournalArticle> journalArticles = journalArticleLocalService.dynamicQuery(dynamicQuery);

			for (JournalArticle journalArticle : journalArticles) {
				journalArticle.setDDMTemplateKey(GLOBAL_NEWS_KEY);
				journalArticleLocalService.updateJournalArticle(journalArticle);
			}
		}
	}

	private void upgradeCompany(long companyId) throws PortalException {
		long globalGroupId = groupLocalService.fetchCompanyGroup(companyId).getGroupId();

		createOrUpdateEmailTemplate(globalGroupId);
		updateJournalArticlesWithEmailTemplate(globalGroupId);
	}

}
