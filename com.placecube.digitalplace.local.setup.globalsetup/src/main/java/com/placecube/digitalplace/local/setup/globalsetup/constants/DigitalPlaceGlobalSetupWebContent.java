package com.placecube.digitalplace.local.setup.globalsetup.constants;

public enum DigitalPlaceGlobalSetupWebContent {

	COOKIE_CONSENT_BANNER("COOKIE-CONSENT-BANNER", "Cookie consent banner"),

	EMAIL_ADDRESS_VERIFIED_CONTENT("EMAIL-ADDRESS-VERIFIED-CONTENT", "Email Address Verified Content"),

	SIGN_IN_CONTENT("SIGN-IN-CONTENT", "Sign In Content");

	private final String articleId;

	private final String title;

	DigitalPlaceGlobalSetupWebContent(String articleId, String title) {
		this.articleId = articleId;
		this.title = title;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getTitle() {
		return title;
	}

}
