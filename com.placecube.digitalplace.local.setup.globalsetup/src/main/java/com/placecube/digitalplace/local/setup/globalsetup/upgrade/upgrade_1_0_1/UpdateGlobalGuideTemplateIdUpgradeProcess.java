package com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_1;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;

public class UpdateGlobalGuideTemplateIdUpgradeProcess extends UpgradeProcess {

	private static final String GLOBAL_GUIDE_CONTENT_KEY = "GLOBAL-GUIDE-CONTENT";

	private static final String GLOBAL_GUIDE_NAVIGATION_KEY = "GLOBAL-GUIDE-NAVIGATION";

	private static final Log LOG = LogFactoryUtil.getLog(UpdateGlobalGuideTemplateIdUpgradeProcess.class);

	private final CompanyLocalService companyLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private final GroupLocalService groupLocalService;

	private final JournalArticleLocalService journalArticleLocalService;

	private final Portal portal;

	public UpdateGlobalGuideTemplateIdUpgradeProcess(CompanyLocalService companyLocalService, DDMTemplateLocalService ddmTemplateLocalService, GroupLocalService groupLocalService,
			JournalArticleLocalService journalArticleLocalService, Portal portal) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
		this.groupLocalService = groupLocalService;
		this.companyLocalService = companyLocalService;
		this.journalArticleLocalService = journalArticleLocalService;
		this.portal = portal;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void createOrUpdateTemplate(long globalGroupId, String currentKey, String globalKey, String currentName, String globalName) {
		DDMTemplate oldEmailTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), currentKey);
		DDMTemplate newEmailTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), globalKey);

		if (Validator.isNotNull(oldEmailTemplate) && Validator.isNotNull(newEmailTemplate)) {
			deleteTemplate(newEmailTemplate);
		}

		if (Validator.isNotNull(oldEmailTemplate)) {
			oldEmailTemplate.setTemplateKey(globalKey);
			oldEmailTemplate.setName(oldEmailTemplate.getName().replace(currentName, globalName));
			ddmTemplateLocalService.updateDDMTemplate(oldEmailTemplate);
		}
	}

	private void deleteTemplate(DDMTemplate newEmailTemplate) {
		try {
			ddmTemplateLocalService.deleteTemplate(newEmailTemplate);
		} catch (PortalException e) {
			LOG.warn(e);
		}
	}

	private void updateJournalArticles(long globalGroupId, String currentKey, String globalKey) {
		DynamicQuery dynamicQuery = journalArticleLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", globalGroupId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("DDMTemplateKey", currentKey));
		List<JournalArticle> journalArticles = journalArticleLocalService.dynamicQuery(dynamicQuery);

		for (JournalArticle journalArticle : journalArticles) {
			journalArticle.setDDMTemplateKey(globalKey);
			journalArticleLocalService.updateJournalArticle(journalArticle);
		}
	}

	private void upgradeCompany(long companyId) {
		long globalGroupId = groupLocalService.fetchCompanyGroup(companyId).getGroupId();

		createOrUpdateTemplate(globalGroupId, GuideDDMTemplate.GUIDE_CONTENT.getKey(), GLOBAL_GUIDE_CONTENT_KEY, "Guide Content", "Global Guide Content");
		updateJournalArticles(globalGroupId, GuideDDMTemplate.GUIDE_CONTENT.getKey(), GLOBAL_GUIDE_CONTENT_KEY);

		createOrUpdateTemplate(globalGroupId, GuideDDMTemplate.GUIDE_NAVIGATION.getKey(), GLOBAL_GUIDE_NAVIGATION_KEY, "Guide Navigation", "Global Guide Navigation");
		updateJournalArticles(globalGroupId, GuideDDMTemplate.GUIDE_NAVIGATION.getKey(), GLOBAL_GUIDE_NAVIGATION_KEY);
	}

}