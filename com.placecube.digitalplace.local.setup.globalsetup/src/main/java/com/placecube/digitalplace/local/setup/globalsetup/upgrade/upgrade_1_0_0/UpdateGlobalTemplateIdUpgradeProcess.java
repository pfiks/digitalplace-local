package com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_0;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

public class UpdateGlobalTemplateIdUpgradeProcess extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final DDMStructureLocalService ddmStructureLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private final GroupLocalService groupLocalService;

	private final JournalArticleLocalService journalArticleLocalService;

	private final Portal portal;

	private static final String EMAIL_KEY = "EMAIL";

	private static final String GLOBAL_EMAIL_KEY = "GLOBAL-EMAIL";

	public UpdateGlobalTemplateIdUpgradeProcess(CompanyLocalService companyLocalService, DDMStructureLocalService ddmStructureLocalService, DDMTemplateLocalService ddmTemplateLocalService, GroupLocalService groupLocalService, JournalArticleLocalService journalArticleLocalService, Portal portal) {
		this.ddmStructureLocalService = ddmStructureLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
		this.groupLocalService = groupLocalService;
		this.companyLocalService = companyLocalService;
		this.journalArticleLocalService = journalArticleLocalService;
		this.portal = portal;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws PortalException {
		long globalGroupId = groupLocalService.fetchCompanyGroup(companyId).getGroupId();

		createOrUpdateEmailTemplate(globalGroupId);
		updateJournalArticlesWithEmailTemplate(globalGroupId);
	}

	private void createOrUpdateEmailTemplate(long globalGroupId) throws PortalException {
		DDMTemplate oldEmailTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), EMAIL_KEY);
		DDMTemplate newEmailTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), GLOBAL_EMAIL_KEY);

		if (Validator.isNotNull(oldEmailTemplate) && Validator.isNotNull(newEmailTemplate)) {
			ddmTemplateLocalService.deleteTemplate(newEmailTemplate);
		}

		oldEmailTemplate.setTemplateKey(GLOBAL_EMAIL_KEY);
		oldEmailTemplate.setName(oldEmailTemplate.getName().replace("Email", "Global Email"));
		ddmTemplateLocalService.updateDDMTemplate(oldEmailTemplate);
	}

	private void updateJournalArticlesWithEmailTemplate(long globalGroupId) {
		DDMStructure emailGlobalStructure = ddmStructureLocalService.fetchStructure(globalGroupId, portal.getClassNameId(JournalArticle.class), EMAIL_KEY);

		DynamicQuery dynamicQuery = journalArticleLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", globalGroupId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("DDMStructureId", emailGlobalStructure.getStructureId()));
		List<JournalArticle> journalArticles = journalArticleLocalService.dynamicQuery(dynamicQuery);

		for (JournalArticle journalArticle : journalArticles) {
			journalArticle.setDDMTemplateKey(GLOBAL_EMAIL_KEY);
			journalArticleLocalService.updateJournalArticle(journalArticle);
		}
	}

}
