package com.placecube.digitalplace.local.setup.globalsetup.service;

import java.io.IOException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.setup.globalsetup.constants.DigitalPlaceGlobalSetupWebContent;
import com.placecube.journal.service.JournalArticleCreationService;

@Component(immediate = true, service = DigitalPlaceGlobalSetupWebContentService.class)
public class DigitalPlaceGlobalSetupWebContentService {

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	public JournalArticle getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent digitalPlaceWebContent, ServiceContext serviceContext) throws PortalException {

		try {

			String articleContent = StringUtil.read(getClass().getClassLoader(),
					"com/placecube/digitalplace/local/setup/globalsetup/dependencies/webcontent/" + digitalPlaceWebContent.getArticleId() + ".xml");

			JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder(digitalPlaceWebContent.getTitle(), serviceContext);

			return journalArticleCreationService.getOrCreateBasicWebContentArticle(digitalPlaceWebContent.getArticleId(), digitalPlaceWebContent.getTitle(), articleContent, journalFolder,
					serviceContext);

		} catch (IOException e) {

			throw new PortalException(e);

		}

	}

}
