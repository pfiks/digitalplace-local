package com.placecube.digitalplace.local.setup.globalsetup.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.setup.globalsetup.service.DigitalPlaceGlobalSetupService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class DigitalPlaceGlobalSetupLifeCycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceGlobalSetupLifeCycleListener.class);

	@Reference
	private DigitalPlaceGlobalSetupService digitalPlaceGlobalSetupService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {

		long companyId = company.getCompanyId();

		LOG.debug("Initializing Digital Place Global Setup For Global GroupId: " + companyId);

		Group globalGroup = company.getGroup();
		ServiceContext serviceContext = digitalPlaceGlobalSetupService.getServiceContext(globalGroup);

		digitalPlaceGlobalSetupService.initializeWebContentStructures(serviceContext);
		digitalPlaceGlobalSetupService.initializeWebContentTemplates(serviceContext);
		digitalPlaceGlobalSetupService.initializeWebContent(serviceContext);
		digitalPlaceGlobalSetupService.initializeWidgetTemplates(serviceContext);
		digitalPlaceGlobalSetupService.initializeAssetVocabularies(serviceContext);
		digitalPlaceGlobalSetupService.configureCookieConsentWidget(serviceContext);

		LOG.debug("Digital Place initialization Global Setup Completed.");
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Waits until the portal has finished initializing. No code needed.
	}
}
