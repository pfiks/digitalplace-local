package com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_3;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

public class UpdateGlobalFormConfirmationTemplateIdUpgradeProcess extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final DDMStructureLocalService ddmStructureLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private final GroupLocalService groupLocalService;

	private final JournalArticleLocalService journalArticleLocalService;

	private final Portal portal;

	private static final String FORM_CONFIRMATION_KEY_OLD = "FORM CONFIRMATION";

	private static final String FORM_CONFIRMATION_NAME_OLD = "Form Confirmation";

	private static final String GLOBAL_FORM_CONFIRMATION_KEY = "GLOBAL-FORM-CONFIRMATION";

	private static final String GLOBAL_FORM_CONFIRMATION_NAME = "Global Form Confirmation";

	public UpdateGlobalFormConfirmationTemplateIdUpgradeProcess(CompanyLocalService companyLocalService, DDMStructureLocalService ddmStructureLocalService, DDMTemplateLocalService ddmTemplateLocalService, GroupLocalService groupLocalService, JournalArticleLocalService journalArticleLocalService, Portal portal) {
		this.ddmStructureLocalService = ddmStructureLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
		this.groupLocalService = groupLocalService;
		this.companyLocalService = companyLocalService;
		this.journalArticleLocalService = journalArticleLocalService;
		this.portal = portal;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}

	}

	private void upgradeCompany(long companyId) throws PortalException {
		long globalGroupId = groupLocalService.fetchCompanyGroup(companyId).getGroupId();

		updateFormConfirmationTemplate(globalGroupId);
		updateJournalArticlesTemplate(globalGroupId);
	}

	private void updateFormConfirmationTemplate(long globalGroupId) throws PortalException {
		DDMTemplate oldFormConfirmationTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), FORM_CONFIRMATION_KEY_OLD);
		DDMTemplate newFormConfirmationTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), GLOBAL_FORM_CONFIRMATION_KEY);

		if (Validator.isNotNull(oldFormConfirmationTemplate) && Validator.isNotNull(newFormConfirmationTemplate)) {
			ddmTemplateLocalService.deleteTemplate(newFormConfirmationTemplate);
		}

		if (Validator.isNotNull(oldFormConfirmationTemplate)) {
			oldFormConfirmationTemplate.setTemplateKey(GLOBAL_FORM_CONFIRMATION_KEY);
			oldFormConfirmationTemplate.setName(oldFormConfirmationTemplate.getName().replace(FORM_CONFIRMATION_NAME_OLD, GLOBAL_FORM_CONFIRMATION_NAME));
			ddmTemplateLocalService.updateDDMTemplate(oldFormConfirmationTemplate);
		}
	}

	private void updateJournalArticlesTemplate(long globalGroupId) {
		DDMStructure formConfirmationGlobalStructure = ddmStructureLocalService.fetchStructure(globalGroupId, portal.getClassNameId(JournalArticle.class), FORM_CONFIRMATION_KEY_OLD);

		if (Validator.isNotNull(formConfirmationGlobalStructure)) {
			DynamicQuery dynamicQuery = journalArticleLocalService.dynamicQuery();
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", globalGroupId));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("DDMStructureId", formConfirmationGlobalStructure.getStructureId()));
			List<JournalArticle> journalArticles = journalArticleLocalService.dynamicQuery(dynamicQuery);

			for (JournalArticle journalArticle : journalArticles) {
				journalArticle.setDDMTemplateKey(GLOBAL_FORM_CONFIRMATION_KEY);
				journalArticleLocalService.updateJournalArticle(journalArticle);
			}
		}
	}

}
