package com.placecube.digitalplace.local.setup.globalsetup.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_0.UpdateGlobalTemplateIdUpgradeProcess;
import com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_1.UpdateGlobalGuideTemplateIdUpgradeProcess;
import com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_2.UpdateGlobalNewsTemplateIdUpgradeProcess;
import com.placecube.digitalplace.local.setup.globalsetup.upgrade.upgrade_1_0_3.UpdateGlobalFormConfirmationTemplateIdUpgradeProcess;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class DigitalPlaceGlobalSetupUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private Portal portal;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0",
				new UpdateGlobalTemplateIdUpgradeProcess(companyLocalService, ddmStructureLocalService, ddmTemplateLocalService, groupLocalService, journalArticleLocalService, portal));
		registry.register("1.0.0", "1.0.1", new UpdateGlobalGuideTemplateIdUpgradeProcess(companyLocalService, ddmTemplateLocalService, groupLocalService, journalArticleLocalService, portal));
		registry.register("1.0.1", "1.0.2",	new UpdateGlobalNewsTemplateIdUpgradeProcess(companyLocalService, ddmStructureLocalService, ddmTemplateLocalService, groupLocalService, journalArticleLocalService, portal));
		registry.register("1.0.2", "1.0.3", new UpdateGlobalFormConfirmationTemplateIdUpgradeProcess(companyLocalService, ddmStructureLocalService, ddmTemplateLocalService, groupLocalService, journalArticleLocalService, portal));
	}

}
