package com.placecube.digitalplace.local.setup.globalsetup.service;

import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.placecube.digitalplace.local.webcontent.webdocs.constants.WebdocsDDMTemplate;
import com.placecube.digitalplace.local.webcontent.webdocs.service.WebdocsWebContentService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.cookieconsent.service.CookieConsentConfigurationService;
import com.placecube.digitalplace.local.assetpublisher.constants.AssetPublisherWidgetTemplate;
import com.placecube.digitalplace.local.assetpublisher.service.AssetPublisherWebContentService;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.search.constants.SearchWidgetTemplate;
import com.placecube.digitalplace.local.search.service.SearchService;
import com.placecube.digitalplace.local.setup.globalsetup.constants.DigitalPlaceGlobalSetupWebContent;
import com.placecube.digitalplace.local.webcontent.accordion.constants.AccordionDDMTemplate;
import com.placecube.digitalplace.local.webcontent.accordion.service.AccordionWebContentService;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleDDMTemplate;
import com.placecube.digitalplace.local.webcontent.article.constants.WidgetTemplate;
import com.placecube.digitalplace.local.webcontent.article.service.ArticleWebContentService;
import com.placecube.digitalplace.local.webcontent.formconfirmation.service.FormConfirmationWebContentService;
import com.placecube.digitalplace.local.webcontent.formerrorpayment.service.FormErrorPaymentWebContentService;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsDDMTemplate;
import com.placecube.digitalplace.local.webcontent.news.constants.NewsWidgetTemplate;
import com.placecube.digitalplace.local.webcontent.news.service.NewsWebContentService;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;

@Component(immediate = true, service = DigitalPlaceGlobalSetupService.class)
public class DigitalPlaceGlobalSetupService {

	@Reference
	private AccordionWebContentService accordionWebContentService;

	@Reference
	private ArticleWebContentService articleWebContentService;

	@Reference
	private AssetPublisherWebContentService assetPublisherWebContentService;

	@Reference
	private CategoryNavigationService categoryNavigationService;

	@Reference
	private CookieConsentConfigurationService cookieConsentConfigurationService;

	@Reference
	private DigitalPlaceGlobalSetupWebContentService digitalPlaceWebContentService;

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private FormConfirmationWebContentService formConfirmationWebContentService;

	@Reference
	private FormErrorPaymentWebContentService formErrorPaymentWebContentService;

	@Reference
	private GuideWebContentService guideWebContentService;

	@Reference
	private NavItemTemplateService navItemTemplateService;

	@Reference
	private NewsWebContentService newsWebContentService;

	@Reference
	private WebdocsWebContentService webdocsWebContentService;

	@Reference
	private Portal portal;

	@Reference
	private SearchService searchService;

	@Reference
	private ServicesAssetVocabularyService servicesAssetVocabularyService;

	public void configureCookieConsentWidget(ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		try {

			Dictionary propertyDictionary = cookieConsentConfigurationService.getPropertyDictionary(groupId);
			cookieConsentConfigurationService.setConsentBannerArticleId(groupId, DigitalPlaceGlobalSetupWebContent.COOKIE_CONSENT_BANNER.getArticleId(), propertyDictionary);

		} catch (ConfigurationException e) {
			throw new PortalException("Error configuring Cookie Consent widget during site initialisation", e);
		}

	}

	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(false);

		return serviceContext;
	}

	public void initializeAssetVocabularies(ServiceContext serviceContext) throws PortalException {
		Map<Long, Set<Long>> selectedClassNameIds = new HashMap<>();
		Map<Long, Set<Long>> requiredClassNameIds = new HashMap<>();

		Set<Long> webContentStructureIds = new LinkedHashSet<>();
		webContentStructureIds.add(articleWebContentService.getOrCreateDDMStructure(serviceContext).getStructureId());
		webContentStructureIds.add(guideWebContentService.getOrCreateDDMStructure(serviceContext).getStructureId());
		requiredClassNameIds.put(portal.getClassNameId(JournalArticle.class), webContentStructureIds);

		selectedClassNameIds.putAll(requiredClassNameIds);
		selectedClassNameIds.put(portal.getClassNameId(Layout.class), Collections.emptySet());

		servicesAssetVocabularyService.getOrCreateVocabulary(serviceContext, selectedClassNameIds, requiredClassNameIds);
	}

	public void initializeWebContent(ServiceContext serviceContext) throws PortalException {
		digitalPlaceWebContentService.getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.COOKIE_CONSENT_BANNER, serviceContext);
		digitalPlaceWebContentService.getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.EMAIL_ADDRESS_VERIFIED_CONTENT, serviceContext);
		digitalPlaceWebContentService.getOrCreateBasicWebContent(DigitalPlaceGlobalSetupWebContent.SIGN_IN_CONTENT, serviceContext);
	}

	public void initializeWebContentStructures(ServiceContext serviceContext) throws PortalException {
		accordionWebContentService.getOrCreateDDMStructure(serviceContext);
		articleWebContentService.getOrCreateDDMStructure(serviceContext);
		formConfirmationWebContentService.getOrCreateDDMStructure(serviceContext, GroupConstants.GLOBAL);
		guideWebContentService.getOrCreateDDMStructure(serviceContext);
		newsWebContentService.getOrCreateDDMStructure(serviceContext);
		emailWebContentService.getOrCreateDDMStructure(serviceContext);
		formErrorPaymentWebContentService.getOrCreateDDMStructure(serviceContext);
		webdocsWebContentService.getOrCreateDDMStructure(serviceContext);
	}

	public void initializeWebContentTemplates(ServiceContext serviceContext) throws PortalException {
		accordionWebContentService.getOrCreateDDMTemplate(serviceContext, AccordionDDMTemplate.ACCORDION_CONTENT);
		guideWebContentService.getOrCreateDDMTemplate(serviceContext, GuideDDMTemplate.GUIDE_CONTENT, GroupConstants.GLOBAL);
		guideWebContentService.getOrCreateDDMTemplate(serviceContext, GuideDDMTemplate.GUIDE_NAVIGATION, GroupConstants.GLOBAL);
		newsWebContentService.getOrCreateDDMTemplate(serviceContext, NewsDDMTemplate.NEWS, GroupConstants.GLOBAL);
		webdocsWebContentService.getOrCreateDDMTemplate(serviceContext, WebdocsDDMTemplate.WEBDOCS_TITLE);
		webdocsWebContentService.getOrCreateDDMTemplate(serviceContext, WebdocsDDMTemplate.WEBDOCS_NAVIGATION);
		webdocsWebContentService.getOrCreateDDMTemplate(serviceContext, WebdocsDDMTemplate.WEBDOCS_CONTENT);
		guideWebContentService.getOrCreateDefaultValues(serviceContext);
	}

	public void initializeWidgetTemplates(ServiceContext serviceContext) throws PortalException {
		articleWebContentService.getOrCreateWidgetTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS, serviceContext);
		articleWebContentService.getOrCreateWidgetTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS_LARGE, serviceContext);
		articleWebContentService.getOrCreateWidgetTemplate(WidgetTemplate.RELATED_CONTENT_LIST, serviceContext);
		articleWebContentService.getOrCreateDDMTemplate(ArticleDDMTemplate.ARTICLE, serviceContext);
		categoryNavigationService.getOrCreateWidgetTemplate(CategoryNavigationWidgetTemplate.SERVICE_MENU, serviceContext);
		categoryNavigationService.getOrCreateWidgetTemplate(CategoryNavigationWidgetTemplate.CATEGORY_NAV_LINKS, serviceContext);
		navItemTemplateService.getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.MY_ACCOUNT, serviceContext);
		articleWebContentService.getOrCreateWidgetTemplate(WidgetTemplate.CATEGORY_CONTENT_LIST, serviceContext);
		navItemTemplateService.getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_LINK_PANELS, serviceContext);
		navItemTemplateService.getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_PAGE_CATEGORY_LINK, serviceContext);
		navItemTemplateService.getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_COLLAPSIBLE_BOX, serviceContext);
		navItemTemplateService.getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.NAV_MENU_POPULAR_PAGES_WEB_CONTENT_NAV_PANELS, serviceContext);
		newsWebContentService.getOrCreateWidgetTemplate(NewsWidgetTemplate.NEWS_CAROUSEL, serviceContext);
		assetPublisherWebContentService.getOrCreateWidgetTemplate(AssetPublisherWidgetTemplate.CONTENT_LINK_PANELS, serviceContext);
		searchService.getOrCreateWidgetTemplate(SearchWidgetTemplate.SEARCH_LIST, serviceContext);
	}
}
