package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.modellistener;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSet;
import com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.service.UserLayoutSetService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutSetModelListenerTest extends PowerMockito {

	@InjectMocks
	private LayoutSetModelListener layoutSetModelListener;

	@Mock
	private Group mockGroup;

	@Mock
	private LayoutSet mockLayoutSet;

	@Mock
	private UserLayoutSetService mockUserModelService;

	@Test
	public void onAfterCreate_WhenError_ThenDoNothing() throws PortalException {
		when(mockLayoutSet.getGroup()).thenThrow(new PortalException());

		layoutSetModelListener.onAfterCreate(mockLayoutSet);

		verify(mockUserModelService, never()).updateLayoutSetId(mockLayoutSet);
	}

	@Parameters({ "true,false", "false,true", "false,false" })
	@Test
	public void onAfterCreate_WhenNoErrorAndIsUserAndIsPrivateLayout_ThenDontUpdateLayoutSetForUser(boolean isUser, boolean isPrivateLayout) throws PortalException {
		when(mockLayoutSet.getGroup()).thenReturn(mockGroup);
		when(mockGroup.isUser()).thenReturn(isUser);
		when(mockLayoutSet.isPrivateLayout()).thenReturn(isPrivateLayout);

		layoutSetModelListener.onAfterCreate(mockLayoutSet);

		verify(mockUserModelService, never()).updateLayoutSetId(mockLayoutSet);
	}

	@Test
	public void onAfterCreate_WhenNoErrorAndIsUserAndIsPrivateLayout_ThenUpdateLayoutSetForUser() throws PortalException {
		when(mockLayoutSet.getGroup()).thenReturn(mockGroup);
		when(mockGroup.isUser()).thenReturn(true);
		when(mockLayoutSet.isPrivateLayout()).thenReturn(true);

		layoutSetModelListener.onAfterCreate(mockLayoutSet);

		verify(mockUserModelService, times(1)).updateLayoutSetId(mockLayoutSet);
	}

}
