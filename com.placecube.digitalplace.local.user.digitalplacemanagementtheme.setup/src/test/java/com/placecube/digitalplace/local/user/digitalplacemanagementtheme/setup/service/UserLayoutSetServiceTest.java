package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.service.LayoutSetLocalService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class UserLayoutSetServiceTest extends PowerMockito {

	static final String THEME_ID = "exampletheme_WAR_exampletheme";

	@Mock
	private LayoutSet mockLayoutSet;

	@Mock
	private LayoutSetLocalService mockLayoutSetLocalService;

	@InjectMocks
	private UserLayoutSetService userLayoutSetService;

	@Test
	public void updateLayoutSetId_WhenNoError_ThenUpdateLayoutSet()  {
		userLayoutSetService.updateLayoutSetId(mockLayoutSet);

		verify(mockLayoutSetLocalService, times(1)).updateLayoutSet(mockLayoutSet);
	}

}
