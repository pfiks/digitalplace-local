package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.service;

import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.constants.DigitalPlaceManagementThemeSetupConstants;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UserLayoutSetService.class)
public class UserLayoutSetService {

	@Reference
	private LayoutSetLocalService layoutSetLocalService;

	public void updateLayoutSetId(LayoutSet layoutSet) {
		layoutSet.setThemeId(DigitalPlaceManagementThemeSetupConstants.THEME_ID);
		layoutSetLocalService.updateLayoutSet(layoutSet);
	}

}
