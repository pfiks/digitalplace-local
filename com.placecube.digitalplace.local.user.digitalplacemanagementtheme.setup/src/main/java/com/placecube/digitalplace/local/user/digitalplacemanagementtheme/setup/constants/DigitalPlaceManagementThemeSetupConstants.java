package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.constants;

public final class DigitalPlaceManagementThemeSetupConstants {

	public static final String THEME_ID = "digitalplacemanagementtheme_WAR_digitalplacemanagementtheme";

	private DigitalPlaceManagementThemeSetupConstants() {
		return;
	}
}
