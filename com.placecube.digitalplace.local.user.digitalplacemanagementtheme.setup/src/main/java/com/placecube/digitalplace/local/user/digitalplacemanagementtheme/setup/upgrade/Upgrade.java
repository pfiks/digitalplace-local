package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.upgrade;

import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.service.UserLayoutSetService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private LayoutSetLocalService layoutSetLocalService;

	@Reference
	private UserLayoutSetService userLayoutSetService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "0.0.1", new UserLayoutSetUpgrade(userLayoutSetService, userLocalService, companyLocalService, layoutSetLocalService));
	}
}