package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.upgrade;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.service.UserLayoutSetService;

import java.util.List;

public class UserLayoutSetUpgrade extends UpgradeProcess {

	private CompanyLocalService companyLocalService;

	private LayoutSetLocalService layoutSetLocalService;

	private UserLayoutSetService userLayoutSetService;

	private UserLocalService userLocalService;

	public UserLayoutSetUpgrade(UserLayoutSetService userLayoutSetService, UserLocalService userLocalService, CompanyLocalService companyLocalService, LayoutSetLocalService layoutSetLocalService) {
		this.companyLocalService = companyLocalService;
		this.userLayoutSetService = userLayoutSetService;
		this.layoutSetLocalService = layoutSetLocalService;
		this.userLocalService = userLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws PortalException {
		long guestUserId = userLocalService.getGuestUserId(companyId);
		ActionableDynamicQuery actionableDynamicQuery = userLocalService.getActionableDynamicQuery();

		actionableDynamicQuery.setAddCriteriaMethod((DynamicQuery query) -> {
			query.add(RestrictionsFactoryUtil.eq("companyId", companyId));
			query.add(RestrictionsFactoryUtil.ne("userId", guestUserId));
		});

		actionableDynamicQuery.setPerformActionMethod((User user) -> {
			LayoutSet layoutSetPrivate = layoutSetLocalService.getLayoutSet(user.getGroupId(), true);
			userLayoutSetService.updateLayoutSetId(layoutSetPrivate);
		});

		actionableDynamicQuery.performActions();

	}

}