package com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.modellistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.model.ModelListener;
import com.placecube.digitalplace.local.user.digitalplacemanagementtheme.setup.service.UserLayoutSetService;

@Component(immediate = true, service = ModelListener.class)
public class LayoutSetModelListener extends BaseModelListener<LayoutSet> {

	private static final Log LOG = LogFactoryUtil.getLog(LayoutSetModelListener.class);

	@Reference
	private UserLayoutSetService userModelService;

	@Override
	public void onAfterCreate(LayoutSet layoutSet) throws ModelListenerException {
		super.onAfterCreate(layoutSet);
		try {
			if (layoutSet.getGroup().isUser() && layoutSet.isPrivateLayout()) {
				userModelService.updateLayoutSetId(layoutSet);
			}
		} catch (PortalException e) {
			LOG.error("Unable to update LayoutSet for layoutSetId: " + layoutSet.getLayoutSetId(), e);
		}
	}

}
