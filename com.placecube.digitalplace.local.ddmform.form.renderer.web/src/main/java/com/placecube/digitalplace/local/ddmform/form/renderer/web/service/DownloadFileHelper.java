package com.placecube.digitalplace.local.ddmform.form.renderer.web.service;

import java.util.Locale;
import java.util.Optional;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordExporterResponse;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordVersionLocalService;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.ddmform.utils.DDMFormFieldTypeUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter.DDMFormInstanceRecordVersionExporter;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter.DDMFormInstanceRecordVersionExporterRequest;

@Component(immediate = true, service = DownloadFileHelper.class)
public class DownloadFileHelper {

	@Reference
	private DDMFormFieldTypeUtil ddmFormFieldTypeUtil;

	@Reference
	private DDMFormInstanceRecordVersionExporter ddmFormInstanceRecordVersionExporter;

	@Reference
	private DDMFormInstanceRecordVersionLocalService ddmFormInstanceRecordVersionLocalService;

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	public void downloadFile(ResourceRequest resourceRequest, ResourceResponse resourceResponse, int status, String extension) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DDMFormInstance formInstance = formEntryInstanceService.getFormInstanceFromConfiguration(resourceRequest);

		Locale locale = ddmFormFieldTypeUtil.getLocale(formInstance, themeDisplay.getLocale());

		Optional<FormEntry> formEntry = formEntrySessionHelper.getLatestFormEntryByUniqueReferenceAndStatus(formEntrySessionHelper.getUniqueReference(resourceRequest), status);

		if (formEntry.isPresent()) {
			DDMFormInstanceRecordVersion latestFormInstanceRecordVersion = ddmFormInstanceRecordVersionLocalService.fetchDDMFormInstanceRecordVersion(formEntry.get().getFormInstanceRecordVersionId());

			if (Validator.isNotNull(latestFormInstanceRecordVersion)) {

				DDMFormInstanceRecordVersionExporterRequest.Builder builder = DDMFormInstanceRecordVersionExporterRequest.Builder.newBuilder(latestFormInstanceRecordVersion, extension);

				DDMFormInstanceRecordVersionExporterRequest ddmFormInstanceRecordExporterRequest = builder.withLocale(locale).withStatus(status).build();

				DDMFormInstanceRecordExporterResponse ddmFormInstanceRecordExporterResponse = ddmFormInstanceRecordVersionExporter.export(ddmFormInstanceRecordExporterRequest);

				String fileName = formInstance.getName(locale) + "." + extension;

				PortletResponseUtil.sendFile(resourceRequest, resourceResponse, fileName, ddmFormInstanceRecordExporterResponse.getContent(), ContentTypes.APPLICATION_OCTET_STREAM);

			}
		}

	}
}
