package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormSuccessPageSettings;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormActionURLHelper;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormConfigurationService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntryInstanceService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntrySessionHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.ADD_FORM_INSTANCE_RECORD }, service = MVCRenderCommand.class)
public class ConfirmationPageMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private FormActionURLHelper formActionURLHelper;

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {

			String ddmFormInstanceRecordId = ParamUtil.getString(renderRequest, FormPortletRequestKeys.DDM_FORM_INSTANCE_RECORD_ID);

			DDMFormInstance ddmFormInstance = formEntryInstanceService.getFormInstanceFromConfiguration(renderRequest);

			DDMForm ddmForm = formEntryInstanceService.getDDMForm(ddmFormInstance);

			DDMFormSuccessPageSettings ddmFormSuccessPageSettings = ddmForm.getDDMFormSuccessPageSettings();

			if (ddmFormSuccessPageSettings.isEnabled()) {

				setSuccesPageAttributes(ddmFormInstanceRecordId, ddmForm, ddmFormSuccessPageSettings, renderRequest);

			} else {

				setArticleIdAttribute(renderRequest);

			}

			String uniqueReference = formEntrySessionHelper.getUniqueReference(renderRequest);

			if (Validator.isNull(uniqueReference)) {
				renderRequest.setAttribute(FormPortletRequestKeys.RETURN_TO_FORM_LINK, formActionURLHelper.getDefaultRedirectURL(renderRequest));
			} else {
				renderRequest.setAttribute(FormPortletRequestKeys.UNIQUE_REFERENCE, uniqueReference);
				formEntrySessionHelper.removeReferenceFromSession(renderRequest);
			}

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/confirmation_page.jsp";
	}

	private void setArticleIdAttribute(RenderRequest renderRequest) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		formConfigurationService.getConfiguration(themeDisplay);

		Optional<String> articleId = formConfigurationService.getArticleId(themeDisplay);

		if (articleId.isPresent()) {
			JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(themeDisplay.getScopeGroupId(), articleId.get(), WorkflowConstants.STATUS_APPROVED);

			if (Validator.isNotNull(journalArticle)) {
				renderRequest.setAttribute(FormPortletRequestKeys.ARTICLE_ID, articleId.get());
			}

		}
	}

	private void setSuccesPageAttributes(String ddmFormInstanceRecordId, DDMForm ddmForm, DDMFormSuccessPageSettings ddmFormSuccessPageSettings, RenderRequest renderRequest) {

		LocalizedValue title = ddmFormSuccessPageSettings.getTitle();
		LocalizedValue body = ddmFormSuccessPageSettings.getBody();
		Locale displayLocale = ddmForm.getDefaultLocale();
		renderRequest.setAttribute("successPageTitle",
				HtmlUtil.escape(GetterUtil.getString(title.getString(displayLocale), title.getString(title.getDefaultLocale()))).replace("$FORM_REFERENCE$", ddmFormInstanceRecordId));
		renderRequest.setAttribute("successPageBody",
				HtmlUtil.escape(GetterUtil.getString(body.getString(displayLocale), body.getString(body.getDefaultLocale()))).replace("$FORM_REFERENCE$", ddmFormInstanceRecordId));

	}

}
