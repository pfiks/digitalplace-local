package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.SUCCESS_REDIRECT }, service = MVCRenderCommand.class)
public class SuccessRedirectMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		renderRequest.setAttribute("redirectURL", ParamUtil.getString(renderRequest, "redirectURL"));

		return "/redirect.jsp";
	}
}
