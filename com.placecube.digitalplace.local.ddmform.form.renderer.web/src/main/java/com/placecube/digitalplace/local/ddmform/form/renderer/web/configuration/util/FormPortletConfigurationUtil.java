package com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.util;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;

@Component(immediate = true, service = FormPortletConfigurationUtil.class)
public class FormPortletConfigurationUtil {

	public FormPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {

		return ConfigurationProviderUtil.getPortletInstanceConfiguration(FormPortletInstanceConfiguration.class, themeDisplay);
	}

}
