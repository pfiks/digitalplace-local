package com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriter;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterRequest;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterResponse;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, property = "ddm.form.instance.record.writer.type=docx", service = DDMFormInstanceRecordWriter.class)
public class DDMFormInstanceRecordVersionDOCXWriter implements DDMFormInstanceRecordWriter {

	@Override
	public DDMFormInstanceRecordWriterResponse write(DDMFormInstanceRecordWriterRequest ddmFormInstanceRecordWriterRequest) throws Exception {

		try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(); XWPFDocument document = new XWPFDocument()) {

			Map<String, String> ddmFormFieldsLabel = ddmFormInstanceRecordWriterRequest.getDDMFormFieldsLabel();

			List<Map<String, String>> ddmFormFieldsValueList = ddmFormInstanceRecordWriterRequest.getDDMFormFieldValues();

			for (Map<String, String> ddmFormFieldsValue : ddmFormFieldsValueList) {
				for (Map.Entry<String, String> entry : ddmFormFieldsValue.entrySet()) {
					if (Validator.isNotNull(entry.getValue())) {
						createParagraphs(document, ddmFormFieldsLabel.get(entry.getKey()), entry.getValue());
					}
				}
			}

			document.write(byteArrayOutputStream);

			DDMFormInstanceRecordWriterResponse.Builder builder = DDMFormInstanceRecordWriterResponse.Builder.newBuilder(byteArrayOutputStream.toByteArray());

			return builder.build();
		}

	}

	private void createParagraphs(XWPFDocument document, String label, String value) {

		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setText(label + " - " + value);

	}
}