package com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter;

import java.util.Locale;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

public final class DDMFormInstanceRecordVersionExporterRequest {

	private DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion;

	private Locale locale;

	private int status = WorkflowConstants.STATUS_ANY;

	private String type;

	public static class Builder {

		private final DDMFormInstanceRecordVersionExporterRequest ddmFormInstanceRecordExporterRequest = new DDMFormInstanceRecordVersionExporterRequest();

		public static Builder newBuilder(DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion, String type) {
			return new Builder(ddmFormInstanceRecordVersion, type);
		}

		private Builder(DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion, String type) {
			ddmFormInstanceRecordExporterRequest.ddmFormInstanceRecordVersion = ddmFormInstanceRecordVersion;

			ddmFormInstanceRecordExporterRequest.type = type;
		}

		public DDMFormInstanceRecordVersionExporterRequest build() {
			return ddmFormInstanceRecordExporterRequest;
		}

		public Builder withLocale(Locale locale) {
			ddmFormInstanceRecordExporterRequest.locale = locale;

			return this;
		}

		public Builder withStatus(int status) {
			ddmFormInstanceRecordExporterRequest.status = status;

			return this;
		}

	}

	private DDMFormInstanceRecordVersionExporterRequest() {
	}

	public DDMFormInstanceRecordVersion getDDMFormInstanceRecordVersion() {
		return ddmFormInstanceRecordVersion;
	}

	public Locale getLocale() {
		return locale;
	}

	public int getStatus() {
		return status;
	}

	public String getType() {
		return type;
	}

}