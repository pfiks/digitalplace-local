package com.placecube.digitalplace.local.ddmform.form.renderer.web.constants;

public final class MVCCommandKeys {

	public static final String ADD_FORM_INSTANCE_RECORD = "/addFormInstanceRecord";

	public static final String ADD_FORM_INSTANCE_RECORD_EXCEPTION = "/addFormInstanceRecordException";

	public static final String CONTINUE_FORM = "/continue/form";

	public static final String DOWNLOAD_FILE_DOCX = "/download/file/docx";

	public static final String DOWNLOAD_FILE_PDF = "/download/file/pdf";

	public static final String DOWNLOAD_FILE_XLSX = "/download/file/xlsx";

	public static final String SAVE_PROGRESS = "/save/progress";

	public static final String SUCCESS_REDIRECT = "/success/redirect";

	private MVCCommandKeys() {

	}
}
