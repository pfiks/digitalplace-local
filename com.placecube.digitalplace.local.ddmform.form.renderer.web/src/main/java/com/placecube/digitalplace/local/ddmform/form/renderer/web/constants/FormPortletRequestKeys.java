package com.placecube.digitalplace.local.ddmform.form.renderer.web.constants;

public final class FormPortletRequestKeys {

	public static final String ADD_FORM_INSTANCE_RECORD_PERMISSION = "addFormInstanceRecordPermission";

	public static final String ARTICLE_ID = "articleId";

	public static final String ARTICLE_ID_EXCEPTION = "articleIdException";

	public static final String CONTINUE_SAVED_FORM_ARTICLE_ID_VIEW = "continueSavedFormArticleIdView";

	public static final String DDM_FORM_INSTANCE_RECORD_ID = "ddmFormInstanceRecordId";

	public static final String DDM_FORM_INSTANCE_RECORD_VERSION_ID = "ddmFormInstanceRecordVersionId";

	public static final String DDM_FORM_INSTANCE_RECORD_VERSIONS = "ddmFormInstanceRecordVersions";

	public static final String DDM_FORM_INSTANCES = "ddmFormInstances";

	public static final String DELETE_DATA_AFTER_SUBMISSION = "deleteDataAfterSubmission";

	public static final String EXPIRE_OLD_FORMS = "expireOldForms";

	public static final String EXPIRE_OLD_FORMS_INTERVAL = "expireOldFormsInterval";

	public static final String FORM_INSTANCE_ID = "formInstanceId";

	public static final String HAS_FORM_DRAFT = "hasFormDraft";

	public static final String HIDE_PAGINATION = "hidePagination";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String REFERENCE_DOES_NOT_EXIST_ERROR_KEY = "reference-does-not-exist";

	public static final String RETURN_TO_FORM_LINK = "returnToFormLink";

	public static final String SAVE_AND_CONTINUE_ENABLED = "saveAndContinueEnabled";

	public static final String SAVE_PROGRESS_ARTICLE_ID_EMAIL = "saveProgressArticleIdEmail";

	public static final String SAVE_PROGRESS_ARTICLE_ID_VIEW = "saveProgressArticleIdView";

	public static final String STATUS = "status";

	public static final String UNIQUE_REFERENCE = "uniqueReference";

	private FormPortletRequestKeys() {

	}
}
