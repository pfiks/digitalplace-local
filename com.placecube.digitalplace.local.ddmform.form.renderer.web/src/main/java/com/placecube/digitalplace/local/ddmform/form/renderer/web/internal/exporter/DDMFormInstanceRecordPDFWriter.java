package com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriter;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterRequest;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterResponse;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.document.pdf.service.PdfConverterService;

@Component(immediate = true, property = "ddm.form.instance.record.writer.type=pdf", service = DDMFormInstanceRecordWriter.class)
public class DDMFormInstanceRecordPDFWriter implements DDMFormInstanceRecordWriter {

	@Reference
	private PdfConverterService pdfConverterService;

	@Override
	public DDMFormInstanceRecordWriterResponse write(DDMFormInstanceRecordWriterRequest ddmFormInstanceRecordWriterRequest) throws Exception {

		Map<String, String> ddmFormFieldsLabel = ddmFormInstanceRecordWriterRequest.getDDMFormFieldsLabel();

		List<Map<String, String>> ddmFormFieldsValueList = ddmFormInstanceRecordWriterRequest.getDDMFormFieldValues();

		Map<String, Object> formFieldValueMap = new LinkedHashMap<>();
		for (Map<String, String> ddmFormFieldsValue : ddmFormFieldsValueList) {
			for (Map.Entry<String, String> entry : ddmFormFieldsValue.entrySet()) {
				if (Validator.isNotNull(entry.getValue())) {
					formFieldValueMap.put(ddmFormFieldsLabel.get(entry.getKey()), entry.getValue());
				}
			}
		}

		byte[] byteArray = pdfConverterService.generatePdfFromKeyValueMap(ddmFormFieldsLabel.get("formInstanceName"), formFieldValueMap);
		DDMFormInstanceRecordWriterResponse.Builder builder = DDMFormInstanceRecordWriterResponse.Builder.newBuilder(byteArray);

		return builder.build();
	}

}