package com.placecube.digitalplace.local.ddmform.form.renderer.web.scheduler;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeRunnable;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.scheduler.SchedulerJobConfiguration;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.util.PortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.FormEntryLocalService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;

@Component(immediate = true, service = SchedulerJobConfiguration.class)
public class FormEntryExpirySchedulerJobConfiguration implements SchedulerJobConfiguration {

	private static final Log LOG = LogFactoryUtil.getLog(FormEntryExpirySchedulerJobConfiguration.class);

	private static final String CRON_EXPRESSION = "0 */5 * ? * *";

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private FormEntryLocalService formEntryLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	@Activate
	protected void activate(Map<String, Object> properties) {
		// no implementation
	}

	@Override
	public UnsafeConsumer<Long, Exception> getCompanyJobExecutorUnsafeConsumer() {
		return this::runJobForCompany;
	}

	@Override
	public UnsafeRunnable<Exception> getJobExecutorUnsafeRunnable() {
		return () -> companyLocalService.forEachCompanyId(this::runJobForCompany);
	}

	@Override
	public TriggerConfiguration getTriggerConfiguration() {
		TriggerConfiguration triggerConfiguration = TriggerConfiguration.createTriggerConfiguration(CRON_EXPRESSION);
		triggerConfiguration.setStartDate(new Date());

		return triggerConfiguration;
	}

	private void processSinglePortletPreferences(PortletPreferences portletPreferences, long companyId) {
		Instant now = Instant.now();
		try {

			long plid = portletPreferences.getPlid();
			Layout layout = layoutLocalService.getLayout(plid);
			LOG.debug(String.format("Starting form entries expiry schedule for page:%s layoutId:%s", layout.getFriendlyURL(), plid));

			FormPortletInstanceConfiguration formPortletInstanceConfiguration = ConfigurationProviderUtil.getPortletInstanceConfiguration(FormPortletInstanceConfiguration.class, layout,
					FormPortletKeys.FORM);

			if (formPortletInstanceConfiguration.expireOldForms() && plid != 0) {
				Date dateExpiration = Date.from(now.minus(Duration.ofDays(formPortletInstanceConfiguration.expireOldFormsInterval())));
				List<FormEntry> formEntries = formEntryLocalService.getFormEntriesModifiedBeforeDate(companyId, plid, dateExpiration);
				if (!formEntries.isEmpty()) {
					LOG.info(String.format("Removing %s form entries with plId:%s and modifiedDate before %s", formEntries.size(), plid, dateExpiration));
					formEntries.forEach(formEntry -> formEntryLocalService.deleteFormEntry(formEntry));
					LOG.info("Finished removing expired form entries.");
				}

			}

		} catch (Exception e) {
			LOG.warn(String.format("Unable to process form entry expiry for companyId:%s preferences id:%s error:%s", companyId, portletPreferences.getPortletPreferencesId(), e.getMessage()));
			LOG.debug(e);
		}
	}

	private void runJobForCompany(long companyId) {
		try {
			List<PortletPreferences> allPortletPreferences = portletPreferencesLocalService.getPortletPreferences(companyId, PortletKeys.PREFS_OWNER_ID_DEFAULT,
					PortletKeys.PREFS_OWNER_TYPE_LAYOUT, FormPortletKeys.FORM);

			allPortletPreferences.forEach(portletPreferences -> processSinglePortletPreferences(portletPreferences, companyId));
		} catch (PortalException e) {
			LOG.error(String.format("Unable to process expiry action for company %s.", companyId), e);
		}
	}

}