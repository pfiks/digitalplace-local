package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.portlet.ActionRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.form.evaluator.DDMFormEvaluator;
import com.liferay.dynamic.data.mapping.form.evaluator.DDMFormEvaluatorEvaluateRequest;
import com.liferay.dynamic.data.mapping.form.evaluator.DDMFormEvaluatorEvaluateResponse;
import com.liferay.dynamic.data.mapping.form.evaluator.DDMFormEvaluatorFieldContextKey;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMFormLayoutColumn;
import com.liferay.dynamic.data.mapping.model.DDMFormLayoutPage;
import com.liferay.dynamic.data.mapping.model.DDMFormLayoutRow;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.model.UnlocalizedValue;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.dataconsumer.utils.executor.DataConsumerExecutorService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.util.FormPortletConfigurationUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntryInstanceService;

@Component(immediate = true, service = AddFormInstanceRecordMVCCommandHelper.class)
public class AddFormInstanceRecordMVCCommandHelper {

	private static final Log LOG = LogFactoryUtil.getLog(AddFormInstanceRecordMVCCommandHelper.class);

	@Reference
	private DataConsumerExecutorService dataConsumerExecutorService;

	@Reference
	private DDMFormEvaluator ddmFormEvaluator;

	@Reference
	private DDMFormInstanceService ddmFormInstanceService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private FormPortletConfigurationUtil formPortletConfigurationUtil;

	@Reference
	private Portal portal;

	public void clearUpFieldsAccordingToVisibility(ActionRequest actionRequest, DDMForm ddmForm, DDMFormValues ddmFormValues, Locale locale) throws PortalException {

		List<DDMFormField> requiredFields = getRequiredFields(ddmForm);

		DDMFormEvaluatorEvaluateResponse ddmFormEvaluatorEvaluateResponse = evaluate(actionRequest, ddmForm, ddmFormValues, locale);

		Set<String> invisibleFields = getInvisibleFields(ddmFormEvaluatorEvaluateResponse);

		DDMFormLayout ddmFormLayout = getDDMFormLayout(actionRequest);

		Set<String> fieldsFromDisabledPages = getFieldNamesFromDisabledPages(ddmFormEvaluatorEvaluateResponse, ddmFormLayout);

		invisibleFields.addAll(fieldsFromDisabledPages);

		removeRequiredProperty(invisibleFields, requiredFields);

		clearValueForInvisibleFields(invisibleFields, ddmFormValues);
	}

	protected DDMFormEvaluatorEvaluateResponse evaluate(ActionRequest actionRequest, DDMForm ddmForm, DDMFormValues ddmFormValues, Locale locale) {

		DDMFormEvaluatorEvaluateRequest.Builder builder = DDMFormEvaluatorEvaluateRequest.Builder.newBuilder(ddmForm, ddmFormValues, locale);

		builder.withCompanyId(portal.getCompanyId(actionRequest)).withGroupId(ParamUtil.getLong(actionRequest, "groupId")).withUserId(portal.getUserId(actionRequest));

		return ddmFormEvaluator.evaluate(builder.build());
	}

	protected int executeDataConsumers(ActionRequest actionRequest, DDMFormInstance ddmFormInstance, DDMFormInstanceRecord ddmFormInstanceRecord) {

		boolean hasDataConsumers = dataConsumerExecutorService.hasDataConsumers(ddmFormInstanceRecord.getFormInstanceId());

		if (hasDataConsumers) {
			try {
				dataConsumerExecutorService.executeDataConsumersFromActionRequest(ddmFormInstanceRecord.getFormInstance(), ddmFormInstanceRecord.getFormInstanceRecordId(), actionRequest);
			} catch (Exception e) {
				LOG.error("Error executing data consumers - formInstanceId: " + ddmFormInstanceRecord.getFormInstanceId(), e);
				return WorkflowConstants.STATUS_INCOMPLETE;
			}
		}

		return WorkflowConstants.STATUS_APPROVED;
	}

	protected DDMFormLayout getDDMFormLayout(ActionRequest actionRequest) throws PortalException {

		long formInstanceId = ParamUtil.getLong(actionRequest, "formInstanceId");

		DDMFormInstance formInstance = ddmFormInstanceService.getFormInstance(formInstanceId);

		DDMStructure ddmStructure = ddmStructureLocalService.getStructure(formInstance.getStructureId());

		return ddmStructure.getDDMFormLayout();
	}

	protected Set<String> getFieldNamesFromDisabledPages(DDMFormEvaluatorEvaluateResponse ddmFormEvaluatorEvaluateResponse, DDMFormLayout ddmFormLayout) {

		Set<Integer> disabledPagesIndexes = ddmFormEvaluatorEvaluateResponse.getDisabledPagesIndexes();

		Stream<Integer> disablePagesIndexesStream = disabledPagesIndexes.stream();

		Stream<String> fieldsStream = disablePagesIndexesStream.map(index -> getFieldNamesFromPage(index, ddmFormLayout)).flatMap(Collection::stream);

		return fieldsStream.collect(Collectors.toSet());
	}

	protected Set<String> getFieldNamesFromPage(int index, DDMFormLayout ddmFormLayout) {

		DDMFormLayoutPage ddmFormLayoutPage = ddmFormLayout.getDDMFormLayoutPage(index);

		List<DDMFormLayoutRow> ddmFormLayoutRows = ddmFormLayoutPage.getDDMFormLayoutRows();

		Set<String> fieldNames = new HashSet<>();

		for (DDMFormLayoutRow ddmFormLayoutRow : ddmFormLayoutRows) {
			for (DDMFormLayoutColumn ddmFormLayoutColumn : ddmFormLayoutRow.getDDMFormLayoutColumns()) {

				fieldNames.addAll(ddmFormLayoutColumn.getDDMFormFieldNames());
			}
		}

		return fieldNames;
	}

	protected Set<String> getInvisibleFields(DDMFormEvaluatorEvaluateResponse ddmFormEvaluatorEvaluateResponse) {

		Map<DDMFormEvaluatorFieldContextKey, Map<String, Object>> ddmFormFieldsPropertyChanges = ddmFormEvaluatorEvaluateResponse.getDDMFormFieldsPropertyChanges();

		Set<Map.Entry<DDMFormEvaluatorFieldContextKey, Map<String, Object>>> entrySet = ddmFormFieldsPropertyChanges.entrySet();

		Stream<Map.Entry<DDMFormEvaluatorFieldContextKey, Map<String, Object>>> stream = entrySet.stream();

		return stream.filter(result -> !MapUtil.getBoolean(result.getValue(), "visible", true)).map(result -> {
			DDMFormEvaluatorFieldContextKey ddmFormFieldContextKey = result.getKey();

			return ddmFormFieldContextKey.getName();
		}).collect(Collectors.toSet());
	}

	protected List<DDMFormField> getRequiredFields(DDMForm ddmForm) {
		Map<String, DDMFormField> ddmFormFieldsMap = ddmForm.getDDMFormFieldsMap(true);

		Collection<DDMFormField> ddmFormFields = ddmFormFieldsMap.values();

		Stream<DDMFormField> stream = ddmFormFields.stream();

		stream = stream.filter(DDMFormField::isRequired);

		return stream.collect(Collectors.toList());
	}

	protected ServiceContext getServiceContext(ActionRequest actionRequest) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstanceRecord.class.getName(), actionRequest);

		serviceContext.setRequest(portal.getHttpServletRequest(actionRequest));

		return serviceContext;
	}

	protected void removeRequiredProperty(DDMFormField ddmFormField) {
		ddmFormField.setRequired(false);
	}

	protected void removeRequiredProperty(Set<String> invisibleFields, List<DDMFormField> requiredFields) {

		Stream<DDMFormField> stream = requiredFields.stream();

		stream = stream.filter(field -> invisibleFields.contains(field.getName()));

		stream.forEach(this::removeRequiredProperty);
	}

	protected DDMFormInstanceRecord saveRecord(ActionRequest actionRequest, DDMFormInstance ddmFormInstance, int status) throws PortalException {

		DDMForm ddmForm = formEntryInstanceService.getDDMForm(ddmFormInstance);
		DDMFormValues ddmFormValues = formEntryInstanceService.createDDMFormValues(actionRequest, ddmForm);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		boolean deleteData = formPortletConfigurationUtil.getConfiguration(themeDisplay).deleteDataAfterSubmission();

		ServiceContext serviceContext = getServiceContext(actionRequest);

		serviceContext.setWorkflowAction(status);
		serviceContext.setAttribute(FormPortletRequestKeys.STATUS, status);

		DDMFormInstanceRecord ddmFormInstanceRecord = formEntryInstanceService.addOrUpdateFormInstanceRecord(ddmFormInstance, ddmFormValues, actionRequest, themeDisplay, serviceContext, deleteData);

		clearUpFieldsAccordingToVisibility(actionRequest, ddmForm, ddmFormValues, themeDisplay.getLocale());

		return ddmFormInstanceRecord;
	}

	protected void setRedirectAttributeInServiceContext(ActionRequest actionRequest, ServiceContext serviceContext) {
		String redirect = GetterUtil.getString(serviceContext.getAttribute("redirect"));

		if (Validator.isNull(redirect)) {
			redirect = actionRequest.getProperty("referer");
		}

		serviceContext.setAttribute("redirect", redirect);
	}

	protected DDMFormInstanceRecord updateFormInstanceRecord(ActionRequest actionRequest, DDMFormInstanceRecord ddmFormInstanceRecord, int status) throws PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		boolean deleteData = formPortletConfigurationUtil.getConfiguration(themeDisplay).deleteDataAfterSubmission();
		ServiceContext serviceContext = getServiceContext(actionRequest);

		if (status == WorkflowConstants.STATUS_APPROVED) {
			serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
		} else {
			serviceContext.setWorkflowAction(WorkflowConstants.ACTION_SAVE_DRAFT);
		}

		serviceContext.setAttribute(FormPortletRequestKeys.STATUS, status);

		return formEntryInstanceService.addOrUpdateFormInstanceRecord(ddmFormInstanceRecord.getFormInstance(), ddmFormInstanceRecord, ddmFormInstanceRecord.getDDMFormValues(), actionRequest,
				themeDisplay, serviceContext, deleteData);
	}

	private void clearValueForInvisibleFields(Set<String> invisibleFields, DDMFormValues ddmFormValues) {

		Stream<DDMFormFieldValue> stream = ddmFormValues.getDDMFormFieldValues().stream();

		stream = stream.filter(field -> invisibleFields.contains(field.getDDMFormField().getName()));

		stream.forEach(formFieldValue -> {
			if (formFieldValue.getValue() != null) {
				Value value;
				if (formFieldValue.getValue().isLocalized()) {
					value = new LocalizedValue(ddmFormValues.getDefaultLocale());
					for (Locale availableLocale : ddmFormValues.getAvailableLocales()) {
						value.addString(availableLocale, StringPool.BLANK);
					}
				} else {
					value = new UnlocalizedValue(StringPool.BLANK);
				}

				formFieldValue.setValue(value);
			}
		});

	}

}