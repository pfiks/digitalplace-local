package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormActionURLHelper;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormConfigurationService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntrySessionHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.ADD_FORM_INSTANCE_RECORD_EXCEPTION }, service = MVCRenderCommand.class)
public class ConfirmationPageExceptionMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private FormActionURLHelper formActionURLHelper;

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		try {

			setArticleIdAttribute(renderRequest);

			renderRequest.setAttribute(FormPortletRequestKeys.RETURN_TO_FORM_LINK, formActionURLHelper.getDefaultRedirectURL(renderRequest));

			formEntrySessionHelper.removeReferenceFromSession(renderRequest);

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/confirmation_page_exception.jsp";
	}

	private void setArticleIdAttribute(RenderRequest renderRequest) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		Optional<String> articleIdException = formConfigurationService.getArticleIdException(themeDisplay);
		if (articleIdException.isPresent()) {
			renderRequest.setAttribute(FormPortletRequestKeys.ARTICLE_ID_EXCEPTION, articleIdException.get());
		}

	}

}
