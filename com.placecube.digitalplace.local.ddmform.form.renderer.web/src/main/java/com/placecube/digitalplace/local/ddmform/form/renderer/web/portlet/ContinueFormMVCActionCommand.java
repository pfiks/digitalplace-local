package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import static com.liferay.portal.kernel.workflow.WorkflowConstants.STATUS_DRAFT;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordVersionLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.FormEntryLocalService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormActionURLHelper;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntrySessionHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.CONTINUE_FORM }, service = MVCActionCommand.class)
public class ContinueFormMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ContinueFormMVCActionCommand.class);

	@Reference
	private DDMFormInstanceRecordVersionLocalService ddmFormInstanceRecordVersionLocalService;

	@Reference
	private FormActionURLHelper formActionURLHelper;

	@Reference
	private FormEntryLocalService formEntryLocalService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String uniqueReference = ParamUtil.getString(actionRequest, FormPortletRequestKeys.UNIQUE_REFERENCE);

		if (Validator.isNotNull(uniqueReference)) {
			try {
				List<FormEntry> formEntries = formEntryLocalService.getFormEntriesByUniqueReferenceAndStatus(uniqueReference, STATUS_DRAFT);

				if (formEntries.size() != 1) {
					throw new PortalException("Found none or multiple results for form entry with reference [" + uniqueReference + "] and status draft");
				}

				FormEntry formEntry = formEntries.get(0);

				formEntrySessionHelper.saveReferenceToSession(formEntry.getUniqueReference(), actionRequest);

				DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion = ddmFormInstanceRecordVersionLocalService.fetchDDMFormInstanceRecordVersion(formEntry.getFormInstanceRecordVersionId());

				if (ddmFormInstanceRecordVersion != null) {
					actionRequest.setAttribute(WebKeys.REDIRECT, formActionURLHelper.getDefaultRedirectURL(actionRequest));
				}

			} catch (PortalException e) {
				LOG.error(e.getMessage(), e);
				SessionErrors.add(actionRequest, FormPortletRequestKeys.REFERENCE_DOES_NOT_EXIST_ERROR_KEY);
				hideDefaultErrorMessage(actionRequest);

				sendRedirect(actionRequest, actionResponse, formActionURLHelper.getContinueFormRedirect(actionRequest));
			}
		}
	}
}
