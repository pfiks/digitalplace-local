package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import static com.liferay.portal.kernel.workflow.WorkflowConstants.STATUS_DRAFT;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.DownloadFileHelper;

@Component(property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.DOWNLOAD_FILE_PDF }, service = MVCResourceCommand.class)
public class FormDownloadPdfFileMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private DownloadFileHelper downloadFileHelper;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		downloadFileHelper.downloadFile(resourceRequest, resourceResponse, STATUS_DRAFT, "pdf");
	}

}