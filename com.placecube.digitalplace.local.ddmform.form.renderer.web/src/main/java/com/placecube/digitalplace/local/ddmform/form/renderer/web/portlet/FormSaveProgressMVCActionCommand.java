package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntryInstanceService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.SaveProgressEmailNotificationService;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.SAVE_PROGRESS }, service = MVCActionCommand.class)
public class FormSaveProgressMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private SaveProgressEmailNotificationService saveProgressEmailNotificationService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DDMFormInstance ddmFormInstance = formEntryInstanceService.getFormInstance(actionRequest);

		DDMForm ddmForm = formEntryInstanceService.getDDMForm(ddmFormInstance);

		DDMFormValues ddmFormValues = formEntryInstanceService.createDDMFormValues(actionRequest, ddmForm);

		ddmForm.getDDMFormFields().forEach(field -> field.setRequired(Boolean.FALSE));

		formEntryInstanceService.addOrUpdateDraftFormInstanceRecord(ddmFormInstance, ddmFormValues, actionRequest, themeDisplay).getLatestFormInstanceRecordVersion();

		saveProgressEmailNotificationService.sendSaveProgressEmail(ddmFormValues, themeDisplay, actionRequest);

		PortletURL portletURL = PortletURLFactoryUtil.create(actionRequest, FormPortletKeys.FORM, PortletRequest.RENDER_PHASE);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.SAVE_PROGRESS);

		actionRequest.setAttribute(WebKeys.REDIRECT, portletURL.toString());
	}
}
