package com.placecube.digitalplace.local.ddmform.form.renderer.web.service;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;

@Component(immediate = true, service = FormActionURLHelper.class)
public class FormActionURLHelper {

	private PortletURL getBasePortletURL(PortletRequest portletRequest) {
		return PortletURLFactoryUtil.create(portletRequest, PortalUtil.getPortletId(portletRequest), PortletRequest.RENDER_PHASE);

	}

	public String getContinueFormRedirect(PortletRequest portletRequest) {

		PortletURL portletURL = getBasePortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONTINUE_FORM);

		return portletURL.toString();
	}

	public String getDefaultRedirectURL(PortletRequest portletRequest) {

		PortletURL portletURL = getBasePortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.MVC_RENDER_COMMAND_NAME, StringPool.FORWARD_SLASH);
		return portletURL.toString();
	}

	public String getErrorRedirect(PortletRequest portletRequest) {

		PortletURL portletURL = getBasePortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ADD_FORM_INSTANCE_RECORD_EXCEPTION);
		return portletURL.toString();
	}

	public String getSuccessRedirect(PortletRequest portletRequest, String redirectURL) {

		PortletURL portletURL = getBasePortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.SUCCESS_REDIRECT);
		portletURL.getRenderParameters().setValue("redirectURL", redirectURL);
		return portletURL.toString();
	}

	public String getFormSuccessPageRedirect(PortletRequest portletRequest, Long formInstanceRecordId) {

		PortletURL portletURL = getBasePortletURL(portletRequest);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ADD_FORM_INSTANCE_RECORD);
		portletURL.getRenderParameters().setValue(FormPortletRequestKeys.DDM_FORM_INSTANCE_RECORD_ID, String.valueOf(formInstanceRecordId));
		return portletURL.toString();
	}

}
