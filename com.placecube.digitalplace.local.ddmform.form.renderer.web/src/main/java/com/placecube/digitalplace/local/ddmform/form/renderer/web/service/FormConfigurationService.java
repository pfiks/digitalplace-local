package com.placecube.digitalplace.local.ddmform.form.renderer.web.service;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.model.FormConfigurationContext;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.util.FormPortletConfigurationUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;

@Component(immediate = true, service = FormConfigurationService.class)
public class FormConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(FormConfigurationService.class);

	@Reference
	private FormPortletConfigurationUtil formPortletConfigurationUtil;

	public Optional<String> getArticleId(ThemeDisplay themeDisplay) throws PortletException {
		FormPortletInstanceConfiguration configuration = getConfiguration(themeDisplay);
		String articleId = configuration.articleId();

		return Validator.isNotNull(articleId) ? Optional.of(articleId) : Optional.empty();

	}

	public Optional<String> getArticleIdContinueSavedFormView(ThemeDisplay themeDisplay) throws PortletException {

		FormPortletInstanceConfiguration configuration = getConfiguration(themeDisplay);

		String continueSavedFormArticleIdView = configuration.continueSavedFormArticleIdView();

		return Validator.isNotNull(continueSavedFormArticleIdView) ? Optional.of(continueSavedFormArticleIdView) : Optional.empty();

	}

	public Optional<String> getArticleIdException(ThemeDisplay themeDisplay) throws PortletException {
		FormPortletInstanceConfiguration configuration = getConfiguration(themeDisplay);
		String articleIdException = configuration.articleIdException();

		return Validator.isNotNull(articleIdException) ? Optional.of(articleIdException) : Optional.empty();

	}

	public Optional<String> getArticleIdSaveProgressView(ThemeDisplay themeDisplay) throws PortletException {
		FormPortletInstanceConfiguration configuration = getConfiguration(themeDisplay);
		String articleIdException = configuration.saveProgressArticleIdView();

		return Validator.isNotNull(articleIdException) ? Optional.of(articleIdException) : Optional.empty();

	}

	public FormPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay) throws PortletException {
		try {
			return formPortletConfigurationUtil.getConfiguration(themeDisplay);
		} catch (ConfigurationException e) {
			LOG.error("Error retriving configuration", e);
			throw new PortletException("Error retriving portlet configuration", e);
		}
	}

	public FormConfigurationContext getFormConfigurationContext(PortletRequest portletRequest) {
		FormConfigurationContext formConfigurationContext = FormConfigurationContext.newInstance();

		formConfigurationContext.setFormInstanceId(ParamUtil.getLong(portletRequest, FormPortletRequestKeys.FORM_INSTANCE_ID));
		formConfigurationContext.setArticleId(ParamUtil.getString(portletRequest, FormPortletRequestKeys.ARTICLE_ID));
		formConfigurationContext.setArticleIdException(ParamUtil.getString(portletRequest, FormPortletRequestKeys.ARTICLE_ID_EXCEPTION));
		formConfigurationContext.setSaveProgressArticleIdEmail(ParamUtil.getString(portletRequest, FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_EMAIL));
		formConfigurationContext.setSaveProgressArticleIdView(ParamUtil.getString(portletRequest, FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_VIEW));
		formConfigurationContext.setContinueSavedFormArticleIdView(ParamUtil.getString(portletRequest, FormPortletRequestKeys.CONTINUE_SAVED_FORM_ARTICLE_ID_VIEW));
		formConfigurationContext.setHidePagination(ParamUtil.getBoolean(portletRequest, FormPortletRequestKeys.HIDE_PAGINATION));
		formConfigurationContext.setDeleteDataAfterSubmission(ParamUtil.getBoolean(portletRequest, FormPortletRequestKeys.DELETE_DATA_AFTER_SUBMISSION));
		formConfigurationContext.setExpireOldForms(ParamUtil.getBoolean(portletRequest, FormPortletRequestKeys.EXPIRE_OLD_FORMS));
		formConfigurationContext.setExpireOldFormsInterval(ParamUtil.getInteger(portletRequest, FormPortletRequestKeys.EXPIRE_OLD_FORMS_INTERVAL));
		formConfigurationContext.setSaveAndContinueEnabled(ParamUtil.getBoolean(portletRequest, FormPortletRequestKeys.SAVE_AND_CONTINUE_ENABLED));

		return formConfigurationContext;
	}

}
