package com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriter;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterRequest;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterResponse;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, property = "ddm.form.instance.record.writer.type=xlsx", service = DDMFormInstanceRecordWriter.class)
public class DDMFormInstanceRecordVersionXLSXWriter implements DDMFormInstanceRecordWriter {

	@Override
	public DDMFormInstanceRecordWriterResponse write(DDMFormInstanceRecordWriterRequest ddmFormInstanceRecordWriterRequest) throws Exception {

		int rowIndex = 0;

		Map<String, String> ddmFormFieldsLabel = ddmFormInstanceRecordWriterRequest.getDDMFormFieldsLabel();

		List<Map<String, String>> ddmFormFieldsValueList = ddmFormInstanceRecordWriterRequest.getDDMFormFieldValues();

		try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(); Workbook workbook = new XSSFWorkbook()) {

			Sheet sheet = workbook.createSheet();

			for (Map<String, String> ddmFormFieldsValue : ddmFormFieldsValueList) {
				for (Map.Entry<String, String> entry : ddmFormFieldsValue.entrySet()) {
					if (Validator.isNotNull(entry.getValue())) {
						createRow(workbook, rowIndex++, ddmFormFieldsLabel.get(entry.getKey()), entry.getValue(), sheet);
					}
				}
			}

			workbook.write(byteArrayOutputStream);

			DDMFormInstanceRecordWriterResponse.Builder builder = DDMFormInstanceRecordWriterResponse.Builder.newBuilder(byteArrayOutputStream.toByteArray());

			return builder.build();
		}
	}

	protected CellStyle createCellStyle(Workbook workbook, boolean bold, String fontName, short heightInPoints) {

		Font font = workbook.createFont();

		font.setBold(bold);
		font.setFontHeightInPoints(heightInPoints);
		font.setFontName(fontName);

		CellStyle style = workbook.createCellStyle();

		style.setFont(font);

		return style;
	}

	protected void createRow(Workbook workbook, int rowIndex, String label, String value, Sheet sheet) {

		Row row = sheet.createRow(rowIndex);

		Cell labelCell = row.createCell(0, CellType.STRING);
		labelCell.setCellStyle(createCellStyle(workbook, true, "Courier New", (short) 14));
		labelCell.setCellValue(label);

		Cell valueCell = row.createCell(1, CellType.STRING);
		valueCell.setCellStyle(createCellStyle(workbook, false, "Courier New", (short) 12));
		valueCell.setCellValue(value);

	}

}