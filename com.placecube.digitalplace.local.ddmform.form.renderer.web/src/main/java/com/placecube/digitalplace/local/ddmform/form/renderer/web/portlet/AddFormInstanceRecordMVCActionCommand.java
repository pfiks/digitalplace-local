package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceSettings;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.ddmform.fieldtype.payment.override.PaymentAddFormInstanceRecordService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormActionURLHelper;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntryInstanceService;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.ADD_FORM_INSTANCE_RECORD }, service = MVCActionCommand.class)
public class AddFormInstanceRecordMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AddFormInstanceRecordMVCActionCommand.class);

	@Reference
	private AddFormInstanceRecordMVCCommandHelper addFormInstanceRecordMVCCommandHelper;

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private PaymentAddFormInstanceRecordService paymentAddFormInstanceRecordService;

	@Reference
	private FormActionURLHelper formActionURLHelper;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		DDMFormInstance ddmFormInstance = formEntryInstanceService.getFormInstance(actionRequest);

		String redirectURL = StringPool.BLANK;

		try {

			if (SessionErrors.isEmpty(actionRequest)) {
				if (paymentAddFormInstanceRecordService.hasField(actionRequest, ddmFormInstance)) {
					redirectURL = processPaymentForm(actionRequest, actionResponse, ddmFormInstance);
				} else {
					redirectURL = processNonPaymentForm(actionRequest, actionResponse, ddmFormInstance);
				}
			} else {
				redirectURL = formActionURLHelper.getErrorRedirect(actionRequest);
			}

		} catch (Exception e) {

			LOG.debug(e);
			LOG.error("Unable to add form instance record. " + e.getMessage(), e);

			redirectURL = formActionURLHelper.getErrorRedirect(actionRequest);
		}

		if (Validator.isNotNull(redirectURL)) {
			actionRequest.setAttribute(WebKeys.REDIRECT, redirectURL);
			sendRedirect(actionRequest, actionResponse);
		}
	}

	private String processPaymentForm(ActionRequest actionRequest, ActionResponse actionResponse, DDMFormInstance ddmFormInstance) throws Exception {

		addFormInstanceRecordMVCCommandHelper.saveRecord(actionRequest, ddmFormInstance, WorkflowConstants.STATUS_DRAFT);

		ServiceContext serviceContext = addFormInstanceRecordMVCCommandHelper.getServiceContext(actionRequest);
		addFormInstanceRecordMVCCommandHelper.setRedirectAttributeInServiceContext(actionRequest, serviceContext);

		String redirectURL = paymentAddFormInstanceRecordService.doProcessAction(actionRequest, ddmFormInstance, serviceContext);
		redirectURL = formActionURLHelper.getSuccessRedirect(actionRequest, redirectURL);

		return redirectURL;
	}

	private String processNonPaymentForm(ActionRequest actionRequest, ActionResponse actionResponse, DDMFormInstance ddmFormInstance) throws Exception {

		DDMFormInstanceRecord ddmFormInstanceRecord = addFormInstanceRecordMVCCommandHelper.saveRecord(actionRequest, ddmFormInstance, WorkflowConstants.STATUS_DRAFT);
		int status = addFormInstanceRecordMVCCommandHelper.executeDataConsumers(actionRequest, ddmFormInstance, ddmFormInstanceRecord);
		ddmFormInstanceRecord = addFormInstanceRecordMVCCommandHelper.updateFormInstanceRecord(actionRequest, ddmFormInstanceRecord, status);

		DDMFormInstanceSettings formInstanceSettings = ddmFormInstance.getSettingsModel();
		String redirectURL = formInstanceSettings.redirectURL();

		if (Validator.isNotNull(redirectURL)) {
			redirectURL = formActionURLHelper.getSuccessRedirect(actionRequest, redirectURL);
		} else {
			redirectURL = formActionURLHelper.getFormSuccessPageRedirect(actionRequest, ddmFormInstanceRecord.getFormInstanceRecordId());
		}

		return redirectURL;
	}

}