package com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.action;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.model.FormConfigurationContext;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + FormPortletKeys.FORM, service = ConfigurationAction.class)
public class FormPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private FormConfigurationService formConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		List<DDMFormInstance> ddmFormInstances = ddmFormInstanceLocalService.getFormInstances(themeDisplay.getScopeGroupId());

		FormPortletInstanceConfiguration formPortletInstanceConfiguration = formConfigurationService.getConfiguration(themeDisplay);

		httpServletRequest.setAttribute(FormPortletRequestKeys.DDM_FORM_INSTANCES, ddmFormInstances);
		httpServletRequest.setAttribute(FormPortletRequestKeys.CONTINUE_SAVED_FORM_ARTICLE_ID_VIEW, formPortletInstanceConfiguration.continueSavedFormArticleIdView());
		httpServletRequest.setAttribute(FormPortletRequestKeys.FORM_INSTANCE_ID, formPortletInstanceConfiguration.formInstanceId());
		httpServletRequest.setAttribute(FormPortletRequestKeys.ARTICLE_ID, formPortletInstanceConfiguration.articleId());
		httpServletRequest.setAttribute(FormPortletRequestKeys.ARTICLE_ID_EXCEPTION, formPortletInstanceConfiguration.articleIdException());
		httpServletRequest.setAttribute(FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_EMAIL, formPortletInstanceConfiguration.saveProgressArticleIdEmail());
		httpServletRequest.setAttribute(FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_VIEW, formPortletInstanceConfiguration.saveProgressArticleIdView());
		httpServletRequest.setAttribute(FormPortletRequestKeys.HIDE_PAGINATION, formPortletInstanceConfiguration.hidePagination());
		httpServletRequest.setAttribute(FormPortletRequestKeys.DELETE_DATA_AFTER_SUBMISSION, formPortletInstanceConfiguration.deleteDataAfterSubmission());
		httpServletRequest.setAttribute(FormPortletRequestKeys.EXPIRE_OLD_FORMS, formPortletInstanceConfiguration.expireOldForms());
		httpServletRequest.setAttribute(FormPortletRequestKeys.EXPIRE_OLD_FORMS_INTERVAL, formPortletInstanceConfiguration.expireOldFormsInterval());
		httpServletRequest.setAttribute(FormPortletRequestKeys.SAVE_AND_CONTINUE_ENABLED, formPortletInstanceConfiguration.saveAndContinueEnabled());

		super.include(portletConfig, httpServletRequest, httpServletResponse);

	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		FormConfigurationContext formConfigurationContext = formConfigurationService.getFormConfigurationContext(actionRequest);

		setPreference(actionRequest, FormPortletRequestKeys.CONTINUE_SAVED_FORM_ARTICLE_ID_VIEW, formConfigurationContext.getContinueSavedFormArticleIdView());
		setPreference(actionRequest, FormPortletRequestKeys.FORM_INSTANCE_ID, String.valueOf(formConfigurationContext.getFormInstanceId()));
		setPreference(actionRequest, FormPortletRequestKeys.ARTICLE_ID, String.valueOf(formConfigurationContext.getArticleId()));
		setPreference(actionRequest, FormPortletRequestKeys.ARTICLE_ID_EXCEPTION, String.valueOf(formConfigurationContext.getArticleIdException()));
		setPreference(actionRequest, FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_EMAIL, String.valueOf(formConfigurationContext.getSaveProgressArticleIdEmail()));
		setPreference(actionRequest, FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_VIEW, String.valueOf(formConfigurationContext.getSaveProgressArticleIdView()));
		setPreference(actionRequest, FormPortletRequestKeys.HIDE_PAGINATION, String.valueOf(formConfigurationContext.isHidePagination()));
		setPreference(actionRequest, FormPortletRequestKeys.DELETE_DATA_AFTER_SUBMISSION, String.valueOf(formConfigurationContext.isDeleteDataAfterSubmission()));
		setPreference(actionRequest, FormPortletRequestKeys.EXPIRE_OLD_FORMS, String.valueOf(formConfigurationContext.isExpireOldForms()));
		setPreference(actionRequest, FormPortletRequestKeys.EXPIRE_OLD_FORMS_INTERVAL, String.valueOf(formConfigurationContext.getExpireOldFormsInterval()));
		setPreference(actionRequest, FormPortletRequestKeys.SAVE_AND_CONTINUE_ENABLED, String.valueOf(formConfigurationContext.isSaveAndContinueEnabled()));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
