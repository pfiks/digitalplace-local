package com.placecube.digitalplace.local.ddmform.form.renderer.web.service;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.pfiks.mail.service.MailService;
import com.placecube.ddmform.fieldtype.notification.util.NotificationDDMFormFieldTypeUtil;
import com.placecube.ddmform.utils.DDMFormFieldTypeUtil;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = SaveProgressEmailNotificationService.class)
public class SaveProgressEmailNotificationService {

	private static final Log LOG = LogFactoryUtil.getLog(SaveProgressEmailNotificationService.class);

	@Reference
	private DDMFormFieldTypeUtil ddmFormFieldTypeUtil;

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private JournalArticleService journalArticleService;

	@Reference
	private MailService mailService;

	@Reference
	private NotificationDDMFormFieldTypeUtil notificationDDMFormFieldTypeUtil;

	public void sendSaveProgressEmail(DDMFormValues ddmFormValues, ThemeDisplay themeDisplay, ActionRequest actionRequest) {

		Optional<DDMFormField> ddmFormField = notificationDDMFormFieldTypeUtil.getFirstNotificatonDDMFormField(ddmFormValues);
		if (ddmFormField.isPresent()) {
			String emailAddressFormValue = notificationDDMFormFieldTypeUtil.getEmailAddressFormValue(ddmFormValues, ddmFormField.get());
			String replyToAddressSettingsValue = notificationDDMFormFieldTypeUtil.getReplyToAddressSettingsValue(ddmFormField.get(), ddmFormValues);

			FormPortletInstanceConfiguration formPortletInstanceConfiguration;
			try {
				formPortletInstanceConfiguration = formConfigurationService.getConfiguration(themeDisplay);
				JournalArticle jouranlArticle = journalArticleService.getArticle(themeDisplay.getScopeGroupId(), formPortletInstanceConfiguration.saveProgressArticleIdEmail());

				String uniqueReference = formEntrySessionHelper.getUniqueReference(actionRequest);
				Locale locale = themeDisplay.getLocale();
				Map<String, Object> labelValueMap = ddmFormFieldTypeUtil.getLabelValueMap(ddmFormValues, ddmFormValues.getDefaultLocale());
				labelValueMap.put("UniqueReferenceNumber", uniqueReference);

				String subject = getSubject(jouranlArticle, locale);
				subject = replacePlaceholders(labelValueMap, subject);

				String body = getBody(jouranlArticle, locale);
				body = replacePlaceholders(labelValueMap, body);

				LOG.debug("Email [to: " + emailAddressFormValue + ", ReplyAddressFrom: " + replyToAddressSettingsValue + ", Subject: " + subject + ", Body: " + body + "]");

				mailService.sendEmail(emailAddressFormValue, emailAddressFormValue, replyToAddressSettingsValue, replyToAddressSettingsValue, subject, body);

			} catch (Exception e) {
				LOG.error("Error sending the save progress email notification.", e);
			}

		}

	}

	private String getBody(JournalArticle journalArticle, Locale locale) throws PortletException {
		Optional<String> body = journalArticleRetrievalService.getFieldValue(journalArticle, "Body", locale);

		if (body.isPresent()) {
			return body.get();
		} else {
			throw new PortletException("Email body field is not present in web content article - articleId: " + journalArticle.getArticleId());
		}

	}

	private String getSubject(JournalArticle journalArticle, Locale locale) throws PortletException {
		Optional<String> subject = journalArticleRetrievalService.getFieldValue(journalArticle, "Subject", locale);
		if (subject.isPresent()) {
			return subject.get();
		} else {
			throw new PortletException("Email subject field is not present in web content article - articleId: " + journalArticle.getArticleId());
		}
	}

	private String replacePlaceholders(Map<String, Object> labelValueMap, String stringToReplace) {
		for (Map.Entry<String, Object> map : labelValueMap.entrySet()) {
			stringToReplace = stringToReplace.replace("$" + map.getKey() + "$", String.valueOf(map.getValue()));
		}

		return stringToReplace;

	}
}
