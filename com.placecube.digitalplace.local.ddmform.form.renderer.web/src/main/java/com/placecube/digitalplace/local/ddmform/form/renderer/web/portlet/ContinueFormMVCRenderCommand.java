package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormConfigurationService;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.CONTINUE_FORM }, service = MVCRenderCommand.class)
public class ContinueFormMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		setArticleIdAttribute(renderRequest);
		return "/continue_previous_submission.jsp";
	}

	private void setArticleIdAttribute(RenderRequest renderRequest) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		Optional<String> continueSavedFormArticleIdView = formConfigurationService.getArticleIdContinueSavedFormView(themeDisplay);
		if (continueSavedFormArticleIdView.isPresent()) {
			JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(themeDisplay.getScopeGroupId(), continueSavedFormArticleIdView.get(), WorkflowConstants.STATUS_APPROVED);

			if (Validator.isNotNull(journalArticle)) {
				renderRequest.setAttribute(FormPortletRequestKeys.CONTINUE_SAVED_FORM_ARTICLE_ID_VIEW, continueSavedFormArticleIdView.get());
			}
		}

	}
}
