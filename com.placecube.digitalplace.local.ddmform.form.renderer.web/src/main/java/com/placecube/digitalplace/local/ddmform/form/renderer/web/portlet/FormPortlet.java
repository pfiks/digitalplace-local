package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=digitalplace form-renderer", "com.liferay.portlet.display-category=category.forms",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "javax.portlet.init-param.add-process-action-success-action=false", "com.liferay.portlet.instanceable=true",
		"javax.portlet.name=" + FormPortletKeys.FORM, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user", "javax.portlet.supports.mime-type=text/html",
		"javax.portlet.version=3.0" }, service = Portlet.class)
public class FormPortlet extends MVCPortlet {

}
