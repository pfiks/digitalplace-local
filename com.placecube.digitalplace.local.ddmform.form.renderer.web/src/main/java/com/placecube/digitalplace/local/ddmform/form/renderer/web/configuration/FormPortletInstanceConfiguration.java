package com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@ExtendedObjectClassDefinition(category = "forms", scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(id = "com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration", localization = "content/Language", name = "form-portlet-instance-configuration-name")
public interface FormPortletInstanceConfiguration {

	@Meta.AD(name = "article-id", description = "article-id-help-message", required = false, deflt = "", type = Type.String) String articleId();

	@Meta.AD(name = "article-id-exception", description = "article-id-exception-help-message", required = false, deflt = "", type = Type.String) String articleIdException();

	@Meta.AD(name = "continue-saved-form-article-id-view", description = "continue-saved-form-article-id-view-help-message", required = false, deflt = "", type = Type.String) String continueSavedFormArticleIdView();

	@Meta.AD(name = "delete-data-after-submission", description = "delete-data-after-submission-help-message", required = false, deflt = "false", type = Type.Boolean) boolean deleteDataAfterSubmission();

	@Meta.AD(name = "expire-old-forms", description = "expire-old-forms-help-message", required = false, deflt = "false", type = Type.Boolean) boolean expireOldForms();

	@Meta.AD(name = "expire-old-forms-interval", description = "expire-old-forms-interval-help-message", required = false, deflt = "31", type = Type.Integer) int expireOldFormsInterval();

	@Meta.AD(required = false, deflt = "0", type = Type.Long) long formInstanceId();

	@Meta.AD(name = "hide-pagination", description = "hide-pagination-help-message", required = false, deflt = "true", type = Type.Boolean) boolean hidePagination();

	@Meta.AD(name = "save-and-continue-enabled", description = "save-and-continue-enabled-help-message", required = false, deflt = "false", type = Type.Boolean) boolean saveAndContinueEnabled();

	@Meta.AD(name = "save-progress-article-id-email", description = "save-progress-article-id-email-help-message", required = false, deflt = "", type = Type.String) String saveProgressArticleIdEmail();

	@Meta.AD(name = "save-progress-article-id-view", description = "save-progress-article-id-view-help-message", required = false, deflt = "", type = Type.String) String saveProgressArticleIdView();

}
