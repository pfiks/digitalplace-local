package com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.exporter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.exception.FormInstanceRecordExporterException;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeServicesRegistry;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordExporterResponse;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriter;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterRegistry;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterRequest;
import com.liferay.dynamic.data.mapping.io.exporter.DDMFormInstanceRecordWriterResponse;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordVersionLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.HtmlParser;

@Component(immediate = true, service = DDMFormInstanceRecordVersionExporter.class)
public class DDMFormInstanceRecordVersionExporter {

	@Reference
	private DDMFormFieldTypeServicesRegistry ddmFormFieldTypeServicesRegistry;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private DDMFormInstanceRecordVersionLocalService ddmFormInstanceRecordVersionLocaleService;

	@Reference
	private DDMFormInstanceRecordWriterRegistry ddmFormInstanceRecordWriterRegistry;

	@Reference
	private HtmlParser htmlParser;

	public DDMFormInstanceRecordExporterResponse export(DDMFormInstanceRecordVersionExporterRequest ddmFormInstanceRecordVersionExporterRequest) throws PortalException {

		DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion = ddmFormInstanceRecordVersionExporterRequest.getDDMFormInstanceRecordVersion();
		Locale locale = ddmFormInstanceRecordVersionExporterRequest.getLocale();
		String type = ddmFormInstanceRecordVersionExporterRequest.getType();
		String formInstanceName = ddmFormInstanceRecordVersion.getFormInstance().getName(locale);

		DDMFormInstanceRecordExporterResponse.Builder builder = DDMFormInstanceRecordExporterResponse.Builder.newBuilder();

		try {

			Map<String, DDMFormField> ddmFormFields = ddmFormInstanceRecordVersion.getDDMForm().getNontransientDDMFormFieldsMap(true);

			byte[] content = write(type, getDDMFormFieldsLabel(formInstanceName, ddmFormFields, locale),
					getDDMFormFieldValues(ddmFormFields, ddmFormInstanceRecordVersion.getFormInstanceRecord(), locale));

			builder = builder.withContent(content);
		} catch (Exception e) {
			throw new FormInstanceRecordExporterException(e);
		}

		return builder.build();
	}

	private Map<String, String> getDDMFormFieldsLabel(String formInstanceName, Map<String, DDMFormField> ddmFormFieldMap, Locale locale) {

		Map<String, String> ddmFormFieldsLabel = new LinkedHashMap<>();

		Collection<DDMFormField> ddmFormFields = ddmFormFieldMap.values();

		Stream<DDMFormField> stream = ddmFormFields.stream();

		stream.forEach(field -> {
			if (!field.getName().contains("hidden")) {
				LocalizedValue localizedValue = field.getLabel();

				ddmFormFieldsLabel.put(field.getName(), localizedValue.getString(locale));
			}
		});
		ddmFormFieldsLabel.put("formInstanceName", formInstanceName);

		return ddmFormFieldsLabel;
	}

	private String getDDMFormFieldValue(DDMFormField ddmFormField, Map<String, List<DDMFormFieldValue>> ddmFormFieldValueMap, Locale locale) {

		List<DDMFormFieldValue> ddmFormFieldValues = ddmFormFieldValueMap.get(ddmFormField.getName());

		com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldValueRenderer ddmFormFieldValueRenderer = ddmFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(ddmFormField.getType());

		return htmlParser.render(ddmFormFieldValueRenderer.render(ddmFormFieldValues.get(0), locale));
	}

	private List<Map<String, String>> getDDMFormFieldValues(Map<String, DDMFormField> ddmFormFields, DDMFormInstanceRecord ddmFormInstanceRecord, Locale locale) throws PortalException {

		List<Map<String, String>> ddmFormFieldValues = new ArrayList<>();

		com.liferay.dynamic.data.mapping.storage.DDMFormValues ddmFormValues = ddmFormInstanceRecord.getDDMFormValues();

		Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = ddmFormValues.getDDMFormFieldValuesMap();

		Map<String, String> ddmFormFieldsValue = new LinkedHashMap<>();

		for (Map.Entry<String, DDMFormField> entry : ddmFormFields.entrySet()) {

			if (!ddmFormFieldValuesMap.containsKey(entry.getKey())) {
				ddmFormFieldsValue.put(entry.getKey(), StringPool.BLANK);
			} else {
				ddmFormFieldsValue.put(entry.getKey(), getDDMFormFieldValue(entry.getValue(), ddmFormFieldValuesMap, locale));
			}
		}

		ddmFormFieldValues.add(ddmFormFieldsValue);

		return ddmFormFieldValues;
	}

	private byte[] write(String type, Map<String, String> ddmFormFieldsLabel, List<Map<String, String>> ddmFormFieldValues) throws Exception {

		DDMFormInstanceRecordWriter ddmFormInstanceRecordWriter = ddmFormInstanceRecordWriterRegistry.getDDMFormInstanceRecordWriter(type);

		DDMFormInstanceRecordWriterRequest.Builder builder = DDMFormInstanceRecordWriterRequest.Builder.newBuilder(ddmFormFieldsLabel, ddmFormFieldValues);

		DDMFormInstanceRecordWriterRequest ddmFormInstanceRecordWriterRequest = builder.build();

		DDMFormInstanceRecordWriterResponse ddmFormInstanceRecordWriterResponse = ddmFormInstanceRecordWriter.write(ddmFormInstanceRecordWriterRequest);

		return ddmFormInstanceRecordWriterResponse.getContent();
	}

}
