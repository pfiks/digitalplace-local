package com.placecube.digitalplace.local.ddmform.form.renderer.web.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletSession;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.FormEntryLocalService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet.FormPortlet;

@Component(immediate = true, service = FormEntrySessionHelper.class)
public class FormEntrySessionHelper {

	private static final Log LOG = LogFactoryUtil.getLog(FormEntrySessionHelper.class);

	@Reference
	private FormEntryLocalService formEntryLocalService;

	@Reference
	private Portal portal;

	public void approvePendingFormEntry(long formInstanceRecordVersionId) {
		try {
			FormEntry formEntry = formEntryLocalService.getFormEntryByFormInstanceRecordVersionIdAndStatus(formInstanceRecordVersionId, WorkflowConstants.STATUS_PENDING);
			formEntryLocalService.updateFormEntry(formEntry, WorkflowConstants.STATUS_APPROVED);
		} catch (PortalException e) {
			LOG.error("Could not update status for for entry - formInstanceRecordVersionId: " + formInstanceRecordVersionId + ", status: STATUS_PENDING");
		}
	}

	public void createFormEntry(ActionRequest actionRequest, long userId, DDMFormInstanceRecordVersion formInstanceRecordVersion, long plid) throws PortalException {
		createFormEntry(actionRequest, userId, null, formInstanceRecordVersion, plid);
	}

	public void createFormEntry(ActionRequest actionRequest, long userId, String uniqueReference, DDMFormInstanceRecordVersion formInstanceRecordVersion, long plid) throws PortalException {
		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
		FormEntry formEntry;

		if (Validator.isNull(uniqueReference)) {
			formEntry = formEntryLocalService.addFormEntry(userId, plid, formInstanceRecordVersion.getFormInstanceRecordVersionId(), formInstanceRecordVersion.getStatus(), serviceContext);
		} else {
			formEntry = formEntryLocalService.addFormEntry(userId, plid, formInstanceRecordVersion.getFormInstanceRecordVersionId(), uniqueReference, formInstanceRecordVersion.getStatus(), serviceContext);
		}

		saveReferenceToSession(formEntry.getUniqueReference(), actionRequest);
	}

	public FormEntry getDraftFormEntryFromSession(PortletRequest portletRequest) {
		return getFormEntryByUniqueReferenceAndDraftStatus(getReferenceFromSession(portletRequest));
	}

	public FormEntry getFormEntryByUniqueReferenceAndDraftStatus(String uniqueReference) {
		List<FormEntry> formEntries = formEntryLocalService.getFormEntriesByUniqueReferenceAndStatus(uniqueReference, WorkflowConstants.STATUS_DRAFT);

		return formEntries.size() == 1 ? formEntries.get(0) : null;
	}

	public Optional<FormEntry> getLatestFormEntryByUniqueReferenceAndStatus(String uniqueReference, int status) {
		List<FormEntry> formEntries = formEntryLocalService.getFormEntriesByUniqueReferenceAndStatus(uniqueReference, status);

		return formEntries.stream().sorted(Comparator.comparing(FormEntry::getFormInstanceRecordVersionId).reversed()).findFirst();
	}

	public String getUniqueReference(PortletRequest portletRequest) {
		String uniqueReference = getReferenceFromSession(portletRequest);
		return Validator.isNull(uniqueReference) ? ParamUtil.getString(portletRequest, FormPortletRequestKeys.UNIQUE_REFERENCE, StringPool.BLANK) : uniqueReference;
	}

	public void removeReferenceFromSession(PortletRequest portletRequest) {
		removeAttributeFromSession(portletRequest, FormPortletRequestKeys.UNIQUE_REFERENCE);
	}

	public void saveReferenceToSession(String uniqueReference, ActionRequest actionRequest) {
		saveAttributeToSession(actionRequest, FormPortletRequestKeys.UNIQUE_REFERENCE, uniqueReference);
	}

	public FormEntry updateFormEntry(DDMFormInstanceRecordVersion formInstanceRecordVersion) throws PortalException {
		FormEntry formEntry = formEntryLocalService.getFormEntryByFormInstanceRecordVersionId(formInstanceRecordVersion.getFormInstanceRecordVersionId());
		formEntry = formEntryLocalService.updateFormEntry(formEntry, formInstanceRecordVersion.getStatus());
		
		List<FormEntry> pendingFormEntriesWithUniqueReferences = formEntryLocalService.getFormEntriesByUniqueReferenceAndStatus(formEntry.getUniqueReference(), WorkflowConstants.STATUS_PENDING);
		for (FormEntry pendingFormEntriesWithUniqueReference : pendingFormEntriesWithUniqueReferences) {
			pendingFormEntriesWithUniqueReference.setModifiedDate(formEntry.getModifiedDate());
			formEntryLocalService.updateFormEntry(pendingFormEntriesWithUniqueReference);
		}

		return formEntry;
	}

	private String buildAttributeName(String attribute, PortletRequest portletRequest) {
		return getPrefixKey(portletRequest).concat(attribute);
	}

	private Object getAttributeFromSession(PortletRequest portletRequest, String attribute) {
		return getHttpSession(portletRequest).getAttribute(buildAttributeName(attribute, portletRequest));
	}

	private HttpSession getHttpSession(PortletRequest portletRequest) {
		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(portletRequest);
		return httpServletRequest.getSession();
	}

	private String getPrefixKey(PortletRequest portletRequest) {
		StringBundler sb = new StringBundler(6);

		LiferayPortletRequest liferayPortletRequest = portal.getLiferayPortletRequest(portletRequest);

		sb.append(LiferayPortletSession.PORTLET_SCOPE_NAMESPACE);
		sb.append(liferayPortletRequest.getPortletName());
		sb.append(LiferayPortletSession.LAYOUT_SEPARATOR);
		sb.append(liferayPortletRequest.getPlid());
		sb.append(StringPool.QUESTION);
		sb.append(FormPortlet.class.getName());
		sb.append(StringPool.UNDERLINE);

		return sb.toString();
	}

	private String getReferenceFromSession(PortletRequest portletRequest) {
		return (String) getAttributeFromSession(portletRequest, FormPortletRequestKeys.UNIQUE_REFERENCE);
	}

	private void removeAttributeFromSession(PortletRequest portletRequest, String attribute) {
		getHttpSession(portletRequest).removeAttribute(buildAttributeName(attribute, portletRequest));
	}

	private void saveAttributeToSession(PortletRequest portletRequest, String attribute, Object value) {
		getHttpSession(portletRequest).setAttribute(buildAttributeName(attribute, portletRequest), value);
	}
}
