package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormConfigurationService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntryInstanceService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntrySessionHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class FormMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		FormPortletInstanceConfiguration formPortletInstanceConfiguration = formConfigurationService.getConfiguration(themeDisplay);

		long formInstanceId = formPortletInstanceConfiguration.formInstanceId();
		renderRequest.setAttribute(FormPortletRequestKeys.FORM_INSTANCE_ID, formInstanceId);

		boolean hasAddFormInstanceRecordPermission = formEntryInstanceService.hasAddFormInstanceRecordPermission(formInstanceId, themeDisplay);

		if (hasAddFormInstanceRecordPermission) {

			renderRequest.setAttribute(FormPortletRequestKeys.DDM_FORM_INSTANCE_RECORD_VERSION_ID, formEntryInstanceService.getLatestDraftFormInstanceRecordVersionId(renderRequest));

		}

		renderRequest.setAttribute(FormPortletRequestKeys.ARTICLE_ID, formPortletInstanceConfiguration.articleId());
		renderRequest.setAttribute(FormPortletRequestKeys.ADD_FORM_INSTANCE_RECORD_PERMISSION, hasAddFormInstanceRecordPermission);
		renderRequest.setAttribute(FormPortletRequestKeys.HIDE_PAGINATION, formPortletInstanceConfiguration.hidePagination());
		renderRequest.setAttribute(FormPortletRequestKeys.SAVE_AND_CONTINUE_ENABLED, formPortletInstanceConfiguration.saveAndContinueEnabled());

		return "/view.jsp";
	}

}
