package com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.model;

public class FormConfigurationContext {

	private String articleId;
	private String articleIdException;
	private String continueSavedFormArticleIdView;
	private boolean deleteDataAfterSubmission;
	private boolean expireOldForms;
	private int expireOldFormsInterval;
	private long formInstanceId;
	private boolean hidePagination;
	private boolean saveAndContinueEnabled;
	private String saveProgressArticleIdEmail;
	private String saveProgressArticleIdView;

	private FormConfigurationContext() {

	}

	public static FormConfigurationContext newInstance() {
		return new FormConfigurationContext();
	}

	public String getArticleId() {
		return articleId;
	}

	public String getArticleIdException() {
		return articleIdException;
	}

	public String getContinueSavedFormArticleIdView() {
		return continueSavedFormArticleIdView;
	}

	public int getExpireOldFormsInterval() {
		return expireOldFormsInterval;
	}

	public long getFormInstanceId() {
		return formInstanceId;
	}

	public String getSaveProgressArticleIdEmail() {
		return saveProgressArticleIdEmail;
	}

	public String getSaveProgressArticleIdView() {
		return saveProgressArticleIdView;
	}

	public boolean isDeleteDataAfterSubmission() {
		return deleteDataAfterSubmission;
	}

	public boolean isExpireOldForms() {
		return expireOldForms;
	}

	public boolean isHidePagination() {
		return hidePagination;
	}

	public boolean isSaveAndContinueEnabled() {
		return saveAndContinueEnabled;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public void setArticleIdException(String articleIdException) {
		this.articleIdException = articleIdException;
	}

	public void setContinueSavedFormArticleIdView(String continueSavedFormArticleIdView) {
		this.continueSavedFormArticleIdView = continueSavedFormArticleIdView;
	}

	public void setDeleteDataAfterSubmission(boolean deleteDataAfterSubmission) {
		this.deleteDataAfterSubmission = deleteDataAfterSubmission;
	}

	public void setExpireOldForms(boolean expireOldForms) {
		this.expireOldForms = expireOldForms;
	}

	public void setExpireOldFormsInterval(int expireOldFormsInterval) {
		this.expireOldFormsInterval = expireOldFormsInterval;
	}

	public void setFormInstanceId(long formInstanceId) {
		this.formInstanceId = formInstanceId;
	}

	public void setHidePagination(boolean hidePagination) {
		this.hidePagination = hidePagination;
	}

	public void setSaveAndContinueEnabled(boolean saveAndContinueEnabled) {
		this.saveAndContinueEnabled = saveAndContinueEnabled;
	}

	public void setSaveProgressArticleIdEmail(String saveProgressArticleIdEmail) {
		this.saveProgressArticleIdEmail = saveProgressArticleIdEmail;
	}

	public void setSaveProgressArticleIdView(String saveProgressArticleIdView) {
		this.saveProgressArticleIdView = saveProgressArticleIdView;
	}

}
