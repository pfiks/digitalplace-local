package com.placecube.digitalplace.local.ddmform.form.renderer.web.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.constants.DDMActionKeys;
import com.liferay.dynamic.data.mapping.form.values.factory.DDMFormValuesFactory;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordVersionLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.ddmform.fieldtype.notification.override.NotificationAddFormInstanceRecordService;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;
import com.placecube.digitalplace.local.ddmform.form.renderer.service.FormEntryLocalService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.configuration.FormPortletInstanceConfiguration;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.security.permission.resource.DDMFormInstancePermission;

@Component(immediate = true, service = FormEntryInstanceService.class)
public class FormEntryInstanceService {

	private static final Log LOG = LogFactoryUtil.getLog(FormEntryInstanceService.class);

	@Reference
	private DDMFormInstanceRecordService ddmFormInstanceRecordService;

	@Reference
	private DDMFormInstanceRecordVersionLocalService ddmFormInstanceRecordVersionLocalService;

	@Reference
	private DDMFormInstanceService ddmFormInstanceService;

	@Reference
	private DDMFormValuesFactory ddmFormValuesFactory;

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private FormEntryLocalService formEntryLocalService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Reference
	private NotificationAddFormInstanceRecordService notificationAddFormInstanceRecordService;

	@Reference
	private Portal portal;

	public DDMFormInstanceRecord addOrUpdateDraftFormInstanceRecord(DDMFormInstance ddmFormInstance, DDMFormValues ddmFormValues, ActionRequest actionRequest, ThemeDisplay themeDisplay)
			throws PortalException {
		ServiceContext serviceContext = getServiceContext(actionRequest);

		serviceContext.setWorkflowAction(WorkflowConstants.ACTION_SAVE_DRAFT);
		serviceContext.setAttribute(FormPortletRequestKeys.STATUS, WorkflowConstants.STATUS_DRAFT);

		return addOrUpdateFormInstanceRecord(ddmFormInstance, ddmFormValues, actionRequest, themeDisplay, serviceContext, false);
	}

	public DDMFormInstanceRecord addOrUpdateFormInstanceRecord(DDMFormInstance ddmFormInstance, DDMFormValues ddmFormValues, ActionRequest actionRequest, ThemeDisplay themeDisplay,
			boolean deleteApprovedFormEntries) throws PortalException {

		ServiceContext serviceContext = getServiceContext(actionRequest);

		return addOrUpdateFormInstanceRecord(ddmFormInstance, ddmFormValues, actionRequest, themeDisplay, serviceContext, deleteApprovedFormEntries);
	}

	public DDMFormValues createDDMFormValues(ActionRequest actionRequest, DDMForm ddmForm) {
		return ddmFormValuesFactory.create(actionRequest, ddmForm);
	}

	public List<DDMFormInstanceRecordVersion> fetchPendingFormInstanceRecordVersions(PortletRequest portletRequest, ThemeDisplay themeDisplay) {
		String uniqueReference = formEntrySessionHelper.getUniqueReference(portletRequest);
		List<DDMFormInstanceRecordVersion> formInstanceRecordVersions = new ArrayList<>();

		if (Validator.isNotNull(uniqueReference)) {
			DynamicQuery dynamicQuery = formEntryLocalService.dynamicQuery();
			dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", themeDisplay.getCompanyId()));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", themeDisplay.getScopeGroupId()));
			dynamicQuery.add(RestrictionsFactoryUtil.eq(FormPortletRequestKeys.UNIQUE_REFERENCE, uniqueReference));
			dynamicQuery.add(RestrictionsFactoryUtil.eq(FormPortletRequestKeys.STATUS, WorkflowConstants.STATUS_PENDING));
			dynamicQuery.addOrder(OrderFactoryUtil.asc("createDate"));

			formInstanceRecordVersions = mapDDMFormInstanceRecordVersionList(formEntryLocalService.dynamicQuery(dynamicQuery));
		}

		return formInstanceRecordVersions;
	}

	public DDMForm getDDMForm(DDMFormInstance ddmFormInstance) throws PortalException {

		DDMStructure ddmStructure = ddmFormInstance.getStructure();

		return ddmStructure.getDDMForm();
	}

	public List<String> getDistinctUniqueReferencesFormEntriesWithStatusApproved(long companyId, long plid) {

		DynamicQuery dynamicQuery = formEntryLocalService.dynamicQuery();
		Projection projection = ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property(FormPortletRequestKeys.UNIQUE_REFERENCE));
		dynamicQuery.setProjection(projection);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("plid", plid));
		dynamicQuery.add(RestrictionsFactoryUtil.eq(FormPortletRequestKeys.STATUS, WorkflowConstants.STATUS_APPROVED));

		return formEntryLocalService.dynamicQuery(dynamicQuery);
	}

	public DDMFormInstance getFormInstance(PortletRequest portletRequest) throws PortalException {

		return getFormInstance(ParamUtil.getLong(portletRequest, FormPortletRequestKeys.FORM_INSTANCE_ID));

	}

	public DDMFormInstance getFormInstanceFromConfiguration(PortletRequest portletRequest) throws PortletException, PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		FormPortletInstanceConfiguration formPortletInstanceConfiguration = formConfigurationService.getConfiguration(themeDisplay);

		long formInstanceId = formPortletInstanceConfiguration.formInstanceId();

		return getFormInstance(formInstanceId);
	}

	public String getLatestDraftFormInstanceRecordVersionId(PortletRequest portletRequest) {
		DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion = getLatestDraftFormInstanceRecordVersion(portletRequest);
		return ddmFormInstanceRecordVersion != null ? String.valueOf(ddmFormInstanceRecordVersion.getFormInstanceRecordVersionId()) : null;
	}

	public List<DDMFormInstanceRecordVersion> getPendingFormInstanceRecordVersions(RenderRequest renderRequest, ThemeDisplay themeDisplay) {

		return fetchPendingFormInstanceRecordVersions(renderRequest, themeDisplay);

	}

	public boolean hasAddFormInstanceRecordPermission(long formInstanceId, ThemeDisplay themeDisplay) {

		try {
			return DDMFormInstancePermission.contains(themeDisplay.getPermissionChecker(), formInstanceId, ActionKeys.VIEW)
					&& DDMFormInstancePermission.contains(themeDisplay.getPermissionChecker(), formInstanceId, DDMActionKeys.ADD_FORM_INSTANCE_RECORD);
		} catch (PortalException e) {
			return false;
		}

	}

	public boolean hasDraftFormInstanceRecordVersion(PortletRequest portletRequest) {
		return getLatestDraftFormInstanceRecordVersion(portletRequest) != null;
	}

	public List<DDMFormInstanceRecordVersion> mapDDMFormInstanceRecordVersionList(List<FormEntry> pendingFormEntries) {
		return pendingFormEntries.stream().map(formEntry -> {
			try {
				return ddmFormInstanceRecordVersionLocalService.getDDMFormInstanceRecordVersion(formEntry.getFormInstanceRecordVersionId());
			} catch (PortalException e) {
				LOG.error("Could not get DDMFormInstanceRecordVersion for form entry -formEntryId: " + formEntry.getFormEntryId(), e);
				return null;
			}
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	public void sendNotifications(DDMFormInstance ddmFormInstance, List<DDMFormInstanceRecordVersion> ddmFormInstanceRecordVersions, ServiceContext serviceContext) {
		for (DDMFormInstanceRecordVersion entry : ddmFormInstanceRecordVersions) {
			try {
				serviceContext.setAttribute("formInstanceRecordId", entry.getFormInstanceRecordId());
				notificationAddFormInstanceRecordService.sendNotifications(ddmFormInstance, entry.getDDMFormValues(), serviceContext);
			} catch (PortalException e) {
				LOG.error("Error sending notifications for formInstanceRecordVersionId: " + entry.getFormInstanceRecordVersionId(), e);
			}
		}
	}

	public DDMFormInstanceRecord addOrUpdateFormInstanceRecord(DDMFormInstance ddmFormInstance, DDMFormValues ddmFormValues, ActionRequest actionRequest, ThemeDisplay themeDisplay,
			ServiceContext serviceContext, boolean deleteApprovedFormEntries) throws PortalException {

		DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion = getLatestDraftFormInstanceRecordVersion(actionRequest);

		return addOrUpdateFormInstanceRecord(ddmFormInstance, ddmFormInstanceRecordVersion == null ? null : ddmFormInstanceRecordVersion.getFormInstanceRecord(), ddmFormValues, actionRequest,
				themeDisplay, serviceContext, deleteApprovedFormEntries);
	}

	public DDMFormInstanceRecord addOrUpdateFormInstanceRecord(DDMFormInstance ddmFormInstance, DDMFormInstanceRecord ddmFormInstanceRecord, DDMFormValues ddmFormValues, ActionRequest actionRequest,
			ThemeDisplay themeDisplay, ServiceContext serviceContext, boolean deleteApprovedFormEntries) throws PortalException {

		if (Validator.isNull(ddmFormInstanceRecord)) {
			ddmFormInstanceRecord = ddmFormInstanceRecordService.addFormInstanceRecord(ddmFormInstance.getGroupId(), ddmFormInstance.getFormInstanceId(), ddmFormValues, serviceContext);
			formEntrySessionHelper.createFormEntry(actionRequest, themeDisplay.getUserId(), ddmFormInstanceRecord.getFormInstanceRecordVersion(), themeDisplay.getPlid());
		} else {
			ddmFormInstanceRecord = ddmFormInstanceRecordService.updateFormInstanceRecord(ddmFormInstanceRecord.getFormInstanceRecordId(), false, ddmFormValues, serviceContext);
			formEntrySessionHelper.updateFormEntry(ddmFormInstanceRecord.getFormInstanceRecordVersion());
		}

		if (ddmFormInstanceRecord.getStatus() == WorkflowConstants.STATUS_APPROVED) {
			updatePendingFormInstanceRecordVersions(actionRequest, themeDisplay);
			String uniqueReference = formEntrySessionHelper.getUniqueReference(actionRequest);
			List<FormEntry> formEntriesWithUniqueReference = formEntryLocalService.getFormEntriesByUniqueReferenceAndStatus(uniqueReference, WorkflowConstants.STATUS_APPROVED);
			List<DDMFormInstanceRecordVersion> ddmFormInstanceRecordVersions = mapDDMFormInstanceRecordVersionList(formEntriesWithUniqueReference);

			sendNotifications(ddmFormInstance, ddmFormInstanceRecordVersions, serviceContext);

			if (deleteApprovedFormEntries) {
				formEntriesWithUniqueReference.forEach(formEntry -> formEntryLocalService.deleteFormEntry(formEntry));
			}
		}

		return ddmFormInstanceRecord;
	}

	private DDMFormInstance getFormInstance(long formInstanceId) throws PortalException {

		return ddmFormInstanceService.getFormInstance(formInstanceId);

	}

	private DDMFormInstanceRecordVersion getLatestDraftFormInstanceRecordVersion(PortletRequest portletRequest) {

		FormEntry formEntry = formEntrySessionHelper.getDraftFormEntryFromSession(portletRequest);

		return formEntry != null ? ddmFormInstanceRecordVersionLocalService.fetchDDMFormInstanceRecordVersion(formEntry.getFormInstanceRecordVersionId()) : null;
	}

	private ServiceContext getServiceContext(ActionRequest actionRequest) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstanceRecord.class.getName(), actionRequest);

		serviceContext.setRequest(portal.getHttpServletRequest(actionRequest));

		return serviceContext;
	}

	private void updatePendingFormInstanceRecordVersions(ActionRequest actionRequest, ThemeDisplay themeDisplay) {
		List<DDMFormInstanceRecordVersion> ddmFormInstanceRecordVersions = fetchPendingFormInstanceRecordVersions(actionRequest, themeDisplay);

		for (DDMFormInstanceRecordVersion entry : ddmFormInstanceRecordVersions) {
			entry.setStatus(WorkflowConstants.STATUS_APPROVED);
			ddmFormInstanceRecordVersionLocalService.updateDDMFormInstanceRecordVersion(entry);
			formEntrySessionHelper.approvePendingFormEntry(entry.getFormInstanceRecordVersionId());
		}
	}
}
