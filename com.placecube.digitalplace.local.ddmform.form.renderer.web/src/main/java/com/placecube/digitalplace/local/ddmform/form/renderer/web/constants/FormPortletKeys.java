package com.placecube.digitalplace.local.ddmform.form.renderer.web.constants;

public final class FormPortletKeys {

	public static final String FORM = "com_placecube_digitalplace_local_ddmform_form_renderer_FormPortlet";

	private FormPortletKeys() {

	}
}
