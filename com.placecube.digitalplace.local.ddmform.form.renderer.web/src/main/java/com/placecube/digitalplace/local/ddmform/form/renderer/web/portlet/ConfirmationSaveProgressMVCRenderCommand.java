package com.placecube.digitalplace.local.ddmform.form.renderer.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.service.JournalArticleService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.FormPortletRequestKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormActionURLHelper;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormConfigurationService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntryInstanceService;
import com.placecube.digitalplace.local.ddmform.form.renderer.web.service.FormEntrySessionHelper;

@Component(immediate = true, property = { "javax.portlet.name=" + FormPortletKeys.FORM, "mvc.command.name=" + MVCCommandKeys.SAVE_PROGRESS }, service = MVCRenderCommand.class)
public class ConfirmationSaveProgressMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ConfirmationSaveProgressMVCRenderCommand.class);

	@Reference
	private FormActionURLHelper formActionURLHelper;

	@Reference
	private FormConfigurationService formConfigurationService;

	@Reference
	private FormEntryInstanceService formEntryInstanceService;

	@Reference
	private FormEntrySessionHelper formEntrySessionHelper;

	@Reference
	private JournalArticleService journalArticleService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		String uniqueReference = formEntrySessionHelper.getUniqueReference(renderRequest);

		renderRequest.setAttribute(FormPortletRequestKeys.UNIQUE_REFERENCE, uniqueReference);

		renderRequest.setAttribute(FormPortletRequestKeys.RETURN_TO_FORM_LINK, formActionURLHelper.getDefaultRedirectURL(renderRequest));

		renderRequest.setAttribute(FormPortletRequestKeys.HAS_FORM_DRAFT, formEntryInstanceService.hasDraftFormInstanceRecordVersion(renderRequest));

		setSaveProgressArticleContentAsRequestAttribute(renderRequest,renderResponse, uniqueReference);

		return "/confirmation_save_progress.jsp";
	}

	@SuppressWarnings("deprecation")
	private void setSaveProgressArticleContentAsRequestAttribute(RenderRequest renderRequest, RenderResponse renderResponse, String uniqueReference) throws PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Optional<String> articleIdSaveProgressView = formConfigurationService.getArticleIdSaveProgressView(themeDisplay);

			if (articleIdSaveProgressView.isPresent()) {
				String contentArticleIdSaveProgressView;
				PortletRequestModel portletRequestModel = new PortletRequestModel(
						renderRequest, renderResponse);
				contentArticleIdSaveProgressView = journalArticleService.getArticleContent(themeDisplay.getScopeGroupId(), articleIdSaveProgressView.get(), themeDisplay.getLanguageId(),portletRequestModel, themeDisplay);
				contentArticleIdSaveProgressView = contentArticleIdSaveProgressView.replace("$UniqueReferencerNumber$", uniqueReference);
				renderRequest.setAttribute(FormPortletRequestKeys.SAVE_PROGRESS_ARTICLE_ID_VIEW, contentArticleIdSaveProgressView);
			}
		} catch (PortalException e) {
			LOG.error("Error getting save progress articleId to display in confirmation save progress view", e);
		}
	}
}
