package com.placecube.digitalplace.local.ddmform.form.renderer.web.internal.security.permission.resource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;

@Component(immediate = true, service = {})
public class DDMFormInstancePermission {

	private static ModelResourcePermission<DDMFormInstance> ddmFormInstanceModelResourcePermission;

	public static boolean contains(PermissionChecker permissionChecker, DDMFormInstance ddmFormInstance, String actionId) throws PortalException {

		return ddmFormInstanceModelResourcePermission.contains(permissionChecker, ddmFormInstance, actionId);
	}

	public static boolean contains(PermissionChecker permissionChecker, long formInstanceId, String actionId) throws PortalException {

		return ddmFormInstanceModelResourcePermission.contains(permissionChecker, formInstanceId, actionId);
	}

	@Reference(target = "(model.class.name=com.liferay.dynamic.data.mapping.model.DDMFormInstance)", unbind = "-")
	protected void setModelResourcePermission(ModelResourcePermission<DDMFormInstance> modelResourcePermission) {

		ddmFormInstanceModelResourcePermission = modelResourcePermission;
	}

}