<%@ include file="init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<aui:form action="${configurationActionURL}" cssClass="container-fluid container-fluid-max-xl mt-3" method="post" name="fm">
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />

	<liferay-frontend:edit-form-body>
		<aui:select name="formInstanceId" label="form" helpMessage="form-help-message">
			<aui:option/>
			<c:forEach items="${ ddmFormInstances }" var="ddmFormInstance">
				<aui:option value="${ ddmFormInstance.getFormInstanceId() }" selected="${ formInstanceId eq ddmFormInstance.getFormInstanceId()}">${ ddmFormInstance.getName(locale) }</aui:option>
			</c:forEach>
		</aui:select>

		<liferay-frontend:edit-form-body>
			<aui:input name="saveAndContinueEnabled" type="checkbox" value="${ saveAndContinueEnabled }" label="save-and-continue-enabled" helpMessage="save-and-continue-enabled-help-message" />
			<div id="<portlet:namespace/>save-and-continue-config-fields" class="${ saveAndContinueEnabled ? '' : 'hide' }">
				<aui:input name="saveProgressArticleIdEmail" type="text" value="${ saveProgressArticleIdEmail }" label="save-progress-article-id-email" helpMessage="save-progress-article-id-email-help-message" />
				<aui:input name="saveProgressArticleIdView" type="text" value="${ saveProgressArticleIdView }" label="save-progress-article-id-confirmation" helpMessage="save-progress-article-id-confirmation-help-message" />
				<aui:input name="continueSavedFormArticleIdView" type="text" value="${ continueSavedFormArticleIdView }" label="continue-saved-form-article-id-view" helpMessage="continue-saved-form-article-id-view-help-message" />
			</div>
		</liferay-frontend:edit-form-body>

		<aui:input name="articleId" type="text" value="${ articleId }" label="article-id" helpMessage="article-id-help-message" />
		<aui:input name="articleIdException" type="text" value="${ articleIdException }" label="article-id-exception" helpMessage="article-id-exception-help-message" />
		<aui:input name="hidePagination" type="checkbox" value="${ hidePagination }" label="hide-pagination" helpMessage="hide-pagination-help-message" />
		<aui:input name="deleteDataAfterSubmission" type="checkbox" value="${ deleteDataAfterSubmission }" label="delete-data-after-submission" helpMessage="delete-data-after-submission-help-message" />

		<liferay-frontend:edit-form-body>
			<aui:input name="expireOldForms" type="checkbox" value="${ expireOldForms }" label="expire-old-forms" helpMessage="expire-old-forms-help-message" />
			<div id="<portlet:namespace/>expire-old-forms-interval" class="${ expireOldForms ? '' : 'hide' }">
				<aui:input name="expireOldFormsInterval" type="text" value="${ expireOldFormsInterval }" label="expire-old-forms-interval" helpMessage="expire-old-forms-interval-help-message" />
			</div>
		</liferay-frontend:edit-form-body>
		<aui:button-row>
			<aui:button type="submit" />
		</aui:button-row>
	</liferay-frontend:edit-form-body>

</aui:form>

<%@ include file="/partials/configuration_script.jspf" %>