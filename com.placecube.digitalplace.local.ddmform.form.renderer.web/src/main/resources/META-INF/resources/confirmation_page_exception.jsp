<%@ include file="init.jsp" %>
<div class="container-fluid container-fluid-max-xl">
	<div class="portlet-forms">
		<div class="govuk-panel__body">
			<a class="btn-back govuk-back-link" href="${returnToFormLink}"><liferay-ui:message key="back"/></a>
		</div>

		<c:choose>
		    <c:when test="${ not empty articleIdException }">

		        <div class="mb-5">
		            <div class="govuk-panel govuk-panel--confirmation">
		                <div class="govuk-panel__title">
		                    <liferay-journal:journal-article articleId="${ articleIdException }" groupId="${ themeDisplay.scopeGroupId }" />
		                </div>
		            </div>
		        </div>

		    </c:when>
		    <c:otherwise>
		        <div class="mb-5">
		            <div class="govuk-panel govuk-panel--confirmation">
		                <div class="govuk-panel__body"><liferay-ui:message key="confirmation-page-exception-is-not-configured-yet"/></div>
		            </div>
		        </div>

		    </c:otherwise>
		</c:choose>

    </div>
</div>