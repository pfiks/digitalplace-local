<%@ include file="init.jsp" %>
<div class="container-fluid container-fluid-max-xl">
	<div class="portlet-forms">
        <c:choose>
            <c:when test="${ not empty uniqueReference }">
                <c:choose>
                    <c:when test="${ not empty successPageTitle && not empty successPageBody }">

                        <div class="mb-5"></div>
                            <div class="govuk-panel govuk-panel--confirmation">

                                <div class="govuk-panel__title">${ successPageTitle }</div>
                                <div class="govuk-panel__body">${ successPageBody }</div>

                            </div>
                        </div>

                    </c:when>
                    <c:when test="${ not empty articleId }">

                        <div class="mb-5">
                            <div class="govuk-panel govuk-panel--confirmation">
                                <div class="govuk-panel__title">
                                    <liferay-journal:journal-article articleId="${ articleId }" groupId="${ themeDisplay.scopeGroupId }" />
                                </div>
                            </div>
                        </div>

                    </c:when>
                    <c:otherwise>
                        <div class="mb-5">
                            <div class="govuk-panel govuk-panel--confirmation">
                                <div class="govuk-panel__body"><liferay-ui:message key="confirmation-page-is-not-configured-yet"/></div>
                            </div>
                        </div>

                    </c:otherwise>
                </c:choose>

            </c:when>
            <c:otherwise>
                <div class="alert alert-warning text-center">
                    <liferay-ui:message key="session-has-been-cleared-x" arguments="${returnToFormLink}"/>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>