<%@ include file="init.jsp" %>

<c:choose>
	<c:when test="${ addFormInstanceRecordPermission }">

		<c:if test="${ saveAndContinueEnabled }">
			<aui:button-row >
				<div class="govuk-button-group">

					<portlet:actionURL name="<%= MVCCommandKeys.SAVE_PROGRESS %>" var="saveProgressURL"/>

					<a href="javascript:;" data-href="${ saveProgressURL }" class="a-save-form-progress">
						<aui:button cssClass="govuk-button " value="save-form-progress" />
					</a>

					<portlet:renderURL var="continueFormURL">
						<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.CONTINUE_FORM %>" />
					</portlet:renderURL>

					<a href="${continueFormURL}">
						<aui:button cssClass="btn-continue-previous-submission hide govuk-button govuk-button--secondary" value="ddm-form-continue-previous-submission" />
					</a>

					<%@ include file="/partials/form_script.jspf" %>
				</div>
			</aui:button-row>
		</c:if>

		<portlet:actionURL  var="actionURL" name="<%= MVCCommandKeys.ADD_FORM_INSTANCE_RECORD %>"/>

		<aui:form action="${ actionURL }" name="customForm">

			<aui:input name="ddmFormInstanceRecordVersionId" type="hidden" value="${ ddmFormInstanceRecordVersionId }" />

			<liferay-form:ddm-form-renderer ddmFormInstanceId="${ formInstanceId }" ddmFormInstanceRecordVersionId="${ ddmFormInstanceRecordVersionId }"  showFormBasicInfo="false" />

		</aui:form>
	</c:when>

	<c:otherwise>
		<c:set var="message"><liferay-ui:message key="you-do-not-have-the-required-permissions"/></c:set>
		<c:set var="title"><liferay-ui:message key="warning"/></c:set>
		<clay:alert message="${message}" style="warning" title="${title}"/>
	</c:otherwise>
</c:choose>