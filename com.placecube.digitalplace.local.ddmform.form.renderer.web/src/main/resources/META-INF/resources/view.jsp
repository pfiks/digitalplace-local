<%@ include file="init.jsp" %>
<div class="container-fluid container-fluid-max-xl">
	<c:choose>

		<c:when test="${ formInstanceId eq 0  }">
			<%@ include file="/partials/invalid_configuration.jspf" %>
		</c:when>

		<c:otherwise>
			<div class="form-details <c:if test="${hidePagination}">hide-navigation</c:if>">
				<%@ include file="/form.jsp" %>
			</div>
		</c:otherwise>

	</c:choose>
</div>