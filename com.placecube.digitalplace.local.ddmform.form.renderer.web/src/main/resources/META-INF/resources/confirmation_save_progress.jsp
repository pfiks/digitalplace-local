<%@ include file="init.jsp" %>
<div class="ccew-form container-fluid container-fluid-max-xl">
	<div class="portlet-forms">

		<c:if test="${not empty uniqueReference}">
			<div class="govuk-panel__body">
				<a class="btn-back govuk-back-link" href="${returnToFormLink}"><liferay-ui:message key="back"/></a>

				<c:if test="${ hasFormDraft }">
						<div class="pull-right">

							<portlet:resourceURL id="<%= MVCCommandKeys.DOWNLOAD_FILE_PDF %>" var="downloadInProgressPdfURL"/>

							<a href="${ downloadInProgressPdfURL }" target="_blank">
								<aui:button cssClass="govuk-button govuk-button--secondary" value="download-from-progess-pdf" />
							</a>

						</div>
				</c:if>

			</div>

			<div class="govuk-panel govuk-panel--confirmation">
				<div class="govuk-panel__body">
					<c:choose>
						<c:when test="${not empty saveProgressArticleIdView}">
							${saveProgressArticleIdView}
						</c:when>
						<c:otherwise>
							<liferay-ui:message key="confirmation-save-progress-is-not-configured-yet" arguments="${uniqueReference}"/>
						</c:otherwise>
					</c:choose>
				</div>

				<div class="govuk-panel__title">
					<liferay-ui:message key="" arguments="${uniqueReference}"/>
				</div>

			</div>
		</c:if>

	</div>
</div>