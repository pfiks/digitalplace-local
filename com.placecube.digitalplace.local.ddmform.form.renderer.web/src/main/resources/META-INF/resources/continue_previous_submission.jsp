<%@ include file="init.jsp" %>

<portlet:actionURL var="actionURL" name="<%= MVCCommandKeys.CONTINUE_FORM %>"/>

<portlet:renderURL var="returnToMainPage"/>
<div class="ccew-form container-fluid container-fluid-max-xl">
	<div class="portlet-forms">
		<c:if test="${ not empty continueSavedFormArticleIdView }">
			<div class="govuk-panel govuk-panel--confirmation">
				<div class="govuk-panel__title">
					<liferay-journal:journal-article articleId="${ continueSavedFormArticleIdView }" groupId="${ themeDisplay.scopeGroupId }" />
				</div>
			</div>
		</c:if>

		<liferay-ui:error key="<%= FormPortletRequestKeys.REFERENCE_DOES_NOT_EXIST_ERROR_KEY %>" message="ddm-form-reference-does-not-exist"/>
		<aui:form action="${ actionURL }">
			<aui:button-row>
		    	<aui:button cssClass="govuk-back-link btn-back" onClick="${returnToMainPage}" type="cancel" value="back"/>
			</aui:button-row>
			<aui:fieldset>
				<aui:input name="uniqueReference" type="text" label="ddm-form-enter-unique-reference">
					<aui:validator name="required"/>
				</aui:input>
			</aui:fieldset>
			<aui:button-row>
				<aui:button cssClass="govuk-button" type="submit" value="continue"/>
			</aui:button-row>
		</aui:form>
	</div>
</div>