package com.placecube.digitalplace.local.login.filter;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.local.login.filter.util.BufferedRenderResponseWrapperUtil;

@PrepareForTest({ BufferedRenderResponseWrapperUtil.class })
@RunWith(PowerMockRunner.class)
public class BufferedRenderResponseWrapperTest {

	private BufferedRenderResponseWrapper bufferedRenderResponseWrapper;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private CharArrayWriter mockCharArrayWriter;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private OutputStream mockOutputStream;

	@Before
	public void setUp() {
		mockStatic(BufferedRenderResponseWrapperUtil.class);

		when(BufferedRenderResponseWrapperUtil.createCharArrayWriter()).thenReturn(mockCharArrayWriter);
		bufferedRenderResponseWrapper = new BufferedRenderResponseWrapper(mockRenderResponse);
	}

	@Test
	public void new_WhenNoErrors_ThenCreatesAndStoresCharArrayWriter() {
		assertThat(bufferedRenderResponseWrapper.getCharWriter(), sameInstance(mockCharArrayWriter));
	}

	@Test
	public void getWriter_WhenWriterIsNullAndGetOutputStreamCalledIsFalse_ThenCreatesAndReturnsWriter() throws IOException {
		when(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter)).thenReturn(mockPrintWriter);

		bufferedRenderResponseWrapper.setGetOutputStreamCalled(false);

		assertThat(bufferedRenderResponseWrapper.getWriter(), sameInstance(mockPrintWriter));
	}

	@Test
	public void getWriter_WhenWriterIsNullAndGetOutputStreamCalledIsFalse_ThenStoresItIntoObject() throws IOException {
		when(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter)).thenReturn(mockPrintWriter);

		bufferedRenderResponseWrapper.setGetOutputStreamCalled(false);

		assertThat(bufferedRenderResponseWrapper.getStoredWriter(), nullValue());
		bufferedRenderResponseWrapper.getWriter();
		assertThat(bufferedRenderResponseWrapper.getStoredWriter(), sameInstance(mockPrintWriter));
	}

	@Test(expected = IllegalStateException.class)
	public void getWriter_WhenGetOutputStreamHasBeenCalled_ThenThrowsIllegalStateException() throws IOException {
		when(BufferedRenderResponseWrapperUtil.createCharArrayWriter()).thenReturn(mockCharArrayWriter);
		when(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter)).thenReturn(mockPrintWriter);

		bufferedRenderResponseWrapper.setGetOutputStreamCalled(true);
		bufferedRenderResponseWrapper.getWriter();
	}

	@Test
	public void getWriter_WhenWriterIsNotNull_ThenReturnsExistingWriter() throws IOException {
		when(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter)).thenReturn(mockPrintWriter);

		bufferedRenderResponseWrapper.setGetOutputStreamCalled(false);

		bufferedRenderResponseWrapper.getWriter();

		assertThat(bufferedRenderResponseWrapper.getWriter(), sameInstance(mockPrintWriter));
	}

	@Test
	public void getWriter_WhenWriterIsNotNull_ThenItDoesNotCreateANewOne() throws IOException {
		when(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter)).thenReturn(mockPrintWriter);

		bufferedRenderResponseWrapper.setGetOutputStreamCalled(false);

		bufferedRenderResponseWrapper.getWriter();

		bufferedRenderResponseWrapper.getWriter();

		verifyStatic(BufferedRenderResponseWrapperUtil.class, times(1));
		BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter);
	}

	@Test(expected = IllegalStateException.class)
	public void getOutputStream_WhenGetWriterHasBeenCalled_ThenThrowsIllegalStateException() throws IOException {
		bufferedRenderResponseWrapper.setGetWriterCalled(true);

		bufferedRenderResponseWrapper.getOutputStream();
	}

	@Test
	public void getOutputStream_WhenGetWriterHasNotBeenCalled_ThenReturnsPortletOutputStream() throws IOException {
		BufferedRenderResponseWrapper spiedBufferedRenderResponseWrapper = spy(bufferedRenderResponseWrapper);

		when(spiedBufferedRenderResponseWrapper.getPortletOutputStream()).thenReturn(mockOutputStream);
		spiedBufferedRenderResponseWrapper.setGetWriterCalled(false);

		assertThat(spiedBufferedRenderResponseWrapper.getOutputStream(), sameInstance(mockOutputStream));
	}

	@Test
	public void getOutputStream_WhenGetWriterHasNotBeenCalled_ThenSetsGetOutputStreamCalledToTrue() throws IOException {
		BufferedRenderResponseWrapper spiedBufferedRenderResponseWrapper = spy(bufferedRenderResponseWrapper);

		when(spiedBufferedRenderResponseWrapper.getPortletOutputStream()).thenReturn(mockOutputStream);
		spiedBufferedRenderResponseWrapper.setGetWriterCalled(false);

		assertThat(spiedBufferedRenderResponseWrapper.getOutputStreamCalled, equalTo(false));
		spiedBufferedRenderResponseWrapper.getOutputStream();
		assertThat(spiedBufferedRenderResponseWrapper.getOutputStreamCalled, equalTo(true));
	}

	@Test
	public void toString_WhenPrintWriterIsNull_ThenReturnsNull() {
		assertThat(bufferedRenderResponseWrapper.toString(), nullValue());
	}

	@Test
	public void toString_WhenCharWriterIsNotNull_ThenReturnsCharWriterToString() throws Exception {
		final String charArrayWriterString = "char writer string";

		when(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter)).thenReturn(mockPrintWriter);
		when(mockCharArrayWriter.toString()).thenReturn(charArrayWriterString);
		bufferedRenderResponseWrapper.setGetOutputStreamCalled(false);
		bufferedRenderResponseWrapper.getWriter();

		assertThat(bufferedRenderResponseWrapper.toString(), equalTo(charArrayWriterString));
	}
}
