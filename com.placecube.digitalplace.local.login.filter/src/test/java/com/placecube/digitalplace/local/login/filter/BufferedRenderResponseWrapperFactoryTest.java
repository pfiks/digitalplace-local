package com.placecube.digitalplace.local.login.filter;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BufferedRenderResponseWrapperFactoryTest {

	private BufferedRenderResponseWrapperFactory bufferedRenderResponseWrapperFactory;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void setUp() {
		bufferedRenderResponseWrapperFactory = new BufferedRenderResponseWrapperFactory();
	}

	@Test
	public void createBufferedRenderResponseWrapper_WhenNoErrors_ThenReturnsNotNullBufferedRenderResponseWrapperInstance() {
		BufferedRenderResponseWrapper result = bufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse);

		assertThat(result, notNullValue());
		assertThat(result, instanceOf(BufferedRenderResponseWrapper.class));
	}
}
