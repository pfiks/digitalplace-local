package com.placecube.digitalplace.local.login.filter;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ResourceBundle;

import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterCompanyConfiguration;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterConfigurationService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@PrepareForTest({ ResourceBundleUtil.class, PropsUtil.class })
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LoginRenderFilterTest {

	private static final long COMPANY_ID = 322l;

	@InjectMocks
	private LoginRenderFilter loginRenderFilter;

	@Mock
	private BufferedRenderResponseWrapperFactory mockBufferedRenderResponseWrapperFactory;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private LoginFilterCompanyConfiguration mockLoginFilterCompanyConfiguration;

	@Mock
	private LoginFilterConfigurationService mockLoginFilterConfigurationService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ResourceBundle mockResourceBundle;

	@Test
	public void getBufferedRenderResponseWrapperFactory_WhenNoErrors_ThenReturnsBufferedRenderResponseWrapperFactory() {
		assertThat(loginRenderFilter.getBufferedRenderResponseWrapperFactory(), sameInstance(mockBufferedRenderResponseWrapperFactory));
	}

	@Test
	public void getPageWriterContent_WhenConfiguredValuesRetrievalFails_ThenReturnsOriginalPageContent() throws Exception {
		String pageContent = "<form class=\"sign-in-form\"></form>";

		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		String result = loginRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo(pageContent));
	}

	@Test
	public void getPageWriterContent_WhenCustomPasswordLabelIsNotEmpty_ThenReturnsPageContentWithReplacedPasswordLabelInForm() throws Exception {
		String pageContent = "<form class=\"sign-in-form\"><label for=\"_com_liferay_login_web_portlet_LoginPortlet_password\">Old label</label></form>";
		final String passwordLabel = "Password";

		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);
		when(mockLoginFilterConfigurationService.getLoginPageCustomPasswordLabelContent(mockLoginFilterCompanyConfiguration)).thenReturn(passwordLabel);

		String result = loginRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo("<form class=\"sign-in-form\"><label for=\"_com_liferay_login_web_portlet_LoginPortlet_password\">Password</label></form>"));
	}

	@Test
	public void getPageWriterContent_WhenLoginPageCustomContentIsNotEmpty_ThenReturnsPageContentWithConfiguredContentAndSessionTimeoutValueBeforeForm() throws Exception {
		final int sessionTimeoutMinutes = 10;

		String pageContent = "<form class=\"sign-in-form\"></form>";
		final String loginPageCustomContent = "<p class=\"login-alert\">you will be automatically signed out after ${SESSION_TIMEOUT} minutes</p>";

		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(PropsUtil.get(PropsKeys.SESSION_TIMEOUT)).thenReturn(String.valueOf(sessionTimeoutMinutes));
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);

		when(mockLoginFilterConfigurationService.getTopLoginPageCustomContent(mockLoginFilterCompanyConfiguration)).thenReturn(loginPageCustomContent);

		String result = loginRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo("<p class=\"login-alert\">you will be automatically signed out after 10 minutes</p><form class=\"sign-in-form\"></form>"));
	}

	@Test
	public void getPageWriterContent_WhenTopPageContentIsEmptyAndCustomPasswordLabelIsEmpty_ThenReturnsOriginalPageContent() throws Exception {
		String pageContent = "<form class=\"sign-in-form\"></form>";

		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);

		String result = loginRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo(pageContent));
	}

	@Test
	public void getSearchText_WhenNoErrors_ThenReturnsLoginFormSearchText() {
		assertThat(loginRenderFilter.getSearchText(), equalTo("<form"));
	}

	@Test
	public void isFilterCommand_WhenMvcRenderCommandIsEmpty_ThenReturnsTrue() {
		assertThat(loginRenderFilter.isFilterCommand(StringPool.BLANK), equalTo(true));
	}

	@Test
	public void isFilterCommand_WhenMvcRenderCommandIsLoginPath_ThenReturnTrue() {
		assertThat(loginRenderFilter.isFilterCommand("/login/login"), equalTo(true));
	}

	@Test
	public void isFilterCommand_WhenMvcRenderCommandIsNotMatching_ThenReturnFalse() {
		assertThat(loginRenderFilter.isFilterCommand("/login/abcdefg"), equalTo(false));
	}

	@Test
	public void isFilterCommand_WhenMvcRenderCommandIsNull_ThenReturnsRenderTrue() {
		assertThat(loginRenderFilter.isFilterCommand(null), equalTo(true));
	}

	@Test
	public void isFilterEnabled_WhenLoginFilterEnabledConfigurationRetrievalFails_ThenReturnsFalse() throws Exception {
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		assertThat(loginRenderFilter.isFilterEnabled(mockRenderRequest), equalTo(false));
	}

	@Test
	@Parameters({ "true", "false" })
	public void isFilterEnabled_WhenNoErrors_ThenReturnsLoginFilterEnabledConfigurationForInstance(boolean enabled) throws Exception {
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);
		when(mockLoginFilterConfigurationService.isLoginFilterEnabled(mockLoginFilterCompanyConfiguration)).thenReturn(enabled);

		assertThat(loginRenderFilter.isFilterEnabled(mockRenderRequest), equalTo(enabled));
	}

	@Before
	public void setUp() {
		mockStatic(ResourceBundleUtil.class, PropsUtil.class);
	}

}
