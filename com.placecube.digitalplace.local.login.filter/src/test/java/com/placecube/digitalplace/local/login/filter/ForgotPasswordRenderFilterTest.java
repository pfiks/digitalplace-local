package com.placecube.digitalplace.local.login.filter;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ResourceBundle;

import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterCompanyConfiguration;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterConfigurationService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@PrepareForTest({ ResourceBundleUtil.class, PropsUtil.class })
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ForgotPasswordRenderFilterTest {

	private static final long COMPANY_ID = 322l;

	@InjectMocks
	private ForgotPasswordRenderFilter forgotPasswordRenderFilter;

	@Mock
	private BufferedRenderResponseWrapperFactory mockBufferedRenderResponseWrapperFactory;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private LoginFilterCompanyConfiguration mockLoginFilterCompanyConfiguration;

	@Mock
	private LoginFilterConfigurationService mockLoginFilterConfigurationService;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ResourceBundle mockResourceBundle;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getBufferedRenderResponseWrapperFactory_WhenNoErrors_ThenReturnsBufferedRenderResponseWrapperFactory() {
		assertThat(forgotPasswordRenderFilter.getBufferedRenderResponseWrapperFactory(), sameInstance(mockBufferedRenderResponseWrapperFactory));
	}

	@Test
	public void getPageWriterContent_WhenConfiguredValuesRetrievalFails_ThenReturnsOriginalPageContent() throws Exception {
		String pageContent = "<form class=\"update-password\"></form>";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		String result = forgotPasswordRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo(pageContent));
	}

	@Test
	public void getPageWriterContent_WhenForgotPasswordPageContentIsEmpty_ThenReturnsOriginalPageContent() throws Exception {
		String pageContent = "<form class=\"update-password\"></form>";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		String result = forgotPasswordRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo(pageContent));
	}

	@Test
	public void getPageWriterContent_WhenForgotPasswordPageCustomContentIsNotEmpty_ThenReturnsPageContentWithConfiguredContentBeforeForm() throws Exception {
		String pageContent = "<form class=\"update-password\"></form>";
		final String forgotPasswordPageCustomContent = "<p>You will receive an email after requesting a password reset</p>";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);
		when(mockLoginFilterConfigurationService.getTopForgotPasswordPageCustomContent(mockLoginFilterCompanyConfiguration)).thenReturn(forgotPasswordPageCustomContent);

		String result = forgotPasswordRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		assertThat(result, equalTo("<p>You will receive an email after requesting a password reset</p><form class=\"update-password\"></form>"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPageWriterContent_WhenNoErrors_ThenSetsShowBackIconToConfiguredSetting(boolean showBackLink) throws Exception {
		String pageContent = "<form class=\"update-password\"></form>";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);
		when(mockLoginFilterCompanyConfiguration.forgotPasswordShowBackIcon()).thenReturn(showBackLink);

		forgotPasswordRenderFilter.getPageWriterContent(mockRenderRequest, pageContent);

		verify(mockPortletDisplay, times(1)).setShowBackIcon(showBackLink);
	}

	@Test
	public void getSearchText_WhenNoErrors_ThenReturnsLoginFormSearchText() {
		assertThat(forgotPasswordRenderFilter.getSearchText(), equalTo("<form"));
	}

	@Test
	public void isFilterCommand_WhenIsNotForgottenPasswordPath_ThenReturnsFalse() {
		assertThat(forgotPasswordRenderFilter.isFilterCommand("/login/not_forgot_password"), equalTo(false));
	}

	@Test
	public void isFilterCommand_WhenNoErrors_ThenReturnsTrue() {
		assertThat(forgotPasswordRenderFilter.isFilterCommand("/login/forgot_password"), equalTo(true));
	}

	@Test
	public void isFilterEnabled_WhenForgotPasswordFilterEnabledConfigurationRetrievalFails_ThenReturnsFalse() throws Exception {
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		assertThat(forgotPasswordRenderFilter.isFilterEnabled(mockRenderRequest), equalTo(false));
	}

	@Test
	@Parameters({ "true", "false" })
	public void isFilterEnabled_WhenNoErrors_ThenReturnsForgotPasswordFilterEnabledConfigurationForInstance(boolean enabled) throws Exception {
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginFilterCompanyConfiguration);
		when(mockLoginFilterCompanyConfiguration.forgotPasswordPageTopContentEnabled()).thenReturn(enabled);

		assertThat(forgotPasswordRenderFilter.isFilterEnabled(mockRenderRequest), equalTo(enabled));
	}

	@Before
	public void setUp() {
		mockStatic(ResourceBundleUtil.class, PropsUtil.class);
	}
}
