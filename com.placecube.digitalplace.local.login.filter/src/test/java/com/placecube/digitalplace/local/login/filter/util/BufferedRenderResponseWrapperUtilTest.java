package com.placecube.digitalplace.local.login.filter.util;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.CharArrayWriter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BufferedRenderResponseWrapperUtilTest {

	@Mock
	private CharArrayWriter mockCharArrayWriter;

	@Test
	public void createCharArrayWriter_WhenNoErrors_ThenReturnsNotNullCharArrayWriter() {
		assertThat(BufferedRenderResponseWrapperUtil.createCharArrayWriter(), notNullValue());
	}

	@Test
	public void createPrintWriter_WhenNoErrors_ThenReturnsNotNullPringWriter() {
		assertThat(BufferedRenderResponseWrapperUtil.createPrintWriter(mockCharArrayWriter), notNullValue());
	}

}
