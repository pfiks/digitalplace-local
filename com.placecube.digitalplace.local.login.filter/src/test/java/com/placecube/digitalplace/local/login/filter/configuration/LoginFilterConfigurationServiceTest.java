package com.placecube.digitalplace.local.login.filter.configuration;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LoginFilterConfigurationServiceTest {

	@InjectMocks
	private LoginFilterConfigurationService loginFilterConfigurationService;

	@Mock
	private LoginFilterCompanyConfiguration mockLoginFilterCompanyConfiguration;

	@Test
	public void getLoginPageCustomPasswordLabelContent_WhenLoginPageCustomPasswordLabelIsEnabled_ThenReturnsConfiguredLoginPageCustomPasswordLabel() {
		final String label = "<p>Login page top content</p>";

		when(mockLoginFilterCompanyConfiguration.loginPageCustomPasswordLabelEnabled()).thenReturn(true);
		when(mockLoginFilterCompanyConfiguration.loginPageCustomPasswordLabel()).thenReturn(label);

		String result = loginFilterConfigurationService.getLoginPageCustomPasswordLabelContent(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(label));
	}

	@Test
	public void getLoginPageCustomPasswordLabelContent_WhenLoginPageCustomPasswordLabelIsNotEnabled_ThenReturnsEmptyString() {
		when(mockLoginFilterCompanyConfiguration.loginPageCustomPasswordLabelEnabled()).thenReturn(false);

		String result = loginFilterConfigurationService.getLoginPageCustomPasswordLabelContent(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getTopForgotPasswordPageCustomContent_WhenTopForgotPasswordPageContentIsEnabled_ThenReturnsConfiguredTopForgotPasswordPageContent() {
		final String configuredForgotPasswordContent = "<p>forgot password top content</p>";

		when(mockLoginFilterCompanyConfiguration.forgotPasswordPageTopContentEnabled()).thenReturn(true);
		when(mockLoginFilterCompanyConfiguration.forgotPasswordPageTopContent()).thenReturn(configuredForgotPasswordContent);

		String result = loginFilterConfigurationService.getTopForgotPasswordPageCustomContent(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(configuredForgotPasswordContent));
	}

	@Test
	public void getTopForgotPasswordPageCustomContent_WhenTopForgotPasswordPageContentIsNotEnabled_ThenReturnsEmptyString() {
		when(mockLoginFilterCompanyConfiguration.forgotPasswordPageTopContentEnabled()).thenReturn(false);

		String result = loginFilterConfigurationService.getTopForgotPasswordPageCustomContent(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getTopLoginPageCustomContent_WhenTopLoginPageContentIsEnabled_ThenReturnsConfiguredTopLoginPageContent() {
		final String configuredTopLoginContent = "<p>Login page top content</p>";

		when(mockLoginFilterCompanyConfiguration.loginPageTopContentEnabled()).thenReturn(true);
		when(mockLoginFilterCompanyConfiguration.loginPageTopContent()).thenReturn(configuredTopLoginContent);

		String result = loginFilterConfigurationService.getTopLoginPageCustomContent(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(configuredTopLoginContent));
	}

	@Test
	public void getTopLoginPageCustomContent_WhenTopLoginPageContentIsNotEnabled_ThenReturnsEmptyString() {
		when(mockLoginFilterCompanyConfiguration.loginPageTopContentEnabled()).thenReturn(false);

		String result = loginFilterConfigurationService.getTopLoginPageCustomContent(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void isLoginFilterEnabled_WhenAllLoginPageSettingsIsDisabled_ThenReturnsFalse() {
		when(mockLoginFilterCompanyConfiguration.loginPageCustomPasswordLabelEnabled()).thenReturn(false);
		when(mockLoginFilterCompanyConfiguration.loginPageTopContentEnabled()).thenReturn(false);

		boolean result = loginFilterConfigurationService.isLoginFilterEnabled(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(false));
	}

	@Test
	@Parameters({ "true, true", "false, true", "true, false" })
	public void isLoginFilterEnabled_WhenAnyOfLoginPageSettingsIsEnabled_ThenReturnsTrue(boolean loginPageCustomPasswordLabelEnabled, boolean loginPageTopContentEnabled) {
		when(mockLoginFilterCompanyConfiguration.loginPageCustomPasswordLabelEnabled()).thenReturn(loginPageCustomPasswordLabelEnabled);
		when(mockLoginFilterCompanyConfiguration.loginPageTopContentEnabled()).thenReturn(loginPageTopContentEnabled);

		boolean result = loginFilterConfigurationService.isLoginFilterEnabled(mockLoginFilterCompanyConfiguration);

		assertThat(result, equalTo(true));
	}

}
