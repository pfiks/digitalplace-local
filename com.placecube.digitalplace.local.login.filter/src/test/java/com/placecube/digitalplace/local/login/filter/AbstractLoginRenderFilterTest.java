package com.placecube.digitalplace.local.login.filter;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.PortletException;
import javax.portlet.RenderParameters;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.placecube.digitalplace.local.login.filter.constants.LoginFilterConstants;

@RunWith(MockitoJUnitRunner.class)
public class AbstractLoginRenderFilterTest {

	private static final String SEARCH_TEXT = "form";
	private static final String FILTER_COMMAND = "/command";
	private static final String PAGE_WRITER_CONTENT = "<form>content</form>";

	private AbstractLoginRenderFilter abstractLoginRenderFilter;

	@Mock
	private BufferedRenderResponseWrapperFactory mockBufferedRenderResponseWrapperFactory;

	@Mock
	private BufferedRenderResponseWrapper mockBufferedRenderResponseWrapper;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private RenderParameters mockRenderParameters;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private FilterChain mockFilterChain;

	@Test
	public void doFilter_WhenFilterIsEnabledAndRenderCommandIsFilterCommandAndPageContainsSearchText_ThenWritesFilterPageContent() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, true, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockRenderParameters.getValue(LoginFilterConstants.MVC_RENDER_COMMAND_NAME)).thenReturn(FILTER_COMMAND);
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		when(mockBufferedRenderResponseWrapper.getResponseContent()).thenReturn("<div>" + SEARCH_TEXT + "</div>");
		when(mockRenderResponse.getWriter()).thenReturn(mockPrintWriter);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);

		verify(mockPrintWriter, times(1)).write(PAGE_WRITER_CONTENT);
	}

	@Test
	public void doFilter_WhenFilterIsNotEnabled_ThenWritesDefaultPageContent() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, false, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		final String defaultPageContent = "<div></div>";
		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockRenderParameters.getValue(LoginFilterConstants.MVC_RENDER_COMMAND_NAME)).thenReturn(FILTER_COMMAND);
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		when(mockBufferedRenderResponseWrapper.getResponseContent()).thenReturn(defaultPageContent);
		when(mockRenderResponse.getWriter()).thenReturn(mockPrintWriter);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);

		verify(mockPrintWriter, times(1)).write(defaultPageContent);
	}

	@Test
	public void doFilter_WhenFilterIsEnabledAndRenderCommandIsNotFilterCommand_ThenWritesDefaultPageContent() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, true, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		final String defaultPageContent = "<div></div>";
		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockRenderParameters.getValue(LoginFilterConstants.MVC_RENDER_COMMAND_NAME)).thenReturn("anotherCommand");
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		when(mockBufferedRenderResponseWrapper.getResponseContent()).thenReturn(defaultPageContent);
		when(mockRenderResponse.getWriter()).thenReturn(mockPrintWriter);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);

		verify(mockPrintWriter, times(1)).write(defaultPageContent);
	}

	@Test
	public void doFilter_WhenFilterIsEnabledAndRenderCommandIsFilterCommandAndPageDoesNotContainSearchText_ThenWritesDefaultPageContent() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, true, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		final String defaultPageContent = "<div></div>";
		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockRenderParameters.getValue(LoginFilterConstants.MVC_RENDER_COMMAND_NAME)).thenReturn(FILTER_COMMAND);
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		when(mockBufferedRenderResponseWrapper.getResponseContent()).thenReturn(defaultPageContent);
		when(mockRenderResponse.getWriter()).thenReturn(mockPrintWriter);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);

		verify(mockPrintWriter, times(1)).write(defaultPageContent);

	}

	@Test
	public void doFilter_WhenNoErrors_ThenFiltersChainAndThenGetsResponseTextInOrder() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, true, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		when(mockRenderResponse.getWriter()).thenReturn(mockPrintWriter);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);

		InOrder inOrder = inOrder(mockFilterChain, mockBufferedRenderResponseWrapper);

		inOrder.verify(mockFilterChain, times(1)).doFilter(mockRenderRequest, mockBufferedRenderResponseWrapper);
		inOrder.verify(mockBufferedRenderResponseWrapper, times(1)).getResponseContent();
	}

	@Test(expected = IOException.class)
	public void doFilter_WhenChainFilteringThrowsIOException_ThenThrowsIOException() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, true, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		doThrow(new IOException()).when(mockFilterChain).doFilter(mockRenderRequest, mockBufferedRenderResponseWrapper);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);
	}

	@Test(expected = PortletException.class)
	public void doFilter_WhenChainFilteringThrowsPortletException_ThenThrowsPortletException() throws Exception {
		abstractLoginRenderFilter = createTestAbstractLoginRenderFilter(SEARCH_TEXT, true, FILTER_COMMAND, PAGE_WRITER_CONTENT, mockBufferedRenderResponseWrapperFactory);

		when(mockRenderRequest.getRenderParameters()).thenReturn(mockRenderParameters);
		when(mockBufferedRenderResponseWrapperFactory.createBufferedRenderResponseWrapper(mockRenderResponse)).thenReturn(mockBufferedRenderResponseWrapper);
		doThrow(new PortletException()).when(mockFilterChain).doFilter(mockRenderRequest, mockBufferedRenderResponseWrapper);

		abstractLoginRenderFilter.doFilter(mockRenderRequest, mockRenderResponse, mockFilterChain);
	}

	private AbstractLoginRenderFilter createTestAbstractLoginRenderFilter(String searchText, boolean filterEnabled, String filterCommand, String pageWriterContent,
			BufferedRenderResponseWrapperFactory bufferedRenderResponseWrapperFactory) {

		AbstractLoginRenderFilter testAbstractLoginRenderFilter = new AbstractLoginRenderFilter() {

			@Override
			public BufferedRenderResponseWrapperFactory getBufferedRenderResponseWrapperFactory() {
				return bufferedRenderResponseWrapperFactory;
			}

			@Override
			protected String getSearchText() {
				return searchText;
			}

			@Override
			protected String getPageWriterContent(RenderRequest request, String pageContent) {
				return pageWriterContent;
			}

			@Override
			protected boolean isFilterEnabled(RenderRequest request) {
				return filterEnabled;
			}

			@Override
			protected boolean isFilterCommand(String mvcRenderCommandName) {
				return filterCommand.equals(mvcRenderCommandName);
			}
		};

		return testAbstractLoginRenderFilter;
	}
}
