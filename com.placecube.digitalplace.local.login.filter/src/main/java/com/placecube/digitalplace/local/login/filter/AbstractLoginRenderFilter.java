package com.placecube.digitalplace.local.login.filter;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;

import com.placecube.digitalplace.local.login.filter.constants.LoginFilterConstants;

public abstract class AbstractLoginRenderFilter implements RenderFilter {

	@Override
	public void init(FilterConfig config) throws PortletException {
		// No actions needed on init
	}

	@Override
	public void destroy() {
		// No actions needed on destroy
	}

	@SuppressWarnings("deprecation")
	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException {
		String mvcRenderCommandName = request.getRenderParameters().getValue(LoginFilterConstants.MVC_RENDER_COMMAND_NAME);

		BufferedRenderResponseWrapper renderResponseWrapper = getBufferedRenderResponseWrapperFactory().createBufferedRenderResponseWrapper(response);

		chain.doFilter(request, renderResponseWrapper);

		String text = renderResponseWrapper.getResponseContent();

		if (isFilterEnabled(request) && isFilterCommand(mvcRenderCommandName) && pageContainsSearchText(text)) {
			response.getWriter().write(getPageWriterContent(request, text));
		} else {
			response.getWriter().write(text);
		}
	}

	private boolean pageContainsSearchText(String pageContent) {
		return pageContent.lastIndexOf(getSearchText()) >= 0;
	}

	protected abstract String getSearchText();

	protected abstract boolean isFilterEnabled(RenderRequest request);

	protected abstract boolean isFilterCommand(String mvcRenderCommandName);

	protected abstract String getPageWriterContent(RenderRequest request, String pageContent);

	protected abstract BufferedRenderResponseWrapperFactory getBufferedRenderResponseWrapperFactory();
}
