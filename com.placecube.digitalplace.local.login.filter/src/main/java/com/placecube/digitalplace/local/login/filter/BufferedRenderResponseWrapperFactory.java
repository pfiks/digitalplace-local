package com.placecube.digitalplace.local.login.filter;

import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = BufferedRenderResponseWrapperFactory.class)
public class BufferedRenderResponseWrapperFactory {

	public BufferedRenderResponseWrapper createBufferedRenderResponseWrapper(RenderResponse renderResponse) {
		return new BufferedRenderResponseWrapper(renderResponse);
	}
}
