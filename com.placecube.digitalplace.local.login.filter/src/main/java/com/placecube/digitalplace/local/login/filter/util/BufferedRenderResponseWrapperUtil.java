package com.placecube.digitalplace.local.login.filter.util;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

public class BufferedRenderResponseWrapperUtil {

	private BufferedRenderResponseWrapperUtil() {
	}

	public static CharArrayWriter createCharArrayWriter() {
		return new CharArrayWriter();
	}

	public static PrintWriter createPrintWriter(CharArrayWriter charArrayWriter) {
		return new PrintWriter(charArrayWriter);
	}
}
