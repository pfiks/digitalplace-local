package com.placecube.digitalplace.local.login.filter.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "login", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.login.filter.configuration.LoginFilterCompanyConfiguration", localization = "content/Language", name = "login-filter")
public interface LoginFilterCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "login-page-custom-password-label-enabled-name")
	boolean loginPageCustomPasswordLabelEnabled();

	@Meta.AD(required = false, deflt = "", name = "login-page-custom-password-label-name", description = "login-page-custom-password-label-description")
	String loginPageCustomPasswordLabel();

	@Meta.AD(required = false, deflt = "false", name = "login-page-top-content-enabled-name")
	boolean loginPageTopContentEnabled();

	@Meta.AD(required = false, deflt = "", name = "login-page-top-content-name", description = "login-page-top-content-description")
	String loginPageTopContent();

	@Meta.AD(required = false, deflt = "false", name = "forgot-password-page-top-content-enabled-name")
	boolean forgotPasswordPageTopContentEnabled();

	@Meta.AD(required = false, deflt = "", name = "forgot-password-page-top-content-name", description = "forgot-password-page-top-content-description")
	String forgotPasswordPageTopContent();

	@Meta.AD(required = false, deflt = "false", name = "forgot-password-show-back-icon-name", description = "forgot-password-show-back-icon-description")
	boolean forgotPasswordShowBackIcon();

}