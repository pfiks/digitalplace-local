package com.placecube.digitalplace.local.login.filter;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.portlet.RenderResponse;
import javax.portlet.filter.RenderResponseWrapper;

import com.placecube.digitalplace.local.login.filter.util.BufferedRenderResponseWrapperUtil;

public class BufferedRenderResponseWrapper extends RenderResponseWrapper {

	protected CharArrayWriter charWriter;
	protected PrintWriter writer;
	protected boolean getOutputStreamCalled;
	protected boolean getWriterCalled;

	public BufferedRenderResponseWrapper(RenderResponse response) {
		super(response);

		charWriter = BufferedRenderResponseWrapperUtil.createCharArrayWriter();
	}

	public OutputStream getOutputStream() throws IOException {
		if (getWriterCalled) {
			throw new IllegalStateException("getWriter already called");
		}

		getOutputStreamCalled = true;

		return super.getPortletOutputStream();
	}

	@Override
	public PrintWriter getWriter() throws IOException {
		if (writer != null) {
			return writer;
		}

		if (getOutputStreamCalled) {
			throw new IllegalStateException("getOutputStream already called");
		}

		getWriterCalled = true;

		writer = BufferedRenderResponseWrapperUtil.createPrintWriter(charWriter);

		return writer;
	}

	@Override
	public String toString() {
		String s = null;

		if (writer != null) {
			s = charWriter.toString();
		}

		return s;
	}

	public String getResponseContent() {
		return toString();
	}

	public boolean isGetOutputStreamCalled() {
		return getOutputStreamCalled;
	}

	public void setGetOutputStreamCalled(boolean getOutputStreamCalled) {
		this.getOutputStreamCalled = getOutputStreamCalled;
	}

	public boolean isGetWriterCalled() {
		return getWriterCalled;
	}

	public void setGetWriterCalled(boolean getWriterCalled) {
		this.getWriterCalled = getWriterCalled;
	}

	public PrintWriter getStoredWriter() {
		return writer;
	}

	public CharArrayWriter getCharWriter() {
		return charWriter;
	}

}