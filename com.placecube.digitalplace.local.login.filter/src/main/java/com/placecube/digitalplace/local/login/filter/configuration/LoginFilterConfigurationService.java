package com.placecube.digitalplace.local.login.filter.configuration;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;

@Component(immediate = true, service = LoginFilterConfigurationService.class)
public class LoginFilterConfigurationService {

	public String getLoginPageCustomPasswordLabelContent(LoginFilterCompanyConfiguration configuration) {
		return configuration.loginPageCustomPasswordLabelEnabled() ? configuration.loginPageCustomPasswordLabel() : StringPool.BLANK;
	}

	public String getTopForgotPasswordPageCustomContent(LoginFilterCompanyConfiguration configuration) {
		return configuration.forgotPasswordPageTopContentEnabled() ? configuration.forgotPasswordPageTopContent() : StringPool.BLANK;
	}

	public String getTopLoginPageCustomContent(LoginFilterCompanyConfiguration configuration) {
		return configuration.loginPageTopContentEnabled() ? configuration.loginPageTopContent() : StringPool.BLANK;
	}

	public boolean isLoginFilterEnabled(LoginFilterCompanyConfiguration configuration) {
		return configuration.loginPageCustomPasswordLabelEnabled() || configuration.loginPageTopContentEnabled();
	}

}
