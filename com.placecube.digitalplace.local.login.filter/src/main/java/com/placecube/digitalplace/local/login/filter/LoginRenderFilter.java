package com.placecube.digitalplace.local.login.filter;

import javax.portlet.RenderRequest;
import javax.portlet.filter.PortletFilter;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterCompanyConfiguration;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterConfigurationService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.LOGIN }, service = PortletFilter.class)
public class LoginRenderFilter extends AbstractLoginRenderFilter {

	private static final String LABLE_TO_REPLACE_END_TEXT = "for=\"_com_liferay_login_web_portlet_LoginPortlet_password\">";
	private static final Log LOG = LogFactoryUtil.getLog(LoginRenderFilter.class);
	private static final String LOGIN_FORM_TAG = "<form";

	private static final String SESSION_TIMEOUT_PLACEHOLDER = "${SESSION_TIMEOUT}";

	@Reference
	private BufferedRenderResponseWrapperFactory bufferedRenderResponseWrapperFactory;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LoginFilterConfigurationService loginFilterConfigurationService;

	@Reference
	private Portal portal;

	@Override
	protected BufferedRenderResponseWrapperFactory getBufferedRenderResponseWrapperFactory() {
		return bufferedRenderResponseWrapperFactory;
	}

	@Override
	protected String getPageWriterContent(RenderRequest request, String pageContent) {
		long companyId = portal.getCompanyId(request);
		int indexLoginForm = pageContent.lastIndexOf(LOGIN_FORM_TAG);
		int indexLabelToReplace = pageContent.lastIndexOf(LABLE_TO_REPLACE_END_TEXT);

		try {
			LoginFilterCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, companyId);

			String loginPageCustomContent = getLoginPageCustomContent(configuration);
			String loginPageCustomPasswordLabel = loginFilterConfigurationService.getLoginPageCustomPasswordLabelContent(configuration);

			StringBuilder content = new StringBuilder();
			content.append(pageContent.substring(0, indexLoginForm));

			if (Validator.isNotNull(loginPageCustomContent)) {
				content.append(loginPageCustomContent);
			}

			if (Validator.isNotNull(loginPageCustomPasswordLabel)) {
				int indexOfLabelEnd = pageContent.indexOf("</label>", indexLabelToReplace);
				content.append(pageContent.substring(indexLoginForm, indexLabelToReplace + LABLE_TO_REPLACE_END_TEXT.length()));
				content.append(loginPageCustomPasswordLabel);
				content.append(pageContent.substring(indexOfLabelEnd));
			} else {
				content.append(pageContent.substring(indexLoginForm));
			}

			return content.toString();
		} catch (ConfigurationException e) {
			LOG.error(e);
			return pageContent;
		}
	}

	@Override
	protected String getSearchText() {
		return LOGIN_FORM_TAG;
	}

	@Override
	protected boolean isFilterCommand(String mvcRenderCommandName) {
		return Validator.isNull(mvcRenderCommandName) || "/login/login".equals(mvcRenderCommandName);
	}

	@Override
	protected boolean isFilterEnabled(RenderRequest request) {
		try {
			LoginFilterCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, portal.getCompanyId(request));

			return loginFilterConfigurationService.isLoginFilterEnabled(configuration);
		} catch (ConfigurationException e) {
			LOG.error(e);
			return false;
		}
	}

	private String getLoginPageCustomContent(LoginFilterCompanyConfiguration configuration) throws ConfigurationException {
		String loginPageCustomContent = loginFilterConfigurationService.getTopLoginPageCustomContent(configuration);
		if (Validator.isNotNull(loginPageCustomContent)) {
			int sessionTimeout = GetterUtil.getInteger(PropsUtil.get(PropsKeys.SESSION_TIMEOUT));
			return loginPageCustomContent.replace(SESSION_TIMEOUT_PLACEHOLDER, String.valueOf(sessionTimeout));
		}

		return StringPool.BLANK;
	}

}