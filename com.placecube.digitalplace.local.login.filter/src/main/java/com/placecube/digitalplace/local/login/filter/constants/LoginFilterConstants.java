package com.placecube.digitalplace.local.login.filter.constants;

public class LoginFilterConstants {

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	private LoginFilterConstants() {

	}
}
