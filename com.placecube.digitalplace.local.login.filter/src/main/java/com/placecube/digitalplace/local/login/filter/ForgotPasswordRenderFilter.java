package com.placecube.digitalplace.local.login.filter;

import javax.portlet.RenderRequest;
import javax.portlet.filter.PortletFilter;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterCompanyConfiguration;
import com.placecube.digitalplace.local.login.filter.configuration.LoginFilterConfigurationService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.LOGIN }, service = PortletFilter.class)
public class ForgotPasswordRenderFilter extends AbstractLoginRenderFilter {

	private static final String FORGOT_PASSWORD_SEARCH_TEXT = "<form";

	private static final Log LOG = LogFactoryUtil.getLog(ForgotPasswordRenderFilter.class);

	@Reference
	private BufferedRenderResponseWrapperFactory bufferedRenderResponseWrapperFactory;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LoginFilterConfigurationService loginFilterConfigurationService;

	@Reference
	private Portal portal;

	@Override
	protected BufferedRenderResponseWrapperFactory getBufferedRenderResponseWrapperFactory() {
		return bufferedRenderResponseWrapperFactory;
	}

	@Override
	protected String getPageWriterContent(RenderRequest request, String pageContent) {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();

		int index = pageContent.lastIndexOf(FORGOT_PASSWORD_SEARCH_TEXT);

		try {
			LoginFilterCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, companyId);

			themeDisplay.getPortletDisplay().setShowBackIcon(configuration.forgotPasswordShowBackIcon());

			String topPageCustomContent = loginFilterConfigurationService.getTopForgotPasswordPageCustomContent(configuration);

			StringBuilder stringBuilder = new StringBuilder(pageContent.substring(0, index));

			if (Validator.isNotNull(topPageCustomContent)) {
				stringBuilder.append(topPageCustomContent);
			}

			stringBuilder.append(pageContent.substring(index));

			return stringBuilder.toString();

		} catch (ConfigurationException e) {
			LOG.error(e);
			return pageContent;
		}
	}

	@Override
	protected String getSearchText() {
		return FORGOT_PASSWORD_SEARCH_TEXT;
	}

	@Override
	protected boolean isFilterCommand(String mvcRenderCommandName) {
		return "/login/forgot_password".equals(mvcRenderCommandName);
	}

	@Override
	protected boolean isFilterEnabled(RenderRequest request) {
		try {
			LoginFilterCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoginFilterCompanyConfiguration.class, portal.getCompanyId(request));
			return configuration.forgotPasswordPageTopContentEnabled();
		} catch (ConfigurationException e) {
			LOG.error(e);
			return false;
		}
	}

}