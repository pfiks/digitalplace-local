package com.placecube.digitalplace.local.input.permissions.category;

import org.osgi.service.component.annotations.Component;

import com.liferay.configuration.admin.category.ConfigurationCategory;

@Component(service = ConfigurationCategory.class)
public class PermissionsConfigurationCategory implements ConfigurationCategory {

	private static final String CATEGORY_ICON = "cog";

	private static final String CATEGORY_KEY = "permissions";

	private static final String CATEGORY_SECTION = "content-and-data";

	@Override
	public String getBundleSymbolicName() {
		return "com.placecube.digitalplace.local.input.permissions.service";
	}

	@Override
	public String getCategoryIcon() {
		return CATEGORY_ICON;
	}

	@Override
	public String getCategoryKey() {
		return CATEGORY_KEY;
	}

	@Override
	public String getCategorySection() {
		return CATEGORY_SECTION;
	}

}
