package com.placecube.digitalplace.local.input.permissions.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.categories.admin.web.constants.AssetCategoriesAdminPortletKeys;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.document.library.constants.DLPortletKeys;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.input.permissions.configuration.InputPermissionsGroupConfiguration;

@Component(immediate = true, service = InputPermissionsService.class)
public class InputPermissionsService {

	private static final Log LOG = LogFactoryUtil.getLog(InputPermissionsService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean checkModelNameDefaultPublishPermissionAsGuest(long groupId, String portletId) {

		Optional<InputPermissionsGroupConfiguration> inputPermissionsConfigurationOptional = getInputPermissionsConfiguration(groupId);

		if (inputPermissionsConfigurationOptional.isPresent()) {
			InputPermissionsGroupConfiguration inputPermissionsGroupConfiguration = inputPermissionsConfigurationOptional.get();

			return isModelNameEnabled(getModelName(portletId), inputPermissionsGroupConfiguration);
		}
		return false;
	}

	private List<String> getEnabledClassNames(InputPermissionsGroupConfiguration inputPermissionsGroupConfiguration) {
		List<String> classNames = new ArrayList<>();

		if (inputPermissionsGroupConfiguration.assetCategoriesEnabled()) {
			classNames.add(AssetCategory.class.getName());
		}
		if (inputPermissionsGroupConfiguration.dLFileEntryEnabled()) {
			classNames.add(DLFileEntry.class.getName());
		}
		if (inputPermissionsGroupConfiguration.journalArticleEnabled()) {
			classNames.add(JournalArticle.class.getName());
		}
		return classNames;
	}

	private Optional<InputPermissionsGroupConfiguration> getInputPermissionsConfiguration(long groupId) {
		try {
			return Optional.of(configurationProvider.getGroupConfiguration(InputPermissionsGroupConfiguration.class, groupId));
		} catch (ConfigurationException e) {
			LOG.error(e);
		}
		return Optional.empty();
	}

	private String getModelName(String portletId) {

		String modelName = "";

		if (portletId.equals(AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN)) {
			modelName = AssetCategory.class.getName();
		}
		if (portletId.equals(DLPortletKeys.DOCUMENT_LIBRARY_ADMIN)) {
			modelName = DLFileEntry.class.getName();
		}
		if (portletId.equals(JournalPortletKeys.JOURNAL)) {
			modelName = JournalArticle.class.getName();
		}
		return modelName;
	}

	private boolean isModelNameEnabled(String modelName, InputPermissionsGroupConfiguration inputPermissionsGroupConfiguration) {
		return getEnabledClassNames(inputPermissionsGroupConfiguration).contains(modelName);
	}
}
