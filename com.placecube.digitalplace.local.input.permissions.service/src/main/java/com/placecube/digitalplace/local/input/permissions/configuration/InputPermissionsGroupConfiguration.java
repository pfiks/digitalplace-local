package com.placecube.digitalplace.local.input.permissions.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "permissions", scope = ExtendedObjectClassDefinition.Scope.GROUP, strictScope = true)
@Meta.OCD(id = "com.placecube.digitalplace.local.input.permissions.configuration.InputPermissionsGroupConfiguration", name = "publish", localization = "content/Language", description = "input-permission-group-configuration-description")
public interface InputPermissionsGroupConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "document-library-files", description = "document-library-files-description")
	boolean dLFileEntryEnabled();

	@Meta.AD(required = false, deflt = "false", name = "vocabularies-and-categories", description = "vocabularies-and-categories-description")
	boolean assetCategoriesEnabled();

	@Meta.AD(required = false, deflt = "false", name = "journal-articles", description = "journal-articles-description")
	boolean journalArticleEnabled();
}
