package com.placecube.digitalplace.local.input.permissions.application;

import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.placecube.digitalplace.local.input.permissions.service.InputPermissionsService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/inputPermission/services", JaxrsWhiteboardConstants.JAX_RS_NAME + "=InputPermission.Rest", "oauth2.scopechecker.type=none",
		"auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class InputPermissionRESTApplication extends Application{

	@Reference
	private InputPermissionsService inputPermissionsService;

	@GET
	@Path("/check-model-name-default-publish-permission-as-guest")
	@Produces(MediaType.TEXT_PLAIN)
	public Response checkModelNameDefaultPublishPermissionAsGuest(@QueryParam("groupId") Long groupId, @QueryParam("portletId") String portletId, @Context HttpServletRequest request) {
		boolean response = inputPermissionsService.checkModelNameDefaultPublishPermissionAsGuest(groupId, portletId);
		return Response.ok(response, MediaType.TEXT_PLAIN).build();
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

}
