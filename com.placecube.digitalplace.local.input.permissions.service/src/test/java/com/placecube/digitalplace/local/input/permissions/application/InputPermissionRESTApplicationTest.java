package com.placecube.digitalplace.local.input.permissions.application;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

public class InputPermissionRESTApplicationTest extends PowerMockito {

	@InjectMocks
	private InputPermissionRESTApplication inputPermissionRESTApplication;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getSingletons_whenNoError_ThenContainClass() {

		Set<Object> result = inputPermissionRESTApplication.getSingletons();

		assertThat(result, contains(inputPermissionRESTApplication));
	}
}
