package com.placecube.digitalplace.local.input.permissions.category;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

public class PermissionsConfigurationCategoryTest extends PowerMockito {

	@InjectMocks
	private PermissionsConfigurationCategory permissionsConfigurationCategory;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getBundleSymbolicName() {
		String result = permissionsConfigurationCategory.getBundleSymbolicName();
		assertEquals("com.placecube.digitalplace.local.input.permissions.service", result);
	}

	@Test
	public void getCategoryIcon() {
		String result = permissionsConfigurationCategory.getCategoryIcon();
		assertEquals("cog", result);
	}

	@Test
	public void getCategoryKey() {
		String result = permissionsConfigurationCategory.getCategoryKey();
		assertEquals("permissions", result);
	}

	@Test
	public void getCategorySection() {
		String result = permissionsConfigurationCategory.getCategorySection();
		assertEquals("content-and-data", result);
	}

}
