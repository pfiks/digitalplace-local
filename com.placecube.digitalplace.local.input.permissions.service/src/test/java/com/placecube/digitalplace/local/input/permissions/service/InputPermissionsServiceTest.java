package com.placecube.digitalplace.local.input.permissions.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.categories.admin.web.constants.AssetCategoriesAdminPortletKeys;
import com.liferay.document.library.constants.DLPortletKeys;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.input.permissions.configuration.InputPermissionsGroupConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class InputPermissionsServiceTest extends PowerMockito {

	private final long GROUP_ID = 1L;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private InputPermissionsGroupConfiguration mockInputPermissionsGroupConfiguration;

	@InjectMocks
	private InputPermissionsService inputPermissionsService;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void checkModelNameDefaultPublishPermissionAsGuest_WhenErrorGettingConfiguration_ThenThrowException() throws ConfigurationException {
		String portletId = "portletId";
		when(mockConfigurationProvider.getGroupConfiguration(InputPermissionsGroupConfiguration.class, GROUP_ID)).thenThrow(new ConfigurationException());

		boolean result = inputPermissionsService.checkModelNameDefaultPublishPermissionAsGuest(GROUP_ID, portletId);
		assertFalse(result);
	}

	@Test
	@Parameters({ AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",false,false,false", AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",false,false,true", AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",false,true,true", AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",false,true,true",
			DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",false,false,false", DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",false,false,true", DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",true,false,true", DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",true,false,false",
			JournalPortletKeys.JOURNAL + ",false,false,false", JournalPortletKeys.JOURNAL + ",false,true,false", JournalPortletKeys.JOURNAL + ",true,true,false", JournalPortletKeys.JOURNAL + ",true,false,false" })
	public void checkModelNameDefaultPublishPermissionAsGuest_WhenConfigurationEnabledAndModelNameNotEnabled_ThenReturnFalse(String portletId, boolean assetCategoriesEnabled, boolean dLFileEntryEnabled, boolean journalArticleEnabled) throws ConfigurationException {
		when(mockConfigurationProvider.getGroupConfiguration(InputPermissionsGroupConfiguration.class, GROUP_ID)).thenReturn(mockInputPermissionsGroupConfiguration);

		when(mockInputPermissionsGroupConfiguration.assetCategoriesEnabled()).thenReturn(assetCategoriesEnabled);
		when(mockInputPermissionsGroupConfiguration.dLFileEntryEnabled()).thenReturn(dLFileEntryEnabled);
		when(mockInputPermissionsGroupConfiguration.journalArticleEnabled()).thenReturn(journalArticleEnabled);

		boolean result = inputPermissionsService.checkModelNameDefaultPublishPermissionAsGuest(GROUP_ID, portletId);
		assertFalse(result);
	}

	@Test
	@Parameters({ AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",true,false,false", AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",true,true,false", AssetCategoriesAdminPortletKeys.ASSET_CATEGORIES_ADMIN + ",true,true,true",
			DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",false,true,false", DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",true,true,false", DLPortletKeys.DOCUMENT_LIBRARY_ADMIN + ",true,true,true",
			JournalPortletKeys.JOURNAL + ",false,false,true", JournalPortletKeys.JOURNAL + ",false,true,true", JournalPortletKeys.JOURNAL + ",true,true,true" })
	public void checkModelNameDefaultPublishPermissionAsGuest_WhenConfigurationEnabledAndModelNameEnabled_ThenReturnTrue(String portletId, boolean assetCategoriesEnabled, boolean dLFileEntryEnabled, boolean journalArticleEnabled) throws ConfigurationException {
		when(mockConfigurationProvider.getGroupConfiguration(InputPermissionsGroupConfiguration.class, GROUP_ID)).thenReturn(mockInputPermissionsGroupConfiguration);

		when(mockInputPermissionsGroupConfiguration.assetCategoriesEnabled()).thenReturn(assetCategoriesEnabled);
		when(mockInputPermissionsGroupConfiguration.dLFileEntryEnabled()).thenReturn(dLFileEntryEnabled);
		when(mockInputPermissionsGroupConfiguration.journalArticleEnabled()).thenReturn(journalArticleEnabled);

		boolean result = inputPermissionsService.checkModelNameDefaultPublishPermissionAsGuest(GROUP_ID, portletId);
		assertTrue(result);
	}
}
