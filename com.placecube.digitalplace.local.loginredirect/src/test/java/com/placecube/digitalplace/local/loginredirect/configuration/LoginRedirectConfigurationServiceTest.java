package com.placecube.digitalplace.local.loginredirect.configuration;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Dictionary;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;

public class LoginRedirectConfigurationServiceTest extends PowerMockito {

	private static final String URL = "redirectURL";
	private static final long COMPANY_ID = 11;

	@InjectMocks
	private LoginRedirectConfigurationService loginRedirectConfigurationService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private LoginRedirectCompanyConfiguration mockLoginRedirectCompanyConfiguration;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getNonAdminRedirectURL_WhenNoError_ThenReturnsTheNonAdminLoginRedirectURL() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(LoginRedirectCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginRedirectCompanyConfiguration);
		when(mockLoginRedirectCompanyConfiguration.nonAdminLoginRedirectURL()).thenReturn(URL);

		String result = loginRedirectConfigurationService.getNonAdminRedirectURL(COMPANY_ID);

		assertThat(result, equalTo(URL));
	}

	@Test(expected = ConfigurationException.class)
	public void getNonAdminRedirectURL_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(LoginRedirectCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		loginRedirectConfigurationService.getNonAdminRedirectURL(COMPANY_ID);
	}

	@Test
	public void setNonAdminRedirectURL_WhenNoError_ThenSavesTheCompanyCOnfiguration() throws ConfigurationException {
		loginRedirectConfigurationService.setNonAdminRedirectURL(COMPANY_ID, URL);

		Dictionary properties = new Properties();
		properties.put("nonAdminLoginRedirectURL", URL);
		verify(mockConfigurationProvider, times(1)).saveCompanyConfiguration(LoginRedirectCompanyConfiguration.class, COMPANY_ID, properties);
	}

	@Test(expected = ConfigurationException.class)
	public void setNonAdminRedirectURL_WhenExceptionSavingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		Dictionary properties = new Properties();
		properties.put("nonAdminLoginRedirectURL", URL);
		doThrow(new ConfigurationException()).when(mockConfigurationProvider).saveCompanyConfiguration(LoginRedirectCompanyConfiguration.class, COMPANY_ID, properties);

		loginRedirectConfigurationService.setNonAdminRedirectURL(COMPANY_ID, URL);

	}

}
