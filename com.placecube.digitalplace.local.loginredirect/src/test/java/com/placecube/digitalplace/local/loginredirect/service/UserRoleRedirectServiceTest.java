package com.placecube.digitalplace.local.loginredirect.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactory;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectCompanyConfiguration;
import com.placecube.digitalplace.local.loginredirect.model.RoleRedirectConfiguration;
import com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PermissionCheckerFactoryUtil.class, RoleRedirectConfiguration.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class UserRoleRedirectServiceTest extends PowerMockito {

	private static final Long GROUP_ID = 11l;
	private static final String GROUP_URL = "GROUP_URL";
	private static final String LAYOUT_URL = "LAYOUT_URL";
	private static final String PORTAL_URL = "PORTAL_URL";
	private static final String PUBLIC_URL = "PUBLIC_URL";
	private static final Long USER_ID = 22l;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private JSONArray mockDataArray;

	@Mock
	private JSONObject mockDataJSONObjectOne;

	@Mock
	private JSONObject mockDataJSONObjectTwo;

	@Mock
	private DigitalPlacePermissionService mockDigitalPlacePermissionService;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LoginRedirectCompanyConfiguration mockLoginRedirectCompanyConfiguration;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private PermissionCheckerFactory mockPermissionCheckerFactory;

	@Mock
	private Portal mockPortal;

	@Mock
	private Role mockRole;

	@Mock
	private RoleRedirectConfiguration mockRoleRedirectConfigurationOne;

	@Mock
	private RoleRedirectConfiguration mockRoleRedirectConfigurationTwo;

	@Mock
	private User mockUser;

	@Mock
	private UserGroupRole mockUserGroupRole;

	@Mock
	private UserGroupRoleLocalService mockUserGroupRoleLocalService;

	private List<UserGroupRole> mockUserGroupRoles;

	@InjectMocks
	private UserRoleRedirectService userRoleRedirectService;

	@Before
	public void activate() {
		mockStatic(PermissionCheckerFactoryUtil.class, RoleRedirectConfiguration.class);
		mockUserGroupRoles = Collections.singletonList(mockUserGroupRole);
	}

	@Test(expected = PortalException.class)
	public void getNonAdminRedirect_WhenExceptionRetrievingTheLayout_ThenThrowsPortalException() throws Exception {
		when(mockLoginRedirectCompanyConfiguration.nonAdminLoginRedirectURL()).thenReturn(LAYOUT_URL);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, false, LAYOUT_URL)).thenThrow(new PortalException());

		userRoleRedirectService.getNonAdminRedirect(mockHttpServletRequest, mockGroup, mockLoginRedirectCompanyConfiguration);
	}

	@Test
	public void getNonAdminRedirect_WhenNoError_ThenReturnsTheFullNonAdminRedirectURL() throws Exception {
		when(mockLoginRedirectCompanyConfiguration.nonAdminLoginRedirectURL()).thenReturn(LAYOUT_URL);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, false, LAYOUT_URL)).thenReturn(mockLayout);
		when(mockLayout.getFriendlyURL()).thenReturn(LAYOUT_URL);
		when(mockPortal.getPortalURL(mockHttpServletRequest)).thenReturn(PORTAL_URL);
		when(mockPortal.getPathFriendlyURLPublic()).thenReturn(PUBLIC_URL);
		when(mockGroup.getFriendlyURL()).thenReturn(GROUP_URL);

		String result = userRoleRedirectService.getNonAdminRedirect(mockHttpServletRequest, mockGroup, mockLoginRedirectCompanyConfiguration);

		assertThat(result, equalTo(PORTAL_URL + PUBLIC_URL + GROUP_URL + LAYOUT_URL));
	}

	@Test(expected = PortalException.class)
	public void getRoleRedirection_WhenExceptionCreatingJsonArray_ThenThrowsPortalException() throws Exception {
		String redirectJsonConfiguration = "redirectJsonConfiguration";

		when(mockLoginRedirectCompanyConfiguration.roleLoginRedirectJsonConfiguration()).thenReturn(redirectJsonConfiguration);
		when(mockJsonFactory.createJSONArray(redirectJsonConfiguration)).thenThrow(new JSONException());

		userRoleRedirectService.getRoleRedirection(mockUser, mockLoginRedirectCompanyConfiguration);
	}

	@Test
	public void getRoleRedirection_WhenJsonArrayContainsJsonObjects_ThenReturnsFirstMatchingLayoutUrl() throws Exception {
		String layoutToReturn = "layoutToReturn";
		long[] userGroupIds = new long[] { 1, 2 };
		long[] userRoleIds = new long[] { 11L, 22l };
		String redirectJsonConfiguration = "redirectJsonConfiguration";
		when(mockUser.getGroupIds()).thenReturn(userGroupIds);
		when(mockUser.getRoleIds()).thenReturn(userRoleIds);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(USER_ID)).thenReturn(mockUserGroupRoles);
		when(mockUserGroupRole.getRoleId()).thenReturn(23L);
		when(mockLoginRedirectCompanyConfiguration.roleLoginRedirectJsonConfiguration()).thenReturn(redirectJsonConfiguration);
		when(mockJsonFactory.createJSONArray(redirectJsonConfiguration)).thenReturn(mockDataArray);
		when(mockDataArray.length()).thenReturn(2);
		when(mockDataArray.get(0)).thenReturn(mockDataJSONObjectOne);
		when(mockJsonFactory.createJSONObject(mockDataJSONObjectOne.toString())).thenReturn(mockDataJSONObjectOne);
		when(mockDataArray.get(1)).thenReturn(mockDataJSONObjectTwo);
		when(mockJsonFactory.createJSONObject(mockDataJSONObjectTwo.toString())).thenReturn(mockDataJSONObjectTwo);
		when(RoleRedirectConfiguration.init(mockDataJSONObjectOne)).thenReturn(mockRoleRedirectConfigurationOne);
		when(RoleRedirectConfiguration.init(mockDataJSONObjectTwo)).thenReturn(mockRoleRedirectConfigurationTwo);
		when(mockRoleRedirectConfigurationOne.getGroupId()).thenReturn(3l);
		when(mockRoleRedirectConfigurationOne.getRoleId()).thenReturn(11l);
		when(mockRoleRedirectConfigurationTwo.getGroupId()).thenReturn(2l);
		when(mockRoleRedirectConfigurationTwo.getRoleId()).thenReturn(22l);
		when(mockRoleRedirectConfigurationTwo.getLayoutUrl()).thenReturn(layoutToReturn);

		Optional<String> result = userRoleRedirectService.getRoleRedirection(mockUser, mockLoginRedirectCompanyConfiguration);

		assertThat(result.get(), equalTo(layoutToReturn));
	}

	@Test
	public void getRoleRedirection_WhenJsonArrayIsEmpty_ThenReturnOptionalEmpty() throws Exception {
		String redirectJsonConfiguration = "redirectJsonConfiguration";

		when(mockLoginRedirectCompanyConfiguration.roleLoginRedirectJsonConfiguration()).thenReturn(redirectJsonConfiguration);
		when(mockJsonFactory.createJSONArray(redirectJsonConfiguration)).thenReturn(mockDataArray);

		Optional<String> result = userRoleRedirectService.getRoleRedirection(mockUser, mockLoginRedirectCompanyConfiguration);

		assertFalse(result.isPresent());
	}

	@Test(expected = PortalException.class)
	public void hasControlPanelPermissions_WhenExceptionRetrievingUser_ThenThrowsPortalException() throws PortalException {
		when(mockPortal.getUser(mockHttpServletRequest)).thenThrow(new PortalException());

		userRoleRedirectService.hasControlPanelPermissions(mockHttpServletRequest, mockGroup);
	}

	@Test
	@Parameters({ "true", "false" })
	public void hasControlPanelPermissions_WhenNoError_ThenReturnsIfUserHasSiteAdminAccess(boolean expected) throws PortalException {
		when(mockPortal.getUser(mockHttpServletRequest)).thenReturn(mockUser);
		when(PermissionCheckerFactoryUtil.getPermissionCheckerFactory()).thenReturn(mockPermissionCheckerFactory);
		when(mockPermissionCheckerFactory.create(mockUser)).thenReturn(mockPermissionChecker);
		when(mockDigitalPlacePermissionService.hasSiteAdminAccess(mockGroup, mockPermissionChecker)).thenReturn(expected);

		boolean result = userRoleRedirectService.hasControlPanelPermissions(mockHttpServletRequest, mockGroup);

		assertThat(result, equalTo(expected));
	}

}
