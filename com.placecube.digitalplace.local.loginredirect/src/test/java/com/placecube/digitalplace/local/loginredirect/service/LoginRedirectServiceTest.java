package com.placecube.digitalplace.local.loginredirect.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectCompanyConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LoginRedirectServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1;
	private static final String PORTAL_URL = "PORTAL_URL";

	@InjectMocks
	private LoginRedirectService loginRedirectService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LoginRedirectCompanyConfiguration mockLoginRedirectCompanyConfiguration;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserRoleRedirectService mockUserRoleRedirectService;

	@Before
	public void activate() throws PortalException {
		mockStatic(ServiceContextThreadLocal.class);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getUser(mockHttpServletRequest)).thenReturn(mockUser);
		when(mockConfigurationProvider.getCompanyConfiguration(LoginRedirectCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockLoginRedirectCompanyConfiguration);
	}

	@Test(expected = ConfigurationException.class)
	public void getLoginRedirect_WhenExceptionRetrievingCompanyConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(LoginRedirectCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		loginRedirectService.getLoginRedirect(mockHttpServletRequest);
	}

	@Test(expected = PortalException.class)
	public void getLoginRedirect_WhenExceptionRetrievingRoleRedirection_ThenThrowsPortalException() throws Exception {

		when(mockUserRoleRedirectService.getRoleRedirection(any(User.class), any(LoginRedirectCompanyConfiguration.class))).thenThrow(new PortalException());

		loginRedirectService.getLoginRedirect(mockHttpServletRequest);
	}

	@Test(expected = PortalException.class)
	public void getLoginRedirect_WhenExceptionRetrievingScopeGroup_ThenThrowsPortalException() throws Exception {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getScopeGroup()).thenThrow(new PortalException());

		when(mockUserRoleRedirectService.getRoleRedirection(any(User.class), any(LoginRedirectCompanyConfiguration.class))).thenThrow(new PortalException());

		loginRedirectService.getLoginRedirect(mockHttpServletRequest);
	}

	@Test
	public void getLoginRedirect_WhenRetrievedRoleRedirectionIsNotPresentAndGroupIsGestAndUserDoesntHaveControlPannelPermission_ThenReturnsNonAdminRedirect() throws Exception {
		String nonAdminRedirect = "nonAdminRedirect";

		when(mockUserRoleRedirectService.getRoleRedirection(mockUser, mockLoginRedirectCompanyConfiguration)).thenReturn(Optional.empty());
		when(mockGroup.isGuest()).thenReturn(true);
		when(mockUserRoleRedirectService.hasControlPanelPermissions(mockHttpServletRequest, mockGroup)).thenReturn(false);
		when(mockUserRoleRedirectService.getNonAdminRedirect(mockHttpServletRequest, mockGroup, mockLoginRedirectCompanyConfiguration)).thenReturn(nonAdminRedirect);

		String result = loginRedirectService.getLoginRedirect(mockHttpServletRequest);

		assertThat(result, equalTo(nonAdminRedirect));
	}

	@Test
	@Parameters({ "true,true", "false,true", "false,false" })
	public void getLoginRedirect_WhenRetrievedRoleRedirectionIsNotPresentAndProvidedGroupIsUserControlPannelPermissionParameters_ThenReturnsEmptyString(boolean isGuest,
			boolean hasControlPanelPermissions) throws Exception {

		when(mockUserRoleRedirectService.getRoleRedirection(mockUser, mockLoginRedirectCompanyConfiguration)).thenReturn(Optional.empty());
		when(mockGroup.isGuest()).thenReturn(isGuest);
		when(mockUserRoleRedirectService.hasControlPanelPermissions(mockHttpServletRequest, mockGroup)).thenReturn(hasControlPanelPermissions);

		String result = loginRedirectService.getLoginRedirect(mockHttpServletRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getLoginRedirect_WhenRetrievedRoleRedirectionIsPresent_ThenReturnsTheFullRedirectURL() throws Exception {
		String roldeRedirection = "roldeRedirection";

		when(mockUserRoleRedirectService.getRoleRedirection(mockUser, mockLoginRedirectCompanyConfiguration)).thenReturn(Optional.of(roldeRedirection));
		when(mockPortal.getPortalURL(mockHttpServletRequest)).thenReturn(PORTAL_URL);

		String result = loginRedirectService.getLoginRedirect(mockHttpServletRequest);

		assertThat(result, equalTo(PORTAL_URL + roldeRedirection));
	}
}
