package com.placecube.digitalplace.local.loginredirect.action;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.local.loginredirect.service.LoginRedirectService;

public class LoginPostActionTest extends PowerMockito {

	private final static String REDIRECT = "redirect";

	@InjectMocks
	private LoginPostAction loginPostAction;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private LoginRedirectService mockLoginRedirectService;

	@Before
	public void activate() {
		initMocks(this);
	}

	@Test
	public void render_WhenStandardRedirectIsNullAndIsExceptionGettingLoginCustomRedirect_ThenDoesNotExecuteAnyRedirect() throws Exception {
		when(mockHttpServletRequest.getParameter(REDIRECT)).thenReturn(null);
		when(mockLoginRedirectService.getLoginRedirect(mockHttpServletRequest)).thenThrow(new PortalException());

		loginPostAction.run(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletResponse, never()).sendRedirect(anyString());
	}

	@Test
	public void render_WhenStandardRedirectIsNullAndIsExceptionSendingRedirect_ThenDoesNotThrowAnyError() throws Exception {
		String redirectValue = "redirectValue";

		when(mockHttpServletRequest.getParameter(REDIRECT)).thenReturn(null);
		when(mockLoginRedirectService.getLoginRedirect(mockHttpServletRequest)).thenReturn(redirectValue);

		doThrow(new IOException()).when(mockHttpServletResponse).sendRedirect(redirectValue);

		try {
			loginPostAction.run(mockHttpServletRequest, mockHttpServletResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void render_WhenStandardRedirectIsNullAndThereIsCustomRedirectValue_ThenSendsRedirect() throws Exception {
		String redirectValue = "redirectValue";

		when(mockHttpServletRequest.getParameter(REDIRECT)).thenReturn(null);
		when(mockLoginRedirectService.getLoginRedirect(mockHttpServletRequest)).thenReturn(redirectValue);

		loginPostAction.run(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletResponse, times(1)).sendRedirect(redirectValue);
	}

	@Test
	public void render_WhenStandardRedirectIsNullAndCustomRedirectIsEmpty_ThenDoesNotExecuteAnyRedirect() throws Exception {
		when(mockHttpServletRequest.getParameter(REDIRECT)).thenReturn(null);
		when(mockLoginRedirectService.getLoginRedirect(mockHttpServletRequest)).thenReturn(StringPool.BLANK);

		loginPostAction.run(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletResponse, never()).sendRedirect(anyString());
	}

	@Test
	public void render_WhenStandardRedirectIsNullAndCustomRedirectIsNull_ThenDoesNotExecuteCustomRedirect() throws Exception {
		when(mockHttpServletRequest.getParameter(REDIRECT)).thenReturn(null);
		when(mockLoginRedirectService.getLoginRedirect(mockHttpServletRequest)).thenReturn(null);

		loginPostAction.run(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletResponse, never()).sendRedirect(anyString());
	}

	@Test
	public void render_WhenThereIsStandardRedirect_ThenDoNothing() {
		when(mockHttpServletRequest.getParameter(REDIRECT)).thenReturn(REDIRECT);

		loginPostAction.run(mockHttpServletRequest, mockHttpServletResponse);

		verifyZeroInteractions(mockLoginRedirectService);
	}

}
