package com.placecube.digitalplace.local.loginredirect.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONObject;

@RunWith(PowerMockRunner.class)
public class RoleRedirectConfigurationTest extends PowerMockito {

	@Mock
	private JSONObject mockJsonObject;

	@InjectMocks
	private RoleRedirectConfiguration roleRedirectConfiguration;

	@Test
	public void init_WhenNoError_ThenConfiguresGroupId() {
		long groupId = 22l;
		when(mockJsonObject.getLong("groupId")).thenReturn(groupId);

		RoleRedirectConfiguration result = RoleRedirectConfiguration.init(mockJsonObject);

		assertThat(result.getGroupId(), equalTo(groupId));
	}

	@Test
	public void init_WhenNoError_ThenConfiguresRoleId() {
		long roleId = 11l;
		when(mockJsonObject.getLong("roleId")).thenReturn(roleId);

		RoleRedirectConfiguration result = RoleRedirectConfiguration.init(mockJsonObject);

		assertThat(result.getRoleId(), equalTo(roleId));
	}

	@Test
	public void init_WhenNoError_ThenLayoutUrl() {
		String layoutURL = "layoutURL";
		when(mockJsonObject.getString("layoutURL")).thenReturn(layoutURL);

		RoleRedirectConfiguration result = RoleRedirectConfiguration.init(mockJsonObject);

		assertThat(result.getLayoutUrl(), equalTo(layoutURL));
	}

}
