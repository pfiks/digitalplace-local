package com.placecube.digitalplace.local.loginredirect.service;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Portal;

import com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectCompanyConfiguration;

@SuppressWarnings("deprecation")
@Component(immediate = true, service = LoginRedirectService.class)
public class LoginRedirectService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private Portal portal;

	@Reference
	private UserRoleRedirectService userRoleRedirectService;

	public String getLoginRedirect(HttpServletRequest httpServletRequest) throws PortalException {
		User user = portal.getUser(httpServletRequest);
		Group scopeGroup = getScopeGroup();
		LoginRedirectCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoginRedirectCompanyConfiguration.class, scopeGroup.getCompanyId());
		Optional<String> redirect = userRoleRedirectService.getRoleRedirection(user, configuration);
		if (redirect.isPresent()) {
			return portal.getPortalURL(httpServletRequest).concat(redirect.get());
		} else if (scopeGroup.isGuest() && !userRoleRedirectService.hasControlPanelPermissions(httpServletRequest, scopeGroup)) {
			return userRoleRedirectService.getNonAdminRedirect(httpServletRequest, scopeGroup, configuration);
		}
		return com.liferay.petra.string.StringPool.BLANK;

	}

	private Group getScopeGroup() throws PortalException {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		return serviceContext.getScopeGroup();
	}

}
