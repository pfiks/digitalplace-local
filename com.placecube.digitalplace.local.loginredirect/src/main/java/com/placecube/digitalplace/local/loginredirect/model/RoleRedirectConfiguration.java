package com.placecube.digitalplace.local.loginredirect.model;

import com.liferay.portal.kernel.json.JSONObject;

public class RoleRedirectConfiguration {

	private long groupId;
	private String layoutUrl;
	private long roleId;

	public static RoleRedirectConfiguration init(JSONObject jsonObject) {
		return new RoleRedirectConfiguration(jsonObject);
	}

	private RoleRedirectConfiguration(JSONObject jsonObject) {
		this.roleId = jsonObject.getLong("roleId");
		this.groupId = jsonObject.getLong("groupId");
		this.layoutUrl = jsonObject.getString("layoutURL");
	}

	public long getGroupId() {
		return groupId;
	}

	public String getLayoutUrl() {
		return layoutUrl;
	}

	public long getRoleId() {
		return roleId;
	}
}
