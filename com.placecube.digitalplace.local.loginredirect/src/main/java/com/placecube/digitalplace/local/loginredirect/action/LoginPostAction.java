package com.placecube.digitalplace.local.loginredirect.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.loginredirect.service.LoginRedirectService;

@Component(property = { "key=" + PropsKeys.LOGIN_EVENTS_POST }, service = LifecycleAction.class)
public class LoginPostAction extends Action {

	private static final Log LOG = LogFactoryUtil.getLog(LoginPostAction.class);

	@Reference
	private LoginRedirectService loginRedirectService;

	@Override
	public void run(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
		if (Validator.isNull(httpServletRequest.getParameter("redirect"))) {
			try {
				String redirect = loginRedirectService.getLoginRedirect(httpServletRequest);
				if (Validator.isNotNull(redirect)) {
					httpServletResponse.sendRedirect(redirect);
				}
			} catch (Exception e) {
				LOG.debug(e);
				LOG.warn("Unable to process post login redirect - " + e.getMessage());
			}
		}
	}

}