package com.placecube.digitalplace.local.loginredirect.configuration;

import java.util.Dictionary;
import java.util.Properties;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;

@Component(immediate = true, service = LoginRedirectConfigurationService.class)
public class LoginRedirectConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public String getNonAdminRedirectURL(long companyId) throws ConfigurationException {
		LoginRedirectCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(LoginRedirectCompanyConfiguration.class, companyId);
		return configuration.nonAdminLoginRedirectURL();
	}

	public void setNonAdminRedirectURL(long companyId, String friendlyURL) throws ConfigurationException {
		Dictionary properties = new Properties();
		properties.put("nonAdminLoginRedirectURL", friendlyURL);
		configurationProvider.saveCompanyConfiguration(LoginRedirectCompanyConfiguration.class, companyId, properties);
	}

}
