package com.placecube.digitalplace.local.loginredirect.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "login-redirect", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectCompanyConfiguration", localization = "content/Language", name = "login-redirect-company-configuration-name")
public interface LoginRedirectCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "non-admin-login-redirect-url-name", description = "non-admin-login-redirect-url-description")
	String nonAdminLoginRedirectURL();

	@Meta.AD(required = false, deflt = "", name = "role-login-redirect-json-configuration-name", description = "role-login-redirect-json-configuration-description")
	String roleLoginRedirectJsonConfiguration();

}
