package com.placecube.digitalplace.local.loginredirect.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.SetUtil;
import com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectCompanyConfiguration;
import com.placecube.digitalplace.local.loginredirect.model.RoleRedirectConfiguration;
import com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService;

@Component(immediate = true, service = UserRoleRedirectService.class)
public class UserRoleRedirectService {

	@Reference
	private DigitalPlacePermissionService digitalPlacePermissionService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	@Reference
	private UserGroupRoleLocalService userGroupRoleLocalService;

	public String getNonAdminRedirect(HttpServletRequest httpServletRequest, Group group, LoginRedirectCompanyConfiguration configuration) throws PortalException {
		String nonAdminRedirectURL = configuration.nonAdminLoginRedirectURL();
		Layout friendlyURLLayout = layoutLocalService.getFriendlyURLLayout(group.getGroupId(), false, nonAdminRedirectURL);
		return portal.getPortalURL(httpServletRequest).concat(portal.getPathFriendlyURLPublic()).concat(group.getFriendlyURL()).concat(friendlyURLLayout.getFriendlyURL());
	}

	public Optional<String> getRoleRedirection(User user, LoginRedirectCompanyConfiguration configuration) throws PortalException {
		String redirectJsonConfiguration = configuration.roleLoginRedirectJsonConfiguration();
		JSONArray jsonArray = jsonFactory.createJSONArray(redirectJsonConfiguration);

		long[] userRoledIds = getUserRoleIds(user);
		long[] userGroupIds = user.getGroupIds();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject roleConfiguration = jsonFactory.createJSONObject(jsonArray.get(i).toString());
			RoleRedirectConfiguration roleRedirectConfiguration = RoleRedirectConfiguration.init(roleConfiguration);
			if (ArrayUtil.contains(userRoledIds, roleRedirectConfiguration.getRoleId()) && ArrayUtil.contains(userGroupIds, roleRedirectConfiguration.getGroupId())) {
				return Optional.of(roleRedirectConfiguration.getLayoutUrl());
			}
		}
		return Optional.empty();

	}

	public boolean hasControlPanelPermissions(HttpServletRequest httpServletRequest, Group scopeGroup) throws PortalException {
		PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.getPermissionCheckerFactory().create(portal.getUser(httpServletRequest));
		return digitalPlacePermissionService.hasSiteAdminAccess(scopeGroup, permissionChecker);
	}

	private long[] getUserRoleIds(User user) {
		Set<Long> userRoles = SetUtil.fromArray(user.getRoleIds());

		userRoles.addAll(userGroupRoleLocalService.getUserGroupRoles(user.getUserId()).stream().map(UserGroupRole::getRoleId).collect(Collectors.toSet()));

		return ArrayUtil.toLongArray(userRoles);
	}

}
