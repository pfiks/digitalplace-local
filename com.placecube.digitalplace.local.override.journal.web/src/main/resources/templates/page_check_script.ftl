<script>

AUI().ready(function(){
    onClickPublishButton();
});

Liferay.after('endNavigate',function(){
    onClickPublishButton();
});


function onClickPublishButton(){
    $('#_com_liferay_journal_web_portlet_JournalPortlet_publishButton').attr('onclick','onSubmit(event);');
}


function onSubmit(event){

    var missingAlt = false;

    $("iframe.cke_reset").contents().find("body.html-editor img.cke_widget_element").each(function(){
        var alt = $(this).attr("alt");
        if( alt === undefined || alt == ''){
            missingAlt = true;
            return false;
        }
    });
    
    if ($("iframe.cke_reset").contents().find("body.html-editor details.govuk-details").length > 0){
        $("iframe.cke_reset").contents().find("body.html-editor details.govuk-details").removeAttr("open");
    }

    if(!missingAlt) {
        let editorContent = document.createElement('div');
        editorContent.innerHTML = $('.cke_contents_ltr').val();
        $(editorContent).find('img').each((i, img)=>{
            if(!img.alt){
                missingAlt = true;
                return;
            }
        })
    }

    if(missingAlt){
        alert('Please check the content, the images require the attribute alt.');
        event.preventDefault();
        event.stopImmediatePropagation();
    }

}

</script>