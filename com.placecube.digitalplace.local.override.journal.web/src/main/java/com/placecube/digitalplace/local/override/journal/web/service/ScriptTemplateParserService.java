package com.placecube.digitalplace.local.override.journal.web.service;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateException;
import com.placecube.digitalplace.local.override.journal.web.constants.FreemarkerKeys;

@Component(immediate = true, service = ScriptTemplateParserService.class)
public class ScriptTemplateParserService {

	private static final Log LOG = LogFactoryUtil.getLog(ScriptTemplateParserService.class);

	private FreemarkerTemplateService freemarkerTemplateService;

	public String getPageCheckScript() {

		return getScript(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY, new HashMap<>());

	}

	@Reference
	protected void setFreemarkerTemplateService(FreemarkerTemplateService freemarkerTemplateService) {
		this.freemarkerTemplateService = freemarkerTemplateService;
	}

	private String getScript(String name, Map<String, String> placeholderValues) {
		Template template;
		try {
			template = freemarkerTemplateService.getTemplate(name);
			return freemarkerTemplateService.processTemplate(template, placeholderValues);
		} catch (TemplateException e) {
			LOG.error("Unable to process template", e);
		}
		return StringPool.BLANK;
	}

}
