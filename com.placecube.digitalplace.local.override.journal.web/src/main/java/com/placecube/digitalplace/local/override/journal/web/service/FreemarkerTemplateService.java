package com.placecube.digitalplace.local.override.journal.web.service;

import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateException;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.kernel.template.URLTemplateResource;

@Component(immediate = true, service = FreemarkerTemplateService.class)
public class FreemarkerTemplateService {

	public Template getTemplate(String templateKey) throws TemplateException {
		URLTemplateResource urlTemplateResource = getURLTemplateResource(templateKey);
		return TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, urlTemplateResource, false);
	}

	public String processTemplate(Template template, Map<String, String> placeholderValues) throws TemplateException {
		for (Entry<String, String> placeholderValue : placeholderValues.entrySet()) {
			template.put(placeholderValue.getKey(), placeholderValue.getValue());
		}
		UnsyncStringWriter unsyncStringWriter = getWriter();
		template.processTemplate(unsyncStringWriter);
		return unsyncStringWriter.toString();
	}

	URLTemplateResource getURLTemplateResource(String templateKey) {
		URL urlTemplate = getClass().getClassLoader().getResource(templateKey);
		return new URLTemplateResource(templateKey, urlTemplate);
	}

	UnsyncStringWriter getWriter() {
		return new UnsyncStringWriter();
	}

}
