package com.placecube.digitalplace.local.override.journal.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "web-content", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.override.journal.web.configuration.JournalArticleImageAltTextConfiguration", localization = "content/Language", name = "images")
public interface JournalArticleImageAltTextConfiguration {

	@Meta.AD(required = false, deflt = "true", name = "validate-image-alt-text")
	boolean enabled();
}
