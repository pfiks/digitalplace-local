package com.placecube.digitalplace.local.override.journal.web.servlet.taglib;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.servlet.taglib.BaseDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.override.journal.web.configuration.JournalArticleImageAltTextConfiguration;
import com.placecube.digitalplace.local.override.journal.web.service.ScriptTemplateParserService;

@Component(immediate = true, service = DynamicInclude.class)
public class EditArticleDynamicIncludePost extends BaseDynamicInclude {

	private static final String EDIT_ARTICLE_JSP = "/edit_article.jsp#post";

	private static final Log LOG = LogFactoryUtil.getLog(EditArticleDynamicIncludePost.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	private ScriptTemplateParserService scriptTemplateParserService;

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (isValidateAltTextEnabled(themeDisplay.getCompanyId())) {
			httpServletResponse.getWriter().println(scriptTemplateParserService.getPageCheckScript());
		}
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register(EDIT_ARTICLE_JSP);
	}

	@Reference
	protected void setScriptTemplateParserService(ScriptTemplateParserService scriptTemplateParserService) {
		this.scriptTemplateParserService = scriptTemplateParserService;
	}

	private boolean isValidateAltTextEnabled(long companyId) {

		try {
			JournalArticleImageAltTextConfiguration journalArticleImageAltTextConfiguration = configurationProvider.getCompanyConfiguration(JournalArticleImageAltTextConfiguration.class, companyId);
			return journalArticleImageAltTextConfiguration.enabled();
		} catch (ConfigurationException e) {
			LOG.error(e);
		}
		return true;
	}
}
