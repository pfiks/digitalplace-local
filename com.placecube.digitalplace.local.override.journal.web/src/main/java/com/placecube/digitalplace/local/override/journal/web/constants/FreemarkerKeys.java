package com.placecube.digitalplace.local.override.journal.web.constants;

public class FreemarkerKeys {

	public static final String PAGE_CHECK_TEMPLATE_KEY = "templates/page_check_script.ftl";

	private FreemarkerKeys() {

	}
}
