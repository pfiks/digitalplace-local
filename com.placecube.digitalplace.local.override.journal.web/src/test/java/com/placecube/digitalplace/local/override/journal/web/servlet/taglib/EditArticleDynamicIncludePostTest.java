package com.placecube.digitalplace.local.override.journal.web.servlet.taglib;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude.DynamicIncludeRegistry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.override.journal.web.configuration.JournalArticleImageAltTextConfiguration;
import com.placecube.digitalplace.local.override.journal.web.service.ScriptTemplateParserService;

@RunWith(PowerMockRunner.class)
public class EditArticleDynamicIncludePostTest {

	private static final long COMPANY_ID = 123L;
	private static final String KEY = "key";

	private static final String PAGE_CHECK_SCRIPT = "<script>page check script</script>";

	@InjectMocks
	private EditArticleDynamicIncludePost editArticleDynamicIncludePost;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DynamicIncludeRegistry mockDynamicIncludeRegistry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JournalArticleImageAltTextConfiguration mockJournalArticleImageAltTextConfiguration;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private ScriptTemplateParserService mockScriptTemplateParserService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Test
	public void include_WhenErrorFetchingConfiguration_ThenCatchTheErrorAndWritesPageCheckScript() throws IOException, ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(JournalArticleImageAltTextConfiguration.class, COMPANY_ID)).thenThrow(ConfigurationException.class);

		editArticleDynamicIncludePost.include(mockHttpServletRequest, mockHttpServletResponse, KEY);

		verify(mockPrintWriter, times(1)).println(PAGE_CHECK_SCRIPT);
	}

	@Test
	public void include_WhenNoErrorAndValidationCheckIsDisabled_ThenWritesPageCheckScript() throws IOException, ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(JournalArticleImageAltTextConfiguration.class, COMPANY_ID)).thenReturn(mockJournalArticleImageAltTextConfiguration);
		when(mockJournalArticleImageAltTextConfiguration.enabled()).thenReturn(false);

		editArticleDynamicIncludePost.include(mockHttpServletRequest, mockHttpServletResponse, KEY);

		verify(mockPrintWriter, never()).println(PAGE_CHECK_SCRIPT);
	}

	@Test
	public void include_WhenNoErrorAndValidationCheckIsEnabled_ThenWritesPageCheckScript() throws IOException, ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(JournalArticleImageAltTextConfiguration.class, COMPANY_ID)).thenReturn(mockJournalArticleImageAltTextConfiguration);
		when(mockJournalArticleImageAltTextConfiguration.enabled()).thenReturn(true);

		editArticleDynamicIncludePost.include(mockHttpServletRequest, mockHttpServletResponse, KEY);

		verify(mockPrintWriter, times(1)).println(PAGE_CHECK_SCRIPT);
	}

	@Test
	public void register_WhenNoError_ThenRegistersTheKeyForEditArticle() throws ConfigurationException {
		editArticleDynamicIncludePost.register(mockDynamicIncludeRegistry);

		verify(mockDynamicIncludeRegistry, times(1)).register("/edit_article.jsp#post");
	}

	@Before
	public void setUp() throws Exception {

		initMocks(this);

		editArticleDynamicIncludePost.setScriptTemplateParserService(mockScriptTemplateParserService);

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockHttpServletResponse.getWriter()).thenReturn(mockPrintWriter);
		when(mockScriptTemplateParserService.getPageCheckScript()).thenReturn(PAGE_CHECK_SCRIPT);
	}

}
