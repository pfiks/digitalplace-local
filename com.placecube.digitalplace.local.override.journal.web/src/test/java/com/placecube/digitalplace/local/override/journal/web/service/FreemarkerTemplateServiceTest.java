package com.placecube.digitalplace.local.override.journal.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateException;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.kernel.template.URLTemplateResource;
import com.liferay.registry.Filter;
import com.liferay.registry.Registry;
import com.liferay.registry.RegistryUtil;
import com.liferay.registry.ServiceTracker;
import com.placecube.digitalplace.local.override.journal.web.constants.FreemarkerKeys;
import com.placecube.digitalplace.local.override.journal.web.service.FreemarkerTemplateService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RegistryUtil.class, TemplateManagerUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.template.TemplateManagerUtil" })
public class FreemarkerTemplateServiceTest extends PowerMockito {

	private static final String PROCESSED_TEMPLATE = "PROCESSED_TEMPLATE";

	private FreemarkerTemplateService freemarkerTemplateService;

	@Mock
	private Registry mockRegistry;

	@Mock
	private ServiceTracker mockServiceTracker;

	@Mock
	private Template mockTemplate;

	@Mock
	private UnsyncStringWriter mockUnsyncStringWriter;

	@Mock
	private URLTemplateResource mockURLTemplateResource;

	@Test(expected = TemplateException.class)
	public void getTemplate_WhenError_ThenThrowsTemplateException() throws TemplateException {
		initTemplateMocks();
		when(TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, mockURLTemplateResource, false)).thenThrow(new TemplateException());

		freemarkerTemplateService.getTemplate(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY);
	}

	@Test
	public void getTemplate_WhenNoError_ThenReturnsTemplate() throws TemplateException {
		initTemplateMocks();
		when(TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, mockURLTemplateResource, false)).thenReturn(mockTemplate);

		Template result = freemarkerTemplateService.getTemplate(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY);

		assertThat(result, sameInstance(result));
	}

	@Test(expected = TemplateException.class)
	public void processTemplate_WhenError_ThenThrowsTemplateException() throws TemplateException {
		when(freemarkerTemplateService.getWriter()).thenReturn(mockUnsyncStringWriter);
		doThrow(new TemplateException()).when(mockTemplate).processTemplate(mockUnsyncStringWriter);

		freemarkerTemplateService.processTemplate(mockTemplate, new HashMap<>());
	}

	@Test
	public void processTemplate_WhenNoError_ThenReturnsProcessedTemplateAsString() throws TemplateException {
		when(freemarkerTemplateService.getWriter()).thenReturn(mockUnsyncStringWriter);
		when(mockUnsyncStringWriter.toString()).thenReturn(PROCESSED_TEMPLATE);

		String result = freemarkerTemplateService.processTemplate(mockTemplate, new HashMap<>());

		assertThat(result, equalTo(PROCESSED_TEMPLATE));
	}

	@Test
	public void processTemplate_WhenThereAreNotPlaceholderValues_ThenNothingIsPutInTemplate() throws TemplateException {
		Map<String, String> placeholderValues = new HashMap<>();

		freemarkerTemplateService.processTemplate(mockTemplate, placeholderValues);

		verify(mockTemplate, never()).put(anyString(), anyString());

	}

	@Test
	public void processTemplate_WhenThereArePlaceholderValues_ThenConfiguresTheTemplateWithValuesFromPlaceholderMap() throws TemplateException {
		Map<String, String> placeholderValues = new HashMap<>();
		placeholderValues.put("myFirstKey", "myFirstValue");
		placeholderValues.put("mySecondKey", "mySecondValue");

		String result = freemarkerTemplateService.processTemplate(mockTemplate, placeholderValues);

		verify(mockTemplate, times(1)).put("myFirstKey", "myFirstValue");
		verify(mockTemplate, times(1)).put("mySecondKey", "mySecondValue");
		assertThat(result, notNullValue());
	}

	@Before
	@SuppressWarnings("unchecked")
	public void setUp() throws Exception {
		initMocks(this);
		mockStatic(RegistryUtil.class);
		mockStatic(TemplateManagerUtil.class);
		freemarkerTemplateService = Mockito.spy(FreemarkerTemplateService.class);
	}

	private void initTemplateMocks() {
		when(RegistryUtil.getRegistry()).thenReturn(mockRegistry);
		when(mockRegistry.trackServices(any(Filter.class), any())).thenReturn(mockServiceTracker);
		when(freemarkerTemplateService.getURLTemplateResource(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY)).thenReturn(mockURLTemplateResource);
	}

}
