package com.placecube.digitalplace.local.override.journal.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateException;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.override.journal.web.constants.FreemarkerKeys;
import com.placecube.digitalplace.local.override.journal.web.service.FreemarkerTemplateService;
import com.placecube.digitalplace.local.override.journal.web.service.ScriptTemplateParserService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class ScriptTemplateParserServiceTest extends PowerMockito {

	@Mock
	private FreemarkerTemplateService mockFreemarkerTemplateService;

	@Mock
	private Template mockTemplate;

	@InjectMocks
	private ScriptTemplateParserService scriptTemplateParserService;

	@Test
	public void getPageCheckScript_WhenNoError_ThenReturnsScripts() throws TemplateException {

		when(mockFreemarkerTemplateService.getTemplate(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY)).thenReturn(mockTemplate);

		String expected = "</script>";
		when(mockFreemarkerTemplateService.processTemplate(eq(mockTemplate), eq(new HashMap<>()))).thenReturn(expected);

		assertThat(scriptTemplateParserService.getPageCheckScript(), equalTo(expected));

	}

	@Test
	public void getScript_WhenErrorGettingTemplate_ThenReturnsEmptyString() throws Exception {

		when(mockFreemarkerTemplateService.getTemplate(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY)).thenThrow(new TemplateException());

		String result = scriptTemplateParserService.getPageCheckScript();

		assertThat(result, equalTo(StringPool.BLANK));

	}

	@Test
	public void getScript_WhenErrorProcessingTemplate_ThenReturnsEmptyString() throws Exception {

		when(mockFreemarkerTemplateService.getTemplate(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY)).thenReturn(mockTemplate);
		when(mockFreemarkerTemplateService.processTemplate(eq(mockTemplate), anyMap())).thenThrow(new TemplateException());

		String result = scriptTemplateParserService.getPageCheckScript();

		assertThat(result, equalTo(StringPool.BLANK));

	}

	@Test
	public void getScript_WhenNoError_ThenReturnsEmptyString() throws Exception {

		when(mockFreemarkerTemplateService.getTemplate(FreemarkerKeys.PAGE_CHECK_TEMPLATE_KEY)).thenReturn(mockTemplate);
		String expected = "</script>";
		when(mockFreemarkerTemplateService.processTemplate(eq(mockTemplate), anyMap())).thenReturn(expected);

		String result = scriptTemplateParserService.getPageCheckScript();

		assertThat(result, equalTo(expected));

	}

}
