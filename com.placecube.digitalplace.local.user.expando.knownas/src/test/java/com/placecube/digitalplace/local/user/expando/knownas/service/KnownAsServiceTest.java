package com.placecube.digitalplace.local.user.expando.knownas.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.user.expando.knownas.configuration.KnownAsCompanyConfiguration;

public class KnownAsServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	@InjectMocks
	private KnownAsService knownAsService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private KnownAsCompanyConfiguration mockKnownAsCompanyConfiguration;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test(expected = ConfigurationException.class)
	public void getKnownAsCompanyConfiguration_WhenErrors_ThenExceptionIsThrown() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(KnownAsCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		knownAsService.getKnownAsCompanyConfiguration(COMPANY_ID);
	}

	@Test
	public void getKnownAsCompanyConfiguration_WhenNoErrors_ThenConfigurationIsReturned() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(KnownAsCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockKnownAsCompanyConfiguration);

		KnownAsCompanyConfiguration result = knownAsService.getKnownAsCompanyConfiguration(COMPANY_ID);

		assertThat(result, sameInstance(mockKnownAsCompanyConfiguration));
	}

}
