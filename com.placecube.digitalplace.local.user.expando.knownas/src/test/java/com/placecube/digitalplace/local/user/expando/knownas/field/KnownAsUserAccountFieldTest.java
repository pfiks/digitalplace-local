package com.placecube.digitalplace.local.user.expando.knownas.field;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.user.expando.knownas.configuration.KnownAsCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;
import com.placecube.digitalplace.local.user.expando.knownas.service.KnownAsService;
import com.placecube.digitalplace.local.user.expando.knownas.service.LanguageMessageService;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.model.AccountContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class KnownAsUserAccountFieldTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	private static final String EXPANDO_FIELD_NAME = "known-as";

	@InjectMocks
	private KnownAsUserAccountField knownAsUserAccountField;

	private final Locale locale = Locale.ENGLISH;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private KnownAsCompanyConfiguration mockKnownAsCompanyConfiguration;

	@Mock
	private KnownAsService mockKnownAsService;

	@Mock
	private LanguageMessageService mockLanguageMessageService;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void configureContextValue_WhenNoErrors_ThenSetExpandoField() {
		when(ParamUtil.getString(mockPortletRequest, EXPANDO_FIELD_NAME)).thenReturn(StringPool.BLANK);

		knownAsUserAccountField.configureContextValue(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addExpandoField(eq(EXPANDO_FIELD_NAME), any(String.class));
	}

	@Test
	public void getAccountCreationStep_WhenNoErrors_ThenAccountCreationStepIsReturned() {

		AccountCreationStep result = knownAsUserAccountField.getAccountCreationStep();

		assertEquals(AccountCreationStep.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getBundleId_WhenNoErrors_ThenBundleIdIsReturned() {

		String result = knownAsUserAccountField.getBundleId();

		assertEquals(KnownAsConstants.BUNDLE_ID, result);
	}

	@Test
	public void getDependentRequiredFieldNamesAndMessage_WhenNoErrors_ThenEmptyMapIsReturned() {

		Map<String, String> results = knownAsUserAccountField.getDependentRequiredFieldNamesAndMessage(StringPool.BLANK, locale);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getDisplayOrder_WhenNoErrors_ThenDisplayOrderIsReturned() {

		int result = knownAsUserAccountField.getDisplayOrder();

		assertEquals(500, result);
	}

	@Test
	public void getExpandoFieldName_WhenNoErrors_ThenExpandoFieldNameIsReturned() {

		String result = knownAsUserAccountField.getExpandoFieldName();

		assertEquals(EXPANDO_FIELD_NAME, result);
	}

	@Test
	public void getRequiredMessage_WhenNoErrors_ThenRequiredMessageIsReturned() {

		String result = knownAsUserAccountField.getRequiredMessage(locale);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void isEnabled_WhenNoErrorsAndConfigurationIsFalse_ThenFalseIsReturned() throws ConfigurationException {
		when(mockKnownAsService.getKnownAsCompanyConfiguration(COMPANY_ID)).thenReturn(mockKnownAsCompanyConfiguration);
		when(mockKnownAsCompanyConfiguration.knownAsEnabled()).thenReturn(false);

		boolean result = knownAsUserAccountField.isEnabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	public void isEnabled_WhenNoErrorsAndConfigurationIsTrue_ThenTrueIsReturned() throws ConfigurationException {
		when(mockKnownAsService.getKnownAsCompanyConfiguration(COMPANY_ID)).thenReturn(mockKnownAsCompanyConfiguration);
		when(mockKnownAsCompanyConfiguration.knownAsEnabled()).thenReturn(true);

		boolean result = knownAsUserAccountField.isEnabled(COMPANY_ID);

		assertTrue(result);
	}

	@Test
	public void isEnabled_WhenThereAreErrors_ThenFalseIsReturned() throws ConfigurationException {
		when(mockKnownAsService.getKnownAsCompanyConfiguration(COMPANY_ID)).thenThrow(ConfigurationException.class);

		boolean result = knownAsUserAccountField.isEnabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	public void isRequired_WhenNoErrors_ThenFalseIsReturned() {
		boolean result = knownAsUserAccountField.isRequired(COMPANY_ID);

		assertFalse(result);
	}

	@Test(expected = PortalException.class)
	public void render_WhenErrorsRendering_ThenExceptionIsThrown() throws PortalException, IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		doThrow(new IOException()).when(mockJspRenderer).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");

		knownAsUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenNoErrors_ThenJSPIsRendered() throws PortalException, IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		knownAsUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenNoErrors_ThenRequestAttributesAreSet() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockLanguageMessageService.getMessage("field-optional", locale)).thenReturn("optional-message");
		when(mockLanguageMessageService.getMessage("known-as", locale)).thenReturn("preferred-message");

		knownAsUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("optionalMessage", "optional-message");
		verify(mockHttpServletRequest, times(1)).setAttribute("knownAsFieldName", EXPANDO_FIELD_NAME);
		verify(mockHttpServletRequest, times(1)).setAttribute("knownAsTitle", "preferred-message");
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

}
