<%@ include file="init.jsp"%>

<gds-forms-ui:input-text 
	portletNamespace="${ portletNamespace }" 
	fieldName="${ knownAsFieldName }"
	fieldLabel="${ knownAsTitle }"
	fieldValue="${ accountContext.getExpandoValue(knownAsFieldName) }"
	errorMessage="${ accountContext.getFieldError(knownAsFieldName) }" 
/>