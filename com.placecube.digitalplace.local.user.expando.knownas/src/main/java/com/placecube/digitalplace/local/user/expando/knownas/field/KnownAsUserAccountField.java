package com.placecube.digitalplace.local.user.expando.knownas.field;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.user.expando.knownas.configuration.KnownAsCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;
import com.placecube.digitalplace.local.user.expando.knownas.service.KnownAsService;
import com.placecube.digitalplace.local.user.expando.knownas.service.LanguageMessageService;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;

@Component(immediate = true, service = UserAccountField.class)
public class KnownAsUserAccountField implements UserAccountField {

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private KnownAsService knownAsService;

	@Reference
	private LanguageMessageService languageMessageService;

	@Reference(target = "(osgi.web.symbolicname=" + KnownAsConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public void configureContextValue(AccountContext accountContext, PortletRequest portletRequest) {
		accountContext.addExpandoField(getExpandoFieldName(), ParamUtil.getString(portletRequest, getExpandoFieldName()));
	}

	@Override
	public AccountCreationStep getAccountCreationStep() {
		return AccountCreationStep.ADDITIONAL_DETAILS;
	}

	@Override
	public String getBundleId() {
		return KnownAsConstants.BUNDLE_ID;
	}

	@Override
	public Map<String, String> getDependentRequiredFieldNamesAndMessage(String selectedValue, Locale locale) {
		return new HashMap<>();
	}

	@Override
	public int getDisplayOrder() {
		return 500;
	}

	@Override
	public String getExpandoFieldName() {
		return KnownAsConstants.KNOWN_AS;
	}

	@Override
	public String getRequiredMessage(Locale locale) {
		return StringPool.BLANK;
	}

	@Override
	public boolean isEnabled(long companyId) {
		try {
			KnownAsCompanyConfiguration configuration = knownAsService.getKnownAsCompanyConfiguration(companyId);
			return configuration.knownAsEnabled();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean isRequired(long companyId) {
		return false;
	}

	@Override
	public void render(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws PortalException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Locale locale = themeDisplay.getLocale();
			httpServletRequest.setAttribute("optionalMessage", languageMessageService.getMessage("field-optional", locale));

			httpServletRequest.setAttribute("knownAsFieldName", getExpandoFieldName());
			httpServletRequest.setAttribute("knownAsTitle", languageMessageService.getMessage("known-as", locale));

			jspRenderer.renderJSP(servletContext, httpServletRequest, httpServletResponse, "/view.jsp");
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

}
