package com.placecube.digitalplace.local.user.expando.knownas.constants;

public final class KnownAsConstants {

	public static final String BUNDLE_ID = "com.placecube.digitalplace.local.user.expando.knownas";

	public static final String KNOWN_AS = "known-as";

	private KnownAsConstants() {

	}
}
