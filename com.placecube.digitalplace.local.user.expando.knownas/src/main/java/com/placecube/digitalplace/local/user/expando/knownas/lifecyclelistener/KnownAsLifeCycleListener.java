package com.placecube.digitalplace.local.user.expando.knownas.lifecyclelistener;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class KnownAsLifeCycleListener extends BasePortalInstanceLifecycleListener {

	@Reference
	private ExpandoColumnCreatorInputStreamService expandoColumnCreatorInputStreamService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		InputStream inputStream = getClassLoader().getResourceAsStream("com/placecube/digitalplace/local/user/expando/knownas/dependencies/known-as.xml");

		expandoColumnCreatorInputStreamService.createExpandoColumn(company, User.class.getName(), inputStream);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		return;
	}

}
