package com.placecube.digitalplace.local.user.expando.knownas.service;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.local.user.expando.knownas.constants.KnownAsConstants;

@Component(immediate = true, service = LanguageMessageService.class)
public class LanguageMessageService {

	public String getMessage(String key, Locale locale) {
		return AggregatedResourceBundleUtil.get(key, locale, KnownAsConstants.BUNDLE_ID, "com.placecube.digitalplace.user.account.web");
	}

}
