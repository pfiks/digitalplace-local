package com.placecube.digitalplace.local.user.expando.knownas.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.user.expando.knownas.configuration.KnownAsCompanyConfiguration", localization = "content/Language", name = "user-profile-known-as")
public interface KnownAsCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "known-as-enabled")
	boolean knownAsEnabled();

}
