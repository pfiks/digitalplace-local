package com.placecube.digitalplace.local.user.expando.knownas.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.user.expando.knownas.configuration.KnownAsCompanyConfiguration;

@Component(immediate = true, service = KnownAsService.class)
public class KnownAsService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public KnownAsCompanyConfiguration getKnownAsCompanyConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(KnownAsCompanyConfiguration.class, companyId);
	}
}
