package com.placecube.digitalplace.local.ddl.place.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = PlaceDDMService.class)
public class PlaceDDMService {

	private static final String RESOURCE_BASE_PATH = "com/placecube/digitalplace/local/ddl/place/dependencies/ddm/";

	private static final String STRUCTURE_KEY = "PLACE";

	private static final String TEMPLATE_KEY_PLACE_DISPLAY = "PLACE-DISPLAY";

	private static final String TEMPLATE_KEY_PLACE_MAP = "PLACE-MAP";

	@Reference
	private DDMInitializer ddmInitializer;

	public void configure(ServiceContext serviceContext) throws PortalException {

		ClassLoader classLoader = getClass().getClassLoader();

		DDMStructure ddmStructure = ddmInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, RESOURCE_BASE_PATH + "structure.xml", classLoader, DDLRecordSet.class, serviceContext);

		DDMTemplate ddmTemplate = getOrCreateDDLTemplate(classLoader, ddmStructure, serviceContext);

		createAssetPublisherTemplate(classLoader, ddmTemplate, serviceContext);
	}

	private void createAssetPublisherTemplate(ClassLoader classLoader, DDMTemplate ddmTemplate, ServiceContext serviceContext) throws PortalException {
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay("Place map", TEMPLATE_KEY_PLACE_MAP, AssetEntry.class,
				RESOURCE_BASE_PATH + TEMPLATE_KEY_PLACE_MAP + ".ftl", classLoader, serviceContext);

		ddmTemplateContext.addContentPlaceholder("[$FULL_VIEW_TEMPLATE_ID$]", String.valueOf(ddmTemplate.getTemplateId()));

		ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);
	}

	private DDMTemplate getOrCreateDDLTemplate(ClassLoader classLoader, DDMStructure ddmStructure, ServiceContext serviceContext) throws PortalException {
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForDDL("Place display", TEMPLATE_KEY_PLACE_DISPLAY, ddmStructure.getStructureId(),
				RESOURCE_BASE_PATH + TEMPLATE_KEY_PLACE_DISPLAY + ".ftl", classLoader, serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);
	}

}