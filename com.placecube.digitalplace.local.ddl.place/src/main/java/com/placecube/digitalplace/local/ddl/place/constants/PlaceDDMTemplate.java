package com.placecube.digitalplace.local.ddl.place.constants;

public enum PlaceDDMTemplate {

	PLACE_MAP("PLACE-MAP", "Place map"), PLACE_DISPLAY("PLACE-DISPLAY", "Place display");

	private final String key;
	private final String name;

	private PlaceDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
