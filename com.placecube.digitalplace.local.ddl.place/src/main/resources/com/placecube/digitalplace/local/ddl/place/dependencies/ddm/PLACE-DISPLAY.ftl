<#if cur_record?has_content>
	<#assign 
		title = ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Title")?first, locale)
		featureCollectionJSONObject = jsonFactoryUtil.createJSONObject()
	/>
	
	<@liferay.silently featureCollectionJSONObject.put("type", "FeatureCollection") />
	
	<#assign 
		featureJSONArray = jsonFactoryUtil.createJSONArray()
		ddmFormFieldValue = cur_record.getDDMFormFieldValues("Geolocation")?first
		coordinatesJSONObject = jsonFactoryUtil.createJSONObject(ddmFormFieldValue.getValue().getString(locale))
		geometryJSONObject = jsonFactoryUtil.createJSONObject()
		featureJSONObject = jsonFactoryUtil.createJSONObject()
		propertiesJSONObject = jsonFactoryUtil.createJSONObject()
	/>

	<@liferay.silently featureJSONObject.put("type", "Feature") />
	<@liferay.silently geometryJSONObject.put("type", "Point") />
	
	<#assign coordinatesJSONArray = [coordinatesJSONObject.getDouble("longitude"), coordinatesJSONObject.getDouble("latitude")] />
	
	<@liferay.silently geometryJSONObject.put("coordinates", coordinatesJSONArray) />
	<@liferay.silently featureJSONObject.put("geometry", geometryJSONObject) />
	<@liferay.silently propertiesJSONObject.put("title", title) />
	
	<#if stringUtil.equals(mapsAPIProvider, "Google")>
		<#assign
			images = {
				"com.liferay.document.library.kernel.model.DLFileEntry": "${themeDisplay.getProtocol()}://maps.google.com/mapfiles/ms/icons/green-dot.png",
				"com.liferay.portlet.dynamicdatalists.model.DDLRecord": "${themeDisplay.getProtocol()}://maps.google.com/mapfiles/ms/icons/red-dot.png",
				"com.liferay.journal.model.JournalArticle": "${themeDisplay.getProtocol()}://maps.google.com/mapfiles/ms/icons/blue-dot.png",
				"default": "${themeDisplay.getProtocol()}://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
			}
		/>

		<#if images?keys?seq_contains(entry.getClassName())>
			<@liferay.silently propertiesJSONObject.put("icon", images[entry.getClassName()]) />
		<#else>
			<@liferay.silently propertiesJSONObject.put("icon", images["default"]) />
		</#if>
	</#if>

	<@liferay.silently featureJSONObject.put("properties", propertiesJSONObject) />
	<@liferay.silently featureJSONArray.put(featureJSONObject) />
	<@liferay.silently featureCollectionJSONObject.put("features", featureJSONArray) />

	<h1>${title}</h1>

	<div class="row">
		<div class="col-md-8">
			<p>${ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Description")?first, locale)}</p>
		</div>
		<div class="col-md-4 text-right">
			<img width="300" src="${ddlDisplayTemplateHelper.getDocumentLibraryPreviewURL(cur_record.getDDMFormFieldValues("Image")?first, locale)}" alt="no-image" />
		</div>
	</div>

	<div>
		<div class="pt-4">
			<strong>${languageUtil.get(locale, "address")}: </strong>
			${ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Address")?first, locale)} 
		</div>
		<@liferay_map["map-display"]
			name='Map'
			points="${featureCollectionJSONObject}"
		/>
	</div>

	<@liferay_aui.script use="liferay-map-base">
		Liferay.componentReady('<@portlet.namespace />Map').then(
			function(map) {
				map.on(
					'featureClick',
					function(event) {
						var feature = event.feature;
						map.openDialog(
							{
								content: feature.getProperty('abstract'),
								marker: feature.getMarker(),
								position: feature.getGeometry().get('location')
							}
						);
					}
				);
			}
		);
	</@liferay_aui.script>
</#if>