package com.placecube.digitalplace.local.ddl.place.service;

import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

public class PlaceDDMServiceTest extends PowerMockito {

	private static final long STRUCTURE_ID = 1L;

	private static final String STRUCTURE_KEY = "PLACE";

	private static final String STRUCTURE_RESOURCE_PATH = "com/placecube/digitalplace/local/ddl/place/dependencies/ddm/structure.xml";

	private static final long TEMPLATE_ID = 2;

	private static final String TEMPLATE_KEY_PLACE_DISPLAY = "PLACE-DISPLAY";

	private static final String TEMPLATE_KEY_PLACE_MAP = "PLACE-MAP";

	private static final String TEMPLATE_NAME_PLACE_DISPLAY = "Place display";

	private static final String TEMPLATE_NAME_PLACE_MAP = "Place map";

	private static final String TEMPLATE_RESOURCE_PATH_PLACE_DISPLAY = "com/placecube/digitalplace/local/ddl/place/dependencies/ddm/PLACE-DISPLAY.ftl";

	private static final String TEMPLATE_RESOURCE_PATH_PLACE_MAP = "com/placecube/digitalplace/local/ddl/place/dependencies/ddm/PLACE-MAP.ftl";

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext1;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext2;

	@Mock
	private ServiceContext mockServiceContext;

	private ClassLoader placeDDLServiceClassLoader;

	@InjectMocks
	private PlaceDDMService placeDDMService;

	@Before
	public void activateSetUp() {
		initMocks(this);

		placeDDLServiceClassLoader = PlaceDDMService.class.getClassLoader();
	}

	@Test
	public void configure_WhenNoError_ThenConfiguresTheStructureAndTemplates() throws Exception {
		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, placeDDLServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockDDMInitializer.getDDMTemplateContextForDDL(TEMPLATE_NAME_PLACE_DISPLAY, TEMPLATE_KEY_PLACE_DISPLAY, STRUCTURE_ID, TEMPLATE_RESOURCE_PATH_PLACE_DISPLAY, placeDDLServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext1);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext1)).thenReturn(mockDDMTemplate);
		when(mockDDMTemplate.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(TEMPLATE_NAME_PLACE_MAP, TEMPLATE_KEY_PLACE_MAP, AssetEntry.class, TEMPLATE_RESOURCE_PATH_PLACE_MAP, placeDDLServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext2);

		placeDDMService.configure(mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockDDMTemplateContext2, mockDDMInitializer);
		inOrder.verify(mockDDMTemplateContext2, times(1)).addContentPlaceholder("[$FULL_VIEW_TEMPLATE_ID$]", String.valueOf(TEMPLATE_ID));
		inOrder.verify(mockDDMInitializer, times(1)).getOrCreateDDMTemplate(mockDDMTemplateContext2);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructureForDDL_WhenErrorGettingOrCreatingDDMStructure_ThenThrowsException() throws Exception {
		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, placeDDLServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenThrow(new PortalException());

		placeDDMService.configure(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplateForDDL_WhenErrorGettingOrCreatingThePlaceDisplayTemplate_ThenThrowsException() throws Exception {
		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, placeDDLServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockDDMInitializer.getDDMTemplateContextForDDL(TEMPLATE_NAME_PLACE_DISPLAY, TEMPLATE_KEY_PLACE_DISPLAY, STRUCTURE_ID, TEMPLATE_RESOURCE_PATH_PLACE_DISPLAY, placeDDLServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext1);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext1)).thenThrow(new PortalException());

		placeDDMService.configure(mockServiceContext);

	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenErrorGettingOrCreatingAssetPublisherTemplate_ThenThrowsException() throws Exception {
		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, placeDDLServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockDDMInitializer.getDDMTemplateContextForDDL(TEMPLATE_NAME_PLACE_DISPLAY, TEMPLATE_KEY_PLACE_DISPLAY, STRUCTURE_ID, TEMPLATE_RESOURCE_PATH_PLACE_DISPLAY, placeDDLServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext1);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext1)).thenReturn(mockDDMTemplate);
		when(mockDDMTemplate.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(TEMPLATE_NAME_PLACE_MAP, TEMPLATE_KEY_PLACE_MAP, AssetEntry.class, TEMPLATE_RESOURCE_PATH_PLACE_MAP, placeDDLServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext2);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext2)).thenThrow(new PortalException());

		placeDDMService.configure(mockServiceContext);
	}

}
