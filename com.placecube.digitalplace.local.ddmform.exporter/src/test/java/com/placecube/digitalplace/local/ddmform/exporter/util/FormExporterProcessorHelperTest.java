package com.placecube.digitalplace.local.ddmform.exporter.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;
import com.placecube.digitalplace.local.ddmform.exporter.constants.PlaceholderField;
import com.placecube.digitalplace.local.ddmform.exporter.service.PlaceholderFieldService;

@RunWith(PowerMockRunner.class)
public class FormExporterProcessorHelperTest extends PowerMockito {

	@InjectMocks
	private FormExporterProcessorHelper formExporterProcessorHelper;

	@Mock
	private DDMFormField mockDdmFormField;

	@Mock
	private DDMFormFieldOptions mockDdmFormFieldOptions;

	@Mock
	private DDMFormFieldValue mockDdmFormFieldValue;

	@Mock
	private DDMFormInstanceRecord mockDdmFormInstanceRecord;

	@Mock
	private DDMFormValues mockDdmFormValues;

	@Mock
	private JSONArray mockFormFieldsJsonArray;

	@Mock
	private JSONObject mockFormFieldJson;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private LocalizedValue mockLocalizedValue;

	@Mock
	private LocalizedValue mockLocalizedValue1;

	@Mock
	private LocalizedValue mockLocalizedValue2;

	@Mock
	private PlaceholderFieldService mockPlaceHolderFieldService;

	@Mock
	private Map<String, List<DDMFormFieldValue>> mockDdmFormFieldValuesReferencesMap;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Value mockValue;

	@Before
	public void setUp() {
		formExporterProcessorHelper = Mockito.spy(FormExporterProcessorHelper.class);
		formExporterProcessorHelper.jsonFactory = mockJsonFactory;
		formExporterProcessorHelper.placeHolderFieldService = mockPlaceHolderFieldService;
	}

	@Test
	public void getFormValues_WhenFieldReferenceValuesIsNotEmpty_ThenReturnValues() throws PortalException {

		String fieldReferencesString = "text,date";
		String values = "\"valueText,valueDate\"";

		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.SEPARATOR, FormExporterSchedulerKeys.DEFAULT_SEPARATOR)).thenReturn(FormExporterSchedulerKeys.DEFAULT_SEPARATOR);
		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK)).thenReturn(fieldReferencesString);

		doReturn(values).when(formExporterProcessorHelper).getValues(mockDdmFormInstanceRecord, fieldReferencesString, mockServiceContext);
		doReturn(false).when(formExporterProcessorHelper).checkEmptyValues(values, mockServiceContext);

		String result = formExporterProcessorHelper.getFormValues(mockFormFieldJson, mockDdmFormInstanceRecord, mockServiceContext);

		assertEquals("\"valueText,valueDate\"", result);
		verify(mockServiceContext, times(1)).setAttribute(FormExporterSchedulerKeys.SEPARATOR, FormExporterSchedulerKeys.DEFAULT_SEPARATOR);

	}

	@Test
	public void getFormValues_WhenFieldReferenceValuesAreEmpty_ThenReturnValuesWithAlt() throws PortalException {

		String alternativeFieldReferencesString = "textAlt,dateAlt";
		String fieldReferencesString = "text,date";
		String values = "\",\"";
		String valuesAlt = "\"valueTextAlt,valueTextAlt\"";

		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.SEPARATOR, FormExporterSchedulerKeys.DEFAULT_SEPARATOR)).thenReturn(FormExporterSchedulerKeys.DEFAULT_SEPARATOR);
		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK)).thenReturn(fieldReferencesString);
		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.ALTERNATIVE_FIELD_REFERENCES, StringPool.BLANK)).thenReturn(alternativeFieldReferencesString);

		doReturn(values).when(formExporterProcessorHelper).getValues(mockDdmFormInstanceRecord, fieldReferencesString, mockServiceContext);
		doReturn(true).when(formExporterProcessorHelper).checkEmptyValues(values, mockServiceContext);
		doReturn(valuesAlt).when(formExporterProcessorHelper).getValues(mockDdmFormInstanceRecord, alternativeFieldReferencesString, mockServiceContext);

		String result = formExporterProcessorHelper.getFormValues(mockFormFieldJson, mockDdmFormInstanceRecord, mockServiceContext);

		assertEquals("\"valueTextAlt,valueTextAlt\"", result);
		verify(mockServiceContext, times(1)).setAttribute(FormExporterSchedulerKeys.SEPARATOR, FormExporterSchedulerKeys.DEFAULT_SEPARATOR);

	}

	@Test
	public void getValues() throws PortalException {
		String fieldReferencesString = "text,date";
		String[] fieldReferences = { "text", "date" };
		when(mockDdmFormInstanceRecord.getDDMFormValues()).thenReturn(mockDdmFormValues);
		when(mockDdmFormValues.getDDMFormFieldValuesReferencesMap(true)).thenReturn(mockDdmFormFieldValuesReferencesMap);

		formExporterProcessorHelper.getValues(mockDdmFormInstanceRecord, fieldReferencesString, mockServiceContext);
		verify(formExporterProcessorHelper, times(1)).getValuesFromFieldReferences(Mockito.any(StringBuilder.class), Mockito.eq(fieldReferences), Mockito.eq(mockDdmFormFieldValuesReferencesMap),
				Mockito.eq(mockServiceContext));
	}

	@Test
	public void getValuesFromFieldReferences_WhenFieldReferencesHasTwoValues() throws JSONException {
		StringBuilder values = new StringBuilder();
		String[] fieldReferences = { "text", "date" };
		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		ddmFormFieldValues.add(mockDdmFormFieldValue);

		when(mockServiceContext.getAttribute(FormExporterSchedulerKeys.SEPARATOR)).thenReturn("-");

		when(mockDdmFormFieldValuesReferencesMap.get("text")).thenReturn(ddmFormFieldValues);
		when(mockDdmFormFieldValuesReferencesMap.get("date")).thenReturn(ddmFormFieldValues);

		doNothing().when(formExporterProcessorHelper).addValue(values, mockDdmFormFieldValue, mockServiceContext);

		formExporterProcessorHelper.getValuesFromFieldReferences(values, fieldReferences, mockDdmFormFieldValuesReferencesMap, mockServiceContext);

		assertEquals("-", values.toString());
	}

	@Test
	public void getValuesFromFieldReferences_WhenFormInstanceRecordIdIsAFieldReference_ThenFormInstanceRecordIdIsAddedToOutput() throws JSONException {
		StringBuilder values = new StringBuilder();
		String[] fieldReferences = new String[] { PlaceholderField.FORM_INSTANCE_RECORD_ID.getFieldName() };

		long formInstanceRecordId = 123l;
		when(mockPlaceHolderFieldService.getPlaceholderFieldValue(PlaceholderField.FORM_INSTANCE_RECORD_ID, mockServiceContext)).thenReturn(String.valueOf(formInstanceRecordId));

		formExporterProcessorHelper.getValuesFromFieldReferences(values, fieldReferences, mockDdmFormFieldValuesReferencesMap, mockServiceContext);

		assertEquals(String.valueOf(formInstanceRecordId), values.toString());
	}

	@Test
	public void getValuesFromFieldReferences_WhenFormInstanceRecordIdNotAFieldReference_ThenFormInstanceRecordIdNotAddedToOutput() throws JSONException {
		StringBuilder values = new StringBuilder();
		String[] fieldReferences = new String[0];

		long formInstanceRecordId = 123l;
		when(mockPlaceHolderFieldService.getPlaceholderFieldValue(PlaceholderField.FORM_INSTANCE_RECORD_ID, mockServiceContext)).thenReturn(String.valueOf(formInstanceRecordId));

		formExporterProcessorHelper.getValuesFromFieldReferences(values, fieldReferences, mockDdmFormFieldValuesReferencesMap, mockServiceContext);

		assertEquals(StringPool.BLANK, values.toString());
	}

	@Test
	public void addValue() throws JSONException {
		StringBuilder values = new StringBuilder("text");

		doReturn(", date").when(formExporterProcessorHelper).getValue(mockDdmFormFieldValue, mockServiceContext);
		formExporterProcessorHelper.addValue(values, mockDdmFormFieldValue, mockServiceContext);
		assertEquals("text, date", values.toString());
	}

	@Test
    public void getValue_WhenValueIsNotSelectRadioOrCheckboxMultipleAndValueNoEmpty_ThenReturnStringValue() throws JSONException {

        when(mockDdmFormFieldValue.getValue()).thenReturn(mockValue);
        when(mockServiceContext.getLocale()).thenReturn(Locale.UK);
        when(mockValue.getString(Locale.UK)).thenReturn("valueString");

        when(mockDdmFormFieldValue.getType()).thenReturn("text");

        String fieldValue = formExporterProcessorHelper.getValue(mockDdmFormFieldValue, mockServiceContext);

        assertEquals("valueString", fieldValue);
    }

	@Test
    public void getValue_WhenValueIsNotEmptyAndTypeIsSelectRadio_ThenReturnValueString() throws JSONException {

        when(mockDdmFormFieldValue.getValue()).thenReturn(mockValue);
        when(mockServiceContext.getLocale()).thenReturn(Locale.UK);
        when(mockValue.getString(Locale.UK)).thenReturn("[\"Option24134136\"]");

        when(mockDdmFormFieldValue.getType()).thenReturn("select");

        when(mockDdmFormFieldValue.getDDMFormField()).thenReturn(mockDdmFormField);
        when(mockDdmFormField.getDDMFormFieldOptions()).thenReturn(mockDdmFormFieldOptions);

        when(mockDdmFormFieldOptions.getOptionLabels("Option24134136")).thenReturn(mockLocalizedValue);
        when(mockLocalizedValue.getString(Locale.UK)).thenReturn("Option24134136String");

        String fieldValue = formExporterProcessorHelper.getValue(mockDdmFormFieldValue, mockServiceContext);

        assertEquals("Option24134136String", fieldValue);
    }

	@Test
	public void getValue_WhenValueHasOneValueAndIsTypeCheckbox_ThenReturnValueString() throws JSONException {

		String valueString = "[\"Option123\",\"Option456\"\"]";

		when(mockDdmFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

		when(mockDdmFormFieldValue.getType()).thenReturn("checkbox_multiple");
		when(mockValue.getString(Locale.UK)).thenReturn(valueString);

		when(mockDdmFormFieldValue.getDDMFormField()).thenReturn(mockDdmFormField);
		when(mockDdmFormField.getDDMFormFieldOptions()).thenReturn(mockDdmFormFieldOptions);

		when(mockJsonFactory.createJSONArray(valueString)).thenReturn(mockFormFieldsJsonArray);
		when(mockFormFieldsJsonArray.length()).thenReturn(2);
		when(mockFormFieldsJsonArray.getString(0)).thenReturn("Option123");
		when(mockFormFieldsJsonArray.getString(1)).thenReturn("Option456");

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

		when(mockDdmFormFieldOptions.getOptionLabels("Option123")).thenReturn(mockLocalizedValue1);
		when(mockLocalizedValue1.getString(Locale.UK)).thenReturn("Option123Value");

		when(mockDdmFormFieldOptions.getOptionLabels("Option456")).thenReturn(mockLocalizedValue2);
		when(mockLocalizedValue2.getString(Locale.UK)).thenReturn("Option456Value");

		String fieldValue = formExporterProcessorHelper.getValue(mockDdmFormFieldValue, mockServiceContext);

		assertEquals("Option123Value/Option456Value", fieldValue);
	}

	@Test
    public void getValue_WhenValueIsEmpty_ThenReturnEmptyString() throws JSONException {

        when(mockDdmFormFieldValue.getValue()).thenReturn(mockValue);
        when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

        when(mockValue.getString(Locale.UK)).thenReturn("");

        String fieldValue = formExporterProcessorHelper.getValue(mockDdmFormFieldValue, mockServiceContext);

        assertEquals("", fieldValue);
    }

	@Test
    public void getValue_WhenValueIsEmptyBrackets_ThenReturnEmptyString() throws JSONException {

        when(mockDdmFormFieldValue.getValue()).thenReturn(mockValue);
        when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

        when(mockValue.getString(Locale.UK)).thenReturn("[]");

        String fieldValue = formExporterProcessorHelper.getValue(mockDdmFormFieldValue, mockServiceContext);

        assertEquals("", fieldValue);
    }

	@Test
	public void selectAndRadioFieldGetValueTest() {
		String valueString = "[\"Option123\"]";

		when(mockDdmFormFieldValue.getDDMFormField()).thenReturn(mockDdmFormField);
		when(mockDdmFormField.getDDMFormFieldOptions()).thenReturn(mockDdmFormFieldOptions);

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

		when(mockDdmFormFieldOptions.getOptionLabels("Option123")).thenReturn(mockLocalizedValue);

		when(mockLocalizedValue.getString(Locale.UK)).thenReturn("Option123Value");

		String result = formExporterProcessorHelper.selectAndRadioFieldGetValue(mockDdmFormFieldValue, mockServiceContext, valueString);

		assertEquals("Option123Value", result);
	}

	@Test
	public void checkboxFieldGetValue() throws JSONException {
		String valueString = "[\"Option123\", \"Option456\"]";

		when(mockDdmFormFieldValue.getDDMFormField()).thenReturn(mockDdmFormField);
		when(mockDdmFormField.getDDMFormFieldOptions()).thenReturn(mockDdmFormFieldOptions);

		when(mockJsonFactory.createJSONArray(valueString)).thenReturn(mockFormFieldsJsonArray);
		when(mockFormFieldsJsonArray.length()).thenReturn(2);
		when(mockFormFieldsJsonArray.getString(0)).thenReturn("Option123");
		when(mockFormFieldsJsonArray.getString(1)).thenReturn("Option456");

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

		when(mockDdmFormFieldOptions.getOptionLabels("Option123")).thenReturn(mockLocalizedValue1);
		when(mockLocalizedValue1.getString(Locale.UK)).thenReturn("Option123Value");

		when(mockDdmFormFieldOptions.getOptionLabels("Option456")).thenReturn(mockLocalizedValue2);
		when(mockLocalizedValue2.getString(Locale.UK)).thenReturn("Option456Value");

		String result = formExporterProcessorHelper.checkboxFieldGetValue(mockDdmFormFieldValue, mockServiceContext, valueString);

		assertEquals("Option123Value/Option456Value", result);
	}

	@Test
	public void checkEmptyValues_WhenStringHasValue_ThenReturnFalse() {

		String values = "\"text,date\"";

		when(mockServiceContext.getAttribute(FormExporterSchedulerKeys.SEPARATOR)).thenReturn("-");

		boolean result = formExporterProcessorHelper.checkEmptyValues(values, mockServiceContext);
		assertFalse(result);
	}

	@Test
	public void checkEmptyValues_WhenStringHasNoValue_ThenReturnTrue() {

		String values = "\" , - \"";

		when(mockServiceContext.getAttribute(FormExporterSchedulerKeys.SEPARATOR)).thenReturn("-");

		boolean result = formExporterProcessorHelper.checkEmptyValues(values, mockServiceContext);
		assertTrue(result);
	}
}
