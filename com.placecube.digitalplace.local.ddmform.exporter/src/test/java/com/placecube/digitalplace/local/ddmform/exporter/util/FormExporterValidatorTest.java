package com.placecube.digitalplace.local.ddmform.exporter.util;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
public class FormExporterValidatorTest extends PowerMockito {

    private static final long formInstanceId = 123L;

    @Mock
    private DDMFormInstance mockDdmFormInstance;

    @Mock
    private DDMFormInstanceLocalService mockDdmFormInstanceLocalService;

    @InjectMocks
    private FormExporterValidator formExporterValidator;

    @Mock
    private FormExporterValidatorHelper formExporterValidatorHelper;

    @Mock
    private JSONArray mockJsonArray;

    @Mock
    private JSONObject mockFormConfigurationJson;

    @Before
    public void setUp() {
        formExporterValidator = Mockito.spy(FormExporterValidator.class);
        formExporterValidator.ddmFormInstanceLocalService = mockDdmFormInstanceLocalService;
        formExporterValidator.formExporterValidatorHelper = formExporterValidatorHelper;
    }

    @Test
    public void validateFormExporterConfigurationJson_WhenFormInstanceIdIsNull_ThenReturnFalse() throws PortalException {

        when(mockFormConfigurationJson.getLong("formInstanceId")).thenReturn(0L);

        boolean result = formExporterValidator.validateFormExporterConfigurationJson(mockFormConfigurationJson);

        assertFalse(result);
    }

    @Test
    public void validateFormExporterConfigurationJson_WhenDdmFormInstanceIsNull_ThenReturnFalse() throws PortalException {

        when(mockFormConfigurationJson.getLong("formInstanceId")).thenReturn(formInstanceId);

        when(mockDdmFormInstanceLocalService.fetchDDMFormInstance(formInstanceId)).thenReturn(null);

        boolean result = formExporterValidator.validateFormExporterConfigurationJson(mockFormConfigurationJson);

        assertFalse(result);
    }

    @Test
    public void validateFormExporterConfigurationJson_WhenThereIsNotJsonArrayFormFields_ThenReturnFalse() throws PortalException {

        when(mockFormConfigurationJson.getLong("formInstanceId")).thenReturn(formInstanceId);

        when(mockDdmFormInstanceLocalService.fetchDDMFormInstance(formInstanceId)).thenReturn(mockDdmFormInstance);
        when(mockFormConfigurationJson.getJSONArray("formFields")).thenReturn(null);

        boolean result = formExporterValidator.validateFormExporterConfigurationJson(mockFormConfigurationJson);

        assertFalse(result);
    }

    @Test
    public void validateFormExporterConfigurationJson_WhenFormInstanceIdIsValidJsonConfigurationHasFormFieldJsonArrayNotEmptyAndIsValid_ThenReturnTrue() throws PortalException {

        when(mockFormConfigurationJson.getLong("formInstanceId")).thenReturn(formInstanceId);

        when(mockDdmFormInstanceLocalService.fetchDDMFormInstance(formInstanceId)).thenReturn(mockDdmFormInstance);
        when(mockFormConfigurationJson.getJSONArray("formFields")).thenReturn(mockJsonArray);

        when(formExporterValidatorHelper.validateJsonFormFields(mockDdmFormInstance, mockJsonArray)).thenReturn(true);

        boolean result = formExporterValidator.validateFormExporterConfigurationJson(mockFormConfigurationJson);

        assertTrue(result);
    }
}
