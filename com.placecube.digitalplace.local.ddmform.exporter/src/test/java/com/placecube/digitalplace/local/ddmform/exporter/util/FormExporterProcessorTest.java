package com.placecube.digitalplace.local.ddmform.exporter.util;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.CSVUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;

@RunWith(PowerMockRunner.class)
public class FormExporterProcessorTest extends PowerMockito {

	private static final String TEXT_COMMA_DATE = "text,date";

	private static final String TIME = "time";

	@InjectMocks
	private FormExporterProcessor formExporterProcessor;

	@Mock
	private Date mockDate;

	@Mock
	private DateFormat mockDateFormat;

	@Mock
	private DDMFormInstance mockDdmFormInstance;

	@Mock
	private DDMFormInstanceRecord mockDdmFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private FormExporterProcessorHelper mockFormExporterProcessorHelper;

	@Mock
	private JSONObject mockFormFieldJson;

	@Mock
	private JSONArray mockFormFieldsJsonArray;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContextHelper mockServiceContextHelper;

	@Test
	public void addFormValues_WhenOneRecordAndArrayLengthOneAndCreateDateDisabled() throws PortalException {

		String values = "valueText,valueDate";

		StringBuilder csvString = new StringBuilder();

		boolean createDateEnabled = false;
		long companyId = 123L;

		List<DDMFormInstanceRecord> ddmFormInstanceRecordList = new ArrayList<>();
		ddmFormInstanceRecordList.add(mockDdmFormInstanceRecord);

		long formInstanceId = 1222;
		when(mockDdmFormInstance.getFormInstanceId()).thenReturn(formInstanceId);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecords(formInstanceId, WorkflowConstants.STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(ddmFormInstanceRecordList);

		when(mockDdmFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockServiceContextHelper.getServiceContext(companyId)).thenReturn(mockServiceContext);

		when(mockFormFieldsJsonArray.length()).thenReturn(1);
		when(mockFormFieldsJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);

		when(mockFormExporterProcessorHelper.getFormValues(mockFormFieldJson, mockDdmFormInstanceRecord, mockServiceContext)).thenReturn(values);

		formExporterProcessor.addFormValues(csvString, mockDdmFormInstance, mockFormFieldsJsonArray, createDateEnabled, mockDateFormat);

		assertEquals(CSVUtil.encode("valueText,valueDate") + StringPool.NEW_LINE, csvString.toString());
	}

	@Test
	public void addFormValues_WhenOneRecordAndArrayLengthOneAndCreateDateEnabled() throws PortalException {

		String values = "valueText,valueDate";

		StringBuilder csvString = new StringBuilder();

		boolean createDateEnabled = true;
		long companyId = 123L;

		List<DDMFormInstanceRecord> ddmFormInstanceRecordList = new ArrayList<>();
		ddmFormInstanceRecordList.add(mockDdmFormInstanceRecord);

		long formInstanceId = 1222;
		when(mockDdmFormInstance.getFormInstanceId()).thenReturn(formInstanceId);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecords(formInstanceId, WorkflowConstants.STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(ddmFormInstanceRecordList);

		when(mockDdmFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockServiceContextHelper.getServiceContext(companyId)).thenReturn(mockServiceContext);

		when(mockFormFieldsJsonArray.length()).thenReturn(1);
		when(mockFormFieldsJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);

		when(mockFormExporterProcessorHelper.getFormValues(mockFormFieldJson, mockDdmFormInstanceRecord, mockServiceContext)).thenReturn(values);

		when(mockDdmFormInstanceRecord.getCreateDate()).thenReturn(mockDate);

		when(mockDateFormat.format(Mockito.any(Date.class), Mockito.any(StringBuffer.class), Mockito.any(FieldPosition.class))).thenReturn(new StringBuffer("20221201T0000"));

		formExporterProcessor.addFormValues(csvString, mockDdmFormInstance, mockFormFieldsJsonArray, createDateEnabled, mockDateFormat);

		assertEquals(CSVUtil.encode("valueText,valueDate") + ",20221201T0000" + StringPool.NEW_LINE, csvString.toString());
	}

	@Test
	public void addFormValues_WhenOneRecordAndArrayLengthTwoAndCreateDateDisabled() throws PortalException {

		String values = "valueText,valueDate";
		String values2 = "valueTime";
		StringBuilder csvString = new StringBuilder();

		boolean createDateEnabled = false;
		long companyId = 123L;

		List<DDMFormInstanceRecord> ddmFormInstanceRecordList = new ArrayList<>();
		ddmFormInstanceRecordList.add(mockDdmFormInstanceRecord);

		long formInstanceId = 1222;
		when(mockDdmFormInstance.getFormInstanceId()).thenReturn(formInstanceId);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecords(formInstanceId, WorkflowConstants.STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(ddmFormInstanceRecordList);

		when(mockDdmFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockServiceContextHelper.getServiceContext(companyId)).thenReturn(mockServiceContext);

		when(mockFormFieldsJsonArray.length()).thenReturn(2);
		when(mockFormFieldsJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);
		when(mockFormFieldsJsonArray.getJSONObject(1)).thenReturn(mockFormFieldJson);

		when(mockFormExporterProcessorHelper.getFormValues(mockFormFieldJson, mockDdmFormInstanceRecord, mockServiceContext)).thenReturn(values, values2);

		formExporterProcessor.addFormValues(csvString, mockDdmFormInstance, mockFormFieldsJsonArray, createDateEnabled, mockDateFormat);

		assertEquals(CSVUtil.encode("valueText,valueDate") + ",valueTime" + StringPool.NEW_LINE, csvString.toString());
	}

	@Test
	public void addFormValues_WhenTwoRecordsAndArrayLengthOneAndCreateDateDisabled() throws PortalException {

		String values = "valueText,valueDate";
		String values2 = "valueTime";
		StringBuilder csvString = new StringBuilder();

		boolean createDateEnabled = false;
		long companyId = 123L;

		List<DDMFormInstanceRecord> ddmFormInstanceRecordList = new ArrayList<>();
		ddmFormInstanceRecordList.add(mockDdmFormInstanceRecord);
		ddmFormInstanceRecordList.add(mockDdmFormInstanceRecord);

		long formInstanceId = 1222;
		when(mockDdmFormInstance.getFormInstanceId()).thenReturn(formInstanceId);
		when(mockDDMFormInstanceRecordLocalService.getFormInstanceRecords(formInstanceId, WorkflowConstants.STATUS_APPROVED, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null))
				.thenReturn(ddmFormInstanceRecordList);

		when(mockDdmFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockServiceContextHelper.getServiceContext(companyId)).thenReturn(mockServiceContext);

		when(mockFormFieldsJsonArray.length()).thenReturn(1);
		when(mockFormFieldsJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);

		when(mockFormExporterProcessorHelper.getFormValues(mockFormFieldJson, mockDdmFormInstanceRecord, mockServiceContext)).thenReturn(values, values2);

		formExporterProcessor.addFormValues(csvString, mockDdmFormInstance, mockFormFieldsJsonArray, createDateEnabled, mockDateFormat);

		assertEquals(CSVUtil.encode("valueText,valueDate") + StringPool.NEW_LINE + "valueTime" + StringPool.NEW_LINE, csvString.toString());
	}

	@Test
	public void createDataHeaderCsv_WhenCreationDateIsEnabled_ThenReturnHeader() {

		when(mockFormFieldsJsonArray.length()).thenReturn(2);
		when(mockFormFieldsJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);
		when(mockFormFieldsJsonArray.getJSONObject(1)).thenReturn(mockFormFieldJson);

		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK)).thenReturn(TEXT_COMMA_DATE, TIME);

		boolean createDateEnabled = true;

		String result = formExporterProcessor.createDataHeaderCsv(mockFormFieldsJsonArray, createDateEnabled);

		assertEquals("text_date,time,Creation date\n", result);
	}

	@Test
	public void createDataHeaderCsv_WhenCreationDateIsNotEnabled_ThenReturnHeader() {

		when(mockFormFieldsJsonArray.length()).thenReturn(2);
		when(mockFormFieldsJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);
		when(mockFormFieldsJsonArray.getJSONObject(1)).thenReturn(mockFormFieldJson);

		when(mockFormFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK)).thenReturn(TEXT_COMMA_DATE, TIME);

		boolean createDateEnabled = false;

		String result = formExporterProcessor.createDataHeaderCsv(mockFormFieldsJsonArray, createDateEnabled);

		assertEquals("text_date,time\n", result);
	}
}