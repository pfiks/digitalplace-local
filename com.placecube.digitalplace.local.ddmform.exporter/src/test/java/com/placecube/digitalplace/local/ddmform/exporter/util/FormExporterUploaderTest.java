package com.placecube.digitalplace.local.ddmform.exporter.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.model.DLVersionNumberIncrease;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, MimeTypesUtil.class})
public class FormExporterUploaderTest extends PowerMockito {

	@InjectMocks
	private FormExporterUploader formExporterUploader;

	@Mock
	private DDMFormInstance mockDdmFormInstance;

	@Mock
	private DLAppLocalService mockDlAppLocalService;

	@Mock
	private FileEntry mockFileEntry;

	@Mock
	private Folder mockFolder1;

	@Mock
	private Folder mockFolder2;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void createAndUploadFile_WhenFileIsAlreadyCreated() throws PortalException {

		long folderId = 12L;
		long formInstanceId = 34L;
		long scopeGroupId = 56L;
		long userId = 78L;
		long entryId = 90L;

		String csv = "formInstanceId";
		String dateString = "_01122022_0000";
		String fileName = "34_01122022_0000.csv";
		String mimeType = "mimeType";

		when(mockDdmFormInstance.getFormInstanceId()).thenReturn(formInstanceId);

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

		when(DateUtil.getCurrentDate("_ddMMyyyy_HHmmss", Locale.UK)).thenReturn(dateString);
		mockStatic(MimeTypesUtil.class);
		when(MimeTypesUtil.getContentType(fileName)).thenReturn(mimeType);

		when(mockServiceContext.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockDlAppLocalService.fetchFileEntryByExternalReferenceCode(scopeGroupId, fileName)).thenReturn(mockFileEntry);

		when(mockServiceContext.getUserId()).thenReturn(userId);

		when(mockFileEntry.getFileEntryId()).thenReturn(entryId);

		formExporterUploader.createAndUploadFile(csv, mockDdmFormInstance, folderId, mockServiceContext);

		verify(mockDlAppLocalService, times(1)).updateFileEntry(Mockito.eq(userId), Mockito.eq(entryId), Mockito.eq(fileName), Mockito.eq(mimeType), Mockito.eq(fileName), Mockito.eq(fileName),
				Mockito.anyString(), Mockito.eq(null), Mockito.eq(DLVersionNumberIncrease.MINOR), Mockito.any(byte[].class), Mockito.eq(null), Mockito.eq(null), Mockito.eq(null),
				Mockito.eq(mockServiceContext));
	}

	@Test
	public void createAndUploadFile_WhenFileIsNotCreated() throws PortalException {

		long folderId = 12L;
		long formInstanceId = 34L;
		long scopeGroupId = 56L;
		long userId = 78L;

		String csv = "formInstanceId";
		String dateString = "_01122022_0000";
		String fileName = "34_01122022_0000.csv";
		String mimeType = "mimeType";

		when(mockDdmFormInstance.getFormInstanceId()).thenReturn(formInstanceId);

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);

		when(DateUtil.getCurrentDate("_ddMMyyyy_HHmmss", Locale.UK)).thenReturn(dateString);
		mockStatic(MimeTypesUtil.class);
		when(MimeTypesUtil.getContentType(fileName)).thenReturn(mimeType);

		when(mockServiceContext.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockDlAppLocalService.fetchFileEntryByExternalReferenceCode(scopeGroupId, fileName)).thenReturn(null);

		when(mockServiceContext.getUserId()).thenReturn(userId);

		formExporterUploader.createAndUploadFile(csv, mockDdmFormInstance, folderId, mockServiceContext);

		verify(mockDlAppLocalService, times(1)).addFileEntry(Mockito.eq(fileName), Mockito.eq(userId), Mockito.eq(scopeGroupId), Mockito.eq(folderId), Mockito.eq(fileName), Mockito.eq(mimeType),
				Mockito.any(byte[].class), Mockito.eq(null), Mockito.eq(null), Mockito.eq(null), Mockito.eq(mockServiceContext));
	}

	@Test
	public void getOrCreateFolder() throws PortalException {

		String formName = "formName";

		long folderId = 123L;

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);
		when(mockDdmFormInstance.getName(Locale.UK)).thenReturn(formName);

		doReturn(folderId).when(formExporterUploader).getOrCreateFoldersPath(Mockito.eq("/Forms/Export/formName"), Mockito.eq(mockServiceContext));

		formExporterUploader.getOrCreateFolder(mockDdmFormInstance, mockServiceContext);

		verify(formExporterUploader, times(1)).getOrCreateFolder(mockDdmFormInstance, mockServiceContext);
	}

	@Test
	public void getOrCreateFoldersPath_WhenFoldersAreNotCreated_ThenReturnFolderIdFromTheDeepest() throws PortalException {

		long scopeGroupId = 123L;
		long folderId1 = 456L;
		long folderId2 = 789L;
		long userId = 111L;

		String path = "/Forms/Export/";

		when(mockServiceContext.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);

		when(mockDlAppLocalService.getFolder(scopeGroupId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, "Forms")).thenThrow(new PortalException());
		when(mockDlAppLocalService.addFolder(null, userId, scopeGroupId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, "Forms", "Forms", mockServiceContext)).thenReturn(mockFolder1);
		when(mockFolder1.getFolderId()).thenReturn(folderId1);

		when(mockDlAppLocalService.getFolder(scopeGroupId, folderId1, "Export")).thenThrow(new PortalException());
		when(mockDlAppLocalService.addFolder(null, userId, scopeGroupId, folderId1, "Export", "Export", mockServiceContext)).thenReturn(mockFolder2);
		when(mockFolder2.getFolderId()).thenReturn(folderId2);

		long parentFolderId = formExporterUploader.getOrCreateFoldersPath(path, mockServiceContext);

		assertEquals(folderId2, parentFolderId);
	}

	@Test
	public void getOrCreateFoldersPath_WhenParentIsAlreadyCreated_ThenReturnFolderIdFromTheDeepest() throws PortalException {

		long scopeGroupId = 123L;
		long folderId1 = 456L;
		long folderId2 = 789L;
		long userId = 111L;

		String path = "/Forms/Export/";

		when(mockServiceContext.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);

		when(mockDlAppLocalService.getFolder(scopeGroupId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, "Forms")).thenReturn(mockFolder1);
		when(mockFolder1.getFolderId()).thenReturn(folderId1);

		when(mockDlAppLocalService.getFolder(scopeGroupId, folderId1, "Export")).thenThrow(new PortalException());
		when(mockDlAppLocalService.addFolder(null, userId, scopeGroupId, folderId1, "Export", "Export", mockServiceContext)).thenReturn(mockFolder2);
		when(mockFolder2.getFolderId()).thenReturn(folderId2);

		long parentFolderId = formExporterUploader.getOrCreateFoldersPath(path, mockServiceContext);

		assertEquals(folderId2, parentFolderId);
	}

	@Before
	public void setUp() {
		mockStatic(DateUtil.class);
		formExporterUploader = Mockito.spy(FormExporterUploader.class);
		formExporterUploader.dlAppLocalService = mockDlAppLocalService;
	}
}
