package com.placecube.digitalplace.local.ddmform.exporter.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;
import com.placecube.digitalplace.local.ddmform.exporter.constants.PlaceholderField;

@RunWith(PowerMockRunner.class)
public class PlaceholderFieldServiceTest extends PowerMockito {

	@InjectMocks
	private PlaceholderFieldService placeholderFieldService;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		placeholderFieldService = new PlaceholderFieldService();
	}

	@Test
	public void getPlaceholderFieldValue_WhenPlaceholdeFieldIsFormInstanceRecordId_ThenReturnFormInstanceRecordIdFromServiceContext() throws PortalException {

		PlaceholderField placeholderField = PlaceholderField.FORM_INSTANCE_RECORD_ID;
		long formInstanceRecordId = 123l;
		when(mockServiceContext.getAttribute(FormExporterSchedulerKeys.FORM_INSTANCE_RECORD_ID)).thenReturn(formInstanceRecordId);

		String result = placeholderFieldService.getPlaceholderFieldValue(placeholderField, mockServiceContext);

		assertEquals(String.valueOf(formInstanceRecordId), result);

	}

	@Test
	public void getPlaceholderFieldValue_WhenPlaceholdeFieldIsFormInstanceRecordIdAndIdIsNullInServiceContext_ThenReturnBlankString() throws PortalException {

		PlaceholderField placeholderField = PlaceholderField.FORM_INSTANCE_RECORD_ID;
		when(mockServiceContext.getAttribute(FormExporterSchedulerKeys.FORM_INSTANCE_RECORD_ID)).thenReturn(null);

		String result = placeholderFieldService.getPlaceholderFieldValue(placeholderField, mockServiceContext);

		assertEquals(StringPool.BLANK, result);

	}
}
