package com.placecube.digitalplace.local.ddmform.exporter.util;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMStructureVersionLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class FormExporterValidatorHelperTest extends PowerMockito {

    @Mock
    private List<DDMStructureVersion> mockDdmStructureVersionsList;

    @InjectMocks
    private FormExporterValidatorHelper formExporterValidatorHelper;

    @Mock
    private JSONArray mockJsonArray;

    @Mock
    private DDMFormInstance mockDdmFormInstance;

    @Mock
    private DDMStructureVersion mockDdmStructureVersion;

    @Mock
    private Set<String> mockDdmStructureVersionListFieldReferences;

    @Mock
    private DDMStructureVersionLocalService mockDdmStructureVersionLocalService;

    @Mock
    private JSONFactory mockJsonFactory;

    @Mock
    private JSONObject mockStructureVersionDefinitionJson;

    @Mock
    private JSONObject mockFormFieldJson;

    @Before
    public void setUp() {
        formExporterValidatorHelper = Mockito.spy(FormExporterValidatorHelper.class);
        formExporterValidatorHelper.ddmStructureVersionLocalService = mockDdmStructureVersionLocalService;
        formExporterValidatorHelper.jsonFactory = mockJsonFactory;
    }

    @Test
    @Parameters({ "true", "false" })
    public void validateJsonFormFields_WhenValidOrInvalidFieldReferences_ThenReturnTrueOrFalse(boolean multiValued) throws PortalException {
        List<DDMStructureVersion> ddmStructureVersionsList = new ArrayList<>();
        ddmStructureVersionsList.add(mockDdmStructureVersion);

        String fieldReferencesString = "text,date";
        String alternativeFieldReferencesString = "time";

        when(mockDdmStructureVersionLocalService.getStructureVersions(mockDdmFormInstance.getStructureId())).thenReturn(ddmStructureVersionsList);

        doReturn(mockDdmStructureVersionListFieldReferences).when(formExporterValidatorHelper).getFieldReferencesFromDdmStructureVersionList(ddmStructureVersionsList);

        when(mockJsonArray.length()).thenReturn(1);

        when(mockJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);

        when(mockFormFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK)).thenReturn(fieldReferencesString);
        when(mockFormFieldJson.getString(FormExporterSchedulerKeys.ALTERNATIVE_FIELD_REFERENCES, StringPool.BLANK)).thenReturn(alternativeFieldReferencesString);

        doReturn(multiValued).when(formExporterValidatorHelper).validateFieldReferencesString(mockDdmStructureVersionListFieldReferences, fieldReferencesString, alternativeFieldReferencesString);

        boolean result = formExporterValidatorHelper.validateJsonFormFields(mockDdmFormInstance, mockJsonArray);

        assertThat(result, equalTo(multiValued));
    }

    @Test
    public void validateJsonFormFields_WhenValidOrInvalidFieldReferences_ThenReturnTrueOrFalsed() throws PortalException {
        List<DDMStructureVersion> ddmStructureVersionsList = new ArrayList<>();
        ddmStructureVersionsList.add(mockDdmStructureVersion);

        String fieldReferencesString = StringPool.BLANK;
        String alternativeFieldReferencesString = "time";

        doReturn(mockDdmStructureVersionListFieldReferences).when(formExporterValidatorHelper).getFieldReferencesFromDdmStructureVersionList(ddmStructureVersionsList);

        when(mockDdmStructureVersionLocalService.getStructureVersions(mockDdmFormInstance.getStructureId())).thenReturn(ddmStructureVersionsList);

        when(mockJsonArray.length()).thenReturn(1);

        when(mockJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);

        when(mockFormFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK)).thenReturn(fieldReferencesString);
        when(mockFormFieldJson.getString(FormExporterSchedulerKeys.ALTERNATIVE_FIELD_REFERENCES, StringPool.BLANK)).thenReturn(alternativeFieldReferencesString);

        boolean result = formExporterValidatorHelper.validateJsonFormFields(mockDdmFormInstance, mockJsonArray);

        assertFalse(result);
    }

    @Test
    @Parameters({ "true", "false" })
    public void validateFieldReferencesString_WhenFieldReferencesAndAlternativesAreBothValidOrInvalid_ThenReturnTrueOrFalse(boolean multiValued) throws Exception {
        String fieldReferences = "text,date";
        String alternativeFieldReferencesString = "time";

        doReturn(mockDdmStructureVersionListFieldReferences).when(formExporterValidatorHelper).getFieldReferencesFromDdmStructureVersionList(mockDdmStructureVersionsList);

        doReturn(multiValued).when(formExporterValidatorHelper).validateFieldReferences(mockDdmStructureVersionListFieldReferences, fieldReferences.split(StringPool.COMMA));
        doReturn(multiValued).when(formExporterValidatorHelper).validateFieldReferences(mockDdmStructureVersionListFieldReferences, alternativeFieldReferencesString.split(StringPool.COMMA));

        boolean result = formExporterValidatorHelper.validateFieldReferencesString(mockDdmStructureVersionListFieldReferences, fieldReferences, alternativeFieldReferencesString);

        assertThat(result, equalTo(multiValued));
    }

    @Test
    @Parameters({ "true", "false" })
    public void validateFieldReferencesString_WhenFieldReferencesIsValidOrInvalidAndAlternativeIsValid_ThenReturnTrueOrFalse(boolean multiValued){
        String fieldReferences = "text,date";
        String alternativeFieldReferencesString = "time";


        doReturn(multiValued).when(formExporterValidatorHelper).validateFieldReferences(mockDdmStructureVersionListFieldReferences, fieldReferences.split(StringPool.COMMA));
        doReturn(true).when(formExporterValidatorHelper).validateFieldReferences(mockDdmStructureVersionListFieldReferences, alternativeFieldReferencesString.split(StringPool.COMMA));

        boolean result = formExporterValidatorHelper.validateFieldReferencesString(mockDdmStructureVersionListFieldReferences, fieldReferences, alternativeFieldReferencesString);

        assertThat(result, equalTo(multiValued));
    }

    @Test
    @Parameters({ "true", "false" })
    public void validateFieldReferencesString_WhenFieldReferencesIsValidOrInvalidAndAlternativeIsBlank_ThenReturnTrue(boolean multiValued) throws Exception {
        String fieldReferences = "text,date";
        String alternativeFieldReferencesString = StringPool.BLANK;

        doReturn(mockDdmStructureVersionListFieldReferences).when(formExporterValidatorHelper).getFieldReferencesFromDdmStructureVersionList(mockDdmStructureVersionsList);

        doReturn(multiValued).when(formExporterValidatorHelper).validateFieldReferences(mockDdmStructureVersionListFieldReferences, fieldReferences.split(StringPool.COMMA));

        boolean result = formExporterValidatorHelper.validateFieldReferencesString(mockDdmStructureVersionListFieldReferences, fieldReferences, alternativeFieldReferencesString);

        assertThat(result, equalTo(multiValued));
    }

    @Test
    public void validateFieldReferences_SetContainsBoth_ThenReturnTrue(){
        String[] fieldReferences = {"text","date"};

        when(mockDdmStructureVersionListFieldReferences.contains("text")).thenReturn(true);
        when(mockDdmStructureVersionListFieldReferences.contains("date")).thenReturn(true);

        boolean result = formExporterValidatorHelper.validateFieldReferences(mockDdmStructureVersionListFieldReferences, fieldReferences);

        assertTrue(result);
    }

    @Test
    public void validateFieldReferences_SetDoesNotContainsAtLeastOne_ThenReturnTrue(){
        String[] fieldReferences = {"text","date"};

        when(mockDdmStructureVersionListFieldReferences.contains("text")).thenReturn(true);
        when(mockDdmStructureVersionListFieldReferences.contains("date")).thenReturn(false);

        boolean result = formExporterValidatorHelper.validateFieldReferences(mockDdmStructureVersionListFieldReferences, fieldReferences);

        assertFalse(result);
    }

    @Test
    public void getFieldReferencesFromDdmStructureVersionList_WhenTwoVersionsHasTheSameFieldReference_ThenReturnList() throws JSONException {
        List<DDMStructureVersion> ddmStructureVersionsList =  new ArrayList<>();
        ddmStructureVersionsList.add(mockDdmStructureVersion);

        String structureVersionDefinition = "structureVersionDefinition";

        when(mockDdmStructureVersion.getDefinition()).thenReturn(structureVersionDefinition);
        when(mockJsonFactory.createJSONObject(structureVersionDefinition)).thenReturn(mockStructureVersionDefinitionJson);

        when(mockStructureVersionDefinitionJson.getJSONArray("fields")).thenReturn(mockJsonArray);

        when(mockJsonArray.length()).thenReturn(1);

        when(mockJsonArray.getJSONObject(0)).thenReturn(mockFormFieldJson);
        when(mockFormFieldJson.get("fieldReference")).thenReturn("text", "text");

        Set<String> result = formExporterValidatorHelper.getFieldReferencesFromDdmStructureVersionList(ddmStructureVersionsList);

        assertEquals(1, result.size());
    }
}
