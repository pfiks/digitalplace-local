package com.placecube.digitalplace.local.ddmform.exporter.scheduler;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.placecube.digitalplace.local.ddmform.exporter.util.FormExporterProcessor;
import com.placecube.digitalplace.local.ddmform.exporter.util.FormExporterUploader;
import com.placecube.digitalplace.local.ddmform.exporter.util.FormExporterValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.text.DateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DateFormatFactoryUtil.class, DateUtil.class})
public class FormExporterSchedulerServiceTest  extends PowerMockito {

    @Mock
    private DateFormat mockDateFormat;

    @Mock
    private DateFormat mockDateFormatDefault;

    @Mock
    private DDMFormInstance mockDdmFormInstance1;

    @Mock
    private DDMFormInstance mockDdmFormInstance2;

    @Mock
    private DDMFormInstanceLocalService mockDdmFormInstanceLocalService;

    @Mock
    private List<DDMFormInstanceRecord> mockDdmFormInstanceRecordList;

    @Mock
    private JSONArray mockFormsConfigurationJsonArray;

    @Mock
    private JSONArray mockFormExporterConfigurationJsonArray;

    @Mock
    private JSONArray mockFormFieldsJsonArray;

    @Mock
    private JSONObject mockFormConfigurationJson;

    @Mock
    private JSONObject mockFormExporterConfigurationJson;

    @Mock
    private FormExporterProcessor mockFormExporterProcessor;

    @InjectMocks
    private FormExporterSchedulerService formExporterSchedulerService;

    @Mock
    private FormExporterUploader mockFormExporterUploader;

    @Mock
    private FormExporterValidator mockFormExporterValidator;

    @Mock
    private Group mockGroup;

    @Mock
    private JSONFactory mockJsonFactory;

    @Mock
    private GroupLocalService mockGroupLocalService;

    @Mock
    private Role mockRole;

    @Mock
    private RoleLocalService mockRoleLocalService;

    @Mock
    private User mockUser;

    @Mock
    private UserLocalService mockUserLocalService;

    @Mock
    private ServiceContext mockServiceContext;

    @Mock
    private ServiceContextHelper mockServiceContextHelper;

    @Mock
    private JSONArray mockValidFormsConfigurationJsonArray;

    @Before
    public void setUp() {
        initMocks(this);
        mockStatic(DateFormatFactoryUtil.class, DateUtil.class);
        formExporterSchedulerService = Mockito.spy(FormExporterSchedulerService.class);
        formExporterSchedulerService.groupLocalService = mockGroupLocalService;
        formExporterSchedulerService.userLocalService = mockUserLocalService;
        formExporterSchedulerService.roleLocalService = mockRoleLocalService;
        formExporterSchedulerService.serviceContextHelper = mockServiceContextHelper;
        formExporterSchedulerService.formExporterUploader = mockFormExporterUploader;
        formExporterSchedulerService.ddmFormInstanceLocalService = mockDdmFormInstanceLocalService;
        formExporterSchedulerService.formExporterProcessor = mockFormExporterProcessor;
        formExporterSchedulerService.jsonFactory = mockJsonFactory;
        formExporterSchedulerService.formExporterValidator = mockFormExporterValidator;
    }

    @Test
    public void getValidFormExporterConfigurationJsonArray_WhenFormExporterConfigurationJsonIsValid_ThenReturnJsonArray() throws PortalException {

        when(mockJsonFactory.createJSONArray()).thenReturn(mockValidFormsConfigurationJsonArray);

        when(mockFormExporterConfigurationJsonArray.length()).thenReturn(1);

        when(mockFormExporterConfigurationJsonArray.getJSONObject(0)).thenReturn(mockFormExporterConfigurationJson);

        when(mockFormExporterValidator.validateFormExporterConfigurationJson(mockFormExporterConfigurationJson)).thenReturn(true);

        formExporterSchedulerService.getValidFormExporterConfigurationJsonArray(mockFormExporterConfigurationJsonArray);

        verify(mockValidFormsConfigurationJsonArray, times(1)).put(mockFormExporterConfigurationJson);
    }

    @Test
    public void getValidFormExporterConfigurationJsonArray_WhenFormExporterConfigurationJsonIsNotValid_ThenReturnEmptyJsonArray() throws PortalException {

        when(mockJsonFactory.createJSONArray()).thenReturn(mockValidFormsConfigurationJsonArray);

        when(mockFormExporterConfigurationJsonArray.length()).thenReturn(1);

        when(mockFormExporterConfigurationJsonArray.getJSONObject(0)).thenReturn(mockFormExporterConfigurationJson);

        when(mockFormExporterValidator.validateFormExporterConfigurationJson(mockFormExporterConfigurationJson)).thenReturn(false);

        JSONArray result = formExporterSchedulerService.getValidFormExporterConfigurationJsonArray(mockFormExporterConfigurationJsonArray);

        assertEquals(0, result.length());
    }

    @Test
    public void getDataToExport_WhenFormsConfigurationJsonArrayHasOneEntry_ThenReturnData() throws PortalException {

        boolean createDateEnabled = false;

        long formInstanceId = 12L;

        String header = "header";

        when(mockFormsConfigurationJsonArray.length()).thenReturn(1);
        when(mockFormsConfigurationJsonArray.getJSONObject(0)).thenReturn(mockFormConfigurationJson);

        when(mockFormConfigurationJson.getLong("formInstanceId")).thenReturn(formInstanceId);

        when(mockDdmFormInstanceLocalService.getDDMFormInstance(formInstanceId)).thenReturn(mockDdmFormInstance1);

        when(mockFormConfigurationJson.getJSONArray("formFields")).thenReturn(mockFormFieldsJsonArray);

        when(mockDdmFormInstance1.getFormInstanceRecords()).thenReturn(mockDdmFormInstanceRecordList);

        when(mockFormExporterProcessor.createDataHeaderCsv(mockFormFieldsJsonArray, createDateEnabled)).thenReturn(header);

        Map<DDMFormInstance, String> result = formExporterSchedulerService.getDataToExport(mockFormsConfigurationJsonArray, createDateEnabled, mockDateFormat);

        assertEquals(1, result.size());
        assertEquals("header", result.get(mockDdmFormInstance1));
    }

    @Test
    public void uploadData_WhenDataContainsTwoEntries() throws PortalException {

        String csv1 = "csv1";
        String csv2 = "csv2";

        long companyId = 12L;
        long groupId = 34L;
        long userId = 56L;
        long folderId1 = 78L;
        long folderId2 = 90L;

        Map<DDMFormInstance, String> data = new LinkedHashMap<>();

        data.put(mockDdmFormInstance1, csv1);
        data.put(mockDdmFormInstance2, csv2);

        when(mockDdmFormInstance1.getCompanyId()).thenReturn(companyId);
        when(mockDdmFormInstance2.getCompanyId()).thenReturn(companyId);

        when(mockServiceContextHelper.getServiceContext(companyId)).thenReturn(mockServiceContext);

        when(mockDdmFormInstance1.getGroupId()).thenReturn(groupId);
        when(mockDdmFormInstance2.getGroupId()).thenReturn(groupId);

        doReturn(userId).when(formExporterSchedulerService).getUser(groupId);

        when(mockFormExporterUploader.getOrCreateFolder(mockDdmFormInstance1, mockServiceContext)).thenReturn(folderId1);
        when(mockFormExporterUploader.getOrCreateFolder(mockDdmFormInstance2, mockServiceContext)).thenReturn(folderId2);

        formExporterSchedulerService.uploadData(data);

        verify(mockServiceContext, times(2)).setScopeGroupId(groupId);
        verify(mockServiceContext, times(2)).setUserId(userId);
        verify(mockFormExporterUploader, times(1)).createAndUploadFile(data.get(mockDdmFormInstance1), mockDdmFormInstance1, folderId1, mockServiceContext);
        verify(mockFormExporterUploader, times(1)).createAndUploadFile(data.get(mockDdmFormInstance2), mockDdmFormInstance2, folderId2, mockServiceContext);
    }

    @Test
    public void getUser_WhenGroupCreatorUserIsNotNull_ThenReturnGroupCreatorUser() throws PortalException {

        long groupId = 12L;
        long creatorUserId = 34L;

        when(mockGroupLocalService.getGroup(groupId)).thenReturn(mockGroup);
        when(mockGroup.getCreatorUserId()).thenReturn(creatorUserId);

        when(mockUserLocalService.fetchUserById(creatorUserId)).thenReturn(mockUser);

        long result = formExporterSchedulerService.getUser(groupId);

        assertEquals(creatorUserId, result);
    }

    @Test
    public void getUser_WhenUserGroupCreatorIsNotNull_ThenReturnAdminUser() throws PortalException {

        long groupId = 12L;
        long creatorUserId = 34L;
        long companyId = 56L;
        long roleId = 78L;
        long[] userIds = {123L, 346L};

        when(mockGroupLocalService.getGroup(groupId)).thenReturn(mockGroup);
        when(mockGroup.getCreatorUserId()).thenReturn(creatorUserId);

        when(mockUserLocalService.fetchUserById(creatorUserId)).thenReturn(null);

        when(mockGroup.getCompanyId()).thenReturn(companyId);

        when(mockRoleLocalService.getRole(companyId, RoleConstants.ADMINISTRATOR)).thenReturn(mockRole);

        when(mockRole.getRoleId()).thenReturn(roleId);

        when(mockUserLocalService.getRoleUserIds(roleId)).thenReturn(userIds);

        long result = formExporterSchedulerService.getUser(groupId);

        assertEquals(userIds[0], result);
    }

    @Test
    public void validateDateFormatPatterns_WhenCreateDateDisabled_ThenReturnDefaultDateFormat(){
        String dateFormatPattern = "yyyyMMdd";

        boolean createDateEnabled = false;

        when(DateUtil.getISOFormat("ISO_DATE_TIME")).thenReturn(mockDateFormatDefault);

        DateFormat dateFormat = formExporterSchedulerService.validateDateFormatPattern(createDateEnabled, dateFormatPattern);

        assertEquals(mockDateFormatDefault, dateFormat);
    }

    @Test
    public void validateDateFormatPatterns_WhenDateFormPatternIsBlank_ThenReturnDefaultDateFormat(){
        String dateFormatPattern = "";

        boolean createDateEnabled = true;

        when(DateUtil.getISOFormat("ISO_DATE_TIME")).thenReturn(mockDateFormatDefault);

        DateFormat dateFormat = formExporterSchedulerService.validateDateFormatPattern(createDateEnabled, dateFormatPattern);

        assertEquals(mockDateFormatDefault, dateFormat);
    }

    @Test
    public void validateDateFormatPattern_When_DateFormatPattersIsNotValid_ThenReturnDefaultDateFormat(){
        String dateFormatPattern = "Wrong_Format";

        boolean createDateEnabled = true;

        when(DateUtil.getISOFormat("ISO_DATE_TIME")).thenReturn(mockDateFormatDefault);

        when(DateFormatFactoryUtil.getSimpleDateFormat(dateFormatPattern)).thenThrow(new IllegalArgumentException());

        DateFormat dateFormat = formExporterSchedulerService.validateDateFormatPattern(createDateEnabled, dateFormatPattern);

        assertEquals(mockDateFormatDefault, dateFormat);
    }

    @Test
    public void validateDateFormatPattern_WhenDataFormPatternIsSetCorrectly_ThenReturnDateFormat(){
        String dateFormatPattern = "yyyyMMdd";

        boolean createDateEnabled = true;

        when(DateUtil.getISOFormat("ISO_DATE_TIME")).thenReturn(mockDateFormatDefault);

        when(DateFormatFactoryUtil.getSimpleDateFormat(dateFormatPattern)).thenReturn(mockDateFormat);

        DateFormat dateFormat = formExporterSchedulerService.validateDateFormatPattern(createDateEnabled, dateFormatPattern);

        assertEquals(mockDateFormat, dateFormat);
    }
}
