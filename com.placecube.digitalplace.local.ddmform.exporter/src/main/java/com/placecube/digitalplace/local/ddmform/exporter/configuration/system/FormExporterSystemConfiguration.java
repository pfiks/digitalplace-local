package com.placecube.digitalplace.local.ddmform.exporter.configuration.system;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

@ExtendedObjectClassDefinition(category="forms", scope = ExtendedObjectClassDefinition.Scope.SYSTEM)
@Meta.OCD(id = "com.placecube.digitalplace.local.ddmform.exporter.configuration.system.FormExporterSystemConfiguration", localization ="content/Language", name = "ddmform-exporter-configuration-name")
public interface FormExporterSystemConfiguration {

    @Meta.AD(required = false, name = "ddmform-exporter-schedule-name", description = "ddmform-exporter-schedule-description", deflt = "0 0 12 ? 1 MON#1 *")
    String schedulerCronExpression();
}
