package com.placecube.digitalplace.local.ddmform.exporter.constants;

import java.util.HashMap;
import java.util.Map;

public enum PlaceholderField {

	FORM_INSTANCE_RECORD_ID("formInstanceRecordId");

	private String fieldName;

	private static final Map<String, PlaceholderField> PLACE_HOLDER_FIELDS = new HashMap<>();

	static {
		for (PlaceholderField placeHolderField : PlaceholderField.values()) {
			PLACE_HOLDER_FIELDS.put(placeHolderField.getFieldName(), placeHolderField);
		}
	}

	PlaceholderField(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldName() {
		return fieldName;
	}
	
	public static PlaceholderField getPlaceholderField(String fieldName) {
		return PLACE_HOLDER_FIELDS.get(fieldName);
	}

	public static boolean isPlaceholderField(String fieldName) {
		return PLACE_HOLDER_FIELDS.containsKey(fieldName);
	}
}
