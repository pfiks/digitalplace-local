package com.placecube.digitalplace.local.ddmform.exporter.service;

import java.io.Serializable;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.ddmform.exporter.constants.PlaceholderField;

@Component(immediate = true, service = PlaceholderFieldService.class)
public class PlaceholderFieldService {

	public String getPlaceholderFieldValue(PlaceholderField placeholderField, ServiceContext serviceContext) {
		Serializable serviceContextAttributeValue = serviceContext.getAttribute(placeholderField.getFieldName());
		return Validator.isNotNull(serviceContextAttributeValue) ? serviceContextAttributeValue.toString() : StringPool.BLANK;

	}

}
