package com.placecube.digitalplace.local.ddmform.exporter.util;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.model.DLVersionNumberIncrease;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;

@Component(immediate = true, service = FormExporterUploader.class)
public class FormExporterUploader {

    @Reference
    DLAppLocalService dlAppLocalService;

    public long getOrCreateFolder(DDMFormInstance ddmFormInstance, ServiceContext serviceContext) throws PortalException {

        String path = FormExporterSchedulerKeys.GENERIC_PATH.concat(ddmFormInstance.getName(serviceContext.getLocale()).trim());

        return getOrCreateFoldersPath(path, serviceContext);
    }

    public void createAndUploadFile(String csv, DDMFormInstance ddmFormInstance, long folderId, ServiceContext serviceContext) throws PortalException {

        String fileName = String.valueOf(ddmFormInstance.getFormInstanceId());

        Locale locale = serviceContext.getLocale();
        fileName = fileName.concat(DateUtil.getCurrentDate("_ddMMyyyy_HHmmss", locale));

        fileName = fileName.concat(".csv");

        String mimeType = MimeTypesUtil.getContentType(fileName);
        String description = StringPool.BLANK;

        FileEntry fileEntry = dlAppLocalService.fetchFileEntryByExternalReferenceCode(serviceContext.getScopeGroupId(), fileName);

        if (Validator.isNull(fileEntry)) {
            dlAppLocalService.addFileEntry(fileName, serviceContext.getUserId(), serviceContext.getScopeGroupId(), folderId, fileName, mimeType, csv.getBytes(), null,null,null, serviceContext);
        }else {
            dlAppLocalService.updateFileEntry(serviceContext.getUserId(), fileEntry.getFileEntryId(), fileName, mimeType, fileName, fileName, description, null, DLVersionNumberIncrease.MINOR,csv.getBytes(),null,null,null,serviceContext);
        }
    }

    public long getOrCreateFoldersPath(String path, ServiceContext serviceContext) throws PortalException {
        long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }

        String[] folders = path.split("/");
        Folder folder;

        for (String folderName : folders) {
            try {
                folder = dlAppLocalService.getFolder(serviceContext.getScopeGroupId(), parentFolderId, folderName);
            } catch (PortalException e) {
                folder = dlAppLocalService.addFolder(null, serviceContext.getUserId(), serviceContext.getScopeGroupId(), parentFolderId, folderName, folderName, serviceContext);
            }
            parentFolderId = folder.getFolderId();
        }
        return parentFolderId;
    }
}
