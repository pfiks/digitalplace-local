package com.placecube.digitalplace.local.ddmform.exporter.configuration.instance;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

@ExtendedObjectClassDefinition(category="forms", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.ddmform.exporter.configuration.instance.FormExporterInstanceConfiguration", localization ="content/Language", name = "ddmform-exporter-configuration-name", description = "ddmform-exporter-configuration-description")
public interface FormExporterInstanceConfiguration {

    @Meta.AD(type = Meta.Type.Boolean, required = false, deflt = "false", name = "ddmform-exporter-activation-name")
    boolean enabled();

    @Meta.AD(required = false, name = "ddmform-exporter-json-configuration-name", description = "ddmform-exporter-json-configuration-description")
    String formExporterJsonConfiguration();

    @Meta.AD(type = Meta.Type.Boolean, required = false, name = "ddmform-exporter-create-date-activation", description = "ddmform-exporter-create-date-description")
    boolean dateOfEachRecordEnabled();

    @Meta.AD(required = false, name = "ddmform-exporter-date-format-pattern-name", description = "ddmform-exporter-date-format-pattern-description")
    String dateFormatPattern();
}