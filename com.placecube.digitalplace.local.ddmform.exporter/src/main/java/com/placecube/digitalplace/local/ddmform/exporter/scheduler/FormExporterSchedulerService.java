package com.placecube.digitalplace.local.ddmform.exporter.scheduler;

import java.text.DateFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.placecube.digitalplace.local.ddmform.exporter.util.FormExporterProcessor;
import com.placecube.digitalplace.local.ddmform.exporter.util.FormExporterUploader;
import com.placecube.digitalplace.local.ddmform.exporter.util.FormExporterValidator;

@Component(immediate = true, service = FormExporterSchedulerService.class)
public class FormExporterSchedulerService {

	private static final Log LOG = LogFactoryUtil.getLog(FormExporterSchedulerService.class);

	@Reference
	DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	FormExporterProcessor formExporterProcessor;

	@Reference
	FormExporterUploader formExporterUploader;

	@Reference
	FormExporterValidator formExporterValidator;

	@Reference
	GroupLocalService groupLocalService;

	@Reference
	JSONFactory jsonFactory;

	@Reference
	RoleLocalService roleLocalService;

	@Reference
	ServiceContextHelper serviceContextHelper;

	@Reference
	UserLocalService userLocalService;

	public JSONArray getValidFormExporterConfigurationJsonArray(JSONArray formExporterConfigurationJsonArray) throws PortalException {
		JSONArray validFormsConfigurationJsonArray = jsonFactory.createJSONArray();
		for (int i = 0; i < formExporterConfigurationJsonArray.length(); i++) {
			JSONObject formExporterConfigurationJson = formExporterConfigurationJsonArray.getJSONObject(i);

			if (formExporterValidator.validateFormExporterConfigurationJson(formExporterConfigurationJson)) {
				validFormsConfigurationJsonArray.put(formExporterConfigurationJson);
			} else {
				LOG.error("Configuration is not valid for formInstanceId: " + formExporterConfigurationJson.getLong("formInstanceId"));
			}
		}
		return validFormsConfigurationJsonArray;
	}

	public Map<DDMFormInstance, String> getDataToExport(JSONArray formsConfigurationJsonArray, boolean createDateEnabled, DateFormat dateFormat) {

		Map<DDMFormInstance, String> data = new LinkedHashMap<>();

		for (int i = 0; i < formsConfigurationJsonArray.length(); i++) {
			JSONObject formConfigurationJson = formsConfigurationJsonArray.getJSONObject(i);
			try {

				long formInstanceId = formConfigurationJson.getLong("formInstanceId");

				DDMFormInstance ddmFormInstance = ddmFormInstanceLocalService.getDDMFormInstance(formInstanceId);

				JSONArray formFieldsJsonArray = formConfigurationJson.getJSONArray("formFields");

				StringBuilder csvString = new StringBuilder();

				csvString.append(formExporterProcessor.createDataHeaderCsv(formFieldsJsonArray, createDateEnabled));

				formExporterProcessor.addFormValues(csvString, ddmFormInstance, formFieldsJsonArray, createDateEnabled, dateFormat);

				data.put(ddmFormInstance, csvString.toString());
			} catch (Exception e) {
				LOG.error("Unable to process configuration: " + formConfigurationJson.toJSONString(), e);
			}
		}
		return data;
	}

	public void uploadData(Map<DDMFormInstance, String> data) throws PortalException {

		Set<DDMFormInstance> ddmFormInstances = data.keySet();

		for (DDMFormInstance ddmFormInstance : ddmFormInstances) {
			try {

				ServiceContext serviceContext = serviceContextHelper.getServiceContext(ddmFormInstance.getCompanyId());
				serviceContext.setAddGroupPermissions(false);
				serviceContext.setAddGuestPermissions(false);
				serviceContext.setDeriveDefaultPermissions(false);
				serviceContext.setModelPermissions(null);

				long groupId = ddmFormInstance.getGroupId();
				serviceContext.setScopeGroupId(groupId);

				long userId = getUser(groupId);
				serviceContext.setUserId(userId);

				long folderId = formExporterUploader.getOrCreateFolder(ddmFormInstance, serviceContext);

				formExporterUploader.createAndUploadFile(data.get(ddmFormInstance), ddmFormInstance, folderId, serviceContext);
			} catch (Exception e) {
				LOG.error("Unable to save exported data for ddmFormInstanceId: " + ddmFormInstance.getFormInstanceId(), e);
			}
		}
	}

	public long getUser(long groupId) throws PortalException {
		Group group = groupLocalService.getGroup(groupId);
		long userId = group.getCreatorUserId();
		User user = userLocalService.fetchUserById(userId);

		if (Validator.isNull(user)) {
			long companyId = group.getCompanyId();
			Role role = roleLocalService.getRole(companyId, RoleConstants.ADMINISTRATOR);
			long[] userIds = userLocalService.getRoleUserIds(role.getRoleId());

			userId = userIds[0];
		}
		return userId;
	}

	public DateFormat validateDateFormatPattern(boolean createDateEnabled, String dateFormatPattern) {
		DateFormat dateFormat = DateUtil.getISOFormat("ISO_DATE_TIME");

		if (createDateEnabled && Validator.isNotNull(dateFormatPattern)) {
			try {
				dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(dateFormatPattern);
			} catch (IllegalArgumentException e) {
				LOG.error("Date format pattern is not valid");
			}
		}
		return dateFormat;
	}
}
