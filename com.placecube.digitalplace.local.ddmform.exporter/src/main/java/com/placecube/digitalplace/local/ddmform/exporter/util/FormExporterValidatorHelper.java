package com.placecube.digitalplace.local.ddmform.exporter.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMStructureVersionLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;
import com.placecube.digitalplace.local.ddmform.exporter.constants.PlaceholderField;

@Component(immediate = true, service = FormExporterValidatorHelper.class)
public class FormExporterValidatorHelper {

	private static final Log LOG = LogFactoryUtil.getLog(FormExporterValidatorHelper.class);

	@Reference
	DDMStructureVersionLocalService ddmStructureVersionLocalService;

	@Reference
	JSONFactory jsonFactory;

	protected boolean validateJsonFormFields(DDMFormInstance ddmFormInstance, JSONArray formFieldsJsonArray) throws PortalException {
		List<DDMStructureVersion> ddmStructureVersionsList = ddmStructureVersionLocalService.getStructureVersions(ddmFormInstance.getStructureId());
		boolean valid = true;
		Set<String> ddmStructureVersionListFieldReferences = getFieldReferencesFromDdmStructureVersionList(ddmStructureVersionsList);

		for (int j = 0; j < formFieldsJsonArray.length() && valid; j++) {
			JSONObject formFieldJson = formFieldsJsonArray.getJSONObject(j);
			String fieldReferencesString = formFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK);
			String alternativeFieldReferencesString = formFieldJson.getString(FormExporterSchedulerKeys.ALTERNATIVE_FIELD_REFERENCES, StringPool.BLANK);
			if (Validator.isNotNull(fieldReferencesString)) {
				if (!validateFieldReferencesString(ddmStructureVersionListFieldReferences, fieldReferencesString, alternativeFieldReferencesString)) {
					valid = false;
				}
			} else {
				valid = false;
				LOG.error("\"fieldReferences\" for formInstanceId: " + ddmFormInstance.getFormInstanceId() + " is not defined.");
			}
		}
		return valid;
	}

	protected boolean validateFieldReferencesString(Set<String> ddmStructureVersionListFieldReferences, String fieldReferencesString, String alternativeFieldReferencesString) {
		boolean valid = true;
		String[] fieldReferences = fieldReferencesString.split(StringPool.COMMA);

		if (!validateFieldReferences(ddmStructureVersionListFieldReferences, fieldReferences)) {
			valid = false;
		}

		if (!StringPool.BLANK.equals(alternativeFieldReferencesString)) {
			String[] alternativeFieldReferences = alternativeFieldReferencesString.split(StringPool.COMMA);
			if (!validateFieldReferences(ddmStructureVersionListFieldReferences, alternativeFieldReferences)) {
				valid = false;
			}
		}

		return valid;
	}

	protected boolean validateFieldReferences(Set<String> ddmStructureVersionListFieldReferences, String[] fieldReferences) {
		boolean valid = true;
		for (String fieldReference : fieldReferences) {
			if (!ddmStructureVersionListFieldReferences.contains(fieldReference) && !PlaceholderField.isPlaceholderField(fieldReference)) {
				valid = false;
				if (LOG.isDebugEnabled()) {
					LOG.debug("\"fieldReference\": " + fieldReference + " does not exist.");
				}
			}
		}
		return valid;
	}

	protected Set<String> getFieldReferencesFromDdmStructureVersionList(List<DDMStructureVersion> ddmStructureVersionsList) throws JSONException {
		Set<String> ddmStructureVersionListFieldReferences = new HashSet<>();
		for (DDMStructureVersion ddmStructureVersion : ddmStructureVersionsList) {
			JSONObject jsonObject = jsonFactory.createJSONObject(ddmStructureVersion.getDefinition());
			JSONArray jsonArray = jsonObject.getJSONArray("fields");
			for (int i = 0; i < jsonArray.length(); i++) {
				String fieldReference = jsonArray.getJSONObject(i).getString("fieldReference");
				ddmStructureVersionListFieldReferences.add(fieldReference);
			}
		}
		return ddmStructureVersionListFieldReferences;
	}
}
