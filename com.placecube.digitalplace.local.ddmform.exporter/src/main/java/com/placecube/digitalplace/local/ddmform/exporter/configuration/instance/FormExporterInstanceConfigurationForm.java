package com.placecube.digitalplace.local.ddmform.exporter.configuration.instance;

import com.liferay.dynamic.data.mapping.annotations.DDMForm;
import com.liferay.dynamic.data.mapping.annotations.DDMFormField;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayout;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayoutColumn;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayoutPage;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayoutRow;
import com.liferay.dynamic.data.mapping.annotations.DDMFormRule;

@DDMForm(
        rules = {
                @DDMFormRule( //
                        actions = {
                                "setVisible('dateFormatPattern', getValue('dateOfEachRecordEnabled'))"
                        }
                ) //
        }
)
@DDMFormLayout(
        paginationMode = com.liferay.dynamic.data.mapping.model.DDMFormLayout.SINGLE_PAGE_MODE,
        value = {
                @DDMFormLayoutPage(
                        {
                                @DDMFormLayoutRow(
                                        {
                                                @DDMFormLayoutColumn(value = {
                                                        "enabled",
                                                        "formExporterJsonConfiguration",
                                                        "dateOfEachRecordEnabled",
                                                        "dateFormatPattern"}
                                                )
                                        }
                                )
                        }
                )
        }
)
public interface FormExporterInstanceConfigurationForm {

    @DDMFormField(label = "%ddmform-exporter-activation-name", properties = "showAsSwitcher=true")
     boolean enabled();

    @DDMFormField(label = "%ddmform-exporter-create-date-activation", tip = "%ddmform-exporter-create-date-description", properties = "showAsSwitcher=true")
     boolean dateOfEachRecordEnabled();

    @DDMFormField(label = "%ddmform-exporter-date-format-pattern-name", tip = "%ddmform-exporter-date-format-pattern-description")
     String dateFormatPattern();
}
