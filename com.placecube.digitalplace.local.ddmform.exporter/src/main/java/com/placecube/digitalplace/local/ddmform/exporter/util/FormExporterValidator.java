package com.placecube.digitalplace.local.ddmform.exporter.util;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


@Component(immediate = true, service = FormExporterValidator.class)
public class FormExporterValidator {

    private static final Log LOG = LogFactoryUtil.getLog(FormExporterValidator.class);

    @Reference
    DDMFormInstanceLocalService ddmFormInstanceLocalService;

    @Reference
    FormExporterValidatorHelper formExporterValidatorHelper;

    public boolean validateFormExporterConfigurationJson(JSONObject formConfigurationJson) throws PortalException {
        boolean valid = false;
        long formInstanceId = formConfigurationJson.getLong("formInstanceId");
        if (Validator.isNotNull(formInstanceId)) {
            DDMFormInstance ddmFormInstance = ddmFormInstanceLocalService.fetchDDMFormInstance(formInstanceId);
            if (Validator.isNotNull(ddmFormInstance)) {
                JSONArray formFieldsJsonArray = formConfigurationJson.getJSONArray("formFields");
                if (Validator.isNotNull(formFieldsJsonArray)) {
                    valid = formExporterValidatorHelper.validateJsonFormFields(ddmFormInstance, formFieldsJsonArray);
                }
            } else {
                LOG.error("DDMFormInstance with formInstanceId = " + formInstanceId + " does not exist.");
            }
        } else {
            LOG.error("\"formInstanceId\" data is required");
        }
        return valid;
    }
}
