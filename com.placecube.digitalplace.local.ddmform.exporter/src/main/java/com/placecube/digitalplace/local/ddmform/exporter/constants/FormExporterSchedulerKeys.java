package com.placecube.digitalplace.local.ddmform.exporter.constants;

public final class FormExporterSchedulerKeys {

	public static final String ALTERNATIVE_FIELD_REFERENCES = "alternativeFieldReferences";

	public static final String DEFAULT_SEPARATOR = ",";

	public static final String EMPTY_BRACKETS = "[]";

	public static final String FIELD_REFERENCES = "fieldReferences";

	public static final String FORM_INSTANCE_RECORD_ID = "formInstanceRecordId";

	public static final String GENERIC_PATH = "/Forms/Export/";

	public static final String SEPARATOR = "separator";

	private FormExporterSchedulerKeys() {

	}
}
