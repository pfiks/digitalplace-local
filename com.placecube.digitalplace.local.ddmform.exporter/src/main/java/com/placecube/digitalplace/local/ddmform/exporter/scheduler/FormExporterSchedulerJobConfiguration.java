package com.placecube.digitalplace.local.ddmform.exporter.scheduler;

import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeRunnable;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import com.liferay.portal.kernel.scheduler.SchedulerJobConfiguration;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.ddmform.exporter.configuration.instance.FormExporterInstanceConfiguration;
import com.placecube.digitalplace.local.ddmform.exporter.configuration.system.FormExporterSystemConfiguration;

@Component(immediate = true, service = SchedulerJobConfiguration.class, configurationPid = "com.placecube.digitalplace.local.ddmform.exporter.configuration.system.FormExporterSystemConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL)
public class FormExporterSchedulerJobConfiguration implements SchedulerJobConfiguration {

	private static final Log LOG = LogFactoryUtil.getLog(FormExporterSchedulerJobConfiguration.class);

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private FormExporterSchedulerService formExporterSchedulerService;

	private FormExporterSystemConfiguration formExporterSystemConfiguration;

	@Reference
	private JSONFactory jsonFactory;

	@Activate
	protected void activate(Map<String, Object> properties) {
		formExporterSystemConfiguration = ConfigurableUtil.createConfigurable(FormExporterSystemConfiguration.class, properties);
	}

	@Override
	public UnsafeConsumer<Long, Exception> getCompanyJobExecutorUnsafeConsumer() {
		return this::runJobForCompany;
	}

	@Override
	public UnsafeRunnable<Exception> getJobExecutorUnsafeRunnable() {
		return () -> companyLocalService.forEachCompanyId(this::runJobForCompany);
	}

	@Override
	public TriggerConfiguration getTriggerConfiguration() {
		String cronExpression = formExporterSystemConfiguration.schedulerCronExpression();

		TriggerConfiguration triggerConfiguration = TriggerConfiguration.createTriggerConfiguration(cronExpression);
		triggerConfiguration.setStartDate(new Date());

		return triggerConfiguration;
	}

	private void runJobForCompany(long companyId) {
		try {
			FormExporterInstanceConfiguration formExporterInstanceConfiguration = configurationProvider.getCompanyConfiguration(FormExporterInstanceConfiguration.class, companyId);
			if (formExporterInstanceConfiguration.enabled()) {
				JSONArray formExporterConfigurationJsonArray = jsonFactory.createJSONArray(formExporterInstanceConfiguration.formExporterJsonConfiguration());

				boolean createDateEnabled = formExporterInstanceConfiguration.dateOfEachRecordEnabled();

				DateFormat dateFormat = formExporterSchedulerService.validateDateFormatPattern(createDateEnabled, formExporterInstanceConfiguration.dateFormatPattern());

				JSONArray validFormExporterConfigurationJsonArray = formExporterSchedulerService.getValidFormExporterConfigurationJsonArray(formExporterConfigurationJsonArray);

				if (Validator.isNotNull(validFormExporterConfigurationJsonArray) && validFormExporterConfigurationJsonArray.length() > 0) {
					Map<DDMFormInstance, String> data = formExporterSchedulerService.getDataToExport(validFormExporterConfigurationJsonArray, createDateEnabled, dateFormat);
					formExporterSchedulerService.uploadData(data);
				}
			} else {
				LOG.debug("FormExporterSchedulerJobConfiguration not enabled for companyId: " + companyId);
			}
		} catch (Exception e) {
			LOG.error("Unable to run FormExporterSchedulerJobConfiguration for companyId: " + companyId, e);
		}
	}

}