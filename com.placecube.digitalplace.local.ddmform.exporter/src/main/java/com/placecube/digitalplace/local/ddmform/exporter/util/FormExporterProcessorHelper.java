package com.placecube.digitalplace.local.ddmform.exporter.util;

import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;
import com.placecube.digitalplace.local.ddmform.exporter.constants.PlaceholderField;
import com.placecube.digitalplace.local.ddmform.exporter.service.PlaceholderFieldService;

@Component(immediate = true, service = FormExporterProcessorHelper.class)
public class FormExporterProcessorHelper {

	@Reference
	JSONFactory jsonFactory;

	@Reference
	PlaceholderFieldService placeHolderFieldService;

	protected void addValue(StringBuilder values, String value) {
		if (Validator.isNotNull(value)) {
			values.append(value);
		}
	}

	protected void addValue(StringBuilder values, DDMFormFieldValue ddmFormFieldValue, ServiceContext serviceContext) throws JSONException {

		if (Validator.isNotNull(ddmFormFieldValue)) {
			String fieldValue = getValue(ddmFormFieldValue, serviceContext);
			values.append(fieldValue);
		}
	}

	protected String checkboxFieldGetValue(DDMFormFieldValue ddmFormFieldValue, ServiceContext serviceContext, String valueString) throws JSONException {
		String fieldValue = StringPool.BLANK;
		DDMFormFieldOptions ddmFormFieldOptions = ddmFormFieldValue.getDDMFormField().getDDMFormFieldOptions();
		JSONArray multipleCheckboxValues = jsonFactory.createJSONArray(valueString);
		for (int i = 0; i < multipleCheckboxValues.length(); i++) {
			LocalizedValue optionLabels = ddmFormFieldOptions.getOptionLabels(multipleCheckboxValues.getString(i));
			if (Validator.isNotNull(optionLabels)) {
				fieldValue = fieldValue.concat(optionLabels.getString(serviceContext.getLocale()));
			} else {
				fieldValue = multipleCheckboxValues.getString(i);
			}
			if (i != multipleCheckboxValues.length() - 1) {
				fieldValue = fieldValue.concat("/");
			}
		}
		return fieldValue;
	}

	protected boolean checkEmptyValues(String values, ServiceContext serviceContext) {
		values = values.replace(StringPool.SPACE, StringPool.BLANK).replace((String) serviceContext.getAttribute(FormExporterSchedulerKeys.SEPARATOR), StringPool.BLANK).replace(StringPool.COMMA,
				StringPool.BLANK);

		return values.equals("\"\"");
	}

	protected String getFormValues(JSONObject formFieldJson, DDMFormInstanceRecord ddmFormInstanceRecord, ServiceContext serviceContext) throws PortalException {

		String separator = formFieldJson.getString(FormExporterSchedulerKeys.SEPARATOR, FormExporterSchedulerKeys.DEFAULT_SEPARATOR);
		serviceContext.setAttribute(FormExporterSchedulerKeys.SEPARATOR, separator);
		serviceContext.setAttribute(FormExporterSchedulerKeys.FORM_INSTANCE_RECORD_ID, ddmFormInstanceRecord.getFormInstanceRecordId());

		String fieldReferencesString = formFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK);
		String alternativeFieldReferencesString = formFieldJson.getString(FormExporterSchedulerKeys.ALTERNATIVE_FIELD_REFERENCES, StringPool.BLANK);

		String values = getValues(ddmFormInstanceRecord, fieldReferencesString, serviceContext);
		if (checkEmptyValues(values, serviceContext) && Validator.isNotNull(alternativeFieldReferencesString)) {
			values = getValues(ddmFormInstanceRecord, alternativeFieldReferencesString, serviceContext);
		}
		return values;
	}

	protected String getValue(DDMFormFieldValue ddmFormFieldValue, ServiceContext serviceContext) throws JSONException {

		String fieldValue = StringPool.BLANK;
		String valueString = ddmFormFieldValue.getValue().getString(serviceContext.getLocale());

		if (Validator.isNotNull(valueString) && !FormExporterSchedulerKeys.EMPTY_BRACKETS.equalsIgnoreCase(valueString)) {
			if (DDMFormFieldTypeConstants.SELECT.equalsIgnoreCase(ddmFormFieldValue.getType()) || DDMFormFieldTypeConstants.RADIO.equalsIgnoreCase(ddmFormFieldValue.getType())) {
				fieldValue = selectAndRadioFieldGetValue(ddmFormFieldValue, serviceContext, valueString);
			} else if (DDMFormFieldTypeConstants.CHECKBOX_MULTIPLE.equalsIgnoreCase(ddmFormFieldValue.getType())) {
				fieldValue = checkboxFieldGetValue(ddmFormFieldValue, serviceContext, valueString);
			} else {
				fieldValue = valueString;
			}
		}
		return fieldValue;
	}

	protected String getValues(DDMFormInstanceRecord ddmFormInstanceRecord, String fieldReferencesString, ServiceContext serviceContext) throws PortalException {
		StringBuilder values = new StringBuilder();
		String[] fieldReferences = fieldReferencesString.split(StringPool.COMMA);

		Map<String, List<DDMFormFieldValue>> referencesMap = ddmFormInstanceRecord.getDDMFormValues().getDDMFormFieldValuesReferencesMap(true);

		getValuesFromFieldReferences(values, fieldReferences, referencesMap, serviceContext);

		return values.toString();
	}

	protected void getValuesFromFieldReferences(StringBuilder values, String[] fieldReferences, Map<String, List<DDMFormFieldValue>> referencesMap, ServiceContext serviceContext)
			throws JSONException {

		for (int i = 0; i < fieldReferences.length; i++) {
			if (i > 0) {
				values.append(serviceContext.getAttribute(FormExporterSchedulerKeys.SEPARATOR));
			}

			if (PlaceholderField.isPlaceholderField(fieldReferences[i])) {

				PlaceholderField placeholderField = PlaceholderField.getPlaceholderField(fieldReferences[i]);

				String placeholderFieldValue = placeHolderFieldService.getPlaceholderFieldValue(placeholderField, serviceContext);
				addValue(values, placeholderFieldValue);
			} else {
				List<DDMFormFieldValue> ddmFormFieldValues = referencesMap.get(fieldReferences[i]);
				if (Validator.isNotNull(ddmFormFieldValues)) {
					for (DDMFormFieldValue ddmFormFieldValue : ddmFormFieldValues) {
						addValue(values, ddmFormFieldValue, serviceContext);
					}
				}
			}

		}
	}

	protected String selectAndRadioFieldGetValue(DDMFormFieldValue ddmFormFieldValue, ServiceContext serviceContext, String valueString) {
		if (Validator.isNull(valueString)) {
			return StringPool.BLANK;
		}
		DDMFormFieldOptions ddmFormFieldOptions = ddmFormFieldValue.getDDMFormField().getDDMFormFieldOptions();
		LocalizedValue optionLabels = ddmFormFieldOptions.getOptionLabels(valueString.replace("[\"", "").replace("\"]", ""));
		if (Validator.isNotNull(optionLabels)) {
			return optionLabels.getString(serviceContext.getLocale());
		}
		return valueString;
	}
}
