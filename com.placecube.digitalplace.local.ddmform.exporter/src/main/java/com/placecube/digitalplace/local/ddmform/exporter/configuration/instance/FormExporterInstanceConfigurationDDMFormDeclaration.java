package com.placecube.digitalplace.local.ddmform.exporter.configuration.instance;

import com.liferay.configuration.admin.definition.ConfigurationDDMFormDeclaration;
import org.osgi.service.component.annotations.Component;

@Component(
        immediate = true,
        property = "configurationPid=com.placecube.digitalplace.local.ddmform.exporter.configuration.instance.FormExporterInstanceConfiguration",
        service = ConfigurationDDMFormDeclaration.class
)
public class FormExporterInstanceConfigurationDDMFormDeclaration implements ConfigurationDDMFormDeclaration{

    @Override
    public Class<?> getDDMFormClass() {
        return FormExporterInstanceConfigurationForm.class;
    }
}
