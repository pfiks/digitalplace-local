package com.placecube.digitalplace.local.ddmform.exporter.util;

import java.text.DateFormat;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.CSVUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.placecube.digitalplace.local.ddmform.exporter.constants.FormExporterSchedulerKeys;

@Component(immediate = true, service = FormExporterProcessor.class)
public class FormExporterProcessor {

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private FormExporterProcessorHelper formExporterProcessorHelper;

	@Reference
	private ServiceContextHelper serviceContextHelper;

	public void addFormValues(StringBuilder csvString, DDMFormInstance ddmFormInstance, JSONArray formFieldsJsonArray, boolean createDateEnabled, DateFormat dateFormat) throws PortalException {

		List<DDMFormInstanceRecord> ddmFormInstanceRecordList = ddmFormInstanceRecordLocalService.getFormInstanceRecords(ddmFormInstance.getFormInstanceId(), WorkflowConstants.STATUS_APPROVED,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);

		for (DDMFormInstanceRecord ddmFormInstanceRecord : ddmFormInstanceRecordList) {
			ServiceContext serviceContext = serviceContextHelper.getServiceContext(ddmFormInstance.getCompanyId());

			for (int j = 0; j < formFieldsJsonArray.length(); j++) {
				if (j > 0) {
					csvString.append(StringPool.COMMA);
				}
				String values = formExporterProcessorHelper.getFormValues(formFieldsJsonArray.getJSONObject(j), ddmFormInstanceRecord, serviceContext);
				csvString.append(CSVUtil.encode(values));
			}

			if (createDateEnabled) {
				csvString.append(StringPool.COMMA).append(dateFormat.format(ddmFormInstanceRecord.getCreateDate()));
			}
			csvString.append(StringPool.NEW_LINE);
		}
	}

	public String createDataHeaderCsv(JSONArray formFieldsJsonArray, boolean createDateEnabled) {
		StringBuilder headers = new StringBuilder();

		for (int i = 0; i < formFieldsJsonArray.length(); i++) {
			JSONObject formFieldJson = formFieldsJsonArray.getJSONObject(i);
			String fieldReferencesString = formFieldJson.getString(FormExporterSchedulerKeys.FIELD_REFERENCES, StringPool.BLANK);

			if (i > 0) {
				headers.append(StringPool.COMMA);
			}

			String[] fieldReferences = fieldReferencesString.split(StringPool.COMMA);

			if (fieldReferences.length > 1) {
				headers.append(fieldReferencesString.replace(StringPool.COMMA, StringPool.UNDERLINE));
			} else {
				headers.append(fieldReferences[0]);
			}
		}

		if (createDateEnabled) {
			headers.append(StringPool.COMMA).append("Creation date");
		}

		headers.append(StringPool.NEW_LINE);
		return headers.toString();
	}
}
