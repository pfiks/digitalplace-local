package com.placecube.digitalplace.local.user.expando.addressinfostatus.field;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.configuration.AddressInfoStatusCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatus;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatusConstants;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.service.AddressInfoStatusService;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.service.LanguageMessageService;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;


@Component(immediate = true, service = UserAccountField.class)
public class AddressInfoStatusUserAccountField implements UserAccountField {

	@Reference
	private AddressInfoStatusService addressinfoStatusService;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private LanguageMessageService languageMessageService;

	@Reference(target = "(osgi.web.symbolicname=" + AddressInfoStatusConstants.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public void configureContextValue(AccountContext accountContext, PortletRequest portletRequest) {

		String value = ParamUtil.getString(portletRequest, getExpandoFieldName(), StringPool.BLANK);
		String fullAddress = accountContext.getFullAddress();

		if (AddressInfoStatus.NO_FIXED_ABODE.getKey().equals(value)) {
			accountContext.addExpandoField(getExpandoFieldName(), AddressInfoStatus.NO_FIXED_ABODE.getKey());
		} else if (Validator.isNull(fullAddress)) {
			accountContext.addExpandoField(getExpandoFieldName(), AddressInfoStatus.HAS_ADDRESS_NOT_PROVIDED.getKey());
		} else {
			accountContext.addExpandoField(getExpandoFieldName(), AddressInfoStatus.ADDRESS_PROVIDED.getKey());
		}
	}

	@Override
	public AccountCreationStep getAccountCreationStep() {
		return AccountCreationStep.ADDRESS_DETAILS;
	}

	@Override
	public String getBundleId() {
		return AddressInfoStatusConstants.BUNDLE_ID;
	}

	@Override
	public Map<String, String> getDependentRequiredFieldNamesAndMessage(String selectedValue, Locale locale) {
		return new HashMap<>();
	}

	@Override
	public int getDisplayOrder() {
		return 400;
	}

	@Override
	public String getExpandoFieldName() {
		return AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME;
	}

	@Override
	public String getRequiredMessage(Locale locale) {
		return StringPool.BLANK;
	}

	@Override
	public boolean isEnabled(long companyId) {
		try {
			AddressInfoStatusCompanyConfiguration configuration = addressinfoStatusService.getAddressInfoStatusCompanyConfiguration(companyId);
			return configuration.addressInfoStatusEnabled();
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean isRequired(long companyId) {
		return false;
	}

	@Override
	public void render(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws PortalException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Locale locale = themeDisplay.getLocale();

			httpServletRequest.setAttribute("addressInfoStatusFieldName", getExpandoFieldName());
			httpServletRequest.setAttribute("addressInfoStatusTitle", languageMessageService.getMessage(AddressInfoStatus.NO_FIXED_ABODE.getKey(), locale));
			httpServletRequest.setAttribute("addressInfoFieldOptions", addressinfoStatusService.getCheckBoxOptions(locale));

			jspRenderer.renderJSP(servletContext, httpServletRequest, httpServletResponse, "/view.jsp");
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}
}
