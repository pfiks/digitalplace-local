package com.placecube.digitalplace.local.user.expando.addressinfostatus.service;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.configuration.AddressInfoStatusCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatus;

@Component(immediate = true, service = AddressInfoStatusService.class)
public class AddressInfoStatusService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LanguageMessageService languageMessageService;

	public AddressInfoStatusCompanyConfiguration getAddressInfoStatusCompanyConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(AddressInfoStatusCompanyConfiguration.class, companyId);
	}

	public Map<String, String> getCheckBoxOptions(Locale locale) {
		Map<String, String> checkBoxOptions = new HashMap<>();

		checkBoxOptions.put(AddressInfoStatus.NO_FIXED_ABODE.getKey(), languageMessageService.getMessage(AddressInfoStatus.NO_FIXED_ABODE.getKey(), locale));

		return checkBoxOptions;
	}
}
