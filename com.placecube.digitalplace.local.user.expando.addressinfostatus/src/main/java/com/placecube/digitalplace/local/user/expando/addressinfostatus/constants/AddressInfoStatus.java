package com.placecube.digitalplace.local.user.expando.addressinfostatus.constants;

public enum AddressInfoStatus {

	NO_FIXED_ABODE("no-fixed-abode"), ADDRESS_PROVIDED("address-provided"), HAS_ADDRESS_NOT_PROVIDED("has-address-not-provided");

	private String key;

	private AddressInfoStatus(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

}
