package com.placecube.digitalplace.local.user.expando.addressinfostatus.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.user.expando.addressinfostatus.configuration.AddressInfoStatusCompanyConfiguration", localization = "content/Language", name = "address-info-status")
public interface AddressInfoStatusCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "address-info-status-enabled")
	boolean addressInfoStatusEnabled();

}
