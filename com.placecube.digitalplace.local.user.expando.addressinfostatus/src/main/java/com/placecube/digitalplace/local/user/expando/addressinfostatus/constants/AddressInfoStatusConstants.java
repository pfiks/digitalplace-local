package com.placecube.digitalplace.local.user.expando.addressinfostatus.constants;

public final class AddressInfoStatusConstants {

	public static final String ADDRESS_INFO_STATUS_FIELD_NAME = "address-info-status";

	public static final String BUNDLE_ID = "com.placecube.digitalplace.local.user.expando.addressinfostatus";

	private AddressInfoStatusConstants() {

	}
}
