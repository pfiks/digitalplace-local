package com.placecube.digitalplace.local.user.expando.addressinfostatus.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.upgrade.upgradestep.ResourcePermissionUpgrade;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class AddressInfoStatusUpgradeRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private CounterLocalService counterLocalService;

	@Reference
	private ExpandoColumnLocalService expandoColumnLocalService;

	@Reference
	private ResourcePermissionLocalService resourcePermissionLocalService;

	@Reference
	private RoleLocalService roleLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new ResourcePermissionUpgrade(counterLocalService, companyLocalService, expandoColumnLocalService, resourcePermissionLocalService, roleLocalService));
	}

}
