package com.placecube.digitalplace.local.user.expando.addressinfostatus.service;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatusConstants;

@Component(immediate = true, service = LanguageMessageService.class)
public class LanguageMessageService {

	public String getMessage(String key, Locale locale) {
		return AggregatedResourceBundleUtil.get(key, locale, AddressInfoStatusConstants.BUNDLE_ID, "com.placecube.digitalplace.user.account.web");
	}

}
