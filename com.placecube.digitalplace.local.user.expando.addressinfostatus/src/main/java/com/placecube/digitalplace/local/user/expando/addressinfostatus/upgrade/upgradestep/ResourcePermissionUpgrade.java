package com.placecube.digitalplace.local.user.expando.addressinfostatus.upgrade.upgradestep;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatusConstants;

import java.util.List;

public class ResourcePermissionUpgrade extends UpgradeProcess {

	private final CounterLocalService counterLocalService;

	private final CompanyLocalService companyLocalService;

	private final ExpandoColumnLocalService expandoColumnLocalService;

	private final ResourcePermissionLocalService resourcePermissionLocalService;

	private final RoleLocalService roleLocalService;

	public ResourcePermissionUpgrade(CounterLocalService counterLocalService, CompanyLocalService companyLocalService, ExpandoColumnLocalService expandoColumnLocalService,
			ResourcePermissionLocalService resourcePermissionLocalService, RoleLocalService roleLocalService) {
		this.companyLocalService = companyLocalService;
		this.counterLocalService = counterLocalService;
		this.expandoColumnLocalService = expandoColumnLocalService;
		this.resourcePermissionLocalService = resourcePermissionLocalService;
		this.roleLocalService = roleLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws PortalException {
		long guestRoleId = roleLocalService.getRole(companyId, RoleConstants.GUEST).getRoleId();
		long userRoleId = roleLocalService.getRole(companyId, RoleConstants.USER).getRoleId();

		ExpandoColumn expandoColumn = expandoColumnLocalService.getColumn(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
				AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME);
		if (Validator.isNotNull(expandoColumn)) {
			ResourcePermission userResourcePermission = resourcePermissionLocalService.getResourcePermission(companyId, ExpandoColumn.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
					String.valueOf(expandoColumn.getColumnId()), userRoleId);
			ResourcePermission guestResourcePermission;

			try {
				guestResourcePermission = resourcePermissionLocalService.getResourcePermission(companyId, ExpandoColumn.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(expandoColumn.getColumnId()), guestRoleId);
			} catch (Exception e) {
				guestResourcePermission = resourcePermissionLocalService.createResourcePermission(counterLocalService.increment(ResourcePermission.class.getName()));
			}
			updateResourcePermission(guestResourcePermission, guestRoleId, expandoColumn.getColumnId());
			updateResourcePermission(userResourcePermission, userRoleId, expandoColumn.getColumnId());
		}
	}

	private void updateResourcePermission(ResourcePermission resourcePermission, long roleId, long columnId) throws PortalException {
		if (Validator.isNotNull(resourcePermission)) {
			resourcePermission.setActionIds(9);
			resourcePermission.setCompanyId(resourcePermission.getCompanyId());
			resourcePermission.setName(ExpandoColumn.class.getName());
			resourcePermission.setPrimKey(String.valueOf(columnId));
			resourcePermission.setRoleId(roleId);
			resourcePermission.setScope(ResourceConstants.SCOPE_INDIVIDUAL);
			resourcePermissionLocalService.updateResourcePermission(resourcePermission);
			resourcePermissionLocalService.setResourcePermissions(resourcePermission.getCompanyId(), resourcePermission.getName(), resourcePermission.getScope(), resourcePermission.getPrimKey(),
					resourcePermission.getRoleId(), new String[] { ActionKeys.VIEW, ActionKeys.UPDATE });
		}
	}

}