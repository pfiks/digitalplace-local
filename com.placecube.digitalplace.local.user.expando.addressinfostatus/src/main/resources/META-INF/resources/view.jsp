<%@ include file="init.jsp"%>

<gds-forms-ui:input-checkbox 
	portletNamespace="${ portletNamespace }" 
	fieldName="${ addressInfoStatusFieldName }"
	fieldValue="${ accountContext.getExpandoValue(addressInfoStatusFieldName) }"
	errorMessage="${ accountContext.getFieldError(addressInfoStatusFieldName) }"
	fieldOptions="${ addressInfoFieldOptions }"
	fieldLabel="${ addressInfoStatusTitle }"

/>