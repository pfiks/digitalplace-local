package com.placecube.digitalplace.local.user.expando.addressinfostatus.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Locale;
import java.util.Map;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.configuration.AddressInfoStatusCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatus;

public class AddressInfoStatusServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	@InjectMocks
	private AddressInfoStatusService addressInfoStatusService;

	private Locale locale = Locale.ENGLISH;

	@Mock
	private AddressInfoStatusCompanyConfiguration mockAddressInfoStatusCompanyConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private LanguageMessageService mockLanguageMessageService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getCheckBoxOptions_WhenNoErrors_ThenIncludedInAvailableContactMethodMap() throws Exception {
		when(mockLanguageMessageService.getMessage(AddressInfoStatus.NO_FIXED_ABODE.getKey(), locale)).thenReturn("test-message");

		Map<String, String> checkBoxes = addressInfoStatusService.getCheckBoxOptions(locale);

		assertThat(checkBoxes, IsMapContaining.hasEntry(AddressInfoStatus.NO_FIXED_ABODE.getKey(), "test-message"));
	}

	@Test(expected = ConfigurationException.class)
	public void getAddressInfoStatusCompanyConfiguration_WhenErrors_ThenExceptionIsThrown() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(AddressInfoStatusCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID);
	}

	@Test
	public void getAddressInfoStatusCompanyConfiguration_WhenNoErrors_ThenConfigurationIsReturned() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(AddressInfoStatusCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockAddressInfoStatusCompanyConfiguration);

		AddressInfoStatusCompanyConfiguration result = addressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID);

		assertThat(result, equalTo(mockAddressInfoStatusCompanyConfiguration));
	}

}
