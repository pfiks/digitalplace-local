package com.placecube.digitalplace.local.user.expando.addressinfostatus.lifecyclelistener;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;

public class AddressInfoStatusLifeCycleListenerTest extends PowerMockito {

	@InjectMocks
	private AddressInfoStatusLifeCycleListener addressInfoStatusLifeCycleListener;

	@Mock
	private Company mockCompany;

	@Mock
	private ExpandoColumnCreatorInputStreamService mockExpandoColumnCreatorInputStreamService;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Before
	public void activeSetUp() {
		initMocks(this);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesExpandoColumn() throws Exception {
		addressInfoStatusLifeCycleListener.portalInstanceRegistered(mockCompany);

		verify(mockExpandoColumnCreatorInputStreamService, times(1)).createExpandoColumn(same(mockCompany), same(User.class.getName()), any(InputStream.class));

	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionCreatingColumn_ThenThrowsException() throws ExpandoColumnCreationException {

		when(mockExpandoColumnCreatorInputStreamService.createExpandoColumn(same(mockCompany), same(User.class.getName()), any(InputStream.class))).thenThrow(new Exception());

	}
}
