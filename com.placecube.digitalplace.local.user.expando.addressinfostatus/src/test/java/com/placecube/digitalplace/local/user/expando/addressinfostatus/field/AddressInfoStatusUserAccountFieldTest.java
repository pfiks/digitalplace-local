package com.placecube.digitalplace.local.user.expando.addressinfostatus.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.configuration.AddressInfoStatusCompanyConfiguration;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatus;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.constants.AddressInfoStatusConstants;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.service.AddressInfoStatusService;
import com.placecube.digitalplace.local.user.expando.addressinfostatus.service.LanguageMessageService;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.model.AccountContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class AddressInfoStatusUserAccountFieldTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	@InjectMocks
	private AddressInfoStatusUserAccountField addressInfoStatusUserAccountField;

	private Locale locale = Locale.ENGLISH;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AddressInfoStatusCompanyConfiguration mockAddressInfoStatusCompanyConfiguration;

	@Mock
	private AddressInfoStatusService mockAddressInfoStatusService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private LanguageMessageService mockLanguageMessageService;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activeSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		initMocks(this);
	}

	@Test
	public void configureContextValue_WhenNoFixedAbodeNotSelectedAndAddressIsBlank_ThenAddressInfoStatusSetToHasAddressNotProvided() {
		String addressValue = StringPool.BLANK;
		String noFixedAbodeValue = StringPool.BLANK;
		when(mockAccountContext.getFullAddress()).thenReturn(addressValue);
		when(ParamUtil.getString(mockPortletRequest, AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME, StringPool.BLANK)).thenReturn(noFixedAbodeValue);

		addressInfoStatusUserAccountField.configureContextValue(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addExpandoField(AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME, AddressInfoStatus.HAS_ADDRESS_NOT_PROVIDED.getKey());

	}

	@Test
	public void configureContextValue_WhenNoFixedAbodeNotSelectedAndAddressIsPopulated_ThenAddressInfoStatusSetToAddressProvided() {
		String addressValue = "address-not-empty";
		String noFixedAbodeValue = StringPool.BLANK;
		when(mockAccountContext.getFullAddress()).thenReturn(addressValue);
		when(ParamUtil.getString(mockPortletRequest, AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME, StringPool.BLANK)).thenReturn(noFixedAbodeValue);

		addressInfoStatusUserAccountField.configureContextValue(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addExpandoField(AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME, AddressInfoStatus.ADDRESS_PROVIDED.getKey());

	}

	@Test
	public void configureContextValue_WhenNoFixedAbodeSelectedAndAddressEmpty_ThenAddressInfoStatusSetAsNoFixedAbode() {
		String addressValue = StringPool.BLANK;
		String noFixedAbodeValue = AddressInfoStatus.NO_FIXED_ABODE.getKey();
		when(mockAccountContext.getFullAddress()).thenReturn(addressValue);
		when(ParamUtil.getString(mockPortletRequest, AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME, StringPool.BLANK)).thenReturn(noFixedAbodeValue);

		addressInfoStatusUserAccountField.configureContextValue(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addExpandoField(AddressInfoStatusConstants.ADDRESS_INFO_STATUS_FIELD_NAME, AddressInfoStatus.NO_FIXED_ABODE.getKey());

	}

	@Test
	public void getAccountCreationStep_WhenNoErrors_ThenAdditonalDetailsStepReturned() {

		AccountCreationStep result = addressInfoStatusUserAccountField.getAccountCreationStep();

		assertEquals(result, AccountCreationStep.ADDRESS_DETAILS);
	}

	@Test
	public void getBundleId_WhenNoErrors_ThenBundleIdIsReturned() {

		String result = addressInfoStatusUserAccountField.getBundleId();

		assertEquals(result, AddressInfoStatusConstants.BUNDLE_ID);
	}

	@Test
	public void getDependentRequiredFieldNamesAndMessage_WhenNoErrors_ThenReturnEmptyMap() {

		Map<String, String> results = addressInfoStatusUserAccountField.getDependentRequiredFieldNamesAndMessage("", locale);

		assertThat(results.entrySet(), Matchers.empty());
	}

	@Test
	public void getDisplayOrder_WhenNoErrors_ThenDisplayOrderIsReturned() {

		int result = addressInfoStatusUserAccountField.getDisplayOrder();

		assertEquals(result, 400);
	}

	@Test
	public void getRequiredMessage_WhenNoErrors_ThenRequiredMessageIsReturned() {

		String result = addressInfoStatusUserAccountField.getRequiredMessage(locale);

		assertEquals(result, StringPool.BLANK);
	}

	@Test
	public void isEnabled_WhenNoErrorsAndEnabledConfigurationIsFalse_ThenFalseIsReturned() throws ConfigurationException {
		when(mockAddressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID)).thenReturn(mockAddressInfoStatusCompanyConfiguration);
		when(mockAddressInfoStatusCompanyConfiguration.addressInfoStatusEnabled()).thenReturn(false);

		boolean result = addressInfoStatusUserAccountField.isEnabled(COMPANY_ID);

		assertEquals(false, result);
	}

	@Test
	public void isEnabled_WhenNoErrorsAndEnabledConfigurationIsTrue_ThenTrueIsReturned() throws ConfigurationException {
		when(mockAddressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID)).thenReturn(mockAddressInfoStatusCompanyConfiguration);
		when(mockAddressInfoStatusCompanyConfiguration.addressInfoStatusEnabled()).thenReturn(true);

		boolean result = addressInfoStatusUserAccountField.isEnabled(COMPANY_ID);

		assertEquals(true, result);
	}

	@Test
	public void isEnabled_WhenThereAreErrors_ThenReturnsFalse() throws ConfigurationException {
		when(mockAddressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		addressInfoStatusUserAccountField.isEnabled(COMPANY_ID);
	}

	@Test
	public void isRequired_WhenThereAreErrors_ThenReturnsFalse() throws ConfigurationException {
		when(mockAddressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		addressInfoStatusUserAccountField.isRequired(COMPANY_ID);
	}

	@Test(expected = PortalException.class)
	public void render_WhenErrorsRednering_ThenExceptionIsThrown() throws PortalException, IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		doThrow(new IOException()).when(mockJspRenderer).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");

		addressInfoStatusUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenNoErrors_ThenJSPIsRendered() throws PortalException, IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		addressInfoStatusUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenNoErrors_ThenRequestAttributesAreSet() throws Exception {
		Map<String, String> checkBoxOptions = new HashMap<>();
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockLanguageMessageService.getMessage("no-fixed-abode", locale)).thenReturn("no-fixed-abode-message");
		when(mockAddressInfoStatusService.getCheckBoxOptions(locale)).thenReturn(checkBoxOptions);
		when(mockAddressInfoStatusService.getAddressInfoStatusCompanyConfiguration(COMPANY_ID)).thenReturn(mockAddressInfoStatusCompanyConfiguration);

		addressInfoStatusUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("addressInfoFieldOptions", checkBoxOptions);
		verify(mockHttpServletRequest, times(1)).setAttribute("addressInfoStatusFieldName", "address-info-status");
		verify(mockHttpServletRequest, times(1)).setAttribute("addressInfoStatusTitle", "no-fixed-abode-message");
	}

	@Test(expected = Exception.class)
	public void render_WhenThereAreErrors_ThenExceptionIsThrown() throws PortalException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenThrow(new Exception());

		addressInfoStatusUserAccountField.render(mockHttpServletRequest, mockHttpServletResponse);
	}

}
