package com.placecube.digitalplace.local.webcontent.guide.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideConstants;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class, StringUtil.class })
public class GuideWebContentServiceTest extends PowerMockito {

	private static final String ESCAPED_HASH = "resident-permit-fees-and-tariffs";

	private static final Locale LOCALE = Locale.CANADA;

	private static final String UNESCAPED_HASH = "Resident (\"£*%)£)$^*\\permit__fees=and//tariffs";

	private GuideWebContentService guideWebContentService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private DefaultDDMStructureHelper mockDefaultDDMStructureHelper;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void generateGuideSectionHash_WhenPredefinedSectionHashIsNotEmpty_ThenReturnsEscapedPredefinedHash() {
		String result = guideWebContentService.generateGuideSectionHash(UNESCAPED_HASH, StringPool.BLANK);
		assertThat(result, equalTo(ESCAPED_HASH));
	}

	@Test
	public void generateGuideSectionHash_WhenPredefinedSectionIsEmptyAndSectionTitleIsEmpty_ThenReturnsRandomLowercaseStringFourCharactersLong() {
		final String randomString = "RaNd";

		when(StringUtil.randomString(4)).thenReturn(randomString);

		String result = guideWebContentService.generateGuideSectionHash(StringPool.BLANK, StringPool.BLANK);

		assertThat(result, equalTo(randomString.toLowerCase()));
	}

	@Test
	public void generateGuideSectionHash_WhenPredefinedSectionIsEmptyAndSectionTitleIsNotEmpty_ThenReturnsEscapedSectionTitle() {
		String result = guideWebContentService.generateGuideSectionHash(StringPool.BLANK, UNESCAPED_HASH);
		assertThat(result, equalTo(ESCAPED_HASH));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, GuideConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		guideWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenAddsDDMStructures() throws Exception {
		long groupId = 123;
		long classNameId = 456;
		long userId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);

		guideWebContentService.getOrCreateDDMStructure(mockServiceContext);

		verify(mockDefaultDDMStructureHelper, times(1)).addDDMStructures(userId, groupId, classNameId, getClass().getClassLoader(),
				"com/placecube/digitalplace/local/webcontent/guide/dependencies/ddm/structure.xml", mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheGuideStructure() throws PortalException {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, GuideConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure result = guideWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, GuideDDMTemplate.GUIDE_CONTENT.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, GuideConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		guideWebContentService.getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_CONTENT);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExist_ThenCreatesTemplate() throws PortalException {

		long groupId = 1;
		long classNameId = 2;
		long userId = 3;
		long structureId = 4;
		long journalArticleClassNameId = 5;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalArticleClassNameId);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, GuideDDMTemplate.GUIDE_CONTENT.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, journalArticleClassNameId, GuideConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getClassNameId()).thenReturn(classNameId);

		guideWebContentService.getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_CONTENT);

		verify(mockDDMTemplateLocalService, times(1)).addTemplate(userId, groupId, classNameId, structureId, classNameId, GuideDDMTemplate.GUIDE_CONTENT.getKey(),
				Collections.singletonMap(LOCALE, GuideDDMTemplate.GUIDE_CONTENT.getName()), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE,
				TemplateConstants.LANG_TYPE_FTL, null, true, false, StringPool.BLANK, null, mockServiceContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExistsAndPrefixIsNotNull_ThenReturnTemplate() throws PortalException {

		long groupId = 1;
		long classNameId = 2;
		long userId = 3;
		long structureId = 4;
		long journalArticleClassNameId = 5;

		String templateName = GroupConstants.GLOBAL + StringPool.SPACE + GuideDDMTemplate.GUIDE_CONTENT.getName();
		String templateKey = templateName.replace(StringPool.SPACE, StringPool.DASH).toUpperCase();

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalArticleClassNameId);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDMStructureLocalService.getStructure(groupId, journalArticleClassNameId, GuideConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getClassNameId()).thenReturn(classNameId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, templateKey)).thenReturn(null);

		guideWebContentService.getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_CONTENT, GroupConstants.GLOBAL);

		verify(mockDDMTemplateLocalService, times(1)).addTemplate(userId, groupId, classNameId, structureId, classNameId, templateKey, Collections.singletonMap(LOCALE, templateName), null,
				DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, null, true, false, StringPool.BLANK, null, mockServiceContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateExists_ThenReturnTemplate() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, GuideDDMTemplate.GUIDE_CONTENT.getKey())).thenReturn(mockDDMTemplate);

		DDMTemplate result = guideWebContentService.getOrCreateDDMTemplate(mockServiceContext, GuideDDMTemplate.GUIDE_CONTENT);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Test
	public void getOrCreateDefaultValues_WhenDefaultValuesAreCreated_ThenReturnThem() throws PortalException {
		long groupId = 1L;
		long ddmStructureClassNameId = 2L;
		long ddmJournalArticleClassNameId = 3L;
		long structureId = 4L;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(ddmStructureClassNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(ddmJournalArticleClassNameId);
		when(mockDDMTemplateLocalService.getTemplate(groupId, ddmStructureClassNameId, "GLOBAL-GUIDE-CONTENT")).thenReturn(mockDDMTemplate);
		when(mockDDMStructureLocalService.getStructure(groupId, ddmJournalArticleClassNameId, GuideConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		when(mockJournalArticleLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockJournalArticleLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.singletonList(mockJournalArticle));

		JournalArticle result = guideWebContentService.getOrCreateDefaultValues(mockServiceContext);
		assertEquals(mockJournalArticle, result);
	}

	@Test
	public void getOrCreateDefaultValues_WhenDefaultValuesAreNotCreated_ThenCreateAndReturnThem() throws Exception {
		long groupId = 1L;
		long ddmStructureClassNameId = 2L;
		long ddmJournalArticleClassNameId = 3L;
		long userId = 4L;
		long classPK = 5L;
		String content = "content";
		String articleLocalizedContent = "articleLocalizedContent";
		String ddmTemplateKey = "ddmTemplateKey";
		long structureId = 123;
		String defaultLanguageId = "defaultLanguageId";
		Map<Locale, String> titleMap = new HashMap<>();
		titleMap.put(Locale.UK, "");

		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(ddmStructureClassNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(ddmJournalArticleClassNameId);
		when(mockDDMTemplateLocalService.getTemplate(groupId, ddmStructureClassNameId, "GLOBAL-GUIDE-CONTENT")).thenReturn(mockDDMTemplate);
		when(mockDDMStructureLocalService.getStructure(groupId, ddmJournalArticleClassNameId, GuideConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		when(mockJournalArticleLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockJournalArticleLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.emptyList());

		when(mockServiceContext.getLocale()).thenReturn(Locale.UK);
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/guide/dependencies/ddm/content_default.xml")).thenReturn(content);
		when(mockGroupLocalService.getGroup(groupId)).thenReturn(mockGroup);
		when(mockGroup.getDefaultLanguageId()).thenReturn(defaultLanguageId);
		when(StringUtil.replace(content, "[$LOCALE_DEFAULT$]", defaultLanguageId)).thenReturn(articleLocalizedContent);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDMTemplate.getClassPK()).thenReturn(classPK);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMTemplate.getTemplateKey()).thenReturn(ddmTemplateKey);

		when(mockJournalArticleLocalService.addArticleDefaultValues(userId, groupId, ddmStructureClassNameId, classPK, titleMap, titleMap, articleLocalizedContent, structureId, ddmTemplateKey, null,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0, true, true, false, 0, 0, null, null, mockServiceContext)).thenReturn(mockJournalArticle);

		JournalArticle result = guideWebContentService.getOrCreateDefaultValues(mockServiceContext);
		assertEquals(mockJournalArticle, result);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDefaultValues_WhenErrorGettingStructure_ThenThrowsPortalException() throws PortalException {
		long groupId = 1L;
		long ddmStructureClassNameId = 2L;
		long ddmJournalArticleClassNameId = 3L;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(ddmStructureClassNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(ddmJournalArticleClassNameId);
		when(mockDDMTemplateLocalService.getTemplate(groupId, ddmStructureClassNameId, "GLOBAL-GUIDE-CONTENT")).thenReturn(mockDDMTemplate);
		when(mockDDMStructureLocalService.getStructure(groupId, ddmJournalArticleClassNameId, GuideConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		guideWebContentService.getOrCreateDefaultValues(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDefaultValues_WhenErrorGettingTemplate_ThenThrowsPortalException() throws PortalException {
		long groupId = 1L;
		long ddmStructureClassNameId = 2L;
		long ddmJournalArticleClassNameId = 3L;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(ddmStructureClassNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(ddmJournalArticleClassNameId);
		when(mockDDMTemplateLocalService.getTemplate(groupId, ddmStructureClassNameId, "GLOBAL-GUIDE-CONTENT")).thenThrow(new PortalException());

		guideWebContentService.getOrCreateDefaultValues(mockServiceContext);
	}

	@Before
	public void setUp() {
		mockStatic(RestrictionsFactoryUtil.class, StringUtil.class);

		guideWebContentService = new GuideWebContentService();
		guideWebContentService.ddmStructureLocalService = mockDDMStructureLocalService;
		guideWebContentService.defaultDDMStructureHelper = mockDefaultDDMStructureHelper;
		guideWebContentService.ddmTemplateLocalService = mockDDMTemplateLocalService;
		guideWebContentService.groupLocalService = mockGroupLocalService;
		guideWebContentService.journalArticleLocalService = mockJournalArticleLocalService;
		guideWebContentService.portal = mockPortal;
	}

}
