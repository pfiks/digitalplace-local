<#if SectionTitle?? && SectionTitle.getSiblings()?has_content && SectionTitle.getSiblings()?size gt 1 >

	<#assign guideWebContentService = digitalplace_guideWebContentService />
	
	<div class="multi-part-guide-navigation">
		<div class="boxed">

			<h2 class="nav__heading">In this section:</h2>

			<ol class="nav__list simple-list">
				
				<#list SectionTitle.getSiblings() as cur_SectionTitle>
					<#if SectionTitle.SectionHash??>
						<#assign sectionHash = cur_SectionTitle.SectionHash.getData() />
					<#else>
						<#assign sectionHash = '' />
					</#if>
					
					<#assign instanceId = guideWebContentService.generateGuideSectionHash(sectionHash, cur_SectionTitle.getData() ) />
					
					<li class="list__item ${(cur_SectionTitle?index == 0)?then('list__item--active-child','')}">
						<span class="list__link is-active">
							<strong class="d-none">You are here:</strong>
							<span class="glyphicon glyphicon-circle-arrow-right"></span>
							<span class="list__link-text ">${cur_SectionTitle.getData()}</span>
						</span>
						<a class="list__link" data-index="${cur_SectionTitle?index}" data-instance-id="${instanceId}" href="#${instanceId}">
							<span class="glyphicon glyphicon-circle-arrow-right"></span>
							<span class="list__link-text ">${cur_SectionTitle.getData()}</span>
						</a>
					</li>
				</#list>
			</ol>
		</div>
	</div>
</#if>