package com.placecube.digitalplace.local.webcontent.guide.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideConstants;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;

@Component(immediate = true, service = GuideWebContentService.class)
public class GuideWebContentService {

	private static final Log LOG = LogFactoryUtil.getLog(GuideWebContentService.class);

	@Reference
	DDMStructureLocalService ddmStructureLocalService;

	@Reference
	DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	GroupLocalService groupLocalService;

	@Reference
	JournalArticleLocalService journalArticleLocalService;

	@Reference
	Portal portal;

	public String generateGuideSectionHash(String predefinedSectionHash, String sectionTitle) {
		return Validator.isNotNull(predefinedSectionHash) ? escapeHash(predefinedSectionHash) : generateSectionHashFromTitle(sectionTitle);
	}

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		try {
			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
					"com/placecube/digitalplace/local/webcontent/guide/dependencies/ddm/structure.xml", serviceContext);

			return ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), "GUIDE");
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public DDMTemplate getOrCreateDDMTemplate(ServiceContext serviceContext, GuideDDMTemplate guideDDMTemplate) throws PortalException {
		try {
			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), guideDDMTemplate.getKey());

			return Validator.isNotNull(ddmTemplate) ? ddmTemplate : addDDMTemplate(serviceContext, guideDDMTemplate, guideDDMTemplate.getName(), guideDDMTemplate.getKey());
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public DDMTemplate getOrCreateDDMTemplate(ServiceContext serviceContext, GuideDDMTemplate guideDDMTemplate, String templatePrefix) throws PortalException {
		try {

			String ddmTemplateName = Validator.isNull(templatePrefix) ? guideDDMTemplate.getName() : templatePrefix + StringPool.SPACE + guideDDMTemplate.getName();
			String ddmTemplateKey = normalizeString(ddmTemplateName);

			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), ddmTemplateKey);

			return Validator.isNotNull(ddmTemplate) ? ddmTemplate : addDDMTemplate(serviceContext, guideDDMTemplate, ddmTemplateName, ddmTemplateKey);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public JournalArticle getOrCreateDefaultValues(ServiceContext serviceContext) throws PortalException {

		try {
			JournalArticle journalArticle;
			DDMTemplate ddmTemplate = ddmTemplateLocalService.getTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), "GLOBAL-GUIDE-CONTENT");
			DDMStructure guideStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), GuideConstants.STRUCTURE_KEY);

			DynamicQuery dynamicQuery = journalArticleLocalService.dynamicQuery();
			dynamicQuery.add(RestrictionsFactoryUtil.eq(Field.CLASS_NAME_ID, portal.getClassNameId(DDMStructure.class)));
			dynamicQuery.add(RestrictionsFactoryUtil.eq(Field.CLASS_PK, guideStructure.getStructureId()));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("DDMStructureId", guideStructure.getStructureId()));
			dynamicQuery.add(RestrictionsFactoryUtil.eq(Field.GROUP_ID, serviceContext.getScopeGroupId()));

			List<JournalArticle> journalArticles = journalArticleLocalService.dynamicQuery(dynamicQuery);
			if (journalArticles.isEmpty()) {
				Map<Locale, String> titleMap = new HashMap<>();
				titleMap.put(serviceContext.getLocale(), "");

				String content = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/guide/dependencies/ddm/content_default.xml");
				String articleLocalizedContent = StringUtil.replace(content, "[$LOCALE_DEFAULT$]", groupLocalService.getGroup(serviceContext.getScopeGroupId()).getDefaultLanguageId());

				journalArticle = journalArticleLocalService.addArticleDefaultValues(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class),
						ddmTemplate.getClassPK(), titleMap, titleMap, articleLocalizedContent, guideStructure.getStructureId(), ddmTemplate.getTemplateKey(), null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true,
						0, 0, 0, 0, 0, true, true, false, 0, 0, null, null, serviceContext);

			} else {
				journalArticle = journalArticles.get(0);
				if (journalArticles.size() > 1) {
					LOG.warn("There is more than one Default Value for the Structure with DDMStructureKey: " + GuideConstants.STRUCTURE_KEY);
				}
			}
			return journalArticle;
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private DDMTemplate addDDMTemplate(ServiceContext serviceContext, GuideDDMTemplate guideDDMTemplate, String ddmTemplateName, String ddmTemplateKey) throws PortalException {

		try {
			String script = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/guide/dependencies/ddm/" + guideDDMTemplate.getKey() + ".ftl");

			DDMStructure guideStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), GuideConstants.STRUCTURE_KEY);

			return ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), guideStructure.getStructureId(),
					guideStructure.getClassNameId(), ddmTemplateKey, Collections.singletonMap(serviceContext.getLocale(), ddmTemplateName), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY,
					DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, script, true, false, StringPool.BLANK, null, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private String escapeHash(String hash) {
		final String regex = "[^a-zA-Z0-9]+";

		String escapedHash = hash.replaceAll(regex, StringPool.SPACE);

		return escapedHash.replaceAll(StringPool.SPACE, StringPool.DASH).toLowerCase();
	}

	private String generateSectionHashFromTitle(String sectionTitle) {
		if (Validator.isNotNull(sectionTitle)) {
			return escapeHash(sectionTitle);
		} else {
			return StringUtil.randomString(4).toLowerCase();
		}
	}

	private String normalizeString(String string) {
		String regex = " +";
		String stringNoSpace = string.trim().replaceAll(regex, StringPool.SPACE);
		String stringCaps = stringNoSpace.toUpperCase();
		return stringCaps.replace(StringPool.SPACE, StringPool.DASH);
	}
}
