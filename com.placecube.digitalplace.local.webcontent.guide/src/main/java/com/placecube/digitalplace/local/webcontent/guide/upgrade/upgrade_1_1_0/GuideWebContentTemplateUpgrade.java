package com.placecube.digitalplace.local.webcontent.guide.upgrade.upgrade_1_1_0;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;

import java.util.List;

public class GuideWebContentTemplateUpgrade extends UpgradeProcess {

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(GuideWebContentTemplateUpgrade.class);

	public GuideWebContentTemplateUpgrade(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		upgradeGuideTemplate(companyId, GuideDDMTemplate.GUIDE_CONTENT);
		upgradeGuideTemplate(companyId, GuideDDMTemplate.GUIDE_NAVIGATION);

	}

	private void upgradeGuideTemplate(long companyId, GuideDDMTemplate template) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("templateKey", template.getKey()));

		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		if (ddmTemplates.isEmpty()) {
			LOG.warn("Template " + template.getKey() + " not found in company " + companyId);
		} else {
			ddmTemplates.forEach(this::updateTemplate);
		}

	}

	private void updateTemplate(DDMTemplate ddmTemplate) {
		String newScript = getAmendedScript(ddmTemplate.getScript());
		ddmTemplate.setScript(newScript);
		ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
	}

	private String getAmendedScript(String script) {
		return script.replace("serviceLocator.findService(\"com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService\")", "digitalplace_guideWebContentService");
	}
}