package com.placecube.digitalplace.local.webcontent.guide.upgrade;

import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;
import com.placecube.digitalplace.local.webcontent.guide.upgrade.upgrade_1_0_0.AddOrUpdateGuideWebContentDefaultValuesUpdate;
import com.placecube.digitalplace.local.webcontent.guide.upgrade.upgrade_1_1_0.GuideWebContentTemplateUpgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class GuideUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private GuideWebContentService guideWebContentService;

	@Reference
	private Portal portal;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new AddOrUpdateGuideWebContentDefaultValuesUpdate(companyLocalService, ddmStructureLocalService, ddmTemplateLocalService, guideWebContentService, portal));
		registry.register("1.0.0", "1.1.0", new GuideWebContentTemplateUpgrade(ddmTemplateLocalService));
	}

}
