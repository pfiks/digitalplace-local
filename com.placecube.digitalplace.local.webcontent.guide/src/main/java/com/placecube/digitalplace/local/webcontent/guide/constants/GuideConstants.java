package com.placecube.digitalplace.local.webcontent.guide.constants;

public final class GuideConstants {

	public static final String STRUCTURE_KEY = "GUIDE";

	private GuideConstants() {

	}

}
