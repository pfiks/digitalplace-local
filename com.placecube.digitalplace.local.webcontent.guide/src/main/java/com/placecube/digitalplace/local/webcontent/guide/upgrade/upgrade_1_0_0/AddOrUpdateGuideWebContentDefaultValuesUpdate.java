package com.placecube.digitalplace.local.webcontent.guide.upgrade.upgrade_1_0_0;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideConstants;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideDDMTemplate;
import com.placecube.digitalplace.local.webcontent.guide.service.GuideWebContentService;

import java.util.List;

public class AddOrUpdateGuideWebContentDefaultValuesUpdate extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final DDMStructureLocalService ddmStructureLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private final GuideWebContentService guideWebContentService;

	private final Portal portal;

	public AddOrUpdateGuideWebContentDefaultValuesUpdate(CompanyLocalService companyLocalService, DDMStructureLocalService ddmStructureLocalService, DDMTemplateLocalService ddmTemplateLocalService,
			GuideWebContentService guideWebContentService, Portal portal) {
		this.companyLocalService = companyLocalService;
		this.ddmStructureLocalService = ddmStructureLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
		this.guideWebContentService = guideWebContentService;
		this.portal = portal;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws Exception {
		Company company = companyLocalService.getCompanyById(companyId);
		Group globalGroup = company.getGroup();
		long globalGroupId = company.getGroupId();
		DDMStructure guideStructure = ddmStructureLocalService.fetchStructure(globalGroupId, portal.getClassNameId(JournalArticle.class), GuideConstants.STRUCTURE_KEY);
		DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(globalGroupId, portal.getClassNameId(DDMStructure.class), GuideDDMTemplate.GUIDE_CONTENT.getKey());
		if (Validator.isNotNull(guideStructure) && Validator.isNotNull(ddmTemplate)) {
			ServiceContext serviceContext = getServiceContext(globalGroup);

			guideWebContentService.getOrCreateDefaultValues(serviceContext);

			runSQLTemplateString("UPDATE JournalArticle SET DDMTemplateKey = '" + GuideDDMTemplate.GUIDE_CONTENT.getKey() + "' WHERE DDMStructureId = " + guideStructure.getStructureId()
					+ " AND companyId = " + company.getCompanyId(), false);
		}
	}

	private ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(false);

		return serviceContext;
	}

}
