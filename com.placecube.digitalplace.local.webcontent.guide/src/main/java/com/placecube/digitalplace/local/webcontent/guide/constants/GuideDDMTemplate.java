package com.placecube.digitalplace.local.webcontent.guide.constants;

public enum GuideDDMTemplate {

	GUIDE_CONTENT("GUIDE-CONTENT", "Guide Content"),
	GUIDE_NAVIGATION("GUIDE-NAVIGATION", "Guide Navigation");

	private final String key;
	private final String name;

	private GuideDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
