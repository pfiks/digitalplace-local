package com.placecube.digitalplace.local.ddmform.util;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessor;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessorRegistry;

public class DDMFormInstanceRecordPostProccesorUtilTest extends PowerMockito {

	@InjectMocks
	private DDMFormInstanceRecordPostProccesorUtil ddmFormInstanceRecordPostProccesorUtil;

	@Mock
	private FormInstanceRecordPostProcessorRegistry mockDDMFormInstanceRecordPostProcessorRegistry;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private FormInstanceRecordPostProcessor mockDDMFormInstanceRecordPostProcessor;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void processPostProcessors_WhenNoError_ThenDoesProcessPostProcessors() {

		List<FormInstanceRecordPostProcessor> postProcessors = new ArrayList<>();
		postProcessors.add(mockDDMFormInstanceRecordPostProcessor);

		when(mockDDMFormInstanceRecordPostProcessorRegistry.getPostProcessors()).thenReturn(postProcessors);

		ddmFormInstanceRecordPostProccesorUtil.processPostProcessors(mockDDMFormInstanceRecord, mockServiceContext);

		verify(mockDDMFormInstanceRecordPostProcessor, times(1)).process(mockDDMFormInstanceRecord, mockServiceContext);
	}

}
