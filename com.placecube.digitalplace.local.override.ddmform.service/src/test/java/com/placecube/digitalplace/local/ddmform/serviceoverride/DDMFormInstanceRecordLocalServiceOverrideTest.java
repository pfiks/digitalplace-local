package com.placecube.digitalplace.local.ddmform.serviceoverride;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.ddmform.util.DDMFormInstanceRecordPostProccesorUtil;
import com.placecube.digitalplace.local.ddmform.util.DDMFormInstanceRecordSessionUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DDMFormInstanceRecordLocalServiceOverrideTest extends PowerMockito {

	@InjectMocks
	private DDMFormInstanceRecordLocalServiceOverride ddmFormInstanceRecordLocalServiceOverride;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDDMFormInstanceRecordLocalService;

	@Mock
	private DDMFormInstanceRecordPostProccesorUtil mockDDMFormInstanceRecordPostProccesorUtil;

	@Mock
	private DDMFormInstanceRecordSessionUtil mockDDMFormInstanceRecordSessionUtil;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecordUpdated;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void addDDMFormInstanceRecord_WhenNoError_ThenDoesProcessPostProcessors() throws PortalException {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockDDMFormInstanceRecordLocalService.addDDMFormInstanceRecord(mockDDMFormInstanceRecord)).thenReturn(mockDDMFormInstanceRecordUpdated);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.addDDMFormInstanceRecord(mockDDMFormInstanceRecord);

		assertThat(result, sameInstance(mockDDMFormInstanceRecordUpdated));

		verify(mockDDMFormInstanceRecordPostProccesorUtil, times(1)).processPostProcessors(mockDDMFormInstanceRecordUpdated, mockServiceContext);
	}

	@Test
	public void addDDMFormInstanceRecord_WhenNoError_ThenSavesTheCreatedFormInstanceRecordIdInSession() throws PortalException {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockDDMFormInstanceRecordLocalService.addDDMFormInstanceRecord(mockDDMFormInstanceRecord)).thenReturn(mockDDMFormInstanceRecordUpdated);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.addDDMFormInstanceRecord(mockDDMFormInstanceRecord);

		assertThat(result, sameInstance(mockDDMFormInstanceRecordUpdated));

		verify(mockDDMFormInstanceRecordSessionUtil, times(1)).saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecordUpdated, mockServiceContext);
	}

	@Test
	public void addFormInstanceRecord_WhenNoError_ThenDoesProcessPostProcessors() throws PortalException {
		long userId = 1;
		long groupId = 2;
		long ddmFormInstanceId = 3;
		when(mockDDMFormInstanceRecordLocalService.addFormInstanceRecord(userId, groupId, ddmFormInstanceId, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDMFormInstanceRecord);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.addFormInstanceRecord(userId, groupId, ddmFormInstanceId, mockDDMFormValues, mockServiceContext);

		assertThat(result, sameInstance(mockDDMFormInstanceRecord));

		verify(mockDDMFormInstanceRecordPostProccesorUtil, times(1)).processPostProcessors(mockDDMFormInstanceRecord, mockServiceContext);
	}

	@Test
	public void addFormInstanceRecord_WhenNoError_ThenSavesTheCreatedFormInstanceRecordIdInSession() throws PortalException {
		long userId = 1;
		long groupId = 2;
		long ddmFormInstanceId = 3;
		when(mockDDMFormInstanceRecordLocalService.addFormInstanceRecord(userId, groupId, ddmFormInstanceId, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDMFormInstanceRecord);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.addFormInstanceRecord(userId, groupId, ddmFormInstanceId, mockDDMFormValues, mockServiceContext);

		assertThat(result, sameInstance(mockDDMFormInstanceRecord));

		verify(mockDDMFormInstanceRecordSessionUtil, times(1)).saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);
	}

	@Before
	public void setUp() {
		mockStatic(ServiceContextThreadLocal.class);
	}

	@Test
	public void updateDDMFormInstanceRecord_WhenNoError_ThenDoesProcessPostProcessors() throws PortalException {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockDDMFormInstanceRecordLocalService.updateDDMFormInstanceRecord(mockDDMFormInstanceRecord)).thenReturn(mockDDMFormInstanceRecordUpdated);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.updateDDMFormInstanceRecord(mockDDMFormInstanceRecord);

		assertThat(result, sameInstance(mockDDMFormInstanceRecordUpdated));

		verify(mockDDMFormInstanceRecordPostProccesorUtil, times(1)).processPostProcessors(mockDDMFormInstanceRecordUpdated, mockServiceContext);
	}

	@Test
	public void updateDDMFormInstanceRecord_WhenNoError_ThenSavesTheUpdatedFormInstanceRecordIdInSession() throws PortalException {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockDDMFormInstanceRecordLocalService.updateDDMFormInstanceRecord(mockDDMFormInstanceRecord)).thenReturn(mockDDMFormInstanceRecordUpdated);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.updateDDMFormInstanceRecord(mockDDMFormInstanceRecord);

		assertThat(result, sameInstance(mockDDMFormInstanceRecordUpdated));

		verify(mockDDMFormInstanceRecordSessionUtil, times(1)).saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecordUpdated, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateFormInstanceRecord_WhenNoError_ThenDoesProcessPostProcessors(boolean majorVersion) throws PortalException {
		long userId = 1;
		long ddmFormInstanceRecordId = 2;
		when(mockDDMFormInstanceRecordLocalService.updateFormInstanceRecord(userId, ddmFormInstanceRecordId, majorVersion, mockDDMFormValues, mockServiceContext))
				.thenReturn(mockDDMFormInstanceRecord);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.updateFormInstanceRecord(userId, ddmFormInstanceRecordId, majorVersion, mockDDMFormValues, mockServiceContext);

		assertThat(result, sameInstance(mockDDMFormInstanceRecord));

		verify(mockDDMFormInstanceRecordPostProccesorUtil, times(1)).processPostProcessors(mockDDMFormInstanceRecord, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateFormInstanceRecord_WhenNoError_ThenSavesTheUpdatedFormInstanceRecordIdInSession(boolean majorVersion) throws PortalException {
		long userId = 1;
		long ddmFormInstanceRecordId = 2;
		when(mockDDMFormInstanceRecordLocalService.updateFormInstanceRecord(userId, ddmFormInstanceRecordId, majorVersion, mockDDMFormValues, mockServiceContext))
				.thenReturn(mockDDMFormInstanceRecord);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.updateFormInstanceRecord(userId, ddmFormInstanceRecordId, majorVersion, mockDDMFormValues, mockServiceContext);

		assertThat(result, sameInstance(mockDDMFormInstanceRecord));

		verify(mockDDMFormInstanceRecordSessionUtil, times(1)).saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);
	}

	@Test
	public void updateStatus_WhenNoError_ThenDoesProcessPostProcessors() throws PortalException {

		int status = WorkflowConstants.STATUS_APPROVED;
		long userId = 111;

		long recordVersionId = 5432;
		when(mockDDMFormInstanceRecordLocalService.updateStatus(userId, recordVersionId, status, mockServiceContext)).thenReturn(mockDDMFormInstanceRecord);

		DDMFormInstanceRecord result = ddmFormInstanceRecordLocalServiceOverride.updateStatus(userId, recordVersionId, status, mockServiceContext);

		assertThat(result, sameInstance(mockDDMFormInstanceRecord));

		verify(mockDDMFormInstanceRecordPostProccesorUtil, times(1)).processPostProcessors(mockDDMFormInstanceRecord, mockServiceContext);

	}

}
