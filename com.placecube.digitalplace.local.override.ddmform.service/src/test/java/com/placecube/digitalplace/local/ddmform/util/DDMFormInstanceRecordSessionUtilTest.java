package com.placecube.digitalplace.local.ddmform.util;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrieval;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrievalRegistry;

public class DDMFormInstanceRecordSessionUtilTest extends PowerMockito {

	@InjectMocks
	private DDMFormInstanceRecordSessionUtil ddmFormInstanceRecordSessionUtil;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private FormInstanceRecordIdRetrieval mockFormInstanceRecordIdRetrieval;

	@Mock
	private FormInstanceRecordIdRetrievalRegistry mockFormInstanceRecordIdRetrievalRegistry;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpSession mockHttpSession;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void saveFormInstanceRecordIdInSession_WhenServiceContextIsNull_ThenNoExceptionIsThrown() {
		when(mockFormInstanceRecordIdRetrievalRegistry.getRetrieval(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());
		try {
			ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, null);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void saveFormInstanceRecordIdInSession_WhenRequestIsNull_ThenNoExceptionIsThrown() {
		when(mockFormInstanceRecordIdRetrievalRegistry.getRetrieval(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());
		when(mockServiceContext.getRequest()).thenReturn(null);

		try {
			ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void saveFormInstanceRecordIdInSession_WhenSessionIsNull_ThenNoExceptionIsThrown() {
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(null);
		when(mockFormInstanceRecordIdRetrievalRegistry.getRetrieval(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		try {
			ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void saveFormInstanceRecordIdInSession_WhenNoError_ThenSetsTheFormInstanceRecordIdInSession() {
		long formInstanceRecordId = 123;
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(formInstanceRecordId);
		when(mockFormInstanceRecordIdRetrievalRegistry.getRetrieval(mockDDMFormInstanceRecord)).thenReturn(Optional.empty());

		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);

		verify(mockHttpSession, times(1)).setAttribute("formInstanceRecordId", String.valueOf(formInstanceRecordId));
	}

	@Test
	public void saveFormInstanceRecordIdInSession_WhenRetrievalIdIsPresent_ThenSetsTheFormInstanceRecordIdInSession() {
		long formInstanceRecordId = 123;
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(formInstanceRecordId);
		when(mockFormInstanceRecordIdRetrievalRegistry.getRetrieval(mockDDMFormInstanceRecord)).thenReturn(Optional.of(mockFormInstanceRecordIdRetrieval));
		String retrievedId = "321";
		when(mockFormInstanceRecordIdRetrieval.getId(mockDDMFormInstanceRecord)).thenReturn(retrievedId);

		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);

		verify(mockHttpSession, times(1)).setAttribute("formInstanceRecordId", retrievedId);
	}

	@Test
	public void saveFormInstanceRecordIdInSession_WhenRetrievalIdIsPresent_ThenSetsTheDdmFormInstanceRecordInSession() {
		long formInstanceRecordId = 123;
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(formInstanceRecordId);
		when(mockFormInstanceRecordIdRetrievalRegistry.getRetrieval(mockDDMFormInstanceRecord)).thenReturn(Optional.of(mockFormInstanceRecordIdRetrieval));
		String retrievedId = "321";
		when(mockFormInstanceRecordIdRetrieval.getId(mockDDMFormInstanceRecord)).thenReturn(retrievedId);

		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(mockDDMFormInstanceRecord, mockServiceContext);

		verify(mockHttpSession, times(1)).setAttribute("ddmFormInstanceRecord", mockDDMFormInstanceRecord);
	}
}
