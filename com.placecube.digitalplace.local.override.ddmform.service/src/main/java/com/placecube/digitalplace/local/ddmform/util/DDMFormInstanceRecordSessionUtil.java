package com.placecube.digitalplace.local.ddmform.util;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrieval;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrievalRegistry;

@Component(immediate = true, service = DDMFormInstanceRecordSessionUtil.class)
public class DDMFormInstanceRecordSessionUtil {

	private static final Log LOG = LogFactoryUtil.getLog(DDMFormInstanceRecordSessionUtil.class);

	@Reference
	private FormInstanceRecordIdRetrievalRegistry formInstanceRecordIdRetrievalRegistry;

	public void saveFormInstanceRecordIdInSession(DDMFormInstanceRecord formInstanceRecord, ServiceContext serviceContext) {
		try {
			String formInstanceRecordId = String.valueOf(formInstanceRecord.getFormInstanceRecordId());

			Optional<FormInstanceRecordIdRetrieval> retrievalId = formInstanceRecordIdRetrievalRegistry.getRetrieval(formInstanceRecord);
			if (retrievalId.isPresent()) {
				formInstanceRecordId = retrievalId.get().getId(formInstanceRecord);
				serviceContext.getRequest().getSession().setAttribute("ddmFormInstanceRecord", formInstanceRecord);
			}

			LOG.debug("Saving FormInstanceRecordId In Session - " + formInstanceRecordId);

			serviceContext.getRequest().getSession().setAttribute("formInstanceRecordId", formInstanceRecordId);
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to save FormInstanceRecordId In Session" + e.getMessage());
		}
	}

}
