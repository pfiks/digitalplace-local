package com.placecube.digitalplace.local.ddmform.util;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessorRegistry;

@Component(immediate = true, service = DDMFormInstanceRecordPostProccesorUtil.class)
public class DDMFormInstanceRecordPostProccesorUtil {

	@Reference
	private FormInstanceRecordPostProcessorRegistry ddmFormInstanceRecordPostProcessorRegistry;

	public void processPostProcessors(DDMFormInstanceRecord formInstanceRecord, ServiceContext serviceContext) {

		ddmFormInstanceRecordPostProcessorRegistry.getPostProcessors().forEach(pp -> pp.process(formInstanceRecord, serviceContext));

	}

}
