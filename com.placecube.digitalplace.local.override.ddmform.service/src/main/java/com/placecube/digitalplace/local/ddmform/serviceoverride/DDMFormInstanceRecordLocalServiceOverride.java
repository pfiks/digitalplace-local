package com.placecube.digitalplace.local.ddmform.serviceoverride;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalServiceWrapper;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.placecube.digitalplace.local.ddmform.util.DDMFormInstanceRecordPostProccesorUtil;
import com.placecube.digitalplace.local.ddmform.util.DDMFormInstanceRecordSessionUtil;

@Component(immediate = true, property = {}, service = ServiceWrapper.class)
public class DDMFormInstanceRecordLocalServiceOverride extends DDMFormInstanceRecordLocalServiceWrapper {

	@Reference(unbind = "-")
	private DDMFormInstanceRecordPostProccesorUtil ddmFormInstanceRecordPostProccesorUtil;

	@Reference(unbind = "-")
	private DDMFormInstanceRecordSessionUtil ddmFormInstanceRecordSessionUtil;

	public DDMFormInstanceRecordLocalServiceOverride() {
		super(null);
	}

	@Override
	public DDMFormInstanceRecord addDDMFormInstanceRecord(DDMFormInstanceRecord ddmFormInstanceRecord) {
		DDMFormInstanceRecord formInstanceRecord = super.addDDMFormInstanceRecord(ddmFormInstanceRecord);
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		ddmFormInstanceRecordPostProccesorUtil.processPostProcessors(formInstanceRecord, serviceContext);
		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(formInstanceRecord, serviceContext);
		return formInstanceRecord;
	}

	@Override
	public DDMFormInstanceRecord addFormInstanceRecord(long userId, long groupId, long ddmFormInstanceId, DDMFormValues ddmFormValues, ServiceContext serviceContext) throws PortalException {
		DDMFormInstanceRecord formInstanceRecord = super.addFormInstanceRecord(userId, groupId, ddmFormInstanceId, ddmFormValues, serviceContext);
		ddmFormInstanceRecordPostProccesorUtil.processPostProcessors(formInstanceRecord, serviceContext);
		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(formInstanceRecord, serviceContext);
		return formInstanceRecord;
	}

	@Override
	public DDMFormInstanceRecord updateDDMFormInstanceRecord(DDMFormInstanceRecord ddmFormInstanceRecord) {
		DDMFormInstanceRecord formInstanceRecord = super.updateDDMFormInstanceRecord(ddmFormInstanceRecord);
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		ddmFormInstanceRecordPostProccesorUtil.processPostProcessors(formInstanceRecord, serviceContext);
		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(formInstanceRecord, serviceContext);
		return formInstanceRecord;
	}

	@Override
	public DDMFormInstanceRecord updateFormInstanceRecord(long userId, long ddmFormInstanceRecordId, boolean majorVersion, DDMFormValues ddmFormValues, ServiceContext serviceContext)
			throws PortalException {
		DDMFormInstanceRecord formInstanceRecord = super.updateFormInstanceRecord(userId, ddmFormInstanceRecordId, majorVersion, ddmFormValues, serviceContext);
		ddmFormInstanceRecordPostProccesorUtil.processPostProcessors(formInstanceRecord, serviceContext);
		ddmFormInstanceRecordSessionUtil.saveFormInstanceRecordIdInSession(formInstanceRecord, serviceContext);
		return formInstanceRecord;
	}

	@Override
	public DDMFormInstanceRecord updateStatus(long userId, long recordVersionId, int status, ServiceContext serviceContext) throws PortalException {
		DDMFormInstanceRecord formInstanceRecord = super.updateStatus(userId, recordVersionId, status, serviceContext);
		ddmFormInstanceRecordPostProccesorUtil.processPostProcessors(formInstanceRecord, serviceContext);
		return formInstanceRecord;
	}

	@Reference(unbind = "-")
	private void serviceSetter(DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService) {
		setWrappedService(ddmFormInstanceRecordLocalService);
	}
}
