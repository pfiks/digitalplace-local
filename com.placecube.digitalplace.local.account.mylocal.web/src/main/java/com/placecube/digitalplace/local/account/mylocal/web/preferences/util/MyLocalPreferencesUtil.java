package com.placecube.digitalplace.local.account.mylocal.web.preferences.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalConstants;

@Component(immediate = true, service = MyLocalPreferencesUtil.class)
public class MyLocalPreferencesUtil {

	@Reference
	private JSONFactory jsonFactory;

	public JSONArray buildPreferencesArray(long[] availableContents, long[] enabledContents) {
		JSONArray preferencesArray = jsonFactory.createJSONArray();

		for (long contentId : availableContents) {
			boolean isEnabled = false;

			for (long enabledContentId : enabledContents) {
				if (enabledContentId == contentId) {
					isEnabled = true;
					break;
				}
			}

			JSONObject jsonObject = jsonFactory.createJSONObject();

			jsonObject.put(MyLocalConstants.ASSET_ENTRY_ID, contentId);
			jsonObject.put(MyLocalConstants.ENABLED_FIELD, isEnabled);

			preferencesArray.put(jsonObject);
		}

		return preferencesArray;
	}

	public List<Long> getDisabledContents(String preferences) throws JSONException {
		List<Long> disabledContents = new ArrayList<>();

		JSONArray preferencesArray = jsonFactory.createJSONArray(preferences);

		for (int i = 0; i < preferencesArray.length(); i++) {
			JSONObject jsonObject = preferencesArray.getJSONObject(i);

			if (!jsonObject.getBoolean(MyLocalConstants.ENABLED_FIELD)) {
				disabledContents.add(jsonObject.getLong(MyLocalConstants.ASSET_ENTRY_ID));
			}
		}

		return disabledContents;
	}

	public Map<AssetEntry, Boolean> getEntriesMap(List<AssetEntry> entries, List<Long> disabledContents) {
		Map<AssetEntry, Boolean> entriesMap = new LinkedHashMap<>();

		for (AssetEntry entry : entries) {
			entriesMap.put(entry, !disabledContents.contains(entry.getPrimaryKey()));
		}

		return entriesMap;
	}
}
