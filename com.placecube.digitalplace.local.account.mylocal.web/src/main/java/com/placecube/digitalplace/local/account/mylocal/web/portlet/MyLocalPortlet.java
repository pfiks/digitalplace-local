package com.placecube.digitalplace.local.account.mylocal.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalPortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.my-account", "com.liferay.portlet.css-class-wrapper=my-local",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true", "com.liferay.portlet.preferences-owned-by-group=true",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.config-jsp=/configuration/configuration.jsp", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MyLocalPortletKeys.MY_LOCAL, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class MyLocalPortlet extends MVCPortlet {

}
