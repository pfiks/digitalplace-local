package com.placecube.digitalplace.local.account.mylocal.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "my-local", scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(id = "com.placecube.digitalplace.local.account.mylocal.web.configuration.MyLocalPortletConfiguration", localization = "content/Language", name = "my-local")
public interface MyLocalPortletConfiguration {

	@Meta.AD(required = false, name = "my-local-asset-category", description = "my-local-asset-category-description")
	long myLocalAssetCategoryId();

	@Meta.AD(required = false, name = "my-local-title", description = "my-local-title-description")
	String myLocalTitle();
}
