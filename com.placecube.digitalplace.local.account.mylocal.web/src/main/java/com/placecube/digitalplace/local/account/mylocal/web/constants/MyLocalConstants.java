package com.placecube.digitalplace.local.account.mylocal.web.constants;

public final class MyLocalConstants {

	public static final String ASSET_ENTRY_ID = "assetEntryId";
	public static final String ENABLED_FIELD = "enabled";

	private MyLocalConstants() {

	}
}
