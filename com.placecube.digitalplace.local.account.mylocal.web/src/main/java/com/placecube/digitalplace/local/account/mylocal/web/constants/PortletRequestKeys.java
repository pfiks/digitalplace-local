package com.placecube.digitalplace.local.account.mylocal.web.constants;

public final class PortletRequestKeys {

	public static final String ENTRIES_MAP = "entriesMap";

	public static final String PRIMARY_ADDRESS = "primaryAddress";

	private PortletRequestKeys() {

	}

}
