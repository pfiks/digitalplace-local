package com.placecube.digitalplace.local.account.mylocal.web.constants;

public final class MyLocalMVCCommandKeys {

	public static final String UPDATE_USER_PREFERENCES = "/update-user-preferences";

	private MyLocalMVCCommandKeys() {

	}
}
