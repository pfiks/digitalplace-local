package com.placecube.digitalplace.local.account.mylocal.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.service.MyLocalPreferencesLocalService;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalMVCCommandKeys;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalPortletKeys;
import com.placecube.digitalplace.local.account.mylocal.web.preferences.util.MyLocalPreferencesUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + MyLocalPortletKeys.MY_LOCAL, "mvc.command.name=" + MyLocalMVCCommandKeys.UPDATE_USER_PREFERENCES }, service = MVCActionCommand.class)
public class UpdateUserPreferencesMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private MyLocalPreferencesUtil myLocalPreferencesUtil;

	@Reference
	private MyLocalPreferencesLocalService preferencesLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long[] availableContents = ParamUtil.getLongValues(actionRequest, "availableContents");
		long[] enabledContents = ParamUtil.getLongValues(actionRequest, "enabledContents");
		String portletId = (String) actionRequest.getAttribute(WebKeys.PORTLET_ID);
		long userId = themeDisplay.getUserId();
		long plid = themeDisplay.getPlid();
		long groupId = themeDisplay.getScopeGroupId();

		if (ArrayUtil.isNotEmpty(availableContents) && ArrayUtil.isNotEmpty(enabledContents)) {
			JSONArray preferencesArray = myLocalPreferencesUtil.buildPreferencesArray(availableContents, enabledContents);

			MyLocalPreferences preferences = preferencesLocalService.fetchPreferencesByGroupIdUserIdPlidAndPortletId(groupId, userId, plid, portletId);

			if (Validator.isNotNull(preferences)) {
				preferences.setPreferences(preferencesArray.toJSONString());
			} else {
				preferences = preferencesLocalService.addPreferences(themeDisplay.getCompanyId(), groupId, userId, plid, portletId, preferencesArray.toJSONString());
			}

			preferencesLocalService.updatePreferences(preferences);
		}
	}
}
