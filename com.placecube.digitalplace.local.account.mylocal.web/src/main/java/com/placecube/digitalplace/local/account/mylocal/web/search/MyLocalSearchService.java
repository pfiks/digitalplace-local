package com.placecube.digitalplace.local.account.mylocal.web.search;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.portal.kernel.exception.PortalException;

@Component(immediate = true, service = MyLocalSearchService.class)
public class MyLocalSearchService {

	@Reference
	private AssetEntryService assetEntryService;

	public List<AssetEntry> searchAssetEntries(long assetCategoryId) throws PortalException {
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		assetEntryQuery.setAllCategoryIds(new long[] { assetCategoryId });

		return assetEntryService.getEntries(assetEntryQuery);
	}

}
