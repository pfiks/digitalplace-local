package com.placecube.digitalplace.local.account.mylocal.web.category;

import org.osgi.service.component.annotations.Component;

import com.liferay.configuration.admin.category.ConfigurationCategory;

@Component(service = ConfigurationCategory.class)
public class MyLocalConfigurationCategory implements ConfigurationCategory {

	private static final String CATEGORY_ICON = "forms";

	private static final String CATEGORY_KEY = "my-local";

	private static final String CATEGORY_SECTION = "other";

	@Override
	public String getCategoryIcon() {
		return CATEGORY_ICON;
	}

	@Override
	public String getCategoryKey() {
		return CATEGORY_KEY;
	}

	@Override
	public String getCategorySection() {
		return CATEGORY_SECTION;
	}

}