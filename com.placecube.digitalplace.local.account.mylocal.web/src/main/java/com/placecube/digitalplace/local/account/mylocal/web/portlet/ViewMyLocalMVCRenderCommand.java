package com.placecube.digitalplace.local.account.mylocal.web.portlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.service.MyLocalPreferencesLocalService;
import com.placecube.digitalplace.local.account.mylocal.web.configuration.MyLocalPortletConfiguration;
import com.placecube.digitalplace.local.account.mylocal.web.configuration.constants.MyLocalPortletConfigurationConstants;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalPortletKeys;
import com.placecube.digitalplace.local.account.mylocal.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.account.mylocal.web.preferences.util.MyLocalPreferencesUtil;
import com.placecube.digitalplace.local.account.mylocal.web.search.MyLocalSearchService;

@Component(immediate = true, property = { "javax.portlet.name=" + MyLocalPortletKeys.MY_LOCAL, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewMyLocalMVCRenderCommand implements MVCRenderCommand {

	private static final Log log = LogFactoryUtil.getLog(MyLocalPortlet.class);

	@Reference
	private ConfigurationProvider configurationProvider;
	
	@Reference
	private MyLocalPreferencesUtil myLocalPreferencesUtil;

	@Reference
	private MyLocalSearchService myLocalSearchService;

	@Reference
	private MyLocalPreferencesLocalService preferencesLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			MyLocalPortletConfiguration myLocalConfiguration = configurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, themeDisplay);
			renderRequest.setAttribute(MyLocalPortletConfigurationConstants.CONFIGURATION, myLocalConfiguration);

			if (myLocalConfiguration.myLocalAssetCategoryId() == 0) {
				renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
			} else {
				User user = userLocalService.getUser(themeDisplay.getUserId());
				renderRequest.setAttribute(PortletRequestKeys.PRIMARY_ADDRESS, getPrimaryAddress(user, themeDisplay.getLocale()));

				String portletId = (String) renderRequest.getAttribute(WebKeys.PORTLET_ID);
				MyLocalPreferences preferences = preferencesLocalService.fetchPreferencesByGroupIdUserIdPlidAndPortletId(themeDisplay.getScopeGroupId(), user.getUserId(), themeDisplay.getPlid(),
						portletId);

				List<Long> disabledContents = new ArrayList<>();

				if (Validator.isNotNull(preferences)) {
					disabledContents = myLocalPreferencesUtil.getDisabledContents(preferences.getPreferences());
				}

				List<AssetEntry> entries = myLocalSearchService.searchAssetEntries(myLocalConfiguration.myLocalAssetCategoryId());

				Map<AssetEntry, Boolean> entriesMap = myLocalPreferencesUtil.getEntriesMap(entries, disabledContents);

				renderRequest.setAttribute(PortletRequestKeys.ENTRIES_MAP, entriesMap);
			}
		} catch (ConfigurationException e) {
			log.error("Configuration error on My Local Portlet: " + e);
		} catch (Exception e) {
			log.error(e);
			throw new PortletException(e);
		}

		return "/view.jsp";
	}

	private String getPrimaryAddress(User user, Locale locale) {
		String primaryAddress = StringPool.BLANK;
		Optional<Address> addressOpt = user.getAddresses().stream().filter(Address::isPrimary).findFirst();

		if (addressOpt.isPresent()) {
			Address address = addressOpt.get();
			StringJoiner joiner = new StringJoiner(", ");

			joiner.add(address.getStreet1());
			if (Validator.isNotNull(address.getStreet2())) {
				joiner.add(address.getStreet2());
			}
			if (Validator.isNotNull(address.getStreet3())) {
				joiner.add(address.getStreet3());
			}
			joiner.add(address.getCity());
			if (Validator.isNotNull(address.getZip())) {
				joiner.add(address.getZip());
			}
			if (Validator.isNotNull(address.getRegion().getName())) {
				joiner.add(address.getRegion().getName());
			}
			if (Validator.isNotNull(address.getCountry().getName(locale))) {
				joiner.add(address.getCountry().getName(locale));
			}

			primaryAddress = joiner.toString();
		}

		return primaryAddress;
	}

}
