package com.placecube.digitalplace.local.account.mylocal.web.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.account.assetvocabulary.mylocal.constants.MyLocalConstants;
import com.placecube.digitalplace.local.account.mylocal.web.configuration.constants.MyLocalPortletConfigurationConstants;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalPortletKeys;

@Component(configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true, property = { "javax.portlet.name=" + MyLocalPortletKeys.MY_LOCAL }, service = ConfigurationAction.class)
public class MyLocalPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;
	
	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		MyLocalPortletConfiguration myLocalConfiguration = configurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, themeDisplay);
		httpServletRequest.setAttribute(MyLocalPortletConfigurationConstants.CONFIGURATION, myLocalConfiguration);

		try {
			AssetVocabulary myLocalVocabulary = assetVocabularyLocalService.getGroupVocabulary(themeDisplay.getCompanyGroupId(), MyLocalConstants.MY_LOCAL_VOCAB_NAME);

			long[] vocabularyIds = new long[] { myLocalVocabulary.getVocabularyId() };
			httpServletRequest.setAttribute(MyLocalPortletConfigurationConstants.VOCABULARY_IDS, vocabularyIds);

			long[] selectedCategoryIds = new long[] { myLocalConfiguration.myLocalAssetCategoryId() };
			httpServletRequest.setAttribute(MyLocalPortletConfigurationConstants.SELECTED_CATEGORY_IDS, selectedCategoryIds);

			httpServletRequest.setAttribute(MyLocalPortletConfigurationConstants.MY_LOCAL_TITLE, myLocalConfiguration.myLocalTitle());

		} catch (PortalException e) {
			throw new PortletException("Unable to get My Local vocabulary from company groupId: " + themeDisplay.getCompanyGroupId(), e);
		}

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		setPreference(actionRequest, MyLocalPortletConfigurationConstants.MY_LOCAL_ASSET_CATEGORY_ID, ParamUtil.getString(actionRequest, "assetCategoryIds"));
		setPreference(actionRequest, MyLocalPortletConfigurationConstants.MY_LOCAL_TITLE, ParamUtil.getString(actionRequest, MyLocalPortletConfigurationConstants.MY_LOCAL_TITLE));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
