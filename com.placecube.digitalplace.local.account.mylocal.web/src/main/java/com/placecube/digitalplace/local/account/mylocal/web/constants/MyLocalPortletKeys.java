package com.placecube.digitalplace.local.account.mylocal.web.constants;

public final class MyLocalPortletKeys {

	public static final String MY_LOCAL = "com_placecube_digitalplace_local_account_mylocal_web_portlet_MyLocalPortlet";

	private MyLocalPortletKeys() {
	}
}
