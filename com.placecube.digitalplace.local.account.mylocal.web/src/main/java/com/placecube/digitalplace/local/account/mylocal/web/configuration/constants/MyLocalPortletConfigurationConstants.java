package com.placecube.digitalplace.local.account.mylocal.web.configuration.constants;

public final class MyLocalPortletConfigurationConstants {

	public static final String CONFIGURATION = "configuration";

	public static final String MY_LOCAL_ASSET_CATEGORY_ID = "myLocalAssetCategoryId";

	public static final String MY_LOCAL_TITLE = "myLocalTitle";

	public static final String SELECTED_CATEGORY_IDS = "selectedCategoryIds";

	public static final String VOCABULARY_IDS = "vocabularyIds";

	private MyLocalPortletConfigurationConstants() {

	}
}
