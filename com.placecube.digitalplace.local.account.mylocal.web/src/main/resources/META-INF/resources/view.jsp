<%@ include file="init.jsp"%>

<c:choose>
	<c:when test="${configuration.myLocalAssetCategoryId() > 0 }">
		<div class="my-local-container container">
			<div class="row">
				<div class="col-md-10">
					<h2>${ configuration.myLocalTitle() }</h2>
					<div>${ primaryAddress }</div>
				</div>
				<div class="col-md-2 pr-0">
					<%@ include file="/partials/settings_button.jspf"%>
				</div>
			</div>
			<br>
			<c:choose>
				<c:when test="${not empty entriesMap}">
					<div class="card-deck">
						<c:forEach items="${entriesMap}" var="entry">
							<c:if test="${entry.getValue()}">
								<%@ include file="/partials/view_asset_entry.jspf"%>
							</c:if>
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
					<div class="alert alert-info text-center">
						<div>
							<liferay-ui:message key="there-are-no-results" />
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</c:when>
	<c:otherwise>
		<div class="alert alert-info text-center">
			<div>
				<liferay-ui:message	key="this-application-is-not-visible-to-users-yet" />
			</div>

			<div>
				<aui:a href="javascript:;" onClick="${ portletDisplay.getURLConfigurationJS() }">
					<liferay-ui:message key="select-a-category-to-make-it-visible" />
				</aui:a>
			</div>
		</div>
	</c:otherwise>
</c:choose>