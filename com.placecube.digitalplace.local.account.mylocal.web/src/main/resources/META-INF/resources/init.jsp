<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/asset" prefix="liferay-asset"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<%@ page import="com.liferay.asset.kernel.model.AssetEntry"%>

<%@ page import="com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalMVCCommandKeys"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />