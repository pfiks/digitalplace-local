<%@ include file="init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%=true%>" var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>">
	<div class="portlet-configuration-body-content">
		<clay:container-fluid>
			<aui:input name="<%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />
			<aui:input name="redirect" type="hidden" value="<%=configurationRenderURL%>" />
			
			<aui:fieldset label="my-local-asset-category">
			     <aui:input type="hidden" name="assetCategoryIds" id="assetCategoryIds" />
			     <dp-frontend:asset-categories-selector vocabularyIds="${ vocabularyIds }" singleSelect="true" categoryIds="${selectedCategoryIds}"/>
			</aui:fieldset>
			
			<aui:fieldset>
				<aui:input type="text" name="myLocalTitle" label="my-local-title" value="${ myLocalTitle }" required="true" />
			</aui:fieldset>
			
			<aui:button-row>
			     <aui:button type="submit" value="save" onClick="populateHiddenCategories();"/>
			</aui:button-row>
		</clay:container-fluid>
	</div>
</aui:form>

<aui:script>
	function populateHiddenCategories() {
		var categoryIds = [];
		var selectedCategories = $('input[name*="<portlet:namespace/>assetCategoryIds_"]');
		
		selectedCategories.each(function(){
            categoryIds.push($(this).val());
		});
		
		$('#<portlet:namespace/>assetCategoryIds').val(categoryIds);
	}
</aui:script>