<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<%@ taglib uri="http://placecube.com/digitalplace/tld/frontend" prefix="dp-frontend" %>

<%@ page import="com.liferay.portal.kernel.util.Constants"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />