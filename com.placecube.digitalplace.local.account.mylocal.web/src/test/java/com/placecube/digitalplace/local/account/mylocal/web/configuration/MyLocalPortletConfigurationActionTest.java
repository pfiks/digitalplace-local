package com.placecube.digitalplace.local.account.mylocal.web.configuration;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.osgi.service.component.annotations.Reference;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.account.assetvocabulary.mylocal.constants.MyLocalConstants;
import com.placecube.digitalplace.local.account.mylocal.web.configuration.constants.MyLocalPortletConfigurationConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, DefaultConfigurationAction.class })
public class MyLocalPortletConfigurationActionTest extends PowerMockito {

	private static final String ASSET_CATEGORY_IDS = "1";
	private static final long CATEGORY_ID = 1;
	private static final long COMPANY_GROUP_ID = 1;
	private static final String MY_LOCAL_TITLE = "MY LOCAL TITLE";
	private static final long VOCABULARY_ID = 2;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;
	
	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private MyLocalPortletConfiguration mockMyLocalPortletConfiguration;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private MyLocalPortletConfigurationAction myLocalPortletConfigurationAction;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenAttributesAreSet() throws Exception {

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(COMPANY_GROUP_ID);

		when(mockAssetVocabularyLocalService.getGroupVocabulary(COMPANY_GROUP_ID, MyLocalConstants.MY_LOCAL_VOCAB_NAME)).thenReturn(mockAssetVocabulary);

		when(mockAssetVocabulary.getVocabularyId()).thenReturn(VOCABULARY_ID);
		when(mockMyLocalPortletConfiguration.myLocalAssetCategoryId()).thenReturn(CATEGORY_ID);
		when(mockMyLocalPortletConfiguration.myLocalTitle()).thenReturn(MY_LOCAL_TITLE);

		suppressSuperCallToIncludeMethod();

		myLocalPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(MyLocalPortletConfigurationConstants.CONFIGURATION, mockMyLocalPortletConfiguration);
		verify(mockHttpServletRequest, times(1)).setAttribute(MyLocalPortletConfigurationConstants.VOCABULARY_IDS, new long[] { VOCABULARY_ID });
		verify(mockHttpServletRequest, times(1)).setAttribute(MyLocalPortletConfigurationConstants.SELECTED_CATEGORY_IDS, new long[] { CATEGORY_ID });
		verify(mockHttpServletRequest, times(1)).setAttribute(MyLocalPortletConfigurationConstants.MY_LOCAL_TITLE, MY_LOCAL_TITLE);
	}

	@Test(expected = PortletException.class)
	public void include_WhenVocabularyDoesNotExistForTheCompanyGroupIdAndName_ThenPortletExceptionIsThrown() throws Exception {

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(COMPANY_GROUP_ID);

		doThrow(new PortalException()).when(mockAssetVocabularyLocalService).getGroupVocabulary(COMPANY_GROUP_ID, MyLocalConstants.MY_LOCAL_VOCAB_NAME);

		suppressSuperCallToIncludeMethod();

		myLocalPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processAction_WhenNoError_ThenSetPreferences() throws Exception {
		myLocalPortletConfigurationAction = Mockito.spy(MyLocalPortletConfigurationAction.class);

		when(ParamUtil.getString(mockActionRequest, "assetCategoryIds")).thenReturn(ASSET_CATEGORY_IDS);

		supressSuperCallToProcessActionMethod();

		myLocalPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(myLocalPortletConfigurationAction, times(1)).setPreference(eq(mockActionRequest), eq(MyLocalPortletConfigurationConstants.MY_LOCAL_ASSET_CATEGORY_ID), eq(ASSET_CATEGORY_IDS));
	}

	private void suppressSuperCallToIncludeMethod() throws NoSuchMethodException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { PortletConfig.class, HttpServletRequest.class, HttpServletResponse.class };
		Method superRenderMethod = DefaultConfigurationAction.class.getMethod("include", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

	private void supressSuperCallToProcessActionMethod() throws NoSuchMethodException, SecurityException {
		@SuppressWarnings("rawtypes")
		Class[] cArg = new Class[] { PortletConfig.class, ActionRequest.class, ActionResponse.class };
		Method superRenderMethod = DefaultConfigurationAction.class.getMethod("processAction", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

}
