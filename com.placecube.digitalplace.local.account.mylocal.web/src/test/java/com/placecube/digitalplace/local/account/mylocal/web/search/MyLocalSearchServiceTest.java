package com.placecube.digitalplace.local.account.mylocal.web.search;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.PropsUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, MyLocalSearchService.class })
public class MyLocalSearchServiceTest extends PowerMockito {

	private static final long CATEGORY_ID = 1;
	private static final int TOTAL = 2;

	@Mock
	private List<AssetEntry> mockAssetEntries;

	@Mock
	private AssetEntryQuery mockAssetEntryQuery;

	@Mock
	private AssetEntryService mockAssetEntryService;

	@InjectMocks
	private MyLocalSearchService myLocalSearchService;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class);
	}

	@Test
	public void searchAssetEntries_WenNoError_ThenSearchContainerIsSet() throws Exception {

		whenNew(AssetEntryQuery.class).withNoArguments().thenReturn(mockAssetEntryQuery);

		when(mockAssetEntryService.getEntries(mockAssetEntryQuery)).thenReturn(mockAssetEntries);
		when(mockAssetEntries.size()).thenReturn(TOTAL);

		myLocalSearchService.searchAssetEntries(CATEGORY_ID);

		verify(mockAssetEntryQuery, times(1)).setAllCategoryIds(new long[] { CATEGORY_ID });
	}

	@Test(expected = PortalException.class)
	public void searchAssetEntries_WhenErrorGettingAssetEntries_ThenThrowsPortalException() throws Exception {
		whenNew(AssetEntryQuery.class).withNoArguments().thenReturn(mockAssetEntryQuery);

		when(mockAssetEntryService.getEntries(mockAssetEntryQuery)).thenThrow(PortalException.class);

		myLocalSearchService.searchAssetEntries(CATEGORY_ID);
	}

}
