package com.placecube.digitalplace.local.account.mylocal.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Country;
import com.liferay.portal.kernel.model.Region;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.service.MyLocalPreferencesLocalService;
import com.placecube.digitalplace.local.account.mylocal.web.configuration.MyLocalPortletConfiguration;
import com.placecube.digitalplace.local.account.mylocal.web.configuration.constants.MyLocalPortletConfigurationConstants;
import com.placecube.digitalplace.local.account.mylocal.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.account.mylocal.web.preferences.util.MyLocalPreferencesUtil;
import com.placecube.digitalplace.local.account.mylocal.web.search.MyLocalSearchService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class })
public class ViewMyLocalMVCRenderCommandTest extends PowerMockito {

	private static final long CATEGORY_ID = 1;

	private static final String CITY = "CITY";

	private static final String COUNTRY = "COUNTRY";

	private static final long GROUP_ID = 3;

	private static final long PLID = 4;

	private static final String PORTLET_ID = "PORTLET_ID";

	private static final String PREFERENCES_BODY = "PREFERENCES_BODY";

	private static final String REGION = "REGION";

	private static final String STREET_1 = "STREET_1";

	private static final String STREET_2 = "STREET_2";

	private static final String STREET_3 = "STREET_3";

	private static final long USER_ID = 2;

	private static final String VIEW_JSP = "/view.jsp";

	private static final String ZIP = "123";

	@Mock
	private Address mockAddress;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;
	
	@Mock
	private List<AssetEntry> mockAssetEntryList;

	@Mock
	private Country mockCountry;

	@Mock
	private Map<AssetEntry, Boolean> mockEntriesMap;

	@Mock
	private Locale mockLocale;

	@Mock
	private MyLocalPortletConfiguration mockMyLocalPortletConfiguration;

	@Mock
	private MyLocalPreferencesUtil mockMyLocalPreferencesUtil;

	@Mock
	private MyLocalSearchService mockMyLocalSearchService;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private MyLocalPreferences mockMyLocalPreferences;

	@Mock
	private MyLocalPreferencesLocalService mockMyLocalPreferencesLocalService;

	@Mock
	private Region mockRegion;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private ViewMyLocalMVCRenderCommand viewMyLocalMVCRenderCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void render_WhenCategoryIsConfigured_ThenUserPrimaryAddressAndEntriesMapAttributesAreSet() throws Exception {
		List<Address> addresses = new ArrayList<>();
		addresses.add(mockAddress);

		String expectedPrimaryAddress = "STREET_1, STREET_2, STREET_3, CITY, 123, REGION, COUNTRY";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);

		when(mockMyLocalPortletConfiguration.myLocalAssetCategoryId()).thenReturn(CATEGORY_ID);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(mockLocale);
		when(mockUser.getAddresses()).thenReturn(addresses);

		when(mockAddress.isPrimary()).thenReturn(true);
		when(mockAddress.getStreet1()).thenReturn(STREET_1);
		when(mockAddress.getStreet2()).thenReturn(STREET_2);
		when(mockAddress.getStreet3()).thenReturn(STREET_3);
		when(mockAddress.getCity()).thenReturn(CITY);
		when(mockAddress.getZip()).thenReturn(ZIP);
		when(mockAddress.getRegion()).thenReturn(mockRegion);
		when(mockRegion.getName()).thenReturn(REGION);
		when(mockAddress.getCountry()).thenReturn(mockCountry);
		when(mockCountry.getName(mockLocale)).thenReturn(COUNTRY);

		when(mockRenderRequest.getAttribute(WebKeys.PORTLET_ID)).thenReturn(PORTLET_ID);

		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockMyLocalPreferencesLocalService.fetchPreferencesByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID)).thenReturn(mockMyLocalPreferences);
		when(mockMyLocalPreferencesUtil.getDisabledContents(PREFERENCES_BODY)).thenReturn(new ArrayList<>());

		when(mockMyLocalSearchService.searchAssetEntries(CATEGORY_ID)).thenReturn(mockAssetEntryList);

		when(mockMyLocalPreferencesUtil.getEntriesMap(eq(mockAssetEntryList), any(List.class))).thenReturn(mockEntriesMap);

		viewMyLocalMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.PRIMARY_ADDRESS, expectedPrimaryAddress);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.ENTRIES_MAP, mockEntriesMap);
	}

	@Test
	public void render_WhenCategoryIsNotConfigured_ThenConfiguratorVisibilityIsSet() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);
		when(mockMyLocalPortletConfiguration.myLocalAssetCategoryId()).thenReturn(0L);

		viewMyLocalMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
	}

	@Test
	public void render_WhenConfigurationError_ThenConfigurationAttributeIsNotSet() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenThrow(ConfigurationException.class);
		viewMyLocalMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(MyLocalPortletConfigurationConstants.CONFIGURATION, mockMyLocalPortletConfiguration);
	}

	@Test(expected = PortletException.class)
	public void render_WhenErrorGettingAssetEntries_ThenThrowsPortletException() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);
		when(mockMyLocalPortletConfiguration.myLocalAssetCategoryId()).thenReturn(CATEGORY_ID);

		when(mockMyLocalSearchService.searchAssetEntries(CATEGORY_ID)).thenThrow(PortalException.class);

		viewMyLocalMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenRequestConfigurationAttributeIsSet() throws Exception {
		List<Address> addresses = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);
		when(mockMyLocalPortletConfiguration.myLocalAssetCategoryId()).thenReturn(CATEGORY_ID);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(mockLocale);
		when(mockUser.getAddresses()).thenReturn(addresses);

		viewMyLocalMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(MyLocalPortletConfigurationConstants.CONFIGURATION, mockMyLocalPortletConfiguration);
	}

	@Test
	public void render_WhenNoError_ThenReturnViewJSP() throws Exception {
		List<Address> addresses = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(MyLocalPortletConfiguration.class, mockThemeDisplay)).thenReturn(mockMyLocalPortletConfiguration);
		when(mockMyLocalPortletConfiguration.myLocalAssetCategoryId()).thenReturn(CATEGORY_ID);

		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockThemeDisplay.getLocale()).thenReturn(mockLocale);
		when(mockUser.getAddresses()).thenReturn(addresses);

		String view = viewMyLocalMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(view, equalTo(VIEW_JSP));
	}

}
