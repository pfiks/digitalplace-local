package com.placecube.digitalplace.local.account.mylocal.web.preferences.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.local.account.mylocal.web.constants.MyLocalConstants;

@RunWith(PowerMockRunner.class)
public class MyLocalPreferencesUtilTest extends PowerMockito {

	private static final long ASSET_ENTRY_PRIMARY_KEY_1 = 5;

	private static final long ASSET_ENTRY_PRIMARY_KEY_2 = 4;

	private static final long[] AVAILABLE_CONTENTS = new long[] { 1, 2, 3 };

	private static final long[] ENABLED_CONTENTS = new long[] { 1, 3 };

	@Mock
	private AssetEntry mockAssetEntry1;

	@Mock
	private AssetEntry mockAssetEntry2;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@InjectMocks
	private MyLocalPreferencesUtil myLocalPreferencesUtil;

	@Test
	public void buildPreferencesArray_WhenNoError_ThenPreferencesArrayIsReturned() {

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);

		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObject);

		myLocalPreferencesUtil.buildPreferencesArray(AVAILABLE_CONTENTS, ENABLED_CONTENTS);

		verify(mockJSONObject, times(1)).put(MyLocalConstants.ASSET_ENTRY_ID, 1L);
		verify(mockJSONObject, times(1)).put(MyLocalConstants.ASSET_ENTRY_ID, 2L);
		verify(mockJSONObject, times(1)).put(MyLocalConstants.ASSET_ENTRY_ID, 3L);
		verify(mockJSONObject, times(2)).put(MyLocalConstants.ENABLED_FIELD, true);
		verify(mockJSONObject, times(1)).put(MyLocalConstants.ENABLED_FIELD, false);
		verify(mockJSONArray, times(3)).put(mockJSONObject);
	}

	@Test
	public void getDisabledContents_WhenNoError_ThenDisabledContentIdsAreReturned() throws JSONException {
		String preferencesBody = "[{\"assetEntryId\": 1,\"enabled\": true},{\"assetEntryId\": 2,\"enabled\": false},{\"assetEntryId\": 3,\"enabled\": true}]";

		when(mockJSONFactory.createJSONArray(preferencesBody)).thenReturn(mockJSONArray);

		when(mockJSONArray.length()).thenReturn(1);

		when(mockJSONArray.getJSONObject(anyInt())).thenReturn(mockJSONObject);
		when(mockJSONObject.getBoolean(MyLocalConstants.ENABLED_FIELD)).thenReturn(false);
		when(mockJSONObject.getLong(MyLocalConstants.ASSET_ENTRY_ID)).thenReturn(2L);

		List<Long> result = myLocalPreferencesUtil.getDisabledContents(preferencesBody);

		assertTrue(result.contains(2L));
	}

	@Test
	public void getEntriesMap_WhenNoError_ThenEntriesMapIsReturned() {

		List<AssetEntry> entries = new ArrayList<>();
		entries.add(mockAssetEntry1);
		entries.add(mockAssetEntry2);

		List<Long> disabledContents = new ArrayList<>();
		disabledContents.add(ASSET_ENTRY_PRIMARY_KEY_2);

		when(mockAssetEntry1.getPrimaryKey()).thenReturn(ASSET_ENTRY_PRIMARY_KEY_1);
		when(mockAssetEntry2.getPrimaryKey()).thenReturn(ASSET_ENTRY_PRIMARY_KEY_2);

		Map<AssetEntry, Boolean> result = myLocalPreferencesUtil.getEntriesMap(entries, disabledContents);

		assertTrue(result.get(mockAssetEntry1));
		assertFalse(result.get(mockAssetEntry2));

	}

}
