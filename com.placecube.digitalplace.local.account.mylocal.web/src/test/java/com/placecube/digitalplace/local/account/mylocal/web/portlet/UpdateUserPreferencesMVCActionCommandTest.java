package com.placecube.digitalplace.local.account.mylocal.web.portlet;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.service.MyLocalPreferencesLocalService;
import com.placecube.digitalplace.local.account.mylocal.web.preferences.util.MyLocalPreferencesUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ArrayUtil.class })
public class UpdateUserPreferencesMVCActionCommandTest extends PowerMockito {

	private static final long[] AVAILABLE_CONTENTS = new long[] { 1, 2, 3 };

	private static final long COMPANY_ID = 4;

	private static final long[] ENABLED_CONTENTS = new long[] { 1, 3 };

	private static final long GROUP_ID = 3;

	private static final long PLID = 2;

	private static final String PORTLET_ID = "PORTLET_ID";

	private static final String PREFERENCES_BODY = "PREFERENCES_BODY";

	private static final long USER_ID = 1;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private MyLocalPreferencesUtil mockMyLocalPreferencesUtil;

	@Mock
	private MyLocalPreferences mockMyLocalPreferences;

	@Mock
	private MyLocalPreferencesLocalService mockMyLocalPreferencesLocalService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateUserPreferencesMVCActionCommand updateUserPreferencesMVCActionCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, ArrayUtil.class);
	}

	@Test
	public void doProcessAction_WhenAvailableContentsAndEnabledContentsAreNotEmptyAndPreferencesDoNotExist_ThenPreferencesAreCreated() throws Exception {

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLongValues(mockActionRequest, "availableContents")).thenReturn(AVAILABLE_CONTENTS);
		when(ParamUtil.getLongValues(mockActionRequest, "enabledContents")).thenReturn(ENABLED_CONTENTS);
		when(mockActionRequest.getAttribute(WebKeys.PORTLET_ID)).thenReturn(PORTLET_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(ArrayUtil.isNotEmpty(AVAILABLE_CONTENTS)).thenReturn(true);
		when(ArrayUtil.isNotEmpty(ENABLED_CONTENTS)).thenReturn(true);

		when(mockMyLocalPreferencesUtil.buildPreferencesArray(AVAILABLE_CONTENTS, ENABLED_CONTENTS)).thenReturn(mockJSONArray);
		when(mockMyLocalPreferencesLocalService.fetchPreferencesByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID)).thenReturn(null);
		when(mockJSONArray.toJSONString()).thenReturn(PREFERENCES_BODY);

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockMyLocalPreferencesLocalService.addPreferences(COMPANY_ID, GROUP_ID, USER_ID, PLID, PORTLET_ID, PREFERENCES_BODY)).thenReturn(mockMyLocalPreferences);

		updateUserPreferencesMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMyLocalPreferencesLocalService, times(1)).addPreferences(COMPANY_ID, GROUP_ID, USER_ID, PLID, PORTLET_ID, PREFERENCES_BODY);
		verify(mockMyLocalPreferencesLocalService, times(1)).updatePreferences(mockMyLocalPreferences);

	}

	@Test
	public void doProcessAction_WhenAvailableContentsAndEnabledContentsAreNotEmptyAndPreferencesExist_ThenPreferencesAreUpdated() throws Exception {

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLongValues(mockActionRequest, "availableContents")).thenReturn(AVAILABLE_CONTENTS);
		when(ParamUtil.getLongValues(mockActionRequest, "enabledContents")).thenReturn(ENABLED_CONTENTS);
		when(mockActionRequest.getAttribute(WebKeys.PORTLET_ID)).thenReturn(PORTLET_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(ArrayUtil.isNotEmpty(AVAILABLE_CONTENTS)).thenReturn(true);
		when(ArrayUtil.isNotEmpty(ENABLED_CONTENTS)).thenReturn(true);

		when(mockMyLocalPreferencesUtil.buildPreferencesArray(AVAILABLE_CONTENTS, ENABLED_CONTENTS)).thenReturn(mockJSONArray);
		when(mockMyLocalPreferencesLocalService.fetchPreferencesByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID)).thenReturn(mockMyLocalPreferences);
		when(mockJSONArray.toJSONString()).thenReturn(PREFERENCES_BODY);

		updateUserPreferencesMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMyLocalPreferences, times(1)).setPreferences(PREFERENCES_BODY);
		verify(mockMyLocalPreferencesLocalService, times(1)).updatePreferences(mockMyLocalPreferences);

	}

	@Test
	public void doProcessAction_WhenAvailableContentsIsEmptyAndEnabledContentsIsNotEmpty_ThenPreferencesAreUpdated() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLongValues(mockActionRequest, "availableContents")).thenReturn(AVAILABLE_CONTENTS);
		when(ParamUtil.getLongValues(mockActionRequest, "enabledContents")).thenReturn(ENABLED_CONTENTS);
		when(mockActionRequest.getAttribute(WebKeys.PORTLET_ID)).thenReturn(PORTLET_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(ArrayUtil.isNotEmpty(AVAILABLE_CONTENTS)).thenReturn(false);
		when(ArrayUtil.isNotEmpty(ENABLED_CONTENTS)).thenReturn(true);

		updateUserPreferencesMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMyLocalPreferencesLocalService, never()).updatePreferences(mockMyLocalPreferences);
	}

	@Test
	public void doProcessAction_WhenAvailableContentsIsNotEmptyAndEnabledContentsIsEmpty_ThenPreferencesAreUpdated() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(ParamUtil.getLongValues(mockActionRequest, "availableContents")).thenReturn(AVAILABLE_CONTENTS);
		when(ParamUtil.getLongValues(mockActionRequest, "enabledContents")).thenReturn(ENABLED_CONTENTS);
		when(mockActionRequest.getAttribute(WebKeys.PORTLET_ID)).thenReturn(PORTLET_ID);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);

		when(ArrayUtil.isNotEmpty(AVAILABLE_CONTENTS)).thenReturn(true);
		when(ArrayUtil.isNotEmpty(ENABLED_CONTENTS)).thenReturn(false);

		updateUserPreferencesMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMyLocalPreferencesLocalService, never()).updatePreferences(mockMyLocalPreferences);
	}

}
