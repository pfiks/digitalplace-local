package com.placecube.digitalplace.local.search.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.search.constants.SearchWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTest {

	@InjectMocks
	private SearchService searchService;

	private ClassLoader searchServiceClassLoader;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private AssetListEntryInitializer mockAssetListEntryInitializer;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		searchServiceClassLoader = SearchService.class.getClassLoader();
	}


	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenException_ThenThrowsPortalException() throws Exception {

		SearchWidgetTemplate widgetTemplate = SearchWidgetTemplate.SEARCH_LIST;
		String templateResourcePath = "com/placecube/digitalplace/local/search/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), "com.liferay.portal.search.web.internal.result.display.context.SearchResultSummaryDisplayContext", templateResourcePath, searchServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		searchService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		SearchWidgetTemplate widgetTemplate = SearchWidgetTemplate.SEARCH_LIST;
		String templateResourcePath = "com/placecube/digitalplace/local/search/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), "com.liferay.portal.search.web.internal.result.display.context.SearchResultSummaryDisplayContext", templateResourcePath, searchServiceClassLoader,
				mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = searchService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

}
