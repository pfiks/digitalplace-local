package com.placecube.digitalplace.local.search.service;


import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.search.constants.SearchWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = SearchService.class)
public class SearchService {
	
	@Reference
	private DDMInitializer ddmInitializer;

	public DDMTemplate getOrCreateWidgetTemplate(SearchWidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = "com/placecube/digitalplace/local/search/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
		DDMTemplateContext ddmTemplateContext = ddmInitializer
				.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), "com.liferay.portal.search.web.internal.result.display.context.SearchResultSummaryDisplayContext", templateResourcePath, getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}

}
