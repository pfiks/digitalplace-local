package com.placecube.digitalplace.local.search.constants;

public enum SearchWidgetTemplate {
	
	SEARCH_LIST("SEARCH-RESULTS-LIST", "Search List");

	private final String key;
	private final String name;

	private SearchWidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}