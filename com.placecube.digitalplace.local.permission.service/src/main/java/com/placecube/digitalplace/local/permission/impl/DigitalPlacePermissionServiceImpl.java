package com.placecube.digitalplace.local.permission.impl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.application.list.PanelCategory;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.application.list.util.PanelCategoryRegistryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService;

@Component(immediate = true, service = DigitalPlacePermissionService.class)
public class DigitalPlacePermissionServiceImpl implements DigitalPlacePermissionService {

	@Override
	public Boolean hasSiteAdminAccess(Group group, PermissionChecker permissionChecker) {
		List<PanelCategory> childPanelCategories = PanelCategoryRegistryUtil.getChildPanelCategories(PanelCategoryKeys.ROOT, permissionChecker, group);
		return !childPanelCategories.isEmpty();
	}

}
