package com.placecube.digitalplace.local.permission.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.application.list.PanelCategory;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.application.list.util.PanelCategoryRegistryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PanelCategoryRegistryUtil.class})
@SuppressStaticInitializationFor({"com.liferay.application.list.util.PanelCategoryRegistryUtil"})
public class DigitalPlacePermissionServiceImplTest extends PowerMockito {

	@InjectMocks
	private DigitalPlacePermissionServiceImpl digitalPlacePermissionServiceImpl;

	@Mock
	private Group mockGroup;

	@Mock
	private PanelCategory mockPanelCategory;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Before
	public void setUp() {
		
		mockStatic(PanelCategoryRegistryUtil.class);
	}

	@Test
	public void hasControlPanelAccess_WhenNoPanelCategoriesAreFound_ThenReturnsFalse() throws PortalException {
		when(PanelCategoryRegistryUtil.getChildPanelCategories(PanelCategoryKeys.ROOT, mockPermissionChecker, mockGroup)).thenReturn(Collections.emptyList());

		boolean result = digitalPlacePermissionServiceImpl.hasSiteAdminAccess(mockGroup, mockPermissionChecker);

		assertFalse(result);
	}

	@Test
	public void hasControlPanelAccess_WhenPanelCategoriesAreFound_ThenReturnsTrue() throws PortalException {
		List<PanelCategory> panelCategories = new ArrayList<>();
		panelCategories.add(mockPanelCategory);
		when(PanelCategoryRegistryUtil.getChildPanelCategories(PanelCategoryKeys.ROOT, mockPermissionChecker, mockGroup)).thenReturn(panelCategories);

		boolean result = digitalPlacePermissionServiceImpl.hasSiteAdminAccess(mockGroup, mockPermissionChecker);

		assertTrue(result);
	}

}
