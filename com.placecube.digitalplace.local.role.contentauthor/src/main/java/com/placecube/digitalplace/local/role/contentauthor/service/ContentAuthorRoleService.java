package com.placecube.digitalplace.local.role.contentauthor.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.placecube.digitalplace.local.role.contentauthor.constant.ContentAuthorRole;

@Component(immediate = true, service = ContentAuthorRoleService.class)
public class ContentAuthorRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(ContentAuthorRoleService.class);

	@Reference
	private RoleLocalService roleLocalService;

	public Role getContentAuthorRole(long companyId) throws PortalException {
		return roleLocalService.getRole(companyId, ContentAuthorRole.ROLE_NAME);
	}

	public long getContentAuthorRoleId(long companyId) throws PortalException {
		return getContentAuthorRole(companyId).getRoleId();
	}

	public boolean hasRole(long userId, long companyId) {
		try {
			return roleLocalService.hasUserRole(userId, companyId, ContentAuthorRole.ROLE_NAME, true);
		} catch (PortalException e) {
			LOG.error("Unable to retrieve Content Author Role", e);
			return false;
		}
	}
}
