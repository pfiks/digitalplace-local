package com.placecube.digitalplace.local.role.contentauthor.constant;

public final class ContentAuthorRole {

	public static final String ROLE_NAME = "Content Author";

	private ContentAuthorRole() {
	}

}
