package com.placecube.digitalplace.local.role.contentauthor.lifecyclelistener;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreatorInputStreamService;

public class ContentAuthorRoleLifecycleListenerTest extends PowerMockito {

	@InjectMocks
	private ContentAuthorRoleLifecycleListener contentAuthorRoleLifecycleListener;

	@Mock
	private Company mockCompany;

	@Mock
	private RoleCreatorInputStreamService mockRoleCreatorInputStreamService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesTheRole() throws Exception {
		contentAuthorRoleLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockRoleCreatorInputStreamService, times(1)).configureMissingRoleFromInputStream(eq(mockCompany), any(InputStream.class));

	}

	@Test(expected = RoleConfigurationException.class)
	public void portalInstanceRegistered_WhenExceptionConfiguringRole_ThenThrowsException() throws Exception {
		doThrow(new RoleConfigurationException("msg")).when(mockRoleCreatorInputStreamService).configureMissingRoleFromInputStream(eq(mockCompany), any(InputStream.class));

		contentAuthorRoleLifecycleListener.portalInstanceRegistered(mockCompany);
	}

}
