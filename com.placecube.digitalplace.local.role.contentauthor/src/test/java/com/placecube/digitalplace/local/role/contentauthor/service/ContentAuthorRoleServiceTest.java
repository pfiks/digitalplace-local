package com.placecube.digitalplace.local.role.contentauthor.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.placecube.digitalplace.local.role.contentauthor.constant.ContentAuthorRole;

public class ContentAuthorRoleServiceTest extends PowerMockito {

	@InjectMocks
	private ContentAuthorRoleService contentAuthorRoleService;

	@Mock
	private Role mockRole;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void getContentAuthorRole_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, ContentAuthorRole.ROLE_NAME)).thenThrow(new PortalException());

		contentAuthorRoleService.getContentAuthorRole(companyId);
	}

	@Test
	public void getContentAuthorRole_WhenNoError_ThenReturnsTheRole() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, ContentAuthorRole.ROLE_NAME)).thenReturn(mockRole);

		Role result = contentAuthorRoleService.getContentAuthorRole(companyId);

		assertThat(result, sameInstance(mockRole));
	}

	@Test(expected = PortalException.class)
	public void getContentAuthorRoleId_WhenExceptionRetrievingTheRole_ThenThrowsPortalException() throws PortalException {
		long companyId = 1;
		when(mockRoleLocalService.getRole(companyId, ContentAuthorRole.ROLE_NAME)).thenThrow(new PortalException());

		contentAuthorRoleService.getContentAuthorRoleId(companyId);
	}

	@Test
	public void getContentAuthorRoleId_WhenNoError_ThenReturnsTheRoleId() throws PortalException {
		long companyId = 1;
		long roleId = 2;
		when(mockRoleLocalService.getRole(companyId, ContentAuthorRole.ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);

		long result = contentAuthorRoleService.getContentAuthorRoleId(companyId);

		assertThat(result, equalTo(roleId));
	}

	@Test
	public void hasRole_WheNoError_ThenCallsHasUserRole() throws PortalException {

		long userId = 1;
		long companyId = 2;

		contentAuthorRoleService.hasRole(userId, companyId);

		verify(mockRoleLocalService, times(1)).hasUserRole(userId, companyId, ContentAuthorRole.ROLE_NAME, true);
	}

	@Test
	public void hasRole_WheErrorCallsHasUserRoles_ThenReturnsFalse() throws PortalException {

		long userId = 1;
		long companyId = 2;

		when(mockRoleLocalService.hasUserRole(userId, companyId, ContentAuthorRole.ROLE_NAME, true)).thenThrow(new PortalException());

		assertFalse(contentAuthorRoleService.hasRole(userId, companyId));

	}

}
