<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ include file="/display/init.jsp" %>

<liferay-util:include page="/display/view.portal.jsp" servletContext="<%= application %>" />

<% 
if (ddmFormDisplayContext.isShowSuccessPage()) {
	try {
		String formInstanceRecordId = GetterUtil.getString(request.getSession().getAttribute("formInstanceRecordId"),"");
		request.getSession().setAttribute("formInstanceRecordId",null);
%>
		<aui:script sandbox="<%= true %>">
			const observer = new MutationObserver( function(mutationList) {
				mutationList.forEach( function(mutation) {
					if (mutation.addedNodes.length) {
						var elem = $(mutation.addedNodes[0]).find('.lfr-ddm__default-page-description');
						if (elem.length > 0) {
							elem.text(elem.text().replace("$FORM_REFERENCE$", "<%=StringUtil.replace(formInstanceRecordId, "\"", "'")%>"));
							observer.disconnect();
						}
					}
				});
			});
			var target = $('#portlet_<%= portletDisplay.getId() %>')[0];
			target = target || $('#p_p_id_com_liferay_dynamic_data_mapping_form_web_portlet_DDMFormPortlet_')[0];
			target && observer.observe(target, { childList: true, subtree: true });
		</aui:script>
<%
	} catch (Exception e) {}
}
%>