<#if cur_record?has_content>

	<#assign

		assetCategoryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")
		dynamicDataListService = digitalplace_dynamicDataListService

		full_page_url = dynamicDataListService.getViewDDLRecordURL(themeDisplay, cur_record, [$FULL_VIEW_TEMPLATE_ID$])
		venue_name = ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Name")?first, locale)
		venue_address = ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Address")?first, locale)
		venue_phone = ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Phone")?first, locale)
		venue_email = ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Email")?first, locale)
		venue_details = ddlDisplayTemplateHelper.renderRecordFieldValue(cur_record.getDDMFormFieldValues("Details")?first, locale)
		venue_image = ddlDisplayTemplateHelper.getDocumentLibraryPreviewURL(cur_record.getDDMFormFieldValues("ImageGallery")?first, locale)
		venue_categories = assetCategoryLocalService.getCategories("com.liferay.dynamic.data.lists.model.DDLRecord", cur_record.getRecordId())

	/>

	<div class="card card-horizontal card-rounded">
		<div class="card-row">
			<div class="autofit-col">
				<img class="card-item-last" src="${ venue_image }">
			</div>
			<div class="autofit-col autofit-col-expand autofit-col-gutters py-2">
				<section class="autofit-section">
					<h3 class="card-title">
						<a href="${ full_page_url }">${ venue_name }</a>
					</h3>
					<h6 class="card-subtitle mb-2">${ venue_address }</h6>
					<div class="card-divider"></div>
					<p class="card-text">${ venue_details }</p>
					<@clay["icon"] symbol="phone" /> <a href="tel:${ venue_phone }" class="card-link mr-2">${ venue_phone }</a>
					<@clay["icon"] symbol="envelope-closed" /> <a href="mailto:${ venue_email }" class="card-link">${ venue_email }</a>

					<#if venue_categories?has_content>
						<p class="card-text">
							<#list venue_categories as venue_category>
								<@clay["badge"] style="info" label="${ venue_category.getName() }"/>
							</#list>
						</p>
					</#if>

				</section>
			</div>
		</div>
	</div>

</#if>
