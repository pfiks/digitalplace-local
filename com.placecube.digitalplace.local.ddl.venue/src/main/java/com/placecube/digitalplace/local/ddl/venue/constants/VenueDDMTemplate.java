package com.placecube.digitalplace.local.ddl.venue.constants;

public enum VenueDDMTemplate {

	VENUE_LISTING("VENUE-LISTING", "Venue listing");

	private final String key;
	private final String name;

	private VenueDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
