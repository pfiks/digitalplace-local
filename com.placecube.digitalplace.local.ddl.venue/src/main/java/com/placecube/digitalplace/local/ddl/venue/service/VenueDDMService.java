package com.placecube.digitalplace.local.ddl.venue.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = VenueDDMService.class)
public class VenueDDMService {

	private static final String RESOURCE_BASE_PATH = "com/placecube/digitalplace/local/ddl/venue/dependencies/ddm/";

	private static final String STRUCTURE_KEY = "VENUE";

	private static final String TEMPLATE_KEY_VENUE_LISTING = "VENUE-LISTING";

	@Reference
	private DDMInitializer ddmInitializer;

	public void configure(ServiceContext serviceContext) throws PortalException {

		ClassLoader classLoader = getClass().getClassLoader();
		String structureResourcePath = RESOURCE_BASE_PATH + "structure.xml";

		DDMStructure ddmStructure = ddmInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, structureResourcePath, classLoader, DDLRecordSet.class, serviceContext);

		configureVenueListingDDLTemplate(classLoader, ddmStructure, serviceContext);

	}

	private void configureVenueListingDDLTemplate(ClassLoader classLoader, DDMStructure ddmStructure, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = RESOURCE_BASE_PATH + TEMPLATE_KEY_VENUE_LISTING + ".ftl";

		DDMTemplateContext ddmTemplateContext = ddmInitializer
				.getDDMTemplateContextForDDL("Venue listing", TEMPLATE_KEY_VENUE_LISTING, ddmStructure.getStructureId(), templateResourcePath, classLoader, serviceContext);

		ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}

}
