package com.placecube.digitalplace.local.ddl.venue.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

public class VenueDDMServiceTest extends PowerMockito {

	private static final long STRUCTURE_ID = 1L;

	private static final String STRUCTURE_KEY = "VENUE";

	private static final String STRUCTURE_RESOURCE_PATH = "com/placecube/digitalplace/local/ddl/venue/dependencies/ddm/structure.xml";

	private static final String VENUE_LISTING_DDL_TEMPLATE_RESOURCE_PATH = "com/placecube/digitalplace/local/ddl/venue/dependencies/ddm/VENUE-LISTING.ftl";

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private VenueDDMService venueDDMService;

	private ClassLoader venueDDMServiceClassLoader;

	@Before
	public void activateSetUp() {

		initMocks(this);

		venueDDMServiceClassLoader = VenueDDMService.class.getClassLoader();

	}

	@Test(expected = PortalException.class)
	public void configure_WhenErrorGettingOrCreatingDDMStructure_ThenThrowsException() throws Exception {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, venueDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenThrow(new PortalException());

		venueDDMService.configure(mockServiceContext);

	}

	@Test(expected = PortalException.class)
	public void configure_WhenErrorConfiguringVenueListingTemplate_ThenThrowsException() throws Exception {

		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, venueDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMInitializer.getDDMTemplateContextForDDL("Venue listing", "VENUE-LISTING", STRUCTURE_ID, VENUE_LISTING_DDL_TEMPLATE_RESOURCE_PATH, venueDDMServiceClassLoader, mockServiceContext))
				.thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		venueDDMService.configure(mockServiceContext);

	}

	@Test
	public void configure_WhenNoError_ThenCreatesTheDDMStructure() throws Exception {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, venueDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenReturn(mockDDMStructure);

		venueDDMService.configure(mockServiceContext);

		verify(mockDDMInitializer, times(1)).getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, venueDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext);

	}

	@Test
	public void configure_WhenNoError_ThenCreatesTheListingDDLTemplate() throws Exception {

		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, venueDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMInitializer.getDDMTemplateContextForDDL("Venue listing", "VENUE-LISTING", STRUCTURE_ID, VENUE_LISTING_DDL_TEMPLATE_RESOURCE_PATH, venueDDMServiceClassLoader, mockServiceContext))
				.thenReturn(mockDDMTemplateContext);

		venueDDMService.configure(mockServiceContext);

		verify(mockDDMInitializer, times(1)).getOrCreateDDMTemplate(mockDDMTemplateContext);

	}

}
