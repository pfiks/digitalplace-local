Liferay.Portlet.ready(
    function (portletId) {
		if ($('[id*="_inputPermissionsViewRole"]').length) {
			Liferay.Util.fetch(
				Liferay.ThemeDisplay.getPortalURL() + '/o/inputPermission/services/check-model-name-default-publish-permission-as-guest?groupId=' + Liferay.ThemeDisplay.getScopeGroupId() + '&portletId=' + portletId,
				{
					method: 'GET',
					headers: {
					'Content-Type':
					'text/plain'
					}
			}).then( function(response) {
				if(response.ok){
					return response.text();
				}
			}).then(function(data) {
				if (data.toString() === 'true') {
					$('select[id$="inputPermissionsViewRole"]').val('Guest');
					$('select[id$="inputPermissionsViewRole"]').trigger('change');
				}
			});
		}
	}
);
