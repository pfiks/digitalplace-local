/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for ChangeOfAddress. This utility wraps
 * <code>com.placecube.digitalplace.local.account.changeofaddress.service.impl.ChangeOfAddressLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddressLocalService
 * @generated
 */
public class ChangeOfAddressLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.local.account.changeofaddress.service.impl.ChangeOfAddressLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the change of address to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddress the change of address
	 * @return the change of address that was added
	 */
	public static ChangeOfAddress addChangeOfAddress(
		ChangeOfAddress changeOfAddress) {

		return getService().addChangeOfAddress(changeOfAddress);
	}

	public static ChangeOfAddress addChangeOfAddress(
			long groupId, long companyId, long userId,
			long formInstanceRecordId, java.util.Date addressUpdateDate)
		throws PortalException {

		return getService().addChangeOfAddress(
			groupId, companyId, userId, formInstanceRecordId,
			addressUpdateDate);
	}

	/**
	 * Creates a new change of address with the primary key. Does not add the change of address to the database.
	 *
	 * @param changeOfAddressId the primary key for the new change of address
	 * @return the new change of address
	 */
	public static ChangeOfAddress createChangeOfAddress(
		long changeOfAddressId) {

		return getService().createChangeOfAddress(changeOfAddressId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static ChangeOfAddress deleteByFormInstanceRecordId(
			long formInstanceRecordId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getService().deleteByFormInstanceRecordId(formInstanceRecordId);
	}

	/**
	 * Deletes the change of address from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddress the change of address
	 * @return the change of address that was removed
	 */
	public static ChangeOfAddress deleteChangeOfAddress(
		ChangeOfAddress changeOfAddress) {

		return getService().deleteChangeOfAddress(changeOfAddress);
	}

	/**
	 * Deletes the change of address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address that was removed
	 * @throws PortalException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress deleteChangeOfAddress(long changeOfAddressId)
		throws PortalException {

		return getService().deleteChangeOfAddress(changeOfAddressId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ChangeOfAddress fetchChangeOfAddress(long changeOfAddressId) {
		return getService().fetchChangeOfAddress(changeOfAddressId);
	}

	/**
	 * Returns the change of address matching the UUID and group.
	 *
	 * @param uuid the change of address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchChangeOfAddressByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchChangeOfAddressByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the change of address with the primary key.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address
	 * @throws PortalException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress getChangeOfAddress(long changeOfAddressId)
		throws PortalException {

		return getService().getChangeOfAddress(changeOfAddressId);
	}

	/**
	 * Returns the change of address matching the UUID and group.
	 *
	 * @param uuid the change of address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching change of address
	 * @throws PortalException if a matching change of address could not be found
	 */
	public static ChangeOfAddress getChangeOfAddressByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getChangeOfAddressByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of change of addresses
	 */
	public static List<ChangeOfAddress> getChangeOfAddresses(
		int start, int end) {

		return getService().getChangeOfAddresses(start, end);
	}

	/**
	 * Returns all the change of addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the change of addresses
	 * @param companyId the primary key of the company
	 * @return the matching change of addresses, or an empty list if no matches were found
	 */
	public static List<ChangeOfAddress> getChangeOfAddressesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getChangeOfAddressesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of change of addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the change of addresses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching change of addresses, or an empty list if no matches were found
	 */
	public static List<ChangeOfAddress> getChangeOfAddressesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getService().getChangeOfAddressesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of change of addresses.
	 *
	 * @return the number of change of addresses
	 */
	public static int getChangeOfAddressesCount() {
		return getService().getChangeOfAddressesCount();
	}

	public static List<ChangeOfAddress> getChangeOfAddressesToUpdate(
		long companyId) {

		return getService().getChangeOfAddressesToUpdate(companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the change of address in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddress the change of address
	 * @return the change of address that was updated
	 */
	public static ChangeOfAddress updateChangeOfAddress(
		ChangeOfAddress changeOfAddress) {

		return getService().updateChangeOfAddress(changeOfAddress);
	}

	public static ChangeOfAddressLocalService getService() {
		return _service;
	}

	public static void setService(ChangeOfAddressLocalService service) {
		_service = service;
	}

	private static volatile ChangeOfAddressLocalService _service;

}