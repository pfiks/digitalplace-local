/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Account_Address_ChangeOfAddress&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddress
 * @generated
 */
public class ChangeOfAddressTable extends BaseTable<ChangeOfAddressTable> {

	public static final ChangeOfAddressTable INSTANCE =
		new ChangeOfAddressTable();

	public final Column<ChangeOfAddressTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Long> changeOfAddressId =
		createColumn(
			"changeOfAddressId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<ChangeOfAddressTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Long> currentAddressId =
		createColumn(
			"currentAddressId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Long> newAddressId = createColumn(
		"newAddressId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Long> formInstanceRecordId =
		createColumn(
			"formInstanceRecordId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<ChangeOfAddressTable, Date> addressUpdateDate =
		createColumn(
			"addressUpdateDate", Date.class, Types.TIMESTAMP,
			Column.FLAG_DEFAULT);

	private ChangeOfAddressTable() {
		super(
			"Placecube_Account_Address_ChangeOfAddress",
			ChangeOfAddressTable::new);
	}

}