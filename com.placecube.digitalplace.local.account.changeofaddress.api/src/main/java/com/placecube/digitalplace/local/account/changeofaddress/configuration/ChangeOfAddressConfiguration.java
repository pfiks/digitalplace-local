package com.placecube.digitalplace.local.account.changeofaddress.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.SYSTEM)
@Meta.OCD(id = "com.placecube.digitalplace.local.account.changeofaddress.configuration.ChangeOfAddressConfiguration", localization = "content/Language", name = "change-of-address")
public interface ChangeOfAddressConfiguration {

	@Meta.AD(required = false, deflt = "0 3 0 * * ?", name = "change-of-address-scheduler-cron-expression", description = "change-of-address-scheduler-cron-expression-description")
	String schedulerCronExpression();

	@Meta.AD(required = false, deflt = "false", name = "change-of-address-scheduler-enabled")
	boolean schedulerEnabled();

}
