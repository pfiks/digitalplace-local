/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link ChangeOfAddressLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddressLocalService
 * @generated
 */
public class ChangeOfAddressLocalServiceWrapper
	implements ChangeOfAddressLocalService,
			   ServiceWrapper<ChangeOfAddressLocalService> {

	public ChangeOfAddressLocalServiceWrapper() {
		this(null);
	}

	public ChangeOfAddressLocalServiceWrapper(
		ChangeOfAddressLocalService changeOfAddressLocalService) {

		_changeOfAddressLocalService = changeOfAddressLocalService;
	}

	/**
	 * Adds the change of address to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddress the change of address
	 * @return the change of address that was added
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress addChangeOfAddress(
			com.placecube.digitalplace.local.account.changeofaddress.model.
				ChangeOfAddress changeOfAddress) {

		return _changeOfAddressLocalService.addChangeOfAddress(changeOfAddress);
	}

	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress addChangeOfAddress(
				long groupId, long companyId, long userId,
				long formInstanceRecordId, java.util.Date addressUpdateDate)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.addChangeOfAddress(
			groupId, companyId, userId, formInstanceRecordId,
			addressUpdateDate);
	}

	/**
	 * Creates a new change of address with the primary key. Does not add the change of address to the database.
	 *
	 * @param changeOfAddressId the primary key for the new change of address
	 * @return the new change of address
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress createChangeOfAddress(long changeOfAddressId) {

		return _changeOfAddressLocalService.createChangeOfAddress(
			changeOfAddressId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress deleteByFormInstanceRecordId(long formInstanceRecordId)
			throws com.placecube.digitalplace.local.account.changeofaddress.
				exception.NoSuchChangeOfAddressException {

		return _changeOfAddressLocalService.deleteByFormInstanceRecordId(
			formInstanceRecordId);
	}

	/**
	 * Deletes the change of address from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddress the change of address
	 * @return the change of address that was removed
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress deleteChangeOfAddress(
			com.placecube.digitalplace.local.account.changeofaddress.model.
				ChangeOfAddress changeOfAddress) {

		return _changeOfAddressLocalService.deleteChangeOfAddress(
			changeOfAddress);
	}

	/**
	 * Deletes the change of address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address that was removed
	 * @throws PortalException if a change of address with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress deleteChangeOfAddress(long changeOfAddressId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.deleteChangeOfAddress(
			changeOfAddressId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _changeOfAddressLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _changeOfAddressLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _changeOfAddressLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _changeOfAddressLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _changeOfAddressLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _changeOfAddressLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _changeOfAddressLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _changeOfAddressLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress fetchChangeOfAddress(long changeOfAddressId) {

		return _changeOfAddressLocalService.fetchChangeOfAddress(
			changeOfAddressId);
	}

	/**
	 * Returns the change of address matching the UUID and group.
	 *
	 * @param uuid the change of address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress fetchChangeOfAddressByUuidAndGroupId(
			String uuid, long groupId) {

		return _changeOfAddressLocalService.
			fetchChangeOfAddressByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _changeOfAddressLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the change of address with the primary key.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address
	 * @throws PortalException if a change of address with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress getChangeOfAddress(long changeOfAddressId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.getChangeOfAddress(
			changeOfAddressId);
	}

	/**
	 * Returns the change of address matching the UUID and group.
	 *
	 * @param uuid the change of address's UUID
	 * @param groupId the primary key of the group
	 * @return the matching change of address
	 * @throws PortalException if a matching change of address could not be found
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress getChangeOfAddressByUuidAndGroupId(
				String uuid, long groupId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.getChangeOfAddressByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of change of addresses
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.changeofaddress.model.
			ChangeOfAddress> getChangeOfAddresses(int start, int end) {

		return _changeOfAddressLocalService.getChangeOfAddresses(start, end);
	}

	/**
	 * Returns all the change of addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the change of addresses
	 * @param companyId the primary key of the company
	 * @return the matching change of addresses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.changeofaddress.model.
			ChangeOfAddress> getChangeOfAddressesByUuidAndCompanyId(
				String uuid, long companyId) {

		return _changeOfAddressLocalService.
			getChangeOfAddressesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of change of addresses matching the UUID and company.
	 *
	 * @param uuid the UUID of the change of addresses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching change of addresses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.changeofaddress.model.
			ChangeOfAddress> getChangeOfAddressesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.local.account.changeofaddress.
						model.ChangeOfAddress> orderByComparator) {

		return _changeOfAddressLocalService.
			getChangeOfAddressesByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of change of addresses.
	 *
	 * @return the number of change of addresses
	 */
	@Override
	public int getChangeOfAddressesCount() {
		return _changeOfAddressLocalService.getChangeOfAddressesCount();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.changeofaddress.model.
			ChangeOfAddress> getChangeOfAddressesToUpdate(long companyId) {

		return _changeOfAddressLocalService.getChangeOfAddressesToUpdate(
			companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _changeOfAddressLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _changeOfAddressLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _changeOfAddressLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _changeOfAddressLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the change of address in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ChangeOfAddressLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param changeOfAddress the change of address
	 * @return the change of address that was updated
	 */
	@Override
	public com.placecube.digitalplace.local.account.changeofaddress.model.
		ChangeOfAddress updateChangeOfAddress(
			com.placecube.digitalplace.local.account.changeofaddress.model.
				ChangeOfAddress changeOfAddress) {

		return _changeOfAddressLocalService.updateChangeOfAddress(
			changeOfAddress);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _changeOfAddressLocalService.getBasePersistence();
	}

	@Override
	public ChangeOfAddressLocalService getWrappedService() {
		return _changeOfAddressLocalService;
	}

	@Override
	public void setWrappedService(
		ChangeOfAddressLocalService changeOfAddressLocalService) {

		_changeOfAddressLocalService = changeOfAddressLocalService;
	}

	private ChangeOfAddressLocalService _changeOfAddressLocalService;

}