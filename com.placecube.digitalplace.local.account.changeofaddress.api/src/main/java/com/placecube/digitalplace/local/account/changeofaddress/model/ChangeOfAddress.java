/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the ChangeOfAddress service. Represents a row in the &quot;Placecube_Account_Address_ChangeOfAddress&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddressModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressImpl"
)
@ProviderType
public interface ChangeOfAddress extends ChangeOfAddressModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ChangeOfAddress, Long>
		CHANGE_OF_ADDRESS_ID_ACCESSOR = new Accessor<ChangeOfAddress, Long>() {

			@Override
			public Long get(ChangeOfAddress changeOfAddress) {
				return changeOfAddress.getChangeOfAddressId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<ChangeOfAddress> getTypeClass() {
				return ChangeOfAddress.class;
			}

		};

}