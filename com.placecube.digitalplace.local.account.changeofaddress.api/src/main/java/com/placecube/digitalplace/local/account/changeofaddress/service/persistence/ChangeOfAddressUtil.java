/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the change of address service. This utility wraps <code>com.placecube.digitalplace.local.account.changeofaddress.service.persistence.impl.ChangeOfAddressPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddressPersistence
 * @generated
 */
public class ChangeOfAddressUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ChangeOfAddress changeOfAddress) {
		getPersistence().clearCache(changeOfAddress);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, ChangeOfAddress> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ChangeOfAddress> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ChangeOfAddress> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ChangeOfAddress> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ChangeOfAddress update(ChangeOfAddress changeOfAddress) {
		return getPersistence().update(changeOfAddress);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ChangeOfAddress update(
		ChangeOfAddress changeOfAddress, ServiceContext serviceContext) {

		return getPersistence().update(changeOfAddress, serviceContext);
	}

	/**
	 * Returns all the change of addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByUuid_First(
			String uuid, OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByUuid_First(
		String uuid, OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByUuid_Last(
			String uuid, OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByUuid_Last(
		String uuid, OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where uuid = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress[] findByUuid_PrevAndNext(
			long changeOfAddressId, String uuid,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUuid_PrevAndNext(
			changeOfAddressId, uuid, orderByComparator);
	}

	/**
	 * Removes all the change of addresses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of change of addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching change of addresses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the change of address where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the change of address that was removed
	 */
	public static ChangeOfAddress removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of change of addresses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching change of addresses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress[] findByUuid_C_PrevAndNext(
			long changeOfAddressId, String uuid, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByUuid_C_PrevAndNext(
			changeOfAddressId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the change of addresses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the change of addresses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	public static List<ChangeOfAddress> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	 * Returns a range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end) {

		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().findByCompanyId(
			companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCompanyId(
			companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByCompanyId_First(
			long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByCompanyId_First(
			companyId, orderByComparator);
	}

	/**
	 * Returns the first change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByCompanyId_First(
		long companyId, OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByCompanyId_First(
			companyId, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByCompanyId_Last(
			long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByCompanyId_Last(
			companyId, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByCompanyId_Last(
		long companyId, OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByCompanyId_Last(
			companyId, orderByComparator);
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress[] findByCompanyId_PrevAndNext(
			long changeOfAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByCompanyId_PrevAndNext(
			changeOfAddressId, companyId, orderByComparator);
	}

	/**
	 * Removes all the change of addresses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	 * Returns the number of change of addresses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	 * Returns all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	public static List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId) {

		return getPersistence().findByNewAddressIdAndCompanyId(
			newAddressId, companyId);
	}

	/**
	 * Returns a range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end) {

		return getPersistence().findByNewAddressIdAndCompanyId(
			newAddressId, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().findByNewAddressIdAndCompanyId(
			newAddressId, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public static List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByNewAddressIdAndCompanyId(
			newAddressId, companyId, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByNewAddressIdAndCompanyId_First(
			long newAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByNewAddressIdAndCompanyId_First(
			newAddressId, companyId, orderByComparator);
	}

	/**
	 * Returns the first change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByNewAddressIdAndCompanyId_First(
		long newAddressId, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByNewAddressIdAndCompanyId_First(
			newAddressId, companyId, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByNewAddressIdAndCompanyId_Last(
			long newAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByNewAddressIdAndCompanyId_Last(
			newAddressId, companyId, orderByComparator);
	}

	/**
	 * Returns the last change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByNewAddressIdAndCompanyId_Last(
		long newAddressId, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().fetchByNewAddressIdAndCompanyId_Last(
			newAddressId, companyId, orderByComparator);
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress[] findByNewAddressIdAndCompanyId_PrevAndNext(
			long changeOfAddressId, long newAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByNewAddressIdAndCompanyId_PrevAndNext(
			changeOfAddressId, newAddressId, companyId, orderByComparator);
	}

	/**
	 * Removes all the change of addresses where newAddressId = &#63; and companyId = &#63; from the database.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 */
	public static void removeByNewAddressIdAndCompanyId(
		long newAddressId, long companyId) {

		getPersistence().removeByNewAddressIdAndCompanyId(
			newAddressId, companyId);
	}

	/**
	 * Returns the number of change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	public static int countByNewAddressIdAndCompanyId(
		long newAddressId, long companyId) {

		return getPersistence().countByNewAddressIdAndCompanyId(
			newAddressId, companyId);
	}

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public static ChangeOfAddress findByFormInstanceRecordId(
			long formInstanceRecordId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByFormInstanceRecordId(
			formInstanceRecordId);
	}

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByFormInstanceRecordId(
		long formInstanceRecordId) {

		return getPersistence().fetchByFormInstanceRecordId(
			formInstanceRecordId);
	}

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public static ChangeOfAddress fetchByFormInstanceRecordId(
		long formInstanceRecordId, boolean useFinderCache) {

		return getPersistence().fetchByFormInstanceRecordId(
			formInstanceRecordId, useFinderCache);
	}

	/**
	 * Removes the change of address where formInstanceRecordId = &#63; from the database.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the change of address that was removed
	 */
	public static ChangeOfAddress removeByFormInstanceRecordId(
			long formInstanceRecordId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().removeByFormInstanceRecordId(
			formInstanceRecordId);
	}

	/**
	 * Returns the number of change of addresses where formInstanceRecordId = &#63;.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the number of matching change of addresses
	 */
	public static int countByFormInstanceRecordId(long formInstanceRecordId) {
		return getPersistence().countByFormInstanceRecordId(
			formInstanceRecordId);
	}

	/**
	 * Caches the change of address in the entity cache if it is enabled.
	 *
	 * @param changeOfAddress the change of address
	 */
	public static void cacheResult(ChangeOfAddress changeOfAddress) {
		getPersistence().cacheResult(changeOfAddress);
	}

	/**
	 * Caches the change of addresses in the entity cache if it is enabled.
	 *
	 * @param changeOfAddresses the change of addresses
	 */
	public static void cacheResult(List<ChangeOfAddress> changeOfAddresses) {
		getPersistence().cacheResult(changeOfAddresses);
	}

	/**
	 * Creates a new change of address with the primary key. Does not add the change of address to the database.
	 *
	 * @param changeOfAddressId the primary key for the new change of address
	 * @return the new change of address
	 */
	public static ChangeOfAddress create(long changeOfAddressId) {
		return getPersistence().create(changeOfAddressId);
	}

	/**
	 * Removes the change of address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address that was removed
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress remove(long changeOfAddressId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().remove(changeOfAddressId);
	}

	public static ChangeOfAddress updateImpl(ChangeOfAddress changeOfAddress) {
		return getPersistence().updateImpl(changeOfAddress);
	}

	/**
	 * Returns the change of address with the primary key or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress findByPrimaryKey(long changeOfAddressId)
		throws com.placecube.digitalplace.local.account.changeofaddress.
			exception.NoSuchChangeOfAddressException {

		return getPersistence().findByPrimaryKey(changeOfAddressId);
	}

	/**
	 * Returns the change of address with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address, or <code>null</code> if a change of address with the primary key could not be found
	 */
	public static ChangeOfAddress fetchByPrimaryKey(long changeOfAddressId) {
		return getPersistence().fetchByPrimaryKey(changeOfAddressId);
	}

	/**
	 * Returns all the change of addresses.
	 *
	 * @return the change of addresses
	 */
	public static List<ChangeOfAddress> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of change of addresses
	 */
	public static List<ChangeOfAddress> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of change of addresses
	 */
	public static List<ChangeOfAddress> findAll(
		int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of change of addresses
	 */
	public static List<ChangeOfAddress> findAll(
		int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the change of addresses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of change of addresses.
	 *
	 * @return the number of change of addresses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ChangeOfAddressPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(ChangeOfAddressPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile ChangeOfAddressPersistence _persistence;

}