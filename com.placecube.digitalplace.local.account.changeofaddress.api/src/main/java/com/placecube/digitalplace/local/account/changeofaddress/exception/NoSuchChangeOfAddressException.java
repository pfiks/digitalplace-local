/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.placecube.digitalplace.local.account.changeofaddress.exception;

import com.liferay.portal.kernel.exception.NoSuchModelException;

/**
 * @author Brian Wing Shun Chan
 */
public class NoSuchChangeOfAddressException extends NoSuchModelException {

	public NoSuchChangeOfAddressException() {
	}

	public NoSuchChangeOfAddressException(String msg) {
		super(msg);
	}

	public NoSuchChangeOfAddressException(String msg, Throwable throwable) {
		super(msg, throwable);
	}

	public NoSuchChangeOfAddressException(Throwable throwable) {
		super(throwable);
	}

}