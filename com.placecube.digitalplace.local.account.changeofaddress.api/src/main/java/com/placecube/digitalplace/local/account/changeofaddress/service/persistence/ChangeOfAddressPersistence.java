/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.local.account.changeofaddress.exception.NoSuchChangeOfAddressException;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the change of address service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddressUtil
 * @generated
 */
@ProviderType
public interface ChangeOfAddressPersistence
	extends BasePersistence<ChangeOfAddress> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ChangeOfAddressUtil} to access the change of address persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the change of addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid(String uuid);

	/**
	 * Returns a range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where uuid = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress[] findByUuid_PrevAndNext(
			long changeOfAddressId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Removes all the change of addresses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of change of addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching change of addresses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByUUID_G(String uuid, long groupId)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the change of address where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the change of address that was removed
	 */
	public ChangeOfAddress removeByUUID_G(String uuid, long groupId)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the number of change of addresses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching change of addresses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress[] findByUuid_C_PrevAndNext(
			long changeOfAddressId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Removes all the change of addresses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the change of addresses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByCompanyId(long companyId);

	/**
	 * Returns a range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByCompanyId_First(
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the first change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the last change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByCompanyId_Last(
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the last change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress[] findByCompanyId_PrevAndNext(
			long changeOfAddressId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Removes all the change of addresses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	public void removeByCompanyId(long companyId);

	/**
	 * Returns the number of change of addresses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	public int countByCompanyId(long companyId);

	/**
	 * Returns all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId);

	/**
	 * Returns a range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	public java.util.List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByNewAddressIdAndCompanyId_First(
			long newAddressId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the first change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByNewAddressIdAndCompanyId_First(
		long newAddressId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the last change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByNewAddressIdAndCompanyId_Last(
			long newAddressId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the last change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByNewAddressIdAndCompanyId_Last(
		long newAddressId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress[] findByNewAddressIdAndCompanyId_PrevAndNext(
			long changeOfAddressId, long newAddressId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
				orderByComparator)
		throws NoSuchChangeOfAddressException;

	/**
	 * Removes all the change of addresses where newAddressId = &#63; and companyId = &#63; from the database.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 */
	public void removeByNewAddressIdAndCompanyId(
		long newAddressId, long companyId);

	/**
	 * Returns the number of change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	public int countByNewAddressIdAndCompanyId(
		long newAddressId, long companyId);

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	public ChangeOfAddress findByFormInstanceRecordId(long formInstanceRecordId)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByFormInstanceRecordId(
		long formInstanceRecordId);

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	public ChangeOfAddress fetchByFormInstanceRecordId(
		long formInstanceRecordId, boolean useFinderCache);

	/**
	 * Removes the change of address where formInstanceRecordId = &#63; from the database.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the change of address that was removed
	 */
	public ChangeOfAddress removeByFormInstanceRecordId(
			long formInstanceRecordId)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the number of change of addresses where formInstanceRecordId = &#63;.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the number of matching change of addresses
	 */
	public int countByFormInstanceRecordId(long formInstanceRecordId);

	/**
	 * Caches the change of address in the entity cache if it is enabled.
	 *
	 * @param changeOfAddress the change of address
	 */
	public void cacheResult(ChangeOfAddress changeOfAddress);

	/**
	 * Caches the change of addresses in the entity cache if it is enabled.
	 *
	 * @param changeOfAddresses the change of addresses
	 */
	public void cacheResult(java.util.List<ChangeOfAddress> changeOfAddresses);

	/**
	 * Creates a new change of address with the primary key. Does not add the change of address to the database.
	 *
	 * @param changeOfAddressId the primary key for the new change of address
	 * @return the new change of address
	 */
	public ChangeOfAddress create(long changeOfAddressId);

	/**
	 * Removes the change of address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address that was removed
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress remove(long changeOfAddressId)
		throws NoSuchChangeOfAddressException;

	public ChangeOfAddress updateImpl(ChangeOfAddress changeOfAddress);

	/**
	 * Returns the change of address with the primary key or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress findByPrimaryKey(long changeOfAddressId)
		throws NoSuchChangeOfAddressException;

	/**
	 * Returns the change of address with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address, or <code>null</code> if a change of address with the primary key could not be found
	 */
	public ChangeOfAddress fetchByPrimaryKey(long changeOfAddressId);

	/**
	 * Returns all the change of addresses.
	 *
	 * @return the change of addresses
	 */
	public java.util.List<ChangeOfAddress> findAll();

	/**
	 * Returns a range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of change of addresses
	 */
	public java.util.List<ChangeOfAddress> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of change of addresses
	 */
	public java.util.List<ChangeOfAddress> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator);

	/**
	 * Returns an ordered range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of change of addresses
	 */
	public java.util.List<ChangeOfAddress> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChangeOfAddress>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the change of addresses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of change of addresses.
	 *
	 * @return the number of change of addresses
	 */
	public int countAll();

}