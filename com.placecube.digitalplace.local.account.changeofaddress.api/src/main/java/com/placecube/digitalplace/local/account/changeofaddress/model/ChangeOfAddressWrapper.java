/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ChangeOfAddress}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddress
 * @generated
 */
public class ChangeOfAddressWrapper
	extends BaseModelWrapper<ChangeOfAddress>
	implements ChangeOfAddress, ModelWrapper<ChangeOfAddress> {

	public ChangeOfAddressWrapper(ChangeOfAddress changeOfAddress) {
		super(changeOfAddress);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("changeOfAddressId", getChangeOfAddressId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("currentAddressId", getCurrentAddressId());
		attributes.put("newAddressId", getNewAddressId());
		attributes.put("formInstanceRecordId", getFormInstanceRecordId());
		attributes.put("addressUpdateDate", getAddressUpdateDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long changeOfAddressId = (Long)attributes.get("changeOfAddressId");

		if (changeOfAddressId != null) {
			setChangeOfAddressId(changeOfAddressId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long currentAddressId = (Long)attributes.get("currentAddressId");

		if (currentAddressId != null) {
			setCurrentAddressId(currentAddressId);
		}

		Long newAddressId = (Long)attributes.get("newAddressId");

		if (newAddressId != null) {
			setNewAddressId(newAddressId);
		}

		Long formInstanceRecordId = (Long)attributes.get(
			"formInstanceRecordId");

		if (formInstanceRecordId != null) {
			setFormInstanceRecordId(formInstanceRecordId);
		}

		Date addressUpdateDate = (Date)attributes.get("addressUpdateDate");

		if (addressUpdateDate != null) {
			setAddressUpdateDate(addressUpdateDate);
		}
	}

	@Override
	public ChangeOfAddress cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the address update date of this change of address.
	 *
	 * @return the address update date of this change of address
	 */
	@Override
	public Date getAddressUpdateDate() {
		return model.getAddressUpdateDate();
	}

	/**
	 * Returns the change of address ID of this change of address.
	 *
	 * @return the change of address ID of this change of address
	 */
	@Override
	public long getChangeOfAddressId() {
		return model.getChangeOfAddressId();
	}

	/**
	 * Returns the company ID of this change of address.
	 *
	 * @return the company ID of this change of address
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this change of address.
	 *
	 * @return the create date of this change of address
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the current address ID of this change of address.
	 *
	 * @return the current address ID of this change of address
	 */
	@Override
	public long getCurrentAddressId() {
		return model.getCurrentAddressId();
	}

	/**
	 * Returns the form instance record ID of this change of address.
	 *
	 * @return the form instance record ID of this change of address
	 */
	@Override
	public long getFormInstanceRecordId() {
		return model.getFormInstanceRecordId();
	}

	/**
	 * Returns the group ID of this change of address.
	 *
	 * @return the group ID of this change of address
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this change of address.
	 *
	 * @return the modified date of this change of address
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the new address ID of this change of address.
	 *
	 * @return the new address ID of this change of address
	 */
	@Override
	public long getNewAddressId() {
		return model.getNewAddressId();
	}

	/**
	 * Returns the primary key of this change of address.
	 *
	 * @return the primary key of this change of address
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this change of address.
	 *
	 * @return the user ID of this change of address
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this change of address.
	 *
	 * @return the user name of this change of address
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this change of address.
	 *
	 * @return the user uuid of this change of address
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this change of address.
	 *
	 * @return the uuid of this change of address
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the address update date of this change of address.
	 *
	 * @param addressUpdateDate the address update date of this change of address
	 */
	@Override
	public void setAddressUpdateDate(Date addressUpdateDate) {
		model.setAddressUpdateDate(addressUpdateDate);
	}

	/**
	 * Sets the change of address ID of this change of address.
	 *
	 * @param changeOfAddressId the change of address ID of this change of address
	 */
	@Override
	public void setChangeOfAddressId(long changeOfAddressId) {
		model.setChangeOfAddressId(changeOfAddressId);
	}

	/**
	 * Sets the company ID of this change of address.
	 *
	 * @param companyId the company ID of this change of address
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this change of address.
	 *
	 * @param createDate the create date of this change of address
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the current address ID of this change of address.
	 *
	 * @param currentAddressId the current address ID of this change of address
	 */
	@Override
	public void setCurrentAddressId(long currentAddressId) {
		model.setCurrentAddressId(currentAddressId);
	}

	/**
	 * Sets the form instance record ID of this change of address.
	 *
	 * @param formInstanceRecordId the form instance record ID of this change of address
	 */
	@Override
	public void setFormInstanceRecordId(long formInstanceRecordId) {
		model.setFormInstanceRecordId(formInstanceRecordId);
	}

	/**
	 * Sets the group ID of this change of address.
	 *
	 * @param groupId the group ID of this change of address
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this change of address.
	 *
	 * @param modifiedDate the modified date of this change of address
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the new address ID of this change of address.
	 *
	 * @param newAddressId the new address ID of this change of address
	 */
	@Override
	public void setNewAddressId(long newAddressId) {
		model.setNewAddressId(newAddressId);
	}

	/**
	 * Sets the primary key of this change of address.
	 *
	 * @param primaryKey the primary key of this change of address
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this change of address.
	 *
	 * @param userId the user ID of this change of address
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this change of address.
	 *
	 * @param userName the user name of this change of address
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this change of address.
	 *
	 * @param userUuid the user uuid of this change of address
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this change of address.
	 *
	 * @param uuid the uuid of this change of address
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected ChangeOfAddressWrapper wrap(ChangeOfAddress changeOfAddress) {
		return new ChangeOfAddressWrapper(changeOfAddress);
	}

}