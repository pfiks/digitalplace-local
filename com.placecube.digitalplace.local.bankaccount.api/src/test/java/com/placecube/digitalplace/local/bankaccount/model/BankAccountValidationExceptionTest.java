package com.placecube.digitalplace.local.bankaccount.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;

import org.junit.Test;

public class BankAccountValidationExceptionTest {

	private static final String MESSAGE = "message";

	@Test
	public void new_WhenCreatedWithMessage_ThenSetsMessageProperty() {
		BankAccountValidationException exception = new BankAccountValidationException(MESSAGE);

		assertThat(exception.getMessage(), equalTo(MESSAGE));
	}

	@Test
	public void new_WhenCreatedWithException_ThenSetsExceptionProperty() {
		final Exception causingException = new Exception();
		BankAccountValidationException exception = new BankAccountValidationException(causingException);

		assertThat(exception.getCause(), sameInstance(causingException));
	}

	@Test
	public void new_WhenCreatedWithMessageAndException_ThenSetsMessageAndExceptionProperties() {
		final Exception causingException = new Exception();
		BankAccountValidationException exception = new BankAccountValidationException(MESSAGE, causingException);

		assertThat(exception.getMessage(), equalTo(MESSAGE));
		assertThat(exception.getCause(), sameInstance(causingException));
	}

}
