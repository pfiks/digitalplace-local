package com.placecube.digitalplace.local.bankaccount.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationRequestException;

import org.junit.Test;

public class BankAccountValidationRequestExceptionTest {

	@Test
	public void new_WhenCreatedWithMessage_ThenSetsMessageProperty() {
		final String message = "message";
		BankAccountValidationRequestException exception = new BankAccountValidationRequestException(message);

		assertThat(exception.getMessage(), equalTo(message));
	}

	@Test
	public void new_WhenCreatedWithException_ThenSetsExceptionProperty() {
		final Exception causingException = new Exception();
		BankAccountValidationRequestException exception = new BankAccountValidationRequestException(causingException);

		assertThat(exception.getCause(), sameInstance(causingException));
	}

}
