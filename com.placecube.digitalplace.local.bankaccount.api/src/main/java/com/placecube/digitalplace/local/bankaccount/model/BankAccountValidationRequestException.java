package com.placecube.digitalplace.local.bankaccount.model;

public class BankAccountValidationRequestException extends BankAccountValidationException {

	/**
	 *
	 */
	private static final long serialVersionUID = -7962346498524121471L;

	public BankAccountValidationRequestException(Exception e) {
		super(e);
	}

	public BankAccountValidationRequestException(String message) {
		super(message);
	}

}
