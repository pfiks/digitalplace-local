package com.placecube.digitalplace.local.bankaccount.model;

public class BankAccountValidationServiceException extends BankAccountValidationException {

	public BankAccountValidationServiceException(Exception e) {
		super(e);
	}

	public BankAccountValidationServiceException(String message) {
		super(message);
	}

	public BankAccountValidationServiceException(String message, Exception e) {
		super(message, e);
	}

}
