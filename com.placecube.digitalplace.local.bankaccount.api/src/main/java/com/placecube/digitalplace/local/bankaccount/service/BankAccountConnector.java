package com.placecube.digitalplace.local.bankaccount.service;

import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationResponse;
import com.placecube.digitalplace.local.connector.model.Connector;

public interface BankAccountConnector extends Connector {

	BankAccountValidationResponse validateBankAccount(long companyId, String accountNumber, String sortCode) throws BankAccountValidationException;

}
