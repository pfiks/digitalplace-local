package com.placecube.digitalplace.local.bankaccount.service;

import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationResponse;
import com.placecube.digitalplace.local.connector.model.ConnectorContext;

public interface BankAccountValidationService {

	BankAccountValidationResponse validateBankAccount(ConnectorContext connectorContext, String accountNumber, String sortCode) throws BankAccountValidationException;
}