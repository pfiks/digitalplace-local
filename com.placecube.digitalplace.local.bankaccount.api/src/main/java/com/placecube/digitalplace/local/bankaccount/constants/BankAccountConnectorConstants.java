package com.placecube.digitalplace.local.bankaccount.constants;

public class BankAccountConnectorConstants {

	public static final String BANK_ACCOUNT_CONNECTOR_TYPE = "BankAccountConnector";

	private BankAccountConnectorConstants() {

	}
}
