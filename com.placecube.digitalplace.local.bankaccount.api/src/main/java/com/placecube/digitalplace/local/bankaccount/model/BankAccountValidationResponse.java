package com.placecube.digitalplace.local.bankaccount.model;

public enum BankAccountValidationResponse {

	INVALID_ACCOUNT, VALID;
}
