package com.placecube.digitalplace.local.bankaccount.model;

public class BankAccountValidationException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 4761415061861218011L;

	public BankAccountValidationException(Exception e) {
		super(e);
	}

	public BankAccountValidationException(String message) {
		super(message);
	}

	public BankAccountValidationException(String message, Exception e) {
		super(message, e);
	}

}
