package com.placecube.digitalplace.local.role.webadministrator.internal.upgrade.upgrade_1_0_0;

import java.util.Arrays;
import java.util.List;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.role.exception.RolePermissionException;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.local.role.webadministrator.constant.WebAdministratorRole;

public class DDLPortletsRoleUpdate extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final RoleLocalService roleLocalService;

	private final RolePermissionService rolePermissionService;

	public DDLPortletsRoleUpdate(CompanyLocalService companyLocalService, RoleLocalService roleLocalService, RolePermissionService rolePermissionService) {
		this.companyLocalService = companyLocalService;
		this.roleLocalService = roleLocalService;
		this.rolePermissionService = rolePermissionService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		List<String> portletList = Arrays.asList("com_placecube_digitalplace_ddl_portlet_DDLDisplayPortlet", "com_placecube_digitalplace_ddl_portlet_DDLFieldFacetPortlet", "com_placecube_digitalplace_ddl_portlet_DDLFieldFilterPortlet", "com_placecube_digitalplace_ddl_portlet_DDLLocationSearchPortlet", "com_placecube_digitalplace_ddl_portlet_DDLSearchListingsPortlet");
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId, portletList);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId(), portletList);
			}
		}
	}

	private void upgradeCompany(long companyId, List<String> portletList) throws RolePermissionException {

		for (String portletName : portletList) {
			if (Validator.isNotNull(roleLocalService.fetchRole(companyId, WebAdministratorRole.ROLE_NAME))) {
				rolePermissionService.addPermissionForRole(companyId, WebAdministratorRole.ROLE_NAME, portletName, 1,
						String.valueOf(companyId), "CONFIGURATION");
				rolePermissionService.addPermissionForRole(companyId, WebAdministratorRole.ROLE_NAME, portletName, 1,
						String.valueOf(companyId), "PERMISSIONS");
			}
		}
	}
}
