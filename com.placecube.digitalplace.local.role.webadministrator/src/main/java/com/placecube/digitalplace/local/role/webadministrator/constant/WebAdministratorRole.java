package com.placecube.digitalplace.local.role.webadministrator.constant;

public final class WebAdministratorRole {

	public static final String ROLE_NAME = "Web Administrator";

	private WebAdministratorRole() {
	}

}
