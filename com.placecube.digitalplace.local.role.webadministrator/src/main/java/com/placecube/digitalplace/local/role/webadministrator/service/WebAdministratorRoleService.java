package com.placecube.digitalplace.local.role.webadministrator.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.placecube.digitalplace.local.role.webadministrator.constant.WebAdministratorRole;

@Component(immediate = true, service = WebAdministratorRoleService.class)
public class WebAdministratorRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(WebAdministratorRoleService.class);

	@Reference
	private RoleLocalService roleLocalService;

	public Role getWebAdministratorRole(long companyId) throws PortalException {
		return roleLocalService.getRole(companyId, WebAdministratorRole.ROLE_NAME);
	}

	public long getWebAdministratorRoleId(long companyId) throws PortalException {
		return getWebAdministratorRole(companyId).getRoleId();
	}

	public boolean hasRole(long userId, long companyId) {
		try {
			return roleLocalService.hasUserRole(userId, companyId, WebAdministratorRole.ROLE_NAME, true);
		} catch (PortalException e) {
			LOG.error("Unable to retrieve Web Administrator Role", e);
			return false;
		}
	}
}
