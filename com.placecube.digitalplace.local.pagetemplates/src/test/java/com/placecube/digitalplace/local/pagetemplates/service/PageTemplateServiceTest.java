package com.placecube.digitalplace.local.pagetemplates.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.local.pagetemplates.constants.DigitalPlacePageTemplateKey;

public class PageTemplateServiceTest extends PowerMockito {

	private static final long GROUP_ID = 123456;

	@InjectMocks
	private PageTemplateService pageTemplateService;

	@Mock
	private DigitalPlaceInitializerService mockDigitalPlaceInitializerService;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void getOrCreatePageTemplate_WhenErrorOnInitializingPageTemplate_ThenThrowsPortalException() throws Exception {
		DigitalPlacePageTemplateKey digitalPlacePageTemplateKey = DigitalPlacePageTemplateKey.FORM_PAGE;
		String jsonPath = "com/placecube/digitalplace/local/pagetemplates/dependencies/pagetemplates/" + digitalPlacePageTemplateKey.getValue() + ".json";
		Map<String, String> prefs = new HashMap<>();
		prefs.put("${PLACEHOLDER_GROUP_ID}", String.valueOf(GROUP_ID));

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockDigitalPlaceInitializerService.initializePageTemplate(PageTemplateService.class, jsonPath, prefs, mockServiceContext)).thenThrow(new PortalException());

		pageTemplateService.getOrCreatePageTemplate(digitalPlacePageTemplateKey, mockServiceContext);
	}

	@Test()
	public void getOrCreatePageTemplate_WhenNoError_ThenReturnsPageTemplate() throws Exception {
		DigitalPlacePageTemplateKey digitalPlacePageTemplateKey = DigitalPlacePageTemplateKey.FORM_PAGE;
		String jsonPath = "com/placecube/digitalplace/local/pagetemplates/dependencies/pagetemplates/" + digitalPlacePageTemplateKey.getValue() + ".json";
		Map<String, String> prefs = new HashMap<>();
		prefs.put("${PLACEHOLDER_GROUP_ID}", String.valueOf(GROUP_ID));

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockDigitalPlaceInitializerService.initializePageTemplate(PageTemplateService.class, jsonPath, prefs, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = pageTemplateService.getOrCreatePageTemplate(digitalPlacePageTemplateKey, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateEntry));
	}
}