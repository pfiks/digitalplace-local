package com.placecube.digitalplace.local.pagetemplates.constants;

public enum DigitalPlacePageTemplateKey {

	ARTICLE_PAGE("article-page"),

	CATEGORY_PAGE("category-page"),

	FORM_CONFIRMATION_PAGE("form-confirmation-page"),

	FORM_ERROR_PAYMENT_PAGE("form-error-payment-page"),

	FORM_PAGE("form-page"),

	GUIDE_PAGE("guide-page"),

	WEBDOCS_PAGE("webdocs-page");

	private String value;

	DigitalPlacePageTemplateKey(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
