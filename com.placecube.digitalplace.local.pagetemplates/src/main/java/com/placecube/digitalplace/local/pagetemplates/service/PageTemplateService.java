package com.placecube.digitalplace.local.pagetemplates.service;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.local.pagetemplates.constants.DigitalPlacePageTemplateKey;

@Component(immediate = true, service = PageTemplateService.class)
public class PageTemplateService {

	public static final String GROUP_ID_PLACEHOLDER = "${PLACEHOLDER_GROUP_ID}";

	@Reference
	private DigitalPlaceInitializerService digitalPlaceInitializerService;

	public LayoutPageTemplateEntry getOrCreatePageTemplate(DigitalPlacePageTemplateKey digitalPlacePageTemplateKey, ServiceContext serviceContext) throws PortalException {
		String jsonPath = "com/placecube/digitalplace/local/pagetemplates/dependencies/pagetemplates/" + digitalPlacePageTemplateKey.getValue() + ".json";
		return digitalPlaceInitializerService.initializePageTemplate(getClass(), jsonPath, getPreferencesPlaceholders(serviceContext), serviceContext);
	}


	private Map<String, String> getPreferencesPlaceholders(ServiceContext serviceContext) {
		Map<String, String> results = new HashMap<>();
		results.put(GROUP_ID_PLACEHOLDER, String.valueOf(serviceContext.getScopeGroupId()));
		return results;
	}

}
