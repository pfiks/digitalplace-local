package com.placecube.digitalplace.local.ddmform.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrieval;

public class FormInstanceRecordIdRetrievalRegistryImplTest extends PowerMockito {

	private FormInstanceRecordIdRetrievalRegistryImpl formInstanceRecordIdRetrievalRegistryImpl;

	@Mock
	private DDMFormInstanceRecord mockFormInstanceRecord;

	@Mock
	private FormInstanceRecordIdRetrieval mockFormInstanceRecordIdRetrieval;

	@Mock
	private FormInstanceRecordIdRetrieval mockFormInstanceRecordIdRetrieval2;

	@Before
	public void setUp() {
		initMocks(this);
		formInstanceRecordIdRetrievalRegistryImpl = new FormInstanceRecordIdRetrievalRegistryImpl();
	}

	@Test
	public void getRetrieval_WhenIsNotEnabled_ThenReturnsEmptyOptional() {

		when(mockFormInstanceRecordIdRetrieval.isEnabled(mockFormInstanceRecord)).thenReturn(false);
		formInstanceRecordIdRetrievalRegistryImpl.registerFormInstanceRecordIdRetrieval(mockFormInstanceRecordIdRetrieval);

		Optional<FormInstanceRecordIdRetrieval> result = formInstanceRecordIdRetrievalRegistryImpl.getRetrieval(mockFormInstanceRecord);

		assertFalse(result.isPresent());

	}

	@Test
	public void getRetrieval_WhenIsEnabled_ThenReturnsOptionalWithTheFormInstanceRecordIdRetrieval() {

		when(mockFormInstanceRecordIdRetrieval.isEnabled(mockFormInstanceRecord)).thenReturn(true);
		formInstanceRecordIdRetrievalRegistryImpl.registerFormInstanceRecordIdRetrieval(mockFormInstanceRecordIdRetrieval);

		Optional<FormInstanceRecordIdRetrieval> result = formInstanceRecordIdRetrievalRegistryImpl.getRetrieval(mockFormInstanceRecord);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockFormInstanceRecordIdRetrieval));

	}

	@Test
	public void registerFormInstanceRecordIdRetrieval_WhenNoError_ThenFormInstanceRecordIdRetrievalIsRegistered() {

		when(mockFormInstanceRecordIdRetrieval.isEnabled(mockFormInstanceRecord)).thenReturn(true);

		formInstanceRecordIdRetrievalRegistryImpl.registerFormInstanceRecordIdRetrieval(mockFormInstanceRecordIdRetrieval);

		Optional<FormInstanceRecordIdRetrieval> result = formInstanceRecordIdRetrievalRegistryImpl.getRetrieval(mockFormInstanceRecord);

		assertThat(result.get(), sameInstance(mockFormInstanceRecordIdRetrieval));
	}

	@Test
	public void unregisterFormInstanceRecordIdRetrieval_WhenNoError_ThenFormInstanceRecordIdRetrievalIsUnregistered() {

		formInstanceRecordIdRetrievalRegistryImpl.registerFormInstanceRecordIdRetrieval(mockFormInstanceRecordIdRetrieval);
		formInstanceRecordIdRetrievalRegistryImpl.registerFormInstanceRecordIdRetrieval(mockFormInstanceRecordIdRetrieval2);

		formInstanceRecordIdRetrievalRegistryImpl.unregisterFormInstanceRecordIdRetrieval(mockFormInstanceRecordIdRetrieval2);

		Optional<FormInstanceRecordIdRetrieval> result = formInstanceRecordIdRetrievalRegistryImpl.getRetrieval(mockFormInstanceRecord);

		assertFalse(result.isPresent());
	}
}
