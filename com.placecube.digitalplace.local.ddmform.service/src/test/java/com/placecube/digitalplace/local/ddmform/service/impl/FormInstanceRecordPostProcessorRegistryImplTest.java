package com.placecube.digitalplace.local.ddmform.service.impl;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessor;

public class FormInstanceRecordPostProcessorRegistryImplTest extends PowerMockito {

	private FormInstanceRecordPostProcessorRegistryImpl formInstanceRecordPostProcessorRegistryImpl;

	@Mock
	private FormInstanceRecordPostProcessor mockFormInstanceRecordPostProcessor;

	@Mock
	private FormInstanceRecordPostProcessor mockFormInstanceRecordPostProcessor2;

	@Before
	public void setUp() {
		initMocks(this);
		formInstanceRecordPostProcessorRegistryImpl = new FormInstanceRecordPostProcessorRegistryImpl();
	}

	@Test
	public void getPostProcessors_WhenNoError_ThenReturnsPostProcessorsRegistered() {

		formInstanceRecordPostProcessorRegistryImpl.registerFormInstanceRecordPostProcessor(mockFormInstanceRecordPostProcessor);

		List<FormInstanceRecordPostProcessor> result = formInstanceRecordPostProcessorRegistryImpl.getPostProcessors();

		assertThat(result, contains(mockFormInstanceRecordPostProcessor));

	}

	@Test
	public void registerDDMFormInstanceRecordPostProcessor_WhenNoError_ThenPostProcessorIsRegistered() {

		formInstanceRecordPostProcessorRegistryImpl.registerFormInstanceRecordPostProcessor(mockFormInstanceRecordPostProcessor);

		List<FormInstanceRecordPostProcessor> result = formInstanceRecordPostProcessorRegistryImpl.getPostProcessors();

		assertThat(result.get(0), sameInstance(mockFormInstanceRecordPostProcessor));
	}

	@Test
	public void unregisterDDMFormInstanceRecordPostProcessor_WhenNoError_ThenPostProcessorIsUnregistered() {

		formInstanceRecordPostProcessorRegistryImpl.registerFormInstanceRecordPostProcessor(mockFormInstanceRecordPostProcessor);
		formInstanceRecordPostProcessorRegistryImpl.registerFormInstanceRecordPostProcessor(mockFormInstanceRecordPostProcessor2);

		formInstanceRecordPostProcessorRegistryImpl.unregisterFormInstanceRecordPostProcessor(mockFormInstanceRecordPostProcessor);

		List<FormInstanceRecordPostProcessor> result = formInstanceRecordPostProcessorRegistryImpl.getPostProcessors();

		assertThat(result, not(contains(mockFormInstanceRecordPostProcessor)));
		assertThat(result, contains(mockFormInstanceRecordPostProcessor2));

	}
}
