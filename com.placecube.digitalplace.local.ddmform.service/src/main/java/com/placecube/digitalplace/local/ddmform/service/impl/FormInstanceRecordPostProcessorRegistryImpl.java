package com.placecube.digitalplace.local.ddmform.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessor;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordPostProcessorRegistry;

@Component(immediate = true, service = FormInstanceRecordPostProcessorRegistry.class)
public class FormInstanceRecordPostProcessorRegistryImpl implements FormInstanceRecordPostProcessorRegistry {

	private List<FormInstanceRecordPostProcessor> postProcessors = new LinkedList<>();

	@Override
	public List<FormInstanceRecordPostProcessor> getPostProcessors() {
		return postProcessors;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, unbind = "unregisterFormInstanceRecordPostProcessor")
	public void registerFormInstanceRecordPostProcessor(FormInstanceRecordPostProcessor formInstanceRecordPostProcessor) {
		postProcessors.add(formInstanceRecordPostProcessor);
	}

	public void unregisterFormInstanceRecordPostProcessor(FormInstanceRecordPostProcessor formInstanceRecordPostProcessor) {
		postProcessors.remove(formInstanceRecordPostProcessor);
	}

}
