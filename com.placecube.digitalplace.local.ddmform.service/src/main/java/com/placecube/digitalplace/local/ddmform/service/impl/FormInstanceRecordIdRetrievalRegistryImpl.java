package com.placecube.digitalplace.local.ddmform.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrieval;
import com.placecube.digitalplace.local.ddmform.service.FormInstanceRecordIdRetrievalRegistry;

@Component(immediate = true, service = FormInstanceRecordIdRetrievalRegistry.class)
public class FormInstanceRecordIdRetrievalRegistryImpl implements FormInstanceRecordIdRetrievalRegistry {

	private List<FormInstanceRecordIdRetrieval> retrievalIds = new LinkedList<>();

	@Override
	public Optional<FormInstanceRecordIdRetrieval> getRetrieval(DDMFormInstanceRecord formInstanceRecord) {

		return retrievalIds.stream().filter(r -> r.isEnabled(formInstanceRecord)).findFirst();

	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, unbind = "unregisterFormInstanceRecordIdRetrieval")
	public void registerFormInstanceRecordIdRetrieval(FormInstanceRecordIdRetrieval formInstanceRecordIdRetrieval) {
		retrievalIds.add(formInstanceRecordIdRetrieval);
	}

	public void unregisterFormInstanceRecordIdRetrieval(FormInstanceRecordIdRetrieval formInstanceRecordIdRetrieval) {
		retrievalIds.remove(formInstanceRecordIdRetrieval);
	}

}
