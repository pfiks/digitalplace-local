package com.placecube.digitalplace.local.connector.service;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.placecube.digitalplace.local.connector.model.Connector;
import com.placecube.digitalplace.local.connector.model.ConnectorRegistryContext;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public interface ConnectorRegistryService {

	ConnectorRegistryContext createConnectorRegistryContext(String connectorType, String connectorClassName);

	Optional<ServiceWrapper<Connector>> getConnector(ConnectorRegistryContext connectorRegistryContext);

	List<ServiceWrapper<Connector>> getConnectors(String interfaceClassName);

	Map<String, String> getConnectorNames(String interfaceClassName, Locale locale);
}
