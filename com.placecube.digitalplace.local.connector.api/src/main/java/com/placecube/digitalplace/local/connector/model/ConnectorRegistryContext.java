package com.placecube.digitalplace.local.connector.model;

public class ConnectorRegistryContext {

	private String connectorType;

	private String connectorClassName;

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public String getConnectorClassName() {
		return connectorClassName;
	}

	public void setConnectorClassName(String connectorClassName) {
		this.connectorClassName = connectorClassName;
	}

}
