package com.placecube.digitalplace.local.connector.constants;

/**
 * @author Ruben Lopez Seone
 */
public class ConnectorConstants {

	public static final String CONNECTOR_DISPLAY_PRIORITY = "connector.display.priority";

	public static final String CONNECTOR_NAME = "connector.name";

	public static final String CONNECTOR_TYPE = "connector.type";

	private ConnectorConstants() {

	}
}