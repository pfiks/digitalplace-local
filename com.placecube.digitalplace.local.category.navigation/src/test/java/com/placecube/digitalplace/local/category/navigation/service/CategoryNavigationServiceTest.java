package com.placecube.digitalplace.local.category.navigation.service;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

import com.liferay.asset.category.property.model.AssetCategoryProperty;
import com.liferay.asset.category.property.service.AssetCategoryPropertyLocalService;
import com.liferay.asset.entry.rel.model.AssetEntryAssetCategoryRel;
import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.list.asset.entry.provider.AssetListAssetEntryProvider;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.info.pagination.InfoPage;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.segments.constants.SegmentsEntryConstants;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationContentSet;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationProperty;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;
import com.placecube.digitalplace.local.category.navigation.model.CategoryLinkWrapper;
import com.placecube.digitalplace.local.category.navigation.model.PageAndWebContentLinkWrapper;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CategoryLinkWrapper.class, PageAndWebContentLinkWrapper.class, StringUtil.class })
public class CategoryNavigationServiceTest extends PowerMockito {

	public static final long CATEGORY_ID = 33;
	public static final long GROUP_ID = 13;
	public static final String JOURNAL_TYPE = "journal";
	public static final String LAYOUT_TYPE = "layout";
	public static final long LAYOUT_ID = 1;
	public static final long LAYOUT_ID_2 = 2;
	private static final Locale LOCALE = LocaleUtil.UK;
	
	@InjectMocks
	private CategoryNavigationService categoryNavigationService;

	private ClassLoader categoryNavigationServiceClassLoader;

	@Mock
	private AssetEntryAssetCategoryRelLocalService mockAssetEntryAssetCategoryRelLocalService;

	@Mock
	private AssetCategoryPropertyLocalService mockAssetCategoryPropertyLocalService;

	@Mock
	private AssetListAssetEntryProvider mockAssetListAssetEntryProvider;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private AssetListEntryInitializer mockAssetListEntryInitializer;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;
	
	@Mock
	private AssetEntryAssetCategoryRel mockAssetEntryAssetCategoryRel;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategoryProperty mockAssetCategoryProperty;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private AssetEntry mockAssetEntry1;

	@Mock
	private AssetEntry mockAssetEntry2;

	@Mock
	private AssetEntry mockAssetEntry3;

	@Mock
	private AssetEntry mockAssetEntry4;

	@Mock
	private CategoryLinkWrapper mockCategoryLinkWrapper1;

	@Mock
	private CategoryLinkWrapper mockCategoryLinkWrapper2;

	@Mock
	private CategoryLinkWrapper mockCategoryLinkWrapper3;
	
	@Mock
	private CategoryLinkWrapper mockCategoryLinkWrapper4;

	@Mock
	private InfoPage mockAssetEntriesInfoPage;
	
	@Mock
	private PageAndWebContentLinkWrapper mockPageAndWebContentLinkWrapper1;

	@Mock
	private PageAndWebContentLinkWrapper mockPageAndWebContentLinkWrapper2;

	@Mock
	private NavItem mockNavItem1;

	@Mock
	private NavItem mockNavItem2;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private SiteNavigationMenuItemLocalService mockSiteNavigationMenuItemLocalService;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItem;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItem2;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleService mockJournalArticleService;

	@Before
	public void setUp() {
		PowerMockito.mockStatic(CategoryLinkWrapper.class, PageAndWebContentLinkWrapper.class);
		categoryNavigationServiceClassLoader = CategoryNavigationService.class.getClassLoader();
	}

	@Test
	public void getLandingPageForCategory_WhenExceptionRetrievingTheCategoryProperty_ThenReturnsEmptyOptional() throws PortalException {
		long categoryId = 1;
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		when(mockAssetCategoryPropertyLocalService.getCategoryProperty(categoryId, CategoryNavigationProperty.LANDING_PAGE.getKey())).thenThrow(new PortalException());

		Optional<String> result = categoryNavigationService.getLandingPageForCategory(mockAssetCategory);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLandingPageForCategory_WhenNoError_ThenReturnsOptionalWithCategoryPropertyValue() throws PortalException {
		long categoryId = 1;
		String expected = "expectedValue";
		when(mockAssetCategory.getCategoryId()).thenReturn(categoryId);
		when(mockAssetCategoryPropertyLocalService.getCategoryProperty(categoryId, CategoryNavigationProperty.LANDING_PAGE.getKey())).thenReturn(mockAssetCategoryProperty);
		when(mockAssetCategoryProperty.getValue()).thenReturn(expected);

		Optional<String> result = categoryNavigationService.getLandingPageForCategory(mockAssetCategory);

		assertThat(result.orElseThrow(IllegalStateException::new), equalTo(StringPool.FORWARD_SLASH + expected));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateContentSet_WhenException_ThenThrowsPortalException() throws Exception {
		when(mockAssetListEntryInitializer.getOrCreateContentSet(CategoryNavigationContentSet.SERVICE_MENU.getName(), true, mockServiceContext)).thenThrow(new PortalException());

		categoryNavigationService.getOrCreateContentSet(CategoryNavigationContentSet.SERVICE_MENU, mockServiceContext);
	}

	@Test
	public void getOrCreateContentSet_WhenNoError_ThenReturnsTheContentSet() throws Exception {
		when(mockAssetListEntryInitializer.getOrCreateContentSet(CategoryNavigationContentSet.SERVICE_MENU.getName(), true, mockServiceContext)).thenReturn(mockAssetListEntry);

		AssetListEntry result = categoryNavigationService.getOrCreateContentSet(CategoryNavigationContentSet.SERVICE_MENU, mockServiceContext);

		assertThat(result, sameInstance(mockAssetListEntry));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenExceptionCreatingTheTemplate_ThenThrowsPortalException() throws Exception {

		CategoryNavigationWidgetTemplate widgetTemplate = CategoryNavigationWidgetTemplate.SERVICE_MENU;
		String templateResourcePath = "com/placecube/digitalplace/local/category/navigation/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetCategory.class, templateResourcePath,
				categoryNavigationServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		categoryNavigationService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		CategoryNavigationWidgetTemplate widgetTemplate = CategoryNavigationWidgetTemplate.SERVICE_MENU;
		String templateResourcePath = "com/placecube/digitalplace/local/category/navigation/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetCategory.class, templateResourcePath,
				categoryNavigationServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate result = categoryNavigationService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

		assertThat(result, sameInstance(mockDDMTemplate));

	}

	@Test
	public void getServiceMenuAssetEntriesByCategory_WhenContentSetNotFound_ThenReturnsEmptyList() {
		Long groupId = 11L;
		when(mockAssetCategory.getGroupId()).thenReturn(groupId);
		when(mockAssetListEntryInitializer.getContentSet(groupId, CategoryNavigationContentSet.SERVICE_MENU.getName())).thenReturn(Optional.empty());

		List<AssetEntry> results = categoryNavigationService.getServiceMenuAssetEntriesByCategory(mockAssetCategory, mockPermissionChecker);

		assertThat(results, empty());
	}

	@Test
	public void getServiceMenuAssetEntriesByCategory_WhenContentSetFound_ThenReturnsListWithAssetsWithMathingCategoryPropertyAndPermissions() {
		Long groupId = 11L;
		List<AssetEntry> assetEntries = new ArrayList<>();
		when(mockAssetCategory.getGroupId()).thenReturn(groupId);
		when(mockAssetCategory.getCategoryId()).thenReturn(CATEGORY_ID);
		when(mockAssetListEntryInitializer.getContentSet(groupId, CategoryNavigationContentSet.SERVICE_MENU.getName())).thenReturn(Optional.of(mockAssetListEntry));
		when(mockAssetListAssetEntryProvider.getAssetEntriesInfoPage(mockAssetListEntry, new long[] {SegmentsEntryConstants.ID_DEFAULT}, null, null, StringPool.BLANK, StringPool.BLANK,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS)).thenReturn(mockAssetEntriesInfoPage);
		when(mockAssetEntriesInfoPage.getPageItems()).thenReturn(assetEntries);
		
		mockAssetEntryCategoryDetails(mockAssetEntry1, 44, 77, true, false, assetEntries);
		mockAssetEntryCategoryDetails(mockAssetEntry2, 55, 88, false, true, assetEntries);
		mockAssetEntryCategoryDetails(mockAssetEntry3, 66, 99, true, true, assetEntries);
		mockAssetEntryCategoryDetails(mockAssetEntry4, 11, 22, false, false, assetEntries);

		List<AssetEntry> results = categoryNavigationService.getServiceMenuAssetEntriesByCategory(mockAssetCategory, mockPermissionChecker);

		assertThat(results, containsInAnyOrder(mockAssetEntry3));
	}

	@Test
	public void getAssetEntriesAndMenuItemsSortedByName_WhenAssetEntriesNotEmptyAndNavItemsNotEmpty_ThenBothAssetEntriesAndNavItemsAreReturnedSortedInTheCollection() {
		List<AssetEntry> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		NavItem[] navItems = new NavItem[] { mockNavItem1, mockNavItem2 };
		when(CategoryLinkWrapper.newInstance(mockAssetEntry1, false)).thenReturn(mockCategoryLinkWrapper1);
		when(CategoryLinkWrapper.newInstance(mockNavItem1, true)).thenReturn(mockCategoryLinkWrapper2);
		when(CategoryLinkWrapper.newInstance(mockNavItem2, true)).thenReturn(mockCategoryLinkWrapper3);
		when(mockAssetEntry1.getTitle(LOCALE)).thenReturn("c-title-1");
		when(mockNavItem1.getName()).thenReturn("b-title-2");
		when(mockNavItem2.getName()).thenReturn("a-title-2");

		Collection<CategoryLinkWrapper> results = categoryNavigationService.getAssetEntriesAndMenuItemsSortedByName(assetEntries, navItems, LOCALE);

		assertThat(results.size(), equalTo(3));
		assertThat(results, contains(mockCategoryLinkWrapper3, mockCategoryLinkWrapper2, mockCategoryLinkWrapper1));

	}

	@Test
	public void getAssetEntriesAndMenuItemsSortedByName_WhenAssetEntriesNullAndNavItemsNotEmpty_ThenOnlyNavItemsAreReturnedInTheCollection() {
		List<AssetEntry> assetEntries = null;
		NavItem[] navItems = new NavItem[] { mockNavItem1, mockNavItem2 };
		when(CategoryLinkWrapper.newInstance(mockNavItem1, true)).thenReturn(mockCategoryLinkWrapper1);
		when(CategoryLinkWrapper.newInstance(mockNavItem2, true)).thenReturn(mockCategoryLinkWrapper2);
		when(mockNavItem1.getName()).thenReturn("a-title-1");
		when(mockNavItem2.getName()).thenReturn("a-title-2");

		Collection<CategoryLinkWrapper> results = categoryNavigationService.getAssetEntriesAndMenuItemsSortedByName(assetEntries, navItems, LOCALE);

		assertThat(results.size(), equalTo(2));
		assertThat(results, containsInAnyOrder(mockCategoryLinkWrapper1, mockCategoryLinkWrapper2));

	}

	@Test
	public void getAssetEntriesAndMenuItemsSortedByName_WhenAssetEntriesEmptyAndNavItemsNotEmpty_ThenOnlyNavItemsAreReturnedInTheCollection() {
		List<AssetEntry> assetEntries = new ArrayList<>();
		NavItem[] navItems = new NavItem[] { mockNavItem1, mockNavItem2 };
		when(CategoryLinkWrapper.newInstance(mockNavItem1, true)).thenReturn(mockCategoryLinkWrapper1);
		when(CategoryLinkWrapper.newInstance(mockNavItem2, true)).thenReturn(mockCategoryLinkWrapper2);
		when(mockNavItem1.getName()).thenReturn("a-title-1");
		when(mockNavItem2.getName()).thenReturn("a-title-2");

		Collection<CategoryLinkWrapper> results = categoryNavigationService.getAssetEntriesAndMenuItemsSortedByName(assetEntries, navItems, LOCALE);

		assertThat(results.size(), equalTo(2));
		assertThat(results, containsInAnyOrder(mockCategoryLinkWrapper1, mockCategoryLinkWrapper2));

	}

	@Test
	public void getAssetEntriesAndMenuItemsSortedByName_WhenAssetEntriesNotEmptyAndNavItemsNull_ThenOnlyAssetEntriesAreReturnedInTheCollection() {
		List<AssetEntry> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);
		NavItem[] navItems = null;
		when(CategoryLinkWrapper.newInstance(mockAssetEntry1, false)).thenReturn(mockCategoryLinkWrapper1);
		when(CategoryLinkWrapper.newInstance(mockAssetEntry2, false)).thenReturn(mockCategoryLinkWrapper2);
		when(mockAssetEntry1.getTitle(LOCALE)).thenReturn("a-title-1");
		when(mockAssetEntry2.getTitle(LOCALE)).thenReturn("a-title-2");

		Collection<CategoryLinkWrapper> results = categoryNavigationService.getAssetEntriesAndMenuItemsSortedByName(assetEntries, navItems, LOCALE);

		assertThat(results.size(), equalTo(2));
		assertThat(results, containsInAnyOrder(mockCategoryLinkWrapper1, mockCategoryLinkWrapper2));

	}

	@Test
	public void getAssetEntriesAndMenuItemsSortedByName_WhenAssetEntriesNotEmptyAndNavItemsEmpty_ThenOnlyAssetEntriesAreReturnedInTheCollection() {
		List<AssetEntry> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);
		NavItem[] navItems = new NavItem[] {};
		when(CategoryLinkWrapper.newInstance(mockAssetEntry1, false)).thenReturn(mockCategoryLinkWrapper1);
		when(CategoryLinkWrapper.newInstance(mockAssetEntry2, false)).thenReturn(mockCategoryLinkWrapper2);
		when(mockAssetEntry1.getTitle(LOCALE)).thenReturn("a-title-1");
		when(mockAssetEntry2.getTitle(LOCALE)).thenReturn("a-title-2");

		Collection<CategoryLinkWrapper> results = categoryNavigationService.getAssetEntriesAndMenuItemsSortedByName(assetEntries, navItems, LOCALE);

		assertThat(results.size(), equalTo(2));
		assertThat(results, containsInAnyOrder(mockCategoryLinkWrapper1, mockCategoryLinkWrapper2));

	}


	@Test
	public void getAssetEntriesAndMenuItems_WhenNoError_ThenReturnsAmergedListOfAssetEntriesAndNavItemsBasedOnTheirNameInTheOriginalOrder() {
		List<AssetEntry> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);
		NavItem[] navItems = new NavItem[] { mockNavItem1, mockNavItem2 };
		when(CategoryLinkWrapper.newInstance(mockAssetEntry1, false)).thenReturn(mockCategoryLinkWrapper1);
		when(CategoryLinkWrapper.newInstance(mockAssetEntry2, false)).thenReturn(mockCategoryLinkWrapper2);
		when(CategoryLinkWrapper.newInstance(mockNavItem1, true)).thenReturn(mockCategoryLinkWrapper3);
		when(CategoryLinkWrapper.newInstance(mockNavItem2, true)).thenReturn(mockCategoryLinkWrapper4);
		when(mockAssetEntry1.getTitle(LOCALE)).thenReturn("c-title-1");
		when(mockAssetEntry2.getTitle(LOCALE)).thenReturn("a-title-2");
		when(mockNavItem1.getName()).thenReturn("b-title-2");
		when(mockNavItem2.getName()).thenReturn("a-title-2");

		Collection<CategoryLinkWrapper> results = categoryNavigationService.getAssetEntriesAndMenuItems(assetEntries, navItems, LOCALE);

		assertThat(results.size(), equalTo(3));
		assertThat(results, contains(mockCategoryLinkWrapper1, mockCategoryLinkWrapper4, mockCategoryLinkWrapper3));

	}
	
	@Test
	public void getMenuItemsSortedByName_WhenNavItemsEmpty_ThenNavItemsIsNotReturnedInTheCollection() {
		NavItem[] navItems = new NavItem[] {};

		Collection<PageAndWebContentLinkWrapper> results = categoryNavigationService.getMenuItemsSortedByName(navItems);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getMenuItemsSortedByName_WhenNavItemsNull_ThenNavItemsIsNotReturnedInTheCollection() {
		NavItem[] navItems = null;

		Collection<PageAndWebContentLinkWrapper> results = categoryNavigationService.getMenuItemsSortedByName(navItems);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getMenuItemsSortedByName_WhenNavItemsNotEmptyAndTypeIsLayout_ThenNavItemsISReturnedSortedInTheCollection() throws PortalException {
		NavItem[] navItems = new NavItem[] { mockNavItem1, mockNavItem2 };
		when(PageAndWebContentLinkWrapper.newInstance(mockNavItem1, true)).thenReturn(mockPageAndWebContentLinkWrapper1);
		when(PageAndWebContentLinkWrapper.newInstance(mockNavItem2, true)).thenReturn(mockPageAndWebContentLinkWrapper2);
		when(mockNavItem1.getName()).thenReturn("b-title-2");
		when(mockNavItem2.getName()).thenReturn("a-title-2");
		when(mockNavItem1.getLayoutId()).thenReturn(LAYOUT_ID);
		when(mockNavItem2.getLayoutId()).thenReturn(LAYOUT_ID_2);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItem(LAYOUT_ID)).thenReturn(mockSiteNavigationMenuItem);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItem(LAYOUT_ID_2)).thenReturn(mockSiteNavigationMenuItem2);
		when(mockSiteNavigationMenuItem.getType()).thenReturn(LAYOUT_TYPE);
		when(mockSiteNavigationMenuItem2.getType()).thenReturn(LAYOUT_TYPE);

		Collection<PageAndWebContentLinkWrapper> results = categoryNavigationService.getMenuItemsSortedByName(navItems);

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains(mockPageAndWebContentLinkWrapper2, mockPageAndWebContentLinkWrapper1));

	}

	@Test
	public void getMenuItemsSortedByName_WhenNavItemsNotEmptyAndTypeIsJournal_ThenNavItemsISReturnedSortedInTheCollection() throws Exception {
		long assetClassPk = 2;
		NavItem[] navItems = new NavItem[] { mockNavItem1 };
		when(PageAndWebContentLinkWrapper.newInstance(mockJournalArticle, false)).thenReturn(mockPageAndWebContentLinkWrapper1);
		when(mockNavItem1.getName()).thenReturn("a-title-1");
		when(mockNavItem1.getLayoutId()).thenReturn(LAYOUT_ID);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItem(LAYOUT_ID)).thenReturn(mockSiteNavigationMenuItem);
		when(mockSiteNavigationMenuItem.getTypeSettings()).thenReturn("assetClassPk=" + assetClassPk);
		when(mockSiteNavigationMenuItem.getType()).thenReturn(JOURNAL_TYPE);
		when(mockJournalArticleService.getLatestArticle(assetClassPk)).thenReturn(mockJournalArticle);

		Collection<PageAndWebContentLinkWrapper> results = categoryNavigationService.getMenuItemsSortedByName(navItems);

		assertThat(results.size(), equalTo(1));
		assertThat(results, contains(mockPageAndWebContentLinkWrapper1));

	}

	@Test
	public void getMenuItemsSortedByName_WhenNavItemsNotEmptyAndTypeIsJournalAndGetLatestArticleThrowException_ThenNavItemsIsNotReturnedInTheCollection() throws Exception {
		long assetClassPk = 2;
		NavItem[] navItems = new NavItem[] { mockNavItem1 };
		when(PageAndWebContentLinkWrapper.newInstance(mockJournalArticle, false)).thenReturn(mockPageAndWebContentLinkWrapper1);
		when(mockNavItem1.getName()).thenReturn("a-title-1");
		when(mockNavItem1.getLayoutId()).thenReturn(LAYOUT_ID);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItem(LAYOUT_ID)).thenReturn(mockSiteNavigationMenuItem);
		when(mockSiteNavigationMenuItem.getTypeSettings()).thenReturn("assetClassPk=" + assetClassPk);
		when(mockSiteNavigationMenuItem.getType()).thenReturn(JOURNAL_TYPE);
		doThrow(new PortalException()).when(mockJournalArticleService).getLatestArticle(assetClassPk);

		Collection<PageAndWebContentLinkWrapper> results = categoryNavigationService.getMenuItemsSortedByName(navItems);

		assertThat(results.size(), equalTo(0));

	}

	@Test
	public void getMenuItemsSortedByName_WhenNavItemsNotEmptyAndTypeAreJournalAndLayout_ThenNavItemsISReturnedSortedInTheCollection() throws Exception {
		long assetClassPk = 3;
		NavItem[] navItems = new NavItem[] { mockNavItem1, mockNavItem2 };
		when(PageAndWebContentLinkWrapper.newInstance(mockJournalArticle, false)).thenReturn(mockPageAndWebContentLinkWrapper1);
		when(PageAndWebContentLinkWrapper.newInstance(mockNavItem2, true)).thenReturn(mockPageAndWebContentLinkWrapper2);
		when(mockNavItem1.getName()).thenReturn("a-title-1");
		when(mockNavItem2.getName()).thenReturn("b-title-2");
		when(mockNavItem1.getLayoutId()).thenReturn(LAYOUT_ID);
		when(mockNavItem2.getLayoutId()).thenReturn(LAYOUT_ID_2);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItem(LAYOUT_ID)).thenReturn(mockSiteNavigationMenuItem);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItem(LAYOUT_ID_2)).thenReturn(mockSiteNavigationMenuItem2);
		when(mockSiteNavigationMenuItem.getTypeSettings()).thenReturn("assetClassPk=" + assetClassPk);
		when(mockSiteNavigationMenuItem.getType()).thenReturn(JOURNAL_TYPE);
		when(mockSiteNavigationMenuItem2.getType()).thenReturn(LAYOUT_TYPE);
		when(mockJournalArticleService.getLatestArticle(assetClassPk)).thenReturn(mockJournalArticle);

		Collection<PageAndWebContentLinkWrapper> results = categoryNavigationService.getMenuItemsSortedByName(navItems);

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains(mockPageAndWebContentLinkWrapper1, mockPageAndWebContentLinkWrapper2));

	}

	@Test(expected = IOException.class)
	public void getDDMTemplateScript_WhenErrorOnReadingFile_ThenExceptionIsThrown() throws Exception {
		PowerMockito.mockStatic(StringUtil.class);

		when(StringUtil.read(eq(categoryNavigationServiceClassLoader), anyString())).thenThrow(new IOException());

		categoryNavigationService.getDDMTemplateScript(CategoryNavigationWidgetTemplate.SERVICE_MENU);
	}

	@Test
	public void getDDMTemplateScript_WhenNoError_ThenReturnsScriptAsString() throws Exception {
		PowerMockito.mockStatic(StringUtil.class);

		CategoryNavigationWidgetTemplate widgetTemplate = CategoryNavigationWidgetTemplate.SERVICE_MENU;
		String templateResourcePath = "com/placecube/digitalplace/local/category/navigation/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
		String expected = "This script";

		when(StringUtil.read(categoryNavigationServiceClassLoader, templateResourcePath)).thenReturn(expected);

		String actual = categoryNavigationService.getDDMTemplateScript(CategoryNavigationWidgetTemplate.SERVICE_MENU);

		assertEquals(expected, actual);
	}

	private void mockAssetEntryCategoryDetails(AssetEntry assetEntry, long entryId, long classPk, boolean hasCategory, boolean hasViewPermission, List<AssetEntry> assetEntries) {
		assetEntries.add(assetEntry);
		when(assetEntry.getEntryId()).thenReturn(entryId);
		when(assetEntry.getGroupId()).thenReturn(GROUP_ID);
		when(assetEntry.getClassPK()).thenReturn(classPk);
		when(mockAssetEntryAssetCategoryRelLocalService.fetchAssetEntryAssetCategoryRel(entryId, CATEGORY_ID)).thenReturn(hasCategory ? mockAssetEntryAssetCategoryRel : null);
		when(mockPermissionChecker.hasPermission(GROUP_ID, JournalArticle.class.getName(), classPk, ActionKeys.VIEW)).thenReturn(hasViewPermission);
	}

}
