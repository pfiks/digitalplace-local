package com.placecube.digitalplace.local.category.navigation.constants;

public enum CategoryNavigationWidgetTemplate {

	CATEGORY_NAV_LINKS("ASSET-CATEGORIES-NAVIGATION-CATEGORY-NAV-LINKS", "Category Nav Links"),

	SERVICE_MENU("ASSET-CATEGORIES-NAVIGATION-SERVICE-MENU", "Service Menu");

	private final String key;
	private final String name;

	private CategoryNavigationWidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
