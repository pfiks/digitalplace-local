package com.placecube.digitalplace.local.category.navigation.model;


public class CategoryLinkWrapper {

	private final boolean navItem;

	private final Object wrappedObject;

	public static CategoryLinkWrapper newInstance(Object wrappedObject, boolean navItem) {
		return new CategoryLinkWrapper(wrappedObject, navItem);
	}

	private CategoryLinkWrapper(Object nativeObject, boolean navItem) {
		this.navItem = navItem;
		this.wrappedObject = nativeObject;
	}

	public Object getWrappedObject() {
		return wrappedObject;
	}

	public boolean isNavItem() {
		return navItem;
	}

}
