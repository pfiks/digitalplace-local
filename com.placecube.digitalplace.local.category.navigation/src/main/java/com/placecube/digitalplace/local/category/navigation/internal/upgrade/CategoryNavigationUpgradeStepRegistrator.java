package com.placecube.digitalplace.local.category.navigation.internal.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.category.navigation.internal.upgrade.upgrade_1_0_0.ServiceMenuWidgetTemplateUpgradeProcess;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class CategoryNavigationUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;
	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new ServiceMenuWidgetTemplateUpgradeProcess(companyLocalService, ddmTemplateLocalService));
	}
}
