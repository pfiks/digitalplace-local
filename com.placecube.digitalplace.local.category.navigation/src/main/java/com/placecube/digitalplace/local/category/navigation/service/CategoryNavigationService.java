package com.placecube.digitalplace.local.category.navigation.service;

import com.liferay.asset.category.property.service.AssetCategoryPropertyLocalService;
import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.list.asset.entry.provider.AssetListAssetEntryProvider;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.info.pagination.InfoPage;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.segments.constants.SegmentsEntryConstants;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationContentSet;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationProperty;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;
import com.placecube.digitalplace.local.category.navigation.model.CategoryLinkWrapper;
import com.placecube.digitalplace.local.category.navigation.model.PageAndWebContentLinkWrapper;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = CategoryNavigationService.class)
public class CategoryNavigationService {

	private static final Log LOG = LogFactoryUtil.getLog(CategoryNavigationService.class);

	@Reference
	private AssetEntryAssetCategoryRelLocalService assetEntryAssetCategoryRelLocalService;

	@Reference
	private AssetCategoryPropertyLocalService assetCategoryPropertyLocalService;

	@Reference
	private AssetListAssetEntryProvider assetListAssetEntryProvider;

	@Reference
	private DDMInitializer ddmInitializer;

	@Reference
	private AssetListEntryInitializer assetListEntryInitializer;

	@Reference
	private SiteNavigationMenuItemLocalService siteNavigationMenuItemLocalService;

	@Reference
	private JournalArticleService journalArticleService;

	public Optional<String> getLandingPageForCategory(AssetCategory assetCategory) {
		try {
			return Optional.of(
					StringPool.FORWARD_SLASH + assetCategoryPropertyLocalService.getCategoryProperty(assetCategory.getCategoryId(), CategoryNavigationProperty.LANDING_PAGE.getKey()).getValue());
		} catch (PortalException e) {
			LOG.debug(e);
			return Optional.empty();
		}
	}

	public AssetListEntry getOrCreateContentSet(CategoryNavigationContentSet contentSet, ServiceContext serviceContext) throws PortalException {
		return assetListEntryInitializer.getOrCreateContentSet(contentSet.getName(), true, serviceContext);
	}

	public DDMTemplate getOrCreateWidgetTemplate(CategoryNavigationWidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = getTemplateResourcePath(widgetTemplate);
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetCategory.class, templateResourcePath,
				getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}

	public String getDDMTemplateScript(CategoryNavigationWidgetTemplate widgetTemplate) throws IOException {
		String templateResourcePath = getTemplateResourcePath(widgetTemplate);
		return StringUtil.read(getClass().getClassLoader(), templateResourcePath);
	}

	public List<AssetEntry> getServiceMenuAssetEntriesByCategory(AssetCategory assetCategory, PermissionChecker permissionChecker) {
		Optional<AssetListEntry> assetListEntry = assetListEntryInitializer.getContentSet(assetCategory.getGroupId(), CategoryNavigationContentSet.SERVICE_MENU.getName());
		if (assetListEntry.isPresent()) {
			
			InfoPage<AssetEntry> assetEntriesInfoPage = assetListAssetEntryProvider.getAssetEntriesInfoPage(assetListEntry.get(), new long[] {SegmentsEntryConstants.ID_DEFAULT}, null, null, StringPool.BLANK, StringPool.BLANK,
					QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
			return assetEntriesInfoPage.getPageItems().stream().filter(assetEntry -> assetFilter(assetCategory, assetEntry, permissionChecker)).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private boolean assetFilter(AssetCategory assetCategory, AssetEntry assetEntry, PermissionChecker permissionChecker) {
		return Validator.isNotNull(assetEntryAssetCategoryRelLocalService.fetchAssetEntryAssetCategoryRel(assetEntry.getEntryId(), assetCategory.getCategoryId())) && permissionChecker.hasPermission(assetEntry.getGroupId(),
				JournalArticle.class.getName(), assetEntry.getClassPK(), ActionKeys.VIEW);
	}

	private String getTemplateResourcePath(CategoryNavigationWidgetTemplate widgetTemplate) {
		return "com/placecube/digitalplace/local/category/navigation/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
	}

	
	public Collection<CategoryLinkWrapper> getAssetEntriesAndMenuItems(List<AssetEntry> assetEntries, NavItem[] navItems, Locale locale) {
		return getAssetEntriesAndMenuItemsMerged(assetEntries, navItems, locale, new LinkedHashMap<>());
	}

	public Collection<CategoryLinkWrapper> getAssetEntriesAndMenuItemsSortedByName(List<AssetEntry> assetEntries, NavItem[] navItems, Locale locale) {
		return getAssetEntriesAndMenuItemsMerged(assetEntries, navItems, locale, new TreeMap<>());
	}

	public Collection<PageAndWebContentLinkWrapper> getMenuItemsSortedByName(NavItem[] navItems) {
		SortedMap<String, PageAndWebContentLinkWrapper> mergeItems = new TreeMap<>();

		if (Validator.isNotNull(navItems)) {
			for (NavItem navItem : navItems) {
				long layoutId = navItem.getLayoutId();
				try {
					SiteNavigationMenuItem siteNavigationMenuItem = siteNavigationMenuItemLocalService.getSiteNavigationMenuItem(layoutId);
					if (siteNavigationMenuItem.getType().equals("journal")) {
						UnicodeProperties unicodeProperties = new UnicodeProperties();
						unicodeProperties.fastLoad(siteNavigationMenuItem.getTypeSettings());
						long assetClassPK = Long.parseLong(unicodeProperties.getProperty("assetClassPk"));
						JournalArticle latestArticle = journalArticleService.getLatestArticle(assetClassPK);
						mergeItems.put(navItem.getName(), PageAndWebContentLinkWrapper.newInstance(latestArticle, false));
					} else {
						mergeItems.put(navItem.getName(), PageAndWebContentLinkWrapper.newInstance(navItem, true));
					}
				} catch (PortalException e) {
					LOG.debug(e);
				}

			}
		}

		return mergeItems.values();
	}
	
	private Collection<CategoryLinkWrapper> getAssetEntriesAndMenuItemsMerged(List<AssetEntry> assetEntries, NavItem[] navItems, Locale locale, Map<String, CategoryLinkWrapper> mergeItems) {
		if (Validator.isNotNull(assetEntries)) {
			for (AssetEntry assetEntry : assetEntries) {
				mergeItems.put(assetEntry.getTitle(locale), CategoryLinkWrapper.newInstance(assetEntry, false));
			}
		}

		if (Validator.isNotNull(navItems)) {
			for (NavItem navItem : navItems) {
				mergeItems.put(navItem.getName(), CategoryLinkWrapper.newInstance(navItem, true));
			}
		}

		return mergeItems.values();
	}
}
