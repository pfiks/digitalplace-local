package com.placecube.digitalplace.local.category.navigation.constants;

public enum CategoryNavigationContentSet {

	SERVICE_MENU("Service Menu");

	private final String name;

	private CategoryNavigationContentSet(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
