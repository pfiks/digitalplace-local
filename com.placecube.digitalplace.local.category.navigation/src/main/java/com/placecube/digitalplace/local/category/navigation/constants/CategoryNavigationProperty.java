package com.placecube.digitalplace.local.category.navigation.constants;

public enum CategoryNavigationProperty {

	LANDING_PAGE("landing-page");

	private final String key;

	private CategoryNavigationProperty(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}