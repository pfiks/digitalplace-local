package com.placecube.digitalplace.local.category.navigation.upgrade;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.category.navigation.upgrade.upgrade_1_0_0.CategoryNavigationWidgetTemplatesUpgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class CategoryNavigationUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new CategoryNavigationWidgetTemplatesUpgrade(ddmTemplateLocalService));
	}
}