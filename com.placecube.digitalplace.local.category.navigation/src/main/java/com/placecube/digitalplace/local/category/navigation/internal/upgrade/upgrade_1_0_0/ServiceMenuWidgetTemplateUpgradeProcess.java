package com.placecube.digitalplace.local.category.navigation.internal.upgrade.upgrade_1_0_0;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;

public class ServiceMenuWidgetTemplateUpgradeProcess extends UpgradeProcess {

	private static final String SEE_ALL = "See all...";

	private static final String SEE_ALL_WITH_ELLIPSIS = "See all…";

	private final CompanyLocalService companyLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	public ServiceMenuWidgetTemplateUpgradeProcess(CompanyLocalService companyLocalService, DDMTemplateLocalService ddmTemplateLocalService) {
		this.companyLocalService = companyLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (CompanyThreadLocal.getCompanyId() == 0) {
			for (Company company : companyLocalService.getCompanies()) {
				upgradeWidgetTemplate(company.getCompanyId());
			}
		} else {
			upgradeWidgetTemplate(CompanyThreadLocal.getCompanyId());
		}
	}

	private void upgradeTemplate(DDMTemplate ddmTemplate) {
		if (ddmTemplate.getScript().contains(SEE_ALL_WITH_ELLIPSIS)) {
			ddmTemplate.setScript(ddmTemplate.getScript().replace(SEE_ALL_WITH_ELLIPSIS, SEE_ALL));
			ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
		}
	}

	private void upgradeWidgetTemplate(long companyId) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("templateKey", CategoryNavigationWidgetTemplate.SERVICE_MENU.getKey()));
		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);
		for (DDMTemplate ddmTemplate : ddmTemplates) {
			upgradeTemplate(ddmTemplate);
		}
	}
}
