package com.placecube.digitalplace.local.category.navigation.model;


public class PageAndWebContentLinkWrapper {

	private final boolean page;

	private final Object wrappedObject;

	public static PageAndWebContentLinkWrapper newInstance(Object wrappedObject, boolean page) {
		return new PageAndWebContentLinkWrapper(wrappedObject, page);
	}

	private PageAndWebContentLinkWrapper(Object nativeObject, boolean page) {
		this.page = page;
		this.wrappedObject = nativeObject;
	}

	public Object getWrappedObject() {
		return wrappedObject;
	}

	public boolean isPage() {
		return page;
	}

}
