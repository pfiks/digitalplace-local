package com.placecube.digitalplace.local.category.navigation.upgrade.upgrade_1_0_0;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;

import java.util.List;

public class CategoryNavigationWidgetTemplatesUpgrade extends UpgradeProcess {

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(CategoryNavigationWidgetTemplatesUpgrade.class);

	public CategoryNavigationWidgetTemplatesUpgrade(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		upgradeWidgetTemplate(companyId, CategoryNavigationWidgetTemplate.CATEGORY_NAV_LINKS);
		upgradeWidgetTemplate(companyId, CategoryNavigationWidgetTemplate.SERVICE_MENU);
	}

	private void upgradeWidgetTemplate(long companyId, CategoryNavigationWidgetTemplate widgetTemplate) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("templateKey", widgetTemplate.getKey()));

		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		if (ddmTemplates.isEmpty()) {
			LOG.warn("Template " + widgetTemplate.getKey() + " not found in company " + companyId);
		} else {
			ddmTemplates.forEach(this::updateTemplate);
		}

	}

	private void updateTemplate(DDMTemplate ddmTemplate) {
		String newScript = getAmendedScript(ddmTemplate.getScript());
		ddmTemplate.setScript(newScript);
		ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
	}

	private String getAmendedScript(String script) {
		return script.replace("serviceLocator.findService(\"com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService\")", "digitalplace_categoryNavigationService")
				.replace("serviceLocator.findService(\"com.placecube.digitalplace.category.service.CategoryRetrievalService\")", "digitalplace_categoryRetrievalService")
				.replace("serviceLocator.findService(\"com.placecube.journal.service.JournalArticleRetrievalService\")", "digitalplace_journalArticleRetrievalService");
	}

}
