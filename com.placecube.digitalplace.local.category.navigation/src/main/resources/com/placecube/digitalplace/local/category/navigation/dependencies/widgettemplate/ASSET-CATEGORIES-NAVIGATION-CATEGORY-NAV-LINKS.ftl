<#if entries?has_content && serviceLocator??>
	<#assign
		categoryNavigationService = digitalplace_categoryNavigationService
		categoryRetrievalService = digitalplace_categoryRetrievalService
		journalArticleRetrievalService = digitalplace_journalArticleRetrievalService 
	/>

	<div class="category-nav-links-widget">
		<div class="card-deck">
			<#list entries as entry>
				<#list entry.getCategories() as category>
					<#if category.isRootCategory() >
						<#assign assetEntries = categoryNavigationService.getServiceMenuAssetEntriesByCategory(category, permissionChecker) />
						<#assign landingPage = categoryNavigationService.getLandingPageForCategory(category) />
						
						<#if landingPage.isPresent() || assetEntries?has_content>

							<div class="card-cell">
								<div class="category-entry card">
									<div class="card-body">
										<h2>
											<#if landingPage.isPresent()>
												<a href="${landingPage.get()}">${category.getName()}</a>
											<#else>
												${category.getName()}
											</#if>
										</h2>
										<#if assetEntries?has_content>
											<div class="row">
												<ul>
													<#list assetEntries as curAssetEntry>
														<#assign journalArticle = curAssetEntry.getAssetRenderer().getArticle() />
														<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />
														<#if articleURLOptional.isPresent() >
															<li>
																<a href="${articleURLOptional.get()}">${curAssetEntry.getTitle(locale)}</a>
															</li>
														</#if>
													</#list>
												</ul>
											</div>
										</#if>
									</div>
								</div>
							</div>
						</#if>
					</#if>
				</#list>
			</#list>
		</div>
	</div>
</#if>