<#if entries?has_content && serviceLocator??>
	<#assign	
		categoryNavigationService = digitalplace_categoryNavigationService
		categoryRetrievalService = digitalplace_categoryRetrievalService
		journalArticleRetrievalService = digitalplace_journalArticleRetrievalService 
	/>
	
	<div class="categories-services-menu">
		<#list entries as entry>
			<#list entry.getCategories() as category>
				<#if category.isRootCategory() >
					<#assign assetEntries = categoryNavigationService.getServiceMenuAssetEntriesByCategory(category, permissionChecker) />
					<#assign landingPage = categoryNavigationService.getLandingPageForCategory(category) />
					
					<#if landingPage.isPresent() || assetEntries?has_content>
					
						<div class="categories-service-menu-entry">
							<#-- Category landing page  -->
							<div class="row nav-heading" data-a11y-toggle="layout_${category.getCategoryId()}" aria-controls="layout_${category.getCategoryId()}">
								<div class="col-md-10 col-xs-11">
									<h2>
										<#if landingPage.isPresent()>
											<a href="${landingPage.get()}">${category.getName()}</a>
										<#else>
											${category.getName()}
										</#if>
									</h2>
								</div>
								<div class="col-md-2 col-xs-1 nav-entry-icon" />
									<span class="${categoryRetrievalService.getPropertyValue(category, "icon")}"></span>
								</div>
							</div>
							
							<#-- Category asset entries  -->
							<#if assetEntries?has_content>
								<div class="row nav-content" id="layout_${category.getCategoryId()}">
									<div class="col-md-12">
										<ul>
											<#list assetEntries as curAssetEntry>
												
												<#assign journalArticle = curAssetEntry.getAssetRenderer().getArticle() />
												<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />

												<#if articleURLOptional.isPresent() >
													<li>
														<a href="${articleURLOptional.get()}">${curAssetEntry.getTitle(locale)}</a>
													</li>
												</#if>
											</#list>
										</ul>
										<#if landingPage.isPresent()>
											<a class="see-all" href="${landingPage.get()}">See all...</a>
										</#if>
									</div>
								</div>
							</#if>
						</div>
					</#if>
				</#if>
			</#list>
		</#list>
	</div>
</#if>