package com.placecube.digitalplace.local.permission.service;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;

public interface DigitalPlacePermissionService {

	Boolean hasSiteAdminAccess(Group group, PermissionChecker permissionChecker);

}
