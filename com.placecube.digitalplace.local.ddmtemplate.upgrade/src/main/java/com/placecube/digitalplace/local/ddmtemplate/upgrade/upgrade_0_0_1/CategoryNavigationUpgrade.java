package com.placecube.digitalplace.local.ddmtemplate.upgrade.upgrade_0_0_1;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.category.navigation.constants.CategoryNavigationWidgetTemplate;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;

public class CategoryNavigationUpgrade extends UpgradeProcess {

	private final CategoryNavigationService categoryNavigationService;

	public CategoryNavigationUpgrade(CategoryNavigationService categoryNavigationService) {
		this.categoryNavigationService = categoryNavigationService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		upgradeTemplate(CategoryNavigationWidgetTemplate.CATEGORY_NAV_LINKS);
		upgradeTemplate(CategoryNavigationWidgetTemplate.SERVICE_MENU);
	}

	private void upgradeTemplate(CategoryNavigationWidgetTemplate widgetTemplate) throws IOException, NamingException, SQLException  {
		String script = categoryNavigationService.getDDMTemplateScript(widgetTemplate);
		runSQLTemplateString("update DDMTemplate set script='" + script + "' where templateKey='" + widgetTemplate.getKey() +"';", false);
	}

}
