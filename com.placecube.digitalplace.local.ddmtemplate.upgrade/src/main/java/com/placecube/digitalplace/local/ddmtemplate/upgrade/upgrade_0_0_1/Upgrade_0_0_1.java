package com.placecube.digitalplace.local.ddmtemplate.upgrade.upgrade_0_0_1;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.category.navigation.service.CategoryNavigationService;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade_0_0_1 implements UpgradeStepRegistrator {

	@Reference
	private CategoryNavigationService categoryNavigationService;
	
	@Override
	public void register(Registry registry) {		
		registry.register("0.0.0", "0.0.1", new CategoryNavigationUpgrade(categoryNavigationService));
	}
}