package com.placecube.digitalplace.local.ddmtemplate.upgrade.upgrade_0_0_2;
 
import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.webcontent.article.constants.WidgetTemplate;
import com.placecube.digitalplace.local.webcontent.article.service.ArticleWebContentService;

public class ArticleWebContentTemplateUpgrade extends UpgradeProcess {

	private final ArticleWebContentService articleWebContentService;

	public ArticleWebContentTemplateUpgrade(ArticleWebContentService articleWebContentService) {
		this.articleWebContentService = articleWebContentService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		upgradeTemplate(WidgetTemplate.CATEGORY_CONTENT_LIST);
		upgradeTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS);
		upgradeTemplate(WidgetTemplate.FEATURED_CONTENT_NAV_PANELS_LARGE);
		upgradeTemplate(WidgetTemplate.RELATED_CONTENT_LIST);

	}

	private void upgradeTemplate(WidgetTemplate widgetTemplate) throws IOException, NamingException, SQLException {
		String script = articleWebContentService.getDDMTemplateScript(widgetTemplate);
		script = script.replace("'", "''");
		runSQL("update DDMTemplate set script='" + script + "' where templateKey='" + widgetTemplate.getKey() +"';");
	}
	
}