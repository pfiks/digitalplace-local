package com.placecube.digitalplace.local.ddmtemplate.upgrade.upgrade_0_0_2;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.webcontent.article.service.ArticleWebContentService;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade_0_0_2 implements UpgradeStepRegistrator {

	@Reference
	private ArticleWebContentService articleWebContentService;
	
	@Override
	public void register(Registry registry) {		
		registry.register("0.0.1", "0.0.2", new ArticleWebContentTemplateUpgrade(articleWebContentService));
	}
}