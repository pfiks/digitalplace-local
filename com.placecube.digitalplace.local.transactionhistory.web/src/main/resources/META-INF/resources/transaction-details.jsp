<%@ include file="/init.jsp" %>
<div class="form-details">
	<dl class="govuk-summary-list">
		<c:forEach items="${ formFieldEntries }" var="formFieldEntry">
			<div class="govuk-summary-list__row">
				<dt class="govuk-summary-list__key">
					${ formFieldEntry.fieldLabel }
				</dt>
				<dd class="govuk-summary-list__value">
					${ formFieldEntry.fieldValue }
				</dd>
			</div>
		</c:forEach>
	</dl>
</div>