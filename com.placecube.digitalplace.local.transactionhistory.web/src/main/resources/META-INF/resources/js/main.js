AUI.add(
		'com-placecube-digitalplace-transactionhistory',
		function(A) {

			var TransactionHistory = {

					init: function(namespace) {

						$(document).on('click', '.transaction-history-view-detail-button', function(e) {

							var url = $(e.target).attr('data-view-url');

							Liferay.Util.openWindow({
								dialog: {
									width: 896,
									destroyOnHide: true,
									cache: true,
									centered: true,
									constrain: true,
									cssClass: 'transaction-history-modal digitalplace-wrapper',
									modal: true,
									resizable: false,
									'toolbars.footer': [
										{
											cssClass: 'btn-default',
											label: Liferay.Language.get('close'),
											on: {
												click: function() {
													Liferay.Util.getWindow(namespace+'viewFormSubmissionDetail').hide();
												}
											}
										}
									]
								},
								dialogIframe: {
									bodyCssClass: 'digitalplace-wrapper'
								},
								title: Liferay.Language.get('your-form-submission'),
								id: namespace+'viewFormSubmissionDetail',
								uri: url
							});

						});
					}
			};

			A.TransactionHistory = TransactionHistory;
		},
		'',
		{
			requires: []
		}
);
