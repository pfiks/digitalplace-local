<%@ include file="/init.jsp"%>

<jsp:useBean id="formEntries" class="java.util.ArrayList" scope="request"/>

<liferay-ui:search-container emptyResultsMessage="there-are-no-transactions-on-your-account">

	<liferay-ui:search-container-results results="${formEntries}" />

	<liferay-ui:search-container-row className="com.placecube.digitalplace.local.transactionhistory.model.FormEntryDetail" modelVar="entry">

		<liferay-ui:search-container-column-text name="form-name">

			<span class="btn-link transaction-history-view-detail-button" data-view-url="${entry.viewURL}">${entry.formName}</span>

			<aui:script use="com-placecube-digitalplace-transactionhistory">
				A.TransactionHistory.init('<portlet:namespace />');
			</aui:script>

		</liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-date property="submitDate" name="submitted" />

		<liferay-ui:search-container-column-date property="lastUpdateDate" name="last-updated" />

		<liferay-ui:search-container-column-text name="status">		
			<span class="label ${ entry.submitted ? 'label-success' : 'label-warning' }"><liferay-ui:message key="${entry.transactionStatus}" /></span>
		</liferay-ui:search-container-column-text>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />

</liferay-ui:search-container>