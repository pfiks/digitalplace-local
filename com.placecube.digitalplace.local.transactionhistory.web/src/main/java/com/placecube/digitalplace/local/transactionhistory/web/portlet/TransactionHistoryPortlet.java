package com.placecube.digitalplace.local.transactionhistory.web.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormEntryDetail;
import com.placecube.digitalplace.local.transactionhistory.web.service.TransactionHistoryService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=transaction-history", "com.liferay.portlet.css-class-wrapper=portlet-transaction-history",
		"com.liferay.portlet.display-category=category.collaboration", "com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.header-portlet-javascript=/js/main.js",
		"com.liferay.portlet.instanceable=false", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PortletKeys.TRANSACTION_HISTORY, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class TransactionHistoryPortlet extends MVCPortlet {

	private static final Log LOG = LogFactoryUtil.getLog(TransactionHistoryPortlet.class);

	@Reference
	private TransactionHistoryService transactionHistoryService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();

		List<TransactionHistory> formSubmissionsForUser = transactionHistoryService.getFormSubmissionsForUser(user);
		renderRequest.setAttribute(PortletRequestKeys.FORM_ENTRIES, getOrderedResults(renderRequest, user.getLocale(), formSubmissionsForUser));

		super.render(renderRequest, renderResponse);
	}

	private List<FormEntryDetail> getOrderedResults(RenderRequest renderRequest, Locale locale, List<TransactionHistory> formSubmissionsForUser) {
		List<FormEntryDetail> formEntryDetails = new ArrayList<>();
		for (TransactionHistory transactionHistory : formSubmissionsForUser) {
			try {
				formEntryDetails.add(transactionHistoryService.getFormEntryDetail(renderRequest, transactionHistory, locale));
			} catch (Exception e) {
				LOG.debug(e);
				LOG.warn("Unable to add form submission - " + e.getMessage());
			}
		}
		return transactionHistoryService.getOrderedResults(formEntryDetails);
	}

}