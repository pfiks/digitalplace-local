package com.placecube.digitalplace.local.transactionhistory.web.model;

public class FormFieldEntry {

	private String fieldLabel;
	private String fieldValue;

	FormFieldEntry(String fieldLabel, String fieldValue) {
		this.fieldLabel = fieldLabel;
		this.fieldValue = fieldValue;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public String getFieldValue() {
		return fieldValue;
	}

}
