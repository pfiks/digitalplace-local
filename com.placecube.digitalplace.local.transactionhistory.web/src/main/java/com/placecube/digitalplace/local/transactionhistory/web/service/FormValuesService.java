package com.placecube.digitalplace.local.transactionhistory.web.service;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Portal;

@Component(immediate = true, service = FormValuesService.class)
public class FormValuesService {

	@Reference
	private Portal portal;

	public AssetEntry getAssetEntryByClassNameIdAndClassPK(long classNameId, long classPK) throws PortalException {
		return AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(classNameId).getAssetEntry(portal.getClassName(classNameId), classPK);
	}

	public DDMFormValues getDDMFormValues(long classNameId, long classPK) throws PortalException {
		DDMFormValues ddmFormValues = null;
		AssetEntry assetEntry = getAssetEntryByClassNameIdAndClassPK(classNameId, classPK);

		if (isDDMFormInstanceRecord(assetEntry)) {
			ddmFormValues = getDDMFormInstanceRecord(assetEntry).getDDMFormValues();

		} else if (isDDLRecord(assetEntry)) {
			ddmFormValues = getDDLRecord(assetEntry).getDDMFormValues();

		} else {
			throw new PortalException("Unable to get DDMFormValues - classNameId: " + classNameId + ", classPK: " + classPK);
		}

		return ddmFormValues;
	}

	public String getFormName(AssetEntry assetEntry, Locale locale) throws PortalException {
		String formName = StringPool.BLANK;

		if (isDDMFormInstanceRecord(assetEntry)) {
			formName = getDDMFormInstanceRecord(assetEntry).getFormInstance().getName(locale);

		} else if (isDDLRecord(assetEntry)) {
			formName = getDDLRecord(assetEntry).getRecordSet().getName(locale);

		} else {
			throw new PortalException("Unable to get form name from AssetEntry - assetEntryId: " + assetEntry.getEntryId());
		}

		return formName;
	}

	private DDLRecord getDDLRecord(AssetEntry assetEntry) {
		return (DDLRecord) assetEntry.getAssetRenderer().getAssetObject();
	}

	private DDMFormInstanceRecord getDDMFormInstanceRecord(AssetEntry assetEntry) {
		return (DDMFormInstanceRecord) assetEntry.getAssetRenderer().getAssetObject();
	}

	private boolean isDDLRecord(AssetEntry assetEntry) {
		return assetEntry.getClassName().equals(DDLRecord.class.getName());
	}

	private boolean isDDMFormInstanceRecord(AssetEntry assetEntry) {
		return assetEntry.getClassName().equals(DDMFormInstanceRecord.class.getName());
	}

}
