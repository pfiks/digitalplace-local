package com.placecube.digitalplace.local.transactionhistory.web.model;

import java.util.Date;

import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;

public class FormEntryDetail {

	private final String formName;
	private Date lastUpdateDate;
	private String transactionStatus;
	private final Date submitDate;
	private boolean submitted;
	private final String viewURL;
	private boolean workflow;

	FormEntryDetail(String formName, Date submitDate, String viewURL) {
		this.formName = formName;
		this.submitDate = submitDate;
		this.viewURL = viewURL;
		transactionStatus = TransactionHistoryStatusConstants.STATUS_SUBMITTED;
		workflow = false;
		submitted = true;
		lastUpdateDate = submitDate;
	}

	public String getFormName() {
		return formName;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public String getViewURL() {
		return viewURL;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public boolean isWorkflow() {
		return workflow;
	}

	void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	void setWorkflow(boolean workflow) {
		this.workflow = workflow;
	}

}
