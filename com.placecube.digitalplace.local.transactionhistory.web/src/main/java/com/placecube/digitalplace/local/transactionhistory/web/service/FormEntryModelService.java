package com.placecube.digitalplace.local.transactionhistory.web.service;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.WindowStateException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormEntryDetail;
import com.placecube.digitalplace.local.transactionhistory.web.model.ModelFactoryBuilder;

@Component(immediate = true, service = FormEntryModelService.class)
public class FormEntryModelService {

	@Reference
	private FormValuesService formValuesService;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	@Reference
	private PortletURLFactory portletURLFactory;

	public void configureFormEntryDetails(FormEntryDetail formEntryDetail, TransactionHistory transactionHistory) {

		boolean isSubmitted = TransactionHistoryStatusConstants.STATUS_SUBMITTED.equalsIgnoreCase(transactionHistory.getTransactionStatus());

		modelFactoryBuilder.updateFormEntryDetails(formEntryDetail, transactionHistory.getTransactionStatus(), transactionHistory.getModifiedDate(), isSubmitted);
	}

	public FormEntryDetail getBasicFormEntryDetail(PortletRequest portletRequest, Locale locale, long classNameId, long classPK) throws PortalException {

		try {
			AssetEntry assetEntry = formValuesService.getAssetEntryByClassNameIdAndClassPK(classNameId, classPK);
			String formName = formValuesService.getFormName(assetEntry, locale);
			String viewURL = getViewURL(portletRequest, classNameId, classPK);

			return modelFactoryBuilder.createFormEntryDetail(formName, assetEntry.getCreateDate(), viewURL);

		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private String getViewURL(PortletRequest portletRequest, long classNameId, long classPK) throws WindowStateException {
		LiferayPortletURL liferayPortletURL = portletURLFactory.create(portletRequest, PortletKeys.TRANSACTION_HISTORY, PortletRequest.RENDER_PHASE);
		liferayPortletURL.setWindowState(LiferayWindowState.POP_UP);
		liferayPortletURL.setParameter(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.VIEW_TRANSACTION_DETAILS);
		liferayPortletURL.setParameter(PortletRequestKeys.CLASS_NAME_ID, String.valueOf(classNameId));
		liferayPortletURL.setParameter(PortletRequestKeys.CLASS_PK, String.valueOf(classPK));

		return liferayPortletURL.toString();
	}

}
