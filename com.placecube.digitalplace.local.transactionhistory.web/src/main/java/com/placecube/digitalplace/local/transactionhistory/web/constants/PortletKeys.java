package com.placecube.digitalplace.local.transactionhistory.web.constants;

public final class PortletKeys {

	public static final String TRANSACTION_HISTORY = "com_placecube_digitalplace_local_transactionhistory_portlet_TransactionHistoryPortlet";

	private PortletKeys() {
	}

}