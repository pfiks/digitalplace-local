package com.placecube.digitalplace.local.transactionhistory.web.constants;

public final class MVCCommandKeys {

	public static final String VIEW_TRANSACTION_DETAILS = "/view-details";

	private MVCCommandKeys() {
	}

}