package com.placecube.digitalplace.local.transactionhistory.web.model;

import java.util.Date;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = ModelFactoryBuilder.class)
public class ModelFactoryBuilder {

	public FormEntryDetail createFormEntryDetail(String formName, Date submitDate, String viewURL) {
		return new FormEntryDetail(formName, submitDate, viewURL);
	}

	public FormFieldEntry createFormFieldEntry(String fieldLabel, String fieldValue) {
		return new FormFieldEntry(fieldLabel, fieldValue);
	}

	public void updateFormEntryDetails(FormEntryDetail formEntryDetail, String status, Date lastUpdateDate, boolean isSubmitted) {
		formEntryDetail.setTransactionStatus(status);
		formEntryDetail.setLastUpdateDate(lastUpdateDate);
		formEntryDetail.setSubmitted(isSubmitted);
	}
}
