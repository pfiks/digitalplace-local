package com.placecube.digitalplace.local.transactionhistory.web.constants;

public class PortletRequestKeys {

	public static final String CLASS_NAME_ID = "classNameId";

	public static final String CLASS_PK = "classPK";

	public static final String COMPANY_ID = "companyId";

	public static final String FORM_ENTRIES = "formEntries";

	public static final String FORM_FIELD_ENTRIES = "formFieldEntries";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String USER_ID = "userId";

	private PortletRequestKeys() {

	}
}
