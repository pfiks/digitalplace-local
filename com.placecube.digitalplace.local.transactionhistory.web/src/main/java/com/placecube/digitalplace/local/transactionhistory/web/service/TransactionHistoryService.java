package com.placecube.digitalplace.local.transactionhistory.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeServicesRegistry;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldValueRenderer;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.petra.string.StringUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormEntryDetail;

@Component(immediate = true, service = TransactionHistoryService.class)
public class TransactionHistoryService {

	private static final Log LOG = LogFactoryUtil.getLog(TransactionHistoryService.class);

	@Reference
	private DDMFormFieldTypeServicesRegistry ddmFormFieldTypeServicesRegistry;

	@Reference
	private FormEntryModelService formEntryModelService;

	@Reference
	private Portal portal;

	@Reference
	private TransactionHistoryLocalService transactionHistoryLocalService;

	public String getFieldLabel(Locale locale, DDMFormField formField) {
		LocalizedValue label = formField.getLabel();

		return label.getString(locale);
	}

	public String getFieldValue(Locale locale, DDMFormField formField, List<DDMFormFieldValue> formFieldValues) {

		if (formField == null || formFieldValues == null) {
			return StringPool.BLANK;
		}

		final DDMFormFieldValueRenderer fieldValueRenderer = ddmFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(formField.getType());

		try {
			List<String> renderedFormFielValues = new ArrayList<>();
			for (DDMFormFieldValue formFieldValue : formFieldValues) {

				renderedFormFielValues.add(fieldValueRenderer.render(formFieldValue, locale));
			}

			return StringUtil.merge(renderedFormFielValues, StringPool.COMMA_AND_SPACE);

		} catch (NullPointerException e) {
			LOG.error("Unable to render form field value for form field type: " + formField.getType());
		}

		return StringPool.BLANK;
	}


	public FormEntryDetail getFormEntryDetail(RenderRequest portletRequest, TransactionHistory transactionHistory, Locale locale) throws PortalException {

		FormEntryDetail formEntryDetail = formEntryModelService.getBasicFormEntryDetail(portletRequest, locale, transactionHistory.getClassNameId(), transactionHistory.getClassPK());

		formEntryModelService.configureFormEntryDetails(formEntryDetail, transactionHistory);

		return formEntryDetail;
	}

	public List<TransactionHistory> getFormSubmissionsForUser(User user) {

		DynamicQuery dynamicQuery = transactionHistoryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq(PortletRequestKeys.COMPANY_ID, user.getCompanyId()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq(PortletRequestKeys.USER_ID, user.getUserId()));

		return transactionHistoryLocalService.dynamicQuery(dynamicQuery);
	}

	public List<FormEntryDetail> getOrderedResults(List<FormEntryDetail> results) {
		results.sort((o1, o2) -> o2.getSubmitDate().compareTo(o1.getSubmitDate()));
		return results;
	}
}
