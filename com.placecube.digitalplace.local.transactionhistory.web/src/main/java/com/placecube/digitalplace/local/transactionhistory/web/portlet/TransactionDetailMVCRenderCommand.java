package com.placecube.digitalplace.local.transactionhistory.web.portlet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.transactionhistory.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormFieldEntry;
import com.placecube.digitalplace.local.transactionhistory.web.model.ModelFactoryBuilder;
import com.placecube.digitalplace.local.transactionhistory.web.service.FormValuesService;
import com.placecube.digitalplace.local.transactionhistory.web.service.TransactionHistoryService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.TRANSACTION_HISTORY, "mvc.command.name=" + MVCCommandKeys.VIEW_TRANSACTION_DETAILS }, service = MVCRenderCommand.class)
public class TransactionDetailMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private FormValuesService formValuesService;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	@Reference
	private TransactionHistoryService transactionHistoryService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			long classNameId = ParamUtil.getLong(renderRequest, PortletRequestKeys.CLASS_NAME_ID);
			long classPK = ParamUtil.getLong(renderRequest, PortletRequestKeys.CLASS_PK);

			DDMFormValues ddmFormValues = formValuesService.getDDMFormValues(classNameId, classPK);

			List<FormFieldEntry> formFieldEntries = getFormFieldEntries(renderRequest, ddmFormValues.getDDMForm(), ddmFormValues);

			renderRequest.setAttribute(PortletRequestKeys.FORM_FIELD_ENTRIES, formFieldEntries);

		} catch (Exception e) {
			throw new PortletException("Unable to create form field entries.", e);
		}

		return "/transaction-details.jsp";
	}

	private List<FormFieldEntry> getFormFieldEntries(RenderRequest renderRequest, DDMForm ddmForm, DDMFormValues ddmFormValues) {

		Map<String, DDMFormField> ddmFormFieldsMap = ddmForm.getDDMFormFieldsMap(false);
		Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = ddmFormValues.getDDMFormFieldValuesMap();

		List<FormFieldEntry> formFieldEntries = new ArrayList<>();

		for (Entry<String, DDMFormField> ddmFormFieldEntry : ddmFormFieldsMap.entrySet()) {

			List<DDMFormFieldValue> formFieldValues = ddmFormFieldValuesMap.get(ddmFormFieldEntry.getKey());
			Locale locale = renderRequest.getLocale();
			String fieldValue = transactionHistoryService.getFieldValue(locale, ddmFormFieldEntry.getValue(), formFieldValues);

			if (Validator.isNotNull(fieldValue)) {
				String fieldLabel = transactionHistoryService.getFieldLabel(locale, ddmFormFieldEntry.getValue());
				formFieldEntries.add(modelFactoryBuilder.createFormFieldEntry(fieldLabel, fieldValue));
			}
		}

		return formFieldEntries;
	}

}
