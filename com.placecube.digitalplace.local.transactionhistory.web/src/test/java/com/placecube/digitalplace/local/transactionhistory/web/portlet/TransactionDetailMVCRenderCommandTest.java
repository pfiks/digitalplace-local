package com.placecube.digitalplace.local.transactionhistory.web.portlet;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormFieldEntry;
import com.placecube.digitalplace.local.transactionhistory.web.model.ModelFactoryBuilder;
import com.placecube.digitalplace.local.transactionhistory.web.service.FormValuesService;
import com.placecube.digitalplace.local.transactionhistory.web.service.TransactionHistoryService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil" })
public class TransactionDetailMVCRenderCommandTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 10;
	private static final long CLASS_PK = 20;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private DDMFormField mockDDMFormField1;

	@Mock
	private DDMFormField mockDDMFormField2;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue1;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue2;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue3;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private FormFieldEntry mockFormFieldEntry;

	@Mock
	private FormValuesService mockFormValuesService;

	@Mock
	private Locale mockLocale;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private TransactionHistoryService mockTransactionHistoryService;

	@InjectMocks
	private TransactionDetailMVCRenderCommand transactionDetailMVCRenderCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionGettingDDMFormValues_ThenPortletExceptionIsThrownAndRequestAttributeIsNotSet() throws Exception {

		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.CLASS_NAME_ID)).thenReturn(CLASS_NAME_ID);
		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.CLASS_PK)).thenReturn(CLASS_PK);

		doThrow(new PortalException()).when(mockFormValuesService).getDDMFormValues(CLASS_NAME_ID, CLASS_PK);

		transactionDetailMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(anyString(), any());
	}

	@Test
	public void render_WhenNoError_ThenFormFieldEntriesWithAValueAreSetAsRequestAttributeAndTransactionDetailsJSPIsReturned() throws Exception {

		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.CLASS_NAME_ID)).thenReturn(CLASS_NAME_ID);
		when(ParamUtil.getLong(mockRenderRequest, PortletRequestKeys.CLASS_PK)).thenReturn(CLASS_PK);

		when(mockFormValuesService.getDDMFormValues(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMForm()).thenReturn(mockDDMForm);

		Map<String, DDMFormField> ddmFormFieldsMap = new HashMap<>();
		ddmFormFieldsMap.put("keyOne", mockDDMFormField1);
		ddmFormFieldsMap.put("keyTwo", mockDDMFormField2);
		when(mockDDMForm.getDDMFormFieldsMap(false)).thenReturn(ddmFormFieldsMap);

		List<DDMFormFieldValue> formFieldValues1 = Collections.singletonList(mockDDMFormFieldValue1);
		List<DDMFormFieldValue> formFieldValues2 = Arrays.asList(mockDDMFormFieldValue2, mockDDMFormFieldValue3);
		Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = new HashMap<>();
		ddmFormFieldValuesMap.put("keyOne", formFieldValues1);
		ddmFormFieldValuesMap.put("keyTwo", formFieldValues2);
		when(mockDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(ddmFormFieldValuesMap);
		when(mockRenderRequest.getLocale()).thenReturn(mockLocale);
		when(mockTransactionHistoryService.getFieldValue(mockLocale, mockDDMFormField1, formFieldValues1)).thenReturn("field1ValueOne");
		when(mockTransactionHistoryService.getFieldLabel(mockLocale, mockDDMFormField1)).thenReturn("field1Label1");
		when(mockModelFactoryBuilder.createFormFieldEntry("field1Label1", "field1ValueOne")).thenReturn(mockFormFieldEntry);
		when(mockTransactionHistoryService.getFieldValue(mockLocale, mockDDMFormField2, formFieldValues2)).thenReturn(" ");

		List<FormFieldEntry> expectedValues = new ArrayList<>();
		expectedValues.add(mockFormFieldEntry);

		String result = transactionDetailMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertEquals("/transaction-details.jsp", result);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.FORM_FIELD_ENTRIES, expectedValues);
	}
}
