package com.placecube.digitalplace.local.transactionhistory.web.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeServicesRegistry;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldValueRenderer;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.petra.string.StringUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormEntryDetail;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class, StringUtil.class })
public class TransactionHistoryServiceTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 10;
	private static final long CLASS_PK = 20;
	private static final long COMPANY_ID = 50;
	private static final String FIELD_LABEL = "fieldLabel";
	private static final String FIELD_VALUE_1 = "fieldValue1";
	private static final String FIELD_VALUE_2 = "fieldValue2";
	private static final String FIELD_VALUE_3 = "fieldValue3";
	private static final long USER_ID = 12;

	@Mock
	private Criterion mockCriterionCompanyId;

	@Mock
	private Criterion mockCriterionUserId;

	@Mock
	private Date mockDate;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldTypeServicesRegistry mockDDMFormFieldTypeServicesRegistry;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue1;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue2;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue3;

	@Mock
	private DDMFormFieldValueRenderer mockDDMFormFieldValueRenderer;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private FormEntryDetail mockFormEntryDetail;

	@Mock
	private FormEntryDetail mockFormEntryDetail2;

	@Mock
	private FormEntryDetail mockFormEntryDetail3;

	@Mock
	private FormEntryModelService mockFormEntryModelService;

	@Mock
	private Locale mockLocale;

	@Mock
	private LocalizedValue mockLocalizedValue;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockPortletRequest;

	@Mock
	private TransactionHistory mockTransactionHistory;

	@Mock
	private TransactionHistoryLocalService mockTransactionHistoryLocalService;

	@Mock
	private User mockUser;

	@InjectMocks
	private TransactionHistoryService transactionHistoryService;

	@Before
	public void activateSetUp() {
		mockStatic(RestrictionsFactoryUtil.class, StringUtil.class);
	}

	@Test
	public void getFieldLabel_WhenNoError_ThenReturnsFieldLabelAsString() {
		when(mockDDMFormField.getLabel()).thenReturn(mockLocalizedValue);
		when(mockLocalizedValue.getString(mockLocale)).thenReturn(FIELD_LABEL);

		String result = transactionHistoryService.getFieldLabel(mockLocale, mockDDMFormField);

		assertEquals(FIELD_LABEL, result);
	}

	@Test
	public void getFieldValue_WhenError_ThenReturnsEmptyString() {
		List<DDMFormFieldValue> formFieldValues = new ArrayList<>();
		formFieldValues.add(mockDDMFormFieldValue1);
		when(mockDDMFormField.getType()).thenReturn("fieldType");
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(mockDDMFormField.getType())).thenReturn(mockDDMFormFieldValueRenderer);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValue1, mockLocale)).thenThrow(NullPointerException.class);

		String result = transactionHistoryService.getFieldValue(mockLocale, mockDDMFormField, formFieldValues);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void getFieldValue_WhenNoError_ThenReturnsFieldValuesAsString() {
		String expected = FIELD_VALUE_1 + StringPool.COMMA_AND_SPACE + FIELD_VALUE_2 + StringPool.COMMA_AND_SPACE + FIELD_VALUE_3;

		List<DDMFormFieldValue> formFieldValues = new ArrayList<>();
		formFieldValues.add(mockDDMFormFieldValue1);
		formFieldValues.add(mockDDMFormFieldValue2);
		formFieldValues.add(mockDDMFormFieldValue3);

		when(mockDDMFormField.getType()).thenReturn("fieldType");
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(mockDDMFormField.getType())).thenReturn(mockDDMFormFieldValueRenderer);

		List<String> renderedFormFielValues = new ArrayList<>();
		renderedFormFielValues.add(FIELD_VALUE_1);
		renderedFormFielValues.add(FIELD_VALUE_2);
		renderedFormFielValues.add(FIELD_VALUE_3);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValue1, mockLocale)).thenReturn(FIELD_VALUE_1);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValue2, mockLocale)).thenReturn(FIELD_VALUE_2);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValue3, mockLocale)).thenReturn(FIELD_VALUE_3);

		when(StringUtil.merge(renderedFormFielValues, StringPool.COMMA_AND_SPACE)).thenReturn(expected);

		String result = transactionHistoryService.getFieldValue(mockLocale, mockDDMFormField, formFieldValues);

		assertEquals(expected, result);
	}

	@Test
	public void getFieldValue_WhenNullDDMFormField_ThenReturnsEmptyString() {
		List<DDMFormFieldValue> formFieldValues = new ArrayList<>();

		String result = transactionHistoryService.getFieldValue(mockLocale, null, formFieldValues);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void getFieldValue_WhenNullDDMFormFieldValues_ThenReturnsEmptyString() {

		String result = transactionHistoryService.getFieldValue(mockLocale, mockDDMFormField, null);

		assertEquals(StringPool.BLANK, result);
	}

	@Test(expected = PortalException.class)
	public void getFormEntryDetail_WhenExceptionRetrievingTheBasicFormEntryDetail_ThenThrowPortalException() throws Exception {

		when(mockTransactionHistory.getClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockTransactionHistory.getClassPK()).thenReturn(CLASS_PK);

		doThrow(new PortalException()).when(mockFormEntryModelService).getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK);

		transactionHistoryService.getFormEntryDetail(mockPortletRequest, mockTransactionHistory, mockLocale);
	}

	@Test
	public void getFormEntryDetail_WhenNoError_ThenReturnFormEntryDetailConfigured() throws Exception {

		when(mockTransactionHistory.getClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockTransactionHistory.getClassPK()).thenReturn(CLASS_PK);
		when(mockFormEntryModelService.getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK)).thenReturn(mockFormEntryDetail);

		FormEntryDetail result = transactionHistoryService.getFormEntryDetail(mockPortletRequest, mockTransactionHistory, mockLocale);

		assertThat(result, sameInstance(mockFormEntryDetail));
		verify(mockFormEntryModelService, times(1)).configureFormEntryDetails(mockFormEntryDetail, mockTransactionHistory);
	}

	@Test
	public void getFormSubmissionsForUser_WhenNoError_ThenConfigureAndExecuteDynamicQuery() {

		when(mockTransactionHistoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(RestrictionsFactoryUtil.eq(PortletRequestKeys.COMPANY_ID, COMPANY_ID)).thenReturn(mockCriterionCompanyId);
		when(RestrictionsFactoryUtil.eq(PortletRequestKeys.USER_ID, USER_ID)).thenReturn(mockCriterionUserId);

		transactionHistoryService.getFormSubmissionsForUser(mockUser);

		InOrder inOrder = Mockito.inOrder(mockDynamicQuery, mockTransactionHistoryLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionCompanyId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionUserId);
		inOrder.verify(mockTransactionHistoryLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

	@Test
	public void getFormSubmissionsForUser_WhenNoError_ThenReturnTransactionHistoryRecordsForTheUser() {
		List<Object> expected = new ArrayList<>();
		expected.add(mockTransactionHistory);

		when(mockTransactionHistoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockTransactionHistoryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);

		List<TransactionHistory> results = transactionHistoryService.getFormSubmissionsForUser(mockUser);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void getOrderedResults_WhenNoError_ThenReturnsTheResultsOrderedByDateDesc() {
		when(mockFormEntryDetail.getSubmitDate()).thenReturn(new Date(222));
		when(mockFormEntryDetail2.getSubmitDate()).thenReturn(new Date(999));
		when(mockFormEntryDetail3.getSubmitDate()).thenReturn(new Date(111));

		List<FormEntryDetail> formEntryDetails = new ArrayList<>();
		formEntryDetails.add(mockFormEntryDetail);
		formEntryDetails.add(mockFormEntryDetail2);
		formEntryDetails.add(mockFormEntryDetail3);

		List<FormEntryDetail> results = transactionHistoryService.getOrderedResults(formEntryDetails);

		assertThat(results, contains(mockFormEntryDetail2, mockFormEntryDetail, mockFormEntryDetail3));
	}
}
