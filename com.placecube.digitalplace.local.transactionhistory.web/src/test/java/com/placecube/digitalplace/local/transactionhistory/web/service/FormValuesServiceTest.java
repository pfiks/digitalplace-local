package com.placecube.digitalplace.local.transactionhistory.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Portal;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AssetRendererFactoryRegistryUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil" })
public class FormValuesServiceTest extends PowerMockito {

	private static final String CLASS_NAME = "CLASS_NAME";

	private static final long CLASS_NAME_ID = 10;

	private static final long CLASS_PK = 20;

	private static final String FORM_NAME = "FORM_NAME";

	@InjectMocks
	private FormValuesService formValuesService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetRenderer mockAssetRenderer;

	@Mock
	private AssetRendererFactory mockAssetRendererFactory;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private Locale mockLocale;

	@Mock
	private Portal mockPortal;

	@Before
	public void activateSetUp() {
		mockStatic(AssetRendererFactoryRegistryUtil.class);
	}

	@Test(expected = PortalException.class)
	public void getAssetEntryByClassNameIdAndClassPK_WhenErrorGettingAssetEntry_ThenPortalExceptionIsThrown() throws PortalException {

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(CLASS_NAME_ID)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(CLASS_NAME_ID)).thenReturn(CLASS_NAME);
		doThrow(new PortalException()).when(mockAssetRendererFactory).getAssetEntry(CLASS_NAME, CLASS_PK);

		formValuesService.getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK);
	}

	@Test
	public void getAssetEntryByClassNameIdAndClassPK_WhenNoError_ThenReturnAssetEntry() throws PortalException {

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(CLASS_NAME_ID)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(CLASS_NAME_ID)).thenReturn(CLASS_NAME);
		when(mockAssetRendererFactory.getAssetEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		AssetEntry result = formValuesService.getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK);

		assertThat(result, sameInstance(mockAssetEntry));
	}

	@Test(expected = PortalException.class)
	public void getDDMFormValues_WhenErrorGettingAssetEntry_ThenPortalExceptionIsThrown() throws PortalException {

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(CLASS_NAME_ID)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(CLASS_NAME_ID)).thenReturn(CLASS_NAME);
		doThrow(new PortalException()).when(mockAssetRendererFactory).getAssetEntry(CLASS_NAME, CLASS_PK);

		formValuesService.getDDMFormValues(CLASS_NAME_ID, CLASS_PK);
	}

	@Test
	public void getDDMFormValues_WhenIsDDLRecord_ThenReturnDDMFormValues() throws PortalException {

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(CLASS_NAME_ID)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(CLASS_NAME_ID)).thenReturn(CLASS_NAME);
		when(mockAssetRendererFactory.getAssetEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		when(mockAssetEntry.getClassName()).thenReturn(DDLRecord.class.getName());
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);

		DDMFormValues result = formValuesService.getDDMFormValues(CLASS_NAME_ID, CLASS_PK);

		assertThat(result, sameInstance(mockDDMFormValues));
	}

	@Test
	public void getDDMFormValues_WhenIsDDMFormInstanceRecord_ThenReturnDDMFormValues() throws PortalException {

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(CLASS_NAME_ID)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(CLASS_NAME_ID)).thenReturn(CLASS_NAME);
		when(mockAssetRendererFactory.getAssetEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		when(mockAssetEntry.getClassName()).thenReturn(DDMFormInstanceRecord.class.getName());
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);

		DDMFormValues result = formValuesService.getDDMFormValues(CLASS_NAME_ID, CLASS_PK);

		assertThat(result, sameInstance(mockDDMFormValues));
	}

	@Test(expected = PortalException.class)
	public void getDDMFormValues_WhenIsNotDDMFormInstanceRecordOrDDLRecord_ThenPortalExceptionIsThrown() throws PortalException {

		when(AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassNameId(CLASS_NAME_ID)).thenReturn(mockAssetRendererFactory);
		when(mockPortal.getClassName(CLASS_NAME_ID)).thenReturn(CLASS_NAME);
		when(mockAssetRendererFactory.getAssetEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		when(mockAssetEntry.getClassName()).thenReturn(Class.class.getName());

		formValuesService.getDDMFormValues(CLASS_NAME_ID, CLASS_PK);
	}

	@Test(expected = PortalException.class)
	public void getFormName_WhenIsDDLRecordAndErrorGettingRecordSet_ThenPortalExceptionIsThrown() throws PortalException {

		when(mockAssetEntry.getClassName()).thenReturn(DDLRecord.class.getName());
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockDDLRecord);
		doThrow(new PortalException()).when(mockDDLRecord).getRecordSet();

		formValuesService.getFormName(mockAssetEntry, mockLocale);
	}

	@Test
	public void getFormName_WhenIsDDLRecordAndNoError_ThenReturnNameFromDDLRecordSet() throws PortalException {

		when(mockAssetEntry.getClassName()).thenReturn(DDLRecord.class.getName());
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getName(mockLocale)).thenReturn(FORM_NAME);

		String result = formValuesService.getFormName(mockAssetEntry, mockLocale);

		assertEquals(FORM_NAME, result);
	}

	@Test(expected = PortalException.class)
	public void getFormName_WhenIsDDMFormInstanceRecordAndErrorGettingFormInstance_ThenPortalExceptionIsThrown() throws PortalException {

		when(mockAssetEntry.getClassName()).thenReturn(DDMFormInstanceRecord.class.getName());
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockDDMFormInstanceRecord);
		doThrow(new PortalException()).when(mockDDMFormInstanceRecord).getFormInstance();

		formValuesService.getFormName(mockAssetEntry, mockLocale);
	}

	@Test
	public void getFormName_WhenIsDDMFormInstanceRecordAndNoError_ThenReturnNameFromDDMFormInstance() throws PortalException {

		when(mockAssetEntry.getClassName()).thenReturn(DDMFormInstanceRecord.class.getName());
		when(mockAssetEntry.getAssetRenderer()).thenReturn(mockAssetRenderer);
		when(mockAssetRenderer.getAssetObject()).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getName(mockLocale)).thenReturn(FORM_NAME);

		String result = formValuesService.getFormName(mockAssetEntry, mockLocale);

		assertEquals(FORM_NAME, result);
	}

	@Test(expected = PortalException.class)
	public void getFormName_WhenIsNotDDMFormInstanceRecordOrDDLRecord_ThenPortalExceptionIsThrown() throws PortalException {

		when(mockAssetEntry.getClassName()).thenReturn(Class.class.getName());

		formValuesService.getFormName(mockAssetEntry, mockLocale);
	}

}
