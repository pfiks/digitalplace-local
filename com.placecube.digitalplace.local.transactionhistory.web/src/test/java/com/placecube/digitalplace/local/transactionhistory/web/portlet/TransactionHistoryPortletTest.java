package com.placecube.digitalplace.local.transactionhistory.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormEntryDetail;
import com.placecube.digitalplace.local.transactionhistory.web.service.TransactionHistoryService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class TransactionHistoryPortletTest extends PowerMockito {

	private List<FormEntryDetail> formDetails;

	private List<TransactionHistory> formSubmissions;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord1;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord2;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord3;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord4;

	@Mock
	private FormEntryDetail mockFormEntryDetail1;

	@Mock
	private FormEntryDetail mockFormEntryDetail2;

	@Mock
	private FormEntryDetail mockFormEntryDetail3;

	@Mock
	private List<FormEntryDetail> mockFormEntryDetails;

	@Mock
	private Locale mockLocale;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TransactionHistory mockTransactionHistory1;

	@Mock
	private TransactionHistory mockTransactionHistory2;

	@Mock
	private TransactionHistory mockTransactionHistory3;

	@Mock
	private TransactionHistory mockTransactionHistory4;

	@Mock
	private TransactionHistoryService mockTransactionHistoryService;

	@Mock
	private User mockUser;

	@InjectMocks
	private TransactionHistoryPortlet transactionHistoryPortlet;

	@Before
	public void activateSetUp() throws Exception {
		mockCallToSuper();
		formSubmissions = new LinkedList<>();
		formDetails = new LinkedList<>();
	}

	@Test
	public void render_WhenNoError_ThenSetTheFormEntriesAsRequestAttribute() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockUser.getLocale()).thenReturn(mockLocale);
		when(mockTransactionHistoryService.getFormSubmissionsForUser(mockUser)).thenReturn(formSubmissions);

		mockFormEntry(mockTransactionHistory1, mockFormEntryDetail1);
		mockFormEntry(mockTransactionHistory2, mockFormEntryDetail2);
		mockFormEntry(mockTransactionHistory3, mockFormEntryDetail3);

		when(mockTransactionHistoryService.getFormEntryDetail(mockRenderRequest, mockTransactionHistory4, mockLocale)).thenThrow(new PortalException());

		formSubmissions.add(mockTransactionHistory4);

		when(mockTransactionHistoryService.getOrderedResults(formDetails)).thenReturn(mockFormEntryDetails);

		transactionHistoryPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.FORM_ENTRIES, mockFormEntryDetails);
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

	private void mockFormEntry(TransactionHistory transactionHistory, FormEntryDetail formEntryDetail) throws PortalException {
		when(mockTransactionHistoryService.getFormEntryDetail(mockRenderRequest, transactionHistory, mockLocale)).thenReturn(formEntryDetail);
		formSubmissions.add(transactionHistory);
		formDetails.add(formEntryDetail);
	}
}
