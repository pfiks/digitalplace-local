package com.placecube.digitalplace.local.transactionhistory.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.WindowStateException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.workflow.WorkflowLog;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletKeys;
import com.placecube.digitalplace.local.transactionhistory.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.local.transactionhistory.web.model.FormEntryDetail;
import com.placecube.digitalplace.local.transactionhistory.web.model.ModelFactoryBuilder;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class FormEntryModelServiceTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 10;
	private static final long CLASS_PK = 20;
	private static final String FORM_NAME = "myFormNameValue";
	private static final String VIEW_URL = "myViewURLValue";

	@InjectMocks
	private FormEntryModelService formEntryModelService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private OrderByComparator<WorkflowLog> mockComparator;

	@Mock
	private Date mockDate;

	@Mock
	private Date mockDate2;

	@Mock
	private FormEntryDetail mockFormEntryDetail;

	@Mock
	private FormValuesService mockFormValuesService;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private Locale mockLocale;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletURLFactory mockPortletURLFactory;

	@Mock
	private TransactionHistory mockTransactionHistory;

	@Test
	@Parameters({ "submitted", "other" })
	public void configureFormEntryDetails_WhenNoError_ThenUpdateFormEntryDetailsWithTransactionHistoryData(String status) {

		when(mockTransactionHistory.getTransactionStatus()).thenReturn(status);
		when(mockTransactionHistory.getModifiedDate()).thenReturn(mockDate);

		boolean isSubmitted = "submitted".equalsIgnoreCase(status);

		formEntryModelService.configureFormEntryDetails(mockFormEntryDetail, mockTransactionHistory);

		verify(mockModelFactoryBuilder, times(1)).updateFormEntryDetails(mockFormEntryDetail, status, mockDate, isSubmitted);
	}

	@Test(expected = PortalException.class)
	public void getBasicFormEntryDetail_WhenExceptionGettingAssetEntry_ThenThrowPortalException() throws Exception {

		doThrow(new PortalException()).when(mockFormValuesService).getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK);

		formEntryModelService.getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK);

		verify(mockModelFactoryBuilder, never()).createFormEntryDetail(anyString(), any(Date.class), anyString());
	}

	@Test(expected = PortalException.class)
	public void getBasicFormEntryDetail_WhenExceptionGettingFormName_ThenThrowPortalException() throws PortalException {

		when(mockFormValuesService.getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockAssetEntry);
		doThrow(new PortalException()).when(mockFormValuesService).getFormName(mockAssetEntry, mockLocale);

		formEntryModelService.getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK);
	}

	@Test(expected = PortalException.class)
	public void getBasicFormEntryDetail_WhenExceptionSettingWindowState_ThenThrowPortalException() throws Exception {

		when(mockFormValuesService.getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockAssetEntry);
		when(mockFormValuesService.getFormName(mockAssetEntry, mockLocale)).thenReturn(FORM_NAME);

		when(mockPortletURLFactory.create(mockPortletRequest, PortletKeys.TRANSACTION_HISTORY, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		doThrow(new WindowStateException("msg", null)).when(mockLiferayPortletURL).setWindowState(LiferayWindowState.POP_UP);

		formEntryModelService.getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK);
	}

	@Test
	public void getBasicFormEntryDetail_WhenNoError_ThenConfigureTheViewURL() throws Exception {

		when(mockFormValuesService.getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockAssetEntry);
		when(mockFormValuesService.getFormName(mockAssetEntry, mockLocale)).thenReturn(FORM_NAME);

		when(mockPortletURLFactory.create(mockPortletRequest, PortletKeys.TRANSACTION_HISTORY, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn(VIEW_URL);
		when(mockAssetEntry.getCreateDate()).thenReturn(mockDate);

		formEntryModelService.getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK);

		InOrder inOrder = Mockito.inOrder(mockLiferayPortletURL, mockModelFactoryBuilder);
		inOrder.verify(mockLiferayPortletURL, times(1)).setWindowState(LiferayWindowState.POP_UP);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.VIEW_TRANSACTION_DETAILS);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter(PortletRequestKeys.CLASS_NAME_ID, String.valueOf(CLASS_NAME_ID));
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter(PortletRequestKeys.CLASS_PK, String.valueOf(CLASS_PK));
		inOrder.verify(mockModelFactoryBuilder, times(1)).createFormEntryDetail(FORM_NAME, mockDate, VIEW_URL);
	}

	@Test
	public void getBasicFormEntryDetail_WhenNoError_ThenReturnFormEntryDetails() throws Exception {

		when(mockFormValuesService.getAssetEntryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockAssetEntry);
		when(mockFormValuesService.getFormName(mockAssetEntry, mockLocale)).thenReturn(FORM_NAME);

		when(mockPortletURLFactory.create(mockPortletRequest, PortletKeys.TRANSACTION_HISTORY, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn(VIEW_URL);
		when(mockAssetEntry.getCreateDate()).thenReturn(mockDate);

		when(mockModelFactoryBuilder.createFormEntryDetail(FORM_NAME, mockDate, VIEW_URL)).thenReturn(mockFormEntryDetail);

		FormEntryDetail result = formEntryModelService.getBasicFormEntryDetail(mockPortletRequest, mockLocale, CLASS_NAME_ID, CLASS_PK);

		assertThat(result, sameInstance(mockFormEntryDetail));
	}
}
