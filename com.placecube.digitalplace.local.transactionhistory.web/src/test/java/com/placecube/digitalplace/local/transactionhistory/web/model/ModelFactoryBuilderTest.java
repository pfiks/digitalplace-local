package com.placecube.digitalplace.local.transactionhistory.web.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ModelFactoryBuilderTest extends PowerMockito {

	private static final String FIELD_LABEL = "fieldLabel";
	private static final String FIELD_VALUE = "fieldValue";
	private static final String NAME = "nameValue";
	private static final String VIEW_URL = "urlValue";

	@Mock
	private Date mockDate;

	@Mock
	private FormEntryDetail mockFormEntryDetail;

	@InjectMocks
	private ModelFactoryBuilder modelFactoryBuilder;

	@Test
	public void createFormEntryDetail_WhenNoError_ThenReturnsFormEntryDetailWithFormNameConfigured() {
		FormEntryDetail result = modelFactoryBuilder.createFormEntryDetail(NAME, mockDate, VIEW_URL);

		assertThat(result.getFormName(), equalTo(NAME));
	}

	@Test
	public void createFormEntryDetail_WhenNoError_ThenReturnsFormEntryDetailWithLastUpdateDateConfiguredWithTheSubmitDateValue() {
		FormEntryDetail result = modelFactoryBuilder.createFormEntryDetail(NAME, mockDate, VIEW_URL);

		assertThat(result.getLastUpdateDate(), sameInstance(mockDate));
	}

	@Test
	public void createFormEntryDetail_WhenNoError_ThenReturnsFormEntryDetailWithSubmitDateConfigured() {
		FormEntryDetail result = modelFactoryBuilder.createFormEntryDetail(NAME, mockDate, VIEW_URL);

		assertThat(result.getSubmitDate(), sameInstance(mockDate));
	}

	@Test
	public void createFormEntryDetail_WhenNoError_ThenReturnsFormEntryDetailWithSubmittedConfiguredToTrue() {
		FormEntryDetail result = modelFactoryBuilder.createFormEntryDetail(NAME, mockDate, VIEW_URL);

		assertTrue(result.isSubmitted());
	}

	@Test
	public void createFormEntryDetail_WhenNoError_ThenReturnsFormEntryDetailWithViewURLConfigured() {
		FormEntryDetail result = modelFactoryBuilder.createFormEntryDetail(NAME, mockDate, VIEW_URL);

		assertThat(result.getViewURL(), equalTo(VIEW_URL));
	}

	@Test
	public void createFormEntryDetail_WhenNoError_ThenReturnsFormEntryDetailWithWorkflowConfiguredToFalse() {
		FormEntryDetail result = modelFactoryBuilder.createFormEntryDetail(NAME, mockDate, VIEW_URL);

		assertFalse(result.isWorkflow());
	}

	@Test
	public void createFormFieldEntry_WhenNoError_ThenReturnsFormEntryDetailWithfieldLabel() {
		FormFieldEntry result = modelFactoryBuilder.createFormFieldEntry(FIELD_LABEL, FIELD_VALUE);

		assertSame(FIELD_LABEL, result.getFieldLabel());
	}

	@Test
	public void createFormFieldEntry_WhenNoError_ThenReturnsFormEntryDetailWithfieldValue() {
		FormFieldEntry result = modelFactoryBuilder.createFormFieldEntry(FIELD_LABEL, FIELD_VALUE);

		assertSame(FIELD_VALUE, result.getFieldValue());
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateFormEntryDetails_WhenNoError_ThenUpdateFormEntryDetails(boolean isSubmitted) {
		String status = "myStatusValue";

		modelFactoryBuilder.updateFormEntryDetails(mockFormEntryDetail, status, mockDate, isSubmitted);

		verify(mockFormEntryDetail, times(1)).setTransactionStatus(status);
		verify(mockFormEntryDetail, times(1)).setLastUpdateDate(mockDate);
		verify(mockFormEntryDetail, times(1)).setSubmitted(isSubmitted);
		verifyNoMoreInteractions(mockFormEntryDetail);
	}
}
