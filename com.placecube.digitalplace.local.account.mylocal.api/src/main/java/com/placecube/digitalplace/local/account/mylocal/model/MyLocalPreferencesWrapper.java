/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MyLocalPreferences}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferences
 * @generated
 */
public class MyLocalPreferencesWrapper
	extends BaseModelWrapper<MyLocalPreferences>
	implements ModelWrapper<MyLocalPreferences>, MyLocalPreferences {

	public MyLocalPreferencesWrapper(MyLocalPreferences myLocalPreferences) {
		super(myLocalPreferences);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("preferencesId", getPreferencesId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("plid", getPlid());
		attributes.put("portletId", getPortletId());
		attributes.put("preferences", getPreferences());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long preferencesId = (Long)attributes.get("preferencesId");

		if (preferencesId != null) {
			setPreferencesId(preferencesId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long plid = (Long)attributes.get("plid");

		if (plid != null) {
			setPlid(plid);
		}

		String portletId = (String)attributes.get("portletId");

		if (portletId != null) {
			setPortletId(portletId);
		}

		String preferences = (String)attributes.get("preferences");

		if (preferences != null) {
			setPreferences(preferences);
		}
	}

	@Override
	public MyLocalPreferences cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this my local preferences.
	 *
	 * @return the company ID of this my local preferences
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this my local preferences.
	 *
	 * @return the create date of this my local preferences
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the group ID of this my local preferences.
	 *
	 * @return the group ID of this my local preferences
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this my local preferences.
	 *
	 * @return the modified date of this my local preferences
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the plid of this my local preferences.
	 *
	 * @return the plid of this my local preferences
	 */
	@Override
	public long getPlid() {
		return model.getPlid();
	}

	/**
	 * Returns the portlet ID of this my local preferences.
	 *
	 * @return the portlet ID of this my local preferences
	 */
	@Override
	public String getPortletId() {
		return model.getPortletId();
	}

	/**
	 * Returns the preferences of this my local preferences.
	 *
	 * @return the preferences of this my local preferences
	 */
	@Override
	public String getPreferences() {
		return model.getPreferences();
	}

	/**
	 * Returns the preferences ID of this my local preferences.
	 *
	 * @return the preferences ID of this my local preferences
	 */
	@Override
	public long getPreferencesId() {
		return model.getPreferencesId();
	}

	/**
	 * Returns the primary key of this my local preferences.
	 *
	 * @return the primary key of this my local preferences
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this my local preferences.
	 *
	 * @return the user ID of this my local preferences
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this my local preferences.
	 *
	 * @return the user name of this my local preferences
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this my local preferences.
	 *
	 * @return the user uuid of this my local preferences
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this my local preferences.
	 *
	 * @return the uuid of this my local preferences
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this my local preferences.
	 *
	 * @param companyId the company ID of this my local preferences
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this my local preferences.
	 *
	 * @param createDate the create date of this my local preferences
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this my local preferences.
	 *
	 * @param groupId the group ID of this my local preferences
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this my local preferences.
	 *
	 * @param modifiedDate the modified date of this my local preferences
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the plid of this my local preferences.
	 *
	 * @param plid the plid of this my local preferences
	 */
	@Override
	public void setPlid(long plid) {
		model.setPlid(plid);
	}

	/**
	 * Sets the portlet ID of this my local preferences.
	 *
	 * @param portletId the portlet ID of this my local preferences
	 */
	@Override
	public void setPortletId(String portletId) {
		model.setPortletId(portletId);
	}

	/**
	 * Sets the preferences of this my local preferences.
	 *
	 * @param preferences the preferences of this my local preferences
	 */
	@Override
	public void setPreferences(String preferences) {
		model.setPreferences(preferences);
	}

	/**
	 * Sets the preferences ID of this my local preferences.
	 *
	 * @param preferencesId the preferences ID of this my local preferences
	 */
	@Override
	public void setPreferencesId(long preferencesId) {
		model.setPreferencesId(preferencesId);
	}

	/**
	 * Sets the primary key of this my local preferences.
	 *
	 * @param primaryKey the primary key of this my local preferences
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this my local preferences.
	 *
	 * @param userId the user ID of this my local preferences
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this my local preferences.
	 *
	 * @param userName the user name of this my local preferences
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this my local preferences.
	 *
	 * @param userUuid the user uuid of this my local preferences
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this my local preferences.
	 *
	 * @param uuid the uuid of this my local preferences
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected MyLocalPreferencesWrapper wrap(
		MyLocalPreferences myLocalPreferences) {

		return new MyLocalPreferencesWrapper(myLocalPreferences);
	}

}