/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link MyLocalPreferencesLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferencesLocalService
 * @generated
 */
public class MyLocalPreferencesLocalServiceWrapper
	implements MyLocalPreferencesLocalService,
			   ServiceWrapper<MyLocalPreferencesLocalService> {

	public MyLocalPreferencesLocalServiceWrapper() {
		this(null);
	}

	public MyLocalPreferencesLocalServiceWrapper(
		MyLocalPreferencesLocalService myLocalPreferencesLocalService) {

		_myLocalPreferencesLocalService = myLocalPreferencesLocalService;
	}

	/**
	 * Adds the my local preferences to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was added
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences addMyLocalPreferences(
				com.placecube.digitalplace.local.account.mylocal.model.
					MyLocalPreferences myLocalPreferences) {

		return _myLocalPreferencesLocalService.addMyLocalPreferences(
			myLocalPreferences);
	}

	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences addPreferences(
					long companyId, long groupId, long userId, long plid,
					String portletId, String preferencesBody)
				throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.addPreferences(
			companyId, groupId, userId, plid, portletId, preferencesBody);
	}

	/**
	 * Creates a new my local preferences with the primary key. Does not add the my local preferences to the database.
	 *
	 * @param preferencesId the primary key for the new my local preferences
	 * @return the new my local preferences
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences createMyLocalPreferences(long preferencesId) {

		return _myLocalPreferencesLocalService.createMyLocalPreferences(
			preferencesId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws PortalException if a my local preferences with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences deleteMyLocalPreferences(long preferencesId)
				throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.deleteMyLocalPreferences(
			preferencesId);
	}

	/**
	 * Deletes the my local preferences from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was removed
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences deleteMyLocalPreferences(
				com.placecube.digitalplace.local.account.mylocal.model.
					MyLocalPreferences myLocalPreferences) {

		return _myLocalPreferencesLocalService.deleteMyLocalPreferences(
			myLocalPreferences);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _myLocalPreferencesLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _myLocalPreferencesLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _myLocalPreferencesLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _myLocalPreferencesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _myLocalPreferencesLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _myLocalPreferencesLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _myLocalPreferencesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _myLocalPreferencesLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences fetchMyLocalPreferences(long preferencesId) {

		return _myLocalPreferencesLocalService.fetchMyLocalPreferences(
			preferencesId);
	}

	/**
	 * Returns the my local preferences matching the UUID and group.
	 *
	 * @param uuid the my local preferences's UUID
	 * @param groupId the primary key of the group
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences fetchMyLocalPreferencesByUuidAndGroupId(
				String uuid, long groupId) {

		return _myLocalPreferencesLocalService.
			fetchMyLocalPreferencesByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences fetchPreferencesByGroupIdUserIdPlidAndPortletId(
				long groupId, long userId, long plid, String portletId) {

		return _myLocalPreferencesLocalService.
			fetchPreferencesByGroupIdUserIdPlidAndPortletId(
				groupId, userId, plid, portletId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _myLocalPreferencesLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _myLocalPreferencesLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _myLocalPreferencesLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the my local preferences with the primary key.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws PortalException if a my local preferences with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences getMyLocalPreferences(long preferencesId)
				throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.getMyLocalPreferences(
			preferencesId);
	}

	/**
	 * Returns the my local preferences matching the UUID and group.
	 *
	 * @param uuid the my local preferences's UUID
	 * @param groupId the primary key of the group
	 * @return the matching my local preferences
	 * @throws PortalException if a matching my local preferences could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences getMyLocalPreferencesByUuidAndGroupId(
					String uuid, long groupId)
				throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.
			getMyLocalPreferencesByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of my local preferenceses
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences> getMyLocalPreferenceses(int start, int end) {

		return _myLocalPreferencesLocalService.getMyLocalPreferenceses(
			start, end);
	}

	/**
	 * Returns all the my local preferenceses matching the UUID and company.
	 *
	 * @param uuid the UUID of the my local preferenceses
	 * @param companyId the primary key of the company
	 * @return the matching my local preferenceses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences> getMyLocalPreferencesesByUuidAndCompanyId(
				String uuid, long companyId) {

		return _myLocalPreferencesLocalService.
			getMyLocalPreferencesesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of my local preferenceses matching the UUID and company.
	 *
	 * @param uuid the UUID of the my local preferenceses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching my local preferenceses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences> getMyLocalPreferencesesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.local.account.mylocal.model.
						MyLocalPreferences> orderByComparator) {

		return _myLocalPreferencesLocalService.
			getMyLocalPreferencesesByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of my local preferenceses.
	 *
	 * @return the number of my local preferenceses
	 */
	@Override
	public int getMyLocalPreferencesesCount() {
		return _myLocalPreferencesLocalService.getMyLocalPreferencesesCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _myLocalPreferencesLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _myLocalPreferencesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the my local preferences in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was updated
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences updateMyLocalPreferences(
				com.placecube.digitalplace.local.account.mylocal.model.
					MyLocalPreferences myLocalPreferences) {

		return _myLocalPreferencesLocalService.updateMyLocalPreferences(
			myLocalPreferences);
	}

	@Override
	public
		com.placecube.digitalplace.local.account.mylocal.model.
			MyLocalPreferences updatePreferences(
				com.placecube.digitalplace.local.account.mylocal.model.
					MyLocalPreferences preferences) {

		return _myLocalPreferencesLocalService.updatePreferences(preferences);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _myLocalPreferencesLocalService.getBasePersistence();
	}

	@Override
	public MyLocalPreferencesLocalService getWrappedService() {
		return _myLocalPreferencesLocalService;
	}

	@Override
	public void setWrappedService(
		MyLocalPreferencesLocalService myLocalPreferencesLocalService) {

		_myLocalPreferencesLocalService = myLocalPreferencesLocalService;
	}

	private MyLocalPreferencesLocalService _myLocalPreferencesLocalService;

}