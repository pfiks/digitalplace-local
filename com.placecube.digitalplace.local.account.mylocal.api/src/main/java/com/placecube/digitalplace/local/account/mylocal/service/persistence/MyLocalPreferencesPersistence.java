/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.local.account.mylocal.exception.NoSuchMyLocalPreferencesException;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the my local preferences service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferencesUtil
 * @generated
 */
@ProviderType
public interface MyLocalPreferencesPersistence
	extends BasePersistence<MyLocalPreferences> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MyLocalPreferencesUtil} to access the my local preferences persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the my local preferenceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid(String uuid);

	/**
	 * Returns a range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public MyLocalPreferences findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
				orderByComparator)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public MyLocalPreferences findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
				orderByComparator)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns the my local preferenceses before and after the current my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param preferencesId the primary key of the current my local preferences
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public MyLocalPreferences[] findByUuid_PrevAndNext(
			long preferencesId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
				orderByComparator)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Removes all the my local preferenceses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of my local preferenceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching my local preferenceses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public MyLocalPreferences findByUUID_G(String uuid, long groupId)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the my local preferences where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the my local preferences that was removed
	 */
	public MyLocalPreferences removeByUUID_G(String uuid, long groupId)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the number of my local preferenceses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching my local preferenceses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public MyLocalPreferences findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
				orderByComparator)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public MyLocalPreferences findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
				orderByComparator)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns the my local preferenceses before and after the current my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param preferencesId the primary key of the current my local preferences
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public MyLocalPreferences[] findByUuid_C_PrevAndNext(
			long preferencesId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
				orderByComparator)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Removes all the my local preferenceses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching my local preferenceses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public MyLocalPreferences findByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId);

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public MyLocalPreferences fetchByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId,
		boolean useFinderCache);

	/**
	 * Removes the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the my local preferences that was removed
	 */
	public MyLocalPreferences removeByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the number of my local preferenceses where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the number of matching my local preferenceses
	 */
	public int countByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId);

	/**
	 * Caches the my local preferences in the entity cache if it is enabled.
	 *
	 * @param myLocalPreferences the my local preferences
	 */
	public void cacheResult(MyLocalPreferences myLocalPreferences);

	/**
	 * Caches the my local preferenceses in the entity cache if it is enabled.
	 *
	 * @param myLocalPreferenceses the my local preferenceses
	 */
	public void cacheResult(
		java.util.List<MyLocalPreferences> myLocalPreferenceses);

	/**
	 * Creates a new my local preferences with the primary key. Does not add the my local preferences to the database.
	 *
	 * @param preferencesId the primary key for the new my local preferences
	 * @return the new my local preferences
	 */
	public MyLocalPreferences create(long preferencesId);

	/**
	 * Removes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public MyLocalPreferences remove(long preferencesId)
		throws NoSuchMyLocalPreferencesException;

	public MyLocalPreferences updateImpl(MyLocalPreferences myLocalPreferences);

	/**
	 * Returns the my local preferences with the primary key or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public MyLocalPreferences findByPrimaryKey(long preferencesId)
		throws NoSuchMyLocalPreferencesException;

	/**
	 * Returns the my local preferences with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences, or <code>null</code> if a my local preferences with the primary key could not be found
	 */
	public MyLocalPreferences fetchByPrimaryKey(long preferencesId);

	/**
	 * Returns all the my local preferenceses.
	 *
	 * @return the my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findAll();

	/**
	 * Returns a range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator);

	/**
	 * Returns an ordered range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of my local preferenceses
	 */
	public java.util.List<MyLocalPreferences> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<MyLocalPreferences>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the my local preferenceses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of my local preferenceses.
	 *
	 * @return the number of my local preferenceses
	 */
	public int countAll();

}