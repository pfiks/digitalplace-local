/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Account_MyLocal_MyLocalPreferences&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferences
 * @generated
 */
public class MyLocalPreferencesTable
	extends BaseTable<MyLocalPreferencesTable> {

	public static final MyLocalPreferencesTable INSTANCE =
		new MyLocalPreferencesTable();

	public final Column<MyLocalPreferencesTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Long> preferencesId =
		createColumn(
			"preferencesId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<MyLocalPreferencesTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, String> userName =
		createColumn(
			"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Date> createDate =
		createColumn(
			"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Date> modifiedDate =
		createColumn(
			"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Long> plid = createColumn(
		"plid", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, String> portletId =
		createColumn(
			"portletId", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<MyLocalPreferencesTable, Clob> preferences =
		createColumn(
			"preferences", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);

	private MyLocalPreferencesTable() {
		super(
			"Placecube_Account_MyLocal_MyLocalPreferences",
			MyLocalPreferencesTable::new);
	}

}