/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the MyLocalPreferences service. Represents a row in the &quot;Placecube_Account_MyLocal_MyLocalPreferences&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferencesModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesImpl"
)
@ProviderType
public interface MyLocalPreferences
	extends MyLocalPreferencesModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<MyLocalPreferences, Long>
		PREFERENCES_ID_ACCESSOR = new Accessor<MyLocalPreferences, Long>() {

			@Override
			public Long get(MyLocalPreferences myLocalPreferences) {
				return myLocalPreferences.getPreferencesId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<MyLocalPreferences> getTypeClass() {
				return MyLocalPreferences.class;
			}

		};

}