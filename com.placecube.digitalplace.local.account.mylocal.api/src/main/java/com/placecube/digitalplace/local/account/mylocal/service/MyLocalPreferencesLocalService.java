/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;

import java.io.Serializable;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for MyLocalPreferences. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferencesLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface MyLocalPreferencesLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.local.account.mylocal.service.impl.MyLocalPreferencesLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the my local preferences local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link MyLocalPreferencesLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the my local preferences to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public MyLocalPreferences addMyLocalPreferences(
		MyLocalPreferences myLocalPreferences);

	@Indexable(type = IndexableType.REINDEX)
	public MyLocalPreferences addPreferences(
			long companyId, long groupId, long userId, long plid,
			String portletId, String preferencesBody)
		throws PortalException;

	/**
	 * Creates a new my local preferences with the primary key. Does not add the my local preferences to the database.
	 *
	 * @param preferencesId the primary key for the new my local preferences
	 * @return the new my local preferences
	 */
	@Transactional(enabled = false)
	public MyLocalPreferences createMyLocalPreferences(long preferencesId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws PortalException if a my local preferences with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public MyLocalPreferences deleteMyLocalPreferences(long preferencesId)
		throws PortalException;

	/**
	 * Deletes the my local preferences from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public MyLocalPreferences deleteMyLocalPreferences(
		MyLocalPreferences myLocalPreferences);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MyLocalPreferences fetchMyLocalPreferences(long preferencesId);

	/**
	 * Returns the my local preferences matching the UUID and group.
	 *
	 * @param uuid the my local preferences's UUID
	 * @param groupId the primary key of the group
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MyLocalPreferences fetchMyLocalPreferencesByUuidAndGroupId(
		String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MyLocalPreferences fetchPreferencesByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the my local preferences with the primary key.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws PortalException if a my local preferences with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MyLocalPreferences getMyLocalPreferences(long preferencesId)
		throws PortalException;

	/**
	 * Returns the my local preferences matching the UUID and group.
	 *
	 * @param uuid the my local preferences's UUID
	 * @param groupId the primary key of the group
	 * @return the matching my local preferences
	 * @throws PortalException if a matching my local preferences could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MyLocalPreferences getMyLocalPreferencesByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of my local preferenceses
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MyLocalPreferences> getMyLocalPreferenceses(int start, int end);

	/**
	 * Returns all the my local preferenceses matching the UUID and company.
	 *
	 * @param uuid the UUID of the my local preferenceses
	 * @param companyId the primary key of the company
	 * @return the matching my local preferenceses, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MyLocalPreferences> getMyLocalPreferencesesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of my local preferenceses matching the UUID and company.
	 *
	 * @param uuid the UUID of the my local preferenceses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching my local preferenceses, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MyLocalPreferences> getMyLocalPreferencesesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator);

	/**
	 * Returns the number of my local preferenceses.
	 *
	 * @return the number of my local preferenceses
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getMyLocalPreferencesesCount();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the my local preferences in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public MyLocalPreferences updateMyLocalPreferences(
		MyLocalPreferences myLocalPreferences);

	@Indexable(type = IndexableType.REINDEX)
	public MyLocalPreferences updatePreferences(MyLocalPreferences preferences);

}