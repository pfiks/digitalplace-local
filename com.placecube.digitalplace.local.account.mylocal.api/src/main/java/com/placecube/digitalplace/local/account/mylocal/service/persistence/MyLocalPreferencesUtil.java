/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the my local preferences service. This utility wraps <code>com.placecube.digitalplace.local.account.mylocal.service.persistence.impl.MyLocalPreferencesPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferencesPersistence
 * @generated
 */
public class MyLocalPreferencesUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(MyLocalPreferences myLocalPreferences) {
		getPersistence().clearCache(myLocalPreferences);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, MyLocalPreferences> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<MyLocalPreferences> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<MyLocalPreferences> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<MyLocalPreferences> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static MyLocalPreferences update(
		MyLocalPreferences myLocalPreferences) {

		return getPersistence().update(myLocalPreferences);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static MyLocalPreferences update(
		MyLocalPreferences myLocalPreferences, ServiceContext serviceContext) {

		return getPersistence().update(myLocalPreferences, serviceContext);
	}

	/**
	 * Returns all the my local preferenceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences findByUuid_First(
			String uuid,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByUuid_First(
		String uuid, OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences findByUuid_Last(
			String uuid,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByUuid_Last(
		String uuid, OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the my local preferenceses before and after the current my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param preferencesId the primary key of the current my local preferences
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences[] findByUuid_PrevAndNext(
			long preferencesId, String uuid,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUuid_PrevAndNext(
			preferencesId, uuid, orderByComparator);
	}

	/**
	 * Removes all the my local preferenceses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of my local preferenceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching my local preferenceses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the my local preferences where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the my local preferences that was removed
	 */
	public static MyLocalPreferences removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of my local preferenceses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching my local preferenceses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching my local preferenceses
	 */
	public static List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the my local preferenceses before and after the current my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param preferencesId the primary key of the current my local preferences
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences[] findByUuid_C_PrevAndNext(
			long preferencesId, String uuid, long companyId,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByUuid_C_PrevAndNext(
			preferencesId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the my local preferenceses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching my local preferenceses
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences findByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId);
	}

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId) {

		return getPersistence().fetchByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId);
	}

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId,
		boolean useFinderCache) {

		return getPersistence().fetchByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId, useFinderCache);
	}

	/**
	 * Removes the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the my local preferences that was removed
	 */
	public static MyLocalPreferences removeByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().removeByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId);
	}

	/**
	 * Returns the number of my local preferenceses where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the number of matching my local preferenceses
	 */
	public static int countByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId) {

		return getPersistence().countByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId);
	}

	/**
	 * Caches the my local preferences in the entity cache if it is enabled.
	 *
	 * @param myLocalPreferences the my local preferences
	 */
	public static void cacheResult(MyLocalPreferences myLocalPreferences) {
		getPersistence().cacheResult(myLocalPreferences);
	}

	/**
	 * Caches the my local preferenceses in the entity cache if it is enabled.
	 *
	 * @param myLocalPreferenceses the my local preferenceses
	 */
	public static void cacheResult(
		List<MyLocalPreferences> myLocalPreferenceses) {

		getPersistence().cacheResult(myLocalPreferenceses);
	}

	/**
	 * Creates a new my local preferences with the primary key. Does not add the my local preferences to the database.
	 *
	 * @param preferencesId the primary key for the new my local preferences
	 * @return the new my local preferences
	 */
	public static MyLocalPreferences create(long preferencesId) {
		return getPersistence().create(preferencesId);
	}

	/**
	 * Removes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences remove(long preferencesId)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().remove(preferencesId);
	}

	public static MyLocalPreferences updateImpl(
		MyLocalPreferences myLocalPreferences) {

		return getPersistence().updateImpl(myLocalPreferences);
	}

	/**
	 * Returns the my local preferences with the primary key or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences findByPrimaryKey(long preferencesId)
		throws com.placecube.digitalplace.local.account.mylocal.exception.
			NoSuchMyLocalPreferencesException {

		return getPersistence().findByPrimaryKey(preferencesId);
	}

	/**
	 * Returns the my local preferences with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences, or <code>null</code> if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences fetchByPrimaryKey(long preferencesId) {
		return getPersistence().fetchByPrimaryKey(preferencesId);
	}

	/**
	 * Returns all the my local preferenceses.
	 *
	 * @return the my local preferenceses
	 */
	public static List<MyLocalPreferences> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of my local preferenceses
	 */
	public static List<MyLocalPreferences> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of my local preferenceses
	 */
	public static List<MyLocalPreferences> findAll(
		int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of my local preferenceses
	 */
	public static List<MyLocalPreferences> findAll(
		int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the my local preferenceses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of my local preferenceses.
	 *
	 * @return the number of my local preferenceses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MyLocalPreferencesPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(
		MyLocalPreferencesPersistence persistence) {

		_persistence = persistence;
	}

	private static volatile MyLocalPreferencesPersistence _persistence;

}