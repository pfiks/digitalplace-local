/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for MyLocalPreferences. This utility wraps
 * <code>com.placecube.digitalplace.local.account.mylocal.service.impl.MyLocalPreferencesLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MyLocalPreferencesLocalService
 * @generated
 */
public class MyLocalPreferencesLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.local.account.mylocal.service.impl.MyLocalPreferencesLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the my local preferences to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was added
	 */
	public static MyLocalPreferences addMyLocalPreferences(
		MyLocalPreferences myLocalPreferences) {

		return getService().addMyLocalPreferences(myLocalPreferences);
	}

	public static MyLocalPreferences addPreferences(
			long companyId, long groupId, long userId, long plid,
			String portletId, String preferencesBody)
		throws PortalException {

		return getService().addPreferences(
			companyId, groupId, userId, plid, portletId, preferencesBody);
	}

	/**
	 * Creates a new my local preferences with the primary key. Does not add the my local preferences to the database.
	 *
	 * @param preferencesId the primary key for the new my local preferences
	 * @return the new my local preferences
	 */
	public static MyLocalPreferences createMyLocalPreferences(
		long preferencesId) {

		return getService().createMyLocalPreferences(preferencesId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws PortalException if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences deleteMyLocalPreferences(
			long preferencesId)
		throws PortalException {

		return getService().deleteMyLocalPreferences(preferencesId);
	}

	/**
	 * Deletes the my local preferences from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was removed
	 */
	public static MyLocalPreferences deleteMyLocalPreferences(
		MyLocalPreferences myLocalPreferences) {

		return getService().deleteMyLocalPreferences(myLocalPreferences);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static MyLocalPreferences fetchMyLocalPreferences(
		long preferencesId) {

		return getService().fetchMyLocalPreferences(preferencesId);
	}

	/**
	 * Returns the my local preferences matching the UUID and group.
	 *
	 * @param uuid the my local preferences's UUID
	 * @param groupId the primary key of the group
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences fetchMyLocalPreferencesByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchMyLocalPreferencesByUuidAndGroupId(
			uuid, groupId);
	}

	public static MyLocalPreferences
		fetchPreferencesByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId) {

		return getService().fetchPreferencesByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the my local preferences with the primary key.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws PortalException if a my local preferences with the primary key could not be found
	 */
	public static MyLocalPreferences getMyLocalPreferences(long preferencesId)
		throws PortalException {

		return getService().getMyLocalPreferences(preferencesId);
	}

	/**
	 * Returns the my local preferences matching the UUID and group.
	 *
	 * @param uuid the my local preferences's UUID
	 * @param groupId the primary key of the group
	 * @return the matching my local preferences
	 * @throws PortalException if a matching my local preferences could not be found
	 */
	public static MyLocalPreferences getMyLocalPreferencesByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getMyLocalPreferencesByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of my local preferenceses
	 */
	public static List<MyLocalPreferences> getMyLocalPreferenceses(
		int start, int end) {

		return getService().getMyLocalPreferenceses(start, end);
	}

	/**
	 * Returns all the my local preferenceses matching the UUID and company.
	 *
	 * @param uuid the UUID of the my local preferenceses
	 * @param companyId the primary key of the company
	 * @return the matching my local preferenceses, or an empty list if no matches were found
	 */
	public static List<MyLocalPreferences>
		getMyLocalPreferencesesByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getMyLocalPreferencesesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of my local preferenceses matching the UUID and company.
	 *
	 * @param uuid the UUID of the my local preferenceses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching my local preferenceses, or an empty list if no matches were found
	 */
	public static List<MyLocalPreferences>
		getMyLocalPreferencesesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			OrderByComparator<MyLocalPreferences> orderByComparator) {

		return getService().getMyLocalPreferencesesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of my local preferenceses.
	 *
	 * @return the number of my local preferenceses
	 */
	public static int getMyLocalPreferencesesCount() {
		return getService().getMyLocalPreferencesesCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the my local preferences in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MyLocalPreferencesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param myLocalPreferences the my local preferences
	 * @return the my local preferences that was updated
	 */
	public static MyLocalPreferences updateMyLocalPreferences(
		MyLocalPreferences myLocalPreferences) {

		return getService().updateMyLocalPreferences(myLocalPreferences);
	}

	public static MyLocalPreferences updatePreferences(
		MyLocalPreferences preferences) {

		return getService().updatePreferences(preferences);
	}

	public static MyLocalPreferencesLocalService getService() {
		return _service;
	}

	public static void setService(MyLocalPreferencesLocalService service) {
		_service = service;
	}

	private static volatile MyLocalPreferencesLocalService _service;

}