package com.placecube.digitalplace.local.account.assetvocabulary.mylocal.constants;

public final class MyLocalConstants {

	public static final String MY_LOCAL_VOCAB_NAME = "My Local";

	private MyLocalConstants() {

	}

}
