package com.placecube.digitalplace.local.account.assetvocabulary.mylocal.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.account.assetvocabulary.mylocal.constants.MyLocalConstants;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;

@Component(immediate = true, service = MyLocalVocabularyService.class)
public class MyLocalVocabularyService {

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Reference
	private ServicesAssetVocabularyService servicesAssetVocabularyService;

	public void createMyLocalVocabulary(ServiceContext serviceContext) throws Exception {
		Map<Long, Set<Long>> selectedClassNameIds = new HashMap<>();
		Map<Long, Set<Long>> requiredClassNameIds = new HashMap<>();

		selectedClassNameIds.put(portal.getClassNameId(JournalArticle.class), Collections.emptySet());

		JSONObject vocabularyDefinition = jsonFactory.createJSONObject(StringUtil.read(getClass().getClassLoader(), "dependencies/my_local.json"));

		servicesAssetVocabularyService.getOrCreateVocabulary(serviceContext, selectedClassNameIds, requiredClassNameIds, vocabularyDefinition);
	}

	public long getMyLocalVocabularyId(long groupId) throws PortalException {
		return assetVocabularyLocalService.getGroupVocabulary(groupId, MyLocalConstants.MY_LOCAL_VOCAB_NAME).getVocabularyId();
	}

	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(false);

		return serviceContext;
	}
}
