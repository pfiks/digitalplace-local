package com.placecube.digitalplace.local.account.assetvocabulary.mylocal.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.account.assetvocabulary.mylocal.service.MyLocalVocabularyService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class MyLocalVocabularyLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(MyLocalVocabularyLifecycleListener.class);

	@Reference
	private MyLocalVocabularyService myLocalVocabularyService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {

		long companyId = company.getCompanyId();
		LOG.debug("Creating My Local vocabulary for companyId: " + companyId);

		Group globalGroup = company.getGroup();
		ServiceContext serviceContext = myLocalVocabularyService.getServiceContext(globalGroup);

		LOG.debug("Creating My Local vocabulary for companyId: " + companyId);
		myLocalVocabularyService.createMyLocalVocabulary(serviceContext);

	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Waits until the portal has finished initializing. No code needed.
	}

}
