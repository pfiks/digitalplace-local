package com.placecube.digitalplace.local.account.assetvocabulary.mylocal.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.account.assetvocabulary.mylocal.constants.MyLocalConstants;
import com.placecube.digitalplace.local.assetvocabulary.services.service.ServicesAssetVocabularyService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class MyLocalVocabularyServiceTest extends PowerMockito {

	private static final long CLASSNAME_ID = 3;

	private static final long GROUP_ID = 1;

	private static final long VOCABULARY_ID = 2;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServicesAssetVocabularyService mockServicesAssetVocabularyService;

	@InjectMocks
	private MyLocalVocabularyService myLocalVocabularyService;

	@Mock
	private RenderRequest renderRequest;

	@Captor
	private ArgumentCaptor<Map<Long, Set<Long>>> requiredClassNameIdsCaptor;

	@Captor
	private ArgumentCaptor<Map<Long, Set<Long>>> selectedClassNameIdsCaptor;

	@Before
	public void activateSetUp() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = JSONException.class)
	public void createMyLocalVocabulary_WhenErrorCreatingJSONDefinition_ThenJSONExceptionIsThrown() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/my_local.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenThrow(new JSONException());

		myLocalVocabularyService.createMyLocalVocabulary(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void createMyLocalVocabulary_WhenErrorCreatingVocabulary_ThenPortalExceptionIsThrown() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/my_local.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenReturn(mockJSONObject);
		when(mockServicesAssetVocabularyService.getOrCreateVocabulary(same(mockServiceContext), any(HashMap.class), any(HashMap.class), same(mockJSONObject))).thenThrow(new PortalException());

		myLocalVocabularyService.createMyLocalVocabulary(mockServiceContext);
	}

	@Test(expected = IOException.class)
	public void createMyLocalVocabulary_WhenErrorReadingJSONDefinitionFile_ThenIOExceptionIsThrown() throws Exception {
		doThrow(new IOException()).when(StringUtil.class);
		StringUtil.read(any(ClassLoader.class), same("dependencies/my_local.json"));

		myLocalVocabularyService.createMyLocalVocabulary(mockServiceContext);
	}

	@Test
	public void createMyLocalVocabulary_WhenNoError_ThenJournalArticleClassIsAddedToSelectedClassNameIdMapsAndNotAddedToRequiredClassNameIdMaps() throws Exception {
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASSNAME_ID);
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/my_local.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenReturn(mockJSONObject);

		myLocalVocabularyService.createMyLocalVocabulary(mockServiceContext);

		verify(mockServicesAssetVocabularyService, times(1)).getOrCreateVocabulary(same(mockServiceContext), selectedClassNameIdsCaptor.capture(), requiredClassNameIdsCaptor.capture(),
				same(mockJSONObject));
		assertTrue(selectedClassNameIdsCaptor.getValue().containsKey(CLASSNAME_ID));
		assertFalse(requiredClassNameIdsCaptor.getValue().containsKey(CLASSNAME_ID));
	}

	@Test
	public void createMyLocalVocabulary_WhenNoError_ThenVocabularyIsCreated() throws Exception {
		when(StringUtil.read(any(ClassLoader.class), same("dependencies/my_local.json"))).thenReturn("JSON");
		when(mockJsonFactory.createJSONObject("JSON")).thenReturn(mockJSONObject);

		myLocalVocabularyService.createMyLocalVocabulary(mockServiceContext);

		verify(mockServicesAssetVocabularyService, times(1)).getOrCreateVocabulary(same(mockServiceContext), any(HashMap.class), any(HashMap.class), same(mockJSONObject));
	}

	@Test(expected = PortalException.class)
	public void getMyLocalVocabularyId_WhenErrorGettingVocabulary_ThenPortalExceptionIsThrown() throws PortalException {
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, MyLocalConstants.MY_LOCAL_VOCAB_NAME)).thenThrow(new PortalException());

		myLocalVocabularyService.getMyLocalVocabularyId(GROUP_ID);
	}

	@Test
	public void getMyLocalVocabularyId_WhenNoError_ThenMyLocalIdIsReturned() throws PortalException {
		when(mockAssetVocabulary.getVocabularyId()).thenReturn(VOCABULARY_ID);
		when(mockAssetVocabularyLocalService.getGroupVocabulary(GROUP_ID, MyLocalConstants.MY_LOCAL_VOCAB_NAME)).thenReturn(mockAssetVocabulary);

		long result = myLocalVocabularyService.getMyLocalVocabularyId(GROUP_ID);

		assertEquals(VOCABULARY_ID, result);
	}
}
