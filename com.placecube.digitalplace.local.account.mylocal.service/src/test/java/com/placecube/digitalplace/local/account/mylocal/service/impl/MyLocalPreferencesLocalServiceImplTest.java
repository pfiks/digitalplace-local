package com.placecube.digitalplace.local.account.mylocal.service.impl;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Date;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.service.persistence.MyLocalPreferencesPersistence;

@RunWith(PowerMockRunner.class)
public class MyLocalPreferencesLocalServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 1;
	private static final long GROUP_ID = 2;
	private static final long PLID = 4;
	private static final String PORTLET_ID = "portlet-id";
	private static final String PREFERENCES_BODY = "preferences-body";
	private static final long PREFERENCES_ID = 5;
	private static final long USER_ID = 3;
	private static final String USER_NAME = "user-name";

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private MyLocalPreferences mockMyLocalPreferences;

	@InjectMocks
	private MyLocalPreferencesLocalServiceImpl mockMyLocalPreferencesLocalServiceImpl;

	@Mock
	private MyLocalPreferencesPersistence mockMyLocalPreferencesPersistence;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Test
	public void addPreferences_WhenNoError_ThenPreferencesIsCreated() throws PortalException {

		when(mockCounterLocalService.increment(MyLocalPreferences.class.getName())).thenReturn(PREFERENCES_ID);
		when(mockMyLocalPreferencesPersistence.create(PREFERENCES_ID)).thenReturn(mockMyLocalPreferences);

		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USER_NAME);

		mockMyLocalPreferencesLocalServiceImpl.addPreferences(COMPANY_ID, GROUP_ID, USER_ID, PLID, PORTLET_ID, PREFERENCES_BODY);

		InOrder inOrder = inOrder(mockMyLocalPreferences, mockMyLocalPreferencesPersistence);
		inOrder.verify(mockMyLocalPreferences, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockMyLocalPreferences, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockMyLocalPreferences, times(1)).setUserId(USER_ID);
		inOrder.verify(mockMyLocalPreferences, times(1)).setUserName(USER_NAME);
		inOrder.verify(mockMyLocalPreferences, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockMyLocalPreferences, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockMyLocalPreferences, times(1)).setPlid(PLID);
		inOrder.verify(mockMyLocalPreferences, times(1)).setPortletId(PORTLET_ID);
		inOrder.verify(mockMyLocalPreferences, times(1)).setPreferences(PREFERENCES_BODY);
		inOrder.verify(mockMyLocalPreferencesPersistence, times(1)).update(mockMyLocalPreferences);

	}

	@Test
	public void fetchPreferencesByGroupIdUserIdPlidAndPortletId_WhenPreferencesDoesNotExist_ThenReturnNull() {

		when(mockMyLocalPreferencesPersistence.fetchByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID)).thenReturn(null);

		MyLocalPreferences result = mockMyLocalPreferencesLocalServiceImpl.fetchPreferencesByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID);

		assertNull(result);
	}

	@Test
	public void fetchPreferencesByGroupIdUserIdPlidAndPortletId_WhenPreferencesExists_ThenReturnPreferences() {
		when(mockMyLocalPreferencesPersistence.fetchByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID)).thenReturn(mockMyLocalPreferences);

		MyLocalPreferences result = mockMyLocalPreferencesLocalServiceImpl.fetchPreferencesByGroupIdUserIdPlidAndPortletId(GROUP_ID, USER_ID, PLID, PORTLET_ID);

		assertThat(result, Matchers.sameInstance(mockMyLocalPreferences));
	}

	@Test
	public void updatePreferences_WhenNoError_ThenPreferencesIsUpdated() {

		when(mockMyLocalPreferencesPersistence.update(mockMyLocalPreferences)).thenReturn(mockMyLocalPreferences);

		mockMyLocalPreferencesLocalServiceImpl.updatePreferences(mockMyLocalPreferences);

		InOrder inOrder = inOrder(mockMyLocalPreferences, mockMyLocalPreferencesPersistence);
		inOrder.verify(mockMyLocalPreferences, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockMyLocalPreferencesPersistence, times(1)).update(mockMyLocalPreferences);
	}

}
