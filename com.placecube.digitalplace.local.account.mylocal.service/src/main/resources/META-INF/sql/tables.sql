create table Placecube_Account_MyLocal_MyLocalPreferences (
	uuid_ VARCHAR(75) null,
	preferencesId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	plid LONG,
	portletId VARCHAR(200) null,
	preferences TEXT null
);