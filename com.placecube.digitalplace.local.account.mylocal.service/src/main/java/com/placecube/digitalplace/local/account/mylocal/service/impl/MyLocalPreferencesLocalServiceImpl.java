/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.local.account.mylocal.service.impl;

import java.util.Date;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.service.base.MyLocalPreferencesLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
	property = "model.class.name=com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences",
	service = AopService.class
)
public class MyLocalPreferencesLocalServiceImpl
	extends MyLocalPreferencesLocalServiceBaseImpl {

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public MyLocalPreferences addPreferences(long companyId, long groupId, long userId, long plid, String portletId, String preferencesBody) throws PortalException {

		MyLocalPreferences preferences = myLocalPreferencesPersistence.create(counterLocalService.increment(MyLocalPreferences.class.getName()));

		User user = userLocalService.getUser(userId);

		preferences.setCompanyId(companyId);
		preferences.setGroupId(groupId);
		preferences.setUserId(userId);
		preferences.setUserName(user.getFullName());

		Date now = new Date();
		preferences.setCreateDate(now);
		preferences.setModifiedDate(now);

		preferences.setPlid(plid);
		preferences.setPortletId(portletId);
		preferences.setPreferences(preferencesBody);

		myLocalPreferencesPersistence.update(preferences);

		return preferences;
	}

	@Override
	public MyLocalPreferences fetchPreferencesByGroupIdUserIdPlidAndPortletId(long groupId, long userId, long plid, String portletId) {
		return myLocalPreferencesPersistence.fetchByGroupIdUserIdPlidAndPortletId(groupId, userId, plid, portletId);
	}

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public MyLocalPreferences updatePreferences(MyLocalPreferences preferences) {
		preferences.setModifiedDate(new Date());
		return myLocalPreferencesPersistence.update(preferences);
	}
}