/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing MyLocalPreferences in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MyLocalPreferencesCacheModel
	implements CacheModel<MyLocalPreferences>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MyLocalPreferencesCacheModel)) {
			return false;
		}

		MyLocalPreferencesCacheModel myLocalPreferencesCacheModel =
			(MyLocalPreferencesCacheModel)object;

		if (preferencesId == myLocalPreferencesCacheModel.preferencesId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, preferencesId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", preferencesId=");
		sb.append(preferencesId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", plid=");
		sb.append(plid);
		sb.append(", portletId=");
		sb.append(portletId);
		sb.append(", preferences=");
		sb.append(preferences);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MyLocalPreferences toEntityModel() {
		MyLocalPreferencesImpl myLocalPreferencesImpl =
			new MyLocalPreferencesImpl();

		if (uuid == null) {
			myLocalPreferencesImpl.setUuid("");
		}
		else {
			myLocalPreferencesImpl.setUuid(uuid);
		}

		myLocalPreferencesImpl.setPreferencesId(preferencesId);
		myLocalPreferencesImpl.setCompanyId(companyId);
		myLocalPreferencesImpl.setGroupId(groupId);
		myLocalPreferencesImpl.setUserId(userId);

		if (userName == null) {
			myLocalPreferencesImpl.setUserName("");
		}
		else {
			myLocalPreferencesImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			myLocalPreferencesImpl.setCreateDate(null);
		}
		else {
			myLocalPreferencesImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			myLocalPreferencesImpl.setModifiedDate(null);
		}
		else {
			myLocalPreferencesImpl.setModifiedDate(new Date(modifiedDate));
		}

		myLocalPreferencesImpl.setPlid(plid);

		if (portletId == null) {
			myLocalPreferencesImpl.setPortletId("");
		}
		else {
			myLocalPreferencesImpl.setPortletId(portletId);
		}

		if (preferences == null) {
			myLocalPreferencesImpl.setPreferences("");
		}
		else {
			myLocalPreferencesImpl.setPreferences(preferences);
		}

		myLocalPreferencesImpl.resetOriginalValues();

		return myLocalPreferencesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		preferencesId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		plid = objectInput.readLong();
		portletId = objectInput.readUTF();
		preferences = (String)objectInput.readObject();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(preferencesId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(plid);

		if (portletId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(portletId);
		}

		if (preferences == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(preferences);
		}
	}

	public String uuid;
	public long preferencesId;
	public long companyId;
	public long groupId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long plid;
	public String portletId;
	public String preferences;

}