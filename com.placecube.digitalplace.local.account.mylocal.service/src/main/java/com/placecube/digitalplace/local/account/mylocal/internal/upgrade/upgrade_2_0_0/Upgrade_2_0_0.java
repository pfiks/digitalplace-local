package com.placecube.digitalplace.local.account.mylocal.internal.upgrade.upgrade_2_0_0;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.upgrade.DummyUpgradeProcess;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade_2_0_0 implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new DummyUpgradeProcess());
		registry.register("1.0.1", "2.0.0", new MyLocalPreferencesTableUpdate());
	}

}
