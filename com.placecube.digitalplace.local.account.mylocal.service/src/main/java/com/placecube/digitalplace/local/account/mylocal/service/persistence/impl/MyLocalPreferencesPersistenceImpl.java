/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.mylocal.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.local.account.mylocal.exception.NoSuchMyLocalPreferencesException;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences;
import com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferencesTable;
import com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesImpl;
import com.placecube.digitalplace.local.account.mylocal.model.impl.MyLocalPreferencesModelImpl;
import com.placecube.digitalplace.local.account.mylocal.service.persistence.MyLocalPreferencesPersistence;
import com.placecube.digitalplace.local.account.mylocal.service.persistence.MyLocalPreferencesUtil;
import com.placecube.digitalplace.local.account.mylocal.service.persistence.impl.constants.Placecube_Account_MyLocalPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the my local preferences service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = MyLocalPreferencesPersistence.class)
public class MyLocalPreferencesPersistenceImpl
	extends BasePersistenceImpl<MyLocalPreferences>
	implements MyLocalPreferencesPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>MyLocalPreferencesUtil</code> to access the my local preferences persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		MyLocalPreferencesImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the my local preferenceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<MyLocalPreferences> list = null;

		if (useFinderCache) {
			list = (List<MyLocalPreferences>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (MyLocalPreferences myLocalPreferences : list) {
					if (!uuid.equals(myLocalPreferences.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_MYLOCALPREFERENCES_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(MyLocalPreferencesModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<MyLocalPreferences>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences findByUuid_First(
			String uuid,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = fetchByUuid_First(
			uuid, orderByComparator);

		if (myLocalPreferences != null) {
			return myLocalPreferences;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchMyLocalPreferencesException(sb.toString());
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByUuid_First(
		String uuid, OrderByComparator<MyLocalPreferences> orderByComparator) {

		List<MyLocalPreferences> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences findByUuid_Last(
			String uuid,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = fetchByUuid_Last(
			uuid, orderByComparator);

		if (myLocalPreferences != null) {
			return myLocalPreferences;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchMyLocalPreferencesException(sb.toString());
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByUuid_Last(
		String uuid, OrderByComparator<MyLocalPreferences> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<MyLocalPreferences> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the my local preferenceses before and after the current my local preferences in the ordered set where uuid = &#63;.
	 *
	 * @param preferencesId the primary key of the current my local preferences
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences[] findByUuid_PrevAndNext(
			long preferencesId, String uuid,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws NoSuchMyLocalPreferencesException {

		uuid = Objects.toString(uuid, "");

		MyLocalPreferences myLocalPreferences = findByPrimaryKey(preferencesId);

		Session session = null;

		try {
			session = openSession();

			MyLocalPreferences[] array = new MyLocalPreferencesImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, myLocalPreferences, uuid, orderByComparator, true);

			array[1] = myLocalPreferences;

			array[2] = getByUuid_PrevAndNext(
				session, myLocalPreferences, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected MyLocalPreferences getByUuid_PrevAndNext(
		Session session, MyLocalPreferences myLocalPreferences, String uuid,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_MYLOCALPREFERENCES_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(MyLocalPreferencesModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						myLocalPreferences)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<MyLocalPreferences> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the my local preferenceses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (MyLocalPreferences myLocalPreferences :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(myLocalPreferences);
		}
	}

	/**
	 * Returns the number of my local preferenceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching my local preferenceses
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_MYLOCALPREFERENCES_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"myLocalPreferences.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(myLocalPreferences.uuid IS NULL OR myLocalPreferences.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences findByUUID_G(String uuid, long groupId)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = fetchByUUID_G(uuid, groupId);

		if (myLocalPreferences == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchMyLocalPreferencesException(sb.toString());
		}

		return myLocalPreferences;
	}

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the my local preferences where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof MyLocalPreferences) {
			MyLocalPreferences myLocalPreferences = (MyLocalPreferences)result;

			if (!Objects.equals(uuid, myLocalPreferences.getUuid()) ||
				(groupId != myLocalPreferences.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_MYLOCALPREFERENCES_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<MyLocalPreferences> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					MyLocalPreferences myLocalPreferences = list.get(0);

					result = myLocalPreferences;

					cacheResult(myLocalPreferences);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (MyLocalPreferences)result;
		}
	}

	/**
	 * Removes the my local preferences where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the my local preferences that was removed
	 */
	@Override
	public MyLocalPreferences removeByUUID_G(String uuid, long groupId)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = findByUUID_G(uuid, groupId);

		return remove(myLocalPreferences);
	}

	/**
	 * Returns the number of my local preferenceses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching my local preferenceses
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_MYLOCALPREFERENCES_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"myLocalPreferences.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(myLocalPreferences.uuid IS NULL OR myLocalPreferences.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"myLocalPreferences.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<MyLocalPreferences> list = null;

		if (useFinderCache) {
			list = (List<MyLocalPreferences>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (MyLocalPreferences myLocalPreferences : list) {
					if (!uuid.equals(myLocalPreferences.getUuid()) ||
						(companyId != myLocalPreferences.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_MYLOCALPREFERENCES_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(MyLocalPreferencesModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<MyLocalPreferences>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (myLocalPreferences != null) {
			return myLocalPreferences;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchMyLocalPreferencesException(sb.toString());
	}

	/**
	 * Returns the first my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		List<MyLocalPreferences> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (myLocalPreferences != null) {
			return myLocalPreferences;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchMyLocalPreferencesException(sb.toString());
	}

	/**
	 * Returns the last my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<MyLocalPreferences> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the my local preferenceses before and after the current my local preferences in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param preferencesId the primary key of the current my local preferences
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences[] findByUuid_C_PrevAndNext(
			long preferencesId, String uuid, long companyId,
			OrderByComparator<MyLocalPreferences> orderByComparator)
		throws NoSuchMyLocalPreferencesException {

		uuid = Objects.toString(uuid, "");

		MyLocalPreferences myLocalPreferences = findByPrimaryKey(preferencesId);

		Session session = null;

		try {
			session = openSession();

			MyLocalPreferences[] array = new MyLocalPreferencesImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, myLocalPreferences, uuid, companyId, orderByComparator,
				true);

			array[1] = myLocalPreferences;

			array[2] = getByUuid_C_PrevAndNext(
				session, myLocalPreferences, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected MyLocalPreferences getByUuid_C_PrevAndNext(
		Session session, MyLocalPreferences myLocalPreferences, String uuid,
		long companyId, OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_MYLOCALPREFERENCES_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(MyLocalPreferencesModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						myLocalPreferences)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<MyLocalPreferences> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the my local preferenceses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (MyLocalPreferences myLocalPreferences :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(myLocalPreferences);
		}
	}

	/**
	 * Returns the number of my local preferenceses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching my local preferenceses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_MYLOCALPREFERENCES_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"myLocalPreferences.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(myLocalPreferences.uuid IS NULL OR myLocalPreferences.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"myLocalPreferences.companyId = ?";

	private FinderPath _finderPathFetchByGroupIdUserIdPlidAndPortletId;
	private FinderPath _finderPathCountByGroupIdUserIdPlidAndPortletId;

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the matching my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences findByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences =
			fetchByGroupIdUserIdPlidAndPortletId(
				groupId, userId, plid, portletId);

		if (myLocalPreferences == null) {
			StringBundler sb = new StringBundler(10);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("groupId=");
			sb.append(groupId);

			sb.append(", userId=");
			sb.append(userId);

			sb.append(", plid=");
			sb.append(plid);

			sb.append(", portletId=");
			sb.append(portletId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchMyLocalPreferencesException(sb.toString());
		}

		return myLocalPreferences;
	}

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId) {

		return fetchByGroupIdUserIdPlidAndPortletId(
			groupId, userId, plid, portletId, true);
	}

	/**
	 * Returns the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching my local preferences, or <code>null</code> if a matching my local preferences could not be found
	 */
	@Override
	public MyLocalPreferences fetchByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId,
		boolean useFinderCache) {

		portletId = Objects.toString(portletId, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {groupId, userId, plid, portletId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByGroupIdUserIdPlidAndPortletId, finderArgs,
				this);
		}

		if (result instanceof MyLocalPreferences) {
			MyLocalPreferences myLocalPreferences = (MyLocalPreferences)result;

			if ((groupId != myLocalPreferences.getGroupId()) ||
				(userId != myLocalPreferences.getUserId()) ||
				(plid != myLocalPreferences.getPlid()) ||
				!Objects.equals(portletId, myLocalPreferences.getPortletId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_SQL_SELECT_MYLOCALPREFERENCES_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_GROUPID_2);

			sb.append(_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_USERID_2);

			sb.append(_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PLID_2);

			boolean bindPortletId = false;

			if (portletId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PORTLETID_3);
			}
			else {
				bindPortletId = true;

				sb.append(
					_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PORTLETID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(userId);

				queryPos.add(plid);

				if (bindPortletId) {
					queryPos.add(portletId);
				}

				List<MyLocalPreferences> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByGroupIdUserIdPlidAndPortletId,
							finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									groupId, userId, plid, portletId
								};
							}

							_log.warn(
								"MyLocalPreferencesPersistenceImpl.fetchByGroupIdUserIdPlidAndPortletId(long, long, long, String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					MyLocalPreferences myLocalPreferences = list.get(0);

					result = myLocalPreferences;

					cacheResult(myLocalPreferences);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (MyLocalPreferences)result;
		}
	}

	/**
	 * Removes the my local preferences where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the my local preferences that was removed
	 */
	@Override
	public MyLocalPreferences removeByGroupIdUserIdPlidAndPortletId(
			long groupId, long userId, long plid, String portletId)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences =
			findByGroupIdUserIdPlidAndPortletId(
				groupId, userId, plid, portletId);

		return remove(myLocalPreferences);
	}

	/**
	 * Returns the number of my local preferenceses where groupId = &#63; and userId = &#63; and plid = &#63; and portletId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param plid the plid
	 * @param portletId the portlet ID
	 * @return the number of matching my local preferenceses
	 */
	@Override
	public int countByGroupIdUserIdPlidAndPortletId(
		long groupId, long userId, long plid, String portletId) {

		portletId = Objects.toString(portletId, "");

		FinderPath finderPath = _finderPathCountByGroupIdUserIdPlidAndPortletId;

		Object[] finderArgs = new Object[] {groupId, userId, plid, portletId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(5);

			sb.append(_SQL_COUNT_MYLOCALPREFERENCES_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_GROUPID_2);

			sb.append(_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_USERID_2);

			sb.append(_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PLID_2);

			boolean bindPortletId = false;

			if (portletId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PORTLETID_3);
			}
			else {
				bindPortletId = true;

				sb.append(
					_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PORTLETID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(userId);

				queryPos.add(plid);

				if (bindPortletId) {
					queryPos.add(portletId);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_GROUPID_2 =
			"myLocalPreferences.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_USERID_2 =
			"myLocalPreferences.userId = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PLID_2 =
			"myLocalPreferences.plid = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PORTLETID_2 =
			"myLocalPreferences.portletId = ?";

	private static final String
		_FINDER_COLUMN_GROUPIDUSERIDPLIDANDPORTLETID_PORTLETID_3 =
			"(myLocalPreferences.portletId IS NULL OR myLocalPreferences.portletId = '')";

	public MyLocalPreferencesPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(MyLocalPreferences.class);

		setModelImplClass(MyLocalPreferencesImpl.class);
		setModelPKClass(long.class);

		setTable(MyLocalPreferencesTable.INSTANCE);
	}

	/**
	 * Caches the my local preferences in the entity cache if it is enabled.
	 *
	 * @param myLocalPreferences the my local preferences
	 */
	@Override
	public void cacheResult(MyLocalPreferences myLocalPreferences) {
		entityCache.putResult(
			MyLocalPreferencesImpl.class, myLocalPreferences.getPrimaryKey(),
			myLocalPreferences);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {
				myLocalPreferences.getUuid(), myLocalPreferences.getGroupId()
			},
			myLocalPreferences);

		finderCache.putResult(
			_finderPathFetchByGroupIdUserIdPlidAndPortletId,
			new Object[] {
				myLocalPreferences.getGroupId(), myLocalPreferences.getUserId(),
				myLocalPreferences.getPlid(), myLocalPreferences.getPortletId()
			},
			myLocalPreferences);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the my local preferenceses in the entity cache if it is enabled.
	 *
	 * @param myLocalPreferenceses the my local preferenceses
	 */
	@Override
	public void cacheResult(List<MyLocalPreferences> myLocalPreferenceses) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (myLocalPreferenceses.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (MyLocalPreferences myLocalPreferences : myLocalPreferenceses) {
			if (entityCache.getResult(
					MyLocalPreferencesImpl.class,
					myLocalPreferences.getPrimaryKey()) == null) {

				cacheResult(myLocalPreferences);
			}
		}
	}

	/**
	 * Clears the cache for all my local preferenceses.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(MyLocalPreferencesImpl.class);

		finderCache.clearCache(MyLocalPreferencesImpl.class);
	}

	/**
	 * Clears the cache for the my local preferences.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MyLocalPreferences myLocalPreferences) {
		entityCache.removeResult(
			MyLocalPreferencesImpl.class, myLocalPreferences);
	}

	@Override
	public void clearCache(List<MyLocalPreferences> myLocalPreferenceses) {
		for (MyLocalPreferences myLocalPreferences : myLocalPreferenceses) {
			entityCache.removeResult(
				MyLocalPreferencesImpl.class, myLocalPreferences);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(MyLocalPreferencesImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(MyLocalPreferencesImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		MyLocalPreferencesModelImpl myLocalPreferencesModelImpl) {

		Object[] args = new Object[] {
			myLocalPreferencesModelImpl.getUuid(),
			myLocalPreferencesModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, myLocalPreferencesModelImpl);

		args = new Object[] {
			myLocalPreferencesModelImpl.getGroupId(),
			myLocalPreferencesModelImpl.getUserId(),
			myLocalPreferencesModelImpl.getPlid(),
			myLocalPreferencesModelImpl.getPortletId()
		};

		finderCache.putResult(
			_finderPathCountByGroupIdUserIdPlidAndPortletId, args,
			Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByGroupIdUserIdPlidAndPortletId, args,
			myLocalPreferencesModelImpl);
	}

	/**
	 * Creates a new my local preferences with the primary key. Does not add the my local preferences to the database.
	 *
	 * @param preferencesId the primary key for the new my local preferences
	 * @return the new my local preferences
	 */
	@Override
	public MyLocalPreferences create(long preferencesId) {
		MyLocalPreferences myLocalPreferences = new MyLocalPreferencesImpl();

		myLocalPreferences.setNew(true);
		myLocalPreferences.setPrimaryKey(preferencesId);

		String uuid = PortalUUIDUtil.generate();

		myLocalPreferences.setUuid(uuid);

		myLocalPreferences.setCompanyId(CompanyThreadLocal.getCompanyId());

		return myLocalPreferences;
	}

	/**
	 * Removes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences remove(long preferencesId)
		throws NoSuchMyLocalPreferencesException {

		return remove((Serializable)preferencesId);
	}

	/**
	 * Removes the my local preferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the my local preferences
	 * @return the my local preferences that was removed
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences remove(Serializable primaryKey)
		throws NoSuchMyLocalPreferencesException {

		Session session = null;

		try {
			session = openSession();

			MyLocalPreferences myLocalPreferences =
				(MyLocalPreferences)session.get(
					MyLocalPreferencesImpl.class, primaryKey);

			if (myLocalPreferences == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMyLocalPreferencesException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(myLocalPreferences);
		}
		catch (NoSuchMyLocalPreferencesException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MyLocalPreferences removeImpl(
		MyLocalPreferences myLocalPreferences) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(myLocalPreferences)) {
				myLocalPreferences = (MyLocalPreferences)session.get(
					MyLocalPreferencesImpl.class,
					myLocalPreferences.getPrimaryKeyObj());
			}

			if (myLocalPreferences != null) {
				session.delete(myLocalPreferences);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (myLocalPreferences != null) {
			clearCache(myLocalPreferences);
		}

		return myLocalPreferences;
	}

	@Override
	public MyLocalPreferences updateImpl(
		MyLocalPreferences myLocalPreferences) {

		boolean isNew = myLocalPreferences.isNew();

		if (!(myLocalPreferences instanceof MyLocalPreferencesModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(myLocalPreferences.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					myLocalPreferences);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in myLocalPreferences proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom MyLocalPreferences implementation " +
					myLocalPreferences.getClass());
		}

		MyLocalPreferencesModelImpl myLocalPreferencesModelImpl =
			(MyLocalPreferencesModelImpl)myLocalPreferences;

		if (Validator.isNull(myLocalPreferences.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			myLocalPreferences.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (myLocalPreferences.getCreateDate() == null)) {
			if (serviceContext == null) {
				myLocalPreferences.setCreateDate(date);
			}
			else {
				myLocalPreferences.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!myLocalPreferencesModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				myLocalPreferences.setModifiedDate(date);
			}
			else {
				myLocalPreferences.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(myLocalPreferences);
			}
			else {
				myLocalPreferences = (MyLocalPreferences)session.merge(
					myLocalPreferences);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			MyLocalPreferencesImpl.class, myLocalPreferencesModelImpl, false,
			true);

		cacheUniqueFindersCache(myLocalPreferencesModelImpl);

		if (isNew) {
			myLocalPreferences.setNew(false);
		}

		myLocalPreferences.resetOriginalValues();

		return myLocalPreferences;
	}

	/**
	 * Returns the my local preferences with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMyLocalPreferencesException {

		MyLocalPreferences myLocalPreferences = fetchByPrimaryKey(primaryKey);

		if (myLocalPreferences == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMyLocalPreferencesException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return myLocalPreferences;
	}

	/**
	 * Returns the my local preferences with the primary key or throws a <code>NoSuchMyLocalPreferencesException</code> if it could not be found.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences
	 * @throws NoSuchMyLocalPreferencesException if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences findByPrimaryKey(long preferencesId)
		throws NoSuchMyLocalPreferencesException {

		return findByPrimaryKey((Serializable)preferencesId);
	}

	/**
	 * Returns the my local preferences with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param preferencesId the primary key of the my local preferences
	 * @return the my local preferences, or <code>null</code> if a my local preferences with the primary key could not be found
	 */
	@Override
	public MyLocalPreferences fetchByPrimaryKey(long preferencesId) {
		return fetchByPrimaryKey((Serializable)preferencesId);
	}

	/**
	 * Returns all the my local preferenceses.
	 *
	 * @return the my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @return the range of my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findAll(
		int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the my local preferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MyLocalPreferencesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of my local preferenceses
	 * @param end the upper bound of the range of my local preferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of my local preferenceses
	 */
	@Override
	public List<MyLocalPreferences> findAll(
		int start, int end,
		OrderByComparator<MyLocalPreferences> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<MyLocalPreferences> list = null;

		if (useFinderCache) {
			list = (List<MyLocalPreferences>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_MYLOCALPREFERENCES);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_MYLOCALPREFERENCES;

				sql = sql.concat(MyLocalPreferencesModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<MyLocalPreferences>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the my local preferenceses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (MyLocalPreferences myLocalPreferences : findAll()) {
			remove(myLocalPreferences);
		}
	}

	/**
	 * Returns the number of my local preferenceses.
	 *
	 * @return the number of my local preferenceses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(
					_SQL_COUNT_MYLOCALPREFERENCES);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "preferencesId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_MYLOCALPREFERENCES;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return MyLocalPreferencesModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the my local preferences persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathFetchByGroupIdUserIdPlidAndPortletId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByGroupIdUserIdPlidAndPortletId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Long.class.getName(), String.class.getName()
			},
			new String[] {"groupId", "userId", "plid", "portletId"}, true);

		_finderPathCountByGroupIdUserIdPlidAndPortletId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupIdUserIdPlidAndPortletId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Long.class.getName(), String.class.getName()
			},
			new String[] {"groupId", "userId", "plid", "portletId"}, false);

		MyLocalPreferencesUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		MyLocalPreferencesUtil.setPersistence(null);

		entityCache.removeCache(MyLocalPreferencesImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_Account_MyLocalPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_Account_MyLocalPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_Account_MyLocalPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_MYLOCALPREFERENCES =
		"SELECT myLocalPreferences FROM MyLocalPreferences myLocalPreferences";

	private static final String _SQL_SELECT_MYLOCALPREFERENCES_WHERE =
		"SELECT myLocalPreferences FROM MyLocalPreferences myLocalPreferences WHERE ";

	private static final String _SQL_COUNT_MYLOCALPREFERENCES =
		"SELECT COUNT(myLocalPreferences) FROM MyLocalPreferences myLocalPreferences";

	private static final String _SQL_COUNT_MYLOCALPREFERENCES_WHERE =
		"SELECT COUNT(myLocalPreferences) FROM MyLocalPreferences myLocalPreferences WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "myLocalPreferences.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No MyLocalPreferences exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No MyLocalPreferences exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		MyLocalPreferencesPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}