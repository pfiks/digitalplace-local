package com.placecube.digitalplace.local.account.mylocal.internal.upgrade.upgrade_2_0_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class MyLocalPreferencesTableUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(MyLocalPreferencesTableUpdate.class);

	@Override
	protected void doUpgrade() throws Exception {
		LOG.info("Initialised upgrade process for Placecube_Account_MyLocal_MyLocalPreferences table");

		if (!hasTable("Placecube_Account_MyLocal_Preferences")) {

			runSQLTemplateString(
					"create table Placecube_Account_MyLocal_MyLocalPreferences (uuid_ VARCHAR(75) null, preferencesId LONG not null primary key, companyId LONG, groupId LONG, userId LONG, userName VARCHAR(75) null, createDate DATE null, modifiedDate DATE null, plid LONG, portletId VARCHAR(200) null, preferences TEXT null);",
					false);

			LOG.info("Created table Placecube_Account_MyLocal_MyLocalPreferences");

			runSQLTemplateString("create index IX_F494678A on Placecube_Account_MyLocal_MyLocalPreferences (groupId, userId, plid, portletId[$COLUMN_LENGTH:200$]);", false);
			runSQLTemplateString("create index IX_C7BE3BE8 on Placecube_Account_MyLocal_MyLocalPreferences (uuid_[$COLUMN_LENGTH:75$], companyId);", false);
			runSQLTemplateString("create unique index IX_2BF49C6A on Placecube_Account_MyLocal_MyLocalPreferences (uuid_[$COLUMN_LENGTH:75$], groupId);", false);

			LOG.info("Created indexes for Placecube_Account_MyLocal_MyLocalPreferences");

		}

		if (hasTable("Placecube_Account_MyLocal_Preferences")) {
			runSQLTemplateString("INSERT INTO Placecube_Account_MyLocal_MyLocalPreferences SELECT * FROM Placecube_Account_MyLocal_Preferences;", false);

			LOG.info("Copied data from Placecube_Account_MyLocal_Preferences to Placecube_Account_MyLocal_MyLocalPreferences");

			runSQLTemplateString(
					"UPDATE Counter SET name = 'com.placecube.digitalplace.local.account.mylocal.model.MyLocalPreferences' WHERE name = 'com.placecube.digitalplace.local.account.mylocal.model.Preferences';",
					false);
			LOG.info("Updated Name of Placecube_Account_MyLocal_Preferences in Counter table");

			dropIndexImmediately("Placecube_Account_MyLocal_Preferences", "IX_C797089F");
			dropIndexImmediately("Placecube_Account_MyLocal_Preferences", "IX_6A1EA5D7");
			dropIndexImmediately("Placecube_Account_MyLocal_Preferences", "IX_AF07A6FD");
			dropIndexImmediately("Placecube_Account_MyLocal_Preferences", "IX_5F57C8BF");

			runSQLTemplateString("DROP TABLE Placecube_Account_MyLocal_Preferences;", false);

			LOG.info("Deleted table Placecube_Account_MyLocal_Preferences");
		}

		LOG.info("Completed upgrade process for Placecube_Account_MyLocal_MyLocalPreferences table");
	}

	private void dropIndexImmediately(String tableName, String dropIndex) throws Exception {
		if (hasIndex(tableName, dropIndex)) {
			String dropSql = String.format("drop index %s on %s;", dropIndex, tableName);
			LOG.info("Dropping existing index:" + dropIndex + String.format(" - %s", dropSql));
			runSQL(connection, dropSql);
		}
	}
}
