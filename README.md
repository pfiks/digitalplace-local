# digitalplace-local

## Supported versions
* Liferay 2024.q2.6
* Java JDK 8

## Required configuration
### Use custom services in ADTs
* Go to: Control Panel > Configuration > System Settings > Template Engines
* Remove 'serviceLocator' from the list of Restricted Variables and save.
* Remove 'staticUtil' from the list of Restricted Variables and save.  

## Modules
### com.placecube.digitalplace.local.assetvocabulary.services
Adds the Vocabulary 'Services'

### com.placecube.digitalplace.local.category.navigation
Adds the Service Menu content set and ADT

### com.placecube.digitalplace.local.language.shared
Adds custom labels to be used in theme

### com.placecube.digitalplace.local.navigationmenu
Adds Widget Templates for the Navigation Menu

### com.placecube.digitalplace.local.webcontent.news
Adds the Carousel Asset Publisher ADT and structure, template, content set for News

### com.placecube.digitalplace.local.pagetemplates
Adds page templates: Form Page, Category Page

### com.placecube.digitalplace.local.siteinitializer
Is the site initializer for Digital Place
Automatically configures the guest group.

### com.placecube.digitalplace.local.testdatasiteinitializer
Site initializer to generate sample content:
* services categories
* web contents of type 'Article'
* link generated web content articles to content sets

### com.placecube.digitalplace.local.themecontributor
Theme contributor to share common css overrides required by the templates

### com.placecube.digitalplace.local.webcontent.article
Adds Article web content structure, Widget templates for the Asset Publisher and Related Assets portlets, Content sets.

## License
Copyright (C) 2019-present Placecube Limited.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
