/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for FormEntry. This utility wraps
 * <code>com.placecube.digitalplace.local.ddmform.form.renderer.service.impl.FormEntryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryLocalService
 * @generated
 */
public class FormEntryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.local.ddmform.form.renderer.service.impl.FormEntryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the form entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntry the form entry
	 * @return the form entry that was added
	 */
	public static FormEntry addFormEntry(FormEntry formEntry) {
		return getService().addFormEntry(formEntry);
	}

	public static FormEntry addFormEntry(
			long userId, long plid, long formInstanceRecordVersionId,
			int status,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addFormEntry(
			userId, plid, formInstanceRecordVersionId, status, serviceContext);
	}

	public static FormEntry addFormEntry(
			long userId, long plid, long formInstanceRecordVersionId,
			String uniqueReference, int status,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addFormEntry(
			userId, plid, formInstanceRecordVersionId, uniqueReference, status,
			serviceContext);
	}

	/**
	 * Creates a new form entry with the primary key. Does not add the form entry to the database.
	 *
	 * @param formEntryId the primary key for the new form entry
	 * @return the new form entry
	 */
	public static FormEntry createFormEntry(long formEntryId) {
		return getService().createFormEntry(formEntryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the form entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntry the form entry
	 * @return the form entry that was removed
	 */
	public static FormEntry deleteFormEntry(FormEntry formEntry) {
		return getService().deleteFormEntry(formEntry);
	}

	/**
	 * Deletes the form entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry that was removed
	 * @throws PortalException if a form entry with the primary key could not be found
	 */
	public static FormEntry deleteFormEntry(long formEntryId)
		throws PortalException {

		return getService().deleteFormEntry(formEntryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static FormEntry fetchFormEntry(long formEntryId) {
		return getService().fetchFormEntry(formEntryId);
	}

	/**
	 * Returns the form entry matching the UUID and group.
	 *
	 * @param uuid the form entry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchFormEntryByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchFormEntryByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns a range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of form entries
	 */
	public static List<FormEntry> getFormEntries(int start, int end) {
		return getService().getFormEntries(start, end);
	}

	public static List<FormEntry> getFormEntriesByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		return getService().getFormEntriesByUniqueReferenceAndStatus(
			uniqueReference, status);
	}

	/**
	 * Returns all the form entries matching the UUID and company.
	 *
	 * @param uuid the UUID of the form entries
	 * @param companyId the primary key of the company
	 * @return the matching form entries, or an empty list if no matches were found
	 */
	public static List<FormEntry> getFormEntriesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getFormEntriesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of form entries matching the UUID and company.
	 *
	 * @param uuid the UUID of the form entries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching form entries, or an empty list if no matches were found
	 */
	public static List<FormEntry> getFormEntriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return getService().getFormEntriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of form entries.
	 *
	 * @return the number of form entries
	 */
	public static int getFormEntriesCount() {
		return getService().getFormEntriesCount();
	}

	public static List<FormEntry> getFormEntriesModifiedBeforeDate(
		long companyId, long plid, java.util.Date dateBefore) {

		return getService().getFormEntriesModifiedBeforeDate(
			companyId, plid, dateBefore);
	}

	/**
	 * Returns the form entry with the primary key.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry
	 * @throws PortalException if a form entry with the primary key could not be found
	 */
	public static FormEntry getFormEntry(long formEntryId)
		throws PortalException {

		return getService().getFormEntry(formEntryId);
	}

	public static FormEntry getFormEntryByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getService().getFormEntryByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
	}

	public static FormEntry getFormEntryByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getService().getFormEntryByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);
	}

	/**
	 * Returns the form entry matching the UUID and group.
	 *
	 * @param uuid the form entry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching form entry
	 * @throws PortalException if a matching form entry could not be found
	 */
	public static FormEntry getFormEntryByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getFormEntryByUuidAndGroupId(uuid, groupId);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the form entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntry the form entry
	 * @return the form entry that was updated
	 */
	public static FormEntry updateFormEntry(FormEntry formEntry) {
		return getService().updateFormEntry(formEntry);
	}

	public static FormEntry updateFormEntry(FormEntry formEntry, int status) {
		return getService().updateFormEntry(formEntry, status);
	}

	public static FormEntryLocalService getService() {
		return _service;
	}

	public static void setService(FormEntryLocalService service) {
		_service = service;
	}

	private static volatile FormEntryLocalService _service;

}