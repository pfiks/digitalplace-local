/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.local.ddmform.form.renderer.exception.NoSuchFormEntryException;
import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the form entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryUtil
 * @generated
 */
@ProviderType
public interface FormEntryPersistence extends BasePersistence<FormEntry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FormEntryUtil} to access the form entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the form entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching form entries
	 */
	public java.util.List<FormEntry> findByUuid(String uuid);

	/**
	 * Returns a range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	public java.util.List<FormEntry> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	public java.util.List<FormEntry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	public java.util.List<FormEntry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uuid = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public FormEntry[] findByUuid_PrevAndNext(
			long formEntryId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Removes all the form entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of form entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching form entries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUUID_G(String uuid, long groupId)
		throws NoSuchFormEntryException;

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the form entry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the form entry that was removed
	 */
	public FormEntry removeByUUID_G(String uuid, long groupId)
		throws NoSuchFormEntryException;

	/**
	 * Returns the number of form entries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching form entries
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching form entries
	 */
	public java.util.List<FormEntry> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	public java.util.List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	public java.util.List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	public java.util.List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public FormEntry[] findByUuid_C_PrevAndNext(
			long formEntryId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Removes all the form entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching form entries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws NoSuchFormEntryException;

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId);

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId, boolean useFinderCache);

	/**
	 * Removes the form entry where formInstanceRecordVersionId = &#63; from the database.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the form entry that was removed
	 */
	public FormEntry removeByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws NoSuchFormEntryException;

	/**
	 * Returns the number of form entries where formInstanceRecordVersionId = &#63;.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the number of matching form entries
	 */
	public int countByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId);

	/**
	 * Returns all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @return the matching form entries
	 */
	public java.util.List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status);

	/**
	 * Returns a range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	public java.util.List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end);

	/**
	 * Returns an ordered range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	public java.util.List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	public java.util.List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUniqueReferenceAndStatus_First(
			String uniqueReference, int status,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Returns the first form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUniqueReferenceAndStatus_First(
		String uniqueReference, int status,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns the last form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByUniqueReferenceAndStatus_Last(
			String uniqueReference, int status,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Returns the last form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByUniqueReferenceAndStatus_Last(
		String uniqueReference, int status,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public FormEntry[] findByUniqueReferenceAndStatus_PrevAndNext(
			long formEntryId, String uniqueReference, int status,
			com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
				orderByComparator)
		throws NoSuchFormEntryException;

	/**
	 * Removes all the form entries where uniqueReference = &#63; and status = &#63; from the database.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 */
	public void removeByUniqueReferenceAndStatus(
		String uniqueReference, int status);

	/**
	 * Returns the number of form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @return the number of matching form entries
	 */
	public int countByUniqueReferenceAndStatus(
		String uniqueReference, int status);

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public FormEntry findByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws NoSuchFormEntryException;

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status);

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public FormEntry fetchByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status, boolean useFinderCache);

	/**
	 * Removes the form entry where formInstanceRecordVersionId = &#63; and status = &#63; from the database.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the form entry that was removed
	 */
	public FormEntry removeByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws NoSuchFormEntryException;

	/**
	 * Returns the number of form entries where formInstanceRecordVersionId = &#63; and status = &#63;.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the number of matching form entries
	 */
	public int countByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status);

	/**
	 * Caches the form entry in the entity cache if it is enabled.
	 *
	 * @param formEntry the form entry
	 */
	public void cacheResult(FormEntry formEntry);

	/**
	 * Caches the form entries in the entity cache if it is enabled.
	 *
	 * @param formEntries the form entries
	 */
	public void cacheResult(java.util.List<FormEntry> formEntries);

	/**
	 * Creates a new form entry with the primary key. Does not add the form entry to the database.
	 *
	 * @param formEntryId the primary key for the new form entry
	 * @return the new form entry
	 */
	public FormEntry create(long formEntryId);

	/**
	 * Removes the form entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry that was removed
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public FormEntry remove(long formEntryId) throws NoSuchFormEntryException;

	public FormEntry updateImpl(FormEntry formEntry);

	/**
	 * Returns the form entry with the primary key or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public FormEntry findByPrimaryKey(long formEntryId)
		throws NoSuchFormEntryException;

	/**
	 * Returns the form entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry, or <code>null</code> if a form entry with the primary key could not be found
	 */
	public FormEntry fetchByPrimaryKey(long formEntryId);

	/**
	 * Returns all the form entries.
	 *
	 * @return the form entries
	 */
	public java.util.List<FormEntry> findAll();

	/**
	 * Returns a range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of form entries
	 */
	public java.util.List<FormEntry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of form entries
	 */
	public java.util.List<FormEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of form entries
	 */
	public java.util.List<FormEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<FormEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the form entries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of form entries.
	 *
	 * @return the number of form entries
	 */
	public int countAll();

}