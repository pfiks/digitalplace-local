/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_FormRenderer_FormEntry&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see FormEntry
 * @generated
 */
public class FormEntryTable extends BaseTable<FormEntryTable> {

	public static final FormEntryTable INSTANCE = new FormEntryTable();

	public final Column<FormEntryTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Long> formEntryId = createColumn(
		"formEntryId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<FormEntryTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, String> uniqueReference = createColumn(
		"uniqueReference", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Long> formInstanceRecordVersionId =
		createColumn(
			"formInstanceRecordVersionId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Long> plid = createColumn(
		"plid", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<FormEntryTable, Integer> status = createColumn(
		"status", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);

	private FormEntryTable() {
		super("Placecube_FormRenderer_FormEntry", FormEntryTable::new);
	}

}