/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FormEntry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormEntry
 * @generated
 */
public class FormEntryWrapper
	extends BaseModelWrapper<FormEntry>
	implements FormEntry, ModelWrapper<FormEntry> {

	public FormEntryWrapper(FormEntry formEntry) {
		super(formEntry);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("formEntryId", getFormEntryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("uniqueReference", getUniqueReference());
		attributes.put(
			"formInstanceRecordVersionId", getFormInstanceRecordVersionId());
		attributes.put("plid", getPlid());
		attributes.put("status", getStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long formEntryId = (Long)attributes.get("formEntryId");

		if (formEntryId != null) {
			setFormEntryId(formEntryId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String uniqueReference = (String)attributes.get("uniqueReference");

		if (uniqueReference != null) {
			setUniqueReference(uniqueReference);
		}

		Long formInstanceRecordVersionId = (Long)attributes.get(
			"formInstanceRecordVersionId");

		if (formInstanceRecordVersionId != null) {
			setFormInstanceRecordVersionId(formInstanceRecordVersionId);
		}

		Long plid = (Long)attributes.get("plid");

		if (plid != null) {
			setPlid(plid);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	@Override
	public FormEntry cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this form entry.
	 *
	 * @return the company ID of this form entry
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this form entry.
	 *
	 * @return the create date of this form entry
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the form entry ID of this form entry.
	 *
	 * @return the form entry ID of this form entry
	 */
	@Override
	public long getFormEntryId() {
		return model.getFormEntryId();
	}

	/**
	 * Returns the form instance record version ID of this form entry.
	 *
	 * @return the form instance record version ID of this form entry
	 */
	@Override
	public long getFormInstanceRecordVersionId() {
		return model.getFormInstanceRecordVersionId();
	}

	/**
	 * Returns the group ID of this form entry.
	 *
	 * @return the group ID of this form entry
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this form entry.
	 *
	 * @return the modified date of this form entry
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the plid of this form entry.
	 *
	 * @return the plid of this form entry
	 */
	@Override
	public long getPlid() {
		return model.getPlid();
	}

	/**
	 * Returns the primary key of this form entry.
	 *
	 * @return the primary key of this form entry
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the status of this form entry.
	 *
	 * @return the status of this form entry
	 */
	@Override
	public int getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the unique reference of this form entry.
	 *
	 * @return the unique reference of this form entry
	 */
	@Override
	public String getUniqueReference() {
		return model.getUniqueReference();
	}

	/**
	 * Returns the user ID of this form entry.
	 *
	 * @return the user ID of this form entry
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this form entry.
	 *
	 * @return the user name of this form entry
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this form entry.
	 *
	 * @return the user uuid of this form entry
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this form entry.
	 *
	 * @return the uuid of this form entry
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this form entry.
	 *
	 * @param companyId the company ID of this form entry
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this form entry.
	 *
	 * @param createDate the create date of this form entry
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the form entry ID of this form entry.
	 *
	 * @param formEntryId the form entry ID of this form entry
	 */
	@Override
	public void setFormEntryId(long formEntryId) {
		model.setFormEntryId(formEntryId);
	}

	/**
	 * Sets the form instance record version ID of this form entry.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID of this form entry
	 */
	@Override
	public void setFormInstanceRecordVersionId(
		long formInstanceRecordVersionId) {

		model.setFormInstanceRecordVersionId(formInstanceRecordVersionId);
	}

	/**
	 * Sets the group ID of this form entry.
	 *
	 * @param groupId the group ID of this form entry
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this form entry.
	 *
	 * @param modifiedDate the modified date of this form entry
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the plid of this form entry.
	 *
	 * @param plid the plid of this form entry
	 */
	@Override
	public void setPlid(long plid) {
		model.setPlid(plid);
	}

	/**
	 * Sets the primary key of this form entry.
	 *
	 * @param primaryKey the primary key of this form entry
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the status of this form entry.
	 *
	 * @param status the status of this form entry
	 */
	@Override
	public void setStatus(int status) {
		model.setStatus(status);
	}

	/**
	 * Sets the unique reference of this form entry.
	 *
	 * @param uniqueReference the unique reference of this form entry
	 */
	@Override
	public void setUniqueReference(String uniqueReference) {
		model.setUniqueReference(uniqueReference);
	}

	/**
	 * Sets the user ID of this form entry.
	 *
	 * @param userId the user ID of this form entry
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this form entry.
	 *
	 * @param userName the user name of this form entry
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this form entry.
	 *
	 * @param userUuid the user uuid of this form entry
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this form entry.
	 *
	 * @param uuid the uuid of this form entry
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected FormEntryWrapper wrap(FormEntry formEntry) {
		return new FormEntryWrapper(formEntry);
	}

}