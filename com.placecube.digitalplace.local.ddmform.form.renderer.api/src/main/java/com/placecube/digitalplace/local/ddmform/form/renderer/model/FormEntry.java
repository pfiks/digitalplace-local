/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the FormEntry service. Represents a row in the &quot;Placecube_FormRenderer_FormEntry&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryImpl"
)
@ProviderType
public interface FormEntry extends FormEntryModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<FormEntry, Long> FORM_ENTRY_ID_ACCESSOR =
		new Accessor<FormEntry, Long>() {

			@Override
			public Long get(FormEntry formEntry) {
				return formEntry.getFormEntryId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<FormEntry> getTypeClass() {
				return FormEntry.class;
			}

		};

}