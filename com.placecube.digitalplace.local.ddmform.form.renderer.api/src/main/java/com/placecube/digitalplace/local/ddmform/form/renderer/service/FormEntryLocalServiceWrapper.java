/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link FormEntryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryLocalService
 * @generated
 */
public class FormEntryLocalServiceWrapper
	implements FormEntryLocalService, ServiceWrapper<FormEntryLocalService> {

	public FormEntryLocalServiceWrapper() {
		this(null);
	}

	public FormEntryLocalServiceWrapper(
		FormEntryLocalService formEntryLocalService) {

		_formEntryLocalService = formEntryLocalService;
	}

	/**
	 * Adds the form entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntry the form entry
	 * @return the form entry that was added
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			addFormEntry(
				com.placecube.digitalplace.local.ddmform.form.renderer.model.
					FormEntry formEntry) {

		return _formEntryLocalService.addFormEntry(formEntry);
	}

	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				addFormEntry(
					long userId, long plid, long formInstanceRecordVersionId,
					int status,
					com.liferay.portal.kernel.service.ServiceContext
						serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.addFormEntry(
			userId, plid, formInstanceRecordVersionId, status, serviceContext);
	}

	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				addFormEntry(
					long userId, long plid, long formInstanceRecordVersionId,
					String uniqueReference, int status,
					com.liferay.portal.kernel.service.ServiceContext
						serviceContext)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.addFormEntry(
			userId, plid, formInstanceRecordVersionId, uniqueReference, status,
			serviceContext);
	}

	/**
	 * Creates a new form entry with the primary key. Does not add the form entry to the database.
	 *
	 * @param formEntryId the primary key for the new form entry
	 * @return the new form entry
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			createFormEntry(long formEntryId) {

		return _formEntryLocalService.createFormEntry(formEntryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the form entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntry the form entry
	 * @return the form entry that was removed
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			deleteFormEntry(
				com.placecube.digitalplace.local.ddmform.form.renderer.model.
					FormEntry formEntry) {

		return _formEntryLocalService.deleteFormEntry(formEntry);
	}

	/**
	 * Deletes the form entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry that was removed
	 * @throws PortalException if a form entry with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				deleteFormEntry(long formEntryId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.deleteFormEntry(formEntryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _formEntryLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _formEntryLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _formEntryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _formEntryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _formEntryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _formEntryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _formEntryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _formEntryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			fetchFormEntry(long formEntryId) {

		return _formEntryLocalService.fetchFormEntry(formEntryId);
	}

	/**
	 * Returns the form entry matching the UUID and group.
	 *
	 * @param uuid the form entry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			fetchFormEntryByUuidAndGroupId(String uuid, long groupId) {

		return _formEntryLocalService.fetchFormEntryByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _formEntryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _formEntryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns a range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.ddmform.form.renderer.model.impl.FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of form entries
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry>
			getFormEntries(int start, int end) {

		return _formEntryLocalService.getFormEntries(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry>
			getFormEntriesByUniqueReferenceAndStatus(
				String uniqueReference, int status) {

		return _formEntryLocalService.getFormEntriesByUniqueReferenceAndStatus(
			uniqueReference, status);
	}

	/**
	 * Returns all the form entries matching the UUID and company.
	 *
	 * @param uuid the UUID of the form entries
	 * @param companyId the primary key of the company
	 * @return the matching form entries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry>
			getFormEntriesByUuidAndCompanyId(String uuid, long companyId) {

		return _formEntryLocalService.getFormEntriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of form entries matching the UUID and company.
	 *
	 * @param uuid the UUID of the form entries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching form entries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry>
			getFormEntriesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.local.ddmform.form.renderer.
						model.FormEntry> orderByComparator) {

		return _formEntryLocalService.getFormEntriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of form entries.
	 *
	 * @return the number of form entries
	 */
	@Override
	public int getFormEntriesCount() {
		return _formEntryLocalService.getFormEntriesCount();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry>
			getFormEntriesModifiedBeforeDate(
				long companyId, long plid, java.util.Date dateBefore) {

		return _formEntryLocalService.getFormEntriesModifiedBeforeDate(
			companyId, plid, dateBefore);
	}

	/**
	 * Returns the form entry with the primary key.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry
	 * @throws PortalException if a form entry with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				getFormEntry(long formEntryId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.getFormEntry(formEntryId);
	}

	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				getFormEntryByFormInstanceRecordVersionId(
					long formInstanceRecordVersionId)
			throws com.placecube.digitalplace.local.ddmform.form.renderer.
				exception.NoSuchFormEntryException {

		return _formEntryLocalService.getFormEntryByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
	}

	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				getFormEntryByFormInstanceRecordVersionIdAndStatus(
					long formInstanceRecordVersionId, int status)
			throws com.placecube.digitalplace.local.ddmform.form.renderer.
				exception.NoSuchFormEntryException {

		return _formEntryLocalService.
			getFormEntryByFormInstanceRecordVersionIdAndStatus(
				formInstanceRecordVersionId, status);
	}

	/**
	 * Returns the form entry matching the UUID and group.
	 *
	 * @param uuid the form entry's UUID
	 * @param groupId the primary key of the group
	 * @return the matching form entry
	 * @throws PortalException if a matching form entry could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
				getFormEntryByUuidAndGroupId(String uuid, long groupId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.getFormEntryByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _formEntryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _formEntryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _formEntryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the form entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FormEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param formEntry the form entry
	 * @return the form entry that was updated
	 */
	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			updateFormEntry(
				com.placecube.digitalplace.local.ddmform.form.renderer.model.
					FormEntry formEntry) {

		return _formEntryLocalService.updateFormEntry(formEntry);
	}

	@Override
	public
		com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry
			updateFormEntry(
				com.placecube.digitalplace.local.ddmform.form.renderer.model.
					FormEntry formEntry,
				int status) {

		return _formEntryLocalService.updateFormEntry(formEntry, status);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _formEntryLocalService.getBasePersistence();
	}

	@Override
	public FormEntryLocalService getWrappedService() {
		return _formEntryLocalService;
	}

	@Override
	public void setWrappedService(FormEntryLocalService formEntryLocalService) {
		_formEntryLocalService = formEntryLocalService;
	}

	private FormEntryLocalService _formEntryLocalService;

}