/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.ddmform.form.renderer.model.FormEntry;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the form entry service. This utility wraps <code>com.placecube.digitalplace.local.ddmform.form.renderer.service.persistence.impl.FormEntryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormEntryPersistence
 * @generated
 */
public class FormEntryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(FormEntry formEntry) {
		getPersistence().clearCache(formEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, FormEntry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<FormEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<FormEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<FormEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static FormEntry update(FormEntry formEntry) {
		return getPersistence().update(formEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static FormEntry update(
		FormEntry formEntry, ServiceContext serviceContext) {

		return getPersistence().update(formEntry, serviceContext);
	}

	/**
	 * Returns all the form entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching form entries
	 */
	public static List<FormEntry> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	public static List<FormEntry> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	public static List<FormEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	public static List<FormEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUuid_First(
			String uuid, OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUuid_First(
		String uuid, OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUuid_Last(
			String uuid, OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUuid_Last(
		String uuid, OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uuid = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public static FormEntry[] findByUuid_PrevAndNext(
			long formEntryId, String uuid,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUuid_PrevAndNext(
			formEntryId, uuid, orderByComparator);
	}

	/**
	 * Removes all the form entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of form entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching form entries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the form entry where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the form entry where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the form entry that was removed
	 */
	public static FormEntry removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of form entries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching form entries
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching form entries
	 */
	public static List<FormEntry> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	public static List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	public static List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	public static List<FormEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public static FormEntry[] findByUuid_C_PrevAndNext(
			long formEntryId, String uuid, long companyId,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			formEntryId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the form entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of form entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching form entries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId) {

		return getPersistence().fetchByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId, boolean useFinderCache) {

		return getPersistence().fetchByFormInstanceRecordVersionId(
			formInstanceRecordVersionId, useFinderCache);
	}

	/**
	 * Removes the form entry where formInstanceRecordVersionId = &#63; from the database.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the form entry that was removed
	 */
	public static FormEntry removeByFormInstanceRecordVersionId(
			long formInstanceRecordVersionId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().removeByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
	}

	/**
	 * Returns the number of form entries where formInstanceRecordVersionId = &#63;.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @return the number of matching form entries
	 */
	public static int countByFormInstanceRecordVersionId(
		long formInstanceRecordVersionId) {

		return getPersistence().countByFormInstanceRecordVersionId(
			formInstanceRecordVersionId);
	}

	/**
	 * Returns all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @return the matching form entries
	 */
	public static List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		return getPersistence().findByUniqueReferenceAndStatus(
			uniqueReference, status);
	}

	/**
	 * Returns a range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of matching form entries
	 */
	public static List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end) {

		return getPersistence().findByUniqueReferenceAndStatus(
			uniqueReference, status, start, end);
	}

	/**
	 * Returns an ordered range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching form entries
	 */
	public static List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().findByUniqueReferenceAndStatus(
			uniqueReference, status, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching form entries
	 */
	public static List<FormEntry> findByUniqueReferenceAndStatus(
		String uniqueReference, int status, int start, int end,
		OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUniqueReferenceAndStatus(
			uniqueReference, status, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUniqueReferenceAndStatus_First(
			String uniqueReference, int status,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUniqueReferenceAndStatus_First(
			uniqueReference, status, orderByComparator);
	}

	/**
	 * Returns the first form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUniqueReferenceAndStatus_First(
		String uniqueReference, int status,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().fetchByUniqueReferenceAndStatus_First(
			uniqueReference, status, orderByComparator);
	}

	/**
	 * Returns the last form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByUniqueReferenceAndStatus_Last(
			String uniqueReference, int status,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUniqueReferenceAndStatus_Last(
			uniqueReference, status, orderByComparator);
	}

	/**
	 * Returns the last form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByUniqueReferenceAndStatus_Last(
		String uniqueReference, int status,
		OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().fetchByUniqueReferenceAndStatus_Last(
			uniqueReference, status, orderByComparator);
	}

	/**
	 * Returns the form entries before and after the current form entry in the ordered set where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param formEntryId the primary key of the current form entry
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public static FormEntry[] findByUniqueReferenceAndStatus_PrevAndNext(
			long formEntryId, String uniqueReference, int status,
			OrderByComparator<FormEntry> orderByComparator)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByUniqueReferenceAndStatus_PrevAndNext(
			formEntryId, uniqueReference, status, orderByComparator);
	}

	/**
	 * Removes all the form entries where uniqueReference = &#63; and status = &#63; from the database.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 */
	public static void removeByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		getPersistence().removeByUniqueReferenceAndStatus(
			uniqueReference, status);
	}

	/**
	 * Returns the number of form entries where uniqueReference = &#63; and status = &#63;.
	 *
	 * @param uniqueReference the unique reference
	 * @param status the status
	 * @return the number of matching form entries
	 */
	public static int countByUniqueReferenceAndStatus(
		String uniqueReference, int status) {

		return getPersistence().countByUniqueReferenceAndStatus(
			uniqueReference, status);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the matching form entry
	 * @throws NoSuchFormEntryException if a matching form entry could not be found
	 */
	public static FormEntry findByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status) {

		return getPersistence().fetchByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);
	}

	/**
	 * Returns the form entry where formInstanceRecordVersionId = &#63; and status = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching form entry, or <code>null</code> if a matching form entry could not be found
	 */
	public static FormEntry fetchByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status, boolean useFinderCache) {

		return getPersistence().fetchByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status, useFinderCache);
	}

	/**
	 * Removes the form entry where formInstanceRecordVersionId = &#63; and status = &#63; from the database.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the form entry that was removed
	 */
	public static FormEntry removeByFormInstanceRecordVersionIdAndStatus(
			long formInstanceRecordVersionId, int status)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().removeByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);
	}

	/**
	 * Returns the number of form entries where formInstanceRecordVersionId = &#63; and status = &#63;.
	 *
	 * @param formInstanceRecordVersionId the form instance record version ID
	 * @param status the status
	 * @return the number of matching form entries
	 */
	public static int countByFormInstanceRecordVersionIdAndStatus(
		long formInstanceRecordVersionId, int status) {

		return getPersistence().countByFormInstanceRecordVersionIdAndStatus(
			formInstanceRecordVersionId, status);
	}

	/**
	 * Caches the form entry in the entity cache if it is enabled.
	 *
	 * @param formEntry the form entry
	 */
	public static void cacheResult(FormEntry formEntry) {
		getPersistence().cacheResult(formEntry);
	}

	/**
	 * Caches the form entries in the entity cache if it is enabled.
	 *
	 * @param formEntries the form entries
	 */
	public static void cacheResult(List<FormEntry> formEntries) {
		getPersistence().cacheResult(formEntries);
	}

	/**
	 * Creates a new form entry with the primary key. Does not add the form entry to the database.
	 *
	 * @param formEntryId the primary key for the new form entry
	 * @return the new form entry
	 */
	public static FormEntry create(long formEntryId) {
		return getPersistence().create(formEntryId);
	}

	/**
	 * Removes the form entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry that was removed
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public static FormEntry remove(long formEntryId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().remove(formEntryId);
	}

	public static FormEntry updateImpl(FormEntry formEntry) {
		return getPersistence().updateImpl(formEntry);
	}

	/**
	 * Returns the form entry with the primary key or throws a <code>NoSuchFormEntryException</code> if it could not be found.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry
	 * @throws NoSuchFormEntryException if a form entry with the primary key could not be found
	 */
	public static FormEntry findByPrimaryKey(long formEntryId)
		throws com.placecube.digitalplace.local.ddmform.form.renderer.exception.
			NoSuchFormEntryException {

		return getPersistence().findByPrimaryKey(formEntryId);
	}

	/**
	 * Returns the form entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param formEntryId the primary key of the form entry
	 * @return the form entry, or <code>null</code> if a form entry with the primary key could not be found
	 */
	public static FormEntry fetchByPrimaryKey(long formEntryId) {
		return getPersistence().fetchByPrimaryKey(formEntryId);
	}

	/**
	 * Returns all the form entries.
	 *
	 * @return the form entries
	 */
	public static List<FormEntry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @return the range of form entries
	 */
	public static List<FormEntry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of form entries
	 */
	public static List<FormEntry> findAll(
		int start, int end, OrderByComparator<FormEntry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the form entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FormEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of form entries
	 * @param end the upper bound of the range of form entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of form entries
	 */
	public static List<FormEntry> findAll(
		int start, int end, OrderByComparator<FormEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the form entries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of form entries.
	 *
	 * @return the number of form entries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static FormEntryPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(FormEntryPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile FormEntryPersistence _persistence;

}