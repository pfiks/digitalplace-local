/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.local.transactionhistory.service.impl;

import java.util.Date;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.base.TransactionHistoryLocalServiceBaseImpl;

/**
 * The implementation of the transaction history local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory", service = AopService.class)
public class TransactionHistoryLocalServiceImpl extends TransactionHistoryLocalServiceBaseImpl {

	private static final Log LOG = LogFactoryUtil.getLog(TransactionHistoryLocalServiceImpl.class);

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.local.transactionhistory.service.
	 * TransactionHistoryLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.local.transactionhistory.service.
	 * TransactionHistoryLocalServiceUtil</code>.
	 */

	@Override
	public TransactionHistory addTransactionHistory(long companyId, long groupId, long userId, String userName, Date createDate, long classNameId, long classPK, String status) {

		TransactionHistory transactionHistory = transactionHistoryPersistence.fetchByClassNameIdAndClassPK(classNameId, classPK);

		if (Validator.isNull(transactionHistory)) {
			transactionHistory = transactionHistoryPersistence.create(counterLocalService.increment(TransactionHistory.class.getName(), 1));

			transactionHistory.setGroupId(groupId);
			transactionHistory.setCompanyId(companyId);
			transactionHistory.setUserId(userId);
			transactionHistory.setUserName(userName);
			transactionHistory.setCreateDate(createDate);
			transactionHistory.setModifiedDate(createDate);
			transactionHistory.setClassNameId(classNameId);
			transactionHistory.setClassPK(classPK);
			transactionHistory.setTransactionStatus(status);

			transactionHistory = transactionHistoryPersistence.update(transactionHistory);

			LOG.info("Transaction history added successfully - transactionHistoryId: " + transactionHistory.getTransactionHistoryId() + ", classPK:" + classPK + ", classNameId: " + classNameId + ", userId: " + userId);

		} else {
			LOG.warn("Transaction history has not been added since it already exists - transactionHistoryId: " + transactionHistory.getTransactionHistoryId());
		}

		return transactionHistory;
	}

	@Override
	public void deleteTransactionHistoryByClassNameIdAndClassPK(long classNameId, long classPK) throws NoSuchTransactionHistoryException {

		transactionHistoryPersistence.removeByClassNameIdAndClassPK(classNameId, classPK);

	}

	@Override
	public TransactionHistory fetchTransactionHistoryByClassNameIdAndClassPK(long classNameId, long classPK) {

		return transactionHistoryPersistence.fetchByClassNameIdAndClassPK(classNameId, classPK);
	}

}