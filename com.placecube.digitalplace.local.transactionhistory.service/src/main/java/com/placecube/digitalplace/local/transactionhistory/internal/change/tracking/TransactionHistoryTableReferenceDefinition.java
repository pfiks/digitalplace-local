/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.placecube.digitalplace.local.transactionhistory.internal.change.tracking;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.change.tracking.spi.reference.TableReferenceDefinition;
import com.liferay.change.tracking.spi.reference.builder.ChildTableReferenceInfoBuilder;
import com.liferay.change.tracking.spi.reference.builder.ParentTableReferenceInfoBuilder;
import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistoryTable;
import com.placecube.digitalplace.local.transactionhistory.service.persistence.TransactionHistoryPersistence;

@Component(service = TableReferenceDefinition.class)
public class TransactionHistoryTableReferenceDefinition implements TableReferenceDefinition<TransactionHistoryTable> {

	@Reference
	private TransactionHistoryPersistence transactionHistoryPersistence;

	@Override
	public BasePersistence<?> getBasePersistence() {
		return transactionHistoryPersistence;
	}

	@Override
	public TransactionHistoryTable getTable() {
		return TransactionHistoryTable.INSTANCE;
	}

	@Override
	public void defineChildTableReferences(ChildTableReferenceInfoBuilder<TransactionHistoryTable> childTableReferenceInfoBuilder) {
		// Not used
	}

	@Override
	public void defineParentTableReferences(ParentTableReferenceInfoBuilder<TransactionHistoryTable> parentTableReferenceInfoBuilder) {
		// Not used
	}

}