package com.placecube.digitalplace.local.transactionhistory.service.upgrade.upgradestep;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.liferay.portal.kernel.dao.db.IndexMetadata;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistoryTable;

public class PublicationsTableUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(PublicationsTableUpdate.class);

	@Override
	protected void doUpgrade() throws Exception {

		LOG.info("Initialised upgrade process for to add publication tracking to transaction history service.");

		TransactionHistoryTable transactionHistoryTable = TransactionHistoryTable.INSTANCE;
		String tableName = transactionHistoryTable.getTableName();

		if (hasColumn(tableName, "status")) {
			alterColumnName(tableName, "status", transactionHistoryTable.transactionStatus.getName() + " VARCHAR(75) null");
		}

		removePrimaryKey(tableName);
		addColumn(tableName, "mvccVersion", "LONG default 0 not null");
		addColumn(tableName, "ctCollectionId", "LONG default 0 not null");
		addColumn(tableName, "primary key", "(transactionHistoryId, ctCollectionId)");

		List<IndexMetadata> indexes = new ArrayList<>();
		dropIndexImmediately(tableName, "IX_E5B1D9C6");
		dropIndexImmediately(tableName, "IX_789B49E5");
		dropIndexImmediately(tableName, "IX_87C705A7");
		addIndexToList(indexes, "IX_59AC6424", tableName, false, transactionHistoryTable.classNameId.getName(), transactionHistoryTable.classPK.getName(),
				transactionHistoryTable.ctCollectionId.getName());
		addIndexToList(indexes, "IX_1047B643", tableName, false, transactionHistoryTable.uuid.getName(), transactionHistoryTable.companyId.getName(), transactionHistoryTable.ctCollectionId.getName());
		addIndexToList(indexes, "IX_C7D09EC1", tableName, false, transactionHistoryTable.uuid.getName(), transactionHistoryTable.ctCollectionId.getName());
		addIndexToList(indexes, "IX_9F57AE05", tableName, true, transactionHistoryTable.uuid.getName(), transactionHistoryTable.groupId.getName(), transactionHistoryTable.ctCollectionId.getName());

		processIndexChanges(indexes);

	}

	private void addColumn(String tableName, String columnName, String type) throws Exception {
		if (!hasColumn(tableName, columnName)) {
			LOG.info(String.format("Adding column %s to %s", columnName, tableName));
			alterTableAddColumn(tableName, columnName, type);
		}
	}

	private void addIndexToList(List<IndexMetadata> indexes, String indexName, String tableName, boolean unique, String... fields) {
		indexes.add(new IndexMetadata(indexName, tableName, unique, fields));
		LOG.info(String.format("Adding index:%s %s %s", indexName, tableName, Arrays.toString(fields)));
	}

	private void dropIndexImmediately(String tableName, String dropIndex) throws Exception {
		if (hasIndex(tableName, dropIndex)) {
			String dropSql = String.format("drop index %s on %s;", dropIndex, tableName);
			LOG.info("Dropping existing index:" + dropIndex + String.format(" - %s", dropSql));
			runSQL(connection, dropSql);
		}
	}

	private void processIndexChanges(List<IndexMetadata> newIndexes) throws Exception {
		for (IndexMetadata newIndex : newIndexes) {
			if (hasIndex(newIndex.getTableName(), newIndex.getIndexName())) {
				String dropSql = String.format("drop index %s on %s;", newIndex.getIndexName(), newIndex.getTableName());
				LOG.info("Dropping existing index:" + newIndex.getIndexName() + String.format(" - %s", dropSql));
				runSQL(connection, dropSql);
			}

			LOG.info(String.format("Adding index:%s %s %s", newIndex.getIndexName(), newIndex.getTableName(), Arrays.toString(newIndex.getColumnNames())));
		}

		addIndexes(connection, newIndexes);
	}

}
