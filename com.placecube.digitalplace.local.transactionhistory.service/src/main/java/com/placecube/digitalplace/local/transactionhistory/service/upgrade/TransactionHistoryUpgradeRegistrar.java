package com.placecube.digitalplace.local.transactionhistory.service.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.transactionhistory.service.upgrade.upgradestep.PublicationsTableUpdate;
import com.placecube.digitalplace.local.transactionhistory.service.upgrade.upgradestep.TransactionHistoryTableUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class TransactionHistoryUpgradeRegistrar implements UpgradeStepRegistrator {
	@Reference
	private CounterLocalService counterLocalService;
	
	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new TransactionHistoryTableUpdate(counterLocalService, ddmFormInstanceRecordLocalService, userLocalService));
		registry.register("1.0.1", "2.0.0", new PublicationsTableUpdate());
	}

}