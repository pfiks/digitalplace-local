/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.MVCCModel;

import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TransactionHistory in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class TransactionHistoryCacheModel
	implements CacheModel<TransactionHistory>, Externalizable, MVCCModel {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof TransactionHistoryCacheModel)) {
			return false;
		}

		TransactionHistoryCacheModel transactionHistoryCacheModel =
			(TransactionHistoryCacheModel)object;

		if ((transactionHistoryId ==
				transactionHistoryCacheModel.transactionHistoryId) &&
			(mvccVersion == transactionHistoryCacheModel.mvccVersion)) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = HashUtil.hash(0, transactionHistoryId);

		return HashUtil.hash(hashCode, mvccVersion);
	}

	@Override
	public long getMvccVersion() {
		return mvccVersion;
	}

	@Override
	public void setMvccVersion(long mvccVersion) {
		this.mvccVersion = mvccVersion;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{mvccVersion=");
		sb.append(mvccVersion);
		sb.append(", ctCollectionId=");
		sb.append(ctCollectionId);
		sb.append(", uuid=");
		sb.append(uuid);
		sb.append(", transactionHistoryId=");
		sb.append(transactionHistoryId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", classNameId=");
		sb.append(classNameId);
		sb.append(", classPK=");
		sb.append(classPK);
		sb.append(", transactionStatus=");
		sb.append(transactionStatus);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TransactionHistory toEntityModel() {
		TransactionHistoryImpl transactionHistoryImpl =
			new TransactionHistoryImpl();

		transactionHistoryImpl.setMvccVersion(mvccVersion);
		transactionHistoryImpl.setCtCollectionId(ctCollectionId);

		if (uuid == null) {
			transactionHistoryImpl.setUuid("");
		}
		else {
			transactionHistoryImpl.setUuid(uuid);
		}

		transactionHistoryImpl.setTransactionHistoryId(transactionHistoryId);
		transactionHistoryImpl.setGroupId(groupId);
		transactionHistoryImpl.setCompanyId(companyId);
		transactionHistoryImpl.setUserId(userId);

		if (userName == null) {
			transactionHistoryImpl.setUserName("");
		}
		else {
			transactionHistoryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			transactionHistoryImpl.setCreateDate(null);
		}
		else {
			transactionHistoryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			transactionHistoryImpl.setModifiedDate(null);
		}
		else {
			transactionHistoryImpl.setModifiedDate(new Date(modifiedDate));
		}

		transactionHistoryImpl.setClassNameId(classNameId);
		transactionHistoryImpl.setClassPK(classPK);

		if (transactionStatus == null) {
			transactionHistoryImpl.setTransactionStatus("");
		}
		else {
			transactionHistoryImpl.setTransactionStatus(transactionStatus);
		}

		transactionHistoryImpl.resetOriginalValues();

		return transactionHistoryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		mvccVersion = objectInput.readLong();

		ctCollectionId = objectInput.readLong();
		uuid = objectInput.readUTF();

		transactionHistoryId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		classNameId = objectInput.readLong();

		classPK = objectInput.readLong();
		transactionStatus = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(mvccVersion);

		objectOutput.writeLong(ctCollectionId);

		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(transactionHistoryId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(classNameId);

		objectOutput.writeLong(classPK);

		if (transactionStatus == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(transactionStatus);
		}
	}

	public long mvccVersion;
	public long ctCollectionId;
	public String uuid;
	public long transactionHistoryId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long classNameId;
	public long classPK;
	public String transactionStatus;

}