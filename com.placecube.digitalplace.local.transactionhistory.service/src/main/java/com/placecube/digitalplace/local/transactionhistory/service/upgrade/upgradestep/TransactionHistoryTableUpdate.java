package com.placecube.digitalplace.local.transactionhistory.service.upgrade.upgradestep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.dao.jdbc.AutoBatchPreparedStatementUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistoryTable;

public class TransactionHistoryTableUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(TransactionHistoryTableUpdate.class);

	private CounterLocalService counterLocalService;

	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	private UserLocalService userLocalService;

	public TransactionHistoryTableUpdate(CounterLocalService counterLocalService, DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService, UserLocalService userLocalService) {
		this.counterLocalService = counterLocalService;
		this.ddmFormInstanceRecordLocalService = ddmFormInstanceRecordLocalService;
		this.userLocalService = userLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {

		LOG.info("Creating transaction logs for users.");
		long classNameId = PortalUtil.getClassNameId(DDMFormInstanceRecord.class);

		List<User> users = userLocalService.getUsers(QueryUtil.ALL_POS, QueryUtil.ALL_POS);

		for (User user : users) {
			DynamicQuery dynamicQuery = ddmFormInstanceRecordLocalService.dynamicQuery();
			dynamicQuery.add(RestrictionsFactoryUtil.eq("userId", user.getUserId()));

			List<DDMFormInstanceRecord> ddmFormInstanceRecords = ddmFormInstanceRecordLocalService.dynamicQuery(dynamicQuery);

			PreparedStatement preparedStatement = AutoBatchPreparedStatementUtil.concurrentAutoBatch(connection,

					StringBundler.concat("INSERT INTO " + TransactionHistoryTable.INSTANCE.getName(),
							" (uuid_, transactionHistoryId, groupId, companyId, userId, userName, createDate, modifiedDate, classNameId, classPK, status)",
							" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"));

			for (DDMFormInstanceRecord ddmFormInstanceRecord : ddmFormInstanceRecords) {
				try {
					DDMFormInstanceRecordVersion ddmFormInstanceRecordVersion = ddmFormInstanceRecord.getFormInstanceRecordVersion();

					if (ddmFormInstanceRecordVersion.getStatus() != WorkflowConstants.STATUS_DRAFT && !transactionHistoryRecordExists(classNameId, ddmFormInstanceRecord.getFormInstanceRecordId())) {
						addTransactionHistoryEntry(ddmFormInstanceRecord, preparedStatement, classNameId);

						preparedStatement.addBatch();
					}

				} catch (PortalException e) {
					LOG.error("Unable to get DDMFormInstanceRecordVersion from DDMFormInstanceRecord - formInstanceRecordId: " + ddmFormInstanceRecord.getFormInstanceRecordId(), e);
				}
			}

			preparedStatement.executeBatch();
		}
	}

	private long addTransactionHistoryEntry(DDMFormInstanceRecord ddmFormInstanceRecord, PreparedStatement preparedStatement, long classNameId) throws SQLException {

		String uuid = PortalUUIDUtil.generate();
		long transactionHistoryId = counterLocalService.increment(TransactionHistory.class.getName(), 1);
		preparedStatement.setString(1, uuid);
		preparedStatement.setLong(2, transactionHistoryId);
		preparedStatement.setLong(3, ddmFormInstanceRecord.getGroupId());
		preparedStatement.setLong(4, ddmFormInstanceRecord.getCompanyId());
		preparedStatement.setLong(5, ddmFormInstanceRecord.getUserId());
		preparedStatement.setString(6, ddmFormInstanceRecord.getUserName());

		Timestamp createDate = new Timestamp(ddmFormInstanceRecord.getCreateDate().getTime());
		preparedStatement.setTimestamp(7, createDate);

		Timestamp modifiedDate = new Timestamp(ddmFormInstanceRecord.getModifiedDate().getTime());
		preparedStatement.setTimestamp(8, modifiedDate);
		preparedStatement.setLong(9, classNameId);
		preparedStatement.setLong(10, ddmFormInstanceRecord.getFormInstanceRecordId());
		preparedStatement.setString(11, TransactionHistoryStatusConstants.STATUS_SUBMITTED);

		return transactionHistoryId;
	}

	private boolean transactionHistoryRecordExists(long classNameId, long classNamePK) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + TransactionHistoryTable.INSTANCE.getName() + " WHERE classNameId = ? AND classPK = ?");
		preparedStatement.setLong(1, classNameId);
		preparedStatement.setLong(2, classNamePK);

		ResultSet resultSet = preparedStatement.executeQuery();

		return resultSet.next();
	}

}
