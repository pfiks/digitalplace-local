/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.change.tracking.CTColumnResolutionType;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.change.tracking.helper.CTPersistenceHelper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistoryTable;
import com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryImpl;
import com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl;
import com.placecube.digitalplace.local.transactionhistory.service.persistence.TransactionHistoryPersistence;
import com.placecube.digitalplace.local.transactionhistory.service.persistence.TransactionHistoryUtil;
import com.placecube.digitalplace.local.transactionhistory.service.persistence.impl.constants.Placecube_Account_TransactionsPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the transaction history service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = TransactionHistoryPersistence.class)
public class TransactionHistoryPersistenceImpl
	extends BasePersistenceImpl<TransactionHistory>
	implements TransactionHistoryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>TransactionHistoryUtil</code> to access the transaction history persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		TransactionHistoryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the transaction histories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache && productionMode) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache && productionMode) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<TransactionHistory> list = null;

		if (useFinderCache && productionMode) {
			list = (List<TransactionHistory>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TransactionHistory transactionHistory : list) {
					if (!uuid.equals(transactionHistory.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_TRANSACTIONHISTORY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(TransactionHistoryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<TransactionHistory>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache && productionMode) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory findByUuid_First(
			String uuid,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByUuid_First(
			uuid, orderByComparator);

		if (transactionHistory != null) {
			return transactionHistory;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchTransactionHistoryException(sb.toString());
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByUuid_First(
		String uuid, OrderByComparator<TransactionHistory> orderByComparator) {

		List<TransactionHistory> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory findByUuid_Last(
			String uuid,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByUuid_Last(
			uuid, orderByComparator);

		if (transactionHistory != null) {
			return transactionHistory;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchTransactionHistoryException(sb.toString());
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByUuid_Last(
		String uuid, OrderByComparator<TransactionHistory> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<TransactionHistory> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the transaction histories before and after the current transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param transactionHistoryId the primary key of the current transaction history
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory[] findByUuid_PrevAndNext(
			long transactionHistoryId, String uuid,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws NoSuchTransactionHistoryException {

		uuid = Objects.toString(uuid, "");

		TransactionHistory transactionHistory = findByPrimaryKey(
			transactionHistoryId);

		Session session = null;

		try {
			session = openSession();

			TransactionHistory[] array = new TransactionHistoryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, transactionHistory, uuid, orderByComparator, true);

			array[1] = transactionHistory;

			array[2] = getByUuid_PrevAndNext(
				session, transactionHistory, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected TransactionHistory getByUuid_PrevAndNext(
		Session session, TransactionHistory transactionHistory, String uuid,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_TRANSACTIONHISTORY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(TransactionHistoryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						transactionHistory)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<TransactionHistory> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the transaction histories where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (TransactionHistory transactionHistory :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(transactionHistory);
		}
	}

	/**
	 * Returns the number of transaction histories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching transaction histories
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		Long count = null;

		if (productionMode) {
			finderPath = _finderPathCountByUuid;

			finderArgs = new Object[] {uuid};

			count = (Long)finderCache.getResult(finderPath, finderArgs, this);
		}

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_TRANSACTIONHISTORY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				if (productionMode) {
					finderCache.putResult(finderPath, finderArgs, count);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"transactionHistory.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(transactionHistory.uuid IS NULL OR transactionHistory.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory findByUUID_G(String uuid, long groupId)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByUUID_G(uuid, groupId);

		if (transactionHistory == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchTransactionHistoryException(sb.toString());
		}

		return transactionHistory;
	}

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		if (result instanceof TransactionHistory) {
			TransactionHistory transactionHistory = (TransactionHistory)result;

			if (!Objects.equals(uuid, transactionHistory.getUuid()) ||
				(groupId != transactionHistory.getGroupId())) {

				result = null;
			}
			else if (!ctPersistenceHelper.isProductionMode(
						TransactionHistory.class,
						transactionHistory.getPrimaryKey())) {

				result = null;
			}
		}
		else if (!productionMode && (result instanceof List<?>)) {
			result = null;
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_TRANSACTIONHISTORY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<TransactionHistory> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache && productionMode) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					TransactionHistory transactionHistory = list.get(0);

					result = transactionHistory;

					cacheResult(transactionHistory);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TransactionHistory)result;
		}
	}

	/**
	 * Removes the transaction history where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the transaction history that was removed
	 */
	@Override
	public TransactionHistory removeByUUID_G(String uuid, long groupId)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = findByUUID_G(uuid, groupId);

		return remove(transactionHistory);
	}

	/**
	 * Returns the number of transaction histories where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching transaction histories
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		Long count = null;

		if (productionMode) {
			finderPath = _finderPathCountByUUID_G;

			finderArgs = new Object[] {uuid, groupId};

			count = (Long)finderCache.getResult(finderPath, finderArgs, this);
		}

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_TRANSACTIONHISTORY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				if (productionMode) {
					finderCache.putResult(finderPath, finderArgs, count);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"transactionHistory.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(transactionHistory.uuid IS NULL OR transactionHistory.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"transactionHistory.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching transaction histories
	 */
	@Override
	public List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache && productionMode) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache && productionMode) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<TransactionHistory> list = null;

		if (useFinderCache && productionMode) {
			list = (List<TransactionHistory>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TransactionHistory transactionHistory : list) {
					if (!uuid.equals(transactionHistory.getUuid()) ||
						(companyId != transactionHistory.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_TRANSACTIONHISTORY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(TransactionHistoryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<TransactionHistory>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache && productionMode) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (transactionHistory != null) {
			return transactionHistory;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchTransactionHistoryException(sb.toString());
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<TransactionHistory> orderByComparator) {

		List<TransactionHistory> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (transactionHistory != null) {
			return transactionHistory;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchTransactionHistoryException(sb.toString());
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<TransactionHistory> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<TransactionHistory> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the transaction histories before and after the current transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param transactionHistoryId the primary key of the current transaction history
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory[] findByUuid_C_PrevAndNext(
			long transactionHistoryId, String uuid, long companyId,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws NoSuchTransactionHistoryException {

		uuid = Objects.toString(uuid, "");

		TransactionHistory transactionHistory = findByPrimaryKey(
			transactionHistoryId);

		Session session = null;

		try {
			session = openSession();

			TransactionHistory[] array = new TransactionHistoryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, transactionHistory, uuid, companyId, orderByComparator,
				true);

			array[1] = transactionHistory;

			array[2] = getByUuid_C_PrevAndNext(
				session, transactionHistory, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected TransactionHistory getByUuid_C_PrevAndNext(
		Session session, TransactionHistory transactionHistory, String uuid,
		long companyId, OrderByComparator<TransactionHistory> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_TRANSACTIONHISTORY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(TransactionHistoryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						transactionHistory)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<TransactionHistory> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the transaction histories where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (TransactionHistory transactionHistory :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(transactionHistory);
		}
	}

	/**
	 * Returns the number of transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching transaction histories
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		Long count = null;

		if (productionMode) {
			finderPath = _finderPathCountByUuid_C;

			finderArgs = new Object[] {uuid, companyId};

			count = (Long)finderCache.getResult(finderPath, finderArgs, this);
		}

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_TRANSACTIONHISTORY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				if (productionMode) {
					finderCache.putResult(finderPath, finderArgs, count);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"transactionHistory.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(transactionHistory.uuid IS NULL OR transactionHistory.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"transactionHistory.companyId = ?";

	private FinderPath _finderPathFetchByClassNameIdAndClassPK;
	private FinderPath _finderPathCountByClassNameIdAndClassPK;

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory findByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByClassNameIdAndClassPK(
			classNameId, classPK);

		if (transactionHistory == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("classNameId=");
			sb.append(classNameId);

			sb.append(", classPK=");
			sb.append(classPK);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchTransactionHistoryException(sb.toString());
		}

		return transactionHistory;
	}

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByClassNameIdAndClassPK(
		long classNameId, long classPK) {

		return fetchByClassNameIdAndClassPK(classNameId, classPK, true);
	}

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchByClassNameIdAndClassPK(
		long classNameId, long classPK, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {classNameId, classPK};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByClassNameIdAndClassPK, finderArgs, this);
		}

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		if (result instanceof TransactionHistory) {
			TransactionHistory transactionHistory = (TransactionHistory)result;

			if ((classNameId != transactionHistory.getClassNameId()) ||
				(classPK != transactionHistory.getClassPK())) {

				result = null;
			}
			else if (!ctPersistenceHelper.isProductionMode(
						TransactionHistory.class,
						transactionHistory.getPrimaryKey())) {

				result = null;
			}
		}
		else if (!productionMode && (result instanceof List<?>)) {
			result = null;
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_TRANSACTIONHISTORY_WHERE);

			sb.append(_FINDER_COLUMN_CLASSNAMEIDANDCLASSPK_CLASSNAMEID_2);

			sb.append(_FINDER_COLUMN_CLASSNAMEIDANDCLASSPK_CLASSPK_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(classNameId);

				queryPos.add(classPK);

				List<TransactionHistory> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache && productionMode) {
						finderCache.putResult(
							_finderPathFetchByClassNameIdAndClassPK, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!productionMode || !useFinderCache) {
								finderArgs = new Object[] {
									classNameId, classPK
								};
							}

							_log.warn(
								"TransactionHistoryPersistenceImpl.fetchByClassNameIdAndClassPK(long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					TransactionHistory transactionHistory = list.get(0);

					result = transactionHistory;

					cacheResult(transactionHistory);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TransactionHistory)result;
		}
	}

	/**
	 * Removes the transaction history where classNameId = &#63; and classPK = &#63; from the database.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the transaction history that was removed
	 */
	@Override
	public TransactionHistory removeByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = findByClassNameIdAndClassPK(
			classNameId, classPK);

		return remove(transactionHistory);
	}

	/**
	 * Returns the number of transaction histories where classNameId = &#63; and classPK = &#63;.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the number of matching transaction histories
	 */
	@Override
	public int countByClassNameIdAndClassPK(long classNameId, long classPK) {
		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		Long count = null;

		if (productionMode) {
			finderPath = _finderPathCountByClassNameIdAndClassPK;

			finderArgs = new Object[] {classNameId, classPK};

			count = (Long)finderCache.getResult(finderPath, finderArgs, this);
		}

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_TRANSACTIONHISTORY_WHERE);

			sb.append(_FINDER_COLUMN_CLASSNAMEIDANDCLASSPK_CLASSNAMEID_2);

			sb.append(_FINDER_COLUMN_CLASSNAMEIDANDCLASSPK_CLASSPK_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(classNameId);

				queryPos.add(classPK);

				count = (Long)query.uniqueResult();

				if (productionMode) {
					finderCache.putResult(finderPath, finderArgs, count);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_CLASSNAMEIDANDCLASSPK_CLASSNAMEID_2 =
			"transactionHistory.classNameId = ? AND ";

	private static final String _FINDER_COLUMN_CLASSNAMEIDANDCLASSPK_CLASSPK_2 =
		"transactionHistory.classPK = ?";

	public TransactionHistoryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(TransactionHistory.class);

		setModelImplClass(TransactionHistoryImpl.class);
		setModelPKClass(long.class);

		setTable(TransactionHistoryTable.INSTANCE);
	}

	/**
	 * Caches the transaction history in the entity cache if it is enabled.
	 *
	 * @param transactionHistory the transaction history
	 */
	@Override
	public void cacheResult(TransactionHistory transactionHistory) {
		if (transactionHistory.getCtCollectionId() != 0) {
			return;
		}

		entityCache.putResult(
			TransactionHistoryImpl.class, transactionHistory.getPrimaryKey(),
			transactionHistory);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {
				transactionHistory.getUuid(), transactionHistory.getGroupId()
			},
			transactionHistory);

		finderCache.putResult(
			_finderPathFetchByClassNameIdAndClassPK,
			new Object[] {
				transactionHistory.getClassNameId(),
				transactionHistory.getClassPK()
			},
			transactionHistory);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the transaction histories in the entity cache if it is enabled.
	 *
	 * @param transactionHistories the transaction histories
	 */
	@Override
	public void cacheResult(List<TransactionHistory> transactionHistories) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (transactionHistories.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (TransactionHistory transactionHistory : transactionHistories) {
			if (transactionHistory.getCtCollectionId() != 0) {
				continue;
			}

			if (entityCache.getResult(
					TransactionHistoryImpl.class,
					transactionHistory.getPrimaryKey()) == null) {

				cacheResult(transactionHistory);
			}
		}
	}

	/**
	 * Clears the cache for all transaction histories.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TransactionHistoryImpl.class);

		finderCache.clearCache(TransactionHistoryImpl.class);
	}

	/**
	 * Clears the cache for the transaction history.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TransactionHistory transactionHistory) {
		entityCache.removeResult(
			TransactionHistoryImpl.class, transactionHistory);
	}

	@Override
	public void clearCache(List<TransactionHistory> transactionHistories) {
		for (TransactionHistory transactionHistory : transactionHistories) {
			entityCache.removeResult(
				TransactionHistoryImpl.class, transactionHistory);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(TransactionHistoryImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(TransactionHistoryImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		TransactionHistoryModelImpl transactionHistoryModelImpl) {

		Object[] args = new Object[] {
			transactionHistoryModelImpl.getUuid(),
			transactionHistoryModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, transactionHistoryModelImpl);

		args = new Object[] {
			transactionHistoryModelImpl.getClassNameId(),
			transactionHistoryModelImpl.getClassPK()
		};

		finderCache.putResult(
			_finderPathCountByClassNameIdAndClassPK, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByClassNameIdAndClassPK, args,
			transactionHistoryModelImpl);
	}

	/**
	 * Creates a new transaction history with the primary key. Does not add the transaction history to the database.
	 *
	 * @param transactionHistoryId the primary key for the new transaction history
	 * @return the new transaction history
	 */
	@Override
	public TransactionHistory create(long transactionHistoryId) {
		TransactionHistory transactionHistory = new TransactionHistoryImpl();

		transactionHistory.setNew(true);
		transactionHistory.setPrimaryKey(transactionHistoryId);

		String uuid = PortalUUIDUtil.generate();

		transactionHistory.setUuid(uuid);

		transactionHistory.setCompanyId(CompanyThreadLocal.getCompanyId());

		return transactionHistory;
	}

	/**
	 * Removes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory remove(long transactionHistoryId)
		throws NoSuchTransactionHistoryException {

		return remove((Serializable)transactionHistoryId);
	}

	/**
	 * Removes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory remove(Serializable primaryKey)
		throws NoSuchTransactionHistoryException {

		Session session = null;

		try {
			session = openSession();

			TransactionHistory transactionHistory =
				(TransactionHistory)session.get(
					TransactionHistoryImpl.class, primaryKey);

			if (transactionHistory == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTransactionHistoryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(transactionHistory);
		}
		catch (NoSuchTransactionHistoryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TransactionHistory removeImpl(
		TransactionHistory transactionHistory) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(transactionHistory)) {
				transactionHistory = (TransactionHistory)session.get(
					TransactionHistoryImpl.class,
					transactionHistory.getPrimaryKeyObj());
			}

			if ((transactionHistory != null) &&
				ctPersistenceHelper.isRemove(transactionHistory)) {

				session.delete(transactionHistory);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (transactionHistory != null) {
			clearCache(transactionHistory);
		}

		return transactionHistory;
	}

	@Override
	public TransactionHistory updateImpl(
		TransactionHistory transactionHistory) {

		boolean isNew = transactionHistory.isNew();

		if (!(transactionHistory instanceof TransactionHistoryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(transactionHistory.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					transactionHistory);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in transactionHistory proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom TransactionHistory implementation " +
					transactionHistory.getClass());
		}

		TransactionHistoryModelImpl transactionHistoryModelImpl =
			(TransactionHistoryModelImpl)transactionHistory;

		if (Validator.isNull(transactionHistory.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			transactionHistory.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (transactionHistory.getCreateDate() == null)) {
			if (serviceContext == null) {
				transactionHistory.setCreateDate(date);
			}
			else {
				transactionHistory.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!transactionHistoryModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				transactionHistory.setModifiedDate(date);
			}
			else {
				transactionHistory.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (ctPersistenceHelper.isInsert(transactionHistory)) {
				if (!isNew) {
					session.evict(
						TransactionHistoryImpl.class,
						transactionHistory.getPrimaryKeyObj());
				}

				session.save(transactionHistory);
			}
			else {
				transactionHistory = (TransactionHistory)session.merge(
					transactionHistory);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (transactionHistory.getCtCollectionId() != 0) {
			if (isNew) {
				transactionHistory.setNew(false);
			}

			transactionHistory.resetOriginalValues();

			return transactionHistory;
		}

		entityCache.putResult(
			TransactionHistoryImpl.class, transactionHistoryModelImpl, false,
			true);

		cacheUniqueFindersCache(transactionHistoryModelImpl);

		if (isNew) {
			transactionHistory.setNew(false);
		}

		transactionHistory.resetOriginalValues();

		return transactionHistory;
	}

	/**
	 * Returns the transaction history with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the transaction history
	 * @return the transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTransactionHistoryException {

		TransactionHistory transactionHistory = fetchByPrimaryKey(primaryKey);

		if (transactionHistory == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTransactionHistoryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return transactionHistory;
	}

	/**
	 * Returns the transaction history with the primary key or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory findByPrimaryKey(long transactionHistoryId)
		throws NoSuchTransactionHistoryException {

		return findByPrimaryKey((Serializable)transactionHistoryId);
	}

	/**
	 * Returns the transaction history with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the transaction history
	 * @return the transaction history, or <code>null</code> if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory fetchByPrimaryKey(Serializable primaryKey) {
		if (ctPersistenceHelper.isProductionMode(
				TransactionHistory.class, primaryKey)) {

			return super.fetchByPrimaryKey(primaryKey);
		}

		TransactionHistory transactionHistory = null;

		Session session = null;

		try {
			session = openSession();

			transactionHistory = (TransactionHistory)session.get(
				TransactionHistoryImpl.class, primaryKey);

			if (transactionHistory != null) {
				cacheResult(transactionHistory);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		return transactionHistory;
	}

	/**
	 * Returns the transaction history with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history, or <code>null</code> if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory fetchByPrimaryKey(long transactionHistoryId) {
		return fetchByPrimaryKey((Serializable)transactionHistoryId);
	}

	@Override
	public Map<Serializable, TransactionHistory> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (ctPersistenceHelper.isProductionMode(TransactionHistory.class)) {
			return super.fetchByPrimaryKeys(primaryKeys);
		}

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, TransactionHistory> map =
			new HashMap<Serializable, TransactionHistory>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			TransactionHistory transactionHistory = fetchByPrimaryKey(
				primaryKey);

			if (transactionHistory != null) {
				map.put(primaryKey, transactionHistory);
			}

			return map;
		}

		if ((databaseInMaxParameters > 0) &&
			(primaryKeys.size() > databaseInMaxParameters)) {

			Iterator<Serializable> iterator = primaryKeys.iterator();

			while (iterator.hasNext()) {
				Set<Serializable> page = new HashSet<>();

				for (int i = 0;
					 (i < databaseInMaxParameters) && iterator.hasNext(); i++) {

					page.add(iterator.next());
				}

				map.putAll(fetchByPrimaryKeys(page));
			}

			return map;
		}

		StringBundler sb = new StringBundler((primaryKeys.size() * 2) + 1);

		sb.append(getSelectSQL());
		sb.append(" WHERE ");
		sb.append(getPKDBName());
		sb.append(" IN (");

		for (Serializable primaryKey : primaryKeys) {
			sb.append((long)primaryKey);

			sb.append(",");
		}

		sb.setIndex(sb.index() - 1);

		sb.append(")");

		String sql = sb.toString();

		Session session = null;

		try {
			session = openSession();

			Query query = session.createQuery(sql);

			for (TransactionHistory transactionHistory :
					(List<TransactionHistory>)query.list()) {

				map.put(
					transactionHistory.getPrimaryKeyObj(), transactionHistory);

				cacheResult(transactionHistory);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the transaction histories.
	 *
	 * @return the transaction histories
	 */
	@Override
	public List<TransactionHistory> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of transaction histories
	 */
	@Override
	public List<TransactionHistory> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of transaction histories
	 */
	@Override
	public List<TransactionHistory> findAll(
		int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of transaction histories
	 */
	@Override
	public List<TransactionHistory> findAll(
		int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean useFinderCache) {

		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache && productionMode) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache && productionMode) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<TransactionHistory> list = null;

		if (useFinderCache && productionMode) {
			list = (List<TransactionHistory>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_TRANSACTIONHISTORY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_TRANSACTIONHISTORY;

				sql = sql.concat(TransactionHistoryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<TransactionHistory>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache && productionMode) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the transaction histories from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (TransactionHistory transactionHistory : findAll()) {
			remove(transactionHistory);
		}
	}

	/**
	 * Returns the number of transaction histories.
	 *
	 * @return the number of transaction histories
	 */
	@Override
	public int countAll() {
		boolean productionMode = ctPersistenceHelper.isProductionMode(
			TransactionHistory.class);

		Long count = null;

		if (productionMode) {
			count = (Long)finderCache.getResult(
				_finderPathCountAll, FINDER_ARGS_EMPTY, this);
		}

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(
					_SQL_COUNT_TRANSACTIONHISTORY);

				count = (Long)query.uniqueResult();

				if (productionMode) {
					finderCache.putResult(
						_finderPathCountAll, FINDER_ARGS_EMPTY, count);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "transactionHistoryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_TRANSACTIONHISTORY;
	}

	@Override
	public Set<String> getCTColumnNames(
		CTColumnResolutionType ctColumnResolutionType) {

		return _ctColumnNamesMap.getOrDefault(
			ctColumnResolutionType, Collections.emptySet());
	}

	@Override
	public List<String> getMappingTableNames() {
		return _mappingTableNames;
	}

	@Override
	public Map<String, Integer> getTableColumnsMap() {
		return TransactionHistoryModelImpl.TABLE_COLUMNS_MAP;
	}

	@Override
	public String getTableName() {
		return "Placecube_Account_Transactions_TransactionHistory";
	}

	@Override
	public List<String[]> getUniqueIndexColumnNames() {
		return _uniqueIndexColumnNames;
	}

	private static final Map<CTColumnResolutionType, Set<String>>
		_ctColumnNamesMap = new EnumMap<CTColumnResolutionType, Set<String>>(
			CTColumnResolutionType.class);
	private static final List<String> _mappingTableNames =
		new ArrayList<String>();
	private static final List<String[]> _uniqueIndexColumnNames =
		new ArrayList<String[]>();

	static {
		Set<String> ctControlColumnNames = new HashSet<String>();
		Set<String> ctIgnoreColumnNames = new HashSet<String>();
		Set<String> ctStrictColumnNames = new HashSet<String>();

		ctControlColumnNames.add("mvccVersion");
		ctControlColumnNames.add("ctCollectionId");
		ctStrictColumnNames.add("uuid_");
		ctStrictColumnNames.add("groupId");
		ctStrictColumnNames.add("companyId");
		ctStrictColumnNames.add("userId");
		ctStrictColumnNames.add("userName");
		ctStrictColumnNames.add("createDate");
		ctIgnoreColumnNames.add("modifiedDate");
		ctStrictColumnNames.add("classNameId");
		ctStrictColumnNames.add("classPK");
		ctStrictColumnNames.add("transactionStatus");

		_ctColumnNamesMap.put(
			CTColumnResolutionType.CONTROL, ctControlColumnNames);
		_ctColumnNamesMap.put(
			CTColumnResolutionType.IGNORE, ctIgnoreColumnNames);
		_ctColumnNamesMap.put(
			CTColumnResolutionType.PK,
			Collections.singleton("transactionHistoryId"));
		_ctColumnNamesMap.put(
			CTColumnResolutionType.STRICT, ctStrictColumnNames);

		_uniqueIndexColumnNames.add(new String[] {"uuid_", "groupId"});
	}

	/**
	 * Initializes the transaction history persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathFetchByClassNameIdAndClassPK = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByClassNameIdAndClassPK",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"classNameId", "classPK"}, true);

		_finderPathCountByClassNameIdAndClassPK = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByClassNameIdAndClassPK",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"classNameId", "classPK"}, false);

		TransactionHistoryUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		TransactionHistoryUtil.setPersistence(null);

		entityCache.removeCache(TransactionHistoryImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_Account_TransactionsPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_Account_TransactionsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_Account_TransactionsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected CTPersistenceHelper ctPersistenceHelper;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_TRANSACTIONHISTORY =
		"SELECT transactionHistory FROM TransactionHistory transactionHistory";

	private static final String _SQL_SELECT_TRANSACTIONHISTORY_WHERE =
		"SELECT transactionHistory FROM TransactionHistory transactionHistory WHERE ";

	private static final String _SQL_COUNT_TRANSACTIONHISTORY =
		"SELECT COUNT(transactionHistory) FROM TransactionHistory transactionHistory";

	private static final String _SQL_COUNT_TRANSACTIONHISTORY_WHERE =
		"SELECT COUNT(transactionHistory) FROM TransactionHistory transactionHistory WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "transactionHistory.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No TransactionHistory exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No TransactionHistory exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		TransactionHistoryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}