package com.placecube.digitalplace.local.transactionhistory.internal.model.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;

@Component(immediate = true, service = ModelListener.class)
public class DDMFormInstanceRecordVersionModelListener extends BaseModelListener<DDMFormInstanceRecordVersion> {

	private static final Log LOG = LogFactoryUtil.getLog(DDMFormInstanceRecordVersionModelListener.class);

	@Reference
	private Portal portal;

	@Reference
	private TransactionHistoryLocalService transactionHistoryLocalService;

	@Override
	public void onAfterRemove(DDMFormInstanceRecordVersion model) throws ModelListenerException {

		try {
			transactionHistoryLocalService.deleteTransactionHistoryByClassNameIdAndClassPK(portal.getClassNameId(DDMFormInstanceRecord.class), model.getFormInstanceRecordId());
		} catch (NoSuchTransactionHistoryException e) {
			LOG.warn("Unable to delete transaction history for formInstanceRecordId: " + model.getFormInstanceRecordId());
		}

		super.onAfterRemove(model);
	}

	@Override
	public void onAfterUpdate(DDMFormInstanceRecordVersion originalModel, DDMFormInstanceRecordVersion model) throws ModelListenerException {

		if (model.getStatus() != WorkflowConstants.STATUS_DRAFT) {

			long classNameId = portal.getClassNameId(DDMFormInstanceRecord.class);
			long classPK = model.getFormInstanceRecordId();

			TransactionHistory transactionHistory = transactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(classNameId, classPK);

			if (Validator.isNull(transactionHistory)) {
				transactionHistory = transactionHistoryLocalService.addTransactionHistory(model.getCompanyId(), model.getGroupId(), model.getUserId(), model.getUserName(), model.getCreateDate(),
						classNameId, classPK, TransactionHistoryStatusConstants.STATUS_SUBMITTED);

				if (LOG.isDebugEnabled()) {
					LOG.debug("Transaction history added successfully - transactionHistoryId: " + transactionHistory.getTransactionHistoryId() + ", userId: " + model.getUserId()
							+ ", classPK (formInstanceRecordId): " + model.getFormInstanceRecordId());
				}
			}
		}

		super.onAfterUpdate(originalModel, model);
	}

}
