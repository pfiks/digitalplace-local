create table Placecube_Account_Transactions_TransactionHistory (
	mvccVersion LONG default 0 not null,
	ctCollectionId LONG default 0 not null,
	uuid_ VARCHAR(75) null,
	transactionHistoryId LONG not null,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	classNameId LONG,
	classPK LONG,
	transactionStatus VARCHAR(75) null,
	primary key (transactionHistoryId, ctCollectionId)
);