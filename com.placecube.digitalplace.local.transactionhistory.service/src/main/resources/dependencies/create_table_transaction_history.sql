create table Placecube_Account_TransactionHistory (
	uuid_ VARCHAR(75) null,
	transactionHistoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	classNameId LONG,
	classPK LONG,
	status VARCHAR(75) null
);