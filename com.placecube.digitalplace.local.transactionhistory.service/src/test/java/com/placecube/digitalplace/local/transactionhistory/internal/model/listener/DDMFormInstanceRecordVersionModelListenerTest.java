package com.placecube.digitalplace.local.transactionhistory.internal.model.listener;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecordVersion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.TransactionHistoryLocalService;
import com.placecube.digitalplace.local.transactionhistory.service.persistence.TransactionHistoryPersistence;

@RunWith(PowerMockRunner.class)
public class DDMFormInstanceRecordVersionModelListenerTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 5;
	private static final long COMPANY_ID = 2;
	private static final long FORM_INSTANCE_RECORD_ID = 6;
	private static final long GROUP_ID = 3;
	private static final long TRANSACTION_HISTORY_ID = 1;
	private static final long USER_ID = 4;
	private static final String USER_NAME = "USER_NAME";

	@InjectMocks
	private DDMFormInstanceRecordVersionModelListener ddmFormInstanceRecordVersionModelListener;

	@Mock
	private Date mockDate;

	@Mock
	private DDMFormInstanceRecordVersion mockDDMFormInstanceRecordVersion;

	@Mock
	private DDMFormInstanceRecordVersion mockOriginalDDMFormInstanceRecordVersion;

	@Mock
	private Portal mockPortal;

	@Mock
	private TransactionHistory mockTransactionHistory;

	@Mock
	private TransactionHistoryLocalService mockTransactionHistoryLocalService;

	@Mock
	private TransactionHistoryPersistence mockTransactionHistoryPersistence;

	@Test
	public void onAfterRemove_WhenNoError_ThenTransactionHistoryIsDeleted() throws NoSuchTransactionHistoryException {

		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);

		ddmFormInstanceRecordVersionModelListener.onAfterRemove(mockDDMFormInstanceRecordVersion);

		verify(mockTransactionHistoryLocalService, times(1)).deleteTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void onAfterRemove_WhenNoSuchTransactionHistoryException_ThenExceptionIsNotThrown() throws NoSuchTransactionHistoryException {

		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);

		when(mockTransactionHistoryPersistence.removeByClassNameIdAndClassPK(CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID)).thenThrow(NoSuchTransactionHistoryException.class);

		try {
			ddmFormInstanceRecordVersionModelListener.onAfterRemove(mockDDMFormInstanceRecordVersion);
		} catch (Exception e) {
			fail("No exception expected to be thrown");
		}

		verify(mockTransactionHistoryLocalService, times(1)).deleteTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void onAfterUpdate_WhenNoError_ThenTransactionHistoryIsAdded() throws PortalException {

		when(mockDDMFormInstanceRecordVersion.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);

		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID)).thenReturn(null);

		when(mockDDMFormInstanceRecordVersion.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMFormInstanceRecordVersion.getGroupId()).thenReturn(GROUP_ID);
		when(mockDDMFormInstanceRecordVersion.getUserId()).thenReturn(USER_ID);
		when(mockDDMFormInstanceRecordVersion.getUserName()).thenReturn(USER_NAME);
		when(mockDDMFormInstanceRecordVersion.getCreateDate()).thenReturn(mockDate);
		when(mockTransactionHistory.getTransactionHistoryId()).thenReturn(TRANSACTION_HISTORY_ID);

		when(mockTransactionHistoryLocalService.addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, mockDate, CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID,
				TransactionHistoryStatusConstants.STATUS_SUBMITTED)).thenReturn(mockTransactionHistory);

		ddmFormInstanceRecordVersionModelListener.onAfterUpdate(mockOriginalDDMFormInstanceRecordVersion, mockDDMFormInstanceRecordVersion);

		verify(mockTransactionHistoryLocalService, times(1)).addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, mockDate, CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID,
				TransactionHistoryStatusConstants.STATUS_SUBMITTED);
	}

	@Test
	public void onAfterUpdate_WhenStatusIsDraft_ThenTransactionHistoryIsNotAdded() throws PortalException {

		when(mockDDMFormInstanceRecordVersion.getStatus()).thenReturn(WorkflowConstants.STATUS_DRAFT);

		ddmFormInstanceRecordVersionModelListener.onAfterUpdate(mockOriginalDDMFormInstanceRecordVersion, mockDDMFormInstanceRecordVersion);

		verify(mockTransactionHistoryLocalService, never()).addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, mockDate, CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID,
				TransactionHistoryStatusConstants.STATUS_SUBMITTED);
	}

	@Test
	public void onAfterUpdate_WhenTransactionHistoryAlreadyExists_ThenTransactionHistoryIsNotAdded() throws PortalException {

		when(mockDDMFormInstanceRecordVersion.getStatus()).thenReturn(WorkflowConstants.STATUS_APPROVED);
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(CLASS_NAME_ID);
		when(mockDDMFormInstanceRecordVersion.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);

		when(mockTransactionHistoryLocalService.fetchTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID)).thenReturn(mockTransactionHistory);

		ddmFormInstanceRecordVersionModelListener.onAfterUpdate(mockOriginalDDMFormInstanceRecordVersion, mockDDMFormInstanceRecordVersion);

		verify(mockTransactionHistoryLocalService, never()).addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, mockDate, CLASS_NAME_ID, FORM_INSTANCE_RECORD_ID,
				TransactionHistoryStatusConstants.STATUS_SUBMITTED);
	}
}
