package com.placecube.digitalplace.local.transactionhistory.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.local.transactionhistory.constants.TransactionHistoryStatusConstants;
import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;
import com.placecube.digitalplace.local.transactionhistory.service.persistence.TransactionHistoryPersistence;

@RunWith(PowerMockRunner.class)
public class TransactionHistoryLocalServiceImplTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 5;
	private static final long CLASS_PK = 6;
	private static final long COMPANY_ID = 2;
	private static final long GROUP_ID = 3;
	private static final long TRANSACTION_HISTORY_ID = 1;
	private static final long USER_ID = 4;
	private static final String USER_NAME = "USER_NAME";

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private TransactionHistory mockTransactionHistory;

	@Mock
	private TransactionHistoryPersistence mockTransactionHistoryPersistence;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private TransactionHistoryLocalServiceImpl transactionHistoryLocalServiceImpl;

	@Test
	public void addTransactionHistory_WhenNoError_ThenTransactionHistoryIsCreated() throws PortalException {

		when(mockTransactionHistoryPersistence.fetchByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(null);

		when(mockCounterLocalService.increment(TransactionHistory.class.getName(), 1)).thenReturn(TRANSACTION_HISTORY_ID);
		when(mockTransactionHistoryPersistence.create(TRANSACTION_HISTORY_ID)).thenReturn(mockTransactionHistory);
		when(mockTransactionHistoryPersistence.update(mockTransactionHistory)).thenReturn(mockTransactionHistory);
		when(mockTransactionHistory.getTransactionHistoryId()).thenReturn(TRANSACTION_HISTORY_ID);

		transactionHistoryLocalServiceImpl.addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, new Date(), CLASS_NAME_ID, CLASS_PK, TransactionHistoryStatusConstants.STATUS_SUBMITTED);

		InOrder inOrder = inOrder(mockTransactionHistory, mockTransactionHistoryPersistence);
		inOrder.verify(mockTransactionHistory, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockTransactionHistory, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockTransactionHistory, times(1)).setUserId(USER_ID);
		inOrder.verify(mockTransactionHistory, times(1)).setUserName(USER_NAME);
		inOrder.verify(mockTransactionHistory, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockTransactionHistory, times(1)).setClassNameId(CLASS_NAME_ID);
		inOrder.verify(mockTransactionHistory, times(1)).setClassPK(CLASS_PK);
		inOrder.verify(mockTransactionHistory, times(1)).setTransactionStatus(TransactionHistoryStatusConstants.STATUS_SUBMITTED);
		inOrder.verify(mockTransactionHistoryPersistence, times(1)).update(mockTransactionHistory);
	}

	@Test
	public void addTransactionHistory_WhenTransactionHistoryAlreadyExists_ThenReturnExistingTransactionHistory() throws PortalException {

		when(mockTransactionHistoryPersistence.fetchByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockTransactionHistory);

		transactionHistoryLocalServiceImpl.addTransactionHistory(COMPANY_ID, GROUP_ID, USER_ID, USER_NAME, new Date(), CLASS_NAME_ID, CLASS_PK, TransactionHistoryStatusConstants.STATUS_SUBMITTED);

		verify(mockCounterLocalService, never()).increment(TransactionHistory.class.getName());
		verify(mockTransactionHistoryPersistence, never()).create(TRANSACTION_HISTORY_ID);
		verify(mockTransactionHistory, never()).setGroupId(GROUP_ID);
		verify(mockTransactionHistory, never()).setCompanyId(COMPANY_ID);
		verify(mockTransactionHistory, never()).setUserId(USER_ID);
		verify(mockTransactionHistory, never()).setUserName(USER_NAME);
		verify(mockTransactionHistory, never()).setCreateDate(any(Date.class));
		verify(mockTransactionHistory, never()).setClassNameId(CLASS_NAME_ID);
		verify(mockTransactionHistory, never()).setClassPK(CLASS_PK);
		verify(mockTransactionHistory, never()).setTransactionStatus(TransactionHistoryStatusConstants.STATUS_SUBMITTED);
		verify(mockTransactionHistoryPersistence, never()).update(mockTransactionHistory);
	}

	@Test
	public void deleteTransactionHistoryByClassNameIdAndClassPK_WhenNoError_ThenTransactionHistoryIsDeleted() throws NoSuchTransactionHistoryException {
		when(mockTransactionHistoryPersistence.removeByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenReturn(mockTransactionHistory);

		transactionHistoryLocalServiceImpl.deleteTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK);

		verify(mockTransactionHistoryPersistence, times(1)).removeByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK);
	}

	@Test(expected = NoSuchTransactionHistoryException.class)
	public void deleteTransactionHistoryByClassNameIdAndClassPK_WhenTransactionHistoryDoesNotExist_ThenNoSuchTransactionHistoryExceptionIsThrown() throws NoSuchTransactionHistoryException {
		when(mockTransactionHistoryPersistence.removeByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK)).thenThrow(NoSuchTransactionHistoryException.class);

		transactionHistoryLocalServiceImpl.deleteTransactionHistoryByClassNameIdAndClassPK(CLASS_NAME_ID, CLASS_PK);
	}

}
