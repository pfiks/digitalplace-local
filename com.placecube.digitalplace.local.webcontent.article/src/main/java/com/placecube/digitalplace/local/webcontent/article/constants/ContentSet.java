package com.placecube.digitalplace.local.webcontent.article.constants;

public enum ContentSet {

	POPULAR_PAGES("Popular Pages");

	private final String name;

	private ContentSet(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
