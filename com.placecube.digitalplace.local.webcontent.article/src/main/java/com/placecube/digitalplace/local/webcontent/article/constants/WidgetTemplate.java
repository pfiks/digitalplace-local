package com.placecube.digitalplace.local.webcontent.article.constants;

public enum WidgetTemplate {

	CATEGORY_CONTENT_LIST("ASSET-PUBLISHER-CATEGORY-CONTENT-LIST", "Category Content List"),

	FEATURED_CONTENT_NAV_PANELS("ASSET-PUBLISHER-FEATURED-CONTENT-NAV-PANELS", "Featured Content Nav Panels"),

	FEATURED_CONTENT_NAV_PANELS_LARGE("ASSET-PUBLISHER-FEATURED-CONTENT-NAV-PANELS-LARGE", "Featured Content Nav Panels Large"),

	RELATED_CONTENT_LIST("ASSET-PUBLISHER-RELATED-CONTENT-LIST", "Related Content List");

	private final String key;
	private final String name;

	private WidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
