package com.placecube.digitalplace.local.webcontent.article.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.webcontent.article.upgrade.upgrade_1_0_0.DDMTemplateTableUpdate;
import com.placecube.digitalplace.local.webcontent.article.upgrade.upgrade_1_0_1.DisplayURLMethodUpdateUpgradeProcess;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class ArticleUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new DDMTemplateTableUpdate(ddmTemplateLocalService));
		registry.register("1.0.0", "1.0.1", new DisplayURLMethodUpdateUpgradeProcess(companyLocalService, ddmTemplateLocalService));
	}

}
