package com.placecube.digitalplace.local.webcontent.article.upgrade.upgrade_1_0_0;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.local.webcontent.article.constants.WidgetTemplate;

public class DDMTemplateTableUpdate extends UpgradeProcess {

	private final DDMTemplateLocalService ddmTemplateLocalService;

	public DDMTemplateTableUpdate(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() {
		DynamicQuery dynamicQueryPanels = ddmTemplateLocalService.dynamicQuery();
		Criterion criterionPanels = RestrictionsFactoryUtil.eq("templateKey", WidgetTemplate.FEATURED_CONTENT_NAV_PANELS.getKey());
		Criterion criterionPanelsLarge = RestrictionsFactoryUtil.eq("templateKey", WidgetTemplate.FEATURED_CONTENT_NAV_PANELS_LARGE.getKey());
		dynamicQueryPanels.add(RestrictionsFactoryUtil.or(criterionPanels, criterionPanelsLarge));

		List<DDMTemplate> ddmTemplatesPanels = ddmTemplateLocalService.dynamicQuery(dynamicQueryPanels);

		for (DDMTemplate ddmTemplate : ddmTemplatesPanels) {
			ddmTemplate.setScript(ddmTemplate.getScript().replace("fragments-editor", "page-editor").replace("container.py-3", "lfr-layout-structure-item-container"));
			ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
		}
	}

}
