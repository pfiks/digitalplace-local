package com.placecube.digitalplace.local.webcontent.article.constants;

public enum ArticleDDMTemplate {

	ARTICLE("ARTICLE", "Article");

	private final String key;
	private final String name;

	private ArticleDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
