package com.placecube.digitalplace.local.webcontent.article.service;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleConstants;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleDDMTemplate;
import com.placecube.digitalplace.local.webcontent.article.constants.ContentSet;
import com.placecube.digitalplace.local.webcontent.article.constants.WidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

import java.io.IOException;
import java.util.Collections;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = ArticleWebContentService.class)
public class ArticleWebContentService {

	@Reference
	private AssetListEntryInitializer assetListEntryInitializer;

	@Reference
	private DDMInitializer ddmInitializer;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private Portal portal;

	public AssetListEntry getOrCreateContentSet(ContentSet contentSet, ServiceContext serviceContext) throws PortalException {
		return assetListEntryInitializer.getOrCreateContentSet(contentSet.getName(), true, serviceContext);
	}

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {

		String structureResourcePath = "com/placecube/digitalplace/local/webcontent/article/dependencies/ddm/structure.xml";

		return ddmInitializer.getOrCreateDDMStructure(ArticleConstants.STRUCTURE_KEY, structureResourcePath, getClass().getClassLoader(), JournalArticle.class, serviceContext);

	}

	public DDMTemplate getOrCreateWidgetTemplate(WidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = getTemplateResourcePath(widgetTemplate);
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath,
				getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}

	public String getDDMTemplateScript(WidgetTemplate widgetTemplate) throws IOException {
		String templateResourcePath = getTemplateResourcePath(widgetTemplate);
		return StringUtil.read(getClass().getClassLoader(), templateResourcePath);
	}

	private String getTemplateResourcePath(WidgetTemplate widgetTemplate) {
		return "com/placecube/digitalplace/local/webcontent/article/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
	}

	public DDMTemplate getOrCreateDDMTemplate(ArticleDDMTemplate articleDDMTemplate, ServiceContext serviceContext) throws PortalException {
		try {
			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), articleDDMTemplate.getKey());

			return Validator.isNotNull(ddmTemplate) ? ddmTemplate : addDDMTemplate(articleDDMTemplate, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private DDMTemplate addDDMTemplate(ArticleDDMTemplate articleDDMTemplate, ServiceContext serviceContext) throws PortalException {

		try {
			String script = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/article/dependencies/ddm/" + articleDDMTemplate.getKey() + ".ftl");

			DDMStructure guideStructure = ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), ArticleConstants.STRUCTURE_KEY);

			return ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), guideStructure.getStructureId(),
					guideStructure.getClassNameId(), articleDDMTemplate.getKey(), Collections.singletonMap(serviceContext.getLocale(), articleDDMTemplate.getName()), null,
					DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, script, true, false, StringPool.BLANK, null,
					serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}
}
