package com.placecube.digitalplace.local.webcontent.article.constants;

public final class ArticleConstants {

	public static final String STRUCTURE_KEY = "ARTICLE";

	private ArticleConstants() {
	}

}
