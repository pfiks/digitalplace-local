<#if entries?has_content>
	<#assign journalArticleRetrievalService = digitalplace_journalArticleRetrievalService />

	<div class="category-content-list-assets-widget">
		<h2 class="section-title">In this section</h2>		
		<div class="row section-entries">
		
			<#assign sortedEntries = journalArticleRetrievalService.getAssetEntriesOrderedByTitle(entries, locale) />
			<#list sortedEntries as curEntry>
				 	
				<#assign journalArticle = curEntry.getAssetRenderer().getArticle() />

				<div class="col-md-4 entry">
					<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />
					<#if articleURLOptional.isPresent()>
						<#assign articleURL = articleURLOptional.get() />
					<#else>
						<#assign articleURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
					</#if>
					
					<span class="icon-arrow-right padded-round-icon-small"></span>
					<a href="${articleURL}">
						<span class="entry-title">${curEntry.getTitle(locale)}</span>
					</a>
				</div>
			  
			</#list>	
		
		</div>
	</div>
</#if>