<#if entries?has_content>
	<div class="featured-content-nav-panels-assets-widget">
		<#assign journalArticleRetrievalService = digitalplace_journalArticleRetrievalService />
		
		<div class="section-content card-deck">
			<#list entries as curEntry>
			 	
			 	<#assign journalArticle = curEntry.getAssetRenderer().getArticle() />
				<div class="card-cell">
					<div class="popular-page card">
						<div class="card-icon">
							<span class="padded-round-icon glyphicon glyphicon-arrow-right"></span>
						</div>
						
						<div class="card-body">
							<h2 class="h3 card-title">
								<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />
								<#if articleURLOptional.isPresent()>
									<#assign articleURL = articleURLOptional.get() />
								<#else>
									<#assign articleURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
								</#if>
							
								<a href="${articleURL}">
									${curEntry.getTitle(locale)}
								</a>
							</h2>
							
							<#assign articleSummary = journalArticle.getDescription(locale) />
							<#if articleSummary?has_content>
								<p class="card-text">
									${articleSummary}
								</p>
							</#if>
						</div>
					</div>
				</div>
			</#list>
		</div>
	</div>
<#else>
    <div class="featured-content-nav-panels-assets-widget-no-values">
		<script type="text/javascript">
			if($('.featured-content-nav-panels-assets-widget-no-values').parents('div[class*="page-editor"]').length===0){
				var containerDiv = $('.featured-content-nav-panels-assets-widget-no-values').closest('.lfr-layout-structure-item-container');
				var containerDivHeight = $(containerDiv).height();
				if(containerDivHeight <= 40){
					$(containerDiv).addClass('hide');
				}
			}
		</script>
    </div>
</#if>