<#if Summary?? && validator.isNotNull(Summary.getData())>
    <div class="lead pb-3">
        ${Summary.getData()}
    </div>
</#if>

<#if Summary.Content??>
    <div class="pb-3">
        ${Summary.Content.getData()}
    </div>
</#if>

<#if Summary.FurtherInfo??>
    <#assign webContentData = jsonFactoryUtil.createJSONObject(Summary.FurtherInfo.getData()) />
    <#if webContentData?? && webContentData.classPK??>
        <#assign journalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
        <#assign furtherInfoArticle = journalService.fetchLatestArticle(getterUtil.getLong(webContentData.classPK)) />
        <#assign content = journalService.getArticleContent(furtherInfoArticle,null,null,themeDisplay.getLanguageId(),null,themeDisplay)/>
    	${content}
	</#if>
</#if>