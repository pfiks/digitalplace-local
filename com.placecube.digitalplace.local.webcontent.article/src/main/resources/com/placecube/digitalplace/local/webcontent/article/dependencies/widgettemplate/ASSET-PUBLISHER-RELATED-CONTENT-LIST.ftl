<#if entries?has_content>
	<#assign journalArticleRetrievalService = digitalplace_journalArticleRetrievalService />

	<div class="related-content-list-widget">
		<h2 class="section-title">Related pages</h2>
			
		<ul class="related-assets-entries">
		
			<#assign sortedEntries = journalArticleRetrievalService.getAssetEntriesOrderedByTitle(entries, locale) />
			<#list sortedEntries as curEntry>
				 	
				<#assign journalArticle = curEntry.getAssetRenderer().getArticle() />
			 	 	 	 	 	 	
				<li class="related-asset-entry">
					<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />
					<#if articleURLOptional.isPresent()>
						<#assign articleURL = articleURLOptional.get() />
					<#else>
						<#assign articleURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
					</#if>
				
					<a href="${articleURL}">
						${curEntry.getTitle(locale)}
					</a>
				</li>
			  
			</#list>	
		
		</ul>
	</div>
</#if>