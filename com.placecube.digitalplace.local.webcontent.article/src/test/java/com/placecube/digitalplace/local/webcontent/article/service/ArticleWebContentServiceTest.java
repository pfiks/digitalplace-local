package com.placecube.digitalplace.local.webcontent.article.service;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Collections;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleConstants;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleDDMTemplate;
import com.placecube.digitalplace.local.webcontent.article.constants.ContentSet;
import com.placecube.digitalplace.local.webcontent.article.constants.WidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.DDMInitializer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class ArticleWebContentServiceTest extends PowerMockito {

	public static final String STRUCTURE_RESOURCE_PATH = "com/placecube/digitalplace/local/webcontent/article/dependencies/ddm/structure.xml";

	public static final String TEMPLATE_RESOURCE_PATH = "com/placecube/digitalplace/local/webcontent/article/dependencies/widgettemplate/" + WidgetTemplate.CATEGORY_CONTENT_LIST.getKey() + ".ftl";

	private static final Locale LOCALE = Locale.CANADA;

	@InjectMocks
	private ArticleWebContentService articleWebContentService;

	private ClassLoader articleWebContentServiceClassLoader;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private AssetListEntryInitializer mockAssetListEntryInitializer;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Portal mockPortal;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Before
	public void setUp() {
		PowerMockito.mockStatic(StringUtil.class);
		articleWebContentServiceClassLoader = ArticleWebContentService.class.getClassLoader();
	}

	@Test(expected = PortalException.class)
	public void getOrCreateContentSet_WhenException_ThenThrowsPortalException() throws Exception {
		when(mockAssetListEntryInitializer.getOrCreateContentSet(ContentSet.POPULAR_PAGES.getName(), true, mockServiceContext)).thenThrow(new PortalException());

		articleWebContentService.getOrCreateContentSet(ContentSet.POPULAR_PAGES, mockServiceContext);
	}

	@Test
	public void getOrCreateContentSet_WhenNoError_ThenReturnsTheContentSet() throws Exception {
		when(mockAssetListEntryInitializer.getOrCreateContentSet(ContentSet.POPULAR_PAGES.getName(), true, mockServiceContext)).thenReturn(mockAssetListEntry);

		AssetListEntry result = articleWebContentService.getOrCreateContentSet(ContentSet.POPULAR_PAGES, mockServiceContext);

		assertThat(result, sameInstance(mockAssetListEntry));
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheArticleStructure() throws PortalException {

		when(mockDDMInitializer.getOrCreateDDMStructure(ArticleConstants.STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, getClass().getClassLoader(), JournalArticle.class, mockServiceContext))
				.thenReturn(mockDDMStructure);

		DDMStructure ddmStructure = articleWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(ddmStructure, sameInstance(mockDDMStructure));

	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		when(mockDDMInitializer.getOrCreateDDMStructure(ArticleConstants.STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, getClass().getClassLoader(), JournalArticle.class, mockServiceContext))
				.thenThrow(new PortalException());

		articleWebContentService.getOrCreateDDMStructure(mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		when(mockDDMInitializer
				.getDDMTemplateContextForPortletDisplay(WidgetTemplate.CATEGORY_CONTENT_LIST.getName(), WidgetTemplate.CATEGORY_CONTENT_LIST.getKey(), AssetEntry.class, TEMPLATE_RESOURCE_PATH,
						articleWebContentServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = articleWebContentService.getOrCreateWidgetTemplate(WidgetTemplate.CATEGORY_CONTENT_LIST, mockServiceContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenException_ThenThrowsPortalException() throws Exception {

		when(mockDDMInitializer
				.getDDMTemplateContextForPortletDisplay(WidgetTemplate.CATEGORY_CONTENT_LIST.getName(), WidgetTemplate.CATEGORY_CONTENT_LIST.getKey(), AssetEntry.class, TEMPLATE_RESOURCE_PATH,
						articleWebContentServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		articleWebContentService.getOrCreateWidgetTemplate(WidgetTemplate.CATEGORY_CONTENT_LIST, mockServiceContext);

	}

	@Test(expected = IOException.class)
	public void getDDMTemplateScript_WhenErrorOnReadingFile_ThenExceptionIsThrown() throws Exception {
		when(StringUtil.read(eq(articleWebContentServiceClassLoader), anyString())).thenThrow(new IOException());

		articleWebContentService.getDDMTemplateScript(WidgetTemplate.CATEGORY_CONTENT_LIST);
	}

	@Test
	public void getDDMTemplateScript_WhenNoError_ThenReturnsScriptAsString() throws Exception {
		WidgetTemplate widgetTemplate = WidgetTemplate.RELATED_CONTENT_LIST;
		String templateResourcePath = "com/placecube/digitalplace/local/webcontent/article/dependencies/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
		String expected = "This script";

		when(StringUtil.read(articleWebContentServiceClassLoader, templateResourcePath)).thenReturn(expected);

		String actual = articleWebContentService.getDDMTemplateScript(WidgetTemplate.RELATED_CONTENT_LIST);

		assertEquals(expected, actual);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateExists_ThenReturnTemplate() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, ArticleDDMTemplate.ARTICLE.getKey())).thenReturn(mockDDMTemplate);

		DDMTemplate result = articleWebContentService.getOrCreateDDMTemplate(ArticleDDMTemplate.ARTICLE, mockServiceContext);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		long groupId = 123;
		long classNameId = 456;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, ArticleDDMTemplate.ARTICLE.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, classNameId, ArticleConstants.STRUCTURE_KEY)).thenThrow(new PortalException());

		articleWebContentService.getOrCreateDDMTemplate(ArticleDDMTemplate.ARTICLE, mockServiceContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExist_ThenCreatesTemplate() throws PortalException {

		long groupId = 1;
		long classNameId = 2;
		long userId = 3;
		long structureId = 4;
		long journalArticleClassNameId = 5;

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(classNameId);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(journalArticleClassNameId);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, ArticleDDMTemplate.ARTICLE.getKey())).thenReturn(null);
		when(mockDDMStructureLocalService.getStructure(groupId, journalArticleClassNameId, ArticleConstants.STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getClassNameId()).thenReturn(classNameId);

		articleWebContentService.getOrCreateDDMTemplate(ArticleDDMTemplate.ARTICLE, mockServiceContext);

		verify(mockDDMTemplateLocalService, times(1)).addTemplate(userId, groupId, classNameId, structureId, classNameId, ArticleDDMTemplate.ARTICLE.getKey(),
				Collections.singletonMap(LOCALE, ArticleDDMTemplate.ARTICLE.getName()), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE,
				TemplateConstants.LANG_TYPE_FTL, null, true, false, StringPool.BLANK, null, mockServiceContext);
	}
}
