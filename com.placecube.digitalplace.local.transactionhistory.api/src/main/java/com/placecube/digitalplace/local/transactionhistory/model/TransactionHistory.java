/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the TransactionHistory service. Represents a row in the &quot;Placecube_Account_Transactions_TransactionHistory&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryImpl"
)
@ProviderType
public interface TransactionHistory
	extends PersistedModel, TransactionHistoryModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<TransactionHistory, Long>
		TRANSACTION_HISTORY_ID_ACCESSOR =
			new Accessor<TransactionHistory, Long>() {

				@Override
				public Long get(TransactionHistory transactionHistory) {
					return transactionHistory.getTransactionHistoryId();
				}

				@Override
				public Class<Long> getAttributeClass() {
					return Long.class;
				}

				@Override
				public Class<TransactionHistory> getTypeClass() {
					return TransactionHistory.class;
				}

			};

}