/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.petra.function.UnsafeFunction;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.change.tracking.CTAware;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.change.tracking.CTService;
import com.liferay.portal.kernel.service.persistence.change.tracking.CTPersistence;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for TransactionHistory. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryLocalServiceUtil
 * @generated
 */
@CTAware
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface TransactionHistoryLocalService
	extends BaseLocalService, CTService<TransactionHistory>,
			PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.local.transactionhistory.service.impl.TransactionHistoryLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the transaction history local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link TransactionHistoryLocalServiceUtil} if injection and service tracking are not available.
	 */
	public TransactionHistory addTransactionHistory(
		long companyId, long groupId, long userId, String userName,
		Date createDate, long classNameId, long classPK, String status);

	/**
	 * Adds the transaction history to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public TransactionHistory addTransactionHistory(
		TransactionHistory transactionHistory);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Creates a new transaction history with the primary key. Does not add the transaction history to the database.
	 *
	 * @param transactionHistoryId the primary key for the new transaction history
	 * @return the new transaction history
	 */
	@Transactional(enabled = false)
	public TransactionHistory createTransactionHistory(
		long transactionHistoryId);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws PortalException if a transaction history with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public TransactionHistory deleteTransactionHistory(
			long transactionHistoryId)
		throws PortalException;

	/**
	 * Deletes the transaction history from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public TransactionHistory deleteTransactionHistory(
		TransactionHistory transactionHistory);

	public void deleteTransactionHistoryByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws NoSuchTransactionHistoryException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TransactionHistory fetchTransactionHistory(
		long transactionHistoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TransactionHistory fetchTransactionHistoryByClassNameIdAndClassPK(
		long classNameId, long classPK);

	/**
	 * Returns the transaction history matching the UUID and group.
	 *
	 * @param uuid the transaction history's UUID
	 * @param groupId the primary key of the group
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TransactionHistory fetchTransactionHistoryByUuidAndGroupId(
		String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns a range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of transaction histories
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TransactionHistory> getTransactionHistories(int start, int end);

	/**
	 * Returns all the transaction histories matching the UUID and company.
	 *
	 * @param uuid the UUID of the transaction histories
	 * @param companyId the primary key of the company
	 * @return the matching transaction histories, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TransactionHistory> getTransactionHistoriesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of transaction histories matching the UUID and company.
	 *
	 * @param uuid the UUID of the transaction histories
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching transaction histories, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TransactionHistory> getTransactionHistoriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator);

	/**
	 * Returns the number of transaction histories.
	 *
	 * @return the number of transaction histories
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTransactionHistoriesCount();

	/**
	 * Returns the transaction history with the primary key.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history
	 * @throws PortalException if a transaction history with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TransactionHistory getTransactionHistory(long transactionHistoryId)
		throws PortalException;

	/**
	 * Returns the transaction history matching the UUID and group.
	 *
	 * @param uuid the transaction history's UUID
	 * @param groupId the primary key of the group
	 * @return the matching transaction history
	 * @throws PortalException if a matching transaction history could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TransactionHistory getTransactionHistoryByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException;

	/**
	 * Updates the transaction history in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public TransactionHistory updateTransactionHistory(
		TransactionHistory transactionHistory);

	@Override
	@Transactional(enabled = false)
	public CTPersistence<TransactionHistory> getCTPersistence();

	@Override
	@Transactional(enabled = false)
	public Class<TransactionHistory> getModelClass();

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public <R, E extends Throwable> R updateWithUnsafeFunction(
			UnsafeFunction<CTPersistence<TransactionHistory>, R, E>
				updateUnsafeFunction)
		throws E;

}