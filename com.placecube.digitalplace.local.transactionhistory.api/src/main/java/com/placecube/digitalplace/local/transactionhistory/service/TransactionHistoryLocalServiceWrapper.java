/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.service;

import com.liferay.petra.function.UnsafeFunction;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.service.persistence.change.tracking.CTPersistence;

import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;

/**
 * Provides a wrapper for {@link TransactionHistoryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryLocalService
 * @generated
 */
public class TransactionHistoryLocalServiceWrapper
	implements ServiceWrapper<TransactionHistoryLocalService>,
			   TransactionHistoryLocalService {

	public TransactionHistoryLocalServiceWrapper() {
		this(null);
	}

	public TransactionHistoryLocalServiceWrapper(
		TransactionHistoryLocalService transactionHistoryLocalService) {

		_transactionHistoryLocalService = transactionHistoryLocalService;
	}

	@Override
	public TransactionHistory addTransactionHistory(
		long companyId, long groupId, long userId, String userName,
		java.util.Date createDate, long classNameId, long classPK,
		String status) {

		return _transactionHistoryLocalService.addTransactionHistory(
			companyId, groupId, userId, userName, createDate, classNameId,
			classPK, status);
	}

	/**
	 * Adds the transaction history to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was added
	 */
	@Override
	public TransactionHistory addTransactionHistory(
		TransactionHistory transactionHistory) {

		return _transactionHistoryLocalService.addTransactionHistory(
			transactionHistory);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _transactionHistoryLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new transaction history with the primary key. Does not add the transaction history to the database.
	 *
	 * @param transactionHistoryId the primary key for the new transaction history
	 * @return the new transaction history
	 */
	@Override
	public TransactionHistory createTransactionHistory(
		long transactionHistoryId) {

		return _transactionHistoryLocalService.createTransactionHistory(
			transactionHistoryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _transactionHistoryLocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws PortalException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory deleteTransactionHistory(
			long transactionHistoryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _transactionHistoryLocalService.deleteTransactionHistory(
			transactionHistoryId);
	}

	/**
	 * Deletes the transaction history from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was removed
	 */
	@Override
	public TransactionHistory deleteTransactionHistory(
		TransactionHistory transactionHistory) {

		return _transactionHistoryLocalService.deleteTransactionHistory(
			transactionHistory);
	}

	@Override
	public void deleteTransactionHistoryByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		_transactionHistoryLocalService.
			deleteTransactionHistoryByClassNameIdAndClassPK(
				classNameId, classPK);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _transactionHistoryLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _transactionHistoryLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _transactionHistoryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _transactionHistoryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _transactionHistoryLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _transactionHistoryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _transactionHistoryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _transactionHistoryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public TransactionHistory fetchTransactionHistory(
		long transactionHistoryId) {

		return _transactionHistoryLocalService.fetchTransactionHistory(
			transactionHistoryId);
	}

	@Override
	public TransactionHistory fetchTransactionHistoryByClassNameIdAndClassPK(
		long classNameId, long classPK) {

		return _transactionHistoryLocalService.
			fetchTransactionHistoryByClassNameIdAndClassPK(
				classNameId, classPK);
	}

	/**
	 * Returns the transaction history matching the UUID and group.
	 *
	 * @param uuid the transaction history's UUID
	 * @param groupId the primary key of the group
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory fetchTransactionHistoryByUuidAndGroupId(
		String uuid, long groupId) {

		return _transactionHistoryLocalService.
			fetchTransactionHistoryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _transactionHistoryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _transactionHistoryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _transactionHistoryLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _transactionHistoryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _transactionHistoryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of transaction histories
	 */
	@Override
	public java.util.List<TransactionHistory> getTransactionHistories(
		int start, int end) {

		return _transactionHistoryLocalService.getTransactionHistories(
			start, end);
	}

	/**
	 * Returns all the transaction histories matching the UUID and company.
	 *
	 * @param uuid the UUID of the transaction histories
	 * @param companyId the primary key of the company
	 * @return the matching transaction histories, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<TransactionHistory>
		getTransactionHistoriesByUuidAndCompanyId(String uuid, long companyId) {

		return _transactionHistoryLocalService.
			getTransactionHistoriesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of transaction histories matching the UUID and company.
	 *
	 * @param uuid the UUID of the transaction histories
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching transaction histories, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<TransactionHistory>
		getTransactionHistoriesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator) {

		return _transactionHistoryLocalService.
			getTransactionHistoriesByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of transaction histories.
	 *
	 * @return the number of transaction histories
	 */
	@Override
	public int getTransactionHistoriesCount() {
		return _transactionHistoryLocalService.getTransactionHistoriesCount();
	}

	/**
	 * Returns the transaction history with the primary key.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history
	 * @throws PortalException if a transaction history with the primary key could not be found
	 */
	@Override
	public TransactionHistory getTransactionHistory(long transactionHistoryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _transactionHistoryLocalService.getTransactionHistory(
			transactionHistoryId);
	}

	/**
	 * Returns the transaction history matching the UUID and group.
	 *
	 * @param uuid the transaction history's UUID
	 * @param groupId the primary key of the group
	 * @return the matching transaction history
	 * @throws PortalException if a matching transaction history could not be found
	 */
	@Override
	public TransactionHistory getTransactionHistoryByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _transactionHistoryLocalService.
			getTransactionHistoryByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Updates the transaction history in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was updated
	 */
	@Override
	public TransactionHistory updateTransactionHistory(
		TransactionHistory transactionHistory) {

		return _transactionHistoryLocalService.updateTransactionHistory(
			transactionHistory);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _transactionHistoryLocalService.getBasePersistence();
	}

	@Override
	public CTPersistence<TransactionHistory> getCTPersistence() {
		return _transactionHistoryLocalService.getCTPersistence();
	}

	@Override
	public Class<TransactionHistory> getModelClass() {
		return _transactionHistoryLocalService.getModelClass();
	}

	@Override
	public <R, E extends Throwable> R updateWithUnsafeFunction(
			UnsafeFunction<CTPersistence<TransactionHistory>, R, E>
				updateUnsafeFunction)
		throws E {

		return _transactionHistoryLocalService.updateWithUnsafeFunction(
			updateUnsafeFunction);
	}

	@Override
	public TransactionHistoryLocalService getWrappedService() {
		return _transactionHistoryLocalService;
	}

	@Override
	public void setWrappedService(
		TransactionHistoryLocalService transactionHistoryLocalService) {

		_transactionHistoryLocalService = transactionHistoryLocalService;
	}

	private TransactionHistoryLocalService _transactionHistoryLocalService;

}