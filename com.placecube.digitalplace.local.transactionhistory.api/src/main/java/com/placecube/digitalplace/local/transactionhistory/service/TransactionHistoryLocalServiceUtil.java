/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for TransactionHistory. This utility wraps
 * <code>com.placecube.digitalplace.local.transactionhistory.service.impl.TransactionHistoryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryLocalService
 * @generated
 */
public class TransactionHistoryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.local.transactionhistory.service.impl.TransactionHistoryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static TransactionHistory addTransactionHistory(
		long companyId, long groupId, long userId, String userName,
		java.util.Date createDate, long classNameId, long classPK,
		String status) {

		return getService().addTransactionHistory(
			companyId, groupId, userId, userName, createDate, classNameId,
			classPK, status);
	}

	/**
	 * Adds the transaction history to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was added
	 */
	public static TransactionHistory addTransactionHistory(
		TransactionHistory transactionHistory) {

		return getService().addTransactionHistory(transactionHistory);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new transaction history with the primary key. Does not add the transaction history to the database.
	 *
	 * @param transactionHistoryId the primary key for the new transaction history
	 * @return the new transaction history
	 */
	public static TransactionHistory createTransactionHistory(
		long transactionHistoryId) {

		return getService().createTransactionHistory(transactionHistoryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws PortalException if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory deleteTransactionHistory(
			long transactionHistoryId)
		throws PortalException {

		return getService().deleteTransactionHistory(transactionHistoryId);
	}

	/**
	 * Deletes the transaction history from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was removed
	 */
	public static TransactionHistory deleteTransactionHistory(
		TransactionHistory transactionHistory) {

		return getService().deleteTransactionHistory(transactionHistory);
	}

	public static void deleteTransactionHistoryByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		getService().deleteTransactionHistoryByClassNameIdAndClassPK(
			classNameId, classPK);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static TransactionHistory fetchTransactionHistory(
		long transactionHistoryId) {

		return getService().fetchTransactionHistory(transactionHistoryId);
	}

	public static TransactionHistory
		fetchTransactionHistoryByClassNameIdAndClassPK(
			long classNameId, long classPK) {

		return getService().fetchTransactionHistoryByClassNameIdAndClassPK(
			classNameId, classPK);
	}

	/**
	 * Returns the transaction history matching the UUID and group.
	 *
	 * @param uuid the transaction history's UUID
	 * @param groupId the primary key of the group
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchTransactionHistoryByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchTransactionHistoryByUuidAndGroupId(
			uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.transactionhistory.model.impl.TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of transaction histories
	 */
	public static List<TransactionHistory> getTransactionHistories(
		int start, int end) {

		return getService().getTransactionHistories(start, end);
	}

	/**
	 * Returns all the transaction histories matching the UUID and company.
	 *
	 * @param uuid the UUID of the transaction histories
	 * @param companyId the primary key of the company
	 * @return the matching transaction histories, or an empty list if no matches were found
	 */
	public static List<TransactionHistory>
		getTransactionHistoriesByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getTransactionHistoriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of transaction histories matching the UUID and company.
	 *
	 * @param uuid the UUID of the transaction histories
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching transaction histories, or an empty list if no matches were found
	 */
	public static List<TransactionHistory>
		getTransactionHistoriesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			OrderByComparator<TransactionHistory> orderByComparator) {

		return getService().getTransactionHistoriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of transaction histories.
	 *
	 * @return the number of transaction histories
	 */
	public static int getTransactionHistoriesCount() {
		return getService().getTransactionHistoriesCount();
	}

	/**
	 * Returns the transaction history with the primary key.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history
	 * @throws PortalException if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory getTransactionHistory(
			long transactionHistoryId)
		throws PortalException {

		return getService().getTransactionHistory(transactionHistoryId);
	}

	/**
	 * Returns the transaction history matching the UUID and group.
	 *
	 * @param uuid the transaction history's UUID
	 * @param groupId the primary key of the group
	 * @return the matching transaction history
	 * @throws PortalException if a matching transaction history could not be found
	 */
	public static TransactionHistory getTransactionHistoryByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getTransactionHistoryByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Updates the transaction history in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect TransactionHistoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param transactionHistory the transaction history
	 * @return the transaction history that was updated
	 */
	public static TransactionHistory updateTransactionHistory(
		TransactionHistory transactionHistory) {

		return getService().updateTransactionHistory(transactionHistory);
	}

	public static TransactionHistoryLocalService getService() {
		return _service;
	}

	public static void setService(TransactionHistoryLocalService service) {
		_service = service;
	}

	private static volatile TransactionHistoryLocalService _service;

}