/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Account_Transactions_TransactionHistory&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistory
 * @generated
 */
public class TransactionHistoryTable
	extends BaseTable<TransactionHistoryTable> {

	public static final TransactionHistoryTable INSTANCE =
		new TransactionHistoryTable();

	public final Column<TransactionHistoryTable, Long> mvccVersion =
		createColumn(
			"mvccVersion", Long.class, Types.BIGINT, Column.FLAG_NULLITY);
	public final Column<TransactionHistoryTable, Long> ctCollectionId =
		createColumn(
			"ctCollectionId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<TransactionHistoryTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Long> transactionHistoryId =
		createColumn(
			"transactionHistoryId", Long.class, Types.BIGINT,
			Column.FLAG_PRIMARY);
	public final Column<TransactionHistoryTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, String> userName =
		createColumn(
			"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Date> createDate =
		createColumn(
			"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Date> modifiedDate =
		createColumn(
			"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Long> classNameId =
		createColumn(
			"classNameId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, Long> classPK = createColumn(
		"classPK", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<TransactionHistoryTable, String> transactionStatus =
		createColumn(
			"transactionStatus", String.class, Types.VARCHAR,
			Column.FLAG_DEFAULT);

	private TransactionHistoryTable() {
		super(
			"Placecube_Account_Transactions_TransactionHistory",
			TransactionHistoryTable::new);
	}

}