/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the transaction history service. This utility wraps <code>com.placecube.digitalplace.local.transactionhistory.service.persistence.impl.TransactionHistoryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryPersistence
 * @generated
 */
public class TransactionHistoryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(TransactionHistory transactionHistory) {
		getPersistence().clearCache(transactionHistory);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, TransactionHistory> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TransactionHistory> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TransactionHistory> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TransactionHistory> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static TransactionHistory update(
		TransactionHistory transactionHistory) {

		return getPersistence().update(transactionHistory);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static TransactionHistory update(
		TransactionHistory transactionHistory, ServiceContext serviceContext) {

		return getPersistence().update(transactionHistory, serviceContext);
	}

	/**
	 * Returns all the transaction histories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public static TransactionHistory findByUuid_First(
			String uuid,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByUuid_First(
		String uuid, OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public static TransactionHistory findByUuid_Last(
			String uuid,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByUuid_Last(
		String uuid, OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the transaction histories before and after the current transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param transactionHistoryId the primary key of the current transaction history
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory[] findByUuid_PrevAndNext(
			long transactionHistoryId, String uuid,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUuid_PrevAndNext(
			transactionHistoryId, uuid, orderByComparator);
	}

	/**
	 * Removes all the transaction histories where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of transaction histories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching transaction histories
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public static TransactionHistory findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the transaction history where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the transaction history that was removed
	 */
	public static TransactionHistory removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of transaction histories where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching transaction histories
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching transaction histories
	 */
	public static List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public static TransactionHistory findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public static TransactionHistory findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the transaction histories before and after the current transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param transactionHistoryId the primary key of the current transaction history
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory[] findByUuid_C_PrevAndNext(
			long transactionHistoryId, String uuid, long companyId,
			OrderByComparator<TransactionHistory> orderByComparator)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			transactionHistoryId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the transaction histories where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching transaction histories
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public static TransactionHistory findByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByClassNameIdAndClassPK(
			classNameId, classPK);
	}

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByClassNameIdAndClassPK(
		long classNameId, long classPK) {

		return getPersistence().fetchByClassNameIdAndClassPK(
			classNameId, classPK);
	}

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public static TransactionHistory fetchByClassNameIdAndClassPK(
		long classNameId, long classPK, boolean useFinderCache) {

		return getPersistence().fetchByClassNameIdAndClassPK(
			classNameId, classPK, useFinderCache);
	}

	/**
	 * Removes the transaction history where classNameId = &#63; and classPK = &#63; from the database.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the transaction history that was removed
	 */
	public static TransactionHistory removeByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().removeByClassNameIdAndClassPK(
			classNameId, classPK);
	}

	/**
	 * Returns the number of transaction histories where classNameId = &#63; and classPK = &#63;.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the number of matching transaction histories
	 */
	public static int countByClassNameIdAndClassPK(
		long classNameId, long classPK) {

		return getPersistence().countByClassNameIdAndClassPK(
			classNameId, classPK);
	}

	/**
	 * Caches the transaction history in the entity cache if it is enabled.
	 *
	 * @param transactionHistory the transaction history
	 */
	public static void cacheResult(TransactionHistory transactionHistory) {
		getPersistence().cacheResult(transactionHistory);
	}

	/**
	 * Caches the transaction histories in the entity cache if it is enabled.
	 *
	 * @param transactionHistories the transaction histories
	 */
	public static void cacheResult(
		List<TransactionHistory> transactionHistories) {

		getPersistence().cacheResult(transactionHistories);
	}

	/**
	 * Creates a new transaction history with the primary key. Does not add the transaction history to the database.
	 *
	 * @param transactionHistoryId the primary key for the new transaction history
	 * @return the new transaction history
	 */
	public static TransactionHistory create(long transactionHistoryId) {
		return getPersistence().create(transactionHistoryId);
	}

	/**
	 * Removes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory remove(long transactionHistoryId)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().remove(transactionHistoryId);
	}

	public static TransactionHistory updateImpl(
		TransactionHistory transactionHistory) {

		return getPersistence().updateImpl(transactionHistory);
	}

	/**
	 * Returns the transaction history with the primary key or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory findByPrimaryKey(long transactionHistoryId)
		throws com.placecube.digitalplace.local.transactionhistory.exception.
			NoSuchTransactionHistoryException {

		return getPersistence().findByPrimaryKey(transactionHistoryId);
	}

	/**
	 * Returns the transaction history with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history, or <code>null</code> if a transaction history with the primary key could not be found
	 */
	public static TransactionHistory fetchByPrimaryKey(
		long transactionHistoryId) {

		return getPersistence().fetchByPrimaryKey(transactionHistoryId);
	}

	/**
	 * Returns all the transaction histories.
	 *
	 * @return the transaction histories
	 */
	public static List<TransactionHistory> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of transaction histories
	 */
	public static List<TransactionHistory> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of transaction histories
	 */
	public static List<TransactionHistory> findAll(
		int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of transaction histories
	 */
	public static List<TransactionHistory> findAll(
		int start, int end,
		OrderByComparator<TransactionHistory> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the transaction histories from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of transaction histories.
	 *
	 * @return the number of transaction histories
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static TransactionHistoryPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(
		TransactionHistoryPersistence persistence) {

		_persistence = persistence;
	}

	private static volatile TransactionHistoryPersistence _persistence;

}