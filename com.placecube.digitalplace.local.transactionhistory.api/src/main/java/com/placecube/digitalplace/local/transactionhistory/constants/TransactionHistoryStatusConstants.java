package com.placecube.digitalplace.local.transactionhistory.constants;

public class TransactionHistoryStatusConstants {

	public static final String STATUS_SUBMITTED = "submitted";

	private TransactionHistoryStatusConstants() {

	}

}
