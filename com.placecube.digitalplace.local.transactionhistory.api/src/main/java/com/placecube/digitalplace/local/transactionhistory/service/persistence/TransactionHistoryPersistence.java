/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.transactionhistory.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.service.persistence.change.tracking.CTPersistence;

import com.placecube.digitalplace.local.transactionhistory.exception.NoSuchTransactionHistoryException;
import com.placecube.digitalplace.local.transactionhistory.model.TransactionHistory;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the transaction history service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TransactionHistoryUtil
 * @generated
 */
@ProviderType
public interface TransactionHistoryPersistence
	extends BasePersistence<TransactionHistory>,
			CTPersistence<TransactionHistory> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TransactionHistoryUtil} to access the transaction history persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the transaction histories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid(String uuid);

	/**
	 * Returns a range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public TransactionHistory findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public TransactionHistory findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns the transaction histories before and after the current transaction history in the ordered set where uuid = &#63;.
	 *
	 * @param transactionHistoryId the primary key of the current transaction history
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public TransactionHistory[] findByUuid_PrevAndNext(
			long transactionHistoryId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator)
		throws NoSuchTransactionHistoryException;

	/**
	 * Removes all the transaction histories where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of transaction histories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching transaction histories
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public TransactionHistory findByUUID_G(String uuid, long groupId)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the transaction history where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the transaction history where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the transaction history that was removed
	 */
	public TransactionHistory removeByUUID_G(String uuid, long groupId)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the number of transaction histories where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching transaction histories
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns an ordered range of all the transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching transaction histories
	 */
	public java.util.List<TransactionHistory> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public TransactionHistory findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the first transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public TransactionHistory findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the last transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns the transaction histories before and after the current transaction history in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param transactionHistoryId the primary key of the current transaction history
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public TransactionHistory[] findByUuid_C_PrevAndNext(
			long transactionHistoryId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
				orderByComparator)
		throws NoSuchTransactionHistoryException;

	/**
	 * Removes all the transaction histories where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of transaction histories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching transaction histories
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the matching transaction history
	 * @throws NoSuchTransactionHistoryException if a matching transaction history could not be found
	 */
	public TransactionHistory findByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByClassNameIdAndClassPK(
		long classNameId, long classPK);

	/**
	 * Returns the transaction history where classNameId = &#63; and classPK = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching transaction history, or <code>null</code> if a matching transaction history could not be found
	 */
	public TransactionHistory fetchByClassNameIdAndClassPK(
		long classNameId, long classPK, boolean useFinderCache);

	/**
	 * Removes the transaction history where classNameId = &#63; and classPK = &#63; from the database.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the transaction history that was removed
	 */
	public TransactionHistory removeByClassNameIdAndClassPK(
			long classNameId, long classPK)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the number of transaction histories where classNameId = &#63; and classPK = &#63;.
	 *
	 * @param classNameId the class name ID
	 * @param classPK the class pk
	 * @return the number of matching transaction histories
	 */
	public int countByClassNameIdAndClassPK(long classNameId, long classPK);

	/**
	 * Caches the transaction history in the entity cache if it is enabled.
	 *
	 * @param transactionHistory the transaction history
	 */
	public void cacheResult(TransactionHistory transactionHistory);

	/**
	 * Caches the transaction histories in the entity cache if it is enabled.
	 *
	 * @param transactionHistories the transaction histories
	 */
	public void cacheResult(
		java.util.List<TransactionHistory> transactionHistories);

	/**
	 * Creates a new transaction history with the primary key. Does not add the transaction history to the database.
	 *
	 * @param transactionHistoryId the primary key for the new transaction history
	 * @return the new transaction history
	 */
	public TransactionHistory create(long transactionHistoryId);

	/**
	 * Removes the transaction history with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history that was removed
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public TransactionHistory remove(long transactionHistoryId)
		throws NoSuchTransactionHistoryException;

	public TransactionHistory updateImpl(TransactionHistory transactionHistory);

	/**
	 * Returns the transaction history with the primary key or throws a <code>NoSuchTransactionHistoryException</code> if it could not be found.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history
	 * @throws NoSuchTransactionHistoryException if a transaction history with the primary key could not be found
	 */
	public TransactionHistory findByPrimaryKey(long transactionHistoryId)
		throws NoSuchTransactionHistoryException;

	/**
	 * Returns the transaction history with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param transactionHistoryId the primary key of the transaction history
	 * @return the transaction history, or <code>null</code> if a transaction history with the primary key could not be found
	 */
	public TransactionHistory fetchByPrimaryKey(long transactionHistoryId);

	/**
	 * Returns all the transaction histories.
	 *
	 * @return the transaction histories
	 */
	public java.util.List<TransactionHistory> findAll();

	/**
	 * Returns a range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @return the range of transaction histories
	 */
	public java.util.List<TransactionHistory> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of transaction histories
	 */
	public java.util.List<TransactionHistory> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator);

	/**
	 * Returns an ordered range of all the transaction histories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>TransactionHistoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of transaction histories
	 * @param end the upper bound of the range of transaction histories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of transaction histories
	 */
	public java.util.List<TransactionHistory> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TransactionHistory>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the transaction histories from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of transaction histories.
	 *
	 * @return the number of transaction histories
	 */
	public int countAll();

}