package com.placecube.digitalplace.local.siteinitializer.customeraccount.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectConfigurationService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuEntry;
import com.placecube.initializer.service.NavigationMenuInitializer;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
public class DigitalPlaceCustomerAccountSiteInitializerServiceTest extends PowerMockito {

	private static final long ID = 1;

	@InjectMocks
	private DigitalPlaceCustomerAccountSiteInitializerService digitalPlaceCustomerAccountSiteInitializerService;

	@Mock
	private LoginRedirectConfigurationService mockLoginRedirectConfigurationService;

	@Mock
	private NavigationMenuInitializer mockNavigationMenuInitializer;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Before
	public void activateSetUp() {
		mockStatic(ServiceContextThreadLocal.class);
	}

	@Test(expected = ConfigurationException.class)
	public void configureLoginRedirect_WhenErrorConfiguringTheLoginRedirect_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockServiceContext.getCompanyId()).thenReturn(ID);
		doThrow(new ConfigurationException()).when(mockLoginRedirectConfigurationService).setNonAdminRedirectURL(ID, "/my-account");

		digitalPlaceCustomerAccountSiteInitializerService.configureLoginRedirect(mockServiceContext);
	}

	@Test
	public void configureLoginRedirect_WhenRedirectAlreadyConfigured_ThenDoesNotConfigureTheLoginRedirect() throws ConfigurationException {
		when(mockServiceContext.getCompanyId()).thenReturn(ID);
		when(mockLoginRedirectConfigurationService.getNonAdminRedirectURL(ID)).thenReturn("testURL");

		digitalPlaceCustomerAccountSiteInitializerService.configureLoginRedirect(mockServiceContext);

		verify(mockLoginRedirectConfigurationService, never()).setNonAdminRedirectURL(anyLong(), anyString());
	}

	@Test
	public void configureLoginRedirect_WhenRedirectNotYetConfigured_ThenConfiguresTheLoginRedirect() throws ConfigurationException {
		when(mockServiceContext.getCompanyId()).thenReturn(ID);

		digitalPlaceCustomerAccountSiteInitializerService.configureLoginRedirect(mockServiceContext);

		verify(mockLoginRedirectConfigurationService, times(1)).setNonAdminRedirectURL(ID, "/my-account");
	}

	@Test(expected = PortalException.class)
	public void configureSiteNavigationMenuEntries_WhenErrorConfiguringTheSiteNavigationMenuEntriesForMyAccountLinks_ThenThrowsPortalException() throws PortalException {
		doThrow(new PortalException()).when(mockNavigationMenuInitializer).configureSiteNavigationMenuEntries(mockServiceContext,
				DigitalPlaceCustomerAccountSiteInitializerService.class.getClassLoader(),
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/navmenuitems/my-account-links.json");

		digitalPlaceCustomerAccountSiteInitializerService.configureSiteNavigationMenuEntries(mockServiceContext);
	}

	@Test
	public void configureSiteNavigationMenuEntries_WhenNoError_ThenConfiguresSiteNavigationMenuEntriesForMyAccountLinks() throws PortalException {
		digitalPlaceCustomerAccountSiteInitializerService.configureSiteNavigationMenuEntries(mockServiceContext);

		verify(mockNavigationMenuInitializer, times(1)).configureSiteNavigationMenuEntries(mockServiceContext, DigitalPlaceCustomerAccountSiteInitializerService.class.getClassLoader(),
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/navmenuitems/my-account-links.json");
	}

	@Test(expected = PortalException.class)
	public void getPreferencesPlaceholders_WhenErrorRetrievingSiteNavigationMenu_ThenThrowsPortalException() throws PortalException {
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(), NavigationMenuEntry.MY_ACCOUNT_LINKS.getType()))
				.thenThrow(new PortalException());

		digitalPlaceCustomerAccountSiteInitializerService.getPreferencesPlaceholders(mockServiceContext);
	}

	@Test
	public void getPreferencesPlaceholders_WhenNoError_ThenReturnsMapWithPlaceholderValues() throws PortalException {
		long navigationMenuId = 123;
		long groupId = 2;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(), NavigationMenuEntry.MY_ACCOUNT_LINKS.getType()))
				.thenReturn(mockSiteNavigationMenu);
		when(mockSiteNavigationMenu.getSiteNavigationMenuId()).thenReturn(navigationMenuId);

		Map<String, String> results = digitalPlaceCustomerAccountSiteInitializerService.getPreferencesPlaceholders(mockServiceContext);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("${PLACEHOLDER_SITE_NAVIGATION_MENU_MY_ACCOUNT_LINKS}"), equalTo(String.valueOf(navigationMenuId)));
		assertThat(results.get("${PLACEHOLDER_GROUP_ID}"), equalTo(String.valueOf(groupId)));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenConfiguresServiceContextWithAddGroupPermissionsTrue() throws Exception {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);

		ServiceContext result =digitalPlaceCustomerAccountSiteInitializerService.getServiceContext();

		InOrder inOrder = inOrder(mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAddGroupPermissions(true);
		inOrder.verify(mockServiceContext, times(1)).setAddGuestPermissions(true);
		assertThat(result, sameInstance(mockServiceContext));
	}

	@Test(expected = PortalException.class)
	public void initializeSiteNavigationMenus_WhenErrorCreatingMyAccountLinks_ThenThrowsPortalException() throws Exception {
		when(mockNavigationMenuInitializer.getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(), NavigationMenuEntry.MY_ACCOUNT_LINKS.getType()))
				.thenThrow(new PortalException());

		digitalPlaceCustomerAccountSiteInitializerService.initializeSiteNavigationMenu(mockServiceContext);
	}

	@Test
	public  void initializeSiteNavigationMenus_WhenNoError_ThenCreatesSiteNavigationMenu() throws PortalException {
		digitalPlaceCustomerAccountSiteInitializerService.initializeSiteNavigationMenu(mockServiceContext);

		InOrder inOrder = inOrder(mockNavigationMenuInitializer);
		inOrder.verify(mockNavigationMenuInitializer, times(1)).getOrCreateSiteNavigationMenu(mockServiceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(),
				NavigationMenuEntry.MY_ACCOUNT_LINKS.getType());
	}
}