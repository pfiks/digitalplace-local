package com.placecube.digitalplace.local.siteinitializer.customeraccount.initializer;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.siteinitializer.customeraccount.constants.DigitalPlaceCustomerAccountSiteInitializerConstants;
import com.placecube.digitalplace.local.siteinitializer.customeraccount.service.DigitalPlaceCustomerAccountSiteInitializerService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, CompanyThreadLocal.class })
public class DigitalPlaceCustomerAccountSiteInitializerTest extends PowerMockito {

	@InjectMocks
	private DigitalPlaceCustomerAccountSiteInitializer digitalPlaceCustomerAccountSiteInitializer;

	@Mock
	private DigitalPlaceCustomerAccountSiteInitializerService mockDigitalPlaceCustomerAccountSiteInitializerService;

	@Mock
	private DigitalPlaceInitializerService mockDigitalPlaceInitializerService;

	@Mock
	private Map<String, String> mockPrefsPlaceholder;

	@Mock
	private NavItemTemplateService mockNavItemTemplateService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private Map<String, String> mockStringMap;

	@Before
	public void activateSetUp() {
		mockStatic(PortalUtil.class, CompanyThreadLocal.class);
	}

	@Test
	public void getDescriptionWithAnyLocale_WhenNoError_ThenReturnsEmptyString() {
		String result = digitalPlaceCustomerAccountSiteInitializer.getDescription(Locale.CANADA_FRENCH);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getKey_WhenNoError_ThenReturnsSiteInitializerKey() {
		String result = digitalPlaceCustomerAccountSiteInitializer.getKey();

		assertThat(result, equalTo(DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_KEY));
	}

	@Test
	public void getNameWithAnyLocale_WhenNoError_ThenReturnsSiteInitializerName() {
		String result = digitalPlaceCustomerAccountSiteInitializer.getName(Locale.GERMAN);

		assertThat(result, equalTo(DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_NAME));
	}

	@Test
	public void getThumbnailSrc_WhenNoError_ThenReturnsTheThumbnailPath() {
		String contextPath = "contextPathValue";
		when(mockServletContext.getContextPath()).thenReturn(contextPath);
		String expected = contextPath + DigitalPlaceCustomerAccountSiteInitializerConstants.THUMBNAIL_PATH;

		String result = digitalPlaceCustomerAccountSiteInitializer.getThumbnailSrc();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void initialize_WhenNoError_ThenInitializesTheResources() throws Exception {
		long groupId = 123;
		long COMPANY_ID = 3434L;
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDigitalPlaceCustomerAccountSiteInitializerService.getServiceContext()).thenReturn(mockServiceContext);
		when(mockDigitalPlaceCustomerAccountSiteInitializerService.getPreferencesPlaceholders(mockServiceContext)).thenReturn(mockPrefsPlaceholder);
		when(mockDigitalPlaceCustomerAccountSiteInitializerService.getGlobalContentPreferencesPlaceholders(COMPANY_ID)).thenReturn(mockStringMap);

		digitalPlaceCustomerAccountSiteInitializer.initialize(groupId);

		InOrder inOrder = inOrder(mockDigitalPlaceCustomerAccountSiteInitializerService, mockDigitalPlaceInitializerService, mockNavItemTemplateService);

		inOrder.verify(mockNavItemTemplateService, times(1)).getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.MY_ACCOUNT_CARDS, mockServiceContext);

		inOrder.verify(mockDigitalPlaceInitializerService, times(1)).initializeContentPage(DigitalPlaceCustomerAccountSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/my-account.json", mockPrefsPlaceholder, mockServiceContext);

		inOrder.verify(mockDigitalPlaceInitializerService, times(1)).initializeContentPage(DigitalPlaceCustomerAccountSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/my-transactions.json", mockPrefsPlaceholder, mockServiceContext);

		inOrder.verify(mockDigitalPlaceInitializerService, times(1)).initializeContentPage(DigitalPlaceCustomerAccountSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/my-details.json", mockPrefsPlaceholder, mockServiceContext);

		inOrder.verify(mockDigitalPlaceInitializerService, times(1)).initializeWidgetPage(DigitalPlaceCustomerAccountSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/create-account.json", mockServiceContext);

		inOrder.verify(mockDigitalPlaceInitializerService, times(1)).initializeWidgetPage(DigitalPlaceCustomerAccountSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/sign-in.json", mockStringMap, mockServiceContext);

		inOrder.verify(mockDigitalPlaceCustomerAccountSiteInitializerService, times(1)).configureSiteNavigationMenuEntries(mockServiceContext);

		inOrder.verify(mockDigitalPlaceCustomerAccountSiteInitializerService, times(1)).configureLoginRedirect(mockServiceContext);

		inOrder.verify(mockDigitalPlaceCustomerAccountSiteInitializerService, times(1)).initializeSiteNavigationMenu(mockServiceContext);

		inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void isActive_WhenNoError_ThenReturnsTrue() {
		long companyId = 123;

		boolean result = digitalPlaceCustomerAccountSiteInitializer.isActive(companyId);

		assertThat(result, equalTo(true));
	}
}
