package com.placecube.digitalplace.local.siteinitializer.customeraccount.constants;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPrivate;
import static java.lang.reflect.Modifier.isStatic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;

public class DigitalPlaceCustomerAccountSiteInitializerConstantsTest {

	@Test
	public void testClassIsAConstantsClass() throws Exception {
		Class<?> classToTest = DigitalPlaceCustomerAccountSiteInitializerConstants.class;

		assertThat(isFinal(classToTest.getModifiers()), equalTo(true));

		assertThat(classToTest.getDeclaredConstructors().length, equalTo(1));
		Constructor<?> constructor = classToTest.getDeclaredConstructor();
		assertThat(isPrivate(constructor.getModifiers()), equalTo(true));
		assertThat(constructor.isAccessible(), equalTo(false));

		constructor.setAccessible(true);
		constructor.newInstance();
		constructor.setAccessible(false);

		for (Method method : classToTest.getMethods()) {
			if (method.isAccessible()) {
				assertThat(isStatic(method.getModifiers()), equalTo(true));
				assertThat(method.getDeclaringClass(), equalTo(classToTest));
			}
		}
	}

	@Test
	public void testConstantValueForBundleName() {
		assertThat(DigitalPlaceCustomerAccountSiteInitializerConstants.BUNDLE_NAME, equalTo("com.placecube.digitalplace.local.siteinitializer.customeraccount"));
	}

	@Test
	public void testConstantValueForSiteInitializerKey() {
		assertThat(DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_KEY, equalTo("digital-place-customer-account-site-initializer"));
	}

	@Test
	public void testConstantValueForSiteInitializerName() {
		assertThat(DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_NAME, equalTo("Digital Place Customer Account"));
	}

	@Test
	public void testConstantValueForThumbnailPath() {
		assertThat(DigitalPlaceCustomerAccountSiteInitializerConstants.THUMBNAIL_PATH, equalTo("/images/thumbnail.png"));
	}
}
