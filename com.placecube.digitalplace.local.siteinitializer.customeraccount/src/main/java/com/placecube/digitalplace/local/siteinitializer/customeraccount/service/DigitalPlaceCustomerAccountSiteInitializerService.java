package com.placecube.digitalplace.local.siteinitializer.customeraccount.service;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.placecube.digitalplace.local.loginredirect.configuration.LoginRedirectConfigurationService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuEntry;
import com.placecube.digitalplace.local.siteinitializer.customeraccount.constants.DigitalPlaceCustomerAccountSiteInitializerConstants;
import com.placecube.initializer.service.NavigationMenuInitializer;

@Component(immediate = true, service = DigitalPlaceCustomerAccountSiteInitializerService.class)
public class DigitalPlaceCustomerAccountSiteInitializerService {

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LoginRedirectConfigurationService loginRedirectConfigurationService;

	@Reference
	private NavigationMenuInitializer navigationMenuInitializer;

	public void configureLoginRedirect(ServiceContext serviceContext) throws ConfigurationException {
		long companyId = serviceContext.getCompanyId();
		if (Validator.isNull(loginRedirectConfigurationService.getNonAdminRedirectURL(companyId))) {
			loginRedirectConfigurationService.setNonAdminRedirectURL(companyId, "/my-account");
		}
	}

	public void configureSiteNavigationMenuEntries(ServiceContext serviceContext) throws PortalException {
		navigationMenuInitializer.configureSiteNavigationMenuEntries(serviceContext, getClass().getClassLoader(),
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/navmenuitems/my-account-links.json");
	}

	public Map<String, String> getGlobalContentPreferencesPlaceholders(long companyId) throws PortalException {
		Group companyGroup = groupLocalService.getCompanyGroup(companyId);
		Map<String, String> prefsPlaceholders = new HashMap<>();
		prefsPlaceholders.put(DigitalPlaceCustomerAccountSiteInitializerConstants.GLOBAL_GROUP_ID_PLACEHOLDER, String.valueOf(companyGroup.getGroupId()));
		return prefsPlaceholders;
	}

	public Map<String, String> getPreferencesPlaceholders(ServiceContext serviceContext) throws PortalException {
		Map<String, String> results = new HashMap<>();
		SiteNavigationMenu siteNavigationMenuMyAccountLinks = navigationMenuInitializer.getOrCreateSiteNavigationMenu(serviceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(),
				NavigationMenuEntry.MY_ACCOUNT_LINKS.getType());
		results.put("${PLACEHOLDER_SITE_NAVIGATION_MENU_MY_ACCOUNT_LINKS}", String.valueOf(siteNavigationMenuMyAccountLinks.getSiteNavigationMenuId()));
		results.put("${PLACEHOLDER_GROUP_ID}", String.valueOf(serviceContext.getScopeGroupId()));
		return results;
	}

	public ServiceContext getServiceContext() {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		return serviceContext;
	}

	public void initializeSiteNavigationMenu(ServiceContext serviceContext) throws PortalException {
		navigationMenuInitializer.getOrCreateSiteNavigationMenu(serviceContext, NavigationMenuEntry.MY_ACCOUNT_LINKS.getName(), NavigationMenuEntry.MY_ACCOUNT_LINKS.getType());
	}
}