package com.placecube.digitalplace.local.siteinitializer.customeraccount.initializer;

import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.site.exception.InitializationException;
import com.liferay.site.initializer.SiteInitializer;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.local.navigationmenu.constants.NavigationMenuWidgetTemplate;
import com.placecube.digitalplace.local.navigationmenu.service.NavItemTemplateService;
import com.placecube.digitalplace.local.siteinitializer.customeraccount.constants.DigitalPlaceCustomerAccountSiteInitializerConstants;
import com.placecube.digitalplace.local.siteinitializer.customeraccount.service.DigitalPlaceCustomerAccountSiteInitializerService;

@Component(immediate = true, property = "site.initializer.key=" + DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_KEY, service = SiteInitializer.class)
public class DigitalPlaceCustomerAccountSiteInitializer implements SiteInitializer {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceCustomerAccountSiteInitializer.class);

	@Reference
	private DigitalPlaceCustomerAccountSiteInitializerService digitalPlaceCustomerAccountSiteInitializerService;

	@Reference
	private DigitalPlaceInitializerService digitalPlaceInitializerService;

	@Reference
	private NavItemTemplateService navItemTemplateService;

	@Reference(target = "(osgi.web.symbolicname=" + DigitalPlaceCustomerAccountSiteInitializerConstants.BUNDLE_NAME + ")")
	private ServletContext servletContext;

	@Override
	public String getDescription(Locale locale) {
		return StringPool.BLANK;
	}

	@Override
	public String getKey() {
		return DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_KEY;
	}

	@Override
	public String getName(Locale locale) {
		return DigitalPlaceCustomerAccountSiteInitializerConstants.CUSTOMER_ACCOUNT_SITE_INITIALIZER_NAME;
	}

	@Override
	public String getThumbnailSrc() {
		return servletContext.getContextPath() + DigitalPlaceCustomerAccountSiteInitializerConstants.THUMBNAIL_PATH;
	}

	@Override
	public void initialize(long groupId) throws InitializationException {

		LOG.info("Running Digital Place customer account site initializer for groupId: " + groupId);

		try {
			ServiceContext serviceContext = digitalPlaceCustomerAccountSiteInitializerService.getServiceContext();

			Map<String, String> prefsPlaceholders = digitalPlaceCustomerAccountSiteInitializerService.getPreferencesPlaceholders(serviceContext);

			navItemTemplateService.getOrCreateWidgetTemplate(NavigationMenuWidgetTemplate.MY_ACCOUNT_CARDS, serviceContext);

			createContentPage("my-account", serviceContext, prefsPlaceholders);
			createContentPage("my-transactions", serviceContext, prefsPlaceholders);
			createContentPage("my-details", serviceContext, prefsPlaceholders);

			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceCustomerAccountSiteInitializer.class,
					"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/create-account.json", serviceContext);

			Map<String, String> globalContentPrefsPlaceholders = digitalPlaceCustomerAccountSiteInitializerService.getGlobalContentPreferencesPlaceholders(CompanyThreadLocal.getCompanyId());

			digitalPlaceInitializerService.initializeWidgetPage(DigitalPlaceCustomerAccountSiteInitializer.class,
					"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/sign-in.json", globalContentPrefsPlaceholders, serviceContext);

			digitalPlaceCustomerAccountSiteInitializerService.configureSiteNavigationMenuEntries(serviceContext);

			digitalPlaceCustomerAccountSiteInitializerService.configureLoginRedirect(serviceContext);

			digitalPlaceCustomerAccountSiteInitializerService.initializeSiteNavigationMenu(serviceContext);
		} catch (PortalException e) {
			throw new InitializationException(e);
		}

		LOG.info("Digital Place customer account site initialization completed.");
	}

	@Override
	public boolean isActive(long companyId) {
		return true;
	}

	private void createContentPage(String pageName, ServiceContext serviceContext, Map<String, String> prefsPlaceholders) throws PortalException {
		digitalPlaceInitializerService.initializeContentPage(DigitalPlaceCustomerAccountSiteInitializer.class,
				"com/placecube/digitalplace/local/siteinitializer/customeraccount/dependencies/layouts/" + pageName + ".json", prefsPlaceholders, serviceContext);
	}
}