package com.placecube.digitalplace.local.siteinitializer.customeraccount.constants;

public final class DigitalPlaceCustomerAccountSiteInitializerConstants {

	public static final String BUNDLE_NAME = "com.placecube.digitalplace.local.siteinitializer.customeraccount";

	public static final String CUSTOMER_ACCOUNT_SITE_INITIALIZER_KEY = "digital-place-customer-account-site-initializer";

	public static final String CUSTOMER_ACCOUNT_SITE_INITIALIZER_NAME = "Digital Place Customer Account";

	public static final String GLOBAL_GROUP_ID_PLACEHOLDER = "${PLACEHOLDER_GLOBAL_GROUP_ID}";

	public static final String THUMBNAIL_PATH = "/images/thumbnail.png";

	private DigitalPlaceCustomerAccountSiteInitializerConstants() {
		return;
	}
}
