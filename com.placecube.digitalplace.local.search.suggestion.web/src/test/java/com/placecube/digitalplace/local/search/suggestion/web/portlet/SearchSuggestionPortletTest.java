package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import static org.mockito.Mockito.times;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.suggest.SuggesterResults;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;
import com.placecube.digitalplace.local.search.suggestion.web.service.JSONBuilderService;
import com.placecube.digitalplace.local.search.suggestion.web.service.QuerySuggestionService;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionConfigurationService;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionService;
import com.placecube.digitalplace.local.search.suggestion.web.service.TermSuggestionService;
import com.placecube.digitalplace.local.search.suggestion.web.settings.SearchSuggestionsSettingsService;
import com.placecube.digitalplace.local.search.suggestion.web.settings.model.SearchSuggestionSettings;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ParamUtil.class, PropsUtil.class, PortalUtil.class, PortletResponseUtil.class, SearchContextFactory.class })
public class SearchSuggestionPortletTest extends PowerMockito {

	private static final String KEYWORDS = "keywords";

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONBuilderService mockJsonBuilderService;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private Portal mockPortal;

	@Mock
	private QuerySuggestionService mockQuerySuggestionService;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchSuggestionCompanyConfiguration mockSearchSuggestionCompanyConfiguration;

	@Mock
	private SearchSuggestionConfigurationService mockSearchSuggestionConfigurationService;

	@Mock
	private SearchSuggestionService mockSearchSuggestionService;

	@Mock
	private SearchSuggestionSettings mockSearchSuggestionSettings;

	@Mock
	private SearchSuggestionsSettingsService mockSearchSuggestionsSettingsService;

	@Mock
	private SuggesterResults mockSuggesterResults;

	@Mock
	private List<String> mockSuggestions;

	@Mock
	private TermSuggestionService mockTermSuggestionService;

	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchSuggestionPortlet searchSuggestionPortlet;

	@Before
	public void activeSetUp() {

		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, PortletResponseUtil.class, SearchContextFactory.class);
	}

	@Test(expected = PortletException.class)
	public void serveResource_WhenErrorGettingQuerySuggestions_ThenPortalExceptionIsThrwon() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		when(mockSearchSuggestionsSettingsService.getSuggestionsSetting(mockThemeDisplay)).thenReturn(mockSearchSuggestionSettings);
		when(mockSearchSuggestionSettings.isSearchSuggestionsEnabled()).thenReturn(true);
		when(mockSearchSuggestionSettings.isQuerySuggestionsEnabled()).thenReturn(true);

		when(ParamUtil.getString(mockResourceRequest, "keywords")).thenReturn(KEYWORDS);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);

		when(mockQuerySuggestionService.getSuggestions(mockSearchContext)).thenThrow(SearchException.class);

		searchSuggestionPortlet.serveResource(mockResourceRequest, mockResourceResponse);

	}

	@Test(expected = PortletException.class)
	public void serveResource_WhenErrorGettingSuggestions_ThenPortletExceptionIsThrown() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		when(mockSearchSuggestionsSettingsService.getSuggestionsSetting(mockThemeDisplay)).thenReturn(mockSearchSuggestionSettings);
		when(mockSearchSuggestionSettings.isSearchSuggestionsEnabled()).thenReturn(true);
		when(mockSearchSuggestionSettings.isQuerySuggestionsEnabled()).thenReturn(false);
		when(ParamUtil.getString(mockResourceRequest, "keywords")).thenReturn(KEYWORDS);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		long companyId = 1;
		when(mockPortal.getCompanyId(mockResourceRequest)).thenReturn(companyId);
		when(mockSearchSuggestionConfigurationService.getConfiguration(companyId)).thenReturn(Optional.of(mockSearchSuggestionCompanyConfiguration));
		when(mockSearchSuggestionService.getSuggesterResults(mockSearchContext, mockSearchSuggestionCompanyConfiguration)).thenThrow(SearchException.class);

		searchSuggestionPortlet.serveResource(mockResourceRequest, mockResourceResponse);

	}

	@Test
	public void serveResource_WhenQuerySuggestionsIsEnabled_ThenWritesJSONResponse() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		when(mockSearchSuggestionsSettingsService.getSuggestionsSetting(mockThemeDisplay)).thenReturn(mockSearchSuggestionSettings);
		when(mockSearchSuggestionSettings.isSearchSuggestionsEnabled()).thenReturn(true);
		when(mockSearchSuggestionSettings.isQuerySuggestionsEnabled()).thenReturn(true);

		when(ParamUtil.getString(mockResourceRequest, "keywords")).thenReturn(KEYWORDS);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);

		when(mockQuerySuggestionService.getSuggestions(mockSearchContext)).thenReturn(mockSuggestions);
		when(mockJsonBuilderService.buildJSONResponse(mockSuggestions)).thenReturn(mockJSONArray);

		searchSuggestionPortlet.serveResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(PortletResponseUtil.class, times(1));
		PortletResponseUtil.write(mockResourceResponse, mockJSONArray.toString());

	}

	@Test
	public void serveResource_WhenSearchSuggestionsIsEnabled_ThenWritesJSONResponse() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);

		when(mockSearchSuggestionsSettingsService.getSuggestionsSetting(mockThemeDisplay)).thenReturn(mockSearchSuggestionSettings);
		when(mockSearchSuggestionSettings.isSearchSuggestionsEnabled()).thenReturn(true);
		when(mockSearchSuggestionSettings.isQuerySuggestionsEnabled()).thenReturn(false);

		when(ParamUtil.getString(mockResourceRequest, "keywords")).thenReturn(KEYWORDS);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);

		long companyId = 1;
		when(mockPortal.getCompanyId(mockResourceRequest)).thenReturn(companyId);
		when(mockSearchSuggestionConfigurationService.getConfiguration(companyId)).thenReturn(Optional.of(mockSearchSuggestionCompanyConfiguration));

		when(mockSearchSuggestionService.getSuggesterResults(mockSearchContext, mockSearchSuggestionCompanyConfiguration)).thenReturn(mockSuggesterResults);

		when(mockTermSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration)).thenReturn(mockSuggestions);
		when(mockJsonBuilderService.buildJSONResponse(mockSuggestions)).thenReturn(mockJSONArray);

		searchSuggestionPortlet.serveResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(PortletResponseUtil.class, times(1));
		PortletResponseUtil.write(mockResourceResponse, mockJSONArray.toString());

	}

	@Test
	public void serveResource_WhenSearchSuggestionsIsNotEnabled_ThenWritesEmptyJSONResponse() throws Exception {

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockSearchSuggestionsSettingsService.getSuggestionsSetting(mockThemeDisplay)).thenReturn(mockSearchSuggestionSettings);
		when(mockSearchSuggestionSettings.isSearchSuggestionsEnabled()).thenReturn(false);

		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);

		searchSuggestionPortlet.serveResource(mockResourceRequest, mockResourceResponse);

		verifyZeroInteractions(mockSearchSuggestionService, mockTermSuggestionService, mockJsonBuilderService);
		verifyStatic(PortletResponseUtil.class, times(1));
		PortletResponseUtil.write(mockResourceResponse, mockJSONArray.toString());

	}
}
