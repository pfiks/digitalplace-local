package com.placecube.digitalplace.local.search.suggestion.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JSONFactoryUtil.class })
public class JSONBuilderServiceTest {

	private JSONBuilderService jsonBuilderService;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJsonTerm;

	List<String> suggestions = new ArrayList<>();

	@Before
	public void activeSetup() {

		mockStatic(JSONFactoryUtil.class);
		jsonBuilderService = new JSONBuilderService();
	}

	@Test
	public void buildJSONResponse_WhereNoError_ThenReturnsResponseAsJSONArray() {

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		String suggestion = "suggestion";
		suggestions.add(suggestion);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJsonTerm);

		JSONArray result = jsonBuilderService.buildJSONResponse(suggestions);

		assertThat(result, sameInstance(mockJSONArray));

	}

	@Test
	public void buildJSONResponse_WhereThereIsNotSuggestion_ThenNoJsonObjectIsCreated() {

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		jsonBuilderService.buildJSONResponse(suggestions);

		verifyStatic(JSONFactoryUtil.class, never());
		JSONFactoryUtil.createJSONObject();

	}

	@Test
	public void buildJSONResponse_WhereThereIsSuggestion_ThenJsonObjectIsCreated() {

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		String suggestion = "suggestion";
		suggestions.add(suggestion);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJsonTerm);

		jsonBuilderService.buildJSONResponse(suggestions);

		verify(mockJsonTerm, times(1)).put("term", suggestion);

	}

}
