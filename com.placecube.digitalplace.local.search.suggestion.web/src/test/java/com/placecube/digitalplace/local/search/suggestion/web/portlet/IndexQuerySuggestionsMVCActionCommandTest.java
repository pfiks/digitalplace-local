package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.suggest.SpellCheckIndexWriter;
import com.liferay.portal.kernel.search.suggest.SuggestionConstants;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.search.suggestion.web.constants.PortletKeys;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionObjectFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, StringUtil.class, SessionMessages.class, SessionErrors.class, SearchContextFactory.class })
public class IndexQuerySuggestionsMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private IndexQuerySuggestionsMVCActionCommand indexQuerySuggestionsMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private File mockFile;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private Portal mockPortal;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchSuggestionObjectFactory mockSearchSuggestionObjectFactory;

	@Mock
	private SpellCheckIndexWriter mockSpellCheckIndexWriter;

	@Mock
	private UploadPortletRequest mockUploadPortletRequest;

	@Before
	public void activeSetup() {
		mockStatic(PortalUtil.class, StringUtil.class, SessionMessages.class, SessionErrors.class, SearchContextFactory.class);
	}

	@Test
	public void doProcessAction_WhenErrorGettingInputStream_ThenHideDefaultErrorMessageAndAddSessionError() throws Exception {

		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("importFile")).thenReturn(mockFile);
		when(mockSearchSuggestionObjectFactory.getFileInputStream(mockFile)).thenThrow(FileNotFoundException.class);

		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(PortletKeys.SEARCH_SUGGESTION_PORTLET);

		indexQuerySuggestionsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionMessages.class);
		SessionMessages.add(mockActionRequest, PortletKeys.SEARCH_SUGGESTION_PORTLET + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		verifyStatic(SessionErrors.class);
		SessionErrors.add(mockActionRequest, FileNotFoundException.class);

	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingLinesFromInputStream_ThenExceptionIsThrown() throws Exception {

		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("importFile")).thenReturn(mockFile);
		when(mockSearchSuggestionObjectFactory.getFileInputStream(mockFile)).thenReturn(mockInputStream);
		when(mockSearchSuggestionObjectFactory.getLinesFromStream(mockInputStream)).thenThrow(IOException.class);

		indexQuerySuggestionsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test
	public void doProcessAction_WhenErrorIndexingKeyword_ThenNoExceptionIsThrown() throws Exception {

		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("importFile")).thenReturn(mockFile);
		when(mockSearchSuggestionObjectFactory.getFileInputStream(mockFile)).thenReturn(mockInputStream);

		Collection<String> lines = Arrays.asList("line1");
		when(mockSearchSuggestionObjectFactory.getLinesFromStream(mockInputStream)).thenReturn(lines);
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		doThrow(new SearchException()).when(mockSpellCheckIndexWriter).indexKeyword(mockSearchContext, 0.0F, SuggestionConstants.TYPE_QUERY_SUGGESTION);

		try {
			indexQuerySuggestionsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	public void doProcessAction_WhenNoError_ThenLineIsIndexed() throws Exception {

		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("importFile")).thenReturn(mockFile);
		when(mockSearchSuggestionObjectFactory.getFileInputStream(mockFile)).thenReturn(mockInputStream);

		Collection<String> lines = Arrays.asList("line1");
		when(mockSearchSuggestionObjectFactory.getLinesFromStream(mockInputStream)).thenReturn(lines);
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockSearchContext.getGroupIds()).thenReturn(new long[] { 20124 });

		indexQuerySuggestionsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSearchContext, mockSpellCheckIndexWriter);
		inOrder.verify(mockSearchContext, times(1)).setKeywords("line1");
		inOrder.verify(mockSpellCheckIndexWriter, times(1)).indexKeyword(mockSearchContext, 0.0f, SuggestionConstants.TYPE_QUERY_SUGGESTION);

	}

	@Test
	public void doProcessAction_WhenThereIsNoLinesInFile_ThenNothingIsIndexed() throws Exception {

		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("importFile")).thenReturn(mockFile);
		when(mockSearchSuggestionObjectFactory.getFileInputStream(mockFile)).thenReturn(mockInputStream);
		when(mockSearchSuggestionObjectFactory.getLinesFromStream(mockInputStream)).thenReturn(Collections.emptyList());

		indexQuerySuggestionsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockSpellCheckIndexWriter);
	}
}
