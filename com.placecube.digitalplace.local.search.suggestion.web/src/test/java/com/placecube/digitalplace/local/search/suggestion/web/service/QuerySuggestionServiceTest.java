package com.placecube.digitalplace.local.search.suggestion.web.service;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexSearcher;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.search.highlight.HighlightUtil;
import com.liferay.portal.kernel.util.Localization;
import com.liferay.portal.kernel.util.LocalizationUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HighlightUtil.class, LocalizationUtil.class, QuerySuggestionService.class })
public class QuerySuggestionServiceTest extends PowerMockito {

	@Mock
	public IndexSearcher mokcIndexSearcher;

	@Mock
	private Document mockDocument;

	@Mock
	private Hits mockHits;

	@Mock
	private Localization mockLocalization;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private StringQuery mockStringQuery;

	@InjectMocks
	private QuerySuggestionService querySuggestionService;

	@Before
	public void activeSetup() {
		mockStatic(LocalizationUtil.class, HighlightUtil.class);
	}

	@Test
	public void getSuggestions_WhenDocsIsNotEmpty_ThenReturnsListOfSuggestions() throws Exception {

		String languageId = "en_GB";
		String localisedKeywordSearchField = QuerySuggestionService.KEYWORD_SEARCH_FIELD + "_" + languageId;
		String keywords = "school";
		long[] groupIds = new long[] { 20124 };
		String stringQuery = String.format(QuerySuggestionService.QUERY, groupIds[0], localisedKeywordSearchField, keywords);
		String value = "school admission";

		when(LocalizationUtil.getLocalization()).thenReturn(mockLocalization);
		when(mockSearchContext.getLanguageId()).thenReturn(languageId);
		when(mockLocalization.getLocalizedName(QuerySuggestionService.KEYWORD_SEARCH_FIELD, languageId)).thenReturn(localisedKeywordSearchField);
		when(mockSearchContext.getKeywords()).thenReturn(keywords);
		when(mockSearchContext.getGroupIds()).thenReturn(groupIds);
		whenNew(StringQuery.class).withArguments(stringQuery).thenReturn(mockStringQuery);
		when(mokcIndexSearcher.search(mockSearchContext, mockStringQuery)).thenReturn(mockHits);

		Document[] documents = new Document[] { mockDocument };
		when(mockDocument.get(localisedKeywordSearchField)).thenReturn(value);
		when(HighlightUtil.highlight(value, new String[] { keywords })).thenReturn(value);
		when(mockHits.getDocs()).thenReturn(documents);

		List<String> result = querySuggestionService.getSuggestions(mockSearchContext);

		assertThat(result, contains(value));

	}

}
