package com.placecube.digitalplace.local.search.suggestion.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;

@RunWith(PowerMockRunner.class)
public class SearchConfigurationServiceTest {

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private SearchSuggestionCompanyConfiguration mockSearchCompanyConfiguration;

	@InjectMocks
	private SearchSuggestionConfigurationService searchSuggestionConfigurationService;

	@Test
	public void getConfiguredEnabled_WhenErrorGettingConfiguration_ThenReturnsEmptyOptional() throws ConfigurationException {
		final long companyId = 342L;

		when(mockConfigurationProvider.getCompanyConfiguration(SearchSuggestionCompanyConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		Optional<SearchSuggestionCompanyConfiguration> result = searchSuggestionConfigurationService.getConfiguration(companyId);

		assertFalse(result.isPresent());
	}

	@Test
	public void getConfiguredEnabled_WhenNoError_ThenReturnsOptional() throws ConfigurationException {
		final long companyId = 342L;

		when(mockConfigurationProvider.getCompanyConfiguration(SearchSuggestionCompanyConfiguration.class, companyId)).thenReturn(mockSearchCompanyConfiguration);

		Optional<SearchSuggestionCompanyConfiguration> result = searchSuggestionConfigurationService.getConfiguration(companyId);

		assertThat(result.get(), sameInstance(mockSearchCompanyConfiguration));
	}
}
