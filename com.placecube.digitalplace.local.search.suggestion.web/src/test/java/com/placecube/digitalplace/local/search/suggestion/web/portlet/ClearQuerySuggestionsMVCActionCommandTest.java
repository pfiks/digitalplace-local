package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.search.engine.adapter.SearchEngineAdapter;
import com.liferay.portal.search.engine.adapter.document.DeleteByQueryDocumentRequest;
import com.liferay.portal.search.index.IndexNameBuilder;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionObjectFactory;

public class ClearQuerySuggestionsMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private ClearQuerySuggestionsMVCActionCommand clearQuerySuggestionsMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private DeleteByQueryDocumentRequest mockDeleteByQueryDocumentRequest;

	@Mock
	private IndexNameBuilder mockIndexNameBuilder;

	@Mock
	private Portal mockPortal;

	@Mock
	private SearchEngineAdapter mockSearchEngineAdapter;

	@Mock
	private SearchSuggestionObjectFactory mockSearchSuggestionObjectFactory;

	@Mock
	private StringQuery mockStringQuery;

	@Before
	public void activeSetUp() {
		initMocks(this);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenExecutesDeleteByQueryDocumentRequest() throws Exception {

		long companyId = 1;
		long groupId = 2;
		String indexName = "indexName";
		String query = String.format("(+groupId:%s AND +type:querySuggestion)", groupId);

		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockPortal.getScopeGroupId(mockActionRequest)).thenReturn(groupId);
		when(mockIndexNameBuilder.getIndexName(companyId)).thenReturn(indexName);
		when(mockSearchSuggestionObjectFactory.buildStringQuery(query)).thenReturn(mockStringQuery);
		when(mockSearchSuggestionObjectFactory.buidDeleteByQueryDocumentRequest(mockStringQuery, indexName)).thenReturn(mockDeleteByQueryDocumentRequest);

		clearQuerySuggestionsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockDeleteByQueryDocumentRequest, mockSearchEngineAdapter);
		inOrder.verify(mockDeleteByQueryDocumentRequest, times(1)).setRefresh(true);
		inOrder.verify(mockSearchEngineAdapter, times(1)).execute(mockDeleteByQueryDocumentRequest);
	}
}
