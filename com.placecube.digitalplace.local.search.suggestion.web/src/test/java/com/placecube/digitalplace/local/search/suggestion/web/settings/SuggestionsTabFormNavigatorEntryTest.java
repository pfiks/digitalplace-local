package com.placecube.digitalplace.local.search.suggestion.web.settings;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil;
import com.liferay.portal.kernel.servlet.taglib.ui.BaseJSPFormNavigatorEntry;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.search.suggestion.web.constants.SearchSuggestionsConstants;
import com.placecube.digitalplace.local.search.suggestion.web.settings.constants.SearchSuggestionsRequestKeys;
import com.placecube.digitalplace.local.search.suggestion.web.settings.model.SearchSuggestionSettings;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BaseJSPFormNavigatorEntry.class, ResourceBundleLoaderUtil.class, ResourceBundleUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil", "com.liferay.portal.kernel.util.ResourceBundleUtil" })
public class SuggestionsTabFormNavigatorEntryTest extends PowerMockito {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Language mockLanguage;

	@Mock
	private ResourceBundle mockResourceBundle;

	@Mock
	private SearchSuggestionSettings mockSearchSuggestionsSettings;

	@Mock
	private SearchSuggestionsSettingsService mockSearchSuggestionsSettingsService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SuggestionsTabFormNavigatorEntry suggestionsTabFormNavigatorEntry;

	@Before
	public void activeSetUp() {

		mockStatic(ResourceBundleLoaderUtil.class, ResourceBundleUtil.class);
	}

	@Test
	public void getCategoryKey_WhenNoError_ThenReturnsCategoryKey() {
		assertThat(suggestionsTabFormNavigatorEntry.getCategoryKey(), equalTo(SearchFormNavigatorCategoryKeys.SEARCH));
	}

	@Test
	public void getFormNavigatorId_WhenNoError_ThenReturnsFormNavigatorId() {

		assertThat(suggestionsTabFormNavigatorEntry.getFormNavigatorId(), equalTo(FormNavigatorConstants.FORM_NAVIGATOR_ID_SITES));
	}

	@Test
	public void getKey_WhenNoError_ThenReturnsKey() {
		assertThat(suggestionsTabFormNavigatorEntry.getKey(), equalTo("suggestions-tab"));
	}

	@Test
	public void getLabel_WhenNoError_ThenReturnsLabel() {

		when(ResourceBundleUtil.getBundle(Locale.CANADA, SearchSuggestionsConstants.SYMBOLIC_NAME)).thenReturn(mockResourceBundle);
		String localisedLabel = "localisedLabel";
		when(mockLanguage.get(mockResourceBundle, suggestionsTabFormNavigatorEntry.getKey())).thenReturn(localisedLabel);

		assertThat(suggestionsTabFormNavigatorEntry.getLabel(Locale.CANADA), equalTo(localisedLabel));

	}

	@Test
	public void include_WhenNoError_ThenSetSettingsAsAttribute() throws Exception {

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockSearchSuggestionsSettingsService.getSuggestionsSetting(mockThemeDisplay)).thenReturn(mockSearchSuggestionsSettings);

		suppress(methods(BaseJSPFormNavigatorEntry.class, "include"));

		suggestionsTabFormNavigatorEntry.include(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(SearchSuggestionsRequestKeys.SEARCH_SUGGESTIONS_SETTINGS, mockSearchSuggestionsSettings);

	}

}
