package com.placecube.digitalplace.local.search.suggestion.web.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.suggest.Suggester.StringDistance;
import com.liferay.portal.kernel.search.suggest.Suggester.SuggestMode;
import com.liferay.portal.kernel.search.suggest.SuggesterResult;
import com.liferay.portal.kernel.search.suggest.SuggesterResult.Entry;
import com.liferay.portal.kernel.search.suggest.SuggesterResult.Entry.Option;
import com.liferay.portal.kernel.search.suggest.SuggesterResults;
import com.liferay.portal.kernel.search.suggest.TermSuggester;
import com.liferay.portal.kernel.security.RandomUtil;
import com.liferay.portal.kernel.util.Localization;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RandomUtil.class, LocalizationUtil.class })
public class TermSuggestionServiceTest {

	private static final String TERM_1 = "term1";

	private static final String TERM_2 = "term2";

	private List<Entry> entries = new ArrayList<>();

	private List<Entry> entries2 = new ArrayList<>();

	@Mock
	private Entry mockEntry;

	@Mock
	private Entry mockEntry2;

	@Mock
	private Localization mockLocalization;

	@Mock
	private Option mockOption;

	@Mock
	private Option mockOption2;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchSuggestionCompanyConfiguration mockSearchSuggestionCompanyConfiguration;

	@Mock
	private SuggesterResult mockSuggesterResult;

	@Mock
	private SuggesterResult mockSuggesterResult2;

	@Mock
	private SuggesterResults mockSuggesterResults;

	private List<Option> options = new ArrayList<>();

	private List<Option> options2 = new ArrayList<>();

	private Collection<SuggesterResult> suggesterResults = new ArrayList<>();

	@InjectMocks
	private TermSuggestionService termSuggestionService;;

	@Before
	public void activeSetup() {

		mockStatic(RandomUtil.class, LocalizationUtil.class);

		termSuggestionService = new TermSuggestionService();

	}

	@Test
	public void createTermSuggester_WhenNoError_ThenCreatesTermSuggester() {

		String keywords = "term";
		int randomValue = 1;
		when(RandomUtil.nextInt(1000)).thenReturn(randomValue);
		String field = "field";
		when(mockSearchSuggestionCompanyConfiguration.field()).thenReturn(field);
		when(LocalizationUtil.getLocalization()).thenReturn(mockLocalization);
		String languageId = "en_GB";
		when(mockSearchContext.getLanguageId()).thenReturn(languageId);
		String localisedField = field + StringPool.UNDERLINE + languageId;
		when(mockLocalization.getLocalizedName(field, languageId)).thenReturn(localisedField);
		when(mockSearchContext.getKeywords()).thenReturn(keywords);

		TermSuggester result = termSuggestionService.createTermSuggester(mockSearchContext, mockSearchSuggestionCompanyConfiguration);

		assertThat(result.getName(), equalTo("termSuggester" + randomValue));
		assertThat(result.getField(), equalTo(localisedField));
		assertThat(result.getValue(), equalTo(keywords));
		assertThat(result.getStringDistance(), equalTo(StringDistance.JAROWINKLER));
		assertThat(result.getAccuracy(), equalTo(0.50f));
		assertThat(result.getMinWordLength(), equalTo(1));
		assertThat(result.getSuggestMode(), equalTo(SuggestMode.POPULAR));

	}

	@Test
	public void getSuggestions_WhenSuggesterResultIsNull_ThenReturnsEmptyList() {

		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);
		List<String> result = termSuggestionService.getSuggestions(null, mockSearchSuggestionCompanyConfiguration);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getSuggestions_WhenThereAreSuggesterResults_ThenSuggestionIsAdded() {

		suggesterResults.add(mockSuggesterResult);
		suggesterResults.add(mockSuggesterResult2);
		entries.add(mockEntry);
		entries2.add(mockEntry2);
		options.add(mockOption);
		options2.add(mockOption2);
		when(mockOption.getText()).thenReturn(TERM_1);
		when(mockOption2.getText()).thenReturn(TERM_2);
		when(mockEntry.getOptions()).thenReturn(options);
		when(mockEntry2.getOptions()).thenReturn(options2);
		when(mockSuggesterResult.getEntries()).thenReturn(entries);
		when(mockSuggesterResult2.getEntries()).thenReturn(entries2);
		when(mockSuggesterResults.getSuggesterResults()).thenReturn(suggesterResults);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);

		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result, contains(TERM_1, TERM_2));
	}

	@Test
	public void getSuggestions_WhenThereAreSuggesterResultsWithStopWord_ThenStopWordIsNotAdded() {

		suggesterResults.add(mockSuggesterResult);
		suggesterResults.add(mockSuggesterResult2);
		entries.add(mockEntry);
		entries2.add(mockEntry2);
		options.add(mockOption);
		options2.add(mockOption2);
		when(mockOption.getText()).thenReturn(TERM_1);
		when(mockOption2.getText()).thenReturn(TERM_2);
		when(mockEntry.getOptions()).thenReturn(options);
		when(mockEntry2.getOptions()).thenReturn(options2);
		when(mockSuggesterResult.getEntries()).thenReturn(entries);
		when(mockSuggesterResult2.getEntries()).thenReturn(entries2);
		when(mockSuggesterResults.getSuggesterResults()).thenReturn(suggesterResults);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(TERM_1);
		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result, contains(TERM_2));
	}

	@Test
	public void getSuggestions_WhenThereAreSuggesterResultsWithTheSameSuggestion_ThenSuggestionIsAddedOnce() {

		suggesterResults.add(mockSuggesterResult);
		suggesterResults.add(mockSuggesterResult2);
		entries.add(mockEntry);
		entries2.add(mockEntry2);
		options.add(mockOption);
		options2.add(mockOption2);
		when(mockOption.getText()).thenReturn(TERM_1);
		when(mockOption2.getText()).thenReturn(TERM_1);
		when(mockEntry.getOptions()).thenReturn(options);
		when(mockEntry2.getOptions()).thenReturn(options2);
		when(mockSuggesterResult.getEntries()).thenReturn(entries);
		when(mockSuggesterResult2.getEntries()).thenReturn(entries2);
		when(mockSuggesterResults.getSuggesterResults()).thenReturn(suggesterResults);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);

		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result.size(), equalTo(1));
		assertThat(result, contains(TERM_1));
	}

	@Test
	public void getSuggestions_WhenThereIsNotEntryForSuggesterResult_ThenReturnsEmptyList() {

		suggesterResults.add(mockSuggesterResult);
		when(mockSuggesterResult.getEntries()).thenReturn(Collections.EMPTY_LIST);
		when(mockSuggesterResults.getSuggesterResults()).thenReturn(suggesterResults);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);

		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getSuggestions_WhenThereIsOptionForEntry_ThenTextIsAddedAsSuggestion() {

		suggesterResults.add(mockSuggesterResult);
		entries.add(mockEntry);
		options.add(mockOption);
		when(mockOption.getText()).thenReturn(TERM_1);
		when(mockEntry.getOptions()).thenReturn(options);
		when(mockSuggesterResult.getEntries()).thenReturn(entries);
		when(mockSuggesterResults.getSuggesterResults()).thenReturn(suggesterResults);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);

		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result, contains(TERM_1));
	}

	@Test
	public void getSuggestionsAsList_WhenThereIsNotOptionForEntry_ThenReturnsEmptyList() {

		suggesterResults.add(mockSuggesterResult);
		entries.add(mockEntry);
		when(mockEntry.getOptions()).thenReturn(Collections.EMPTY_LIST);
		when(mockSuggesterResult.getEntries()).thenReturn(entries);
		when(mockSuggesterResults.getSuggesterResults()).thenReturn(suggesterResults);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);

		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getSuggestionsAsList_WhenThereIsNotSuggesterResult_ThenReturnsEmptyList() {

		when(mockSuggesterResults.getSuggesterResults()).thenReturn(Collections.EMPTY_LIST);
		when(mockSearchSuggestionCompanyConfiguration.stopWords()).thenReturn(StringPool.BLANK);

		List<String> result = termSuggestionService.getSuggestions(mockSuggesterResults, mockSearchSuggestionCompanyConfiguration);

		assertThat(result.isEmpty(), equalTo(true));
	}

}
