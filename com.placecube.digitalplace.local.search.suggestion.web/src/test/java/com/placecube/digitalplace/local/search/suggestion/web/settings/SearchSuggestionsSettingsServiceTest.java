package com.placecube.digitalplace.local.search.suggestion.web.settings;

import static org.junit.Assert.assertFalse;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionConfigurationService;
import com.placecube.digitalplace.local.search.suggestion.web.settings.constants.SearchSuggestionsRequestKeys;
import com.placecube.digitalplace.local.search.suggestion.web.settings.model.SearchSuggestionSettings;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ SearchSuggestionSettings.class, PropertiesParamUtil.class })
public class SearchSuggestionsSettingsServiceTest extends PowerMockito {

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private SearchSuggestionCompanyConfiguration mockSearchSuggestionCompanyConfiguration;

	@Mock
	private SearchSuggestionConfigurationService mockSearchSuggestionConfigurationService;

	@Mock
	private SearchSuggestionSettings mockSearchSuggestionSettings;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@InjectMocks
	private SearchSuggestionsSettingsService searchSuggestionsSettingsService;

	@Before
	public void activeSetup() {

		mockStatic(SearchSuggestionSettings.class, PropertiesParamUtil.class);

	}

	@Test
	@Parameters({ "termSuggestions,false,true", "querySuggestions,true,false" })
	public void isQuerySuggestionEnabled_WhenSearchSuggestionCompanyConfigurationIsEnabled_ThenReturnsSearchSuggestionSettings(String searchTypeSuggestion, boolean querySuggestion,
			boolean termSuggestions) {

		long companyId = 1;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockSearchSuggestionConfigurationService.getConfiguration(companyId)).thenReturn(Optional.of(mockSearchSuggestionCompanyConfiguration));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);

		when(mockSearchSuggestionCompanyConfiguration.enabled()).thenReturn(true);
		when(PropertiesParamUtil.getString(mockUnicodeProperties, mockHttpServletRequest, SearchSuggestionsRequestKeys.SEARCH_SUGGESTIONS)).thenReturn(searchTypeSuggestion);
		when(SearchSuggestionSettings.create(querySuggestion, termSuggestions)).thenReturn(mockSearchSuggestionSettings);

		assertFalse(searchSuggestionsSettingsService.isQuerySuggestionEnabled(mockThemeDisplay));

	}

	@Test
	public void isQuerySuggestionEnabled_WhenSearchSuggestionCompanyConfigurationIsNotPresent_ThenReturnsFale() {

		long companyId = 1;

		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockSearchSuggestionConfigurationService.getConfiguration(companyId)).thenReturn(Optional.empty());

		assertFalse(searchSuggestionsSettingsService.isQuerySuggestionEnabled(mockThemeDisplay));

	}
}
