package com.placecube.digitalplace.local.search.suggestion.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.suggest.QuerySuggester;
import com.liferay.portal.kernel.search.suggest.SuggesterResults;
import com.liferay.portal.kernel.search.suggest.TermSuggester;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;

public class SearchSuggestionServiceTest extends PowerMockito {

	@Mock
	private QuerySuggester mockQuerySuggester;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchSuggestionCompanyConfiguration mockSearchSuggestionCompanyConfiguration;

	@Mock
	private TermSuggester mockSuggester;

	@Mock
	private SuggesterResults mockSuggesterResults;

	@Mock
	private TermSuggestionService mockTermSuggestionService;

	@InjectMocks
	private SearchSuggestionService searchSuggestionService;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test(expected = SearchException.class)
	public void getSuggesterResults_WhenErrorSuggesting_ThenThrowSearchException() throws SearchException {

		when(mockTermSuggestionService.createTermSuggester(mockSearchContext, mockSearchSuggestionCompanyConfiguration)).thenReturn(mockSuggester);

		when(mockQuerySuggester.suggest(mockSearchContext, mockSuggester)).thenThrow(SearchException.class);

		searchSuggestionService.getSuggesterResults(mockSearchContext, mockSearchSuggestionCompanyConfiguration);

	}

	@Test
	public void getSuggesterResults_WhenNoError_ThenReturnsSuggesterResults() throws SearchException {

		when(mockTermSuggestionService.createTermSuggester(mockSearchContext, mockSearchSuggestionCompanyConfiguration)).thenReturn(mockSuggester);

		when(mockQuerySuggester.suggest(mockSearchContext, mockSuggester)).thenReturn(mockSuggesterResults);

		SuggesterResults result = searchSuggestionService.getSuggesterResults(mockSearchContext, mockSearchSuggestionCompanyConfiguration);

		assertThat(result, sameInstance(mockSuggesterResults));

	}

}
