AUI().ready('aui-base','autocomplete-list','datasource-io','liferay-portlet-url',function(A){
	
	A.on('resize',function(event){
		resetSearchSuggestionAutoComplete();
	});
	
	Liferay.after('endNavigate',function(){
		resetSearchSuggestionAutoComplete();
	});
	
	A.all(".search-bar-keywords-input, input[name='keywords'], .keywords-input").each(function(node, index, list){
		
		node.on('focus',function(){
			
			var inputNode = "#" + this.get('id');
				
			if(typeof(Liferay.component(inputNode)) === "undefined"){
			
				
				var TPL_BLOCK_SUGGESTION_ROW = '<div class="nameplate">' +
												'<div class="nameplate-content">' +
													'{term}' +
												'</div>' +
											'</div>';	
				var dataSource = new A.DataSource.IO(
						{
						source: getResourceUrlAsString()
						}
					);
				
				var autocomplete = new A.AutoCompleteList({
					activateFirstItem: false,
					inputNode: inputNode,
					maxResults: 10,
					minQueryLength: 1,
					on: {
						select: function(event) {
							var result = event.result.raw;
							A.one(inputNode).val(result.term.replace(/(<([^>]+)>)/gi, ""));
							var formName = A.one(inputNode).ancestor("form").get('name');
							document.forms[formName].submit();
						}
					},		
					render: 'true',
					source: dataSource,
					requestTemplate: '&_com_placecube_digitalplace_local_search_suggestion_web_portlet_SearchSuggestionPortlet_keywords={query}',
					resultFormatter: function(query, results){
			
						var resultListEntries = A.Array.map(results, function(result){
			
							var resultListEntryMarkUp  = A.Lang.sub(
											TPL_BLOCK_SUGGESTION_ROW,
											{
												term: result.raw.term
											}
										);
			
			
							return resultListEntryMarkUp;
						});
			
						return resultListEntries;
					},
					resultListLocator: function (response) {
						return A.JSON.parse(response[0].responseText);
					},
					resultTextLocator: function (result) {
						return '';
					}		
				});
				
				Liferay.component(inputNode, autocomplete);

			}
		});
		
		
	});
	
	function getResourceUrlAsString(){
		var resourceURL = Liferay.PortletURL.createResourceURL();
		resourceURL.setParameter("p_p_id", 'com_placecube_digitalplace_local_search_suggestion_web_portlet_SearchSuggestionPortlet');
		
		return resourceURL.toString();
	}

	function resetSearchSuggestionAutoComplete(){
		
		A.all(".search-bar-keywords-input, input[name='keywords'], .keywords-input").each(function(node, index, list){
			
			var inputNode = "#" + this.get('id');
			var component = Liferay.component(inputNode);
			
			if(typeof(component) != "undefined"){
				component.destroy(true);
				Liferay.destroyComponent(inputNode);
			}
			
		});
	}
});
