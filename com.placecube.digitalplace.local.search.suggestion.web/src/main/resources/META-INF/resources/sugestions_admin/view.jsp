<%@ include file="/init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<aui:fieldset>
		<portlet:actionURL name="/index-suggestions" var="indexSuggestionsURL" />
		<portlet:actionURL name="/clear-suggestions" var="clearSuggestionsURL" />
	
		<aui:form action="${ indexSuggestionsURL }" method="post" name="fm" enctype="multipart/form-data" cssClass="container-fluid container-fluid-max-xl sheet" >
		
		<div class="inline-alert-container lfr-alert-container"></div>
	
		<liferay-ui:error exception="<%=FileNotFoundException.class%>" message="please-upload-a-valid-txt-file" />
		
		<div class="alert alert-info">
			<liferay-ui:message key="suggestions-import-help"/>
		</div>
			
			<aui:fieldset label="upload-file">
				<div class="form-group input-text-wrapper">
					<input name="<portlet:namespace/>importFile" id="<portlet:namespace/>importFile" size="50" type="file"  />
				</div>
			</aui:fieldset>		
			
			<aui:button-row>
				<aui:button type="submit" value="index"/>
				<aui:a label="clear" href="${ clearSuggestionsURL }" cssClass="btn btn-secondary clear"/>
			</aui:button-row>
		</aui:form>
	</aui:fieldset>
</div>

<aui:script>
	$('a.clear').on('click',function(e){
		return confirm('<liferay-ui:message key="clear-suggestions-confirmation" />');
	});
</aui:script>