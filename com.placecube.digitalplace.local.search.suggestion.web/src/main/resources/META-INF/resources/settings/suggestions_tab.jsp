<%@ include file="/init.jsp" %>


<aui:input helpMessage="disable-search-suggestions-on-this-site-help" label="disable-search-suggestions-on-this-site" name="TypeSettingsProperties--searchSuggestions--" type="radio" value="0" checked="${ !searchSuggestionSettings.isSearchSuggestionsEnabled() }" />
<aui:input helpMessage="term-suggestions-help" label="term-suggestions" name="TypeSettingsProperties--searchSuggestions--" type="radio" value="termSuggestions" checked="${ searchSuggestionSettings.isTermSuggestionsEnabled() }" />
<aui:input helpMessage="query-suggestions-help" label="query-suggestions" name="TypeSettingsProperties--searchSuggestions--" type="radio" value="querySuggestions" checked="${ searchSuggestionSettings.isQuerySuggestionsEnabled() }" />

