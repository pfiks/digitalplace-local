package com.placecube.digitalplace.local.search.suggestion.web.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;

@Component(immediate = true, service = SearchSuggestionConfigurationService.class)
public class SearchSuggestionConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(SearchSuggestionConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	public Optional<SearchSuggestionCompanyConfiguration> getConfiguration(long companyId) {

		try {
			return Optional.of(configurationProvider.getCompanyConfiguration(SearchSuggestionCompanyConfiguration.class, companyId));
		} catch (ConfigurationException e) {
			LOG.error(e);
			return Optional.empty();
		}
	}

}
