package com.placecube.digitalplace.local.search.suggestion.web.controlpanel;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelCategory;
import com.liferay.application.list.PanelCategory;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.placecube.digitalplace.local.search.suggestion.web.constants.SearchSuggestionsConstants;

@Component(immediate = true, property = { "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION, "panel.category.order:Integer=600" }, service = PanelCategory.class)
public class SearchPanelCategory extends BasePanelCategory {

	@Reference
	private Language language;

	@Override
	public String getKey() {
		return SearchPanelCategoryKeys.CONTROL_PANEL_SEARCH;
	}

	@Override
	public String getLabel(Locale locale) {
		return language.get(ResourceBundleUtil.getBundle(locale, SearchSuggestionsConstants.SYMBOLIC_NAME), getKey());

	}

}