package com.placecube.digitalplace.local.search.suggestion.web.settings.model;

public class SearchSuggestionSettings {

	private final boolean querySuggestions;
	private final boolean termSuggestions;

	private SearchSuggestionSettings(boolean querySuggestions, boolean termSuggestions) {
		this.querySuggestions = querySuggestions;
		this.termSuggestions = termSuggestions;
	}

	public static SearchSuggestionSettings create(boolean querySuggestions, boolean termSuggestions) {
		return new SearchSuggestionSettings(querySuggestions, termSuggestions);
	}

	public boolean isQuerySuggestionsEnabled() {
		return querySuggestions;
	}

	public boolean isSearchSuggestionsEnabled() {
		return querySuggestions || termSuggestions;
	}

	public boolean isTermSuggestionsEnabled() {
		return termSuggestions;
	}
}
