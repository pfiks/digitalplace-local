package com.placecube.digitalplace.local.search.suggestion.web.constants;

public final class SearchSuggestionsConstants {

	public static final String SYMBOLIC_NAME = "com.placecube.digitalplace.local.search.suggestion.web";

	private SearchSuggestionsConstants() {

	}

}
