package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.search.engine.adapter.SearchEngineAdapter;
import com.liferay.portal.search.engine.adapter.document.DeleteByQueryDocumentRequest;
import com.liferay.portal.search.index.IndexNameBuilder;
import com.placecube.digitalplace.local.search.suggestion.web.constants.PortletKeys;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionObjectFactory;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SUGGESTION_PORTLET, "mvc.command.name=/clear-suggestions" }, service = MVCActionCommand.class)
public class ClearQuerySuggestionsMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private IndexNameBuilder indexNameBuilder;

	@Reference
	private Portal portal;

	@Reference(target = "(search.engine.impl=Elasticsearch)")
	private SearchEngineAdapter searchEngineAdapter;

	@Reference
	private SearchSuggestionObjectFactory searchSuggestionObjectFactory;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String indexName = indexNameBuilder.getIndexName(portal.getCompanyId(actionRequest));

		StringQuery stringQuery = searchSuggestionObjectFactory.buildStringQuery(String.format("(+groupId:%s AND +type:querySuggestion)", portal.getScopeGroupId(actionRequest)));

		DeleteByQueryDocumentRequest deleteByQueryDocumentRequest = searchSuggestionObjectFactory.buidDeleteByQueryDocumentRequest(stringQuery, indexName);

		deleteByQueryDocumentRequest.setRefresh(true);

		searchEngineAdapter.execute(deleteByQueryDocumentRequest);

	}

}