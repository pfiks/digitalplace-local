package com.placecube.digitalplace.local.search.suggestion.web.settings;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorCategory;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.placecube.digitalplace.local.search.suggestion.web.constants.SearchSuggestionsConstants;

@Component(property = "form.navigator.category.order:Integer=1", service = FormNavigatorCategory.class)
public class SearchFormNavigatorCategory implements FormNavigatorCategory {

	@Reference
	private Language language;

	@Override
	public String getFormNavigatorId() {
		return FormNavigatorConstants.FORM_NAVIGATOR_ID_SITES;
	}

	@Override
	public String getKey() {
		return SearchFormNavigatorCategoryKeys.SEARCH;
	}

	@Override
	public String getLabel(Locale locale) {
		return language.get(ResourceBundleUtil.getBundle(locale, SearchSuggestionsConstants.SYMBOLIC_NAME), getKey());
	}

}