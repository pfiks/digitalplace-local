package com.placecube.digitalplace.local.search.suggestion.web.controlpanel;

public final class SearchPanelCategoryKeys {

	public static final String CONTROL_PANEL_SEARCH = "control_panel.dp_search";

	private SearchPanelCategoryKeys() {
	}
}
