package com.placecube.digitalplace.local.search.suggestion.web.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.LinkedList;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.search.engine.adapter.document.DeleteByQueryDocumentRequest;

@Component(immediate = true, service = SearchSuggestionObjectFactory.class)
public class SearchSuggestionObjectFactory {

	public DeleteByQueryDocumentRequest buidDeleteByQueryDocumentRequest(StringQuery stringQuery, String indexName) {
		return new DeleteByQueryDocumentRequest(stringQuery, indexName);
	}

	public StringQuery buildStringQuery(String query) {
		return new StringQuery(query);
	}

	public InputStream getFileInputStream(File file) throws FileNotFoundException {
		return new FileInputStream(file);
	}

	public Collection<String> getLinesFromStream(InputStream inputStream) throws IOException {
		Collection<String> lines = new LinkedList<>();

		StringUtil.readLines(inputStream, lines);

		return lines;

	}

}
