package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.local.search.suggestion.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.add-default-resource=true",
		"javax.portlet.init-param.view-template=/sugestions_admin/view.jsp", "javax.portlet.name=" + PortletKeys.SUGGESTION_PORTLET, "javax.portlet.security-role-ref=administrator,power-user,user",
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class SuggestionsAdminPortlet extends MVCPortlet {

}