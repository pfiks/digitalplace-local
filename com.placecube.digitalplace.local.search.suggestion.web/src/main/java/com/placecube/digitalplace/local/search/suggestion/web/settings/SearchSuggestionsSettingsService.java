package com.placecube.digitalplace.local.search.suggestion.web.settings;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionConfigurationService;
import com.placecube.digitalplace.local.search.suggestion.web.settings.constants.SearchSuggestionsRequestKeys;
import com.placecube.digitalplace.local.search.suggestion.web.settings.model.SearchSuggestionSettings;

@Component(immediate = true, service = SearchSuggestionsSettingsService.class)
public class SearchSuggestionsSettingsService {

	@Reference
	private SearchSuggestionConfigurationService searchSuggestionConfigurationService;

	public SearchSuggestionSettings getSuggestionsSetting(ThemeDisplay themeDisplay) {

		HttpServletRequest httpServletRequest = themeDisplay.getRequest();
		UnicodeProperties typeSettingsProperties = themeDisplay.getScopeGroup().getTypeSettingsProperties();
		String searchSuggestions = PropertiesParamUtil.getString(typeSettingsProperties, httpServletRequest, SearchSuggestionsRequestKeys.SEARCH_SUGGESTIONS);

		Optional<SearchSuggestionCompanyConfiguration> searchSuggestionCompanyConfiguration = searchSuggestionConfigurationService.getConfiguration(themeDisplay.getCompanyId());
		boolean isSearchSuggestionEnabled = searchSuggestionCompanyConfiguration.isPresent() && searchSuggestionCompanyConfiguration.get().enabled();
		boolean isTermSuggestionEnabled = isSearchSuggestionEnabled && searchSuggestions.equalsIgnoreCase(SearchSuggestionsRequestKeys.TERM_SUGGESTIONS);
		boolean isQuerySuggestionEnabled = isSearchSuggestionEnabled && searchSuggestions.equalsIgnoreCase(SearchSuggestionsRequestKeys.QUERY_SUGGESTIONS);

		return SearchSuggestionSettings.create(isQuerySuggestionEnabled, isTermSuggestionEnabled);
	}

	public boolean isQuerySuggestionEnabled(ThemeDisplay themeDisplay) {
		Optional<SearchSuggestionCompanyConfiguration> searchSuggestionCompanyConfiguration = searchSuggestionConfigurationService.getConfiguration(themeDisplay.getCompanyId());
		return searchSuggestionCompanyConfiguration.isPresent() && searchSuggestionCompanyConfiguration.get().enabled() && getSuggestionsSetting(themeDisplay).isQuerySuggestionsEnabled();
	}

}
