package com.placecube.digitalplace.local.search.suggestion.web.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.servlet.taglib.ui.BaseJSPFormNavigatorEntry;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorConstants;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.local.search.suggestion.web.constants.SearchSuggestionsConstants;
import com.placecube.digitalplace.local.search.suggestion.web.settings.constants.SearchSuggestionsRequestKeys;
import com.placecube.digitalplace.local.search.suggestion.web.settings.model.SearchSuggestionSettings;

@Component(immediate = true, property = "form.navigator.entry.order:Integer=30", service = FormNavigatorEntry.class)
public class SuggestionsTabFormNavigatorEntry extends BaseJSPFormNavigatorEntry<Group> {

	@Reference
	private Language language;

	@Reference
	private SearchSuggestionsSettingsService searchSuggestionsSettingsService;

	@Override
	public String getCategoryKey() {
		return SearchFormNavigatorCategoryKeys.SEARCH;
	}

	@Override
	public String getFormNavigatorId() {
		return FormNavigatorConstants.FORM_NAVIGATOR_ID_SITES;
	}

	@Override
	public String getKey() {
		return "suggestions-tab";
	}

	@Override
	public String getLabel(Locale locale) {
		return language.get(ResourceBundleUtil.getBundle(locale, SearchSuggestionsConstants.SYMBOLIC_NAME), getKey());
	}

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

		SearchSuggestionSettings searchSuggestionSettings = searchSuggestionsSettingsService.getSuggestionsSetting((ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY));

		httpServletRequest.setAttribute(SearchSuggestionsRequestKeys.SEARCH_SUGGESTIONS_SETTINGS, searchSuggestionSettings);

		super.include(httpServletRequest, httpServletResponse);

	}

	@Override
	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.local.search.suggestion.web)", unbind = "-")
	public void setServletContext(ServletContext servletContext) {
		super.setServletContext(servletContext);
	}

	@Override
	protected String getJspPath() {
		return "/settings/suggestions_tab.jsp";
	}

}