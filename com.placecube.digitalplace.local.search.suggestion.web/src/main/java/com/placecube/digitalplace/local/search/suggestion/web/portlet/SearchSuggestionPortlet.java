package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.suggest.SuggesterResults;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.util.PropsUtil;
import com.liferay.portal.util.PropsValues;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;
import com.placecube.digitalplace.local.search.suggestion.web.constants.PortletKeys;
import com.placecube.digitalplace.local.search.suggestion.web.service.JSONBuilderService;
import com.placecube.digitalplace.local.search.suggestion.web.service.QuerySuggestionService;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionConfigurationService;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionService;
import com.placecube.digitalplace.local.search.suggestion.web.service.TermSuggestionService;
import com.placecube.digitalplace.local.search.suggestion.web.settings.SearchSuggestionsSettingsService;
import com.placecube.digitalplace.local.search.suggestion.web.settings.model.SearchSuggestionSettings;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.add-default-resource=true", "javax.portlet.display-name=Search Suggestion",
		"javax.portlet.info.keywords=Search Suggestion", "javax.portlet.info.short-title=Search Suggestion", "javax.portlet.info.title=Search Suggestion", "javax.portlet.init-param.template-path=/",
		"javax.portlet.name=" + PortletKeys.SEARCH_SUGGESTION_PORTLET, "javax.portlet.security-role-ref=administrator,guest,power-user,user", "javax.portlet.supports.mime-type=text/html",
		"portlet.add.default.resource.check.whitelist=" + PortletKeys.SEARCH_SUGGESTION_PORTLET, "com.liferay.portlet.header-portlet-javascript=/js/search.js" }, service = Portlet.class)
public class SearchSuggestionPortlet extends MVCPortlet {

	private static final Log LOG = LogFactoryUtil.getLog(SearchSuggestionPortlet.class);

	@Reference
	private JSONBuilderService jsonBuilderService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Reference
	private QuerySuggestionService querySuggestionService;

	@Reference
	private SearchSuggestionConfigurationService searchSuggestionConfigurationService;

	@Reference
	private SearchSuggestionService searchSuggestionService;

	@Reference
	private SearchSuggestionsSettingsService searchSuggestionsSettingsService;

	@Reference
	private TermSuggestionService termSuggestionService;

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		// do not remove this empty doView, otherwise it will display null logs
	}

	@SuppressWarnings("deprecation")
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {

		try {

			SearchSuggestionSettings searchSuggestionSettings = searchSuggestionsSettingsService.getSuggestionsSetting((ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY));

			if (searchSuggestionSettings.isSearchSuggestionsEnabled()) {

				String keywords = ParamUtil.getString(resourceRequest, "keywords");
				SearchContext searchContext = SearchContextFactory.getInstance(portal.getHttpServletRequest(resourceRequest));
				searchContext.setKeywords(keywords);

				List<String> suggestions = null;

				if (!searchSuggestionSettings.isQuerySuggestionsEnabled()) {

					Optional<SearchSuggestionCompanyConfiguration> configuration = searchSuggestionConfigurationService.getConfiguration(portal.getCompanyId(resourceRequest));
					SuggesterResults suggesterResults = searchSuggestionService.getSuggesterResults(searchContext, configuration.get());
					suggestions = termSuggestionService.getSuggestions(suggesterResults, configuration.get());

				} else {

					suggestions = querySuggestionService.getSuggestions(searchContext);
				}

				JSONArray jsonResult = jsonBuilderService.buildJSONResponse(suggestions);

				LOG.debug("Suggestion results: " + jsonResult.toJSONString());

				writeJSON(resourceRequest, resourceResponse, jsonResult);

			} else {

				writeJSON(resourceRequest, resourceResponse, jsonFactory.createJSONArray());
			}

		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (!hasPortletId()) {
			addPortletIdLayoutStaticPortletsAll();
		}
	}

	@Deactivate
	@Modified
	protected void deactivate(Map<String, Object> properties) {
		if (hasPortletId()) {
			removePortletIdLayoutStaticPortletsAll();
		}
	}

	private void addPortletIdLayoutStaticPortletsAll() {
		String[] layoutStaticPortletsAll = PropsValues.LAYOUT_STATIC_PORTLETS_ALL;

		layoutStaticPortletsAll = ArrayUtil.append(layoutStaticPortletsAll, PortletKeys.SEARCH_SUGGESTION_PORTLET);

		PropsUtil.set(PropsKeys.LAYOUT_STATIC_PORTLETS_ALL, StringUtil.merge(layoutStaticPortletsAll));

		PropsValues.LAYOUT_STATIC_PORTLETS_ALL = layoutStaticPortletsAll;
	}

	private boolean hasPortletId() {
		return ArrayUtil.contains(PropsValues.LAYOUT_STATIC_PORTLETS_ALL, PortletKeys.SEARCH_SUGGESTION_PORTLET, false);
	}

	private void removePortletIdLayoutStaticPortletsAll() {
		String[] layoutStaticPortletsAll = PropsValues.LAYOUT_STATIC_PORTLETS_ALL;

		layoutStaticPortletsAll = ArrayUtil.remove(layoutStaticPortletsAll, PortletKeys.SEARCH_SUGGESTION_PORTLET);

		PropsUtil.set(PropsKeys.LAYOUT_STATIC_PORTLETS_ALL, StringUtil.merge(layoutStaticPortletsAll));

		PropsValues.LAYOUT_STATIC_PORTLETS_ALL = layoutStaticPortletsAll;
	}
}