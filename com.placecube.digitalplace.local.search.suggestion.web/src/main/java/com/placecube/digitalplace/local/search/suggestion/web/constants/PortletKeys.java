package com.placecube.digitalplace.local.search.suggestion.web.constants;

public final class PortletKeys {

	public static final String SEARCH_SUGGESTION_PORTLET = "com_placecube_digitalplace_local_search_suggestion_web_portlet_SearchSuggestionPortlet";

	public static final String SUGGESTION_PORTLET = "com_placecube_digitalplace_local_search_suggestion_web_portlet_SuggestionAdminPortlet";

	private PortletKeys() {

	}
}