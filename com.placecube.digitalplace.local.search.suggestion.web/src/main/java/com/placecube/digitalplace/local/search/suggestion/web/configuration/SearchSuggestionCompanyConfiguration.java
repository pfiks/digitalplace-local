package com.placecube.digitalplace.local.search.suggestion.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "search", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration", localization = "content/Language", name = "digitalplace-search-suggestion")
public interface SearchSuggestionCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false")
	boolean enabled();

	@Meta.AD(required = true, deflt = "localized_title", description = "field-description")
	String field();

	@Meta.AD(required = false, deflt = "a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with", description = "stop-words-description")
	String stopWords();
}
