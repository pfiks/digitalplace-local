package com.placecube.digitalplace.local.search.suggestion.web.service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexSearcher;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.search.highlight.HighlightUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;

@Component(immediate = true, service = QuerySuggestionService.class)
public class QuerySuggestionService {

	static final String KEYWORD_SEARCH_FIELD = "keywordSearch";

	static final String QUERY = "(+groupId:%s AND +type:querySuggestion AND %s:%s)";

	@Reference
	public IndexSearcher indexSearcher;

	public List<String> getSuggestions(SearchContext searchContext) throws SearchException {

		String localisedKeywordSearchField = LocalizationUtil.getLocalization().getLocalizedName(KEYWORD_SEARCH_FIELD, searchContext.getLanguageId());

		StringQuery query = new StringQuery(String.format(QUERY, searchContext.getGroupIds()[0], localisedKeywordSearchField, searchContext.getKeywords()));

		Hits hits = indexSearcher.search(searchContext, query);

		List<String> suggestions = new LinkedList<>();

		Arrays.asList(hits.getDocs()).stream().forEach(doc -> suggestions.add(HighlightUtil.highlight(doc.get(localisedKeywordSearchField), new String[] { searchContext.getKeywords() })));

		return suggestions;
	}

}
