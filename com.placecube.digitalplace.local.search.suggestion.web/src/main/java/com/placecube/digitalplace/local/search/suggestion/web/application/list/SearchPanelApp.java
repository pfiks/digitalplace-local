package com.placecube.digitalplace.local.search.suggestion.web.application.list;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.placecube.digitalplace.local.search.suggestion.web.constants.PortletKeys;
import com.placecube.digitalplace.local.search.suggestion.web.controlpanel.SearchPanelCategoryKeys;
import com.placecube.digitalplace.local.search.suggestion.web.settings.SearchSuggestionsSettingsService;

@Component(immediate = true, property = { "panel.app.order:Integer=600", "panel.category.key=" + SearchPanelCategoryKeys.CONTROL_PANEL_SEARCH }, service = PanelApp.class)
public class SearchPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.SUGGESTION_PORTLET + ")", unbind = "-")
	private Portlet portlet;

	@Reference
	private SearchSuggestionsSettingsService searchSuggestionsSettingsService;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.SUGGESTION_PORTLET;
	}

	@Override
	public boolean isShow(PermissionChecker permissionChecker, Group group) throws PortalException {

		return super.isShow(permissionChecker, group) && !group.isCompany()
				&& searchSuggestionsSettingsService.isQuerySuggestionEnabled(ServiceContextThreadLocal.getServiceContext().getThemeDisplay());
	}

}