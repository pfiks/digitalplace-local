package com.placecube.digitalplace.local.search.suggestion.web.settings.constants;

public final class SearchSuggestionsRequestKeys {

	public static final String QUERY_SUGGESTIONS = "querySuggestions";

	public static final String SEARCH_SUGGESTIONS = "searchSuggestions";

	public static final String SEARCH_SUGGESTIONS_SETTINGS = "searchSuggestionSettings";

	public static final String TERM_SUGGESTIONS = "termSuggestions";

	private SearchSuggestionsRequestKeys() {

	}
}
