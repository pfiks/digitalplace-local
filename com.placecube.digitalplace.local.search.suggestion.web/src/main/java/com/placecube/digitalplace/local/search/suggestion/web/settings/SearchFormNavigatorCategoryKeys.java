package com.placecube.digitalplace.local.search.suggestion.web.settings;

public final class SearchFormNavigatorCategoryKeys {

	public static final String SEARCH = "dp-search";

	private SearchFormNavigatorCategoryKeys() {

	}
}