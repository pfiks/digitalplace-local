package com.placecube.digitalplace.local.search.suggestion.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.suggest.QuerySuggester;
import com.liferay.portal.kernel.search.suggest.Suggester;
import com.liferay.portal.kernel.search.suggest.SuggesterResults;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;

@Component(immediate = true, service = SearchSuggestionService.class)
public class SearchSuggestionService {

	@Reference
	private QuerySuggester querySuggester;

	@Reference
	private TermSuggestionService termSuggestionService;

	@SuppressWarnings("deprecation")
	public SuggesterResults getSuggesterResults(SearchContext searchContext, SearchSuggestionCompanyConfiguration configuration) throws SearchException {

		Suggester termSuggester = termSuggestionService.createTermSuggester(searchContext, configuration);

		return querySuggester.suggest(searchContext, termSuggester);

	}

}
