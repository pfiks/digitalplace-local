package com.placecube.digitalplace.local.search.suggestion.web.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

@Component(immediate = true, service = JSONBuilderService.class)
public class JSONBuilderService {

	private static final String TERM = "term";

	public JSONArray buildJSONResponse(List<String> suggestions) {

		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (String suggestion : suggestions) {
			JSONObject termJson = JSONFactoryUtil.createJSONObject();
			termJson.put(TERM, suggestion);
			jsonArray.put(termJson);
		}

		return jsonArray;
	}

}
