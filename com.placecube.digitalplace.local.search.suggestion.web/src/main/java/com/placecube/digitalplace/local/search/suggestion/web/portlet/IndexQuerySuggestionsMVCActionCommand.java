package com.placecube.digitalplace.local.search.suggestion.web.portlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collection;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.suggest.SpellCheckIndexWriter;
import com.liferay.portal.kernel.search.suggest.SuggestionConstants;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.search.suggestion.web.constants.PortletKeys;
import com.placecube.digitalplace.local.search.suggestion.web.service.SearchSuggestionObjectFactory;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SUGGESTION_PORTLET, "mvc.command.name=/index-suggestions" }, service = MVCActionCommand.class)
public class IndexQuerySuggestionsMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(IndexQuerySuggestionsMVCActionCommand.class);

	@Reference
	private Portal portal;

	@Reference
	private SearchSuggestionObjectFactory searchSuggestionObjectFactory;

	@Reference
	private SpellCheckIndexWriter spellCheckIndexWriter;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		UploadPortletRequest uploadPortletRequest = portal.getUploadPortletRequest(actionRequest);

		File importFile = uploadPortletRequest.getFile("importFile");

		try {

			InputStream inputStream = searchSuggestionObjectFactory.getFileInputStream(importFile);

			Collection<String> lines = searchSuggestionObjectFactory.getLinesFromStream(inputStream);

			for (String line : lines) {

				SearchContext searchContext = SearchContextFactory.getInstance(portal.getHttpServletRequest(actionRequest));
				searchContext.setKeywords(line);

				indexKeyword(searchContext);
			}

		} catch (FileNotFoundException e) {
			LOG.error(e);
			hideDefaultErrorMessage(actionRequest);
			SessionErrors.add(actionRequest, FileNotFoundException.class);
		}

	}

	private void indexKeyword(SearchContext searchContext) {
		try {
			spellCheckIndexWriter.indexKeyword(searchContext, 0.0f, SuggestionConstants.TYPE_QUERY_SUGGESTION);
			LOG.debug(String.format("Query suggestion indexed successfully. Keyword[companyId: %s, groupId: %s, languageId: %s, keywordSearch: %s]", searchContext.getCompanyId(),
					searchContext.getGroupIds()[0], searchContext.getLanguageId(), searchContext.getKeywords()));
		} catch (SearchException se) {
			LOG.error("Unable to index the query suggestion: " + searchContext.getKeywords(), se);
		}

	}

}