package com.placecube.digitalplace.local.search.suggestion.web.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.suggest.Suggester.StringDistance;
import com.liferay.portal.kernel.search.suggest.Suggester.SuggestMode;
import com.liferay.portal.kernel.search.suggest.SuggesterResult;
import com.liferay.portal.kernel.search.suggest.SuggesterResult.Entry;
import com.liferay.portal.kernel.search.suggest.SuggesterResult.Entry.Option;
import com.liferay.portal.kernel.search.suggest.SuggesterResults;
import com.liferay.portal.kernel.search.suggest.TermSuggester;
import com.liferay.portal.kernel.security.RandomUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.placecube.digitalplace.local.search.suggestion.web.configuration.SearchSuggestionCompanyConfiguration;

@Component(immediate = true, service = TermSuggestionService.class)
public class TermSuggestionService {

	private static final Log LOG = LogFactoryUtil.getLog(TermSuggestionService.class);

	public TermSuggester createTermSuggester(SearchContext searchContext, SearchSuggestionCompanyConfiguration configuration) {

		String field = LocalizationUtil.getLocalization().getLocalizedName(configuration.field(), searchContext.getLanguageId());

		TermSuggester termSuggester = new TermSuggester("termSuggester" + RandomUtil.nextInt(1000), field, searchContext.getKeywords());
		termSuggester.setAccuracy(0.5f);
		termSuggester.setStringDistance(StringDistance.JAROWINKLER);
		termSuggester.setSuggestMode(SuggestMode.POPULAR);
		termSuggester.setMinWordLength(1);
		LOG.debug("TermSuggester[keywords: " + searchContext.getKeywords() + ", field: " + termSuggester.getField() + ", accuracy: " + termSuggester.getAccuracy() + ", stringDistance: "
				+ termSuggester.getStringDistance() + ", suggestMode: " + termSuggester.getSuggestMode());
		return termSuggester;
	}

	@SuppressWarnings("deprecation")
	public List<String> getSuggestions(SuggesterResults suggesterResults, SearchSuggestionCompanyConfiguration searchSuggestionCompanyConfiguration) {

		List<String> suggestions = new ArrayList<>();
		List<String> stopWords = Arrays.asList(searchSuggestionCompanyConfiguration.stopWords().split(StringPool.COMMA));
		if (suggesterResults != null) {

			for (SuggesterResult suggesterResult : suggesterResults.getSuggesterResults()) {

				for (Entry entry : suggesterResult.getEntries()) {

					for (Option option : entry.getOptions()) {

						if (!suggestions.contains(option.getText()) && !stopWords.contains(option.getText())) {
							suggestions.add(option.getText());
						}
					}
				}
			}
		}

		return suggestions;
	}

}
