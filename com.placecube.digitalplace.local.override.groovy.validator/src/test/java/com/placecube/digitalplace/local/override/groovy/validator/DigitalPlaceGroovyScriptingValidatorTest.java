package com.placecube.digitalplace.local.override.groovy.validator;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.scripting.ScriptingException;
import com.liferay.portal.kernel.scripting.ScriptingValidator;
import com.placecube.digitalplace.local.override.groovy.validator.service.GroovyValidatorService;

@RunWith(MockitoJUnitRunner.class)
public class DigitalPlaceGroovyScriptingValidatorTest {

	private static final String SCRIPT = "groovy.script";

	@InjectMocks
	private DigitalPlaceGroovyScriptingValidator digitalPlaceGroovyScriptingValidator;

	@Mock
	private ScriptingValidator mockGroovyScriptingValidator;

	@Mock
	private GroovyValidatorService mockGroovyValidatorService;

	@Test
	public void validate_WhenNoErrors_ThenRunsCustomValidationAndExecutesDefaultValidationInOrder() throws Exception {
		digitalPlaceGroovyScriptingValidator.validate(SCRIPT);

		InOrder inOrder = inOrder(mockGroovyValidatorService, mockGroovyScriptingValidator);

		inOrder.verify(mockGroovyValidatorService, times(1)).validate(SCRIPT);
		inOrder.verify(mockGroovyScriptingValidator, times(1)).validate(SCRIPT);
	}

	@Test(expected = ScriptingException.class)
	public void validate_WhenCustomValidationFails_ThenExecutesDefaultValidationAndThrowsScriptingException() throws Exception {
		doThrow(new RuntimeException()).when(mockGroovyValidatorService).validate(SCRIPT);

		digitalPlaceGroovyScriptingValidator.validate(SCRIPT);

		verify(mockGroovyScriptingValidator, times(1)).validate(SCRIPT);
	}

	@Test
	public void getLanguage_WhenNoErrors_ThenReturnsGroovyLanguage() {
		assertThat(digitalPlaceGroovyScriptingValidator.getLanguage(), equalTo("groovy"));
	}
}
