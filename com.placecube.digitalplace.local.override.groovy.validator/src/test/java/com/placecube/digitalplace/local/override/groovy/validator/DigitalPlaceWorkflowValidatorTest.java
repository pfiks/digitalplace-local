package com.placecube.digitalplace.local.override.groovy.validator;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.workflow.WorkflowException;
import com.liferay.portal.workflow.kaleo.definition.Action;
import com.liferay.portal.workflow.kaleo.definition.ActionType;
import com.liferay.portal.workflow.kaleo.definition.Definition;
import com.liferay.portal.workflow.kaleo.definition.Node;
import com.liferay.portal.workflow.kaleo.definition.ScriptAction;
import com.liferay.portal.workflow.kaleo.definition.parser.WorkflowValidator;
import com.placecube.digitalplace.local.override.groovy.validator.service.GroovyValidatorService;

@RunWith(MockitoJUnitRunner.class)
public class DigitalPlaceWorkflowValidatorTest {

	private static final String SCRIPT = "groovy.script";

	@InjectMocks
	private DigitalPlaceWorkflowValidator digitalPlaceWorkflowValidator;

	@Mock
	private WorkflowValidator mockDefaultWorkflowValidator;

	@Mock
	private GroovyValidatorService mockGroovyValidatorService;

	@Mock
	private Definition mockDefinition;

	@Mock
	private Node mockNode;

	@Mock
	private Action mockAction;

	@Mock
	private ScriptAction mockScriptAction;

	@Test
	public void validate_WhenActionInWorkflowIsScript_ThenValidatesScriptAndValidatesWorkflowDefinitionInOrder() throws Exception {
		when(mockDefinition.getNodes()).thenReturn(Arrays.asList(mockNode));
		when(mockNode.getActions()).thenReturn(new HashSet<>(Arrays.asList(mockScriptAction)));
		when(mockScriptAction.getActionType()).thenReturn(ActionType.SCRIPT);
		when(mockScriptAction.getScript()).thenReturn(SCRIPT);

		digitalPlaceWorkflowValidator.validate(mockDefinition);

		InOrder inOrder = inOrder(mockGroovyValidatorService, mockDefaultWorkflowValidator);

		inOrder.verify(mockGroovyValidatorService, times(1)).validate(SCRIPT);
		inOrder.verify(mockDefaultWorkflowValidator, times(1)).validate(mockDefinition);
	}

	@Test(expected = WorkflowException.class)
	public void validate_WhenScriptFailsToBeValidated_ThenThrowsWorkflowException() throws Exception {
		when(mockDefinition.getNodes()).thenReturn(Arrays.asList(mockNode));
		when(mockNode.getActions()).thenReturn(new HashSet<>(Arrays.asList(mockScriptAction)));
		when(mockScriptAction.getActionType()).thenReturn(ActionType.SCRIPT);
		when(mockScriptAction.getScript()).thenReturn(SCRIPT);
		doThrow(new RuntimeException()).when(mockGroovyValidatorService).validate(SCRIPT);

		digitalPlaceWorkflowValidator.validate(mockDefinition);
	}

	@Test
	public void validate_WhenActionInWorkflowIsNotScript_ThenDoesNotValidatesScriptAndvalidatesWorkflowDefinition() throws Exception {
		when(mockDefinition.getNodes()).thenReturn(Arrays.asList(mockNode));
		when(mockNode.getActions()).thenReturn(new HashSet<>(Arrays.asList(mockScriptAction)));
		when(mockScriptAction.getActionType()).thenReturn(ActionType.UPDATE_STATUS);

		digitalPlaceWorkflowValidator.validate(mockDefinition);

		verify(mockGroovyValidatorService, never()).validate(anyString());
		verify(mockDefaultWorkflowValidator, times(1)).validate(mockDefinition);
	}

}
