package com.placecube.digitalplace.local.override.groovy.validator.service;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.InputStream;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;

import com.placecube.digitalplace.local.override.groovy.validator.configuration.GroovyValidatorConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ConfigurationProviderUtil.class})
public class GroovyValidatorServiceTest {

	@InjectMocks
	private GroovyValidatorService groovyValidatorService;

	@Mock
	private GroovyValidatorConfiguration mockGroovyValidatorConfiguration;

	private static final String SET_COMPANY_ID_BLACKLIST_REGEX = ".*CompanyThreadLocal\\.setCompanyId\\(.*\\).*";

	private static final String TEST_SCRIPT = "  import com.liferay.portal.kernel.service.*  " + System.lineSeparator() + System.lineSeparator() + //
			"      CompanyLocalServiceUtil.getCompanies(QueryUtil.ALL_POS, QueryUtil.ALL_POS) ";

	private static final String[] REGEX_LINES = { //
			"import com\\.liferay\\.portal\\.kernel\\.service\\.\\*", //
			"CompanyLocalServiceUtil\\.getCompanies\\(QueryUtil.ALL_POS, QueryUtil.ALL_POS\\)" //
	};

	@Before
	public void activeSetup(){
		PowerMockito.mockStatic(ConfigurationProviderUtil.class);
	}

	@Test
	public void validate_WhenTypeIsWhitelistAndScriptHasWhitelistedLines_ThenDoesNotThrowException() {
		try {
			when(ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class)).thenReturn(mockGroovyValidatorConfiguration);
			when(mockGroovyValidatorConfiguration.enabled()).thenReturn(true);
			when(mockGroovyValidatorConfiguration.whitelistMode()).thenReturn(true);
			when(mockGroovyValidatorConfiguration.linesRegexes()).thenReturn(REGEX_LINES);

			groovyValidatorService.validate(TEST_SCRIPT);

		} catch (Exception e) {
			fail("Unexpected exception thrown");
		}
	}

	@Test(expected = RuntimeException.class)
	public void validate_WhenTypeIsWhitelistAndScriptHasNonWhitelistedLines_ThenThrowsException() throws Exception {
		when(ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class)).thenReturn(mockGroovyValidatorConfiguration);
		when(mockGroovyValidatorConfiguration.enabled()).thenReturn(true);
		when(mockGroovyValidatorConfiguration.whitelistMode()).thenReturn(true);
		when(mockGroovyValidatorConfiguration.linesRegexes()).thenReturn(REGEX_LINES);

		groovyValidatorService.validate(TEST_SCRIPT + System.lineSeparator() + "unexpected line");
	}

	@Test
	public void validate_WhenTypeIsBlacklistAndScriptDoesNotHaveBlacklistedLines_ThenDoesNotThrowException() {
		try {
			when(ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class)).thenReturn(mockGroovyValidatorConfiguration);
			when(mockGroovyValidatorConfiguration.enabled()).thenReturn(true);
			when(mockGroovyValidatorConfiguration.whitelistMode()).thenReturn(false);
			when(mockGroovyValidatorConfiguration.linesRegexes()).thenReturn(new String[] { SET_COMPANY_ID_BLACKLIST_REGEX });

			groovyValidatorService.validate(TEST_SCRIPT);

		} catch (Exception e) {
			fail("Unexpected exception thrown");
		}
	}

	@Test(expected = RuntimeException.class)
	public void validate_WhenTypeIsBlacklistAndScriptHasBlacklistedLines_ThenThrowsException() throws Exception {
		when(ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class)).thenReturn(mockGroovyValidatorConfiguration);
		when(mockGroovyValidatorConfiguration.enabled()).thenReturn(true);
		when(mockGroovyValidatorConfiguration.whitelistMode()).thenReturn(false);
		when(mockGroovyValidatorConfiguration.linesRegexes()).thenReturn(new String[] { SET_COMPANY_ID_BLACKLIST_REGEX });

		groovyValidatorService.validate(TEST_SCRIPT + System.lineSeparator() + "CompanyThreadLocal.setCompanyId(companyId)");
	}

	@Test(expected = RuntimeException.class)
	public void validate_WhenConfigurationRetrievalFails_ThenThrowsRuntimeException() throws Exception {
		when(ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class)).thenThrow(new ConfigurationException());

		groovyValidatorService.validate(TEST_SCRIPT);
	}

	@Test(expected = RuntimeException.class)
	@Parameters({ "set_company_id_script.groovy", "set_company_id_spread_script.groovy", "set_company_id_as_class_script.groovy", "set_company_id_meta_method_script.groovy",
			"set_company_id_class_for_name.groovy", "set_company_id_encoded_strings.groovy" })
	public void validate_WhenBlacklistIsEnabledAndScriptContainsReflectionDirective_ThenThrowsRuntimeException(String fileName) throws Exception {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("scripts/groovy/" + fileName);

		if (inputStream != null) {
			String script = new String(inputStream.readAllBytes());

			when(ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class)).thenReturn(mockGroovyValidatorConfiguration);
			when(mockGroovyValidatorConfiguration.enabled()).thenReturn(true);
			when(mockGroovyValidatorConfiguration.whitelistMode()).thenReturn(false);
			when(mockGroovyValidatorConfiguration.linesRegexes()).thenReturn(new String[] { SET_COMPANY_ID_BLACKLIST_REGEX });

			groovyValidatorService.validate(script);
		} else {
			fail("File not found");
		}
	}
}