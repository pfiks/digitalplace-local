try{
	def className = "com.liferay.portal.kernel.security.auth.Company" + "Thread" + "Local";  
	("$className" as Class).setCompanyId(otherCompanyId);
	
	user = UserLocalServiceUtil.getUser(userId);
	out.println("TEST 4 USER: " + user.getFullName());
} catch (Exception e) {
	out.println("TEST 4 ERROR: " + e.getMessage());
} finally {
	CompanyThreadLocal.setCompanyId(currentCompanyId);
	out.println();
}