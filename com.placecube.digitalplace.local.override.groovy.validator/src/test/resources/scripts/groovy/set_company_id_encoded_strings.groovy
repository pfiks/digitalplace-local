import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

def currentCompanyId = 20097l;
def otherCompanyId = 49105l;
def userId = 486438l; // This user only exists in the other instance

def classNameEncoded = "Y29tLmxpZmVyYXkucG9ydGFsLmtlcm5lbC5zZWN1cml0eS5hdXRoLkNvbXBhbnlUaHJlYWRMb2NhbA==";
def methodEncoded = "c2V0Q29tcGFueUlk";

try{
	def classNameDecoded = new String(classNameEncoded.decodeBase64());
	def methodDecoded = new String(methodEncoded.decodeBase64());

	("$classNameDecoded" as Class)."$methodDecoded"(otherCompanyId);
	
	user = UserLocalServiceUtil.getUser(userId);
	out.println("TEST USER: " + user.getFullName());
} catch (Exception e) {
	out.println("TEST ERROR: " + e.getMessage());
} finally {
	CompanyThreadLocal.setCompanyId(currentCompanyId);
}