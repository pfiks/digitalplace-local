try{
	def companyThreadLocalClass = Class.forName("com.liferay.portal.kernel.security.auth.CompanyThreadLocal");
	companyThreadLocalClass.setCompanyId(otherCompanyId);
	
	user = UserLocalServiceUtil.getUser(userId);
	out.println("TEST 6 USER: " + user.getFullName());
} catch (Exception e) {
	out.println("TEST 6 ERROR: " + e.getMessage());
	e.printStackTrace();
} finally {
	CompanyThreadLocal.setCompanyId(currentCompanyId);
	out.println();
}