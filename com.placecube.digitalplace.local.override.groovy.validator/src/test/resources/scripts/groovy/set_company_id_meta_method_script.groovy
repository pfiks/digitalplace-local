try{
	def setCompanyIdMethod = CompanyThreadLocal.metaClass.getMetaMethod("setCompanyId", Long)
	setCompanyIdMethod.invoke(null, otherCompanyId)	
	
	user = UserLocalServiceUtil.getUser(userId);
	out.println("TEST 5 USER: " + user.getFullName());
} catch (Exception e) {
	out.println("TEST 5 ERROR: " + e.getMessage());
	e.printStackTrace();
} finally {
	CompanyThreadLocal.setCompanyId(currentCompanyId);
	out.println();
}