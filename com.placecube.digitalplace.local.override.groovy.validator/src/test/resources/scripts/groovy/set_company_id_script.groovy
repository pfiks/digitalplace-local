try{
	CompanyThreadLocal.setCompanyId(otherCompanyId);
	
	user = UserLocalServiceUtil.getUser(userId);
	out.println("TEST 2 USER: " + user.getFullName());
} catch (Exception e) {
	out.println("TEST 2 ERROR: " + e.getMessage());
} finally {
	CompanyThreadLocal.setCompanyId(currentCompanyId);
	out.println();
}