try{
	def methodName = "set" + "Company" + "Id";  
	CompanyThreadLocal."$methodName"(otherCompanyId);
	
	user = UserLocalServiceUtil.getUser(userId);
	out.println("TEST 3 USER: " + user.getFullName());
} catch (Exception e) {
	out.println("TEST 3 ERROR: " + e.getMessage());
} finally {
	CompanyThreadLocal.setCompanyId(currentCompanyId);
	out.println();
}