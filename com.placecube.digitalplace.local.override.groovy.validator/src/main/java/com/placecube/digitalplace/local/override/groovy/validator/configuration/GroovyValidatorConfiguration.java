package com.placecube.digitalplace.local.override.groovy.validator.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "workflow", scope = ExtendedObjectClassDefinition.Scope.SYSTEM)
@Meta.OCD(id = "com.placecube.digitalplace.local.override.groovy.validator.configuration.GroovyValidatorConfiguration", localization = "content/Language", name = "groovy-validator")
public interface GroovyValidatorConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "true", name = "whitelist-mode", description = "whitelist-mode-description")
	boolean whitelistMode();

	@Meta.AD(required = false, deflt = "", name = "line-regex", description = "line-regex-description")
	String[] linesRegexes();

}
