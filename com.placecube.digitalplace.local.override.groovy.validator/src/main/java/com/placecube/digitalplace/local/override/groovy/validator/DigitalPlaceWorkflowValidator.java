package com.placecube.digitalplace.local.override.groovy.validator;

import com.liferay.portal.kernel.workflow.WorkflowException;
import com.liferay.portal.workflow.kaleo.definition.Action;
import com.liferay.portal.workflow.kaleo.definition.ActionType;
import com.liferay.portal.workflow.kaleo.definition.Definition;
import com.liferay.portal.workflow.kaleo.definition.Node;
import com.liferay.portal.workflow.kaleo.definition.ScriptAction;
import com.liferay.portal.workflow.kaleo.definition.parser.WorkflowValidator;

import com.placecube.digitalplace.local.override.groovy.validator.service.GroovyValidatorService;

import java.util.Collection;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = "service.ranking:Integer=100", service = WorkflowValidator.class)
public class DigitalPlaceWorkflowValidator implements WorkflowValidator {

	@Reference(
		target = "(component.name=com.liferay.portal.workflow.kaleo.definition.internal.parser.XMLWorkflowValidator)"
	)
	private WorkflowValidator defaultWorkflowValidator;

	@Reference
	private GroovyValidatorService groovyValidatorService;

	@Override
	public void validate(Definition definition) throws WorkflowException {

		Collection<Node> nodes = definition.getNodes();

		for (Node node : nodes) {

			Set<Action> actions = node.getActions();

			for (Action action: actions) {
				if (action.getActionType() == ActionType.SCRIPT) {
					try {
						String script = ((ScriptAction)action).getScript();
						groovyValidatorService.validate(script);
					} catch (Exception e) {
						throw new WorkflowException(e);
					}
				}
			}
		}

		defaultWorkflowValidator.validate(definition);
	}

}
