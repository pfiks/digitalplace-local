package com.placecube.digitalplace.local.override.groovy.validator;

import com.liferay.portal.kernel.scripting.ScriptingException;
import com.liferay.portal.kernel.scripting.ScriptingValidator;

import com.placecube.digitalplace.local.override.groovy.validator.service.GroovyValidatorService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
	property = {"scripting.language=" + DigitalPlaceGroovyScriptingValidator.LANGUAGE, "service.ranking:Integer=100"},
	service = ScriptingValidator.class
)
public class DigitalPlaceGroovyScriptingValidator implements ScriptingValidator {

	@Reference(
		target = "(component.name=com.liferay.portal.scripting.groovy.internal.GroovyScriptingValidator)"
	)
	private ScriptingValidator groovyScriptingValidator;

	@Reference
	private GroovyValidatorService groovyValidatorService;

	public static final String LANGUAGE = "groovy";

	@Override
	public String getLanguage() {
		return LANGUAGE;
	}

	@Override
	public void validate(String script) throws ScriptingException {
		try {
			groovyValidatorService.validate(script);
		}
		catch (Exception e) {
			throw new ScriptingException(e);
		} finally {
			groovyScriptingValidator.validate(script);
		}
	}

}
