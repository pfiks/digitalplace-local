package com.placecube.digitalplace.local.override.groovy.validator.service;

import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.placecube.digitalplace.local.override.groovy.validator.configuration.GroovyValidatorConfiguration;

@Component(immediate = true, property = {}, service = GroovyValidatorService.class)
public class GroovyValidatorService {

	public void validate(String script) throws RuntimeException {

		try {
			GroovyValidatorConfiguration configuration = ConfigurationProviderUtil.getSystemConfiguration(GroovyValidatorConfiguration.class);
			if (configuration.enabled()) {
				String[] linesRegexes = configuration.linesRegexes();

				Stream<String> lines = script.lines();

				lines.forEach(line -> {
					if (!isLineValid(line, linesRegexes, configuration.whitelistMode())) {
						throw new RuntimeException("Invalid line: " + line);
					}
				});
			}
		} catch (ConfigurationException ce) {
			throw new RuntimeException("Error getting configuration: " + ce);
		}
	}

	private boolean isLineValid(String line, String[] linesRegexes, boolean whitelist) {

		line = line.trim();

		if (line.isEmpty()) {
			return true;
		}

		for (String validLine : linesRegexes) {

			if (line.matches(validLine)) {
				return whitelist;
			}
		}

		return !whitelist;
	}

}
