package com.placecube.digitalplace.local.assetpublisher.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.assetpublisher.configuration.AssetPublisherCompanyConfiguration;

@RunWith(MockitoJUnitRunner.class)
public class AssetPublisherTemplateServiceTest extends PowerMockito {

	private final static long COMPANY_ID = 322L;

	@InjectMocks
	private AssetPublisherTemplateService assetPublisherTemplateService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private AssetPublisherCompanyConfiguration mockAssetPublisherCompanyConfiguration;

	@Test
	public void getIconVocabularyId_WhenNoErrors_ThenReturnsIconVocabularyIdConfiguredAtCompanyLevel() throws Exception {
		final long vocabularyId = 123L;

		when(mockConfigurationProvider.getCompanyConfiguration(AssetPublisherCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockAssetPublisherCompanyConfiguration);
		when(mockAssetPublisherCompanyConfiguration.iconVocabularyId()).thenReturn(vocabularyId);

		long result = assetPublisherTemplateService.getIconVocabularyId(COMPANY_ID);

		assertThat(result, equalTo(vocabularyId));
	}

}
