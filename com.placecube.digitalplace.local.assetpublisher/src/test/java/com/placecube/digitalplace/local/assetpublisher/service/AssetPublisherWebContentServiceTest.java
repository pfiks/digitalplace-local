package com.placecube.digitalplace.local.assetpublisher.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.assetpublisher.constants.AssetPublisherWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

@RunWith(MockitoJUnitRunner.class)
public class AssetPublisherWebContentServiceTest extends PowerMockito {

	public static final String TEMPLATE_RESOURCE_PATH = "com/placecube/digitalplace/local/assetpublisher/widgettemplate/" + AssetPublisherWidgetTemplate.CONTENT_LINK_PANELS.getKey() + ".ftl";

	@InjectMocks
	private AssetPublisherWebContentService assetPublisherWebContentService;

	private ClassLoader assetPublisherWebContentServiceClassLoader;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		assetPublisherWebContentServiceClassLoader = AssetPublisherWebContentService.class.getClassLoader();
	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenException_ThenThrowsPortalException() throws Exception {

		AssetPublisherWidgetTemplate widgetTemplate = AssetPublisherWidgetTemplate.CONTENT_LINK_PANELS;
		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, TEMPLATE_RESOURCE_PATH,
				assetPublisherWebContentServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		assetPublisherWebContentService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		AssetPublisherWidgetTemplate widgetTemplate = AssetPublisherWidgetTemplate.CONTENT_LINK_PANELS;
		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, TEMPLATE_RESOURCE_PATH,
				assetPublisherWebContentServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = assetPublisherWebContentService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

}
