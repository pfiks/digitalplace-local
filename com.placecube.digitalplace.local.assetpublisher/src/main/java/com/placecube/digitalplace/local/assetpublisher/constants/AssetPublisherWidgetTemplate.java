package com.placecube.digitalplace.local.assetpublisher.constants;

public enum AssetPublisherWidgetTemplate {

	CONTENT_LINK_PANELS("ASSET-PUBLISHER-CONTENT-LINK-PANELS", "Content Link Panels");

	private final String key;
	private final String name;

	private AssetPublisherWidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
