package com.placecube.digitalplace.local.assetpublisher.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "assets", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.assetpublisher.configuration.AssetPublisherCompanyConfiguration", localization = "content/Language", name = "asset-publisher")
public interface AssetPublisherCompanyConfiguration {

	@Meta.AD(required = false, deflt = "0", name = "icon-vocabulary-id")
	long iconVocabularyId();

}