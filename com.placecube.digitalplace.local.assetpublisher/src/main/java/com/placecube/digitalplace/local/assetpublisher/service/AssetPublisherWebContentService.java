package com.placecube.digitalplace.local.assetpublisher.service;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.assetpublisher.constants.AssetPublisherWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = AssetPublisherWebContentService.class)
public class AssetPublisherWebContentService {

	@Reference
	private DDMInitializer ddmInitializer;

	public DDMTemplate getOrCreateWidgetTemplate(AssetPublisherWidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {

		String templateResourcePath = "com/placecube/digitalplace/local/assetpublisher/widgettemplate/" + widgetTemplate.getKey() + ".ftl";
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath,
				getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);

	}

}
