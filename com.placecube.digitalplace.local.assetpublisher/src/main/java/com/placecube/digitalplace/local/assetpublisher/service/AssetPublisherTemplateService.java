package com.placecube.digitalplace.local.assetpublisher.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.local.assetpublisher.configuration.AssetPublisherCompanyConfiguration;

@Component(immediate = true, service = AssetPublisherTemplateService.class)
public class AssetPublisherTemplateService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public long getIconVocabularyId(long companyId) throws PortalException {

		AssetPublisherCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(AssetPublisherCompanyConfiguration.class, companyId);

		return configuration.iconVocabularyId();

	}
}
