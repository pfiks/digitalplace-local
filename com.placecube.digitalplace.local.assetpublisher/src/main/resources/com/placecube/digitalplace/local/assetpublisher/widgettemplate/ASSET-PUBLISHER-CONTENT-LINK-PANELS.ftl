<#if serviceLocator?? && entries?has_content>
	<div class="nav-menu-link-panels-navigation-widget">
		<div class="row">
			
			<#assign journalArticleRetrievalService = digitalplace_journalArticleRetrievalService />
			<#assign journalArticleService = serviceLocator.findService("com.liferay.journal.service.JournalArticleService") />

			<#list entries as curEntry>
			
				<#assign journalArticle = journalArticleService.getLatestArticle(curEntry.getClassPK()) />
				<#assign articleURLOptional = journalArticleRetrievalService.getDisplayURL(themeDisplay, journalArticle, locale) />

				<#if articleURLOptional.isPresent()>
					<#assign articleURL = articleURLOptional.get() />
				<#else>
					<#assign articleURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
				</#if>

				<div class="col-md-4 nav-item-entry">
					<h2 class="page-title">
						<a href="${articleURL}">
							${curEntry.getTitle(locale)}
						</a>
					</h2>

					<#assign articleSummary = journalArticle.getDescription(locale) />
					<#if articleSummary?has_content>
						<p class="page-description">
						 	${articleSummary}
						</p>
					</#if>
					
				</div>
				
			</#list>
			
		</div>	
	</div>
</#if>