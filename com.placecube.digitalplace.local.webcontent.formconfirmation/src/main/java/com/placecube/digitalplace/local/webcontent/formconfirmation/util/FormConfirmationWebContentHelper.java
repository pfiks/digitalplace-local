package com.placecube.digitalplace.local.webcontent.formconfirmation.util;

import java.util.Collections;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.formconfirmation.constants.FormConfirmationConstants;
import com.placecube.digitalplace.local.webcontent.formconfirmation.constants.FormConfirmationDDMTemplate;

@Component(immediate = true, service = FormConfirmationWebContentHelper.class)
public class FormConfirmationWebContentHelper {

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	private Portal portal;

	public DDMStructure addDDMStructure(ServiceContext serviceContext) throws Exception {

		defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), getClass().getClassLoader(),
				"com/placecube/digitalplace/local/webcontent/formconfirmation/dependencies/ddm/structure.xml", serviceContext);

		return getDDMStructure(serviceContext);
	}

	public DDMStructure getDDMStructure(ServiceContext serviceContext) throws PortalException {
		return ddmStructureLocalService.getStructure(serviceContext.getScopeGroupId(), portal.getClassNameId(JournalArticle.class), FormConfirmationConstants.STRUCTURE_KEY);
	}

	public DDMTemplate getDDMTemplate(ServiceContext serviceContext, String ddmTemplateKey) {
		return ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), ddmTemplateKey);
	}

	public void getOrCreateDDMTemplate(String ddmTemplateName, String ddmTemplateKey, DDMStructure formConfirmationStructure, ServiceContext serviceContext) throws PortalException {

		DDMTemplate ddmTemplate = getDDMTemplate(serviceContext, ddmTemplateKey);

		if (ddmTemplate != null) {
			return;
		}

		try {

			String script = StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/webcontent/formconfirmation/dependencies/ddm/" + FormConfirmationDDMTemplate.FORM_CONFIRMATION.getKey() + ".ftl");

			ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), serviceContext.getScopeGroupId(), portal.getClassNameId(DDMStructure.class), formConfirmationStructure.getStructureId(),
					formConfirmationStructure.getClassNameId(), ddmTemplateKey, Collections.singletonMap(serviceContext.getLocale(), ddmTemplateName), null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY,
					DDMTemplateConstants.TEMPLATE_MODE_CREATE, TemplateConstants.LANG_TYPE_FTL, script, false, false, StringPool.BLANK, null, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public String normalizeString(String string) {
		String regex = " +";
		String stringNoSpace = string.trim().replaceAll(regex, StringPool.SPACE);
		String stringCaps = stringNoSpace.toUpperCase();
		return stringCaps.replace(StringPool.SPACE, StringPool.DASH);
	}

}
