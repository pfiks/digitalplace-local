package com.placecube.digitalplace.local.webcontent.formconfirmation.service;

import java.io.IOException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.webcontent.formconfirmation.constants.FormConfirmationDDMTemplate;
import com.placecube.digitalplace.local.webcontent.formconfirmation.util.FormConfirmationWebContentHelper;

@Component(immediate = true, service = FormConfirmationWebContentService.class)
public class FormConfirmationWebContentService {

	@Reference
	private FormConfirmationWebContentHelper formConfirmationWebContentHelper;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {

		try {
			DDMStructure ddmStructure = formConfirmationWebContentHelper.addDDMStructure(serviceContext);

			formConfirmationWebContentHelper.getOrCreateDDMTemplate(FormConfirmationDDMTemplate.FORM_CONFIRMATION.getName(), FormConfirmationDDMTemplate.FORM_CONFIRMATION.getKey(), ddmStructure, serviceContext);

			return ddmStructure;

		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public void getOrCreateDDMStructure(ServiceContext serviceContext, String prefixTemplateName) throws PortalException {

		try {

			DDMStructure formConfirmationStructure = formConfirmationWebContentHelper.addDDMStructure(serviceContext);

			String ddmTemplateName = Validator.isNull(prefixTemplateName) ? FormConfirmationDDMTemplate.FORM_CONFIRMATION.getName() : prefixTemplateName + StringPool.SPACE + FormConfirmationDDMTemplate.FORM_CONFIRMATION.getName();
			String ddmTemplateKey = formConfirmationWebContentHelper.normalizeString(ddmTemplateName);

			formConfirmationWebContentHelper.getOrCreateDDMTemplate(ddmTemplateName, ddmTemplateKey, formConfirmationStructure, serviceContext);

		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public String getDDMTemplateScript() throws IOException {
		String templateResourcePath = getTemplateResourcePath();
		return StringUtil.read(getClass().getClassLoader(), templateResourcePath);
	}

	private String getTemplateResourcePath() {
		return "com/placecube/digitalplace/local/webcontent/formconfirmation/dependencies/ddm/FORM-CONFIRMATION.ftl";
	}

}
