package com.placecube.digitalplace.local.webcontent.formconfirmation.constants;

public enum FormConfirmationDDMTemplate {

	FORM_CONFIRMATION("FORM-CONFIRMATION", "Form Confirmation");

	private final String key;
	private final String name;

	private FormConfirmationDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
