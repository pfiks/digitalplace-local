package com.placecube.digitalplace.local.webcontent.formconfirmation.upgrade;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.webcontent.formconfirmation.upgrade.upgrade_1_0_0.FormConfirmationWebContentTemplateUpgrade;
import com.placecube.digitalplace.local.webcontent.formconfirmation.upgrade.upgrade_1_0_1.FormConfirmationWebContentTemplateStaticUtilUpgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class FormConfirmationUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new FormConfirmationWebContentTemplateUpgrade(ddmTemplateLocalService, companyLocalService));
		registry.register("1.0.0", "1.0.1", new FormConfirmationWebContentTemplateStaticUtilUpgrade(ddmTemplateLocalService, companyLocalService));
	}
}