package com.placecube.digitalplace.local.webcontent.formconfirmation.constants;

public final class FormConfirmationConstants {

	public static final String STRUCTURE_KEY = "FORM CONFIRMATION";

	private FormConfirmationConstants() {

	}

}
