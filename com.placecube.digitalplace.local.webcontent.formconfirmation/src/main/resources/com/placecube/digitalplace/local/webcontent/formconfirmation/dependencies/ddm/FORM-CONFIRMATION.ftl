<#assign ddmFormFieldTypeUtil = digitalplace_ddmFormFieldTypeUtil />
<#assign httpServletRequest = digitalplace_serviceContext.getRequest()/>

<#if httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webddmFormInstanceRecord")?has_content>
    <#assign formInstanceRecordId = httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webformInstanceRecordId") />
    <#assign ddmFormValues = httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webddmFormInstanceRecord").getDDMFormValues() />
    <#assign ddmFormFieldValues = ddmFormValues.getDDMFormFieldValues() />
<#else>
    <#assign formInstanceRecordId = getterUtil.getLong(httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webformInstanceRecordId"), 0) />
    <#if (formInstanceRecordId > 0  ) >
        <#assign ddmFormInstanceRecordLocalService = serviceLocator.findService("com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService") />
	    <#assign ddmFormInstanceRecord = ddmFormInstanceRecordLocalService.fetchFormInstanceRecord(formInstanceRecordId) />
	    <#assign ddmFormValues = ddmFormInstanceRecord.getDDMFormValues() />
	    <#assign ddmFormFieldValues = ddmFormValues.getDDMFormFieldValues() />
    </#if>
</#if>

<#if (ddmFormFieldValues)?has_content>
    <#assign formFieldReferencesValueMap = ddmFormFieldTypeUtil.getFormFieldReferenceValueMap(ddmFormValues) />
    <#assign paymentReferenceId = getterUtil.getString(httpServletRequest.getSession().getAttribute("equinox.http.defaultpaymentReferenceId"), "") />
    <#assign bodyText = Body.getData()/>
    <#assign bodyText = bodyText?replace("$FORM_PAYMENT_REFERENCE$",paymentReferenceId) />
    <#assign bodyText = bodyText?replace("$FORM_REFERENCE$",formInstanceRecordId) />
    <#list formFieldReferencesValueMap as fieldReference, fieldValue>
    	<#if fieldValue?? >
          <#assign bodyText = bodyText?replace("$FORM_"+fieldReference+"$", fieldValue) />
        </#if>
    </#list>
    ${bodyText}
</#if>