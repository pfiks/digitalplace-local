package com.placecube.digitalplace.local.webcontent.formconfirmation.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.webcontent.formconfirmation.constants.FormConfirmationDDMTemplate;
import com.placecube.digitalplace.local.webcontent.formconfirmation.util.FormConfirmationWebContentHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
public class FormConfirmationWebContentServiceTest extends PowerMockito {

	@Mock
	private FormConfirmationWebContentHelper mockFormConfirmationWebContentHelper;

	@InjectMocks
	private FormConfirmationWebContentService formConfirmationWebContentService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private ServiceContext mockServiceContext;


	@Before
	public void setUp() {
		mockStatic(StringUtil.class);
	}

	@Test
	public  void getOrCreateDDMStructure_WhenNoError_ThenCreateDDMStructure() throws Exception {
		when(mockFormConfirmationWebContentHelper.addDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);

		DDMStructure result = formConfirmationWebContentService.getOrCreateDDMStructure(mockServiceContext);
		assertEquals(result, mockDDMStructure);
		verify(mockFormConfirmationWebContentHelper, times(1)).getOrCreateDDMTemplate(FormConfirmationDDMTemplate.FORM_CONFIRMATION.getName(), FormConfirmationDDMTemplate.FORM_CONFIRMATION.getKey(), mockDDMStructure, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public  void getOrCreateDDMStructure_WhenErrorAddingStructure_ThenThrowPortalException() throws Exception {
		when(mockFormConfirmationWebContentHelper.addDDMStructure(mockServiceContext)).thenThrow(new Exception());
		formConfirmationWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public  void getOrCreateDDMStructure_WhenErrorGettingOrAddingTemplate_ThenThrowPortalException() throws Exception {
		when(mockFormConfirmationWebContentHelper.addDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		doThrow(new PortalException()).when(mockFormConfirmationWebContentHelper, "getOrCreateDDMTemplate", FormConfirmationDDMTemplate.FORM_CONFIRMATION.getName(), FormConfirmationDDMTemplate.FORM_CONFIRMATION.getKey(), mockDDMStructure, mockServiceContext);
		formConfirmationWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public  void getOrCreateDDMStructureWithPrefix_WhenNoError_ThenCreateDDMStructure() throws Exception {
		String ddmTemplateName = "Global Form Confirmation";
		String ddmTemplateKey = "GLOBAL-FORM-CONFIRMATION";
		when(mockFormConfirmationWebContentHelper.addDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockFormConfirmationWebContentHelper.normalizeString(GroupConstants.GLOBAL + StringPool.SPACE + "Form Confirmation")).thenReturn(ddmTemplateKey);

		formConfirmationWebContentService.getOrCreateDDMStructure(mockServiceContext, GroupConstants.GLOBAL);

		verify(mockFormConfirmationWebContentHelper, times(1)).getOrCreateDDMTemplate(ddmTemplateName, ddmTemplateKey, mockDDMStructure, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public  void getOrCreateDDMStructureWithPrefix_WhenErrorGettingOrAddingTemplate_ThenThrowPortalException() throws Exception {
		String ddmTemplateName = "Global Form Confirmation";
		String ddmTemplateKey = "GLOBAL-FORM-CONFIRMATION";
		when(mockFormConfirmationWebContentHelper.addDDMStructure(mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockFormConfirmationWebContentHelper.normalizeString(GroupConstants.GLOBAL + StringPool.SPACE + "Form Confirmation")).thenReturn(ddmTemplateKey);
		doThrow(new PortalException()).when(mockFormConfirmationWebContentHelper, "getOrCreateDDMTemplate", ddmTemplateName, ddmTemplateKey, mockDDMStructure, mockServiceContext);

		formConfirmationWebContentService.getOrCreateDDMStructure(mockServiceContext, GroupConstants.GLOBAL);
	}

	@Test(expected = IOException.class)
	public void getDDMTemplateScript_WhenErrorOnReadingFile_ThenExceptionIsThrown() throws Exception {
		when(StringUtil.read(eq(getClass().getClassLoader()), anyString())).thenThrow(new IOException());

		formConfirmationWebContentService.getDDMTemplateScript();
	}

	@Test
	public void getDDMTemplateScript_WhenNoError_ThenReturnsScriptAsString() throws Exception {
		String templateResourcePath = "com/placecube/digitalplace/local/webcontent/formconfirmation/dependencies/ddm/FORM-CONFIRMATION.ftl";
		String expected = "This script";

		when(StringUtil.read(getClass().getClassLoader(), templateResourcePath)).thenReturn(expected);

		String actual = formConfirmationWebContentService.getDDMTemplateScript();

		assertEquals(expected, actual);
	}
}
