package com.placecube.digitalplace.local.account.mylocal.content.mock.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.local.account.mylocal.content.service.MyLocalContentConnector;

@Component(immediate = true, service = MyLocalContentConnector.class)
public class MockMyLocalContentConnector implements MyLocalContentConnector {

	@Reference
	private MockMyLocalContentRetrievalService mockMyLocalContentRetrievalService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return mockMyLocalContentRetrievalService.isEnabled(companyId);
		} catch (ConfigurationException e) {
			return false;
		}
	}

	@Override
	public String getDateValue(JSONObject jsonObject) {
		return mockMyLocalContentRetrievalService.getDateValue(jsonObject);
	}

	@Override
	public JSONObject getFoodWasteDay(long companyId, String uprn) {
		return mockMyLocalContentRetrievalService.getFoodWasteDay(uprn);
	}

	@Override
	public JSONObject getGardenWasteDay(long companyId, String uprn) {
		return mockMyLocalContentRetrievalService.getGardenWasteDay(uprn);
	}

	@Override
	public JSONObject getRecyclingDay(long companyId, String uprn) {
		return mockMyLocalContentRetrievalService.getRecyclingDay(uprn);
	}

	@Override
	public JSONObject getRefuseDay(long companyId, String uprn) {
		return mockMyLocalContentRetrievalService.getRefuseDay(uprn);
	}

}
