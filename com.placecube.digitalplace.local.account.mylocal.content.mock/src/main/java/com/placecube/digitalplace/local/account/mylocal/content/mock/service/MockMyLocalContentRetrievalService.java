package com.placecube.digitalplace.local.account.mylocal.content.mock.service;

import java.util.Date;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.placecube.digitalplace.local.account.mylocal.content.mock.configuration.MockMyContentConnectorCompanyConfiguration;
import com.placecube.digitalplace.local.account.mylocal.content.mock.constants.MockMyLocalContentKeys;

@Component(immediate = true, service = MockMyLocalContentRetrievalService.class)
public class MockMyLocalContentRetrievalService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	public String getDateValue(JSONObject jsonObject) {
		return jsonObject.getString(MockMyLocalContentKeys.DAY_KEY);
	}

	public JSONObject getFoodWasteDay(String uprn) {
		return getTodayJSONObject();
	}

	public JSONObject getGardenWasteDay(String uprn) {
		return getTodayJSONObject();
	}

	public JSONObject getRecyclingDay(String uprn) {
		return getTodayJSONObject();
	}

	public JSONObject getRefuseDay(String uprn) {
		return getTodayJSONObject();
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		MockMyContentConnectorCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockMyContentConnectorCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

	private JSONObject getTodayJSONObject() {
		return jsonFactory.createJSONObject().put(MockMyLocalContentKeys.DAY_KEY, DateFormatFactoryUtil.getSimpleDateFormat("EEEE dd MMMM yyyy").format(new Date()));
	}

}
