package com.placecube.digitalplace.local.account.mylocal.content.mock.constants;

public final class MockMyLocalContentKeys {

	public static final String DAY_KEY = "day";

	private MockMyLocalContentKeys() {

	}

}
