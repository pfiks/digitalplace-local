package com.placecube.digitalplace.local.account.mylocal.content.mock.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.local.account.mylocal.content.mock.configuration.MockMyContentConnectorCompanyConfiguration", localization = "content/Language", name = "my-content-connector-configuration-mock")
public interface MockMyContentConnectorCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();
}
