<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<liferay-theme:defineObjects />

<liferay-util:buffer var="html">
	<liferay-util:include page="/document_library/info_panel.portal.jsp" servletContext="<%= application %>" />
</liferay-util:buffer>

<%= html %>

<c:if test="${!empty param.folderId}">
	<div class="sidebar-body pt-0">
		<dl class="sidebar-dl sidebar-section">
			<dt class="sidebar-dt">
				Folder ID
			</dt>
			<dd class="sidebar-dd">
				${param.folderId}
			</dd>
		</dl>
	</div>

	<style>
		.info-panel-content .sidebar-body{
			padding-bottom:0px !important;
		}
	</style>
</c:if>
