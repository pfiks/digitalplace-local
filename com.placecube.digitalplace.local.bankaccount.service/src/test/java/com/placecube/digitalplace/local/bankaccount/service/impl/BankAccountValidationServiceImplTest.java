package com.placecube.digitalplace.local.bankaccount.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.placecube.digitalplace.local.bankaccount.constants.BankAccountConnectorConstants;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationResponse;
import com.placecube.digitalplace.local.bankaccount.service.BankAccountConnector;
import com.placecube.digitalplace.local.connector.model.Connector;
import com.placecube.digitalplace.local.connector.model.ConnectorContext;
import com.placecube.digitalplace.local.connector.model.ConnectorRegistryContext;
import com.placecube.digitalplace.local.connector.service.ConnectorRegistryService;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BankAccountValidationServiceImplTest {

	private static final long COMPANY_ID = 232L;
	private static final String ACCOUNT_NUMBER = "2342232";
	private static final String SORT_CODE = "221144";
	private static final String CONNECTOR_NAME = "BankAccountConnector";

	@InjectMocks
	private BankAccountValidationServiceImpl bankAccountValidationServiceImpl;

	@Mock
	private ConnectorRegistryService mockConnectorRegistryService;

	@Mock
	private ConnectorContext mockConnectorContext;

	@Mock
	private ConnectorRegistryContext mockConnectorRegistryContext;

	@Mock
	private BankAccountConnector mockBankAccountConnector;

	@Mock
	private ServiceWrapper<Connector> mockConnectorSeviceWrapper;

	@Test
	public void validateBankAccount_WhenNoErrors_ThenValidatesBankAccountUsingConfiguredConnector() throws Exception {
		when(mockConnectorContext.getConnectorName()).thenReturn(CONNECTOR_NAME);
		when(mockConnectorContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConnectorRegistryService.createConnectorRegistryContext(BankAccountConnectorConstants.BANK_ACCOUNT_CONNECTOR_TYPE, CONNECTOR_NAME)).thenReturn(mockConnectorRegistryContext);

		when(mockConnectorRegistryService.getConnector(mockConnectorRegistryContext)).thenReturn(Optional.of(mockConnectorSeviceWrapper));
		when(mockConnectorSeviceWrapper.getService()).thenReturn(mockBankAccountConnector);
		when(mockBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE)).thenReturn(BankAccountValidationResponse.VALID);

		BankAccountValidationResponse result = bankAccountValidationServiceImpl.validateBankAccount(mockConnectorContext, ACCOUNT_NUMBER, SORT_CODE);

		assertThat(result, equalTo(BankAccountValidationResponse.VALID));
	}

	@Test(expected = BankAccountValidationException.class)
	public void validateBankAccount_WhenBankAccountValidationFails_ThenThrowsBankAccountValidationException() throws Exception {
		when(mockConnectorContext.getConnectorName()).thenReturn(CONNECTOR_NAME);
		when(mockConnectorContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConnectorRegistryService.createConnectorRegistryContext(BankAccountConnectorConstants.BANK_ACCOUNT_CONNECTOR_TYPE, CONNECTOR_NAME)).thenReturn(mockConnectorRegistryContext);

		when(mockConnectorRegistryService.getConnector(mockConnectorRegistryContext)).thenReturn(Optional.of(mockConnectorSeviceWrapper));
		when(mockConnectorSeviceWrapper.getService()).thenReturn(mockBankAccountConnector);
		when(mockBankAccountConnector.validateBankAccount(COMPANY_ID, ACCOUNT_NUMBER, SORT_CODE)).thenThrow(new BankAccountValidationException("error"));

		bankAccountValidationServiceImpl.validateBankAccount(mockConnectorContext, ACCOUNT_NUMBER, SORT_CODE);
	}

	@Test(expected = BankAccountValidationException.class)
	public void validateBankAccount_WhenNoConnectorsAvailable_ThenThrowsBankAccountValidationException() throws Exception {
		when(mockConnectorContext.getConnectorName()).thenReturn(CONNECTOR_NAME);
		when(mockConnectorRegistryService.createConnectorRegistryContext(BankAccountConnectorConstants.BANK_ACCOUNT_CONNECTOR_TYPE, CONNECTOR_NAME)).thenReturn(mockConnectorRegistryContext);

		when(mockConnectorRegistryService.getConnector(mockConnectorRegistryContext)).thenReturn(Optional.empty());

		bankAccountValidationServiceImpl.validateBankAccount(mockConnectorContext, ACCOUNT_NUMBER, SORT_CODE);
	}
}
