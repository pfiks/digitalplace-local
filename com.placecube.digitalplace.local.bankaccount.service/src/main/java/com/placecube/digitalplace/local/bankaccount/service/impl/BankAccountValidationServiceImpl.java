package com.placecube.digitalplace.local.bankaccount.service.impl;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.placecube.digitalplace.local.bankaccount.constants.BankAccountConnectorConstants;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationException;
import com.placecube.digitalplace.local.bankaccount.model.BankAccountValidationResponse;
import com.placecube.digitalplace.local.bankaccount.service.BankAccountConnector;
import com.placecube.digitalplace.local.bankaccount.service.BankAccountValidationService;
import com.placecube.digitalplace.local.connector.model.Connector;
import com.placecube.digitalplace.local.connector.model.ConnectorContext;
import com.placecube.digitalplace.local.connector.model.ConnectorRegistryContext;
import com.placecube.digitalplace.local.connector.service.ConnectorRegistryService;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Ruben Lopez Seone
 */
@Component(immediate = true, service = BankAccountValidationService.class)
public class BankAccountValidationServiceImpl implements BankAccountValidationService {

	@Reference
	private ConnectorRegistryService connectorRegistryService;

	private BankAccountConnector getBankAccountConnector(ConnectorContext connectorContext) throws BankAccountValidationException {

		ConnectorRegistryContext connectorRegistryContext = connectorRegistryService.createConnectorRegistryContext(BankAccountConnectorConstants.BANK_ACCOUNT_CONNECTOR_TYPE,
				connectorContext.getConnectorName());

		Optional<ServiceWrapper<Connector>> optionalCouncilTaxConnectorServiceWrapper = connectorRegistryService.getConnector(connectorRegistryContext);

		ServiceWrapper<Connector> councilTaxConnectorServiceWrapper = optionalCouncilTaxConnectorServiceWrapper
				.orElseThrow(() -> new BankAccountValidationException("No bank account connector configured"));

		return (BankAccountConnector) councilTaxConnectorServiceWrapper.getService();
	}

	@Override
	public BankAccountValidationResponse validateBankAccount(ConnectorContext connectorContext, String accountNumber, String sortCode) throws BankAccountValidationException {
		return getBankAccountConnector(connectorContext).validateBankAccount(connectorContext.getCompanyId(), accountNumber, sortCode);
	}
}