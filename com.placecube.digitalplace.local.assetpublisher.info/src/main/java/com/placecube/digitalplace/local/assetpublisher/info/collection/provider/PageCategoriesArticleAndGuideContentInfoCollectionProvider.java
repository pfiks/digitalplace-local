package com.placecube.digitalplace.local.assetpublisher.info.collection.provider;

import java.util.List;
import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.info.collection.provider.CollectionQuery;
import com.liferay.info.collection.provider.InfoCollectionProvider;
import com.liferay.info.pagination.InfoPage;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService;
import com.placecube.digitalplace.local.assetpublisher.info.constants.MessageKeys;

@Component(immediate = true, service = InfoCollectionProvider.class)
public class PageCategoriesArticleAndGuideContentInfoCollectionProvider implements InfoCollectionProvider<AssetEntry> {

	@Reference
	private InfoCollectionProviderService infoCollectionProviderService;

	@Reference(target = "(bundle.symbolic.name=com.placecube.digitalplace.local.assetpublisher.info)", unbind = "-")
	private ResourceBundleLoader resourceBundleLoader;

	@Override
	public InfoPage<AssetEntry> getCollectionInfoPage(CollectionQuery collectionQuery) {
		List<AssetEntry> infoCollection = infoCollectionProviderService.getInfoCollection();
		return InfoPage.of(infoCollection, collectionQuery.getPagination(), infoCollection.size());
	}

	@Override
	public String getLabel(Locale locale) {
		return resourceBundleLoader.loadResourceBundle(locale).getString(MessageKeys.PAGE_CATEGORIES_ARTICLE_AND_GUIDE_CONTENT);
	}

}
