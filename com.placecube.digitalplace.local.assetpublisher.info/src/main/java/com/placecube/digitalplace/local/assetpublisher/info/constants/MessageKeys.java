package com.placecube.digitalplace.local.assetpublisher.info.constants;

public final class MessageKeys {

	public static final String PAGE_CATEGORIES_ARTICLE_AND_GUIDE_CONTENT = "page-categories-article-and-guide-content";

	private MessageKeys() {

	}
}
