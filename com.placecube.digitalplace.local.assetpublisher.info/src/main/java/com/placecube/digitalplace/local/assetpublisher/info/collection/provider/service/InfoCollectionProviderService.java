package com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.depot.model.DepotEntry;
import com.liferay.depot.service.DepotEntryLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal.AssetEntrySearchService;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal.AssetEntryService;

@Component(immediate = true, service = InfoCollectionProviderService.class)
public class InfoCollectionProviderService {

	private static final Log LOG = LogFactoryUtil.getLog(InfoCollectionProviderService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetEntrySearchService assetEntrySearchService;

	@Reference
	private AssetEntryService assetEntryService;

	@Reference
	private DepotEntryLocalService depotEntryLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	public List<AssetEntry> getInfoCollection() {

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		return getInfoCollection(serviceContext);
	}

	public List<AssetEntry> getInfoCollection(ServiceContext serviceContext) {
		List<AssetCategory> layoutCategories = assetCategoryLocalService.getCategories(Layout.class.getName(), serviceContext.getPlid());

		if (!layoutCategories.isEmpty()) {

			long[] assetCategorysIds = ListUtil.toLongArray(layoutCategories, AssetCategory.CATEGORY_ID_ACCESSOR);

			long groupId = serviceContext.getScopeGroupId();
			long companyId = serviceContext.getCompanyId();

			try {
				List<Long> groupIds = getAssetLibraryGroupIds(groupId);
				groupIds.add(groupId);

				SearchContext searchContext = assetEntrySearchService.createSearchContext(groupIds, companyId, assetCategorysIds);

				List<Long> classPKs = assetEntrySearchService.getEntryClassPKsFromSearchResults(searchContext);

				List<AssetEntry> entries = assetEntryService.getAssetEntriesByClassPKs(groupIds, companyId, classPKs);

				return assetEntryService.sort(entries);

			} catch (SearchException se) {
				LOG.error("Unable to search article and guide content for layout with plid:" + serviceContext.getPlid());
				LOG.debug(se);
			}
		}
		return new ArrayList<>();
	}

	private List<Long> getAssetLibraryGroupIds(long groupId) {
		List<DepotEntry> depotEntries = new ArrayList<>();
		try {
			depotEntries = depotEntryLocalService.getGroupConnectedDepotEntries(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		} catch (PortalException e) {
			LOG.error(e);
		}
		return depotEntries.stream().mapToLong(DepotEntry::getGroupId).boxed().collect(Collectors.toList());
	}
}
