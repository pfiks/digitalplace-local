package com.placecube.digitalplace.local.assetpublisher.info.util;

import java.util.Comparator;

import com.liferay.asset.kernel.model.AssetEntry;

public class AssetEntryPriorityComparator implements Comparator<AssetEntry> {

	private static AssetEntryPriorityComparator instance;

	private AssetEntryPriorityComparator() {
	}

	@Override
	public int compare(AssetEntry assetEntry1, AssetEntry assetEntry2) {

		int value = Double.compare(assetEntry1.getPriority(), assetEntry2.getPriority());

		if (value == 0) {
			return assetEntry1.getTitle().compareTo(assetEntry2.getTitle());
		} else {
			return -value;
		}

	}

	public static AssetEntryPriorityComparator getInstance() {
		if (instance == null) {
			instance = new AssetEntryPriorityComparator();
		}

		return instance;
	}
}