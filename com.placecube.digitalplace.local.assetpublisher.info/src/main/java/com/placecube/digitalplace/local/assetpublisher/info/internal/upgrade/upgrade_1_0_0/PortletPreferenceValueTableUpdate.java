package com.placecube.digitalplace.local.assetpublisher.info.internal.upgrade.upgrade_1_0_0;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.PortletPreferenceValue;
import com.liferay.portal.kernel.service.PortletPreferenceValueLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

import java.util.List;

public class PortletPreferenceValueTableUpdate extends UpgradeProcess {
	
	private static final Log LOG = LogFactoryUtil.getLog(PortletPreferenceValueTableUpdate.class);
	
	private static final String INFO_LIST_PROVIDER_KEY = "infoListProviderKey";
	private static final String INFO_LIST_PROVIDER_CLASSNAME = "infoListProviderClassName";
	private static final String SMALL_VALUE = "com.placecube.digitalplace.local.assetpublisher.info.list.provider.PageCategoriesArticleAndGuideContentInfoListProvider";
	private static final String NEW_SMALL_VALUE = "com.placecube.digitalplace.local.assetpublisher.info.collection.provider.PageCategoriesArticleAndGuideContentInfoCollectionProvider";
	
	private PortletPreferenceValueLocalService portletPreferenceValueLocalService;
	
	public PortletPreferenceValueTableUpdate(PortletPreferenceValueLocalService portletPreferenceValueLocalService) {
		this.portletPreferenceValueLocalService = portletPreferenceValueLocalService;		
	}

	@Override
	protected void doUpgrade() throws Exception {
		LOG.info("Initialised upgrade process for PortletPreferenceValue records");
		
		List<PortletPreferenceValue> portletPreferenceValues = portletPreferenceValueLocalService.getPortletPreferenceValues(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		
		for(PortletPreferenceValue portletPreferenceValue : portletPreferenceValues) {
			if((portletPreferenceValue.getName().equals(INFO_LIST_PROVIDER_KEY) || portletPreferenceValue.getName().equals(INFO_LIST_PROVIDER_CLASSNAME)) && portletPreferenceValue.getSmallValue().equals(SMALL_VALUE)) {
				if(portletPreferenceValue.getName().equals(INFO_LIST_PROVIDER_CLASSNAME)){					 
					boolean containsProviderKey = false;
					for(PortletPreferenceValue p : portletPreferenceValues) {
						if(p.getPortletPreferencesId() == portletPreferenceValue.getPortletPreferencesId() && p.getName().equals(INFO_LIST_PROVIDER_KEY)) {
							containsProviderKey = true;
						}
					}
					if(!containsProviderKey) {
						portletPreferenceValue.setName(INFO_LIST_PROVIDER_KEY);
					}
				}
				
				portletPreferenceValue.setSmallValue(NEW_SMALL_VALUE);
				portletPreferenceValueLocalService.updatePortletPreferenceValue(portletPreferenceValue);
			}
		}
		
		LOG.info("Completed upgrade process for PortletPreferenceValue records");
	}

}
