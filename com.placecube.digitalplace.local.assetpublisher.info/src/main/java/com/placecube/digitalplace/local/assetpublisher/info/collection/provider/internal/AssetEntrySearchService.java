package com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleConstants;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideConstants;

@Component(immediate = true, service = AssetEntrySearchService.class)
public class AssetEntrySearchService {

	@Reference
	private IndexerRegistry indexerRegistry;

	public SearchContext createSearchContext(List<Long> groupIds, long companyId, long[] assetCategorysIds) {

		SearchContext searchContext = new SearchContext();
		searchContext.setCompanyId(companyId);
		searchContext.setGroupIds(ArrayUtil.toLongArray(groupIds));
		searchContext.setEntryClassNames(new String[] { JournalArticle.class.getName() });
		searchContext.setAssetCategoryIds(assetCategorysIds);
		searchContext.setBooleanClauses(new BooleanClause[] { getArticleAndGuideBooleanClause() });

		return searchContext;
	}

	public List<Long> getEntryClassPKsFromSearchResults(SearchContext searchContext) throws SearchException {

		Hits hits = indexerRegistry.getIndexer(JournalArticle.class).search(searchContext);

		List<Long> classPKs = new ArrayList<>();

		Arrays.asList(hits.getDocs()).stream().forEach(doc -> classPKs.add(GetterUtil.getLong(doc.get(com.liferay.portal.kernel.search.Field.ENTRY_CLASS_PK))));

		return classPKs;
	}

	private BooleanClause<Query> getArticleAndGuideBooleanClause() {
		return BooleanClauseFactoryUtil.create(new StringQuery("+(ddmStructureKey:" + ArticleConstants.STRUCTURE_KEY + " ddmStructureKey:" + GuideConstants.STRUCTURE_KEY + ")"),
				BooleanClauseOccur.MUST.toString());
	}

}
