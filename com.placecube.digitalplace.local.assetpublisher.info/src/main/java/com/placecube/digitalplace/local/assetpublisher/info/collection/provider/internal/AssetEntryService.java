package com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal;

import java.util.Collections;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.assetpublisher.info.util.AssetEntryPriorityComparator;

@Component(immediate = true, service = AssetEntryService.class)
public class AssetEntryService {

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private Portal portal;

	public List<AssetEntry> getAssetEntriesByClassPKs(List<Long> groupIds, long companyId, List<Long> classPKs) {

		if (classPKs.isEmpty()) {
			return Collections.emptyList();
		}

		DynamicQuery dQuery = assetEntryLocalService.dynamicQuery();
		dQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dQuery.add(RestrictionsFactoryUtil.in("groupId", groupIds));
		dQuery.add(RestrictionsFactoryUtil.eq("classNameId", portal.getClassNameId(JournalArticle.class)));
		dQuery.add(RestrictionsFactoryUtil.in("classPK", classPKs));

		return ListUtil.copy(assetEntryLocalService.dynamicQuery(dQuery, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null));
	}

	public List<AssetEntry> sort(List<AssetEntry> entries) {
		Collections.sort(entries, AssetEntryPriorityComparator.getInstance());

		return entries;
	}

}
