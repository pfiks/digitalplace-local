package com.placecube.digitalplace.local.assetpublisher.info.internal.upgrade.upgrade_1_0_0;

import com.liferay.portal.kernel.service.PortletPreferenceValueLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class Upgrade_1_0_0 implements UpgradeStepRegistrator {
	
	@Reference
	private PortletPreferenceValueLocalService portletPreferenceValueLocalService;
	
	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new PortletPreferenceValueTableUpdate(portletPreferenceValueLocalService));
	}

}
