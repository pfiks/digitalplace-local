package com.placecube.digitalplace.local.assetpublisher.info.list.provider.internal;

import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.generic.StringQuery;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal.AssetEntrySearchService;
import com.placecube.digitalplace.local.webcontent.article.constants.ArticleConstants;
import com.placecube.digitalplace.local.webcontent.guide.constants.GuideConstants;

public class AssetEntrySearchServiceTest {

	private static final long COMPANY_ID = 2;

	private static final long GROUP_ID = 3;

	@InjectMocks
	private AssetEntrySearchService assetEntrySearchService;

	@Mock
	private Document mockDoc1;

	@Mock
	private Document mockDoc2;

	@Mock
	private Hits mockHits;

	@Mock
	private Indexer<JournalArticle> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private SearchContext mockSearchContext;

	@Test
	public void createSearchContext_WhenNoError_ThenReturnsSearchContextCreated() {

		long[] assetCategorysIds = new long[] { 1, 2, 3 };
		List<Long> groupIds = Collections.singletonList(GROUP_ID);
		String[] entryClassNames = new String[] { JournalArticle.class.getName() };
		BooleanClause<Query> booleanClause = BooleanClauseFactoryUtil
				.create(new StringQuery("+(ddmStructureKey:" + ArticleConstants.STRUCTURE_KEY + " ddmStructureKey:" + GuideConstants.STRUCTURE_KEY + ")"), BooleanClauseOccur.MUST.toString());

		SearchContext result = assetEntrySearchService.createSearchContext(groupIds, COMPANY_ID, assetCategorysIds);

		assertThat(result.getCompanyId(), equalTo(COMPANY_ID));
		assertThat(result.getGroupIds(), equalTo(ArrayUtil.toLongArray(groupIds)));
		assertThat(result.getEntryClassNames(), equalTo(entryClassNames));
		assertThat(result.getAssetCategoryIds(), equalTo(assetCategorysIds));
		assertThat(result.getBooleanClauses(), arrayWithSize(1));
		assertThat(result.getBooleanClauses()[0].toString(), equalTo(booleanClause.toString()));
	}

	@Test(expected = SearchException.class)
	public void getEntryClassPKsFromSearchResults_WhenErrorSearching_ThenThrowsSearchException() throws Exception {

		when(mockIndexerRegistry.getIndexer(JournalArticle.class)).thenReturn(mockIndexer);
		when(mockIndexer.search(mockSearchContext)).thenThrow(new SearchException());

		assetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext);

	}

	@Test
	public void getEntryClassPKsFromSearchResults_WhenNoError_ThenReturnsListWithEntryClassPKs() throws Exception {

		when(mockIndexerRegistry.getIndexer(JournalArticle.class)).thenReturn(mockIndexer);
		when(mockIndexer.search(mockSearchContext)).thenReturn(mockHits);
		Document[] docs = new Document[] { mockDoc1, mockDoc2 };
		String entryClassPK1 = "123";
		when(mockDoc1.get(com.liferay.portal.kernel.search.Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK1);
		String entryClassPK2 = "456";
		when(mockDoc2.get(com.liferay.portal.kernel.search.Field.ENTRY_CLASS_PK)).thenReturn(entryClassPK2);
		when(mockHits.getDocs()).thenReturn(docs);

		List<Long> expected = Arrays.asList(Long.valueOf(entryClassPK1), Long.valueOf(entryClassPK2));

		List<Long> result = assetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext);

		assertThat(result, equalTo(expected));
	}

	@Before
	public void setUp() {

		initMocks(this);

	}
}
