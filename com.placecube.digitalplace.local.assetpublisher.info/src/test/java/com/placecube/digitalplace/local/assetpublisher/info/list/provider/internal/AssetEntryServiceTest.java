package com.placecube.digitalplace.local.assetpublisher.info.list.provider.internal;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal.AssetEntryService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ListUtil.class, RestrictionsFactoryUtil.class })
public class AssetEntryServiceTest {

	private static final long CLASS_NAME_ID = 1;

	private static final long COMPANY_ID = 2;

	private static final long GROUP_ID = 3;

	private static final List<Long> GROUP_IDS = Collections.singletonList(GROUP_ID);

	private static final Double PRIORITY_1 = 1.1;

	private static final Double PRIORITY_2 = 1.2;

	@InjectMocks
	private AssetEntryService assetEntryService;

	@Mock
	private AssetEntry mockAssetEntry1;

	@Mock
	private AssetEntry mockAssetEntry2;
	@Mock
	private List<Object> mockAssetEntryList;

	@Mock
	private List<Object> mockAssetEntryListCopied;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private List<Long> mockClassPKs;

	@Mock
	private Criterion mockCriterionOnClassNameId;

	@Mock
	private Criterion mockCriterionOnClassPks;

	@Mock
	private Criterion mockCriterionOnCompanyId;

	@Mock
	private Criterion mockCriterionOnGroupId;

	@Mock
	private DynamicQuery mockDQuery;

	@Mock
	private Portal mockPortal;

	@Test
	public void getAssetEntriesByClassPKs_WhenClassPKsIsEmpty_ThenReturnsEmptyList() {

		List<AssetEntry> result = assetEntryService.getAssetEntriesByClassPKs(GROUP_IDS, COMPANY_ID, Collections.emptyList());

		assertTrue(result.isEmpty());

	}

	@Test
	public void getAssetEntriesByClassPKs_WhenNoError_ThenAddCriterions() {

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(RestrictionsFactoryUtil.eq("companyId", COMPANY_ID)).thenReturn(mockCriterionOnCompanyId);
		when(RestrictionsFactoryUtil.in("groupId", GROUP_IDS)).thenReturn(mockCriterionOnGroupId);
		when(RestrictionsFactoryUtil.eq("classNameId", CLASS_NAME_ID)).thenReturn(mockCriterionOnClassNameId);
		when(RestrictionsFactoryUtil.in("classPK", mockClassPKs)).thenReturn(mockCriterionOnClassPks);

		assetEntryService.getAssetEntriesByClassPKs(GROUP_IDS, COMPANY_ID, mockClassPKs);

		InOrder inOrder = inOrder(mockDQuery, mockAssetEntryLocalService);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnCompanyId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnGroupId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnClassNameId);
		inOrder.verify(mockDQuery, times(1)).add(mockCriterionOnClassPks);
		inOrder.verify(mockAssetEntryLocalService, times(1)).dynamicQuery(mockDQuery, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);

	}

	@Test
	public void getAssetEntriesByClassPKs_WhenNoError_ThenReturnsAssetEntryList() {

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockDQuery);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockAssetEntryLocalService.dynamicQuery(eq(mockDQuery), eq(QueryUtil.ALL_POS), eq(QueryUtil.ALL_POS), eq(null))).thenReturn(mockAssetEntryList);
		when(ListUtil.copy(mockAssetEntryList)).thenReturn(mockAssetEntryListCopied);

		List<AssetEntry> result = assetEntryService.getAssetEntriesByClassPKs(GROUP_IDS, COMPANY_ID, mockClassPKs);

		assertThat(result, sameInstance(mockAssetEntryListCopied));

	}

	@Before
	public void setUp() {

		mockStatic(ListUtil.class, RestrictionsFactoryUtil.class);

	}

	@Test
	public void sort_WhenNoError_ThenReturnsSortedList() {

		List<AssetEntry> assetEntries = new ArrayList<AssetEntry>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);

		when(mockAssetEntry1.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry2.getPriority()).thenReturn(PRIORITY_2);

		List<AssetEntry> result = assetEntryService.sort(assetEntries);

		assertThat(result.get(0), sameInstance(mockAssetEntry2));
		assertThat(result.get(1), sameInstance(mockAssetEntry1));

	}
}
