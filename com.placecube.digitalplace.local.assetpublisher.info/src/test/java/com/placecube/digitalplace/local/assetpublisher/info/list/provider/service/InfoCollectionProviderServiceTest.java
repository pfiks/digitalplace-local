package com.placecube.digitalplace.local.assetpublisher.info.list.provider.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.depot.service.DepotEntryLocalService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal.AssetEntrySearchService;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.internal.AssetEntryService;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class })
public class InfoCollectionProviderServiceTest extends PowerMockito {

	private static final long CATEGORY_ID_1 = 1233;

	private static final long CATEGORY_ID_2 = 2233;

	private static final long COMPANY_ID = 44;

	private static final long GROUP_ID = 33;

	private static final List<Long> GROUP_IDS = Collections.singletonList(GROUP_ID);

	private static final long PLID = 1;

	@InjectMocks
	private InfoCollectionProviderService infoCollectionProviderService;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private List<AssetEntry> mockAssetEntryList;

	@Mock
	private AssetEntrySearchService mockAssetEntrySearchService;

	@Mock
	private AssetEntryService mockAssetEntryService;

	@Mock
	private List<Long> mockClassPKs;

	@Mock
	private DepotEntryLocalService mockDepotEntryLocalService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private List<AssetEntry> mockSortedAssetEntryList;

	List<AssetCategory> assetCategoriesList;

	@Before
	public void activateSetup() {
		initMocks(this);
		mockStatic(ServiceContextThreadLocal.class);
		assetCategoriesList = new ArrayList<AssetCategory>();
	}

	@Test
	public void getInfoCollection_WithInfoCollectionProviderContextParameter_WhenErrorGettingEntryClassPKsFromSearchResults_ThenReturnEmptyList() throws Exception {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);

		when(mockAssetCategory1.getCategoryId()).thenReturn(CATEGORY_ID_1);
		assetCategoriesList.add(mockAssetCategory1);
		when(mockAssetCategory2.getCategoryId()).thenReturn(CATEGORY_ID_2);
		assetCategoriesList.add(mockAssetCategory2);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(assetCategoriesList);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		long[] assetCategorysIds = new long[] { CATEGORY_ID_1, CATEGORY_ID_2 };

		when(mockAssetEntrySearchService.createSearchContext(eq(GROUP_IDS), eq(COMPANY_ID), eq(assetCategorysIds))).thenReturn(mockSearchContext);
		when(mockAssetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext)).thenThrow(new SearchException());

		assertTrue(infoCollectionProviderService.getInfoCollection(mockServiceContext).isEmpty());
		verifyZeroInteractions(mockAssetEntryService);
	}

	@Test
	public void getInfoCollection_WithInfoCollectionProviderContextParameter_WhenLayoutHasCategories_ThenReturnsAssetEntryList() throws Exception {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);

		when(mockAssetCategory1.getCategoryId()).thenReturn(CATEGORY_ID_1);
		assetCategoriesList.add(mockAssetCategory1);
		when(mockAssetCategory2.getCategoryId()).thenReturn(CATEGORY_ID_2);
		assetCategoriesList.add(mockAssetCategory2);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(assetCategoriesList);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		long[] assetCategorysIds = new long[] { CATEGORY_ID_1, CATEGORY_ID_2 };

		when(mockAssetEntrySearchService.createSearchContext(eq(GROUP_IDS), eq(COMPANY_ID), eq(assetCategorysIds))).thenReturn(mockSearchContext);
		when(mockAssetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext)).thenReturn(mockClassPKs);
		when(mockAssetEntryService.getAssetEntriesByClassPKs(GROUP_IDS, COMPANY_ID, mockClassPKs)).thenReturn(mockAssetEntryList);
		when(mockAssetEntryService.sort(mockAssetEntryList)).thenReturn(mockSortedAssetEntryList);

		assertThat(infoCollectionProviderService.getInfoCollection(mockServiceContext), sameInstance(mockSortedAssetEntryList));
	}

	@Test
	public void getInfoCollection_WithInfoCollectionProviderContextParameter_WhenLayoutHasNoCategories_ThenReturnsEmptyList() {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(Collections.emptyList());

		assertThat(infoCollectionProviderService.getInfoCollection(mockServiceContext), equalTo(Collections.emptyList()));
	}

	@Test
	public void getInfoCollection_WithServiceContextParameter_WhenErrorGettingEntryClassPKsFromSearchResults_ThenReturnEmptyList() throws Exception {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);

		when(mockAssetCategory1.getCategoryId()).thenReturn(CATEGORY_ID_1);
		assetCategoriesList.add(mockAssetCategory1);
		when(mockAssetCategory2.getCategoryId()).thenReturn(CATEGORY_ID_2);
		assetCategoriesList.add(mockAssetCategory2);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(assetCategoriesList);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		long[] assetCategorysIds = new long[] { CATEGORY_ID_1, CATEGORY_ID_2 };

		when(mockAssetEntrySearchService.createSearchContext(eq(GROUP_IDS), eq(COMPANY_ID), eq(assetCategorysIds))).thenReturn(mockSearchContext);
		when(mockAssetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext)).thenThrow(new SearchException());

		assertTrue(infoCollectionProviderService.getInfoCollection().isEmpty());
		verifyZeroInteractions(mockAssetEntryService);
	}

	@Test
	public void getInfoCollection_WithServiceContextParameter_WhenGetGroupConnectedDepotEntriesHasError_ThenProcessWithCurrentSiteGroupIdOnly() throws PortalException {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);
		when(mockAssetCategory1.getCategoryId()).thenReturn(CATEGORY_ID_1);
		assetCategoriesList.add(mockAssetCategory1);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(assetCategoriesList);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDepotEntryLocalService.getGroupConnectedDepotEntries(GROUP_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS)).thenThrow(PortalException.class);

		long[] assetCategorysIds = new long[] { CATEGORY_ID_1 };

		when(mockAssetEntrySearchService.createSearchContext(eq(GROUP_IDS), eq(COMPANY_ID), eq(assetCategorysIds))).thenReturn(mockSearchContext);
		when(mockAssetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext)).thenReturn(mockClassPKs);
		when(mockAssetEntryService.getAssetEntriesByClassPKs(GROUP_IDS, COMPANY_ID, mockClassPKs)).thenReturn(mockAssetEntryList);
		when(mockAssetEntryService.sort(mockAssetEntryList)).thenReturn(mockSortedAssetEntryList);

		assertThat(infoCollectionProviderService.getInfoCollection(mockServiceContext), sameInstance(mockSortedAssetEntryList));

	}

	@Test
	public void getInfoCollection_WithServiceContextParameter_WhenLayoutHasCategories_ThenReturnsAssetEntryList() throws Exception {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);

		when(mockAssetCategory1.getCategoryId()).thenReturn(CATEGORY_ID_1);
		assetCategoriesList.add(mockAssetCategory1);
		when(mockAssetCategory2.getCategoryId()).thenReturn(CATEGORY_ID_2);
		assetCategoriesList.add(mockAssetCategory2);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(assetCategoriesList);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		long[] assetCategorysIds = new long[] { CATEGORY_ID_1, CATEGORY_ID_2 };

		when(mockAssetEntrySearchService.createSearchContext(eq(GROUP_IDS), eq(COMPANY_ID), eq(assetCategorysIds))).thenReturn(mockSearchContext);
		when(mockAssetEntrySearchService.getEntryClassPKsFromSearchResults(mockSearchContext)).thenReturn(mockClassPKs);
		when(mockAssetEntryService.getAssetEntriesByClassPKs(GROUP_IDS, COMPANY_ID, mockClassPKs)).thenReturn(mockAssetEntryList);
		when(mockAssetEntryService.sort(mockAssetEntryList)).thenReturn(mockSortedAssetEntryList);

		assertThat(infoCollectionProviderService.getInfoCollection(), sameInstance(mockSortedAssetEntryList));
	}

	@Test
	public void getInfoCollection_WithServiceContextParameter_WhenServiceContextHasNoCategories_ThenReturnsEmptyList() {

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getPlid()).thenReturn(PLID);
		when(mockAssetCategoryLocalService.getCategories(Layout.class.getName(), PLID)).thenReturn(Collections.emptyList());

		assertThat(infoCollectionProviderService.getInfoCollection(), equalTo(Collections.emptyList()));
	}

	@Test(expected = NullPointerException.class)
	public void getInfoCollection_WithServiceContextParameter_WhenServiceContextIsNull_ThenThrowsNullPointerException() {

		ServiceContext serviceContext = null;

		infoCollectionProviderService.getInfoCollection(serviceContext);
	}
}
