package com.placecube.digitalplace.local.assetpublisher.info.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.asset.kernel.model.AssetEntry;

public class AssetEntryPriorityComparatorTest {

	private static final Double PRIORITY_1 = 1.1;

	private static final Double PRIORITY_2 = 1.2;

	private static final String TITLE_1 = "title1";

	private static final String TITLE_2 = "title2";

	@Mock
	private AssetEntry mockAssetEntry1;

	@Mock
	private AssetEntry mockAssetEntry2;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getInstance_WheNoError_ThenReturnsInstance() {

		assertNotNull(AssetEntryPriorityComparator.getInstance());

	}

	@Test
	public void compare_WhenPriority1IsLessThanPriority2_ThenReturnsOne() {

		when(mockAssetEntry1.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry2.getPriority()).thenReturn(PRIORITY_2);

		int result = AssetEntryPriorityComparator.getInstance().compare(mockAssetEntry1, mockAssetEntry2);

		assertTrue(result == 1);

	}

	@Test
	public void compare_WhenPriority1IsGreaterThanPriority2_ThenReturnsMinusOne() {

		when(mockAssetEntry1.getPriority()).thenReturn(PRIORITY_2);
		when(mockAssetEntry2.getPriority()).thenReturn(PRIORITY_1);

		int result = AssetEntryPriorityComparator.getInstance().compare(mockAssetEntry1, mockAssetEntry2);

		assertTrue(result == -1);

	}

	@Test
	public void compare_WhenPriorityAreTheSameAndTitle1IsLessThanTitle2_ThenReturnsMinusOne() {

		when(mockAssetEntry1.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry1.getTitle()).thenReturn(TITLE_1);
		when(mockAssetEntry2.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry2.getTitle()).thenReturn(TITLE_2);

		int result = AssetEntryPriorityComparator.getInstance().compare(mockAssetEntry1, mockAssetEntry2);

		assertTrue(result == -1);

	}

	@Test
	public void compare_WhenTitle1IsGreaterThanTitle2_ThenReturnsOne() {

		when(mockAssetEntry1.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry1.getTitle()).thenReturn(TITLE_2);
		when(mockAssetEntry2.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry2.getTitle()).thenReturn(TITLE_1);

		int result = AssetEntryPriorityComparator.getInstance().compare(mockAssetEntry1, mockAssetEntry2);

		assertTrue(result == 1);

	}

	@Test
	public void compare_WhenTitlesAreEquals_ThenReturnsZero() {

		when(mockAssetEntry1.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry1.getTitle()).thenReturn(TITLE_1);
		when(mockAssetEntry2.getPriority()).thenReturn(PRIORITY_1);
		when(mockAssetEntry2.getTitle()).thenReturn(TITLE_1);

		int result = AssetEntryPriorityComparator.getInstance().compare(mockAssetEntry1, mockAssetEntry2);

		assertTrue(result == 0);

	}
}
