package com.placecube.digitalplace.local.assetpublisher.info.list.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.info.collection.provider.CollectionQuery;
import com.liferay.info.pagination.InfoPage;
import com.liferay.info.pagination.Pagination;
import com.liferay.info.sort.Sort;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.PageCategoriesArticleAndGuideContentInfoCollectionProvider;
import com.placecube.digitalplace.local.assetpublisher.info.collection.provider.service.InfoCollectionProviderService;

public class PageCategoriesArticleAndGuideContentInfoCollectionProviderTest {

	@Mock
	private List<AssetEntry> mockAssetEntryList;

	@Mock
	private CollectionQuery mockCollectionQuery;

	@Mock
	private InfoCollectionProviderService mockInfoCollectionProviderService;

	@Mock
	private InfoPage<AssetEntry> mockInfoPage;

	@Mock
	private Pagination mockPagination;

	@Mock
	private Sort mockSort;

	@InjectMocks
	private PageCategoriesArticleAndGuideContentInfoCollectionProvider pageCategoriesArticleAndGuideContentInfoCollectionProvider;

	@Test
	public void getCollectionInfoPage_WhenNoError_ThenReturnsInfoPage() {
		int count = 4;
		when(mockInfoCollectionProviderService.getInfoCollection()).thenReturn(mockAssetEntryList);
		when(mockCollectionQuery.getPagination()).thenReturn(mockPagination);
		when(mockAssetEntryList.size()).thenReturn(count);

		InfoPage<AssetEntry> infoPage = pageCategoriesArticleAndGuideContentInfoCollectionProvider.getCollectionInfoPage(mockCollectionQuery);

		assertNotNull(infoPage);
		assertEquals(count, infoPage.getTotalCount());
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

}
