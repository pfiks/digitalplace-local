package com.placecube.digitalplace.local.account.webcontent.bincollectiondates.constants;

public enum BinCollectionDatesWidgetTemplate {

	BIN_COLLECTION_DATES("bin_collection_dates", "Bin Collection Dates");

	private final String key;
	private final String name;

	BinCollectionDatesWidgetTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}
}
