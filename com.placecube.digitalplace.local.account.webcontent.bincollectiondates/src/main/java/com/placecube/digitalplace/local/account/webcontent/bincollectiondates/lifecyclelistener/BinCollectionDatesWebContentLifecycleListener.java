package com.placecube.digitalplace.local.account.webcontent.bincollectiondates.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.account.webcontent.bincollectiondates.constants.BinCollectionDatesWidgetTemplate;
import com.placecube.digitalplace.local.account.webcontent.bincollectiondates.service.BinCollectionDatesWebContentService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class BinCollectionDatesWebContentLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(BinCollectionDatesWebContentLifecycleListener.class);

	@Reference
	private BinCollectionDatesWebContentService binCollectionDatesWebContentService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {

		long companyId = company.getCompanyId();
		LOG.debug("Creating Bin Collection Dates resources for companyId: " + companyId);

		Group globalGroup = company.getGroup();
		ServiceContext serviceContext = binCollectionDatesWebContentService.getServiceContext(globalGroup);

		binCollectionDatesWebContentService.getOrCreateDDMStructure(serviceContext);
		binCollectionDatesWebContentService.getOrCreateWidgetTemplate(BinCollectionDatesWidgetTemplate.BIN_COLLECTION_DATES, serviceContext);

		LOG.debug("Created Bin Collection Dates resources for companyId: " + companyId);

	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Waits until the portal has finished initializing. No code needed.
	}

}
