package com.placecube.digitalplace.local.account.webcontent.bincollectiondates.service;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.account.webcontent.bincollectiondates.constants.BinCollectionDatesWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = BinCollectionDatesWebContentService.class)
public class BinCollectionDatesWebContentService {

	@Reference
	private DDMInitializer ddmInitializer;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		String resourcePath = "com/placecube/digitalplace/local/account/webcontent/bincollectiondates/dependencies/ddm/structure.xml";

		return ddmInitializer.getOrCreateDDMStructure("Bin Collection Dates", resourcePath, getClass().getClassLoader(), JournalArticle.class, serviceContext);
	}

	public DDMTemplate getOrCreateWidgetTemplate(BinCollectionDatesWidgetTemplate widgetTemplate, ServiceContext serviceContext) throws PortalException {
		String templateResourcePath = "com/placecube/digitalplace/local/account/webcontent/bincollectiondates/dependencies/ddm/" + widgetTemplate.getKey() + ".ftl";
		DDMTemplateContext ddmTemplateContext = ddmInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath,
				getClass().getClassLoader(), serviceContext);

		return ddmInitializer.getOrCreateDDMTemplate(ddmTemplateContext);
	}

	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(false);

		return serviceContext;
	}
}
