<#assign 
    user = themeDisplay.getUser()
    uprn = user.getExpandoBridge().getAttribute("uprn")
/>

<#if uprn?? && uprn != "">
    <#assign     
        myLocalContentService = digitalplace_myLocalContentService 
        
        recyclingDateJSON = myLocalContentService.getRecyclingDay(themeDisplay.getCompanyId(), uprn)
        nextRecyclingDate = myLocalContentService.getDateValue(themeDisplay.getCompanyId(), recyclingDateJSON)
        
        gardenWasteDateJSON = myLocalContentService.getGardenWasteDay(themeDisplay.getCompanyId(), uprn)
        nextGardenWasteDate = myLocalContentService.getDateValue(themeDisplay.getCompanyId(), gardenWasteDateJSON)
        
        foodWasteDateJSON = myLocalContentService.getFoodWasteDay(themeDisplay.getCompanyId(), uprn)
        nextFoodWasteDate = myLocalContentService.getDateValue(themeDisplay.getCompanyId(), foodWasteDateJSON)
        
        refuseDateJSON = myLocalContentService.getRefuseDay(themeDisplay.getCompanyId(), uprn)
        nextRefuseDate = myLocalContentService.getDateValue(themeDisplay.getCompanyId(), refuseDateJSON)
        
        bodyText = Body.getData()
        
        bodyText = bodyText?replace("$RECYCLING_DATE$", nextRecyclingDate)
        bodyText = bodyText?replace("$GARDEN_WASTE_DATE$", nextGardenWasteDate)
        bodyText = bodyText?replace("$FOOD_WASTE_DATE$", nextFoodWasteDate)
        bodyText = bodyText?replace("$REFUSE_DATE$", nextRefuseDate)
    />

    <div class="bin-collection-dates">
        <div class="bin-collection-dates-title">
            <h2>${Title.getData()}</h2>
        </div>
        
        <div class="bin-collection-dates-body">
            ${bodyText}
        </div>
    </div>
<#else>
    <@liferay_ui["message"] key="there-is-no-x" arguments="waste data for your address" />
</#if>