package com.placecube.digitalplace.local.account.webcontent.bincollectiondates.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.account.webcontent.bincollectiondates.constants.BinCollectionDatesWidgetTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BinCollectionDatesWebContentService.class })
public class BinCollectionDatesWebContentServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 2;

	private static final long CREATOR_USER_ID = 3;

	private static final long GROUP_ID = 1;

	private static final String LANGUAGE_ID = "language_id";

	private static final String STRUCTURE_KEY = "Bin Collection Dates";

	private static final String STRUCTURE_RESOURCE_PATH = "com/placecube/digitalplace/local/account/webcontent/bincollectiondates/dependencies/ddm/structure.xml";

	private static final String TEMPLATE_RESOURCE_PATH = "com/placecube/digitalplace/local/account/webcontent/bincollectiondates/dependencies/ddm/";

	@InjectMocks
	private BinCollectionDatesWebContentService binCollectionDatesWebContentService;

	private ClassLoader binCollectionDatesWebContentServiceClassLoader;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@Mock
	private Group mockGroup;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetUp() {
		binCollectionDatesWebContentServiceClassLoader = BinCollectionDatesWebContentService.class.getClassLoader();
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenExceptionRetrievingTheStructure_ThenThrowsPortalException() throws PortalException {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, getClass().getClassLoader(), JournalArticle.class, mockServiceContext))
				.thenThrow(new PortalException());

		binCollectionDatesWebContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenReturnsTheBinCollectionDatesStructure() throws PortalException {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, getClass().getClassLoader(), JournalArticle.class, mockServiceContext)).thenReturn(mockDDMStructure);

		DDMStructure ddmStructure = binCollectionDatesWebContentService.getOrCreateDDMStructure(mockServiceContext);

		assertThat(ddmStructure, sameInstance(mockDDMStructure));

	}

	@Test(expected = PortalException.class)
	public void getOrCreateWidgetTemplate_WhenException_ThenThrowsPortalException() throws Exception {

		BinCollectionDatesWidgetTemplate widgetTemplate = BinCollectionDatesWidgetTemplate.BIN_COLLECTION_DATES;
		String templateResourcePath = TEMPLATE_RESOURCE_PATH + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath,
				binCollectionDatesWebContentServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenThrow(new PortalException());

		binCollectionDatesWebContentService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

	}

	@Test
	public void getOrCreateWidgetTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws Exception {

		BinCollectionDatesWidgetTemplate widgetTemplate = BinCollectionDatesWidgetTemplate.BIN_COLLECTION_DATES;
		String templateResourcePath = TEMPLATE_RESOURCE_PATH + widgetTemplate.getKey() + ".ftl";

		when(mockDDMInitializer.getDDMTemplateContextForPortletDisplay(widgetTemplate.getName(), widgetTemplate.getKey(), AssetEntry.class, templateResourcePath,
				binCollectionDatesWebContentServiceClassLoader, mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = binCollectionDatesWebContentService.getOrCreateWidgetTemplate(widgetTemplate, mockServiceContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnServiceContextConfigured() throws Exception {

		whenNew(ServiceContext.class).withNoArguments().thenReturn(mockServiceContext);

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getCreatorUserId()).thenReturn(CREATOR_USER_ID);
		when(mockGroup.getDefaultLanguageId()).thenReturn(LANGUAGE_ID);

		binCollectionDatesWebContentService.getServiceContext(mockGroup);

		verify(mockServiceContext, times(1)).setScopeGroupId(GROUP_ID);
		verify(mockServiceContext, times(1)).setCompanyId(COMPANY_ID);
		verify(mockServiceContext, times(1)).setUserId(CREATOR_USER_ID);
		verify(mockServiceContext, times(1)).setLanguageId(LANGUAGE_ID);
		verify(mockServiceContext, times(1)).setAddGroupPermissions(true);
		verify(mockServiceContext, times(1)).setAddGuestPermissions(false);
	}
}
