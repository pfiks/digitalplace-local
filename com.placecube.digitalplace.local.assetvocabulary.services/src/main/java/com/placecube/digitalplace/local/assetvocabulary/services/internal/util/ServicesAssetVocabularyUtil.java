package com.placecube.digitalplace.local.assetvocabulary.services.internal.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;

@Component(immediate = true, service = ServicesAssetVocabularyUtil.class)
public class ServicesAssetVocabularyUtil {

	@Reference
	JSONFactory jsonFactory;

	public JSONObject getJSONObject() throws PortalException {
		try {
			return jsonFactory.createJSONObject(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/assetvocabulary/services/dependencies/vocabulary.json"));
		} catch (JSONException | IOException e) {
			throw new PortalException(e);
		}
	}

	public String getSettings(JSONObject jsonObject, Map<Long, Set<Long>> selectedClassNameIds, Map<Long, Set<Long>> requiredClassNameIds) {
		StringBuilder settings = new StringBuilder();
		settings.append("multiValued=" + jsonObject.getBoolean("multiValued"));
		appendClassNameIds("selectedClassNameIds", selectedClassNameIds, settings);
		appendClassNameIds("requiredClassNameIds", requiredClassNameIds, settings);
		return settings.toString();
	}

	public Map<Locale, String> getTitleMap(ServiceContext serviceContext, String title) {
		Map<Locale, String> titleMap = new HashMap<>();
		titleMap.put(serviceContext.getLocale(), title);
		return titleMap;
	}

	private void appendClassNameIds(String settingName, Map<Long, Set<Long>> classNameIds, StringBuilder settings) {
		if (!classNameIds.isEmpty()) {
			settings.append(StringPool.NEW_LINE);
			settings.append(settingName + StringPool.EQUAL);

			for (Entry<Long, Set<Long>> items : classNameIds.entrySet()) {
				Long classNameId = items.getKey();
				Set<Long> classPKs = items.getValue();
				if (classPKs.isEmpty()) {
					settings.append(classNameId + StringPool.COLON + "-1" + StringPool.COMMA);
				} else {
					for (Long classPK : classPKs) {
						settings.append(classNameId + StringPool.COLON + classPK + StringPool.COMMA);
					}
				}
			}
		}
	}
}
