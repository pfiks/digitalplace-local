package com.placecube.digitalplace.local.assetvocabulary.services.service;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.assetvocabulary.services.internal.util.ServicesAssetVocabularyUtil;

@Component(immediate = true, service = ServicesAssetVocabularyService.class)
public class ServicesAssetVocabularyService {

	private static final Log LOG = LogFactoryUtil.getLog(ServicesAssetVocabularyService.class);

	@Reference
	AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	ServicesAssetVocabularyUtil servicesAssetVocabularyUtil;

	public AssetVocabulary getOrCreateVocabulary(ServiceContext serviceContext, Map<Long, Set<Long>> selectedClassNameIds, Map<Long, Set<Long>> requiredClassNameIds, JSONObject definition)
			throws PortalException {

		if (definition == null) {
			throw new PortalException("Vocabulary definition is null!");
		}

		String title = definition.getString("vocabularyName");

		LOG.debug("Configuring vocabulary: " + title + " in groupId: " + serviceContext.getScopeGroupId());

		AssetVocabulary existingGroupVocabulary = assetVocabularyLocalService.fetchGroupVocabulary(serviceContext.getScopeGroupId(), title);

		return Validator.isNull(existingGroupVocabulary) ? createVocabulary(serviceContext, definition, title, selectedClassNameIds, requiredClassNameIds) : existingGroupVocabulary;

	}

	public AssetVocabulary getOrCreateVocabulary(ServiceContext serviceContext, Map<Long, Set<Long>> selectedClassNameIds, Map<Long, Set<Long>> requiredClassNameIds) throws PortalException {

		return getOrCreateVocabulary(serviceContext, selectedClassNameIds, requiredClassNameIds, servicesAssetVocabularyUtil.getJSONObject());
	}

	private AssetVocabulary createVocabulary(ServiceContext serviceContext, JSONObject jsonObject, String title, Map<Long, Set<Long>> selectedClassNameIds, Map<Long, Set<Long>> requiredClassNameIds)
			throws PortalException {
		String settings = servicesAssetVocabularyUtil.getSettings(jsonObject, selectedClassNameIds, requiredClassNameIds);
		Map<Locale, String> titleMap = servicesAssetVocabularyUtil.getTitleMap(serviceContext, title);
		AssetVocabulary assetVocabulary = assetVocabularyLocalService.addVocabulary(serviceContext.getUserId(), serviceContext.getScopeGroupId(), StringPool.BLANK, titleMap, Collections.emptyMap(), settings, serviceContext);
		LOG.info("Created vocabulary: " + assetVocabulary.getTitle() + " in groupId: " + assetVocabulary.getGroupId());
		return assetVocabulary;
	}

}
