package com.placecube.digitalplace.local.assetvocabulary.services.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.local.assetvocabulary.services.internal.util.ServicesAssetVocabularyUtil;

public class ServicesAssetVocabularyServiceTest {

	private ServicesAssetVocabularyService servicesAssetVocabularyCreator;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private ServicesAssetVocabularyUtil mockServicesAssetVocabularyUtil;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Map<Long, Set<Long>> mockSelectedClassNameIds;

	@Mock
	private Map<Long, Set<Long>> mockRequiredClassNameIds;

	@Mock
	private Map<Locale, String> mockTitleMap;

	@Mock
	private JSONObject mockJSONObject;

	@Before
	public void setUp() {
		initMocks(this);

		servicesAssetVocabularyCreator = new ServicesAssetVocabularyService();
		servicesAssetVocabularyCreator.assetVocabularyLocalService = mockAssetVocabularyLocalService;
		servicesAssetVocabularyCreator.servicesAssetVocabularyUtil = mockServicesAssetVocabularyUtil;
	}

	@Test(expected = PortalException.class)
	public void getOrCreateVocabulary_WhenDefinitionIsNull_ThenThrowsPortalException() throws PortalException {
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenReturn(null);

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateVocabulary_WhenExceptionRetrievingJSONObject_ThenThrowsPortalException() throws PortalException {
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenThrow(new PortalException());

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateVocabulary_WhenExceptionRetrievingJSONObject_ThenDoesNotgetOrCreateVocabularyVocabulary() throws PortalException {
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenThrow(new PortalException());

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);

		verifyZeroInteractions(mockAssetVocabularyLocalService);
	}

	@Test
	public void getOrCreateVocabulary_WhenAssetVocabularyAlreadyExists_ThenReturnsTheVocabulary() throws PortalException {
		String title = "titleValue";
		long groupId = 123;
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenReturn(mockJSONObject);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(mockAssetVocabulary);

		AssetVocabulary result = servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);

		assertThat(result, sameInstance(mockAssetVocabulary));
	}

	@Test
	public void getOrCreateVocabulary_WhenAssetVocabularyAlreadyExists_ThenDoesNotCreateVocabulary() throws PortalException {
		String title = "titleValue";
		long groupId = 123;
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenReturn(mockJSONObject);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(mockAssetVocabulary);

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);

		verify(mockAssetVocabularyLocalService, never()).addVocabulary(anyLong(), anyLong(), anyString(), anyMap(), anyMap(), anyString(), any(ServiceContext.class));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateVocabulary_WhenNoVocabularyExistsAndExceptionAddingVocabulary_ThenThrowsPortalException() throws PortalException {
		String title = "titleValue";
		String settings = "settingsValue";
		long groupId = 123;
		long userId = 456;
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenReturn(mockJSONObject);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(null);
		when(mockServicesAssetVocabularyUtil.getSettings(mockJSONObject, mockSelectedClassNameIds, mockRequiredClassNameIds)).thenReturn(settings);
		when(mockServicesAssetVocabularyUtil.getTitleMap(mockServiceContext, title)).thenReturn(mockTitleMap);
		when(mockAssetVocabularyLocalService.addVocabulary(userId, groupId, StringPool.BLANK, mockTitleMap, Collections.emptyMap(), settings, mockServiceContext)).thenThrow(new PortalException());

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);
	}

	@Test
	public void getOrCreateVocabulary_WhenNoVocabularyExistsAndNoError_ThenReturnTheCreatedVocabulary() throws PortalException {
		String title = "titleValue";
		String settings = "settingsValue";
		long groupId = 123;
		long userId = 456;
		when(mockServicesAssetVocabularyUtil.getJSONObject()).thenReturn(mockJSONObject);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(null);
		when(mockServicesAssetVocabularyUtil.getSettings(mockJSONObject, mockSelectedClassNameIds, mockRequiredClassNameIds)).thenReturn(settings);
		when(mockServicesAssetVocabularyUtil.getTitleMap(mockServiceContext, title)).thenReturn(mockTitleMap);
		when(mockAssetVocabularyLocalService.addVocabulary(userId, groupId, StringPool.BLANK, mockTitleMap, Collections.emptyMap(), settings, mockServiceContext)).thenReturn(mockAssetVocabulary);

		AssetVocabulary result = servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds);

		assertThat(result, sameInstance(mockAssetVocabulary));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateVocabulary_WithDefinitionParameter_WhenDefinitionIsNull_ThenThrowsPortalException() throws PortalException {
		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds, null);
	}

	@Test
	public void getOrCreateVocabulary_WithDefinitionParameter_WhenAssetVocabularyAlreadyExists_ThenReturnsTheVocabulary() throws PortalException {
		String title = "titleValue";
		long groupId = 123;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(mockAssetVocabulary);

		AssetVocabulary result = servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds, mockJSONObject);

		assertThat(result, sameInstance(mockAssetVocabulary));
	}

	@Test
	public void getOrCreateVocabulary_WithDefinitionParameter_WhenAssetVocabularyAlreadyExists_ThenDoesNotCreateVocabulary() throws PortalException {
		String title = "titleValue";
		long groupId = 123;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(mockAssetVocabulary);

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds, mockJSONObject);

		verify(mockAssetVocabularyLocalService, never()).addVocabulary(anyLong(), anyLong(), anyString(), anyMap(), anyMap(), anyString(), any(ServiceContext.class));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateVocabulary_WithDefinitionParameter_WhenNoVocabularyExistsAndExceptionAddingVocabulary_ThenThrowsPortalException() throws PortalException {
		String title = "titleValue";
		String settings = "settingsValue";
		long groupId = 123;
		long userId = 456;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(null);
		when(mockServicesAssetVocabularyUtil.getSettings(mockJSONObject, mockSelectedClassNameIds, mockRequiredClassNameIds)).thenReturn(settings);
		when(mockServicesAssetVocabularyUtil.getTitleMap(mockServiceContext, title)).thenReturn(mockTitleMap);
		when(mockAssetVocabularyLocalService.addVocabulary(userId, groupId, StringPool.BLANK, mockTitleMap, Collections.emptyMap(), settings, mockServiceContext)).thenThrow(new PortalException());

		servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds, mockJSONObject);
	}

	@Test
	public void getOrCreateVocabulary_WithDefinitionParameter_WhenNoVocabularyExistsAndNoError_ThenReturnTheCreatedVocabulary() throws PortalException {
		String title = "titleValue";
		String settings = "settingsValue";
		long groupId = 123;
		long userId = 456;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockJSONObject.getString("vocabularyName")).thenReturn(title);
		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(groupId, title)).thenReturn(null);
		when(mockServicesAssetVocabularyUtil.getSettings(mockJSONObject, mockSelectedClassNameIds, mockRequiredClassNameIds)).thenReturn(settings);
		when(mockServicesAssetVocabularyUtil.getTitleMap(mockServiceContext, title)).thenReturn(mockTitleMap);
		when(mockAssetVocabularyLocalService.addVocabulary(userId, groupId, StringPool.BLANK, mockTitleMap, Collections.emptyMap(), settings, mockServiceContext)).thenReturn(mockAssetVocabulary);

		AssetVocabulary result = servicesAssetVocabularyCreator.getOrCreateVocabulary(mockServiceContext, mockSelectedClassNameIds, mockRequiredClassNameIds, mockJSONObject);

		assertThat(result, sameInstance(mockAssetVocabulary));
	}
}
