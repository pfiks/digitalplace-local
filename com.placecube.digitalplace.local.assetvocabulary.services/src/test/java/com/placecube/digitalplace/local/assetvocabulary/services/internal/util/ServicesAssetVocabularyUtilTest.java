package com.placecube.digitalplace.local.assetvocabulary.services.internal.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.imageio.IIOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ServicesAssetVocabularyUtilTest extends PowerMockito {

	private ServicesAssetVocabularyUtil servicesAssetVocabularyUtil;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(StringUtil.class);

		servicesAssetVocabularyUtil = new ServicesAssetVocabularyUtil();
		servicesAssetVocabularyUtil.jsonFactory = mockJSONFactory;
	}

	@Test(expected = PortalException.class)
	public void getJSONObject_WhenIOException_ThenThrowsPortalException() throws Exception {
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/assetvocabulary/services/dependencies/vocabulary.json")).thenThrow(new IIOException("msg"));

		servicesAssetVocabularyUtil.getJSONObject();
	}

	@Test(expected = PortalException.class)
	public void getJSONObject_WhenJSONException_ThenThrowsPortalException() throws Exception {
		String jsonValue = "jsonStringValue";
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/assetvocabulary/services/dependencies/vocabulary.json")).thenReturn(jsonValue);
		when(mockJSONFactory.createJSONObject(jsonValue)).thenThrow(new JSONException());

		servicesAssetVocabularyUtil.getJSONObject();
	}

	@Test
	public void getJSONObject_WhenNoError_ThenReturnsTheJSONObject() throws Exception {
		String jsonValue = "jsonStringValue";
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/local/assetvocabulary/services/dependencies/vocabulary.json")).thenReturn(jsonValue);
		when(mockJSONFactory.createJSONObject(jsonValue)).thenReturn(mockJSONObject);

		JSONObject result = servicesAssetVocabularyUtil.getJSONObject();

		assertThat(result, equalTo(mockJSONObject));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSettings_WhenSelectedClassNameIdsAndRequiredClassNameIdsAreEmpty_ThenReturnsSettingsWithOnlyMultiValuedEntry(boolean multiValued) {
		when(mockJSONObject.getBoolean("multiValued")).thenReturn(multiValued);

		String result = servicesAssetVocabularyUtil.getSettings(mockJSONObject, Collections.emptyMap(), Collections.emptyMap());

		assertThat(result, equalTo("multiValued=" + multiValued));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSettings_WhenSelectedClassNameIdsHasAnEntryWithNoValues_ThenReturnsSettingsWithMultiValuedEntryAndSelectedClassNameIdsEntryForMinusOne(boolean multiValued) {
		when(mockJSONObject.getBoolean("multiValued")).thenReturn(multiValued);
		Map<Long, Set<Long>> selectedClassNameIds = new LinkedHashMap<>();
		Set<Long> firstEntryValues = new LinkedHashSet<>();
		firstEntryValues.add(10l);
		selectedClassNameIds.put(1l, firstEntryValues);
		selectedClassNameIds.put(2l, new LinkedHashSet<>());

		String result = servicesAssetVocabularyUtil.getSettings(mockJSONObject, selectedClassNameIds, Collections.emptyMap());

		assertThat(result, equalTo("multiValued=" + multiValued + StringPool.NEW_LINE + "selectedClassNameIds=1:10,2:-1,"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSettings_WhenRequiredClassNameIdsHasAnEntryWithNoValues_ThenReturnsSettingsWithMultiValuedEntryAndRequiredClassNameIdsEntryForMinusOne(boolean multiValued) {
		when(mockJSONObject.getBoolean("multiValued")).thenReturn(multiValued);
		Map<Long, Set<Long>> requiredClassNameIds = new LinkedHashMap<>();
		Set<Long> firstEntryValues = new LinkedHashSet<>();
		firstEntryValues.add(10l);
		requiredClassNameIds.put(1l, firstEntryValues);
		requiredClassNameIds.put(2l, new LinkedHashSet<>());

		String result = servicesAssetVocabularyUtil.getSettings(mockJSONObject, Collections.emptyMap(), requiredClassNameIds);

		assertThat(result, equalTo("multiValued=" + multiValued + StringPool.NEW_LINE + "requiredClassNameIds=1:10,2:-1,"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSettings_WhenSelectedClassNameIdsHasValuesAndRequiredClassNameIdsAreEmpty_ThenReturnsSettingsWithMultiValuedEntryAndSelectedClassNameIdsEntry(boolean multiValued) {
		when(mockJSONObject.getBoolean("multiValued")).thenReturn(multiValued);
		Map<Long, Set<Long>> selectedClassNameIds = new LinkedHashMap<>();
		Set<Long> firstEntryValues = new LinkedHashSet<>();
		firstEntryValues.add(10l);
		selectedClassNameIds.put(1l, firstEntryValues);
		Set<Long> secondEntryValues = new LinkedHashSet<>();
		secondEntryValues.add(10l);
		secondEntryValues.add(20l);
		selectedClassNameIds.put(2l, secondEntryValues);

		String result = servicesAssetVocabularyUtil.getSettings(mockJSONObject, selectedClassNameIds, Collections.emptyMap());

		assertThat(result, equalTo("multiValued=" + multiValued + StringPool.NEW_LINE + "selectedClassNameIds=1:10,2:10,2:20,"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSettings_WhenSelectedClassNameIdsAreEmptyAndRequiredClassNameIdsHasValue_ThenReturnsSettingsWithMultiValuedEntryAndRequiredClassNameIdsEntry(boolean multiValued) {
		when(mockJSONObject.getBoolean("multiValued")).thenReturn(multiValued);
		Map<Long, Set<Long>> requiredClassNameIds = new LinkedHashMap<>();
		Set<Long> firstEntryValues = new LinkedHashSet<>();
		firstEntryValues.add(10l);
		requiredClassNameIds.put(1l, firstEntryValues);
		Set<Long> secondEntryValues = new LinkedHashSet<>();
		secondEntryValues.add(10l);
		secondEntryValues.add(20l);
		requiredClassNameIds.put(2l, secondEntryValues);

		String result = servicesAssetVocabularyUtil.getSettings(mockJSONObject, Collections.emptyMap(), requiredClassNameIds);

		assertThat(result, equalTo("multiValued=" + multiValued + StringPool.NEW_LINE + "requiredClassNameIds=1:10,2:10,2:20,"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSettings_WhenSelectedClassNameIdsHasValueAndRequiredClassNameIdsHasValue_ThenReturnsSettingsWithMultiValuedEntryAndSelectedClassNameIdsEntryAndRequiredClassNameIdsEntry(
			boolean multiValued) {
		when(mockJSONObject.getBoolean("multiValued")).thenReturn(multiValued);
		Map<Long, Set<Long>> selectedClassNameIds = new LinkedHashMap<>();
		Set<Long> firstEntryValues = new LinkedHashSet<>();
		firstEntryValues.add(10l);
		selectedClassNameIds.put(1l, firstEntryValues);
		Set<Long> secondEntryValues = new LinkedHashSet<>();
		secondEntryValues.add(10l);
		secondEntryValues.add(20l);
		selectedClassNameIds.put(2l, secondEntryValues);

		Map<Long, Set<Long>> requiredClassNameIds = new LinkedHashMap<>();
		Set<Long> thirdEntryValues = new LinkedHashSet<>();
		thirdEntryValues.add(30l);
		requiredClassNameIds.put(3l, thirdEntryValues);
		Set<Long> fourthEntryValues = new LinkedHashSet<>();
		fourthEntryValues.add(40l);
		fourthEntryValues.add(50l);
		requiredClassNameIds.put(4l, fourthEntryValues);

		String result = servicesAssetVocabularyUtil.getSettings(mockJSONObject, selectedClassNameIds, requiredClassNameIds);

		assertThat(result, equalTo("multiValued=" + multiValued + StringPool.NEW_LINE + "selectedClassNameIds=1:10,2:10,2:20," + StringPool.NEW_LINE + "requiredClassNameIds=3:30,4:40,4:50,"));
	}

	@Test
	public void getTitleMap_WhenNoError_ThenReturnsMapWithTheServiceContextLocaleAndTheTitleValue() {
		Locale locale = Locale.CANADA_FRENCH;
		when(mockServiceContext.getLocale()).thenReturn(locale);
		String title = "titleValue";

		Map<Locale, String> result = servicesAssetVocabularyUtil.getTitleMap(mockServiceContext, title);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(locale), equalTo(title));
	}
}
