package com.placecube.digitalplace.local.ddl.offer.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = OfferDDMService.class)
public class OfferDDMService {

	private static final String RESOURCE_BASE_PATH = "com/placecube/digitalplace/local/ddl/offer/dependencies/ddm/";

	@Reference
	private DDMInitializer ddmInitializer;

	public void configure(ServiceContext serviceContext) throws PortalException {
		String structureResourcePath = RESOURCE_BASE_PATH + "structure.xml";

		ddmInitializer.getOrCreateDDMStructure("OFFER", structureResourcePath, getClass().getClassLoader(), DDLRecordSet.class, serviceContext);
	}

}
