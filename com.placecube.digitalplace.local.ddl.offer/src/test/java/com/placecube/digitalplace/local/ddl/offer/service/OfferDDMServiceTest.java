package com.placecube.digitalplace.local.ddl.offer.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.service.DDMInitializer;

public class OfferDDMServiceTest extends PowerMockito {

	private static final String STRUCTURE_KEY = "OFFER";

	private static final String STRUCTURE_RESOURCE_PATH = "com/placecube/digitalplace/local/ddl/offer/dependencies/ddm/structure.xml";

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private OfferDDMService offerDDMService;

	private ClassLoader offerDDMServiceClassLoader;

	@Before
	public void activateSetUp() {
		initMocks(this);

		offerDDMServiceClassLoader = OfferDDMService.class.getClassLoader();
	}

	@Test(expected = PortalException.class)
	public void configure_WhenErrorGettingOrCreatingDDMStructure_ThenThrowsException() throws Exception {

		when(mockDDMInitializer.getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, offerDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext)).thenThrow(new PortalException());

		offerDDMService.configure(mockServiceContext);

	}

	@Test
	public void configure_WhenNoError_ThenCreatesTheDDMStructure() throws Exception {
		offerDDMService.configure(mockServiceContext);

		verify(mockDDMInitializer, times(1)).getOrCreateDDMStructure(STRUCTURE_KEY, STRUCTURE_RESOURCE_PATH, offerDDMServiceClassLoader, DDLRecordSet.class, mockServiceContext);
	}

}
