package com.placecube.digitalplace.editor.buttons.contributor;

import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.editor.buttons.contributor.service.CKEditorToolbarService;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;
import com.placecube.digitalplace.editor.configuration.CKEditorGuidelineHelpGroupConfiguration;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JSONUtil.class })
public class CKEditorButtonsConfigContributorTest extends Mockito {

	public static final String HELP_DIALOG = "helpDialog";
	private static final String ARTICLE_ID = "article_id";
	private static final long COMPANY_ID = 0L;

	private static final String GUIDANCE_HELP_CONTENT = "GUIDANCE_HELP_CONTENT";
	private static final String HELP_CONTENT = "helpContent";
	private static final String PLUGIN_CONFIG_KEY_1 = "pluginConfigKey1";

	private static final String PLUGIN_CONFIG_KEY_2 = "pluginConfigKey2";
	private static final Map<String, String> PLUGIN_CONFIG_MAP = new HashMap<>() {

		{
			put(PLUGIN_CONFIG_KEY_1, PLUGIN_CONFIG_VAL_1);
			put(PLUGIN_CONFIG_KEY_2, PLUGIN_CONFIG_VAL_2);
		}
	};

	private static final String PLUGIN_CONFIG_VAL_1 = "pluginConfigVal1";
	private static final String PLUGIN_CONFIG_VAL_2 = "pluginConfigVal2";
	private static final long SCOPE_GROUP_ID = 123L;

	@InjectMocks
	private CKEditorButtonsConfigContributor ckEditorButtonsConfigContributor;

	@Mock
	private CKEditorCommonToolbarService mockCKEditorCommonToolbarService;

	@Mock
	private CKEditorConfigurationService mockCKEditorConfigurationService;

	@Mock
	private CKEditorGuidelineHelpGroupConfiguration mockCKEditorGuidelineHelpGroupConfiguration;

	@Mock
	private CKEditorToolbarService mockCKEditorToolbarService;

	@Mock
	private JSONArray mockContentsCss;

	@Mock
	private JSONObject mockEditorJSONConfig;

	@Mock
	private Map<String, Object> mockInputEditorTaglibAttributes;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private JSONObject mockPluginConfig;

	@Mock
	private RequestBackedPortletURLFactory mockRequestBackedPortletURLFactory;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private JSONArray mockToolbar;

	@Before
	public void activateSetup() {
		mockStatic(JSONUtil.class);
	}

	@Test
	public void populateConfigJSONObject_WhenGDSThemeContributorHasCssFiles_ThenAddOnlyNewLTRCssFilesToConfig() {

		final String SERVLET_CONTEXT_PATH = "/o/gds-theme-contributor";
		final String RESOURCES_PATH = "/META-INF/resources";
		final String FILE_PATH_EXISTING_CSS = "/css/file1.css";
		final String FILE_PATH_NEW_CSS = "/css/file2.css";
		final String FILE_PATH_NEW_RTL_CSS = "/css/file_rtl.css";

		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.contains("toolbar"))).thenReturn(null);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		List<String> contentsCssList = new ArrayList<>();
		contentsCssList.add(SERVLET_CONTEXT_PATH + FILE_PATH_EXISTING_CSS);

		Set<String> cssPathSet = new HashSet<>();
		cssPathSet.add(RESOURCES_PATH + FILE_PATH_EXISTING_CSS);
		cssPathSet.add(RESOURCES_PATH + FILE_PATH_NEW_CSS);
		cssPathSet.add(RESOURCES_PATH + FILE_PATH_NEW_RTL_CSS);

		when(mockEditorJSONConfig.getJSONArray("contentsCss")).thenReturn(mockContentsCss);
		when(JSONUtil.toStringList(mockContentsCss)).thenReturn(contentsCssList);
		when(mockServletContext.getResourcePaths(ArgumentMatchers.anyString())).thenReturn(cssPathSet);
		when(mockServletContext.getContextPath()).thenReturn(SERVLET_CONTEXT_PATH);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockContentsCss, never()).put(SERVLET_CONTEXT_PATH + FILE_PATH_EXISTING_CSS);
		verify(mockContentsCss, times(1)).put(SERVLET_CONTEXT_PATH + FILE_PATH_NEW_CSS);
	}

	@Test
	public void populateConfigJSONObject_WhenGDSThemeContributorHasNoFiles_ThenContentsCssConfigIsNotModified() {

		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.contains("toolbar"))).thenReturn(null);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		when(mockEditorJSONConfig.getJSONArray("contentsCss")).thenReturn(mockContentsCss);
		when(JSONUtil.toStringList(mockContentsCss)).thenReturn(new ArrayList<>());
		when(mockServletContext.getResourcePaths(ArgumentMatchers.anyString())).thenReturn(new HashSet<>());

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verifyNoInteractions(mockContentsCss);
	}

	@Test
	public void populateConfigJSONObject_WhenGDSThemeContributorHasNonCssFiles_ThenContentsCssConfigIsNotModified() {

		final String SERVLET_CONTEXT_PATH = "/o/gds-theme-contributor";
		final String RESOURCES_PATH = "/META-INF/resources";
		final String FILE_PATH_DIR = "/css/extra/";
		final String FILE_PATH_SCSS = "/css/file1.scss";

		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.contains("toolbar"))).thenReturn(null);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		Set<String> cssPathSet = new HashSet<>();
		cssPathSet.add(RESOURCES_PATH + FILE_PATH_DIR);
		cssPathSet.add(RESOURCES_PATH + FILE_PATH_SCSS);

		when(mockEditorJSONConfig.getJSONArray("contentsCss")).thenReturn(mockContentsCss);
		when(JSONUtil.toStringList(mockContentsCss)).thenReturn(new ArrayList<>());
		when(mockServletContext.getResourcePaths(ArgumentMatchers.anyString())).thenReturn(cssPathSet);
		when(mockServletContext.getContextPath()).thenReturn(SERVLET_CONTEXT_PATH);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verifyNoInteractions(mockContentsCss);
	}

	@Test
	public void populateConfigJSONObject_WhenPluginConfigIsPresent_ThenAddPluginConfigs() {
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.contains("toolbar"))).thenReturn(null);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		when(mockEditorJSONConfig.getJSONArray("contentsCss")).thenReturn(mockContentsCss);
		when(JSONUtil.toStringList(mockContentsCss)).thenReturn(new ArrayList<>());
		when(mockServletContext.getResourcePaths(ArgumentMatchers.anyString())).thenReturn(new HashSet<>());

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCKEditorConfigurationService.getPluginJsonConfiguration(mockThemeDisplay.getCompanyId())).thenReturn(Optional.of(mockPluginConfig));
		when(mockPluginConfig.keySet()).thenReturn(PLUGIN_CONFIG_MAP.keySet());
		when(mockPluginConfig.get(PLUGIN_CONFIG_KEY_1)).thenReturn(PLUGIN_CONFIG_MAP.get(PLUGIN_CONFIG_KEY_1));
		when(mockPluginConfig.get(PLUGIN_CONFIG_KEY_2)).thenReturn(PLUGIN_CONFIG_MAP.get(PLUGIN_CONFIG_KEY_2));

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, times(1)).put("pluginConfigKey1", mockPluginConfig.get(PLUGIN_CONFIG_KEY_1));
		verify(mockEditorJSONConfig, times(1)).put("pluginConfigKey2", mockPluginConfig.get(PLUGIN_CONFIG_KEY_2));
	}

	@Test
	public void populateConfigJSONObject_WhenPluginConfigNotPresent_ThenAddPluginConfigs() {
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.contains("toolbar"))).thenReturn(null);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		when(mockEditorJSONConfig.getJSONArray("contentsCss")).thenReturn(mockContentsCss);
		when(JSONUtil.toStringList(mockContentsCss)).thenReturn(new ArrayList<>());
		when(mockServletContext.getResourcePaths(ArgumentMatchers.anyString())).thenReturn(new HashSet<>());

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCKEditorConfigurationService.getPluginJsonConfiguration(mockThemeDisplay.getCompanyId())).thenReturn(Optional.empty());

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, never()).put("pluginConfigKey1", mockPluginConfig.get(PLUGIN_CONFIG_KEY_1));
		verify(mockEditorJSONConfig, never()).put("pluginConfigKey2", mockPluginConfig.get(PLUGIN_CONFIG_KEY_2));
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsNotPresent_ThenEmbedButtonsNotAdded() {
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(null);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verifyNoMoreInteractions(mockCKEditorToolbarService);
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAdded_ThenExtraPluginsSettingIsRead() {
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, times(1)).getString("extraPlugins");
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingContainsEmbed_ThenEmbedExtraPluginsSettingIsNotSet() {
		String extraPlugins = "contains-embed";
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorConfigurationService.getGuidanceHelpConfiguration(SCOPE_GROUP_ID)).thenReturn(Optional.of(mockCKEditorGuidelineHelpGroupConfiguration));
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentArticleId()).thenReturn(ARTICLE_ID);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(extraPlugins);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, times(1)).put("extraPlugins", extraPlugins
				+ ",gdsConfirmationPanel,gdsDefaultButton,gdsDetails,gdsDisabledButton,gdsNotificationBanner,gdsSecondaryButton,gdsStartButton,gdsWarningButton,gdsWarningPanel,maximize,scayt,helpDialog");
		verify(mockEditorJSONConfig, times(1)).put(HELP_DIALOG, mockJsonObject);
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingContainsEmbed_ThenEmbedProviderSettingIsNotSet() {
		String providerUrl = "//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}";
		String extraPlugins = "contains-embed";
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorConfigurationService.getGuidanceHelpConfiguration(SCOPE_GROUP_ID)).thenReturn(Optional.of(mockCKEditorGuidelineHelpGroupConfiguration));
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentArticleId()).thenReturn(ARTICLE_ID);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(extraPlugins);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, never()).put("embed_provider", providerUrl);
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingContainsGDSButtons_ThenNotAddButtonsInExtraPluginsSetting() {

		String extraPlugins = "contains-embed,gdsConfirmationPanel,gdsDefaultButton,gdsDetails,gdsDisabledButton,gdsNotificationBanner,gdsSecondaryButton,gdsStartButton,gdsWarningButton,gdsWarningPanel,maximize,scayt,helpDialog";
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockCKEditorCommonToolbarService.buildToolbar(mockThemeDisplay)).thenReturn(mockToolbar);

		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(extraPlugins);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, times(1)).put("extraPlugins", extraPlugins);
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingIsNull_ThenEmbedProviderSettingIsNotSet() {
		String providerUrl = "//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}";
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, never()).put("embed_provider", providerUrl);
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingIsNull_ThenExtraPluginsSettingIsNotSet() {
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(null);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, never()).put(ArgumentMatchers.eq("extraPlugins"), ArgumentMatchers.anyString());
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingNotContainsEmbed_ThenEmbedProviderSettingIsSet() {
		String providerUrl = "//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}";
		String extraPlugins = "not-contains-e-m-b-e-d";
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorConfigurationService.getGuidanceHelpConfiguration(SCOPE_GROUP_ID)).thenReturn(Optional.of(mockCKEditorGuidelineHelpGroupConfiguration));
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentArticleId()).thenReturn(ARTICLE_ID);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(extraPlugins);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, times(1)).put("embed_provider", providerUrl);
	}

	@Test
	public void populateConfigJSONObject_WhenToolbarsPresentAndButtonsAddedAndExtraPluginsSettingNotContainsEmbed_ThenExtraPluginsSettingIsSet() {
		String extraPlugins = "not-contains-e-m-b-e-d";
		when(mockEditorJSONConfig.getJSONArray(ArgumentMatchers.any())).thenReturn(mockToolbar);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorConfigurationService.getGuidanceHelpConfiguration(SCOPE_GROUP_ID)).thenReturn(Optional.of(mockCKEditorGuidelineHelpGroupConfiguration));
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockCKEditorGuidelineHelpGroupConfiguration.helpContentArticleId()).thenReturn(ARTICLE_ID);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockCKEditorToolbarService.addEmbedButtonToToolbar(mockToolbar)).thenReturn(true);
		when(mockEditorJSONConfig.getString("extraPlugins")).thenReturn(extraPlugins);

		ckEditorButtonsConfigContributor.populateConfigJSONObject(mockEditorJSONConfig, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockEditorJSONConfig, times(1)).put("extraPlugins", extraPlugins
				+ ",embed,gdsConfirmationPanel,gdsDefaultButton,gdsDetails,gdsDisabledButton,gdsNotificationBanner,gdsSecondaryButton,gdsStartButton,gdsWarningButton,gdsWarningPanel,maximize,scayt,helpDialog");
	}
}
