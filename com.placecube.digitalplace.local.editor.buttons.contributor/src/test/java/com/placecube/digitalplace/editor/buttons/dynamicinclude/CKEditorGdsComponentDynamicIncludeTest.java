package com.placecube.digitalplace.editor.buttons.dynamicinclude;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarService;

@RunWith(PowerMockRunner.class)
public class CKEditorGdsComponentDynamicIncludeTest extends PowerMockito {

	private static final String CONTEXT_PATH = "/o/digitalplace-editor-buttons-contributor";

	private static final String SCRIPT_TAG = "<script data-senna-track=\"temporary\" src=\"%s\" type=\"text/javascript\"></script>";

	@InjectMocks
	private CKEditorGdsComponentDynamicInclude ckEditorGdsComponentDynamicInclude;

	@Mock
	private CKEditorCommonToolbarService mockCKEditorCommonToolbarService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void include_WhenNoErrorsThrown_ThenPrintWriterCalled() throws Exception {
		when(mockHttpServletResponse.getWriter()).thenReturn(mockPrintWriter);
		when(mockServletContext.getContextPath()).thenReturn(CONTEXT_PATH);

		ckEditorGdsComponentDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "Some key");

		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_confirmation_panel.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_warning_panel.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_notification_banner.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_details.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_default_button.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_disabled_button.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_secondary_button.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_start_button.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/gds_warning_button.js"));
		verify(mockPrintWriter, times(1)).println(String.format(SCRIPT_TAG, CONTEXT_PATH + "/js/ckeditor_help_dialog.js"));
	}

	@Test(expected = IOException.class)
	public void include_WhenWriterNotAvailable_ThenThrowException() throws Exception {
		when(mockHttpServletResponse.getWriter()).thenThrow(new IOException());

		ckEditorGdsComponentDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "Some key");
	}

	@Before
	public void setUp() {
		initMocks(this);
	}
}
