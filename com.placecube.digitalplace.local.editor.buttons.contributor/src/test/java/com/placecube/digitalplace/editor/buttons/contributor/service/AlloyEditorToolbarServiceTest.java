package com.placecube.digitalplace.editor.buttons.contributor.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONObject;

@RunWith(PowerMockRunner.class)
public class AlloyEditorToolbarServiceTest {

	private static final String NEW_BUTTON = "newButton";

	@InjectMocks
	private AlloyEditorToolbarService alloyEditorToolbarService;

	@Mock
	private JSONObject baseJsonObject;

	@Mock
	private JSONObject mockToolbars;

	@Mock
	private JSONObject mockStylesToolbar;

	@Mock
	private JSONArray mockSelectionArray;

	@Mock
	private JSONObject mockSelection;

	@Mock
	private JSONArray mockButtons;

	@Test
	public void addButton_WhenAllToolbarsExist_ThenNewButtonIsAdded() throws JSONException {

		when(baseJsonObject.getJSONObject("toolbars")).thenReturn(mockToolbars);
		when(mockToolbars.getJSONObject("styles")).thenReturn(mockStylesToolbar);
		when(mockStylesToolbar.getJSONArray("selections")).thenReturn(mockSelectionArray);
		when(mockSelectionArray.length()).thenReturn(1);
		when(mockSelectionArray.getJSONObject(0)).thenReturn(mockSelection);
		when(mockSelection.get("name")).thenReturn("text");
		when(mockSelection.getJSONArray("buttons")).thenReturn(mockButtons);

		alloyEditorToolbarService.addButton(baseJsonObject, NEW_BUTTON);

		verify(mockButtons, times(1)).put(NEW_BUTTON);

	}

	@Test
	public void addButton_WhenStylesToolbarDoesNotExist_ThenFailSilentlyNoButtonsAdded() throws JSONException {
		
		when(baseJsonObject.getJSONObject("toolbars")).thenReturn(mockToolbars);
		when(mockToolbars.getJSONObject("styles")).thenReturn(null);

		alloyEditorToolbarService.addButton(baseJsonObject, NEW_BUTTON);

		verify(mockButtons, never()).put(NEW_BUTTON);
	}

	@Test
	public void addButton_WhenStylesToolbarEmpty_ThenNoButtonsAdded() throws JSONException {

		when(baseJsonObject.getJSONObject("toolbars")).thenReturn(mockToolbars);
		when(mockToolbars.getJSONObject("styles")).thenReturn(mockStylesToolbar);
		when(mockStylesToolbar.getJSONArray("selections")).thenReturn(mockSelectionArray);
		when(mockSelectionArray.length()).thenReturn(0);

		alloyEditorToolbarService.addButton(baseJsonObject, NEW_BUTTON);

		verify(mockButtons, never()).put(NEW_BUTTON);

	}

	@Test
	public void addButton_WhenTextSectionNotPresent_ThenNoButtonsAdded() throws JSONException {

		when(baseJsonObject.getJSONObject("toolbars")).thenReturn(mockToolbars);
		when(mockToolbars.getJSONObject("styles")).thenReturn(mockStylesToolbar);
		when(mockStylesToolbar.getJSONArray("selections")).thenReturn(mockSelectionArray);
		when(mockSelectionArray.length()).thenReturn(1);
		when(mockSelectionArray.getJSONObject(0)).thenReturn(mockSelection);
		when(mockSelection.get("name")).thenReturn("noTextHere");

		alloyEditorToolbarService.addButton(baseJsonObject, NEW_BUTTON);

		verify(mockButtons, never()).put(NEW_BUTTON);

	}
}
