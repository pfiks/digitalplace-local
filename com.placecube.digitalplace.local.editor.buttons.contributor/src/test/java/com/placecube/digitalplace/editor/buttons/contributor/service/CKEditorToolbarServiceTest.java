package com.placecube.digitalplace.editor.buttons.contributor.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JSONFactoryUtil.class })
public class CKEditorToolbarServiceTest extends Mockito {

	@InjectMocks
	private CKEditorToolbarService ckEditorToolbarService;

	@Mock
	private JSONArray mockButtonArray;

	@Mock
	private JSONArray mockButtonArray2;

	@Mock
	private CKEditorConfigurationService mockCkEditorConfigurationService;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONArray mockToolbar;

	@Before
	public void activeSetup() {
		mockStatic(JSONFactoryUtil.class);
	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasLengthGtZeroAndHasButtonArrayWithCutValue_ThenAddDividerAndEmbedButtonToArray() throws JSONException {
		when(mockToolbar.length()).thenReturn(1);
		when(mockToolbar.getJSONArray(0)).thenReturn(mockButtonArray);
		when(mockButtonArray.length()).thenReturn(1);
		when(mockButtonArray.getString(0)).thenReturn("Cut");

		ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		InOrder inOrder = inOrder(mockButtonArray);
		inOrder.verify(mockButtonArray, times(1)).put(StringPool.DASH);
		inOrder.verify(mockButtonArray, times(1)).put("Embed");

	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasLengthGtZeroAndHasButtonArrayWithCutValue_ThenReturnTrue() throws JSONException {
		when(mockToolbar.length()).thenReturn(1);
		when(mockToolbar.getJSONArray(0)).thenReturn(mockButtonArray);
		when(mockButtonArray.length()).thenReturn(1);
		when(mockButtonArray.getString(0)).thenReturn("Cut");

		boolean result = ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		assertThat(result, equalTo(true));

	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasLengthGtZeroAndHasButtonArrayWithNoHandledValues_ThenReturnFalse() throws JSONException {
		when(mockToolbar.length()).thenReturn(1);
		when(mockToolbar.getJSONArray(0)).thenReturn(mockButtonArray);

		boolean result = ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		assertThat(result, equalTo(false));

	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasLengthGtZeroAndHasButtonArrayWithSourceValue_ThenReturnTrue() throws JSONException {
		when(mockToolbar.length()).thenReturn(1);
		when(mockToolbar.getJSONArray(0)).thenReturn(mockButtonArray);
		when(mockButtonArray.length()).thenReturn(1);
		when(mockButtonArray.getString(0)).thenReturn("Source");

		boolean result = ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		assertThat(result, equalTo(true));

	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasLengthGtZeroAndNullButtonArray_ThenReturnFalse() throws JSONException {
		when(mockToolbar.length()).thenReturn(1);
		when(mockToolbar.getJSONArray(0)).thenReturn(null);

		boolean result = ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		assertThat(result, equalTo(false));

	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasLengthZero_ThenReturnFalse() throws JSONException {
		when(mockToolbar.length()).thenReturn(0);

		boolean result = ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		assertThat(result, equalTo(false));

	}

	@Test
	public void addEmbedButtonToToolbar_WhenToolbarHasMultipleButtonArraysWhenHandledValueIsFound_ThenAddEmbedButtonToPreviousArray() throws JSONException {
		when(mockToolbar.length()).thenReturn(2);
		when(mockToolbar.getJSONArray(0)).thenReturn(mockButtonArray);
		when(mockToolbar.getJSONArray(1)).thenReturn(mockButtonArray2);

		when(mockButtonArray.length()).thenReturn(1);
		when(mockButtonArray.getString(0)).thenReturn("not-handled-value");

		when(mockButtonArray2.length()).thenReturn(1);
		when(mockButtonArray2.getString(0)).thenReturn("Source");

		ckEditorToolbarService.addEmbedButtonToToolbar(mockToolbar);

		InOrder inOrder = inOrder(mockButtonArray);
		inOrder.verify(mockButtonArray, times(1)).put(StringPool.DASH);
		inOrder.verify(mockButtonArray, times(1)).put("Embed");

	}

	@Test(expected = NullPointerException.class)
	public void addEmbedButtonToToolbar_WhenToolbarIsNull_ThenThrowNullPointerException() throws JSONException {

		boolean result = ckEditorToolbarService.addEmbedButtonToToolbar(null);

		assertThat(result, equalTo(false));

	}

}
