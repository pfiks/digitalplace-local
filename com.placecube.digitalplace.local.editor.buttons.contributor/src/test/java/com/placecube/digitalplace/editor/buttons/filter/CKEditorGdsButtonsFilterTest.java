package com.placecube.digitalplace.editor.buttons.filter;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.Portal;

@RunWith(PowerMockRunner.class)
public class CKEditorGdsButtonsFilterTest extends PowerMockito {

	private static final String FORWARD_CONTEXT_PATH = "/o/digitalplace-editor-buttons-contributor";

	private static final String FORWARD_URL_1 = FORWARD_CONTEXT_PATH + "/js/gds_confirmation_panel.js";

	private static final String FORWARD_URL_2 = FORWARD_CONTEXT_PATH + "/js/gds_confirmation_panel.js.map";

	private static final String FORWARD_URL_3 = FORWARD_CONTEXT_PATH + "/js/gds_warning_panel.js";

	private static final String FORWARD_URL_4 = FORWARD_CONTEXT_PATH + "/js/gds_warning_panel.js.map";

	private static final String FORWARD_URL_5 = FORWARD_CONTEXT_PATH + "/js/gds_details.js";

	private static final String FORWARD_URL_6 = FORWARD_CONTEXT_PATH + "/js/gds_details.js.map";

	private static final String FORWARD_URL_7 = FORWARD_CONTEXT_PATH + "/js/gds_notification_banner.js";

	private static final String FORWARD_URL_8 = FORWARD_CONTEXT_PATH + "/js/gds_notification_banner.js.map";

	private static final String FORWARD_URL_9 = FORWARD_CONTEXT_PATH + "/js/gds_default_button.js";

	private static final String FORWARD_URL_10 = FORWARD_CONTEXT_PATH + "/js/gds_default_button.js.map";

	private static final String FORWARD_URL_11 = FORWARD_CONTEXT_PATH + "/js/gds_disabled_button.js";

	private static final String FORWARD_URL_12 = FORWARD_CONTEXT_PATH + "/js/gds_disabled_button.js.map";

	private static final String FORWARD_URL_13 = FORWARD_CONTEXT_PATH + "/js/gds_secondary_button.js";

	private static final String FORWARD_URL_14 = FORWARD_CONTEXT_PATH + "/js/gds_secondary_button.js.map";

	private static final String FORWARD_URL_15 = FORWARD_CONTEXT_PATH + "/js/gds_start_button.js";

	private static final String FORWARD_URL_16 = FORWARD_CONTEXT_PATH + "/js/gds_start_button.js.map";

	private static final String FORWARD_URL_17 = FORWARD_CONTEXT_PATH + "/js/gds_warning_button.js";

	private static final String FORWARD_URL_18 = FORWARD_CONTEXT_PATH + "/js/gds_warning_button.js.map";
	
	private static final String FORWARD_URL_19 = FORWARD_CONTEXT_PATH + "/js/ckeditor_help_dialog.js.map";

	private static final String REQUEST_CONTEXT_PATH = "/o/frontend-editor-ckeditor-web";

	private static final String REQUEST_URL_1 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsConfirmationPanel/plugin.js";

	private static final String REQUEST_URL_2 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsConfirmationPanel/gds_confirmation_panel.js.map";

	private static final String REQUEST_URL_3 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsWarningPanel/plugin.js";

	private static final String REQUEST_URL_4 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsWarningPanel/gds_warning_panel.js.map";

	private static final String REQUEST_URL_5 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsDetails/plugin.js";

	private static final String REQUEST_URL_6 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsDetails/gds_details.js.map";

	private static final String REQUEST_URL_7 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsNotificationBanner/plugin.js";

	private static final String REQUEST_URL_8 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsNotificationBanner/gds_notification_banner.js.map";

	private static final String REQUEST_URL_9 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsDefaultButton/plugin.js";

	private static final String REQUEST_URL_10 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsDefaultButton/gds_default_button.js.map";

	private static final String REQUEST_URL_11 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsDisabledButton/plugin.js";

	private static final String REQUEST_URL_12 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsDisabledButton/gds_disabled_button.js.map";

	private static final String REQUEST_URL_13 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsSecondaryButton/plugin.js";

	private static final String REQUEST_URL_14 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsSecondaryButton/gds_secondary_button.js.map";

	private static final String REQUEST_URL_15 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsStartButton/plugin.js";

	private static final String REQUEST_URL_16 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsStartButton/gds_start_button.js.map";

	private static final String REQUEST_URL_17 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsWarningButton/plugin.js";

	private static final String REQUEST_URL_18 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/gdsWarningButton/gds_warning_button.js.map";
	
	private static final String REQUEST_URL_19 = REQUEST_CONTEXT_PATH + "/ckeditor/plugins/helpDialog/ckeditor_help_dialog.js.map";

	@InjectMocks
	private CKEditorGdsButtonsFilter ckEditorGdsButtonsFilter;

	@Mock
	private FilterChain mockFilterChain;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private RequestDispatcher mockRequestDispatcher;

	@Mock
	private ServletContext mockServletContext;

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardConfirmationPanelMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_2, FORWARD_URL_2);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardConfirmationPanelScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_1, FORWARD_URL_1);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardDetailsMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_6, FORWARD_URL_6);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardDetailsScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_5, FORWARD_URL_5);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardNotificationBannerMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_8, FORWARD_URL_8);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardNotificationBannerScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_7, FORWARD_URL_7);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardWarningPanelMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_4, FORWARD_URL_4);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardWarningPanelScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_3, FORWARD_URL_3);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardDefaultButtonMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_9, FORWARD_URL_9);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardDefaultButtonScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_10, FORWARD_URL_10);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardDisabledButtonMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_11, FORWARD_URL_11);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardDisabledButtonScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_12, FORWARD_URL_12);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardSecondaryButtonMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_13, FORWARD_URL_13);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardSecondaryButtonScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_14, FORWARD_URL_14);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardStartButtonMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_15, FORWARD_URL_15);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardStartButtonScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_16, FORWARD_URL_16);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardWarningButtonMapScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_17, FORWARD_URL_17);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardWarningButtonScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_18, FORWARD_URL_18);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}
	
	@Test
	public void processFilter_WhenNoErrorsThrown_ThenForwardCKEditorHelpDialogScriptURL() throws Exception {
		mockProcessFilterRequestURL(REQUEST_URL_19, FORWARD_URL_19);
		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

		verify(mockRequestDispatcher, times(1)).forward(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void processFilter_WhenTargetNotAvailable_ThenThrowException() throws Exception {
		when(mockPortal.getCurrentURL(mockHttpServletRequest)).thenReturn(REQUEST_URL_1);
		when(mockServletContext.getContextPath()).thenReturn(FORWARD_CONTEXT_PATH);
		when(mockServletContext.getRequestDispatcher(anyString())).thenReturn(mockRequestDispatcher);

		doThrow(new IOException()).when(mockRequestDispatcher).forward(mockHttpServletRequest, mockHttpServletResponse);

		ckEditorGdsButtonsFilter.processFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

	private void mockProcessFilterRequestURL(String requestURL, String replacedURL) {
		when(mockPortal.getCurrentURL(mockHttpServletRequest)).thenReturn(requestURL);
		when(mockServletContext.getContextPath()).thenReturn(FORWARD_CONTEXT_PATH);
		when(mockServletContext.getRequestDispatcher(replacedURL)).thenReturn(mockRequestDispatcher);
	}
}
