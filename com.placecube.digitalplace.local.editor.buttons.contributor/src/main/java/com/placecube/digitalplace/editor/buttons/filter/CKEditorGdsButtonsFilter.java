package com.placecube.digitalplace.editor.buttons.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.editor.buttons.contributor.CKEditorButtonsConfigContributor;

@Component(immediate = true, property = { "dispatcher=FORWARD", "dispatcher=REQUEST", "servlet-context-name=", "servlet-filter-name=CKEditor GDS Buttons Filter", "before-filter=Virtual Host Filter",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_CONFIRMATION_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_WARNING_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_DETAILS_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_NOTIFICATION_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_DEFAULT_BUTTON_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_DISABLED_BUTTON_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_SECONDARY_BUTTON_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_START_BUTTON_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.GDS_WARNING_BUTTON_PLUGIN_NAME + "/*",
		"url-pattern=/o/frontend-editor-ckeditor-web/ckeditor/plugins/" + CKEditorButtonsConfigContributor.HELP_DIALOG + "/*" }, service = Filter.class)
public class CKEditorGdsButtonsFilter extends BaseFilter {

	private static final Log _log = LogFactoryUtil.getLog(CKEditorGdsButtonsFilter.class);

	@Reference
	private Portal portal;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.local.editor.buttons.contributor)")
	private ServletContext servletContext;

	@Override
	protected Log getLog() {
		return _log;
	}

	@Override
	protected void processFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws Exception {

		String url = portal.getCurrentURL(httpServletRequest);
		url = url.replace("/o/frontend-editor-ckeditor-web/ckeditor/plugins/", servletContext.getContextPath() + "/js/");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_CONFIRMATION_PLUGIN_NAME + "/plugin.js", "/gds_confirmation_panel.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_CONFIRMATION_PLUGIN_NAME + "/gds_confirmation_panel.js.map", "/gds_confirmation_panel.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_WARNING_PLUGIN_NAME + "/plugin.js", "/gds_warning_panel.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_WARNING_PLUGIN_NAME + "/gds_warning_panel.js.map", "/gds_warning_panel.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_DETAILS_PLUGIN_NAME + "/plugin.js", "/gds_details.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_DETAILS_PLUGIN_NAME + "/gds_details.js.map", "/gds_details.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_NOTIFICATION_PLUGIN_NAME + "/plugin.js", "/gds_notification_banner.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_NOTIFICATION_PLUGIN_NAME + "/gds_notification_banner.js.map", "/gds_notification_banner.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_DEFAULT_BUTTON_PLUGIN_NAME + "/plugin.js", "/gds_default_button.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_DEFAULT_BUTTON_PLUGIN_NAME + "/gds_default_button.js.map", "/gds_default_button.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_DISABLED_BUTTON_PLUGIN_NAME + "/plugin.js", "/gds_disabled_button.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_DISABLED_BUTTON_PLUGIN_NAME + "/gds_disabled_button.js.map", "/gds_disabled_button.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_SECONDARY_BUTTON_PLUGIN_NAME + "/plugin.js", "/gds_secondary_button.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_SECONDARY_BUTTON_PLUGIN_NAME + "/gds_secondary_button.js.map", "/gds_secondary_button.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_START_BUTTON_PLUGIN_NAME + "/plugin.js", "/gds_start_button.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_START_BUTTON_PLUGIN_NAME + "/gds_start_button.js.map", "/gds_start_button.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_WARNING_BUTTON_PLUGIN_NAME + "/plugin.js", "/gds_warning_button.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.GDS_WARNING_BUTTON_PLUGIN_NAME + "/gds_warning_button.js.map", "/gds_warning_button.js.map");

		url = url.replace("/" + CKEditorButtonsConfigContributor.HELP_DIALOG + "/plugin.js", "/ckeditor_help_dialog.js");
		url = url.replace("/" + CKEditorButtonsConfigContributor.HELP_DIALOG + "/ckeditor_help_dialog.js.map", "/ckeditor_help_dialog.js.map");

		RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(url);
		requestDispatcher.forward(httpServletRequest, httpServletResponse);
	}

}