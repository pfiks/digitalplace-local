package com.placecube.digitalplace.editor.buttons.contributor.service;

import java.util.HashSet;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = CKEditorToolbarService.class)
public class CKEditorToolbarService {

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private RoleLocalService roleLocalService;

	public boolean addEmbedButtonToToolbar(JSONArray toolbar) {
		JSONArray lastAvailableButtonCollection = null;

		Set<String> buttonNames = new HashSet<>();
		buttonNames.add("Cut");
		buttonNames.add("Source");

		for (int i = 0; i < toolbar.length(); i++) {
			JSONArray currentButtonCollection = toolbar.getJSONArray(i);

			if (Validator.isNotNull(currentButtonCollection)) {

				if (hasButtonInArray(currentButtonCollection, buttonNames)) {
					addEmbedButtonToButtonArray(lastAvailableButtonCollection == null ? currentButtonCollection : lastAvailableButtonCollection);
					return true;
				}

				lastAvailableButtonCollection = currentButtonCollection;
			}

		}

		return false;
	}

	private void addEmbedButtonToButtonArray(JSONArray buttonArray) {
		buttonArray.put(StringPool.DASH);
		buttonArray.put("Embed");
	}

	private boolean hasButtonInArray(JSONArray buttonArray, Set<String> buttonNames) {
		for (int i = 0; i < buttonArray.length(); i++) {
			if (buttonNames.contains(buttonArray.getString(i))) {
				return true;
			}
		}

		return false;
	}
}
