package com.placecube.digitalplace.editor.buttons.contributor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.editor.configuration.BaseEditorConfigContributor;
import com.liferay.portal.kernel.editor.configuration.EditorConfigContributor;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.editor.buttons.contributor.service.CKEditorToolbarService;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;
import com.placecube.digitalplace.editor.configuration.CKEditorGuidelineHelpGroupConfiguration;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarService;

@Component(property = { "editor.name=ckeditor", "editor.name=ckeditor_classic", "service.ranking:Integer=100" }, service = EditorConfigContributor.class)
public class CKEditorButtonsConfigContributor extends BaseEditorConfigContributor {

	public static final String GDS_CONFIRMATION_PLUGIN_NAME = "gdsConfirmationPanel";

	public static final String GDS_DEFAULT_BUTTON_PLUGIN_NAME = "gdsDefaultButton";

	public static final String GDS_DETAILS_PLUGIN_NAME = "gdsDetails";

	public static final String GDS_DISABLED_BUTTON_PLUGIN_NAME = "gdsDisabledButton";

	public static final String GDS_NOTIFICATION_PLUGIN_NAME = "gdsNotificationBanner";

	public static final String GDS_SECONDARY_BUTTON_PLUGIN_NAME = "gdsSecondaryButton";

	public static final String GDS_START_BUTTON_PLUGIN_NAME = "gdsStartButton";

	public static final String GDS_WARNING_BUTTON_PLUGIN_NAME = "gdsWarningButton";

	public static final String GDS_WARNING_PLUGIN_NAME = "gdsWarningPanel";

	public static final String HELP_DIALOG = "helpDialog";

	public static final String MAXIMIZE = "Maximize";

	public static final String MAXIMIZE_PLUGIN = "maximize";

	public static final String SCAYT = "Scayt";

	public static final String SCAYT_PLUGIN = "scayt";

	private static final String EMBED_PLUGIN_NAME = "embed";

	private static final String EXTRA_PLUGINS = "extraPlugins";

	private static final String HELP_DIALOG_ARTICLE_ID = "articleId";

	private static final String HELP_DIALOG_GROUP_ID = "groupId";

	private static final String[] TARGET_TOOLBARS = new String[] { "toolbar_liferayArticle", "toolbar_liferay", "toolbar_tablet", "toolbar_simple", "toolbar_phone", "toolbar_email" };

	@Reference
	private CKEditorCommonToolbarService ckEditorCommonToolbarService;

	@Reference
	private CKEditorConfigurationService ckEditorConfigurationService;

	@Reference
	private CKEditorToolbarService ckEditorToolbarService;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.local.gdsthemecontributor)", cardinality = ReferenceCardinality.OPTIONAL)
	private ServletContext gdsThemeContributorServletContext;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public void populateConfigJSONObject(JSONObject editorJSONConfig, Map<String, Object> inputEditorTaglibAttributes, ThemeDisplay themeDisplay,
			RequestBackedPortletURLFactory requestBackedPortletURLFactory) {

		boolean hasOneEmbedButtonAdded = false;
		for (String toolbarCandidate : TARGET_TOOLBARS) {
			JSONArray currentToolBar = editorJSONConfig.getJSONArray(toolbarCandidate);

			if (Validator.isNotNull(currentToolBar)) {

				editorJSONConfig.put(toolbarCandidate, ckEditorCommonToolbarService.buildToolbar(themeDisplay));

				hasOneEmbedButtonAdded |= ckEditorToolbarService.addEmbedButtonToToolbar(currentToolBar);
			}
		}

		String extraPlugins = editorJSONConfig.getString(EXTRA_PLUGINS);

		if (Validator.isNotNull(extraPlugins)) {

			if (hasOneEmbedButtonAdded && !extraPlugins.contains(EMBED_PLUGIN_NAME)) {
				extraPlugins = extraPlugins + StringPool.COMMA + EMBED_PLUGIN_NAME;
				editorJSONConfig.put("embed_provider", "//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}");
			}
			if (!extraPlugins.contains(GDS_CONFIRMATION_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_CONFIRMATION_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_DEFAULT_BUTTON_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_DEFAULT_BUTTON_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_DETAILS_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_DETAILS_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_DISABLED_BUTTON_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_DISABLED_BUTTON_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_NOTIFICATION_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_NOTIFICATION_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_SECONDARY_BUTTON_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_SECONDARY_BUTTON_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_START_BUTTON_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_START_BUTTON_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_WARNING_BUTTON_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_WARNING_BUTTON_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(GDS_WARNING_PLUGIN_NAME)) {
				extraPlugins += StringPool.COMMA + GDS_WARNING_PLUGIN_NAME;
			}
			if (!extraPlugins.contains(MAXIMIZE_PLUGIN)) {
				extraPlugins += StringPool.COMMA + MAXIMIZE_PLUGIN;
			}
			if (!extraPlugins.contains(SCAYT_PLUGIN)) {
				extraPlugins += StringPool.COMMA + SCAYT_PLUGIN;
			}
			if (!extraPlugins.contains(HELP_DIALOG)) {
				extraPlugins += StringPool.COMMA + HELP_DIALOG;
				Optional<CKEditorGuidelineHelpGroupConfiguration> ckEditorGuidelineHelpGroupConfigurationOptional = ckEditorConfigurationService
						.getGuidanceHelpConfiguration(themeDisplay.getScopeGroupId());
				if (ckEditorGuidelineHelpGroupConfigurationOptional.isPresent()) {
					CKEditorGuidelineHelpGroupConfiguration ckEditorGuidelineHelpGroupConfiguration = ckEditorGuidelineHelpGroupConfigurationOptional.get();
					JSONObject helpDialog = jsonFactory.createJSONObject();
					helpDialog.put(HELP_DIALOG_GROUP_ID, ckEditorGuidelineHelpGroupConfiguration.helpContentGroupId());
					helpDialog.put(HELP_DIALOG_ARTICLE_ID, ckEditorGuidelineHelpGroupConfiguration.helpContentArticleId().trim());
					editorJSONConfig.put(HELP_DIALOG, helpDialog);
				}
			}

			editorJSONConfig.put(EXTRA_PLUGINS, extraPlugins);
		}

		if (gdsThemeContributorServletContext != null) {
			applyCSSFromGDSThemeContributor(editorJSONConfig);
		}

		Optional<JSONObject> optPluginConfig = ckEditorConfigurationService.getPluginJsonConfiguration(themeDisplay.getCompanyId());

		if (optPluginConfig.isPresent()) {

			JSONObject pluginConfig = optPluginConfig.get();
			for (String key : pluginConfig.keySet()) {
				editorJSONConfig.put(key, pluginConfig.get(key));
			}
		}
	}

	private void applyCSSFromGDSThemeContributor(JSONObject editorJSONConfig) {
		// Apply all CSS files from the GDS Theme Contributor module to the
		// CKEditor's iframe
		JSONArray contentsCss = editorJSONConfig.getJSONArray("contentsCss");
		List<String> contentsCssList = JSONUtil.toStringList(contentsCss);

		for (String cssPath : gdsThemeContributorServletContext.getResourcePaths("/META-INF/resources/css")) {
			cssPath = cssPath.replace("/META-INF/resources", gdsThemeContributorServletContext.getContextPath());
			if (cssPath.endsWith(".css") && !cssPath.endsWith("rtl.css") && !contentsCssList.contains(cssPath)) {
				contentsCss.put(cssPath);
			}
		}

	}
}
