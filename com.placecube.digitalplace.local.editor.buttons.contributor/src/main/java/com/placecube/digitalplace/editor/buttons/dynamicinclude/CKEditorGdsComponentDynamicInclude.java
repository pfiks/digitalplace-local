package com.placecube.digitalplace.editor.buttons.dynamicinclude;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.servlet.taglib.BaseDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;

@Component(immediate = true, service = DynamicInclude.class)
public class CKEditorGdsComponentDynamicInclude extends BaseDynamicInclude {

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.local.editor.buttons.contributor)")
	private ServletContext servletContext;

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {

		PrintWriter printWriter = httpServletResponse.getWriter();
		printWriter.println(getIncludeString("/js/gds_confirmation_panel.js"));
		printWriter.println(getIncludeString("/js/gds_warning_panel.js"));
		printWriter.println(getIncludeString("/js/gds_notification_banner.js"));
		printWriter.println(getIncludeString("/js/gds_details.js"));
		printWriter.println(getIncludeString("/js/gds_default_button.js"));
		printWriter.println(getIncludeString("/js/gds_disabled_button.js"));
		printWriter.println(getIncludeString("/js/gds_secondary_button.js"));
		printWriter.println(getIncludeString("/js/gds_start_button.js"));
		printWriter.println(getIncludeString("/js/gds_warning_button.js"));
		printWriter.println(getIncludeString("/js/ckeditor_help_dialog.js"));
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("com.liferay.frontend.editor.ckeditor.web#ckeditor#" + "additionalResources");
	}

	private String getIncludeString(String srcPath) {
		String scriptURL = servletContext.getContextPath() + srcPath;
		return "<script data-senna-track=\"temporary\" src=\"" + scriptURL + "\" type=\"text/javascript\"></script>";
	}
}