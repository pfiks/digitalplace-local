package com.placecube.digitalplace.editor.buttons.contributor.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Component(immediate = true, service = AlloyEditorToolbarService.class)
public class AlloyEditorToolbarService {

	private static final Log LOG = LogFactoryUtil.getLog(AlloyEditorToolbarService.class);

	public void addButton(JSONObject editorConfig, String buttonName) {
		try {
			JSONObject toolbarsJSONObject = editorConfig.getJSONObject("toolbars");

			JSONObject stylesToolbar = toolbarsJSONObject.getJSONObject("styles");

			JSONArray selectionsJSONArray = stylesToolbar.getJSONArray("selections");

			for (int i = 0; i < selectionsJSONArray.length(); i++) {
				JSONObject selection = selectionsJSONArray.getJSONObject(i);

				if ("text".equals(selection.get("name"))) {
					JSONArray buttons = selection.getJSONArray("buttons");
					buttons.put(buttonName);
				}
			}
		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Error while adding new button to Alloyeditor: " + e.getMessage());
		}

	}

}
