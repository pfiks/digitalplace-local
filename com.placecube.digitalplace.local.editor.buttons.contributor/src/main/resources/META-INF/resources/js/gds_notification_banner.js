(function () {
    CKEDITOR.plugins.add('gdsNotificationBanner', {
        init: function (editor) {
            var instance = this;

            editor.addCommand('gdsNotificationBanner', {
                canUndo: false,
                exec: function () {
                    instance._replaceSelected(editor);
                }
            });

            if (editor.ui.addButton) {
                editor.ui.addButton('gdsNotificationBanner', {
                    command: 'gdsNotificationBanner',
                    label: 'GDS Notification Banner'
                });

                this._gdsIcon('info-panel-open');
            }
        },

        _replaceSelected: function (editor) {
            let html = this._gdsElement(editor.getSelectedHtml(true));
            let range;
            if (editor.window.$.getSelection && editor.window.$.getSelection().getRangeAt) {
                range = editor.window.$.getSelection().getRangeAt(0);
                range.deleteContents();
                let div = document.createElement("div");
                div.innerHTML = html;
                let frag = document.createDocumentFragment(), child;
                while ( (child = div.firstChild) ) {
                    frag.appendChild(child);
                }
                range.insertNode(frag);
                editor.window.$.getSelection().collapseToEnd();
            } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                range.pasteHTML(html);
                editor.window.$.getSelection().collapseToEnd();
            }
        },

        _gdsElement: function (text) {
            let placeholder = "Message";
            if(text){
                placeholder = text;
            }
            return "<div class=\"govuk-notification-banner\" role=\"region\" aria-labelledby=\"govuk-notification-banner-title\" data-module=\"govuk-notification-banner\">" +
                        "<div class=\"govuk-notification-banner__header\">" +
                            "<h2 class=\"govuk-notification-banner__title\" id=\"govuk-notification-banner-title\">" +
                                "Important" +
                            "</h2>" +
                        "</div>" +
                        "<div class=\"govuk-notification-banner__content\">" +
                            "<p class=\"govuk-notification-banner__heading\">" +
                                placeholder +
                           "</p>" +
                        "</div>" +
                   "</div>";
        },

        _gdsIcon: function (icon) {
            var instance = this;

            $.get("/o/admin-theme/images/clay/icons.svg", function( svg ) {
                var $styles = $('<style data-senna-track="temporary" type="text/css"></style>');
                var $svg = $(svg);
                var $icon = $svg.find('#'+icon);
                var $iconElem = $svg.find('svg').attr('viewBox', $icon.attr('viewBox')).html($icon.html());

                instance._gdsIconCssRule($styles, $iconElem, '.cke_button', '6b6c7e');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button.cke_button_on', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button:not(.cke_button_disabled):hover', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button:not(.cke_button_disabled):focus', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button.cke_button_disabled', 'a7a9bc');

                document.head.appendChild($styles[0]);
            });
        },

        _gdsIconCssRule: function ($styles, $iconElem, key, fill) {
            $styles.append(`
                ${key} .cke_button__gdsnotificationbanner_icon {
                    background: url('data:image/svg+xml;charset=utf8, ${encodeURIComponent($iconElem.attr('fill','#'+fill).prop('outerHTML'))}') !important;
                }
            `);
        }
    });
})();