(function () {
    CKEDITOR.plugins.add('gdsStartButton', {
        init: function (editor) {
            var instance = this;

            editor.addCommand('gdsStartButton', {
                canUndo: false,
                exec: function () {
                    instance._replaceSelected(editor);
                }
            });

            if (editor.ui.addButton) {
                editor.ui.addButton('gdsStartButton', {
                    command: 'gdsStartButton',
                    label: 'GDS Start Button'
                });

                this._gdsIcon('arrow-end');
            }
        },

        _replaceSelected: function (editor) {
            let html = this._gdsElement(editor.getSelectedHtml(true));
            let range;
            if (editor.window.$.getSelection && editor.window.$.getSelection().getRangeAt) {
                range = editor.window.$.getSelection().getRangeAt(0);
                range.deleteContents();
                let div = document.createElement("div");
                div.innerHTML = html;
                let frag = document.createDocumentFragment(), child;
                while ( (child = div.firstChild) ) {
                    frag.appendChild(child);
                }
                range.insertNode(frag);
             } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                range.pasteHTML(html);
            }
        },

        _gdsElement: function (text) {
			let title = "Placeholder title";
			if (text) {
				title = text;
			}
            return "<a role='button' draggable='false' class='govuk-button govuk-button--start' data-module='govuk-button'>" +
            	title +
				"<svg class='govuk-button__start-icon' xmlns='http://www.w3.org/2000/svg' width='17.5' height='19' viewBox='0 0 33 40' aria-hidden='true' focusable='false'>" +
					"<path fill='currentColor' d='M0 0h13l20 20-20 20H0l20-20z' />" +
				"</svg>" +
			"</a>";
        },

        _gdsIcon: function (icon) {
            var instance = this;

            $.get("/o/admin-theme/images/clay/icons.svg", function( svg ) {
                var $styles = $('<style data-senna-track="temporary" type="text/css"></style>');
                var $svg = $(svg);
                var $icon = $svg.find('#'+icon);
                var $iconElem = $svg.find('svg').attr('viewBox', $icon.attr('viewBox')).html($icon.html());

                instance._gdsIconCssRule($styles, $iconElem, '.cke_button', '6b6c7e');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button.cke_button_on', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button:not(.cke_button_disabled):hover', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button:not(.cke_button_disabled):focus', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button.cke_button_disabled', 'a7a9bc');

                document.head.appendChild($styles[0]);
            });
        },

        _gdsIconCssRule: function ($styles, $iconElem, key, fill) {
            $styles.append(`
                ${key} .cke_button__gdsstartbutton_icon {
                    background: url('data:image/svg+xml;charset=utf8, ${encodeURIComponent($iconElem.attr('fill','#'+fill).prop('outerHTML'))}') !important;
                }
            `);
        }
    });
})();