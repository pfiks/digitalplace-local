(function () {
    CKEDITOR.plugins.add('gdsConfirmationPanel', {
        init: function (editor) {
            var instance = this;

            editor.addCommand('gdsConfirmationPanel', {
                canUndo: false,
                exec: function () {
                    instance._replaceSelected(editor);
                }
            });

            if (editor.ui.addButton) {
                editor.ui.addButton('gdsConfirmationPanel', {
                    command: 'gdsConfirmationPanel',
                    label: 'GDS Confirmation Panel'
                });

                this._gdsIcon('star');
            }
        },

        _replaceSelected: function (editor) {
            let html = this._gdsElement(editor.getSelectedHtml(true));
            let range;
            if (editor.window.$.getSelection && editor.window.$.getSelection().getRangeAt) {
                range = editor.window.$.getSelection().getRangeAt(0);
                range.deleteContents();
                let div = document.createElement("div");
                div.innerHTML = html;
                let frag = document.createDocumentFragment(), child;
                while ( (child = div.firstChild) ) {
                    frag.appendChild(child);
                }
                range.insertNode(frag);
             } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                range.pasteHTML(html);
            }
        },

        _gdsElement: function (text) {
            return "<div class=\"govuk-panel govuk-panel--confirmation\">" +
                    "<h1 class=\"govuk-panel__title\">" +
                        text +
                    "</h1>" +
                    "<div class=\"govuk-panel__body\">" +
                        "Placeholder body" +
                    "</div>" +
                "</div>";
        },

        _gdsIcon: function (icon) {
            var instance = this;

            $.get("/o/admin-theme/images/clay/icons.svg", function( svg ) {
                var $styles = $('<style data-senna-track="temporary" type="text/css"></style>');
                var $svg = $(svg);
                var $icon = $svg.find('#'+icon);
                var $iconElem = $svg.find('svg').attr('viewBox', $icon.attr('viewBox')).html($icon.html());

                instance._gdsIconCssRule($styles, $iconElem, '.cke_button', '6b6c7e');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button.cke_button_on', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button:not(.cke_button_disabled):hover', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button:not(.cke_button_disabled):focus', '272833');
                instance._gdsIconCssRule($styles, $iconElem, '.cke_button.cke_button_disabled', 'a7a9bc');

                document.head.appendChild($styles[0]);
            });
        },

        _gdsIconCssRule: function ($styles, $iconElem, key, fill) {
            $styles.append(`
                ${key} .cke_button__gdsconfirmationpanel_icon {
                    background: url('data:image/svg+xml;charset=utf8, ${encodeURIComponent($iconElem.attr('fill','#'+fill).prop('outerHTML'))}') !important;
                }
            `);
        }
    });
})();