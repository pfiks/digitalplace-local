if (!CKEDITOR.plugins.registered.helpDialog) {
	CKEDITOR.plugins.add('helpDialog', {
		init: function (editor) {

			editor.addCommand('helpDialog', new CKEDITOR.dialogCommand('helpDialog'));
			editor.ui.addButton('HelpDialog', {
				label: 'Help',
				icon: 'about',
				command: 'helpDialog'
			});

			CKEDITOR.dialog.add('helpDialog', function (editor) {
				return {
					title: 'Guideline',
					minWidth: 400,
					minHeight: 100,
					contents: [
						{
							id: 'tab1',
							label: '',
							title: '',
							elements: [
								{
									id: 'content',
									type: 'html',
									html: '<div style="max-height: 500px; min-height: 200px;">'+Liferay.Language.get('ckeditor-help-content-article-not-configured')+'</div>'
								}
							]
						}
					],
					buttons: [ CKEDITOR.dialog.cancelButton ],
					onShow: function() {
						$('body').children('.cke_dialog_container').removeClass('cke_reset_all');
						setArticleContent(editor.config.helpDialog.groupId, editor.config.helpDialog.articleId, this);
					},
					onCancel: function() {
						$('body').children('.cke_dialog_container').addClass('cke_reset_all');
					}
				};
			});
		}
	});
	
	function setArticleContent(groupId, articleId, dialog) {
		Liferay.Service(
			'/journal.journalarticle/get-article',
			{
				'articleId': articleId,
				'groupId': groupId
			},
			function(obj) {
				if (obj.content) {
					let parser = new DOMParser();

					let xmlDoc = parser.parseFromString(obj.content, 'text/xml');

					let content = [];
					let dynamicContents = xmlDoc.querySelectorAll('dynamic-content[language-id="' + Liferay.ThemeDisplay.getLanguageId() + '"]');
					dynamicContents.forEach(function(dynamicContent) {
						content.push(dynamicContent.textContent.trim());
					});

					let eHtml = dialog.getContentElement('tab1', 'content').getElement();
					eHtml.setHtml('<div style="max-height: 500px; min-height: 200px;">'+content+'</div>');
					$('.cke_dialog').css('top', '180px');
				}
			}
		);
	}
}