package com.placecube.digitalplace.local.ddmform.dataprovider;

import com.liferay.dynamic.data.mapping.data.provider.DDMDataProvider;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderException;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRequest;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponse;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component(
		configurationPid = "com.liferay.dynamic.data.mapping.data.provider.configuration.DDMDataProviderConfiguration",
		property = {"ddm.data.provider.type=rest", "service.ranking:Integer=100"}, service = DDMDataProvider.class)
public class DigitalPlaceDDMRestDataProvider implements DDMDataProvider {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceDDMRestDataProvider.class);

	@Reference(
			target = "(component.name=com.liferay.dynamic.data.mapping.data.provider.internal.rest.DDMRESTDataProvider)"
	)
	DDMDataProvider ddmRESTDataProvider;

	@Reference
	private DDMDataProviderInstanceLocalService ddmDataProviderInstanceService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Override
	public DDMDataProviderResponse getData(DDMDataProviderRequest ddmDataProviderRequest) throws DDMDataProviderException {

		LOG.debug("CompanyThreadLocal.getCompanyId() - " + CompanyThreadLocal.getCompanyId());
		LOG.debug("ddmDataProviderRequest.getCompanyId() - " + ddmDataProviderRequest.getCompanyId());
		LOG.debug("ddmDataProviderRequest.getDDMDataProviderId() - " + ddmDataProviderRequest.getDDMDataProviderId());

		long originalCompanyId = CompanyThreadLocal.getCompanyId();

		if (CompanyThreadLocal.getCompanyId() == 0 || CompanyThreadLocal.getCompanyId() != ddmDataProviderRequest.getCompanyId()) {

			long requestCompanyId = ddmDataProviderRequest.getCompanyId();

			if (requestCompanyId != 0) {

				CompanyThreadLocal.setCompanyId(requestCompanyId);
				LOG.debug("Set companyId to " + requestCompanyId + " in CompanyThreadLocal to match DDMDataProviderRequest");

			} else {

				List<DDMDataProviderInstance> ddmDataProviderInstances;

				for (Company company : companyLocalService.getCompanies()) {

					long companyId = company.getCompanyId();

					CompanyThreadLocal.setCompanyId(companyId);
					LOG.debug("Retrieving DDMDataProviderInstances by companyId: " + companyId + " and UUID: " + ddmDataProviderRequest.getDDMDataProviderId());
					ddmDataProviderInstances = ddmDataProviderInstanceService.getDDMDataProviderInstancesByUuidAndCompanyId(ddmDataProviderRequest.getDDMDataProviderId(), companyId);

					if (!ddmDataProviderInstances.isEmpty()) {
						LOG.debug("Found DDMDataProviderInstance in companyId: " + companyId);
						break;
					} else {
						CompanyThreadLocal.setCompanyId(originalCompanyId);
					}

				}

			}

		}

		DDMDataProviderResponse ddmDataProviderResponse = ddmRESTDataProvider.getData(ddmDataProviderRequest);
		CompanyThreadLocal.setCompanyId(originalCompanyId);
		LOG.debug("Setting CompanyThreadLocal companyId to original value: " + originalCompanyId);

		return ddmDataProviderResponse;
	}

	@Override
	public Class<?> getSettings() {
		return ddmRESTDataProvider.getSettings();
	}

}
