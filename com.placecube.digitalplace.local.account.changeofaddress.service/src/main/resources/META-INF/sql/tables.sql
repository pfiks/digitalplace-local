create table Placecube_Account_Address_ChangeOfAddress (
	uuid_ VARCHAR(75) null,
	changeOfAddressId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	currentAddressId LONG,
	newAddressId LONG,
	formInstanceRecordId LONG,
	addressUpdateDate DATE null
);