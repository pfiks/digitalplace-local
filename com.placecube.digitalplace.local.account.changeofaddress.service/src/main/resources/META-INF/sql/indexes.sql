create index IX_B3FCEEB8 on Placecube_Account_Address_ChangeOfAddress (companyId, newAddressId);
create index IX_B8376FCC on Placecube_Account_Address_ChangeOfAddress (formInstanceRecordId);
create index IX_2FCD3EE1 on Placecube_Account_Address_ChangeOfAddress (uuid_[$COLUMN_LENGTH:75$]);