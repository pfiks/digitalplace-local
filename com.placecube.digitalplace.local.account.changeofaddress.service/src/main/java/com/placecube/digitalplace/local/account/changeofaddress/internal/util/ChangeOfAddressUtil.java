package com.placecube.digitalplace.local.account.changeofaddress.internal.util;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.pfiks.mail.service.MailService;
import com.placecube.ddmform.fieldtype.address.util.AddressDDMFormFieldTypeUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.local.account.changeofaddress.exception.NoSuchChangeOfAddressException;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressFormConstants;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.service.ChangeOfAddressLocalService;

@Component(immediate = true, service = ChangeOfAddressUtil.class)
public class ChangeOfAddressUtil {

	private static final Log LOG = LogFactoryUtil.getLog(ChangeOfAddressUtil.class);

	@Reference
	private AddressDDMFormFieldTypeUtil addressDDMFormFieldTypeUtil;

	@Reference
	private AddressLookupService addressLookupService;

	@Reference
	private AddressTypeConfigurationService addressTypeConfigurationService;

	@Reference
	private ChangeOfAddressContentUtil changeOfAddressContentUtil;

	@Reference
	private ChangeOfAddressLocalService changeOfAddressLocalService;

	@Reference
	private MailService mailService;

	@Reference
	private UserLocalService userLocalService;

	public void processFormInstanceRecord(DDMFormInstanceRecord model) throws PortalException {
		DDMFormValues ddmFormValues = model.getDDMFormValues();
		Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = ddmFormValues.getDDMFormFieldValuesMap();
		Locale defaultLocale = ddmFormValues.getDefaultLocale();
		try {
			Date moveOutDate = ChangeOfAddressFormUtil.getMoveOutDate(ddmFormFieldValuesMap, defaultLocale);
			Date now = new Date();

			ChangeOfAddress changeOfAddress = changeOfAddressLocalService.addChangeOfAddress(model.getGroupId(), model.getCompanyId(), model.getUserId(), model.getFormInstanceRecordId(), moveOutDate);

			if (moveOutDate.before(now)) {
				ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
				JournalArticle article = changeOfAddressContentUtil.getOrCreateJournalArticleEmail(serviceContext);

				String subject = changeOfAddressContentUtil.getSubject(article, defaultLocale);
				String body = changeOfAddressContentUtil.getBody(article, defaultLocale);

				updateAddress(model, ddmFormValues, ddmFormFieldValuesMap, defaultLocale, changeOfAddress, subject, body, serviceContext);
			}
		} catch (Exception e) {
			LOG.error("Error processing form instance record with formInstanceRecordId: " + model.getFormInstanceRecordId());
			throw new PortalException(e);
		}
	}

	public void removeChangeOfAddressByFormInstanceRecordId(long formInstanceRecordId) throws NoSuchChangeOfAddressException {
		ChangeOfAddress changeOfAddress = changeOfAddressLocalService.deleteByFormInstanceRecordId(formInstanceRecordId);

		if (LOG.isDebugEnabled()) {
			LOG.debug("Removed change of address with id: " + changeOfAddress.getChangeOfAddressId());
		}
	}

	public void updateAddress(DDMFormInstanceRecord formRecord, DDMFormValues ddmFormValues, Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, Locale defaultLocale,
			ChangeOfAddress changeOfAddress, String emailSubject, String emailBody, ServiceContext serviceContext) throws PortalException {
		User user = userLocalService.getUser(formRecord.getUserId());
		long companyId = formRecord.getCompanyId();

		AddressContext addressContext = null;

		if (ChangeOfAddressFormUtil.isSameCouncil(ddmFormFieldValuesMap, defaultLocale)) {
			Map<String, Object> addressMap = addressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(ddmFormValues);

			Optional<Object> uprnOpt = addressDDMFormFieldTypeUtil.getUPRN(addressMap);

			if (uprnOpt.isPresent()) {
				String uprn = GetterUtil.getString(uprnOpt.get());

				try {
					Optional<AddressContext> addressContextOpt = addressLookupService.getByUprn(companyId, uprn, new String[0], false);

					if (addressContextOpt.isPresent()) {
						addressContext = addressContextOpt.get();
					}
				} catch (Exception e) {
					LOG.error("Exception configuring user address - uprn: " + uprn + " formInstanceRecordId: " + formRecord.getFormInstanceRecordId());
					throw new PortalException(e);
				}
			}
		} else {
			String street1 = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_1_FIELD_NAME, defaultLocale);
			String street2 = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_2_FIELD_NAME, defaultLocale);
			String street3 = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_3_FIELD_NAME, defaultLocale);
			String city = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.CITY_FIELD_NAME, defaultLocale);
			String postcode = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.POSTCODE_FIELD_NAME, defaultLocale);

			addressContext = new AddressContext(StringPool.BLANK, street1, street2, street3, StringPool.BLANK, city, postcode);
		}

		if (addressContext == null) {
			throw new PortalException("AddressContext is null!");
		}
		addressContext.setAddressType(addressTypeConfigurationService.getDefaultAddressType(companyId));

		Address updatedAddress = addressLookupService.saveAddress(user, addressContext, serviceContext);

		changeOfAddress.setNewAddressId(updatedAddress.getAddressId());
		changeOfAddress.setModifiedDate(new Date());
		changeOfAddressLocalService.updateChangeOfAddress(changeOfAddress);

		try {
			String toEmailAddress = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.EMAIL_ADDRESS_FIELD_NAME, defaultLocale);
			emailSubject = changeOfAddressContentUtil.replaceEmailValues(emailSubject, ddmFormFieldValuesMap, defaultLocale, user);
			emailBody = changeOfAddressContentUtil.replaceEmailValues(emailBody, ddmFormFieldValuesMap, defaultLocale, user);
			sendConfirmationEmail(emailSubject, emailBody, toEmailAddress);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	private void sendConfirmationEmail(String subject, String body, String toAddress) {
		try {
			mailService.sendEmail(toAddress, toAddress, subject, body);
		} catch (Exception e) {
			LOG.error("Unable to send change of address confirmation email", e);
		}
	}
}