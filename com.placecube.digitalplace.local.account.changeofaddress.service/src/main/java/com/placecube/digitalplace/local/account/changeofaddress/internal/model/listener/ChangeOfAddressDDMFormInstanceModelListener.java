package com.placecube.digitalplace.local.account.changeofaddress.internal.model.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressFormConstants;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressUtil;

@Component(immediate = true, service = ModelListener.class)
public class ChangeOfAddressDDMFormInstanceModelListener extends BaseModelListener<DDMFormInstanceRecord> {

	private static final Log LOG = LogFactoryUtil.getLog(ChangeOfAddressDDMFormInstanceModelListener.class);

	@Reference
	private ChangeOfAddressUtil changeOfAddressUtil;

	@Override
	public void onAfterRemove(DDMFormInstanceRecord model) throws ModelListenerException {

		try {
			if (model.getFormInstance().getStructure().getStructureKey().equals(ChangeOfAddressFormConstants.CHANGE_OF_ADDRESS_STRUCTURE_KEY)) {
				changeOfAddressUtil.removeChangeOfAddressByFormInstanceRecordId(model.getFormInstanceRecordId());
			}
		} catch (PortalException e) {
			LOG.error("Cannot remove change of address for formInstanceRecordId: " + model.getFormInstanceRecordId(), e);
		}

		super.onAfterRemove(model);
	}

	@Override
	public void onAfterUpdate(DDMFormInstanceRecord originalModel, DDMFormInstanceRecord model) throws ModelListenerException {

		try {
			if (model.getFormInstance().getStructure().getStructureKey().equals(ChangeOfAddressFormConstants.CHANGE_OF_ADDRESS_STRUCTURE_KEY)) {
				changeOfAddressUtil.processFormInstanceRecord(model);
			}
		} catch (PortalException e) {
			LOG.error(e);
		}

		super.onAfterUpdate(originalModel, model);
	}
}
