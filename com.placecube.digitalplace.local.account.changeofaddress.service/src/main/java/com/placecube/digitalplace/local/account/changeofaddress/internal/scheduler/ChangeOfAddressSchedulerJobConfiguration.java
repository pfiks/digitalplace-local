package com.placecube.digitalplace.local.account.changeofaddress.internal.scheduler;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeRunnable;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.scheduler.SchedulerJobConfiguration;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.placecube.digitalplace.local.account.changeofaddress.configuration.ChangeOfAddressConfiguration;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressContentUtil;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressUtil;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.service.ChangeOfAddressLocalService;

@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE, configurationPid = "com.placecube.digitalplace.local.account.changeofaddress.configuration.ChangeOfAddressConfiguration", service = SchedulerJobConfiguration.class)
public class ChangeOfAddressSchedulerJobConfiguration implements SchedulerJobConfiguration {

	private static final Log LOG = LogFactoryUtil.getLog(ChangeOfAddressSchedulerJobConfiguration.class);

	@Reference
	private ChangeOfAddressContentUtil changeOfAddressContentUtil;

	@Reference
	private ChangeOfAddressLocalService changeOfAddressLocalService;

	@Reference
	private ChangeOfAddressUtil changeOfAddressUtil;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMFormInstanceRecordLocalService ddmFormInstanceRecordLocalService;

	@Reference
	private UserLocalService userLocalService;

	private ChangeOfAddressConfiguration changeOfAddressConfiguration;

	@Activate
	protected void activate(Map<String, Object> properties) {
		changeOfAddressConfiguration = ConfigurableUtil.createConfigurable(ChangeOfAddressConfiguration.class, properties);
	}

	@Override
	public UnsafeConsumer<Long, Exception> getCompanyJobExecutorUnsafeConsumer() {
		return companyId -> {
			if (changeOfAddressConfiguration.schedulerEnabled()) {
				runJobForCompany(companyId);
			}
		};
	}

	@Override
	public UnsafeRunnable<Exception> getJobExecutorUnsafeRunnable() {
		return () -> {
			if (changeOfAddressConfiguration.schedulerEnabled()) {
				companyLocalService.forEachCompanyId(this::runJobForCompany);
			}
		};
	}

	@Override
	public TriggerConfiguration getTriggerConfiguration() {

		String cronExpression = changeOfAddressConfiguration.schedulerCronExpression();

		TriggerConfiguration triggerConfiguration = TriggerConfiguration.createTriggerConfiguration(cronExpression);
		triggerConfiguration.setStartDate(new Date());

		return triggerConfiguration;
	}

	private void runJobForCompany(long companyId) throws PortalException, IOException, PortletException {
		CompanyThreadLocal.setCompanyId(companyId);
		LOG.debug("Running scheduled change of address task for company with companyId: " + companyId);

		List<ChangeOfAddress> addressChangesToUpdate = changeOfAddressLocalService.getChangeOfAddressesToUpdate(companyId);

		ServiceContext serviceContext = new ServiceContext();
		long groupId = 0;
		String emailSubject = StringPool.BLANK;
		String emailBody = StringPool.BLANK;

		serviceContext.setCompanyId(companyId);
		serviceContext.setUserId(userLocalService.getGuestUserId(companyId));

		for (ChangeOfAddress changeOfAddress : addressChangesToUpdate) {
			DDMFormInstanceRecord formRecord = ddmFormInstanceRecordLocalService.getDDMFormInstanceRecord(changeOfAddress.getFormInstanceRecordId());
			DDMFormValues ddmFormValues = formRecord.getDDMFormValues();
			Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap = ddmFormValues.getDDMFormFieldValuesMap();
			Locale defaultLocale = ddmFormValues.getDefaultLocale();

			serviceContext.setUuid(PortalUUIDUtil.generate());

			if (groupId != changeOfAddress.getGroupId()) {
				groupId = changeOfAddress.getGroupId();

				serviceContext.setScopeGroupId(groupId);

				JournalArticle article = changeOfAddressContentUtil.getOrCreateJournalArticleEmail(serviceContext);

				emailSubject = changeOfAddressContentUtil.getSubject(article, defaultLocale);
				emailBody = changeOfAddressContentUtil.getBody(article, defaultLocale);
			}

			changeOfAddressUtil.updateAddress(formRecord, ddmFormValues, ddmFormFieldValuesMap, defaultLocale, changeOfAddress, emailSubject, emailBody, serviceContext);
		}

		LOG.debug("Change of address scheduled task finished. Updated data on " + addressChangesToUpdate.size() + " elements.");
	}

}
