package com.placecube.digitalplace.local.account.changeofaddress.internal.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressContentUtil;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class ChangeOfAddressEmailWebContentLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(ChangeOfAddressEmailWebContentLifecycleListener.class);

	@Reference
	private ChangeOfAddressContentUtil changeOfAddressContentUtil;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {

		long companyId = company.getCompanyId();
		LOG.debug("Creating Change your Address resources for companyId: " + companyId);

		changeOfAddressContentUtil.getOrCreateJournalArticleEmail(changeOfAddressContentUtil.getServiceContext(company.getGroup()));

		LOG.debug("Created Change your Address resources for companyId: " + companyId);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Waits until the portal has finished initializing. No code needed.
	}

}
