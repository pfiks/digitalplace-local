package com.placecube.digitalplace.local.account.changeofaddress.internal.upgrade.upgrade_1_0_1;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;


public class ChangeOfAddressIndexUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		
		if (hasIndex("Placecube_Account_Address", "IX_9CAA7AD6")) {
			runSQLTemplateString("drop index IX_9CAA7AD6 on Placecube_Account_Address;", false);
		}
		if (hasIndex("Placecube_Account_Address", "IX_84BC5EA7")) {
			runSQLTemplateString("drop index IX_84BC5EA7 on Placecube_Account_Address;", false);
		}
		if (hasIndex("Placecube_Account_Address", "IX_674A62E9")) {
			runSQLTemplateString("drop index IX_674A62E9 on Placecube_Account_Address;", false);
		}

		if (!hasIndex("Placecube_Account_Address", "IX_B3FCEEB8")) {
			runSQLTemplateString("create index IX_B3FCEEB8 on Placecube_Account_Address_ChangeOfAddress (companyId, newAddressId);", false);
		}
		if (!hasIndex("Placecube_Account_Address", "IX_B8376FCC")) {
			runSQLTemplateString("create index IX_B8376FCC on Placecube_Account_Address_ChangeOfAddress (formInstanceRecordId);", false);
		}
		if (!hasIndex("Placecube_Account_Address", "IX_2FCD3EE1")) {
			runSQLTemplateString("create index IX_2FCD3EE1 on Placecube_Account_Address_ChangeOfAddress (uuid_[$COLUMN_LENGTH:75$]);", false);
		}
	}

}
