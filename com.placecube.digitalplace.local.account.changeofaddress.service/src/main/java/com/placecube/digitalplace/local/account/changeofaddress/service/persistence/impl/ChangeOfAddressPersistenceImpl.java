/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.local.account.changeofaddress.exception.NoSuchChangeOfAddressException;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddressTable;
import com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressImpl;
import com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl;
import com.placecube.digitalplace.local.account.changeofaddress.service.persistence.ChangeOfAddressPersistence;
import com.placecube.digitalplace.local.account.changeofaddress.service.persistence.ChangeOfAddressUtil;
import com.placecube.digitalplace.local.account.changeofaddress.service.persistence.impl.constants.Placecube_Account_AddressPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the change of address service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = ChangeOfAddressPersistence.class)
public class ChangeOfAddressPersistenceImpl
	extends BasePersistenceImpl<ChangeOfAddress>
	implements ChangeOfAddressPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ChangeOfAddressUtil</code> to access the change of address persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ChangeOfAddressImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the change of addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<ChangeOfAddress> list = null;

		if (useFinderCache) {
			list = (List<ChangeOfAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChangeOfAddress changeOfAddress : list) {
					if (!uuid.equals(changeOfAddress.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<ChangeOfAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByUuid_First(
			String uuid, OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByUuid_First(
			uuid, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByUuid_First(
		String uuid, OrderByComparator<ChangeOfAddress> orderByComparator) {

		List<ChangeOfAddress> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByUuid_Last(
			String uuid, OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByUuid_Last(
			uuid, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByUuid_Last(
		String uuid, OrderByComparator<ChangeOfAddress> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ChangeOfAddress> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where uuid = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress[] findByUuid_PrevAndNext(
			long changeOfAddressId, String uuid,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		uuid = Objects.toString(uuid, "");

		ChangeOfAddress changeOfAddress = findByPrimaryKey(changeOfAddressId);

		Session session = null;

		try {
			session = openSession();

			ChangeOfAddress[] array = new ChangeOfAddressImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, changeOfAddress, uuid, orderByComparator, true);

			array[1] = changeOfAddress;

			array[2] = getByUuid_PrevAndNext(
				session, changeOfAddress, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChangeOfAddress getByUuid_PrevAndNext(
		Session session, ChangeOfAddress changeOfAddress, String uuid,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						changeOfAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ChangeOfAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the change of addresses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ChangeOfAddress changeOfAddress :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(changeOfAddress);
		}
	}

	/**
	 * Returns the number of change of addresses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching change of addresses
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_CHANGEOFADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"changeOfAddress.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(changeOfAddress.uuid IS NULL OR changeOfAddress.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByUUID_G(String uuid, long groupId)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByUUID_G(uuid, groupId);

		if (changeOfAddress == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchChangeOfAddressException(sb.toString());
		}

		return changeOfAddress;
	}

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the change of address where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof ChangeOfAddress) {
			ChangeOfAddress changeOfAddress = (ChangeOfAddress)result;

			if (!Objects.equals(uuid, changeOfAddress.getUuid()) ||
				(groupId != changeOfAddress.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<ChangeOfAddress> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					ChangeOfAddress changeOfAddress = list.get(0);

					result = changeOfAddress;

					cacheResult(changeOfAddress);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ChangeOfAddress)result;
		}
	}

	/**
	 * Removes the change of address where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the change of address that was removed
	 */
	@Override
	public ChangeOfAddress removeByUUID_G(String uuid, long groupId)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = findByUUID_G(uuid, groupId);

		return remove(changeOfAddress);
	}

	/**
	 * Returns the number of change of addresses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching change of addresses
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_CHANGEOFADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"changeOfAddress.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(changeOfAddress.uuid IS NULL OR changeOfAddress.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"changeOfAddress.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<ChangeOfAddress> list = null;

		if (useFinderCache) {
			list = (List<ChangeOfAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChangeOfAddress changeOfAddress : list) {
					if (!uuid.equals(changeOfAddress.getUuid()) ||
						(companyId != changeOfAddress.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<ChangeOfAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the first change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		List<ChangeOfAddress> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the last change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ChangeOfAddress> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress[] findByUuid_C_PrevAndNext(
			long changeOfAddressId, String uuid, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		uuid = Objects.toString(uuid, "");

		ChangeOfAddress changeOfAddress = findByPrimaryKey(changeOfAddressId);

		Session session = null;

		try {
			session = openSession();

			ChangeOfAddress[] array = new ChangeOfAddressImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, changeOfAddress, uuid, companyId, orderByComparator,
				true);

			array[1] = changeOfAddress;

			array[2] = getByUuid_C_PrevAndNext(
				session, changeOfAddress, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChangeOfAddress getByUuid_C_PrevAndNext(
		Session session, ChangeOfAddress changeOfAddress, String uuid,
		long companyId, OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						changeOfAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ChangeOfAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the change of addresses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ChangeOfAddress changeOfAddress :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(changeOfAddress);
		}
	}

	/**
	 * Returns the number of change of addresses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_CHANGEOFADDRESS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"changeOfAddress.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(changeOfAddress.uuid IS NULL OR changeOfAddress.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"changeOfAddress.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByCompanyId;
	private FinderPath _finderPathWithoutPaginationFindByCompanyId;
	private FinderPath _finderPathCountByCompanyId;

	/**
	 * Returns all the change of addresses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByCompanyId(long companyId) {
		return findByCompanyId(
			companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end) {

		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return findByCompanyId(companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the change of addresses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByCompanyId;
				finderArgs = new Object[] {companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCompanyId;
			finderArgs = new Object[] {
				companyId, start, end, orderByComparator
			};
		}

		List<ChangeOfAddress> list = null;

		if (useFinderCache) {
			list = (List<ChangeOfAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChangeOfAddress changeOfAddress : list) {
					if (companyId != changeOfAddress.getCompanyId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				list = (List<ChangeOfAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByCompanyId_First(
			long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByCompanyId_First(
			companyId, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the first change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByCompanyId_First(
		long companyId, OrderByComparator<ChangeOfAddress> orderByComparator) {

		List<ChangeOfAddress> list = findByCompanyId(
			companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByCompanyId_Last(
			long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByCompanyId_Last(
			companyId, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the last change of address in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByCompanyId_Last(
		long companyId, OrderByComparator<ChangeOfAddress> orderByComparator) {

		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<ChangeOfAddress> list = findByCompanyId(
			companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress[] findByCompanyId_PrevAndNext(
			long changeOfAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = findByPrimaryKey(changeOfAddressId);

		Session session = null;

		try {
			session = openSession();

			ChangeOfAddress[] array = new ChangeOfAddressImpl[3];

			array[0] = getByCompanyId_PrevAndNext(
				session, changeOfAddress, companyId, orderByComparator, true);

			array[1] = changeOfAddress;

			array[2] = getByCompanyId_PrevAndNext(
				session, changeOfAddress, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChangeOfAddress getByCompanyId_PrevAndNext(
		Session session, ChangeOfAddress changeOfAddress, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

		sb.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						changeOfAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ChangeOfAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the change of addresses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	@Override
	public void removeByCompanyId(long companyId) {
		for (ChangeOfAddress changeOfAddress :
				findByCompanyId(
					companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(changeOfAddress);
		}
	}

	/**
	 * Returns the number of change of addresses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	@Override
	public int countByCompanyId(long companyId) {
		FinderPath finderPath = _finderPathCountByCompanyId;

		Object[] finderArgs = new Object[] {companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_CHANGEOFADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 =
		"changeOfAddress.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByNewAddressIdAndCompanyId;
	private FinderPath
		_finderPathWithoutPaginationFindByNewAddressIdAndCompanyId;
	private FinderPath _finderPathCountByNewAddressIdAndCompanyId;

	/**
	 * Returns all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @return the matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId) {

		return findByNewAddressIdAndCompanyId(
			newAddressId, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end) {

		return findByNewAddressIdAndCompanyId(
			newAddressId, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return findByNewAddressIdAndCompanyId(
			newAddressId, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findByNewAddressIdAndCompanyId(
		long newAddressId, long companyId, int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByNewAddressIdAndCompanyId;
				finderArgs = new Object[] {newAddressId, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByNewAddressIdAndCompanyId;
			finderArgs = new Object[] {
				newAddressId, companyId, start, end, orderByComparator
			};
		}

		List<ChangeOfAddress> list = null;

		if (useFinderCache) {
			list = (List<ChangeOfAddress>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChangeOfAddress changeOfAddress : list) {
					if ((newAddressId != changeOfAddress.getNewAddressId()) ||
						(companyId != changeOfAddress.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_NEWADDRESSID_2);

			sb.append(_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(newAddressId);

				queryPos.add(companyId);

				list = (List<ChangeOfAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByNewAddressIdAndCompanyId_First(
			long newAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByNewAddressIdAndCompanyId_First(
			newAddressId, companyId, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("newAddressId=");
		sb.append(newAddressId);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the first change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByNewAddressIdAndCompanyId_First(
		long newAddressId, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		List<ChangeOfAddress> list = findByNewAddressIdAndCompanyId(
			newAddressId, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByNewAddressIdAndCompanyId_Last(
			long newAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByNewAddressIdAndCompanyId_Last(
			newAddressId, companyId, orderByComparator);

		if (changeOfAddress != null) {
			return changeOfAddress;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("newAddressId=");
		sb.append(newAddressId);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchChangeOfAddressException(sb.toString());
	}

	/**
	 * Returns the last change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByNewAddressIdAndCompanyId_Last(
		long newAddressId, long companyId,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		int count = countByNewAddressIdAndCompanyId(newAddressId, companyId);

		if (count == 0) {
			return null;
		}

		List<ChangeOfAddress> list = findByNewAddressIdAndCompanyId(
			newAddressId, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the change of addresses before and after the current change of address in the ordered set where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param changeOfAddressId the primary key of the current change of address
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress[] findByNewAddressIdAndCompanyId_PrevAndNext(
			long changeOfAddressId, long newAddressId, long companyId,
			OrderByComparator<ChangeOfAddress> orderByComparator)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = findByPrimaryKey(changeOfAddressId);

		Session session = null;

		try {
			session = openSession();

			ChangeOfAddress[] array = new ChangeOfAddressImpl[3];

			array[0] = getByNewAddressIdAndCompanyId_PrevAndNext(
				session, changeOfAddress, newAddressId, companyId,
				orderByComparator, true);

			array[1] = changeOfAddress;

			array[2] = getByNewAddressIdAndCompanyId_PrevAndNext(
				session, changeOfAddress, newAddressId, companyId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChangeOfAddress getByNewAddressIdAndCompanyId_PrevAndNext(
		Session session, ChangeOfAddress changeOfAddress, long newAddressId,
		long companyId, OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

		sb.append(_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_NEWADDRESSID_2);

		sb.append(_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(newAddressId);

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						changeOfAddress)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ChangeOfAddress> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the change of addresses where newAddressId = &#63; and companyId = &#63; from the database.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 */
	@Override
	public void removeByNewAddressIdAndCompanyId(
		long newAddressId, long companyId) {

		for (ChangeOfAddress changeOfAddress :
				findByNewAddressIdAndCompanyId(
					newAddressId, companyId, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(changeOfAddress);
		}
	}

	/**
	 * Returns the number of change of addresses where newAddressId = &#63; and companyId = &#63;.
	 *
	 * @param newAddressId the new address ID
	 * @param companyId the company ID
	 * @return the number of matching change of addresses
	 */
	@Override
	public int countByNewAddressIdAndCompanyId(
		long newAddressId, long companyId) {

		FinderPath finderPath = _finderPathCountByNewAddressIdAndCompanyId;

		Object[] finderArgs = new Object[] {newAddressId, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_CHANGEOFADDRESS_WHERE);

			sb.append(_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_NEWADDRESSID_2);

			sb.append(_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(newAddressId);

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_NEWADDRESSID_2 =
			"changeOfAddress.newAddressId = ? AND ";

	private static final String
		_FINDER_COLUMN_NEWADDRESSIDANDCOMPANYID_COMPANYID_2 =
			"changeOfAddress.companyId = ?";

	private FinderPath _finderPathFetchByFormInstanceRecordId;
	private FinderPath _finderPathCountByFormInstanceRecordId;

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching change of address
	 * @throws NoSuchChangeOfAddressException if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress findByFormInstanceRecordId(long formInstanceRecordId)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByFormInstanceRecordId(
			formInstanceRecordId);

		if (changeOfAddress == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("formInstanceRecordId=");
			sb.append(formInstanceRecordId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchChangeOfAddressException(sb.toString());
		}

		return changeOfAddress;
	}

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByFormInstanceRecordId(
		long formInstanceRecordId) {

		return fetchByFormInstanceRecordId(formInstanceRecordId, true);
	}

	/**
	 * Returns the change of address where formInstanceRecordId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching change of address, or <code>null</code> if a matching change of address could not be found
	 */
	@Override
	public ChangeOfAddress fetchByFormInstanceRecordId(
		long formInstanceRecordId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {formInstanceRecordId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByFormInstanceRecordId, finderArgs, this);
		}

		if (result instanceof ChangeOfAddress) {
			ChangeOfAddress changeOfAddress = (ChangeOfAddress)result;

			if (formInstanceRecordId !=
					changeOfAddress.getFormInstanceRecordId()) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_CHANGEOFADDRESS_WHERE);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDID_FORMINSTANCERECORDID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(formInstanceRecordId);

				List<ChangeOfAddress> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByFormInstanceRecordId, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									formInstanceRecordId
								};
							}

							_log.warn(
								"ChangeOfAddressPersistenceImpl.fetchByFormInstanceRecordId(long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ChangeOfAddress changeOfAddress = list.get(0);

					result = changeOfAddress;

					cacheResult(changeOfAddress);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ChangeOfAddress)result;
		}
	}

	/**
	 * Removes the change of address where formInstanceRecordId = &#63; from the database.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the change of address that was removed
	 */
	@Override
	public ChangeOfAddress removeByFormInstanceRecordId(
			long formInstanceRecordId)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = findByFormInstanceRecordId(
			formInstanceRecordId);

		return remove(changeOfAddress);
	}

	/**
	 * Returns the number of change of addresses where formInstanceRecordId = &#63;.
	 *
	 * @param formInstanceRecordId the form instance record ID
	 * @return the number of matching change of addresses
	 */
	@Override
	public int countByFormInstanceRecordId(long formInstanceRecordId) {
		FinderPath finderPath = _finderPathCountByFormInstanceRecordId;

		Object[] finderArgs = new Object[] {formInstanceRecordId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_CHANGEOFADDRESS_WHERE);

			sb.append(
				_FINDER_COLUMN_FORMINSTANCERECORDID_FORMINSTANCERECORDID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(formInstanceRecordId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_FORMINSTANCERECORDID_FORMINSTANCERECORDID_2 =
			"changeOfAddress.formInstanceRecordId = ?";

	public ChangeOfAddressPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(ChangeOfAddress.class);

		setModelImplClass(ChangeOfAddressImpl.class);
		setModelPKClass(long.class);

		setTable(ChangeOfAddressTable.INSTANCE);
	}

	/**
	 * Caches the change of address in the entity cache if it is enabled.
	 *
	 * @param changeOfAddress the change of address
	 */
	@Override
	public void cacheResult(ChangeOfAddress changeOfAddress) {
		entityCache.putResult(
			ChangeOfAddressImpl.class, changeOfAddress.getPrimaryKey(),
			changeOfAddress);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {
				changeOfAddress.getUuid(), changeOfAddress.getGroupId()
			},
			changeOfAddress);

		finderCache.putResult(
			_finderPathFetchByFormInstanceRecordId,
			new Object[] {changeOfAddress.getFormInstanceRecordId()},
			changeOfAddress);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the change of addresses in the entity cache if it is enabled.
	 *
	 * @param changeOfAddresses the change of addresses
	 */
	@Override
	public void cacheResult(List<ChangeOfAddress> changeOfAddresses) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (changeOfAddresses.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (ChangeOfAddress changeOfAddress : changeOfAddresses) {
			if (entityCache.getResult(
					ChangeOfAddressImpl.class,
					changeOfAddress.getPrimaryKey()) == null) {

				cacheResult(changeOfAddress);
			}
		}
	}

	/**
	 * Clears the cache for all change of addresses.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ChangeOfAddressImpl.class);

		finderCache.clearCache(ChangeOfAddressImpl.class);
	}

	/**
	 * Clears the cache for the change of address.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ChangeOfAddress changeOfAddress) {
		entityCache.removeResult(ChangeOfAddressImpl.class, changeOfAddress);
	}

	@Override
	public void clearCache(List<ChangeOfAddress> changeOfAddresses) {
		for (ChangeOfAddress changeOfAddress : changeOfAddresses) {
			entityCache.removeResult(
				ChangeOfAddressImpl.class, changeOfAddress);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(ChangeOfAddressImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(ChangeOfAddressImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		ChangeOfAddressModelImpl changeOfAddressModelImpl) {

		Object[] args = new Object[] {
			changeOfAddressModelImpl.getUuid(),
			changeOfAddressModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, changeOfAddressModelImpl);

		args = new Object[] {
			changeOfAddressModelImpl.getFormInstanceRecordId()
		};

		finderCache.putResult(
			_finderPathCountByFormInstanceRecordId, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByFormInstanceRecordId, args,
			changeOfAddressModelImpl);
	}

	/**
	 * Creates a new change of address with the primary key. Does not add the change of address to the database.
	 *
	 * @param changeOfAddressId the primary key for the new change of address
	 * @return the new change of address
	 */
	@Override
	public ChangeOfAddress create(long changeOfAddressId) {
		ChangeOfAddress changeOfAddress = new ChangeOfAddressImpl();

		changeOfAddress.setNew(true);
		changeOfAddress.setPrimaryKey(changeOfAddressId);

		String uuid = PortalUUIDUtil.generate();

		changeOfAddress.setUuid(uuid);

		changeOfAddress.setCompanyId(CompanyThreadLocal.getCompanyId());

		return changeOfAddress;
	}

	/**
	 * Removes the change of address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address that was removed
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress remove(long changeOfAddressId)
		throws NoSuchChangeOfAddressException {

		return remove((Serializable)changeOfAddressId);
	}

	/**
	 * Removes the change of address with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the change of address
	 * @return the change of address that was removed
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress remove(Serializable primaryKey)
		throws NoSuchChangeOfAddressException {

		Session session = null;

		try {
			session = openSession();

			ChangeOfAddress changeOfAddress = (ChangeOfAddress)session.get(
				ChangeOfAddressImpl.class, primaryKey);

			if (changeOfAddress == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchChangeOfAddressException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(changeOfAddress);
		}
		catch (NoSuchChangeOfAddressException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ChangeOfAddress removeImpl(ChangeOfAddress changeOfAddress) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(changeOfAddress)) {
				changeOfAddress = (ChangeOfAddress)session.get(
					ChangeOfAddressImpl.class,
					changeOfAddress.getPrimaryKeyObj());
			}

			if (changeOfAddress != null) {
				session.delete(changeOfAddress);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (changeOfAddress != null) {
			clearCache(changeOfAddress);
		}

		return changeOfAddress;
	}

	@Override
	public ChangeOfAddress updateImpl(ChangeOfAddress changeOfAddress) {
		boolean isNew = changeOfAddress.isNew();

		if (!(changeOfAddress instanceof ChangeOfAddressModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(changeOfAddress.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					changeOfAddress);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in changeOfAddress proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom ChangeOfAddress implementation " +
					changeOfAddress.getClass());
		}

		ChangeOfAddressModelImpl changeOfAddressModelImpl =
			(ChangeOfAddressModelImpl)changeOfAddress;

		if (Validator.isNull(changeOfAddress.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			changeOfAddress.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (changeOfAddress.getCreateDate() == null)) {
			if (serviceContext == null) {
				changeOfAddress.setCreateDate(date);
			}
			else {
				changeOfAddress.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!changeOfAddressModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				changeOfAddress.setModifiedDate(date);
			}
			else {
				changeOfAddress.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(changeOfAddress);
			}
			else {
				changeOfAddress = (ChangeOfAddress)session.merge(
					changeOfAddress);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			ChangeOfAddressImpl.class, changeOfAddressModelImpl, false, true);

		cacheUniqueFindersCache(changeOfAddressModelImpl);

		if (isNew) {
			changeOfAddress.setNew(false);
		}

		changeOfAddress.resetOriginalValues();

		return changeOfAddress;
	}

	/**
	 * Returns the change of address with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the change of address
	 * @return the change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress findByPrimaryKey(Serializable primaryKey)
		throws NoSuchChangeOfAddressException {

		ChangeOfAddress changeOfAddress = fetchByPrimaryKey(primaryKey);

		if (changeOfAddress == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchChangeOfAddressException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return changeOfAddress;
	}

	/**
	 * Returns the change of address with the primary key or throws a <code>NoSuchChangeOfAddressException</code> if it could not be found.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address
	 * @throws NoSuchChangeOfAddressException if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress findByPrimaryKey(long changeOfAddressId)
		throws NoSuchChangeOfAddressException {

		return findByPrimaryKey((Serializable)changeOfAddressId);
	}

	/**
	 * Returns the change of address with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param changeOfAddressId the primary key of the change of address
	 * @return the change of address, or <code>null</code> if a change of address with the primary key could not be found
	 */
	@Override
	public ChangeOfAddress fetchByPrimaryKey(long changeOfAddressId) {
		return fetchByPrimaryKey((Serializable)changeOfAddressId);
	}

	/**
	 * Returns all the change of addresses.
	 *
	 * @return the change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @return the range of change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findAll(
		int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the change of addresses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ChangeOfAddressModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of change of addresses
	 * @param end the upper bound of the range of change of addresses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of change of addresses
	 */
	@Override
	public List<ChangeOfAddress> findAll(
		int start, int end,
		OrderByComparator<ChangeOfAddress> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<ChangeOfAddress> list = null;

		if (useFinderCache) {
			list = (List<ChangeOfAddress>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_CHANGEOFADDRESS);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_CHANGEOFADDRESS;

				sql = sql.concat(ChangeOfAddressModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<ChangeOfAddress>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the change of addresses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ChangeOfAddress changeOfAddress : findAll()) {
			remove(changeOfAddress);
		}
	}

	/**
	 * Returns the number of change of addresses.
	 *
	 * @return the number of change of addresses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_CHANGEOFADDRESS);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "changeOfAddressId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_CHANGEOFADDRESS;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ChangeOfAddressModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the change of address persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"companyId"}, true);

		_finderPathWithoutPaginationFindByCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] {Long.class.getName()}, new String[] {"companyId"},
			true);

		_finderPathCountByCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] {Long.class.getName()}, new String[] {"companyId"},
			false);

		_finderPathWithPaginationFindByNewAddressIdAndCompanyId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByNewAddressIdAndCompanyId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				},
				new String[] {"newAddressId", "companyId"}, true);

		_finderPathWithoutPaginationFindByNewAddressIdAndCompanyId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByNewAddressIdAndCompanyId",
				new String[] {Long.class.getName(), Long.class.getName()},
				new String[] {"newAddressId", "companyId"}, true);

		_finderPathCountByNewAddressIdAndCompanyId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByNewAddressIdAndCompanyId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"newAddressId", "companyId"}, false);

		_finderPathFetchByFormInstanceRecordId = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByFormInstanceRecordId",
			new String[] {Long.class.getName()},
			new String[] {"formInstanceRecordId"}, true);

		_finderPathCountByFormInstanceRecordId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByFormInstanceRecordId", new String[] {Long.class.getName()},
			new String[] {"formInstanceRecordId"}, false);

		ChangeOfAddressUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		ChangeOfAddressUtil.setPersistence(null);

		entityCache.removeCache(ChangeOfAddressImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_Account_AddressPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_Account_AddressPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_Account_AddressPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_CHANGEOFADDRESS =
		"SELECT changeOfAddress FROM ChangeOfAddress changeOfAddress";

	private static final String _SQL_SELECT_CHANGEOFADDRESS_WHERE =
		"SELECT changeOfAddress FROM ChangeOfAddress changeOfAddress WHERE ";

	private static final String _SQL_COUNT_CHANGEOFADDRESS =
		"SELECT COUNT(changeOfAddress) FROM ChangeOfAddress changeOfAddress";

	private static final String _SQL_COUNT_CHANGEOFADDRESS_WHERE =
		"SELECT COUNT(changeOfAddress) FROM ChangeOfAddress changeOfAddress WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "changeOfAddress.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No ChangeOfAddress exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No ChangeOfAddress exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		ChangeOfAddressPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}