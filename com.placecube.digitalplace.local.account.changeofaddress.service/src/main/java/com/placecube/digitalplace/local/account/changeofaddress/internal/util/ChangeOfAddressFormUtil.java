package com.placecube.digitalplace.local.account.changeofaddress.internal.util;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressFormConstants;

public class ChangeOfAddressFormUtil {

	private ChangeOfAddressFormUtil() {

	}

	public static String formatSelectField(String fieldValue) {
		return Validator.isNull(fieldValue) ? fieldValue
				: fieldValue.replace(StringPool.OPEN_BRACKET, StringPool.BLANK).replace(StringPool.CLOSE_BRACKET, StringPool.BLANK).replace(StringPool.QUOTE, StringPool.BLANK);
	}

	public static String getFieldValue(Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, String fieldName, Locale defaultLocale) {
		return ddmFormFieldValuesMap.get(fieldName).get(0).getValue().getString(defaultLocale);
	}

	public static Date getMoveOutDate(Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, Locale defaultLocale) throws ParseException {
		String moveOutDate = getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.MOVE_OUT_DATE_FIELD_NAME, defaultLocale);
		return DateFormatFactoryUtil.getSimpleDateFormat(ChangeOfAddressFormConstants.FORM_DATE_FORMAT).parse(moveOutDate);
	}

	public static boolean isSameCouncil(Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, Locale defaultLocale) {
		return getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.IS_SAME_COUNCIL_FIELD_NAME, defaultLocale).equals(ChangeOfAddressFormConstants.YES_FIELD_VALUE);
	}

}
