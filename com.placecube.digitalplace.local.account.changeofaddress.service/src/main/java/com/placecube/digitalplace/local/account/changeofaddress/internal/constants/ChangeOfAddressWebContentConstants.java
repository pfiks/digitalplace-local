package com.placecube.digitalplace.local.account.changeofaddress.internal.constants;

public final class ChangeOfAddressWebContentConstants {

	public static final String CHANGE_OF_ADDRESS_ARTICLE_BODY_FIELD = "Body";
	public static final String CHANGE_OF_ADDRESS_ARTICLE_SUBJECT_FIELD = "Subject";
	public static final String CHANGE_OF_ADDRESS_CONFIRMATION_ARTICLE_TITLE = "Change of Address Confirmation Email";
	public static final String CHANGE_OF_ADDRESS_CONFIRMATION_TEMPLATE_ARTICLE_ID = "CHANGE-OF-ADDRESS-CONFIRMATION-EMAIL";
	public static final String CHANGE_OF_ADDRESS_CONFIRMATION_TEMPLATE_FILE_NAME = "Change of Address Confirmation Email.xml";
	public static final String EMAIL_PLACEHOLDER_FIRST_NAME = "$FirstName$";
	public static final String EMAIL_PLACEHOLDER_LAST_NAME = "$LastName$";
	public static final String EMAIL_PLACEHOLDER_TITLE = "$Title$";
	public static final String EMAIL_PLACEHOLDER_UPDATED_ADDRESSS = "$UpdatedAddress$";
	public static final String TEMPLATES_FOLDER = "Templates";

	private ChangeOfAddressWebContentConstants() {

	}
}
