package com.placecube.digitalplace.local.account.changeofaddress.internal.constants;

public final class ChangeOfAddressFormConstants {

	public static final String CHANGE_OF_ADDRESS_STRUCTURE_KEY = "CHANGE-OF-ADDRESS";

	public static final String CITY_FIELD_NAME = "City";

	public static final String EMAIL_ADDRESS_FIELD_NAME = "EmailAddress";

	public static final String FORM_DATE_FORMAT = "yyyy-MM-dd";

	public static final String IS_SAME_COUNCIL_FIELD_NAME = "isSameCouncil";

	public static final String MOVE_OUT_DATE_FIELD_NAME = "MoveOutDate";

	public static final String NEW_ADDRESS_FIELD_NAME = "NewAddress";

	public static final String POSTCODE_FIELD_NAME = "Postcode";

	public static final String STREET_1_FIELD_NAME = "Street1";

	public static final String STREET_2_FIELD_NAME = "Street2";

	public static final String STREET_3_FIELD_NAME = "Street3";

	public static final String TITLE_FIELD_NAME = "Title";

	public static final String UPDATED_ADDRESS_FIELD_NAME = "UpdatedAddress";

	public static final String YES_FIELD_VALUE = "Yes";

	private ChangeOfAddressFormConstants() {
	}

}
