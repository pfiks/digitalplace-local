package com.placecube.digitalplace.local.account.changeofaddress.internal.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.local.account.changeofaddress.internal.upgrade.upgrade_1_0_1.ChangeOfAddressIndexUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class ChangeOfAddressUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new ChangeOfAddressIndexUpdate());
	}

}
