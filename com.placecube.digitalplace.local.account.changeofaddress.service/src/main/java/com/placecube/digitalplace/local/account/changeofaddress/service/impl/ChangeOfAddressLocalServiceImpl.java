/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.local.account.changeofaddress.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.placecube.digitalplace.local.account.changeofaddress.exception.NoSuchChangeOfAddressException;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.service.base.ChangeOfAddressLocalServiceBaseImpl;

/**
 * The implementation of the change of address local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.local.account.changeofaddress.service.ChangeOfAddressLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChangeOfAddressLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress", service = AopService.class)
public class ChangeOfAddressLocalServiceImpl extends ChangeOfAddressLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.local.account.changeofaddress.service.
	 * ChangeOfAddressLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.local.account.changeofaddress.service.
	 * ChangeOfAddressLocalServiceUtil</code>.
	 */

	@Indexable(type = IndexableType.REINDEX)
	@Override
	public ChangeOfAddress addChangeOfAddress(long groupId, long companyId, long userId, long formInstanceRecordId, Date addressUpdateDate) throws PortalException {

		ChangeOfAddress changeOfAddress = changeOfAddressPersistence.create(counterLocalService.increment(ChangeOfAddress.class.getName()));

		User user = userLocalService.getUser(userId);

		Optional<Address> optPrimaryAddress = user.getAddresses().stream().filter(Address::isPrimary).findFirst();

		if (optPrimaryAddress.isPresent()) {
			changeOfAddress.setCurrentAddressId(optPrimaryAddress.get().getAddressId());
		}

		changeOfAddress.setGroupId(groupId);
		changeOfAddress.setCompanyId(companyId);
		changeOfAddress.setUserId(userId);
		changeOfAddress.setUserName(user.getFullName());

		Date now = new Date();
		changeOfAddress.setCreateDate(now);
		changeOfAddress.setModifiedDate(now);

		changeOfAddress.setFormInstanceRecordId(formInstanceRecordId);
		changeOfAddress.setAddressUpdateDate(addressUpdateDate);

		return changeOfAddressPersistence.update(changeOfAddress);
	}

	@Override
	public ChangeOfAddress deleteByFormInstanceRecordId(long formInstanceRecordId) throws NoSuchChangeOfAddressException {
		return changeOfAddressPersistence.removeByFormInstanceRecordId(formInstanceRecordId);
	}

	@Override
	public List<ChangeOfAddress> getChangeOfAddressesToUpdate(long companyId) {
		List<ChangeOfAddress> changesOfAddressBeforeCurrentDate = new ArrayList<>();

		Date now = new Date();

		for (ChangeOfAddress changeOfAddress : changeOfAddressPersistence.findByNewAddressIdAndCompanyId(0, companyId)) {
			if (changeOfAddress.getAddressUpdateDate().before(now)) {
				changesOfAddressBeforeCurrentDate.add(changeOfAddress);
			}
		}

		return changesOfAddressBeforeCurrentDate;
	}

}