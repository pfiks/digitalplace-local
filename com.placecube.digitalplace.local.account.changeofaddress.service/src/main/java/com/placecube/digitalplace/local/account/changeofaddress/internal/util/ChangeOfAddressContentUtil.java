package com.placecube.digitalplace.local.account.changeofaddress.internal.util;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;

import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressFormConstants;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressWebContentConstants;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = ChangeOfAddressContentUtil.class)
public class ChangeOfAddressContentUtil {

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	public String getBody(JournalArticle journalArticle, Locale locale) throws PortletException {
		Optional<String> body = journalArticleRetrievalService.getFieldValue(journalArticle, ChangeOfAddressWebContentConstants.CHANGE_OF_ADDRESS_ARTICLE_BODY_FIELD, locale);
		if (body.isPresent()) {
			return body.get();
		} else {
			throw new PortletException("Email body field is not present in jouranlArticleId " + journalArticle.getArticleId());
		}
	}

	public JournalArticle getOrCreateJournalArticleEmail(ServiceContext serviceContext) throws PortalException, IOException {
		serviceContext.setScopeGroupId(groupLocalService.getCompanyGroup(serviceContext.getCompanyId()).getGroupId());
		JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(groupLocalService.getCompanyGroup(serviceContext.getCompanyId()).getGroupId(), ChangeOfAddressWebContentConstants.CHANGE_OF_ADDRESS_CONFIRMATION_TEMPLATE_ARTICLE_ID, WorkflowConstants.STATUS_APPROVED);

		if (Validator.isNull(journalArticle)) {

			String articleContent = StringUtil.read(getClass().getClassLoader(),
				"com/placecube/digitalplace/local/account/changeofaddress/dependencies/webcontent/" + ChangeOfAddressWebContentConstants.CHANGE_OF_ADDRESS_CONFIRMATION_TEMPLATE_FILE_NAME);
		JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder(ChangeOfAddressWebContentConstants.TEMPLATES_FOLDER, serviceContext);
		DDMStructure emailStructure = emailWebContentService.getOrCreateDDMStructure(serviceContext);

		JournalArticleContext context = JournalArticleContext.init(ChangeOfAddressWebContentConstants.CHANGE_OF_ADDRESS_CONFIRMATION_TEMPLATE_ARTICLE_ID,
				ChangeOfAddressWebContentConstants.CHANGE_OF_ADDRESS_CONFIRMATION_ARTICLE_TITLE, articleContent);
		context.setJournalFolder(journalFolder);
		context.setDDMStructure(emailStructure);
		context.setIndexable(false);
			journalArticle = journalArticleCreationService.getOrCreateArticle(context, serviceContext);
		}

		return journalArticle;
	}

	public String getSubject(JournalArticle journalArticle, Locale locale) throws PortalException {
		Optional<String> subject = journalArticleRetrievalService.getFieldValue(journalArticle, ChangeOfAddressWebContentConstants.CHANGE_OF_ADDRESS_ARTICLE_SUBJECT_FIELD, locale);
		if (subject.isPresent()) {
			return subject.get();
		} else {
			throw new PortalException("Email subject field is not present in jouranlArticleId " + journalArticle.getArticleId());
		}
	}

	public String replaceEmailValues(String section, Map<String, List<DDMFormFieldValue>> ddmFormFieldValuesMap, Locale locale, User user) {
		String address = ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.UPDATED_ADDRESS_FIELD_NAME, locale);
		String title = ChangeOfAddressFormUtil.formatSelectField(ChangeOfAddressFormUtil.getFieldValue(ddmFormFieldValuesMap, ChangeOfAddressFormConstants.TITLE_FIELD_NAME, locale));

		return StringUtil.replace(section,
				new String[] { ChangeOfAddressWebContentConstants.EMAIL_PLACEHOLDER_TITLE, ChangeOfAddressWebContentConstants.EMAIL_PLACEHOLDER_FIRST_NAME,
						ChangeOfAddressWebContentConstants.EMAIL_PLACEHOLDER_LAST_NAME, ChangeOfAddressWebContentConstants.EMAIL_PLACEHOLDER_UPDATED_ADDRESSS },
				new String[] { title, user.getFirstName(), user.getLastName(), address });
	}

	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(group.getCreatorUserId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(false);

		return serviceContext;
	}

}
