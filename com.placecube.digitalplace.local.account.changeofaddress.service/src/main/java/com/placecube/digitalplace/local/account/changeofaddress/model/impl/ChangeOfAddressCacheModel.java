/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ChangeOfAddress in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ChangeOfAddressCacheModel
	implements CacheModel<ChangeOfAddress>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ChangeOfAddressCacheModel)) {
			return false;
		}

		ChangeOfAddressCacheModel changeOfAddressCacheModel =
			(ChangeOfAddressCacheModel)object;

		if (changeOfAddressId == changeOfAddressCacheModel.changeOfAddressId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, changeOfAddressId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", changeOfAddressId=");
		sb.append(changeOfAddressId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", currentAddressId=");
		sb.append(currentAddressId);
		sb.append(", newAddressId=");
		sb.append(newAddressId);
		sb.append(", formInstanceRecordId=");
		sb.append(formInstanceRecordId);
		sb.append(", addressUpdateDate=");
		sb.append(addressUpdateDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ChangeOfAddress toEntityModel() {
		ChangeOfAddressImpl changeOfAddressImpl = new ChangeOfAddressImpl();

		if (uuid == null) {
			changeOfAddressImpl.setUuid("");
		}
		else {
			changeOfAddressImpl.setUuid(uuid);
		}

		changeOfAddressImpl.setChangeOfAddressId(changeOfAddressId);
		changeOfAddressImpl.setGroupId(groupId);
		changeOfAddressImpl.setCompanyId(companyId);
		changeOfAddressImpl.setUserId(userId);

		if (userName == null) {
			changeOfAddressImpl.setUserName("");
		}
		else {
			changeOfAddressImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			changeOfAddressImpl.setCreateDate(null);
		}
		else {
			changeOfAddressImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			changeOfAddressImpl.setModifiedDate(null);
		}
		else {
			changeOfAddressImpl.setModifiedDate(new Date(modifiedDate));
		}

		changeOfAddressImpl.setCurrentAddressId(currentAddressId);
		changeOfAddressImpl.setNewAddressId(newAddressId);
		changeOfAddressImpl.setFormInstanceRecordId(formInstanceRecordId);

		if (addressUpdateDate == Long.MIN_VALUE) {
			changeOfAddressImpl.setAddressUpdateDate(null);
		}
		else {
			changeOfAddressImpl.setAddressUpdateDate(
				new Date(addressUpdateDate));
		}

		changeOfAddressImpl.resetOriginalValues();

		return changeOfAddressImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		changeOfAddressId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		currentAddressId = objectInput.readLong();

		newAddressId = objectInput.readLong();

		formInstanceRecordId = objectInput.readLong();
		addressUpdateDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(changeOfAddressId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(currentAddressId);

		objectOutput.writeLong(newAddressId);

		objectOutput.writeLong(formInstanceRecordId);
		objectOutput.writeLong(addressUpdateDate);
	}

	public String uuid;
	public long changeOfAddressId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long currentAddressId;
	public long newAddressId;
	public long formInstanceRecordId;
	public long addressUpdateDate;

}