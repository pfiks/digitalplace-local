/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.changeofaddress.service.persistence.impl;

import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.model.BaseModel;

import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddressTable;
import com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressImpl;
import com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressModelImpl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Component;

/**
 * The arguments resolver class for retrieving value from ChangeOfAddress.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(
	property = {
		"class.name=com.placecube.digitalplace.local.account.changeofaddress.model.impl.ChangeOfAddressImpl",
		"table.name=Placecube_Account_Address_ChangeOfAddress"
	},
	service = ArgumentsResolver.class
)
public class ChangeOfAddressModelArgumentsResolver
	implements ArgumentsResolver {

	@Override
	public Object[] getArguments(
		FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
		boolean original) {

		String[] columnNames = finderPath.getColumnNames();

		if ((columnNames == null) || (columnNames.length == 0)) {
			if (baseModel.isNew()) {
				return new Object[0];
			}

			return null;
		}

		ChangeOfAddressModelImpl changeOfAddressModelImpl =
			(ChangeOfAddressModelImpl)baseModel;

		long columnBitmask = changeOfAddressModelImpl.getColumnBitmask();

		if (!checkColumn || (columnBitmask == 0)) {
			return _getValue(changeOfAddressModelImpl, columnNames, original);
		}

		Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
			finderPath);

		if (finderPathColumnBitmask == null) {
			finderPathColumnBitmask = 0L;

			for (String columnName : columnNames) {
				finderPathColumnBitmask |=
					changeOfAddressModelImpl.getColumnBitmask(columnName);
			}

			if (finderPath.isBaseModelResult() &&
				(ChangeOfAddressPersistenceImpl.
					FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
						finderPath.getCacheName())) {

				finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
			}

			_finderPathColumnBitmasksCache.put(
				finderPath, finderPathColumnBitmask);
		}

		if ((columnBitmask & finderPathColumnBitmask) != 0) {
			return _getValue(changeOfAddressModelImpl, columnNames, original);
		}

		return null;
	}

	@Override
	public String getClassName() {
		return ChangeOfAddressImpl.class.getName();
	}

	@Override
	public String getTableName() {
		return ChangeOfAddressTable.INSTANCE.getTableName();
	}

	private static Object[] _getValue(
		ChangeOfAddressModelImpl changeOfAddressModelImpl, String[] columnNames,
		boolean original) {

		Object[] arguments = new Object[columnNames.length];

		for (int i = 0; i < arguments.length; i++) {
			String columnName = columnNames[i];

			if (original) {
				arguments[i] = changeOfAddressModelImpl.getColumnOriginalValue(
					columnName);
			}
			else {
				arguments[i] = changeOfAddressModelImpl.getColumnValue(
					columnName);
			}
		}

		return arguments;
	}

	private static final Map<FinderPath, Long> _finderPathColumnBitmasksCache =
		new ConcurrentHashMap<>();

	private static final long _ORDER_BY_COLUMNS_BITMASK;

	static {
		long orderByColumnsBitmask = 0;

		orderByColumnsBitmask |= ChangeOfAddressModelImpl.getColumnBitmask(
			"groupId");
		orderByColumnsBitmask |= ChangeOfAddressModelImpl.getColumnBitmask(
			"companyId");

		_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
	}

}