package com.placecube.digitalplace.local.account.changeofaddress.internal.util;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;
import com.pfiks.mail.service.MailService;
import com.placecube.ddmform.fieldtype.address.util.AddressDDMFormFieldTypeUtil;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.local.account.changeofaddress.exception.NoSuchChangeOfAddressException;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressFormConstants;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.service.ChangeOfAddressLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ChangeOfAddressFormUtil.class, ServiceContextThreadLocal.class, ChangeOfAddressUtil.class })
public class ChangeOfAddressUtilTest extends PowerMockito {

	private static final long ADDRESS_ID = 5;

	private static final String BODY = "Body";

	private static final String CITY = "city";

	private static final long COMPANY_ID = 2;

	private static final String EMAIL_ADDRESS = "mail@mail.com";

	private static final long FORM_INSTANCE_ID = 4;

	private static final long FORM_INSTANCE_RECORD_ID = 6;

	private static final long GROUP_ID = 1;

	private static final String POSTCODE = "12345";

	private static final String STREET_1 = "str1";

	private static final String STREET_2 = "str2";

	private static final String STREET_3 = "str3";

	private static final String SUBJECT = "Subject";

	private static final String UPDATED_BODY = "UpdatedBody";

	private static final String UPDATED_SUBJECT = "UpdatedSubject";

	private static final String UPRN = "1";

	private static final long USER_ID = 3;

	@Mock
	private Address mockAddress;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private AddressDDMFormFieldTypeUtil mockAddressDDMFormFieldTypeUtil;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Mock
	private Map<String, Object> mockAddressMap;

	@Mock
	private AddressTypeConfigurationService mockAddressTypeConfigurationService;

	@Mock
	private JournalArticle mockArticle;

	@Mock
	private ChangeOfAddress mockChangeOfAddress;

	@Mock
	private ChangeOfAddressContentUtil mockChangeOfAddressContentUtil;

	@Mock
	private ChangeOfAddressLocalService mockChangeOfAddressLocalService;

	@InjectMocks
	private ChangeOfAddressUtil mockChangeOfAddressUtil;

	@Mock
	private Date mockDate;

	@Mock
	private Map<String, List<DDMFormFieldValue>> mockDDMFormFieldValuesMap;

	@Mock
	private DDMFormInstanceRecord mockDdmFormInstanceRecord;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private Locale mockLocale;

	@Mock
	private MailService mockMailService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(ChangeOfAddressFormUtil.class, ServiceContextThreadLocal.class);
	}

	@Test(expected = PortalException.class)
	public void proccessFormInstanceRecord_WhenErrorAddingChangeOfAddress_ThenPortalExceptionIsThrown() throws Exception {
		when(mockDdmFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(mockDDMFormFieldValuesMap);
		when(mockDDMFormValues.getDefaultLocale()).thenReturn(mockLocale);

		when(ChangeOfAddressFormUtil.getMoveOutDate(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(mockDate);

		when(mockDdmFormInstanceRecord.getGroupId()).thenReturn(GROUP_ID);
		when(mockDdmFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockDdmFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_ID);

		when(mockChangeOfAddressLocalService.addChangeOfAddress(eq(GROUP_ID), eq(COMPANY_ID), eq(USER_ID), eq(FORM_INSTANCE_ID), any(Date.class))).thenThrow(PortalException.class);

		mockChangeOfAddressUtil.processFormInstanceRecord(mockDdmFormInstanceRecord);
	}

	@Test
	public void proccessFormInstanceRecord_WhenMoveOutDateIsBeforeExecution_ThenAddressIsUpdated() throws Exception {
		when(mockDdmFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(mockDDMFormFieldValuesMap);
		when(mockDDMFormValues.getDefaultLocale()).thenReturn(mockLocale);

		when(ChangeOfAddressFormUtil.getMoveOutDate(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(mockDate);

		when(mockDdmFormInstanceRecord.getGroupId()).thenReturn(GROUP_ID);
		when(mockDdmFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockDdmFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_ID);

		when(mockChangeOfAddressLocalService.addChangeOfAddress(eq(GROUP_ID), eq(COMPANY_ID), eq(USER_ID), eq(FORM_INSTANCE_ID), any(Date.class))).thenReturn(mockChangeOfAddress);

		when(mockDate.before(any(Date.class))).thenReturn(true);

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);

		when(mockChangeOfAddressContentUtil.getOrCreateJournalArticleEmail(any(ServiceContext.class))).thenReturn(mockArticle);
		when(mockChangeOfAddressContentUtil.getSubject(mockArticle, mockLocale)).thenReturn(SUBJECT);
		when(mockChangeOfAddressContentUtil.getBody(mockArticle, mockLocale)).thenReturn(BODY);

		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(false);

		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_1_FIELD_NAME, mockLocale)).thenReturn(STREET_1);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_2_FIELD_NAME, mockLocale)).thenReturn(STREET_2);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_3_FIELD_NAME, mockLocale)).thenReturn(STREET_3);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.CITY_FIELD_NAME, mockLocale)).thenReturn(CITY);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.POSTCODE_FIELD_NAME, mockLocale)).thenReturn(POSTCODE);

		whenNew(AddressContext.class).withArguments(StringPool.BLANK, STREET_1, STREET_2, STREET_3, StringPool.BLANK, CITY, POSTCODE).thenReturn(mockAddressContext);

		when(mockAddressLookupService.saveAddress(any(User.class), any(AddressContext.class), any(ServiceContext.class))).thenReturn(mockAddress);
		when(mockAddress.getAddressId()).thenReturn(ADDRESS_ID);

		mockChangeOfAddressUtil.processFormInstanceRecord(mockDdmFormInstanceRecord);

		verify(mockChangeOfAddress, times(1)).setNewAddressId(ADDRESS_ID);
		verify(mockChangeOfAddress, times(1)).setModifiedDate(any(Date.class));
		verify(mockChangeOfAddressLocalService, times(1)).updateChangeOfAddress(mockChangeOfAddress);
	}

	@Test
	public void processFormInstanceRecord_WhenMoveOutDateIsNotBeforeExecution_ThenAddressIsNotUpdated() throws Exception {
		when(mockDdmFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(mockDDMFormFieldValuesMap);
		when(mockDDMFormValues.getDefaultLocale()).thenReturn(mockLocale);

		when(ChangeOfAddressFormUtil.getMoveOutDate(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(mockDate);

		when(mockDdmFormInstanceRecord.getGroupId()).thenReturn(GROUP_ID);
		when(mockDdmFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockDdmFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_ID);

		when(mockChangeOfAddressLocalService.addChangeOfAddress(eq(GROUP_ID), eq(COMPANY_ID), eq(USER_ID), eq(FORM_INSTANCE_ID), any(Date.class))).thenReturn(mockChangeOfAddress);

		when(mockDate.before(any(Date.class))).thenReturn(false);

		mockChangeOfAddressUtil.processFormInstanceRecord(mockDdmFormInstanceRecord);

		verify(mockChangeOfAddressLocalService, times(1)).addChangeOfAddress(eq(GROUP_ID), eq(COMPANY_ID), eq(USER_ID), eq(FORM_INSTANCE_ID), any(Date.class));
		verify(mockChangeOfAddressContentUtil, never()).getOrCreateJournalArticleEmail(any(ServiceContext.class));
		verify(mockChangeOfAddressContentUtil, never()).getSubject(any(JournalArticle.class), any(Locale.class));
		verify(mockChangeOfAddressContentUtil, never()).getBody(any(JournalArticle.class), any(Locale.class));
		verify(mockChangeOfAddressLocalService, never()).updateChangeOfAddress(mockChangeOfAddress);

	}

	@Test(expected = NoSuchChangeOfAddressException.class)
	public void removeChangeOfAddressByFormInstanceRecordId_WhenChangeOfAddressDataDoesNotExist_ThenExceptionIsThrown() throws NoSuchChangeOfAddressException {
		when(mockChangeOfAddressLocalService.deleteByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID)).thenThrow(NoSuchChangeOfAddressException.class);

		mockChangeOfAddressUtil.removeChangeOfAddressByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);

	}

	@Test
	public void removeChangeOfAddressByFormInstanceRecordId_WhenNoError_ChangeOfAddressDataIsRemoved() throws NoSuchChangeOfAddressException {
		when(mockChangeOfAddressLocalService.deleteByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID)).thenReturn(mockChangeOfAddress);

		mockChangeOfAddressUtil.removeChangeOfAddressByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);

		verify(mockChangeOfAddressLocalService, times(1)).deleteByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void updateAdddress_WhenAddressIsNotFromSameCouncil_ThenAddressContextIsCreatedFromFormData() throws Exception {
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(false);

		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_1_FIELD_NAME, mockLocale)).thenReturn(STREET_1);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_2_FIELD_NAME, mockLocale)).thenReturn(STREET_2);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_3_FIELD_NAME, mockLocale)).thenReturn(STREET_3);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.CITY_FIELD_NAME, mockLocale)).thenReturn(CITY);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.POSTCODE_FIELD_NAME, mockLocale)).thenReturn(POSTCODE);

		whenNew(AddressContext.class).withArguments(StringPool.BLANK, STREET_1, STREET_2, STREET_3, StringPool.BLANK, CITY, POSTCODE).thenReturn(mockAddressContext);

		when(mockAddressLookupService.saveAddress(eq(mockUser), any(AddressContext.class), eq(mockServiceContext))).thenReturn(mockAddress);
		when(mockAddress.getAddressId()).thenReturn(ADDRESS_ID);

		mockChangeOfAddressUtil.updateAddress(mockDdmFormInstanceRecord, mockDDMFormValues, mockDDMFormFieldValuesMap, mockLocale, mockChangeOfAddress, SUBJECT, BODY, mockServiceContext);

		verifyNew(AddressContext.class).withArguments(StringPool.BLANK, STREET_1, STREET_2, STREET_3, StringPool.BLANK, CITY, POSTCODE);
	}

	@Test
	public void updateAddress_WhenAddressContextIsNotNull_ThenAddressContextIsSavedWithConfiguredDefaultAddressType() throws Exception {
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockDdmFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(false);

		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_1_FIELD_NAME, mockLocale)).thenReturn(STREET_1);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_2_FIELD_NAME, mockLocale)).thenReturn(STREET_2);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_3_FIELD_NAME, mockLocale)).thenReturn(STREET_3);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.CITY_FIELD_NAME, mockLocale)).thenReturn(CITY);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.POSTCODE_FIELD_NAME, mockLocale)).thenReturn(POSTCODE);

		whenNew(AddressContext.class).withArguments(StringPool.BLANK, STREET_1, STREET_2, STREET_3, StringPool.BLANK, CITY, POSTCODE).thenReturn(mockAddressContext);
		when(mockAddressLookupService.saveAddress(eq(mockUser), any(AddressContext.class), eq(mockServiceContext))).thenReturn(mockAddress);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(AddressType.BUSINESS);

		mockChangeOfAddressUtil.updateAddress(mockDdmFormInstanceRecord, mockDDMFormValues, mockDDMFormFieldValuesMap, mockLocale, mockChangeOfAddress, SUBJECT, BODY, mockServiceContext);

		InOrder inOrder = inOrder(mockAddressContext, mockAddressLookupService);
		inOrder.verify(mockAddressContext, times(1)).setAddressType(AddressType.BUSINESS);
		inOrder.verify(mockAddressLookupService, times(1)).saveAddress(mockUser, mockAddressContext, mockServiceContext);
	}

	@Test
	public void updateAddress_WhenAddressContextIsNotNull_ThenAddressIsUpdated() throws Exception {
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(false);

		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_1_FIELD_NAME, mockLocale)).thenReturn(STREET_1);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_2_FIELD_NAME, mockLocale)).thenReturn(STREET_2);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_3_FIELD_NAME, mockLocale)).thenReturn(STREET_3);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.CITY_FIELD_NAME, mockLocale)).thenReturn(CITY);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.POSTCODE_FIELD_NAME, mockLocale)).thenReturn(POSTCODE);

		whenNew(AddressContext.class).withArguments(StringPool.BLANK, STREET_1, STREET_2, STREET_3, StringPool.BLANK, CITY, POSTCODE).thenReturn(mockAddressContext);

		when(mockAddressLookupService.saveAddress(eq(mockUser), any(AddressContext.class), eq(mockServiceContext))).thenReturn(mockAddress);
		when(mockAddress.getAddressId()).thenReturn(ADDRESS_ID);

		mockChangeOfAddressUtil.updateAddress(mockDdmFormInstanceRecord, mockDDMFormValues, mockDDMFormFieldValuesMap, mockLocale, mockChangeOfAddress, SUBJECT, BODY, mockServiceContext);

		InOrder inOrder = inOrder(mockChangeOfAddress, mockChangeOfAddressLocalService);
		inOrder.verify(mockChangeOfAddress, times(1)).setNewAddressId(ADDRESS_ID);
		inOrder.verify(mockChangeOfAddress, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockChangeOfAddressLocalService, times(1)).updateChangeOfAddress(mockChangeOfAddress);
	}

	@Test
	public void updateAddress_WhenAddressContextIsNotNull_ThenConfirmationEmailIsSent() throws Exception {
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(false);

		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_1_FIELD_NAME, mockLocale)).thenReturn(STREET_1);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_2_FIELD_NAME, mockLocale)).thenReturn(STREET_2);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.STREET_3_FIELD_NAME, mockLocale)).thenReturn(STREET_3);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.CITY_FIELD_NAME, mockLocale)).thenReturn(CITY);
		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.POSTCODE_FIELD_NAME, mockLocale)).thenReturn(POSTCODE);

		whenNew(AddressContext.class).withArguments(StringPool.BLANK, STREET_1, STREET_2, STREET_3, StringPool.BLANK, CITY, POSTCODE).thenReturn(mockAddressContext);

		when(mockAddressLookupService.saveAddress(eq(mockUser), any(AddressContext.class), eq(mockServiceContext))).thenReturn(mockAddress);
		when(mockAddress.getAddressId()).thenReturn(ADDRESS_ID);

		when(ChangeOfAddressFormUtil.getFieldValue(mockDDMFormFieldValuesMap, ChangeOfAddressFormConstants.EMAIL_ADDRESS_FIELD_NAME, mockLocale)).thenReturn(EMAIL_ADDRESS);
		when(mockChangeOfAddressContentUtil.replaceEmailValues(BODY, mockDDMFormFieldValuesMap, mockLocale, mockUser)).thenReturn(UPDATED_BODY);
		when(mockChangeOfAddressContentUtil.replaceEmailValues(SUBJECT, mockDDMFormFieldValuesMap, mockLocale, mockUser)).thenReturn(UPDATED_SUBJECT);

		mockChangeOfAddressUtil.updateAddress(mockDdmFormInstanceRecord, mockDDMFormValues, mockDDMFormFieldValuesMap, mockLocale, mockChangeOfAddress, SUBJECT, BODY, mockServiceContext);

		verify(mockMailService, times(1)).sendEmail(EMAIL_ADDRESS, EMAIL_ADDRESS, UPDATED_SUBJECT, UPDATED_BODY);
	}

	@Test
	public void updateAddress_WhenAddressIsFromSameCouncil_ThenAddressContextIsFetched() throws Exception {
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockDdmFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(true);

		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockAddressMap);
		when(mockAddressDDMFormFieldTypeUtil.getUPRN(mockAddressMap)).thenReturn(Optional.of(UPRN));

		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenReturn(Optional.of(mockAddressContext));

		when(mockAddressLookupService.saveAddress(eq(mockUser), any(AddressContext.class), eq(mockServiceContext))).thenReturn(mockAddress);
		when(mockAddress.getAddressId()).thenReturn(ADDRESS_ID);

		mockChangeOfAddressUtil.updateAddress(mockDdmFormInstanceRecord, mockDDMFormValues, mockDDMFormFieldValuesMap, mockLocale, mockChangeOfAddress, SUBJECT, BODY, mockServiceContext);

		verify(mockAddressDDMFormFieldTypeUtil, times(1)).getUPRN(mockAddressMap);
		verify(mockAddressLookupService, times(1)).getByUprn(COMPANY_ID, UPRN, new String[0], false);
	}

	@Test(expected = PortalException.class)
	public void updateAddress_WhenErrorGettingAddress_ThenPortalExceptionIsThrown() throws Exception {
		when(mockDdmFormInstanceRecord.getUserId()).thenReturn(USER_ID);
		when(mockDdmFormInstanceRecord.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		when(ChangeOfAddressFormUtil.isSameCouncil(mockDDMFormFieldValuesMap, mockLocale)).thenReturn(true);

		when(mockAddressDDMFormFieldTypeUtil.getFirstAddressFieldNameAndSettingValueMap(mockDDMFormValues)).thenReturn(mockAddressMap);
		when(mockAddressDDMFormFieldTypeUtil.getUPRN(mockAddressMap)).thenReturn(Optional.of(UPRN));

		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], false)).thenThrow(PortalException.class);

		mockChangeOfAddressUtil.updateAddress(mockDdmFormInstanceRecord, mockDDMFormValues, mockDDMFormFieldValuesMap, mockLocale, mockChangeOfAddress, SUBJECT, BODY, mockServiceContext);
	}

}
