package com.placecube.digitalplace.local.account.changeofaddress.internal.scheduler;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.placecube.digitalplace.local.account.changeofaddress.configuration.ChangeOfAddressConfiguration;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressContentUtil;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressUtil;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.service.ChangeOfAddressLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ConfigurableUtil.class, PortalUUIDUtil.class, TriggerConfiguration.class, CompanyThreadLocal.class })
public class ChangeOfAddressSchedulerJobConfigurationTest extends PowerMockito {

	private static final String BODY = "Body";
	private static final long COMPANY_ID = 1;
	private static final String CRON_EXPRESSION = "0 3 0 * * ?";
	private static final long DDM_FORM_INSTANCE_RECORD_ID = 0;
	private static final long GROUP_ID = 2;
	private static final String SUBJECT = "Subject";
	private static final long USER_ID = 3;
	private static final String UUID = "u_u_i_d";

	@InjectMocks
	private ChangeOfAddressSchedulerJobConfiguration changeOfAddressSchedulerJobConfiguration;

	@Mock
	private ChangeOfAddressConfiguration mockChangeOfAddressConfiguration;

	@Mock
	private ChangeOfAddressContentUtil mockChangeOfAddressContentUtil;

	@Mock
	private ChangeOfAddressLocalService mockChangeOfAddressLocalService;

	@Mock
	private ChangeOfAddressUtil mockChangeOfAddressUtil;

	@Mock
	private Map<String, List<DDMFormFieldValue>> mockDDMFormFieldValuesMap;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMFormInstanceRecordLocalService mockDdmFormInstanceRecordLocalService;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private Locale mockLocale;

	@Mock
	private Map<String, Object> mockProperties;

	@Mock
	private TriggerConfiguration mockTriggerConfiguration;

	@Mock
	private UserLocalService mockUserLocalService;

	@Test
	public void activate_WhenNoError_ActivateScheduler() {
		changeOfAddressSchedulerJobConfiguration.activate(mockProperties);

		verifyStatic(ConfigurableUtil.class, times(1));
		ConfigurableUtil.createConfigurable(ChangeOfAddressConfiguration.class, mockProperties);
	}

	@Before
	public void activateSetUp() {
		mockStatic(ConfigurableUtil.class, PortalUUIDUtil.class, TriggerConfiguration.class, CompanyThreadLocal.class);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenThereAreChangesOfAddressToUpdate_ThenChangesOfAddressAreUpdated() throws Exception {
		List<ChangeOfAddress> mockChangesOfAddress = new ArrayList<>();
		ChangeOfAddress mockChangeOfAddress = mock(ChangeOfAddress.class);
		mockChangesOfAddress.add(mockChangeOfAddress);

		when(mockChangeOfAddressConfiguration.schedulerEnabled()).thenReturn(true);
		when(mockChangeOfAddressLocalService.getChangeOfAddressesToUpdate(COMPANY_ID)).thenReturn(mockChangesOfAddress);

		when(mockChangeOfAddress.getFormInstanceRecordId()).thenReturn(DDM_FORM_INSTANCE_RECORD_ID);

		when(mockDdmFormInstanceRecordLocalService.getDDMFormInstanceRecord(DDM_FORM_INSTANCE_RECORD_ID)).thenReturn(mockDDMFormInstanceRecord);
		when(mockDDMFormInstanceRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(mockDDMFormFieldValuesMap);
		when(mockDDMFormValues.getDefaultLocale()).thenReturn(mockLocale);

		when(PortalUUIDUtil.generate()).thenReturn(UUID);

		when(mockChangeOfAddress.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(USER_ID);

		when(mockChangeOfAddress.getGroupId()).thenReturn(GROUP_ID);

		when(mockChangeOfAddressContentUtil.getOrCreateJournalArticleEmail(any(ServiceContext.class))).thenReturn(mockJournalArticle);

		when(mockChangeOfAddressContentUtil.getSubject(mockJournalArticle, mockLocale)).thenReturn(SUBJECT);
		when(mockChangeOfAddressContentUtil.getBody(mockJournalArticle, mockLocale)).thenReturn(BODY);

		UnsafeConsumer<Long, Exception> companyJobExecutorUnsafeConsumer = changeOfAddressSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		companyJobExecutorUnsafeConsumer.accept(COMPANY_ID);

		verify(mockChangeOfAddressLocalService, times(1)).getChangeOfAddressesToUpdate(COMPANY_ID);
		verify(mockChangeOfAddressUtil, times(1)).updateAddress(eq(mockDDMFormInstanceRecord), eq(mockDDMFormValues), eq(mockDDMFormFieldValuesMap), eq(mockLocale), eq(mockChangeOfAddress),
				eq(SUBJECT), eq(BODY), any(ServiceContext.class));
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenThereAreNoChangesOfAddressToUpdate_ThenNoUpdatesAreExecuted() throws Exception {
		when(mockChangeOfAddressConfiguration.schedulerEnabled()).thenReturn(true);
		when(mockChangeOfAddressLocalService.getChangeOfAddressesToUpdate(COMPANY_ID)).thenReturn(new ArrayList<>());

		UnsafeConsumer<Long, Exception> companyJobExecutorUnsafeConsumer = changeOfAddressSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		companyJobExecutorUnsafeConsumer.accept(COMPANY_ID);

		verify(mockChangeOfAddressLocalService, times(1)).getChangeOfAddressesToUpdate(COMPANY_ID);
		verifyZeroInteractions(mockChangeOfAddressUtil);
		verifyStatic(CompanyThreadLocal.class, times(1));
		CompanyThreadLocal.setCompanyId(COMPANY_ID);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenSchedulerDisabled_ThenDoNothing() throws Exception {
		when(mockChangeOfAddressConfiguration.schedulerEnabled()).thenReturn(false);

		UnsafeConsumer<Long, Exception> companyJobExecutorUnsafeConsumer = changeOfAddressSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer();
		companyJobExecutorUnsafeConsumer.accept(COMPANY_ID);

		verifyZeroInteractions(mockChangeOfAddressUtil, mockChangeOfAddressLocalService);
	}

	@Test
	public void getTriggerConfiguration_WhenNoError_ThenReturnTriggerConfiguration() {
		when(mockChangeOfAddressConfiguration.schedulerCronExpression()).thenReturn(CRON_EXPRESSION);
		when(TriggerConfiguration.createTriggerConfiguration(CRON_EXPRESSION)).thenReturn(mockTriggerConfiguration);

		TriggerConfiguration result = changeOfAddressSchedulerJobConfiguration.getTriggerConfiguration();

		assertThat(result, equalTo(mockTriggerConfiguration));
	}

}
