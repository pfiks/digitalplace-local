package com.placecube.digitalplace.local.account.changeofaddress.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.local.account.changeofaddress.exception.NoSuchChangeOfAddressException;
import com.placecube.digitalplace.local.account.changeofaddress.model.ChangeOfAddress;
import com.placecube.digitalplace.local.account.changeofaddress.service.persistence.ChangeOfAddressPersistence;

@RunWith(PowerMockRunner.class)
public class ChangeOfAddressLocalServiceImplTest extends PowerMockito {

	private static final long CHANGE_OF_ADDRESS_ID = 6;
	private static final long COMPANY_ID = 2;
	private static final long FORM_INSTANCE_RECORD_ID = 3;
	private static final long GROUP_ID = 1;
	private static final long USER_ID = 4;
	private static final String USERNAME = "username";

	@InjectMocks
	private ChangeOfAddressLocalServiceImpl changeOfAddressLocalServiceImpl;

	@Mock
	private ChangeOfAddress mockChangeOfAddress;

	@Mock
	private ChangeOfAddressPersistence mockChangeOfAddressPersistence;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Test
	public void addChangeOfAddress_WhenNoError_ThenChangeOfAddressIsCreated() throws PortalException {

		when(mockCounterLocalService.increment(ChangeOfAddress.class.getName())).thenReturn(CHANGE_OF_ADDRESS_ID);
		when(mockChangeOfAddressPersistence.create(CHANGE_OF_ADDRESS_ID)).thenReturn(mockChangeOfAddress);

		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getFullName()).thenReturn(USERNAME);

		changeOfAddressLocalServiceImpl.addChangeOfAddress(GROUP_ID, COMPANY_ID, USER_ID, FORM_INSTANCE_RECORD_ID, new Date());

		InOrder inOrder = inOrder(mockChangeOfAddress, mockChangeOfAddressPersistence);
		inOrder.verify(mockChangeOfAddress, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockChangeOfAddress, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockChangeOfAddress, times(1)).setUserId(USER_ID);
		inOrder.verify(mockChangeOfAddress, times(1)).setUserName(USERNAME);
		inOrder.verify(mockChangeOfAddress, times(1)).setCreateDate(any(Date.class));
		inOrder.verify(mockChangeOfAddress, times(1)).setModifiedDate(any(Date.class));
		inOrder.verify(mockChangeOfAddress, times(1)).setFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);
		inOrder.verify(mockChangeOfAddress, times(1)).setAddressUpdateDate(any(Date.class));
		inOrder.verify(mockChangeOfAddressPersistence, times(1)).update(mockChangeOfAddress);
	}

	@Test(expected = NoSuchChangeOfAddressException.class)
	public void deleteByFormInstanceRecordId_WhenChangeOfAddressDoesNotExist_ThenExceptionIsThrown() throws NoSuchChangeOfAddressException {
		when(mockChangeOfAddressPersistence.removeByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID)).thenThrow(NoSuchChangeOfAddressException.class);

		changeOfAddressLocalServiceImpl.deleteByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void deleteByFormInstanceRecordId_WhenNoError_ThenChangeOfAddressIsDeleted() throws NoSuchChangeOfAddressException {
		when(mockChangeOfAddressPersistence.removeByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID)).thenReturn(mockChangeOfAddress);

		changeOfAddressLocalServiceImpl.deleteByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);

		verify(mockChangeOfAddressPersistence, times(1)).removeByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);
	}

	@Test
	public void getChangeOfAddressesToUpdate_WhenChangesOfAddressDoNotExist_ThenReturnedListIsEmpty() {
		when(mockChangeOfAddressPersistence.findByNewAddressIdAndCompanyId(0, COMPANY_ID)).thenReturn(new ArrayList<>());

		List<ChangeOfAddress> result = changeOfAddressLocalServiceImpl.getChangeOfAddressesToUpdate(COMPANY_ID);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getChangeOfAddressesToUpdate_WhenChangesOfAddressExist_ThenListIsReturned() {
		List<ChangeOfAddress> mockChangesOfAddress = new ArrayList<>();
		mockChangesOfAddress.add(mockChangeOfAddress);

		Date mockDate = mock(Date.class);

		when(mockChangeOfAddressPersistence.findByNewAddressIdAndCompanyId(0, COMPANY_ID)).thenReturn(mockChangesOfAddress);

		when(mockChangeOfAddress.getAddressUpdateDate()).thenReturn(mockDate);
		when(mockDate.before(any(Date.class))).thenReturn(true);

		List<ChangeOfAddress> result = changeOfAddressLocalServiceImpl.getChangeOfAddressesToUpdate(COMPANY_ID);

		assertTrue(result.contains(mockChangeOfAddress));
		assertEquals(mockChangesOfAddress.size(), result.size());

	}

	@Test
	public void getChangeOfAddressesToUpdate_WhenChangesOfAddressExistButAreBeforeCurrentDate_ThenReturnedListIsEmpty() {
		List<ChangeOfAddress> mockChangesOfAddress = new ArrayList<>();
		mockChangesOfAddress.add(mockChangeOfAddress);

		Date mockDate = mock(Date.class);

		when(mockChangeOfAddressPersistence.findByNewAddressIdAndCompanyId(0, COMPANY_ID)).thenReturn(mockChangesOfAddress);

		when(mockChangeOfAddress.getAddressUpdateDate()).thenReturn(mockDate);
		when(mockDate.before(any(Date.class))).thenReturn(false);

		List<ChangeOfAddress> result = changeOfAddressLocalServiceImpl.getChangeOfAddressesToUpdate(COMPANY_ID);

		assertTrue(result.isEmpty());
	}

}
