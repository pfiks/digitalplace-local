package com.placecube.digitalplace.local.account.changeofaddress.internal.model.listener;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.local.account.changeofaddress.internal.constants.ChangeOfAddressFormConstants;
import com.placecube.digitalplace.local.account.changeofaddress.internal.util.ChangeOfAddressUtil;

@RunWith(PowerMockRunner.class)
public class ChangeOfAddressDDMFormInstanceModelListenerTest extends PowerMockito {

	private static final long FORM_INSTANCE_RECORD_ID = 1;

	@InjectMocks
	private ChangeOfAddressDDMFormInstanceModelListener changeOfAddressDDMFormInstanceModelListener;

	@Mock
	private ChangeOfAddressUtil mockChangeOfAddressUtil;

	@Mock
	private DDMFormInstance mockDdmFormInstance;

	@Mock
	private DDMFormInstanceRecord mockDDMFormInstanceRecord;

	@Mock
	private DDMStructure mockDdmStructure;

	@Mock
	private DDMFormInstanceRecord mockOriginalDDMFormInstanceRecord;

	@Test
	public void onAfterRemove_WhenFormRecordIsChangeOfAddress_ThenChangeOfAddressDataIsRemoved() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockDdmFormInstance);
		when(mockDdmFormInstance.getStructure()).thenReturn(mockDdmStructure);
		when(mockDdmStructure.getStructureKey()).thenReturn(ChangeOfAddressFormConstants.CHANGE_OF_ADDRESS_STRUCTURE_KEY);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);

		changeOfAddressDDMFormInstanceModelListener.onAfterRemove(mockDDMFormInstanceRecord);

		verify(mockChangeOfAddressUtil, times(1)).removeChangeOfAddressByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);

	}

	@Test
	public void onAfterRemove_WhenFormRecordIsNotChangeOfAddress_ThenChangeOfAddressDataIsNotRemoved() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockDdmFormInstance);
		when(mockDdmFormInstance.getStructure()).thenReturn(mockDdmStructure);
		when(mockDdmStructure.getStructureKey()).thenReturn(StringPool.BLANK);
		when(mockDDMFormInstanceRecord.getFormInstanceRecordId()).thenReturn(FORM_INSTANCE_RECORD_ID);

		changeOfAddressDDMFormInstanceModelListener.onAfterRemove(mockDDMFormInstanceRecord);

		verify(mockChangeOfAddressUtil, never()).removeChangeOfAddressByFormInstanceRecordId(FORM_INSTANCE_RECORD_ID);

	}

	@Test
	public void onAfterUpdate_WhenNoError_ThenRecordIsProcessed() throws PortalException {
		when(mockDDMFormInstanceRecord.getFormInstance()).thenReturn(mockDdmFormInstance);
		when(mockDdmFormInstance.getStructure()).thenReturn(mockDdmStructure);
		when(mockDdmStructure.getStructureKey()).thenReturn(ChangeOfAddressFormConstants.CHANGE_OF_ADDRESS_STRUCTURE_KEY);

		changeOfAddressDDMFormInstanceModelListener.onAfterUpdate(mockOriginalDDMFormInstanceRecord, mockDDMFormInstanceRecord);

		verify(mockChangeOfAddressUtil, times(1)).processFormInstanceRecord(mockDDMFormInstanceRecord);
	}
}
