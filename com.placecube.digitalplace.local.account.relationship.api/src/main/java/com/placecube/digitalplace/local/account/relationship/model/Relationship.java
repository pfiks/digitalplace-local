/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the Relationship service. Represents a row in the &quot;Placecube_Account_Relationships_Relationship&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see RelationshipModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipImpl"
)
@ProviderType
public interface Relationship extends PersistedModel, RelationshipModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Relationship, Long> RELATIONSHIP_ID_ACCESSOR =
		new Accessor<Relationship, Long>() {

			@Override
			public Long get(Relationship relationship) {
				return relationship.getRelationshipId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Relationship> getTypeClass() {
				return Relationship.class;
			}

		};

}