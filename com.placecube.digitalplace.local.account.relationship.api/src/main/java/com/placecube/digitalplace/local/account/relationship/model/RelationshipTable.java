/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Account_Relationships_Relationship&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see Relationship
 * @generated
 */
public class RelationshipTable extends BaseTable<RelationshipTable> {

	public static final RelationshipTable INSTANCE = new RelationshipTable();

	public final Column<RelationshipTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<RelationshipTable, Long> relationshipId = createColumn(
		"relationshipId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<RelationshipTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<RelationshipTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<RelationshipTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<RelationshipTable, Long> relUserId = createColumn(
		"relUserId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<RelationshipTable, String> type = createColumn(
		"type_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);

	private RelationshipTable() {
		super(
			"Placecube_Account_Relationships_Relationship",
			RelationshipTable::new);
	}

}