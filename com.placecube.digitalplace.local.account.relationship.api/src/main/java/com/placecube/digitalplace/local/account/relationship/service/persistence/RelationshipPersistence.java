/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.local.account.relationship.exception.NoSuchRelationshipException;
import com.placecube.digitalplace.local.account.relationship.model.Relationship;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the relationship service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RelationshipUtil
 * @generated
 */
@ProviderType
public interface RelationshipPersistence extends BasePersistence<Relationship> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RelationshipUtil} to access the relationship persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the relationships where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching relationships
	 */
	public java.util.List<Relationship> findByUuid(String uuid);

	/**
	 * Returns a range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	public java.util.List<Relationship> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	public java.util.List<Relationship> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	public java.util.List<Relationship> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where uuid = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public Relationship[] findByUuid_PrevAndNext(
			long relationshipId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Removes all the relationships where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of relationships where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching relationships
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching relationships
	 */
	public java.util.List<Relationship> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	public java.util.List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	public java.util.List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	public java.util.List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public Relationship[] findByUuid_C_PrevAndNext(
			long relationshipId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Removes all the relationships where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching relationships
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @return the matching relationships
	 */
	public java.util.List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId);

	/**
	 * Returns a range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	public java.util.List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	public java.util.List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns an ordered range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	public java.util.List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUserIdAndCompanyId_First(
			long userId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Returns the first relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUserIdAndCompanyId_First(
		long userId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns the last relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUserIdAndCompanyId_Last(
			long userId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Returns the last relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUserIdAndCompanyId_Last(
		long userId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public Relationship[] findByUserIdAndCompanyId_PrevAndNext(
			long relationshipId, long userId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Relationship>
				orderByComparator)
		throws NoSuchRelationshipException;

	/**
	 * Removes all the relationships where userId = &#63; and companyId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 */
	public void removeByUserIdAndCompanyId(long userId, long companyId);

	/**
	 * Returns the number of relationships where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @return the number of matching relationships
	 */
	public int countByUserIdAndCompanyId(long userId, long companyId);

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or throws a <code>NoSuchRelationshipException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public Relationship findByUserIdAndRelUserId(long userId, long relUserId)
		throws NoSuchRelationshipException;

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUserIdAndRelUserId(long userId, long relUserId);

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public Relationship fetchByUserIdAndRelUserId(
		long userId, long relUserId, boolean useFinderCache);

	/**
	 * Removes the relationship where userId = &#63; and relUserId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the relationship that was removed
	 */
	public Relationship removeByUserIdAndRelUserId(long userId, long relUserId)
		throws NoSuchRelationshipException;

	/**
	 * Returns the number of relationships where userId = &#63; and relUserId = &#63;.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the number of matching relationships
	 */
	public int countByUserIdAndRelUserId(long userId, long relUserId);

	/**
	 * Caches the relationship in the entity cache if it is enabled.
	 *
	 * @param relationship the relationship
	 */
	public void cacheResult(Relationship relationship);

	/**
	 * Caches the relationships in the entity cache if it is enabled.
	 *
	 * @param relationships the relationships
	 */
	public void cacheResult(java.util.List<Relationship> relationships);

	/**
	 * Creates a new relationship with the primary key. Does not add the relationship to the database.
	 *
	 * @param relationshipId the primary key for the new relationship
	 * @return the new relationship
	 */
	public Relationship create(long relationshipId);

	/**
	 * Removes the relationship with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship that was removed
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public Relationship remove(long relationshipId)
		throws NoSuchRelationshipException;

	public Relationship updateImpl(Relationship relationship);

	/**
	 * Returns the relationship with the primary key or throws a <code>NoSuchRelationshipException</code> if it could not be found.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public Relationship findByPrimaryKey(long relationshipId)
		throws NoSuchRelationshipException;

	/**
	 * Returns the relationship with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship, or <code>null</code> if a relationship with the primary key could not be found
	 */
	public Relationship fetchByPrimaryKey(long relationshipId);

	/**
	 * Returns all the relationships.
	 *
	 * @return the relationships
	 */
	public java.util.List<Relationship> findAll();

	/**
	 * Returns a range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of relationships
	 */
	public java.util.List<Relationship> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of relationships
	 */
	public java.util.List<Relationship> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator);

	/**
	 * Returns an ordered range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of relationships
	 */
	public java.util.List<Relationship> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Relationship>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the relationships from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of relationships.
	 *
	 * @return the number of relationships
	 */
	public int countAll();

}