/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link RelationshipLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see RelationshipLocalService
 * @generated
 */
public class RelationshipLocalServiceWrapper
	implements RelationshipLocalService,
			   ServiceWrapper<RelationshipLocalService> {

	public RelationshipLocalServiceWrapper() {
		this(null);
	}

	public RelationshipLocalServiceWrapper(
		RelationshipLocalService relationshipLocalService) {

		_relationshipLocalService = relationshipLocalService;
	}

	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			addRelationship(
				long userId, long relUserId, long companyId, String type) {

		return _relationshipLocalService.addRelationship(
			userId, relUserId, companyId, type);
	}

	/**
	 * Adds the relationship to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationship the relationship
	 * @return the relationship that was added
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			addRelationship(
				com.placecube.digitalplace.local.account.relationship.model.
					Relationship relationship) {

		return _relationshipLocalService.addRelationship(relationship);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _relationshipLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new relationship with the primary key. Does not add the relationship to the database.
	 *
	 * @param relationshipId the primary key for the new relationship
	 * @return the new relationship
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			createRelationship(long relationshipId) {

		return _relationshipLocalService.createRelationship(relationshipId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _relationshipLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the relationship with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship that was removed
	 * @throws PortalException if a relationship with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
				deleteRelationship(long relationshipId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _relationshipLocalService.deleteRelationship(relationshipId);
	}

	/**
	 * Deletes the relationship from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationship the relationship
	 * @return the relationship that was removed
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			deleteRelationship(
				com.placecube.digitalplace.local.account.relationship.model.
					Relationship relationship) {

		return _relationshipLocalService.deleteRelationship(relationship);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _relationshipLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _relationshipLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _relationshipLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _relationshipLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _relationshipLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _relationshipLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _relationshipLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _relationshipLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			fetchRelationship(long relationshipId) {

		return _relationshipLocalService.fetchRelationship(relationshipId);
	}

	/**
	 * Returns the relationship with the matching UUID and company.
	 *
	 * @param uuid the relationship's UUID
	 * @param companyId the primary key of the company
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			fetchRelationshipByUuidAndCompanyId(String uuid, long companyId) {

		return _relationshipLocalService.fetchRelationshipByUuidAndCompanyId(
			uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _relationshipLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _relationshipLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _relationshipLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _relationshipLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the relationship with the primary key.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship
	 * @throws PortalException if a relationship with the primary key could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
				getRelationship(long relationshipId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _relationshipLocalService.getRelationship(relationshipId);
	}

	/**
	 * Returns the relationship with the matching UUID and company.
	 *
	 * @param uuid the relationship's UUID
	 * @param companyId the primary key of the company
	 * @return the matching relationship
	 * @throws PortalException if a matching relationship could not be found
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
				getRelationshipByUuidAndCompanyId(String uuid, long companyId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _relationshipLocalService.getRelationshipByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of relationships
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.relationship.model.
			Relationship> getRelationships(int start, int end) {

		return _relationshipLocalService.getRelationships(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.local.account.relationship.model.
			Relationship> getRelationshipsByUserIdAndCompanyId(
				long userId, long companyId) {

		return _relationshipLocalService.getRelationshipsByUserIdAndCompanyId(
			userId, companyId);
	}

	/**
	 * Returns the number of relationships.
	 *
	 * @return the number of relationships
	 */
	@Override
	public int getRelationshipsCount() {
		return _relationshipLocalService.getRelationshipsCount();
	}

	/**
	 * Updates the relationship in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationship the relationship
	 * @return the relationship that was updated
	 */
	@Override
	public
		com.placecube.digitalplace.local.account.relationship.model.Relationship
			updateRelationship(
				com.placecube.digitalplace.local.account.relationship.model.
					Relationship relationship) {

		return _relationshipLocalService.updateRelationship(relationship);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _relationshipLocalService.getBasePersistence();
	}

	@Override
	public RelationshipLocalService getWrappedService() {
		return _relationshipLocalService;
	}

	@Override
	public void setWrappedService(
		RelationshipLocalService relationshipLocalService) {

		_relationshipLocalService = relationshipLocalService;
	}

	private RelationshipLocalService _relationshipLocalService;

}