/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.relationship.model.Relationship;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Relationship. This utility wraps
 * <code>com.placecube.digitalplace.local.account.relationship.service.impl.RelationshipLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see RelationshipLocalService
 * @generated
 */
public class RelationshipLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.local.account.relationship.service.impl.RelationshipLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Relationship addRelationship(
		long userId, long relUserId, long companyId, String type) {

		return getService().addRelationship(userId, relUserId, companyId, type);
	}

	/**
	 * Adds the relationship to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationship the relationship
	 * @return the relationship that was added
	 */
	public static Relationship addRelationship(Relationship relationship) {
		return getService().addRelationship(relationship);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new relationship with the primary key. Does not add the relationship to the database.
	 *
	 * @param relationshipId the primary key for the new relationship
	 * @return the new relationship
	 */
	public static Relationship createRelationship(long relationshipId) {
		return getService().createRelationship(relationshipId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the relationship with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship that was removed
	 * @throws PortalException if a relationship with the primary key could not be found
	 */
	public static Relationship deleteRelationship(long relationshipId)
		throws PortalException {

		return getService().deleteRelationship(relationshipId);
	}

	/**
	 * Deletes the relationship from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationship the relationship
	 * @return the relationship that was removed
	 */
	public static Relationship deleteRelationship(Relationship relationship) {
		return getService().deleteRelationship(relationship);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Relationship fetchRelationship(long relationshipId) {
		return getService().fetchRelationship(relationshipId);
	}

	/**
	 * Returns the relationship with the matching UUID and company.
	 *
	 * @param uuid the relationship's UUID
	 * @param companyId the primary key of the company
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchRelationshipByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchRelationshipByUuidAndCompanyId(
			uuid, companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the relationship with the primary key.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship
	 * @throws PortalException if a relationship with the primary key could not be found
	 */
	public static Relationship getRelationship(long relationshipId)
		throws PortalException {

		return getService().getRelationship(relationshipId);
	}

	/**
	 * Returns the relationship with the matching UUID and company.
	 *
	 * @param uuid the relationship's UUID
	 * @param companyId the primary key of the company
	 * @return the matching relationship
	 * @throws PortalException if a matching relationship could not be found
	 */
	public static Relationship getRelationshipByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getRelationshipByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.local.account.relationship.model.impl.RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of relationships
	 */
	public static List<Relationship> getRelationships(int start, int end) {
		return getService().getRelationships(start, end);
	}

	public static List<Relationship> getRelationshipsByUserIdAndCompanyId(
		long userId, long companyId) {

		return getService().getRelationshipsByUserIdAndCompanyId(
			userId, companyId);
	}

	/**
	 * Returns the number of relationships.
	 *
	 * @return the number of relationships
	 */
	public static int getRelationshipsCount() {
		return getService().getRelationshipsCount();
	}

	/**
	 * Updates the relationship in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect RelationshipLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param relationship the relationship
	 * @return the relationship that was updated
	 */
	public static Relationship updateRelationship(Relationship relationship) {
		return getService().updateRelationship(relationship);
	}

	public static RelationshipLocalService getService() {
		return _service;
	}

	public static void setService(RelationshipLocalService service) {
		_service = service;
	}

	private static volatile RelationshipLocalService _service;

}