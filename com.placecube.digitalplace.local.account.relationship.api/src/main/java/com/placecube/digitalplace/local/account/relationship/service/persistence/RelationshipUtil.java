/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.local.account.relationship.model.Relationship;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the relationship service. This utility wraps <code>com.placecube.digitalplace.local.account.relationship.service.persistence.impl.RelationshipPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RelationshipPersistence
 * @generated
 */
public class RelationshipUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Relationship relationship) {
		getPersistence().clearCache(relationship);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Relationship> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Relationship> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Relationship> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Relationship> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Relationship update(Relationship relationship) {
		return getPersistence().update(relationship);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Relationship update(
		Relationship relationship, ServiceContext serviceContext) {

		return getPersistence().update(relationship, serviceContext);
	}

	/**
	 * Returns all the relationships where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching relationships
	 */
	public static List<Relationship> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	public static List<Relationship> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	public static List<Relationship> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	public static List<Relationship> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUuid_First(
			String uuid, OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUuid_First(
		String uuid, OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUuid_Last(
			String uuid, OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUuid_Last(
		String uuid, OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where uuid = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public static Relationship[] findByUuid_PrevAndNext(
			long relationshipId, String uuid,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUuid_PrevAndNext(
			relationshipId, uuid, orderByComparator);
	}

	/**
	 * Removes all the relationships where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of relationships where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching relationships
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching relationships
	 */
	public static List<Relationship> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	public static List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	public static List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	public static List<Relationship> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public static Relationship[] findByUuid_C_PrevAndNext(
			long relationshipId, String uuid, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUuid_C_PrevAndNext(
			relationshipId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the relationships where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of relationships where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching relationships
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @return the matching relationships
	 */
	public static List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId) {

		return getPersistence().findByUserIdAndCompanyId(userId, companyId);
	}

	/**
	 * Returns a range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of matching relationships
	 */
	public static List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end) {

		return getPersistence().findByUserIdAndCompanyId(
			userId, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching relationships
	 */
	public static List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().findByUserIdAndCompanyId(
			userId, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the relationships where userId = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching relationships
	 */
	public static List<Relationship> findByUserIdAndCompanyId(
		long userId, long companyId, int start, int end,
		OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUserIdAndCompanyId(
			userId, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUserIdAndCompanyId_First(
			long userId, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUserIdAndCompanyId_First(
			userId, companyId, orderByComparator);
	}

	/**
	 * Returns the first relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUserIdAndCompanyId_First(
		long userId, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().fetchByUserIdAndCompanyId_First(
			userId, companyId, orderByComparator);
	}

	/**
	 * Returns the last relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUserIdAndCompanyId_Last(
			long userId, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUserIdAndCompanyId_Last(
			userId, companyId, orderByComparator);
	}

	/**
	 * Returns the last relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUserIdAndCompanyId_Last(
		long userId, long companyId,
		OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().fetchByUserIdAndCompanyId_Last(
			userId, companyId, orderByComparator);
	}

	/**
	 * Returns the relationships before and after the current relationship in the ordered set where userId = &#63; and companyId = &#63;.
	 *
	 * @param relationshipId the primary key of the current relationship
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public static Relationship[] findByUserIdAndCompanyId_PrevAndNext(
			long relationshipId, long userId, long companyId,
			OrderByComparator<Relationship> orderByComparator)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUserIdAndCompanyId_PrevAndNext(
			relationshipId, userId, companyId, orderByComparator);
	}

	/**
	 * Removes all the relationships where userId = &#63; and companyId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 */
	public static void removeByUserIdAndCompanyId(long userId, long companyId) {
		getPersistence().removeByUserIdAndCompanyId(userId, companyId);
	}

	/**
	 * Returns the number of relationships where userId = &#63; and companyId = &#63;.
	 *
	 * @param userId the user ID
	 * @param companyId the company ID
	 * @return the number of matching relationships
	 */
	public static int countByUserIdAndCompanyId(long userId, long companyId) {
		return getPersistence().countByUserIdAndCompanyId(userId, companyId);
	}

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or throws a <code>NoSuchRelationshipException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the matching relationship
	 * @throws NoSuchRelationshipException if a matching relationship could not be found
	 */
	public static Relationship findByUserIdAndRelUserId(
			long userId, long relUserId)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByUserIdAndRelUserId(userId, relUserId);
	}

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUserIdAndRelUserId(
		long userId, long relUserId) {

		return getPersistence().fetchByUserIdAndRelUserId(userId, relUserId);
	}

	/**
	 * Returns the relationship where userId = &#63; and relUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching relationship, or <code>null</code> if a matching relationship could not be found
	 */
	public static Relationship fetchByUserIdAndRelUserId(
		long userId, long relUserId, boolean useFinderCache) {

		return getPersistence().fetchByUserIdAndRelUserId(
			userId, relUserId, useFinderCache);
	}

	/**
	 * Removes the relationship where userId = &#63; and relUserId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the relationship that was removed
	 */
	public static Relationship removeByUserIdAndRelUserId(
			long userId, long relUserId)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().removeByUserIdAndRelUserId(userId, relUserId);
	}

	/**
	 * Returns the number of relationships where userId = &#63; and relUserId = &#63;.
	 *
	 * @param userId the user ID
	 * @param relUserId the rel user ID
	 * @return the number of matching relationships
	 */
	public static int countByUserIdAndRelUserId(long userId, long relUserId) {
		return getPersistence().countByUserIdAndRelUserId(userId, relUserId);
	}

	/**
	 * Caches the relationship in the entity cache if it is enabled.
	 *
	 * @param relationship the relationship
	 */
	public static void cacheResult(Relationship relationship) {
		getPersistence().cacheResult(relationship);
	}

	/**
	 * Caches the relationships in the entity cache if it is enabled.
	 *
	 * @param relationships the relationships
	 */
	public static void cacheResult(List<Relationship> relationships) {
		getPersistence().cacheResult(relationships);
	}

	/**
	 * Creates a new relationship with the primary key. Does not add the relationship to the database.
	 *
	 * @param relationshipId the primary key for the new relationship
	 * @return the new relationship
	 */
	public static Relationship create(long relationshipId) {
		return getPersistence().create(relationshipId);
	}

	/**
	 * Removes the relationship with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship that was removed
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public static Relationship remove(long relationshipId)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().remove(relationshipId);
	}

	public static Relationship updateImpl(Relationship relationship) {
		return getPersistence().updateImpl(relationship);
	}

	/**
	 * Returns the relationship with the primary key or throws a <code>NoSuchRelationshipException</code> if it could not be found.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship
	 * @throws NoSuchRelationshipException if a relationship with the primary key could not be found
	 */
	public static Relationship findByPrimaryKey(long relationshipId)
		throws com.placecube.digitalplace.local.account.relationship.exception.
			NoSuchRelationshipException {

		return getPersistence().findByPrimaryKey(relationshipId);
	}

	/**
	 * Returns the relationship with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param relationshipId the primary key of the relationship
	 * @return the relationship, or <code>null</code> if a relationship with the primary key could not be found
	 */
	public static Relationship fetchByPrimaryKey(long relationshipId) {
		return getPersistence().fetchByPrimaryKey(relationshipId);
	}

	/**
	 * Returns all the relationships.
	 *
	 * @return the relationships
	 */
	public static List<Relationship> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @return the range of relationships
	 */
	public static List<Relationship> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of relationships
	 */
	public static List<Relationship> findAll(
		int start, int end, OrderByComparator<Relationship> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the relationships.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>RelationshipModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of relationships
	 * @param end the upper bound of the range of relationships (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of relationships
	 */
	public static List<Relationship> findAll(
		int start, int end, OrderByComparator<Relationship> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the relationships from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of relationships.
	 *
	 * @return the number of relationships
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static RelationshipPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(RelationshipPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile RelationshipPersistence _persistence;

}