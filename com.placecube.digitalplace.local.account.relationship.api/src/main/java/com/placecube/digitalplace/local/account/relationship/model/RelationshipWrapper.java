/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.local.account.relationship.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Relationship}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Relationship
 * @generated
 */
public class RelationshipWrapper
	extends BaseModelWrapper<Relationship>
	implements ModelWrapper<Relationship>, Relationship {

	public RelationshipWrapper(Relationship relationship) {
		super(relationship);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("relationshipId", getRelationshipId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createDate", getCreateDate());
		attributes.put("userId", getUserId());
		attributes.put("relUserId", getRelUserId());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long relationshipId = (Long)attributes.get("relationshipId");

		if (relationshipId != null) {
			setRelationshipId(relationshipId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long relUserId = (Long)attributes.get("relUserId");

		if (relUserId != null) {
			setRelUserId(relUserId);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	@Override
	public Relationship cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this relationship.
	 *
	 * @return the company ID of this relationship
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this relationship.
	 *
	 * @return the create date of this relationship
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the primary key of this relationship.
	 *
	 * @return the primary key of this relationship
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the relationship ID of this relationship.
	 *
	 * @return the relationship ID of this relationship
	 */
	@Override
	public long getRelationshipId() {
		return model.getRelationshipId();
	}

	/**
	 * Returns the rel user ID of this relationship.
	 *
	 * @return the rel user ID of this relationship
	 */
	@Override
	public long getRelUserId() {
		return model.getRelUserId();
	}

	/**
	 * Returns the rel user uuid of this relationship.
	 *
	 * @return the rel user uuid of this relationship
	 */
	@Override
	public String getRelUserUuid() {
		return model.getRelUserUuid();
	}

	/**
	 * Returns the type of this relationship.
	 *
	 * @return the type of this relationship
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this relationship.
	 *
	 * @return the user ID of this relationship
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this relationship.
	 *
	 * @return the user uuid of this relationship
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this relationship.
	 *
	 * @return the uuid of this relationship
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this relationship.
	 *
	 * @param companyId the company ID of this relationship
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this relationship.
	 *
	 * @param createDate the create date of this relationship
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the primary key of this relationship.
	 *
	 * @param primaryKey the primary key of this relationship
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the relationship ID of this relationship.
	 *
	 * @param relationshipId the relationship ID of this relationship
	 */
	@Override
	public void setRelationshipId(long relationshipId) {
		model.setRelationshipId(relationshipId);
	}

	/**
	 * Sets the rel user ID of this relationship.
	 *
	 * @param relUserId the rel user ID of this relationship
	 */
	@Override
	public void setRelUserId(long relUserId) {
		model.setRelUserId(relUserId);
	}

	/**
	 * Sets the rel user uuid of this relationship.
	 *
	 * @param relUserUuid the rel user uuid of this relationship
	 */
	@Override
	public void setRelUserUuid(String relUserUuid) {
		model.setRelUserUuid(relUserUuid);
	}

	/**
	 * Sets the type of this relationship.
	 *
	 * @param type the type of this relationship
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this relationship.
	 *
	 * @param userId the user ID of this relationship
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this relationship.
	 *
	 * @param userUuid the user uuid of this relationship
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this relationship.
	 *
	 * @param uuid the uuid of this relationship
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	protected RelationshipWrapper wrap(Relationship relationship) {
		return new RelationshipWrapper(relationship);
	}

}