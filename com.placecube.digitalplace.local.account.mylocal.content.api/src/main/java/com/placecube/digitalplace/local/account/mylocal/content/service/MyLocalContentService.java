package com.placecube.digitalplace.local.account.mylocal.content.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;

public interface MyLocalContentService {

	String getDateValue(long companyId, JSONObject jsonObject) throws PortalException;

	JSONObject getFoodWasteDay(long companyId, String uprn) throws PortalException;

	JSONObject getGardenWasteDay(long companyId, String uprn) throws PortalException;

	JSONObject getRecyclingDay(long companyId, String uprn) throws PortalException;

	JSONObject getRefuseDay(long companyId, String uprn) throws PortalException;
}
