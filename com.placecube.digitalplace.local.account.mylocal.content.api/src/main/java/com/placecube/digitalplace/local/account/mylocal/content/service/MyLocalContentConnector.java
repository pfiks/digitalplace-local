package com.placecube.digitalplace.local.account.mylocal.content.service;

import com.liferay.portal.kernel.json.JSONObject;

public interface MyLocalContentConnector {

	boolean enabled(long companyId);

	JSONObject getFoodWasteDay(long companyId, String uprn);

	JSONObject getGardenWasteDay(long companyId, String uprn);

	String getDateValue(JSONObject jsonObject);

	JSONObject getRecyclingDay(long companyId, String uprn);

	JSONObject getRefuseDay(long companyId, String uprn);
}
