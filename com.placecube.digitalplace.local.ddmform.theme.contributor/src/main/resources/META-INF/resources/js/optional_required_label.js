AUI().ready(

	function () {

		setTimeout(function () {
			$('form label, .portlet-forms .ddm-label').addClass('optional');
			$('form legend, .portlet-forms .lfr-ddm-legend').addClass('optional');
			$('form label .reference-mark, .portlet-forms .ddm-label .reference-mark').parent().removeClass('optional').addClass('required');
			$('form legend .reference-mark, .portlet-forms .lfr-ddm-legend .reference-mark').parent().removeClass('optional').addClass('required');
			$('.portlet-forms .liferay-ddm-form-field-paragraph').parent().find('.ddm-label, .lfr-ddm-legend').removeClass('optional');
			$('.portlet-forms .ddm-form-field-summarypage').parent().find('.ddm-label, .lfr-ddm-legend').removeClass('optional');
		}, 500);
	}
);
