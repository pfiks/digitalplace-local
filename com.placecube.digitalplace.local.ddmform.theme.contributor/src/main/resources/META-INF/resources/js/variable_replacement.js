const contentElements = '.ddm-field p, .ddm-field li, .ddm-field h2, .ddm-field h3, .ddm-field h4, .ddm-field legend, .ddm-field span.form-text, .ddm-field dd, .ddm-field dt, .ddm-field th, .ddm-field td';
var fieldReferenceMap;

const isElementNotPresent = async selector => {
	while ( document.querySelector(selector) ) {
	  await new Promise( resolve =>  requestAnimationFrame(resolve) )
	}
	return true;
};

Liferay.on('ddmFormPageShow', function (event) {

	isElementNotPresent('.ddm-field-container .loading-animation').then((selector) => {

		const observer = new MutationObserver((mutations, observer) => {
			let triggerUpdate = mutations.filter(mutationRecord => mutationRecord.target.className === 'ddm-field' || mutationRecord.target.getAttribute('data-variable-field') === 'true');
	
			if (triggerUpdate.length > 0) {
				updateVariables();
				overrideInputSetter();
			}
		});
	
		$('.portlet-forms-display, .form-renderer').each(function () {
			observer.observe(this, {
				childList: true,
				subtree: true
			});
		});

		const inputValueObserver = new MutationObserver((mutations, observer) => {
			let triggerUpdate = mutations.filter(mutationRecord => mutationRecord.target.tagName === 'INPUT' && $(mutationRecord.target).closest('.ddm-field').find('.form-control-select').attr('data-variable-field') === 'true');

			if (triggerUpdate.length > 0) {
				updateVariables();
			}
		});
		
		$('input[type="hidden"]').each(function () {
			let ddmField = $(this).closest(".ddm-field");
			
			if (ddmField.length > 0) {
				if ($(ddmField).find("button.form-control-select").length > 0) {
					inputValueObserver.observe(this, {
						attributeFilter: ['value']
					});
				}
			}
		});

		const paragraphObserver = new MutationObserver((mutations, observer) => {
			let triggerUpdate = mutations.filter(mutationRecord => mutationRecord.target.className === 'liferay-ddm-form-field-paragraph-text');
			if (triggerUpdate.length > 0) {
				tagVariablesInHTML();
				replaceURLVariablesInHTML();
				replaceFieldVariablesInHTML();
			}
		});

		$('.ddm-field .liferay-ddm-form-field-paragraph').each(function () {
			paragraphObserver.observe(this, {
				childList: true,
				subtree: true
			});
		});

		updateVariables();
		overrideInputSetter();
		addNameAttributeToRadioButtons();
	
		if (event.page > 0) {
			let target = $('form[data-ddmforminstanceid="' + event.formId + '"]');
			scrollToFormTop(target);
		}
    });

});

Liferay.on('allPortletsReady', function () {

	loadFieldReferenceMap();

	Liferay.on('fileFieldEdited', function (event) {
		updateVariables();
	});
	document.body.addEventListener('change', function (event) {
		if (event.target.getAttribute('data-variable-field') === 'true' || event.target.id.endsWith('_uprn') || event.target.id.endsWith('_postcode')) {
			updateVariables();
		}
	});

});

function addNameAttributeToRadioButtons() {

	$('.ddm-field-container').each(function(index, formFieldContainer) {

		const inputElements = $(formFieldContainer).find('input[type="radio"]');

		if (inputElements.length) {

			const dataFieldName = formFieldContainer.getAttribute('data-field-name');

			inputElements.each(function(index, inputElement){
				let attr = $(inputElement).attr('name');
				if ( attr === undefined || typeof attr === 'undefined' || attr === false) {
					inputElement.setAttribute("name", dataFieldName);
				}
			});

		}

	});

}

function updateVariables() {
	tagVariablesInHTML();
	tagVariablesInTextfields()
	replaceURLVariablesInHTML();
	replaceURLVariablesInTextfields();
	replaceFieldVariablesInHTML();
	replaceFieldVariablesInTextfields();
}

function tagVariablesInHTML() {
    $(".ddm-field .liferay-ddm-form-field-paragraph").each(function () {
        let text = $(this).html();
        let regex = /{{(.*?)}}/g;
        if(text.match(regex) != null){
            $(this).html(text.replace(regex, '<span data-replace-name="$1"></span>'));
        }
	});
}

function tagVariablesInTextfields() {
	$(".ddm-field input[type='text']").each(function () {
		let inputValue = $(this).val();
		if (inputValue.startsWith('[[') || inputValue.startsWith('{{')) {
			$(this).attr('data-variable', inputValue);
			setTextfieldValue($(this).attr('name'), ' ');
		}
	});
}

function replaceURLVariablesInHTML() {
    $(".ddm-field .liferay-ddm-form-field-paragraph").each(function () {
        let text = $(this).html();
        let regex = /\[\[(.*?)]]/;
        if(text.match(regex) != null ){
            $(this).html(text.replace(regex, replacer));
        }
    });
}

function replaceFieldVariablesInHTML() {
    $(".ddm-field .liferay-ddm-form-field-paragraph span[data-replace-name]").each(function () {
        let fieldReference = $(this).attr('data-replace-name');
        if( fieldReference ){
            let fieldName = getFieldName(fieldReference);
            let textValue = varReplacementGetFieldValue(fieldName);
            if (textValue && $(this).text() !== textValue) {
                $(this).text(textValue);
            }
        }
    });
}

function replaceURLVariablesInTextfields() {
	$("input[data-variable][data-variable*='[[']").each(function () {
		var text = $(this).attr('data-variable');
		var value = text.replace(/\[\[(.*?)]]/, replacer);
		var name = $(this).attr('name');

		if (value && value != null && value != 'null') {
			$(this).attr('data-variable', value);
			setTextfieldValue(name, value);
		}
	});
}

function replaceFieldVariablesInTextfields() {
	$("input[data-variable][data-variable*='{{']").each(function () {
		var text = $(this).attr('data-variable');
		var fieldName = text.replace(/{{(.*?)}}/, replacerFieldValue);
		let value = varReplacementGetFieldValue(fieldName);
		var name = $(this).attr('name');

		if (value === '') {
			value = ' ';
		}

		if ($(this).val() !== value) {
			setTextfieldValue(name, value);
		}
	});
}

function varReplacementGetFieldValue(fieldName) {
	if (fieldName.startsWith('File')) {
		let field = $('input[type="hidden"][name*="' + fieldName.split('.')[0] + '"]');
		field.attr("data-variable-field", "true");
		if (field.length === 0 || field.val() === '') return '';
		return fieldName.includes('.') ? JSON.parse(field.val())[fieldName.split('.')[1]] : field.val();
	} else if (fieldName == 'fullAddress') {
		let field = $('input[type="hidden"][name*="' + fieldName + '"]');
		field.attr("data-variable-field", "true");
		return field.val();
	} else if (fieldName == 'uprn') {
		let field = $('select[name*="' + fieldName + '"]');
		field.attr("data-variable-field", "true");
		return field.val();
	} else if ($('input[type="text"][name*="' + fieldName + '"]').length) {
		let field = $('input[type="text"][name*="' + fieldName + '"]');
		field.attr("data-variable-field", "true");
		return field.val();
	} else if ($('textarea[type="text"][name*="' + fieldName + '"]').length) {
		let field = $('textarea[type="text"][name*="' + fieldName + '"]');
		field.attr("data-variable-field", "true");
		return field.val();
	} else if ($('select[name*="' + fieldName + '"]').length) {
		let field = $('select[name*="' + fieldName + '"]');
		field.attr("data-variable-field", "true");
		return $('select[name*="' + fieldName + '"] option:selected').text();
	} else if ($('input[type="radio"][name*="' + fieldName + '"]').length) {
		$('input[type="radio"][name*="' + fieldName + '"]').attr("data-variable-field", "true");
		if ($('input[type="radio"][name*="' + fieldName + '"]:checked').length) {
			return $('input[type="radio"][name*="' + fieldName + '"]:checked').siblings('.custom-control-label').find('.custom-control-label-text').text();
		}
	} else if ($('div[data-field-name="' + fieldName + '"] .date-picker input[name="datePicker"]').length) {
		let field = $('div[data-field-name="' + fieldName + '"] .date-picker input[name="datePicker"]');
		field.attr("data-variable-field", "true");
		return field.val();
	} else if ($('div[data-field-name="' + fieldName + '"] button.form-control-select').length) {
		let field = $('div[data-field-name="' + fieldName + '"] button.form-control-select');
		field.attr("data-variable-field", "true");
		return getCleanValue(field.text());
	}
}

function getCleanValue(value) {
	if(value != undefined){
        var regex = /["\][]/g;
        value = value.replaceAll(regex, "");
	}
	return value;
}

function setTextfieldValue(fieldName, value) {
	var input = document.querySelector('[name="' + fieldName + '"]');

	const valueSetter = Object.getOwnPropertyDescriptor(input, 'value').set;
	const prototype = Object.getPrototypeOf(input);
	const prototypeValueSetter = Object.getOwnPropertyDescriptor(prototype, 'value').set;
	if (valueSetter && valueSetter !== prototypeValueSetter) {
		prototypeValueSetter.call(input, value);
	} else {
		valueSetter.call(input, value);
	}
	input.dispatchEvent(new Event('change', { bubbles: true, 'cancelable': true }));
	input.dispatchEvent(new Event('input', { bubbles: true, 'cancelable': true }));

	input.value = value;
}

function replacer(match, p1) {
	const urlParams = new URLSearchParams(window.location.search);
	return urlParams.get(p1) != null ? urlParams.get(p1) : '';
}

function replacerFieldValue(match, p1) {
	return getFieldName(p1);
}

function overrideInputSetter() {
	$('.portlet-forms input[type="text"]').each(function (index) {

		if (this.getAttribute('data-variable-field') === 'true') {

			const descriptor = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(this), 'value');

			Object.defineProperty(this, 'value', {
				set: function (t) {
					let setVar = descriptor.set.apply(this, arguments);
					updateVariables();
					return setVar;
				},
				get: function () {
					return descriptor.get.apply(this);
				},
				configurable: true
			});
		}
	});
}

function loadFieldReferenceMap() {
	fieldReferenceMap = new Map();

	$('.portlet-forms input[type="hidden"][name*="formInstanceId"]').each(function () {
		Liferay.Util.fetch(
			Liferay.ThemeDisplay.getPortalURL() + '/o/dp-headless-form/1.0.0/forms/' + this.value + '/form-structure',
			{
				method: 'GET',
				headers: {
				'Content-Type':
				'application/json'
				}
			}
        ).then(function(response) {
			if(response.ok){
				return data = response.json();
			}else{
				console.debug(response);
			}
        }).then(function(data) {
       		 if(data){
				let fields = JSON.parse(data.definition).fields;
				fields.forEach(function (field) {
					fieldReferenceMap.set(field.fieldReference, field.name);
				});
			}
        });
	});
}

function getFieldName(fieldReference) {
	let fieldName = fieldReferenceMap && fieldReferenceMap.get(fieldReference);
	if (fieldName === undefined) {
		fieldName = getFieldNameByFieldReferenceFromHTML(fieldReference);
	}
	
	return fieldName;
}

function getFieldNameByFieldReferenceFromHTML(fieldReference){

    let fieldName = fieldReference;
    let ddmField = $('div[data-field-reference="' + fieldReference + '"]');
    if(ddmField.length > 0){
       try{
        fieldName = $(ddmField).closest('.ddm-field').attr('data-field-name')
       }catch{}
    }
    return fieldName;
}

function scrollToFormTop(target) {
	$('html, body').animate({
		scrollTop: $(target).offset().top - 50
	}, 'slow');
}
